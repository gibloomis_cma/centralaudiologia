<?php
    // SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=lista_usuarios_sistema.xls");

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('php/config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_usuarios = "SELECT id_sesion,id_usuario,fecha_entrada,hora_entrada
                       FROM en_sistema";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_usuarios = mysql_query($query_usuarios) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>  </title>
	<!--<link type="text/css" rel="stylesheet" href="../css/style3.css" />-->
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
</head>
<body>
	<div id="wrapp">
		<div id="contenido_columna2">
    		<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:600px;">
           			</div>
        		</div><!--Fin de fondo titulo-->
        		<div class="area_contenido1">
            		<div class="contenido_proveedor">
            		<br />
               			<!--<div class="titulos"> Lista de Control de Moldes </div>-->
            		<br />
            		<table border="1" width="700">
            			<tr>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> N° de Sesi&oacute;n </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> N° de Usuario </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Nombre Completo </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Fecha de Entrada </th>
                            <th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Hora de Entrada </th>
            			</tr>
            		<?php
            			// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            			while ( $row_usuario = mysql_fetch_array($resultado_usuarios) )
            			{
                            $id_sesion = $row_usuario['id_sesion'];
                            $id_usuario = $row_usuario['id_usuario'];
                            $fecha_entrada = $row_usuario['fecha_entrada'];
                            $hora_entrada = $row_usuario['hora_entrada'];

                            $query_empleado = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo
                                               FROM usuarios,empleados
                                               WHERE id_usuario = '$id_usuario'
                                               AND usuarios.id_empleado = empleados.id_empleado";

                            $resultado_query_empleado = mysql_query($query_empleado) or die(mysql_error());
                            $row_nombre_empleado = mysql_fetch_array($resultado_query_empleado);
                            $nombre_empleado = $row_nombre_empleado['nombre_completo'];
                    ?>
                            <tr>
                                <td style="text-align: center;"> <?php echo $id_sesion; ?> </td>
                                <td style="text-align: center;"> <?php echo $id_usuario; ?> </td>
                                <td> <?php echo $nombre_empleado; ?> </td>
                                <td style="text-align: center;"> <?php echo $fecha_entrada; ?> </td>
                                <td style="text-align: center;"> <?php echo $hora_entrada; ?> </td>
                            </tr>
            		<?php
            			}
            		?>
            		</table>
            		<br/>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>