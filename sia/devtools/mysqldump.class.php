<?php
/*
* Database MySQLDump Class File
* Copyright (c) 2009 by James Elliott
* James.d.Elliott@gmail.com
* GNU General Public License v3 http://www.gnu.org/licenses/gpl.html
*/

$version1 = '1.3.2'; //This Scripts Version.

class MySQLDump {
	var $tables = array();
	var $connected = false;
	var $output;
	var $droptableifexists = true;
	var $onlytables = false;
	var $mysql_error;
	var $dir = "/";
	var $basename = "db-";
	var $excludetables = array();
	var $linesperfile = 1000;
	var $fileheader = "";
	var $linescount = 1;
	var $filecount = 1;
	var $fp = null;
	
function connect($host,$user,$pass,$db) {	
	$return = true;
	$conn = @mysql_connect($host,$user,$pass);
	if (!$conn) { $this->mysql_error = mysql_error(); $return = false; }
	$seldb = @mysql_select_db($db);
	if (!$conn) { $this->mysql_error = mysql_error();  $return = false; }
	$this->connected = $return;
	return $return;
}

function list_tables() {
	$return = true;
	if (!$this->connected) { $return = false; }
	$this->tables = array();
	$sql = mysql_query("SHOW TABLES");
	while ($row = mysql_fetch_array($sql)) {
		array_push($this->tables,$row[0]);
	}
	return $return;
}

function dobackup(){
	$this->creararchivo();
	$this->list_tables(); //List Database Tables.
	$broj = count($this->tables); //Count Database Tables.
	for ($i=0;$i<$broj;$i++) {
		$table_name = $this->tables[$i]; //Get Table Names.
		if (in_array(strtolower($table_name),$this->excludetables)){continue;};
		$this->dump_table($table_name);
	}
	@fclose($this->fp);	
}
function creararchivo(){
	@fclose($this->fp);
	$nvonombre = ($this->dir).($this->basename).($this->filecount).".sql";
	$this->fp = fopen($nvonombre,"w");
	fwrite($this->fp,$this->fileheader);
	$this->filecount++;
}

function list_values($tablename) {
	$sql = mysql_query("SELECT * FROM $tablename");
	//$this->output .= "\n\n-- Dumping data for table: $tablename\n\n";
	while ($row = mysql_fetch_array($sql)) {
		$this->output = "";############
		$broj_polja = count($row) / 2;
		$this->output .= "INSERT INTO `$tablename` VALUES(";
		$buffer = '';
		for ($i=0;$i < $broj_polja;$i++) {
			$vrednost = $row[$i];
			if (!is_integer($vrednost)) { $vrednost = "'".addslashes($vrednost)."'"; } 
			$buffer .= $vrednost.', ';
		}
		$buffer = substr($buffer,0,count($buffer)-3);
		$this->output .= $buffer . ");\n";
		###################
		$this->linescount++;
		if ($this->linescount >= $this->linesperfile){
			//crear nuevo archivo
			$this->linescount = 1;
			$this->creararchivo();
			
		}
		fwrite($this->fp,$this->output);
		##################
	}	
}

function dump_table($tablename) {
	$this->output = "";
	$this->get_table_structure($tablename);
	if (!$this->onlytables){	
		$this->list_values($tablename);
	}
}

function get_table_structure($tablename) {
	//$this->output .= "\n\n-- Dumping structure for table: $tablename\n\n";
	$this->output = "";
	if ($this->droptableifexists) { $this->output .= "\nDROP TABLE IF EXISTS `$tablename`;\nCREATE TABLE `$tablename` ("; }
		else { $this->output .= "CREATE TABLE `$tablename` ("; }
	$sql = mysql_query("DESCRIBE $tablename");
	$this->fields = array();
	while ($row = mysql_fetch_array($sql)) {
		$name = $row[0];
		$type = $row[1];
		$null = $row[2];
		if (empty($null)) {
			$null = "NOT NULL"; 
		} elseif ($null=="NO"){
			$null = "NOT NULL";
		} elseif ($null=="YES"){
			$null = "NULL";
		}
		
		$key = $row[3];
		if ($key == "PRI") { $primary = $name; }
		$default = $row[4];
		$extra = $row[5];
		if ($extra !== "") { $extra .= ' '; }
		$this->output .= " `$name` $type $null $extra,";
	}
	$this->output .= " PRIMARY KEY (`$primary`));\n";
	$this->linescount+=2;
	fwrite($this->fp,$this->output);
}

}
?>
