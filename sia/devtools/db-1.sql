USE `sia`;

DROP TABLE IF EXISTS `accesorios_problemas`;
CREATE TABLE `accesorios_problemas` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` int(11) NOT NULL , `estuche` varchar(2) NOT NULL , `molde` varchar(2) NOT NULL , `pila` varchar(2) NOT NULL , `no_se_oye` varchar(2) NOT NULL , `portapila` varchar(2) NOT NULL , `circuito` varchar(2) NOT NULL , `se_oye_dis` varchar(2) NOT NULL , `microfono` varchar(2) NOT NULL , `tono` varchar(2) NOT NULL , `falla` varchar(2) NOT NULL , `receptor` varchar(2) NOT NULL , `caja` varchar(2) NOT NULL , `gasta_pila` varchar(2) NOT NULL , `control_volumen` varchar(2) NOT NULL , `contactos_pila` varchar(2) NOT NULL , `cayo_piso` varchar(2) NOT NULL , `switch` varchar(2) NOT NULL , `cambio_tubo` varchar(2) NOT NULL , `diagnostico` varchar(2) NOT NULL , `bobina` varchar(2) NOT NULL , `cableado` varchar(2) NOT NULL , `limpieza` varchar(2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `actividades_extra_laborales`;
CREATE TABLE `actividades_extra_laborales` ( `id_registro_actividad` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `actividad` varchar(250) NOT NULL , `desde_que_ano` int(4) NOT NULL , `expuesto_a_ruido` varchar(2) NOT NULL , `utiliza_solventes_o_metales` varchar(2) NOT NULL , `otros_factores_riesgo_auditivo` varchar(250) NOT NULL , PRIMARY KEY (`id_registro_actividad`));

DROP TABLE IF EXISTS `adaptaciones_pacientes`;
CREATE TABLE `adaptaciones_pacientes` ( `id_registro_adaptaciones` int(11) NOT NULL auto_increment , `id_registro_relacion_aparato` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `costo_total` decimal(10,2) NOT NULL , `estatus_pago` varchar(50) NOT NULL , PRIMARY KEY (`id_registro_adaptaciones`));

DROP TABLE IF EXISTS `almacenes`;
CREATE TABLE `almacenes` ( `id_almacen` int(11) NOT NULL auto_increment , `id_sucursal` int(11) NOT NULL , `almacen` varchar(45) NOT NULL , PRIMARY KEY (`id_almacen`));

DROP TABLE IF EXISTS `anamnesis`;
CREATE TABLE `anamnesis` ( `id_anamnesis` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `fecha_elaboracion` date NOT NULL , `elaboro` int(11) NOT NULL , PRIMARY KEY (`id_anamnesis`));

DROP TABLE IF EXISTS `antecedentes_familiares_consaguineos`;
CREATE TABLE `antecedentes_familiares_consaguineos` ( `id_registro_consaguineos` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `sordera` varchar(2) NOT NULL , `hipertencion` varchar(2) NOT NULL , `diabeticos` varchar(2) NOT NULL , `con_sordera_infantil` varchar(2) NOT NULL , `otros` varchar(250) NOT NULL , PRIMARY KEY (`id_registro_consaguineos`));

DROP TABLE IF EXISTS `antecedentes_personales_no_patologicos`;
CREATE TABLE `antecedentes_personales_no_patologicos` ( `id_registro_no_patalogicos` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `lugar_residencia` int(11) NOT NULL , `ocupacion_actual` varchar(250) NOT NULL , `desde_el_ano` int(4) NOT NULL , `expuesto_a_ruido` varchar(2) NOT NULL , `utiliza_solventes_o_metales_actual` varchar(2) NOT NULL , `otros_factores_riesgo_auditivo` varchar(250) NOT NULL , `otros_trabajos_expuesto_riesgos` varchar(2) NOT NULL , `practica_alguna_actividad_expuesto_riesgos` varchar(2) NOT NULL , PRIMARY KEY (`id_registro_no_patalogicos`));

DROP TABLE IF EXISTS `antecedentes_personales_patologicos`;
CREATE TABLE `antecedentes_personales_patologicos` ( `id_registro_patologicos` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `presenta_sordera` varchar(2) NOT NULL , `hipertencion_personales` varchar(2) NOT NULL , `diabeticos_personales` varchar(2) NOT NULL , `infecciones_garganta` varchar(2) NOT NULL , `cirugia_traumatismo` varchar(2) NOT NULL , PRIMARY KEY (`id_registro_patologicos`));

DROP TABLE IF EXISTS `areas_departamentos`;
CREATE TABLE `areas_departamentos` ( `id_departamento` int(11) NOT NULL auto_increment , `id_sucursal` int(11) NOT NULL , `departamentos` varchar(30) NOT NULL , PRIMARY KEY (`id_departamento`));

DROP TABLE IF EXISTS `base_productos`;
CREATE TABLE `base_productos` ( `id_base_producto` int(11) NOT NULL auto_increment , `id_categoria` int(11) NOT NULL , `id_subcategoria` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `referencia_codigo` varchar(30) NOT NULL , `descripcion` varchar(250) NOT NULL , `precio_venta` decimal(10,2) NOT NULL , `precio_compra` decimal(10,2) NOT NULL , `activo` varchar(2) NOT NULL , `inventario_minimo` int(11) NOT NULL , `inventario_maximo` int(11) NOT NULL , PRIMARY KEY (`id_base_producto`));

DROP TABLE IF EXISTS `base_productos_2`;
CREATE TABLE `base_productos_2` ( `id_base_producto2` int(11) NOT NULL auto_increment , `id_categoria` int(11) NOT NULL , `id_subcategoria` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `referencia_codigo` varchar(30) NOT NULL , `descripcion` varchar(250) NOT NULL , `precio_venta` decimal(10,2) NOT NULL , `precio_compra` decimal(10,2) NOT NULL , `activo` varchar(2) NOT NULL , PRIMARY KEY (`id_base_producto2`));

DROP TABLE IF EXISTS `baterias`;
CREATE TABLE `baterias` ( `id_bateria` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `referencia` varchar(45) NOT NULL , `descripcion` varchar(45) NOT NULL , PRIMARY KEY (`id_bateria`));

DROP TABLE IF EXISTS `cat_nivel1`;
CREATE TABLE `cat_nivel1` ( `id_cat_nivel1` int(11) NOT NULL auto_increment , `id_material` int(11) NOT NULL , `id_catalogo` int(11) NOT NULL , PRIMARY KEY (`id_cat_nivel1`));

DROP TABLE IF EXISTS `cat_nivel2`;
CREATE TABLE `cat_nivel2` ( `id_cat_nivel2` int(11) NOT NULL auto_increment , `id_cat_nivel1` int(11) NOT NULL , `id_ventilacion` int(11) NOT NULL , PRIMARY KEY (`id_cat_nivel2`));

DROP TABLE IF EXISTS `cat_nivel3`;
CREATE TABLE `cat_nivel3` ( `id_cat_nivel3` int(11) NOT NULL auto_increment , `id_cat_nivel2` int(11) NOT NULL , `id_salida` int(11) NOT NULL , PRIMARY KEY (`id_cat_nivel3`));

DROP TABLE IF EXISTS `catalogo`;
CREATE TABLE `catalogo` ( `id_catalogo` int(11) NOT NULL auto_increment , `id_estilo` int(11) NOT NULL , PRIMARY KEY (`id_catalogo`));

DROP TABLE IF EXISTS `categorias_productos`;
CREATE TABLE `categorias_productos` ( `id_categoria` int(11) NOT NULL auto_increment , `categoria` varchar(25) NOT NULL , PRIMARY KEY (`id_categoria`));

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE `ciudades` ( `id_ciudad` int(11) NOT NULL auto_increment , `ciudad` varchar(100) NOT NULL , `id_estado` int(11) NOT NULL , PRIMARY KEY (`id_ciudad`));

DROP TABLE IF EXISTS `clientes_instituciones`;
CREATE TABLE `clientes_instituciones` ( `id_clientes_instituciones` int(11) NOT NULL auto_increment , `id_institucion` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , PRIMARY KEY (`id_clientes_instituciones`));

DROP TABLE IF EXISTS `colores`;
CREATE TABLE `colores` ( `id_color` int(11) NOT NULL auto_increment , `color` varchar(50) NOT NULL , `r` int(11) NOT NULL , `g` int(11) NOT NULL , `b` int(11) NOT NULL , PRIMARY KEY (`id_color`));

DROP TABLE IF EXISTS `comisiones_consultas`;
CREATE TABLE `comisiones_consultas` ( `id_registro` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_empleado` int(11) NOT NULL , `comision` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `consultas`;
CREATE TABLE `consultas` ( `id_consulta` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `referencia` varchar(45) NOT NULL , `descripcion` varchar(60) NOT NULL , `precio` decimal(10,2) NOT NULL , PRIMARY KEY (`id_consulta`));

DROP TABLE IF EXISTS `contacto_empleados`;
CREATE TABLE `contacto_empleados` ( `id_contacto_empleado` int(11) NOT NULL auto_increment , `id_empleado` int(11) NOT NULL , `tipo_telefono` varchar(20) NOT NULL , `descripcion` varchar(25) NOT NULL , PRIMARY KEY (`id_contacto_empleado`));

DROP TABLE IF EXISTS `contactos_clientes`;
CREATE TABLE `contactos_clientes` ( `id_contactos_cliente` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `tipo` varchar(8) NOT NULL , `descripcion` varchar(45) NOT NULL , `titular` varchar(30) NOT NULL , PRIMARY KEY (`id_contactos_cliente`));

DROP TABLE IF EXISTS `contactos_instituciones`;
CREATE TABLE `contactos_instituciones` ( `id_contacto_institucion` int(11) NOT NULL auto_increment , `id_institucion` int(11) NOT NULL , `nombre` varchar(30) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , `puesto` varchar(45) NOT NULL , `departamento` varchar(45) NOT NULL , PRIMARY KEY (`id_contacto_institucion`));

DROP TABLE IF EXISTS `contactos_proveedores`;
CREATE TABLE `contactos_proveedores` ( `id_contacto_proveedor` int(11) NOT NULL auto_increment , `id_proveedor` int(11) NOT NULL , `nombre` varchar(30) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , PRIMARY KEY (`id_contacto_proveedor`));

DROP TABLE IF EXISTS `contador_clientes`;
CREATE TABLE `contador_clientes` ( `id_incrementador` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , PRIMARY KEY (`id_incrementador`));

DROP TABLE IF EXISTS `contador_empleados`;
CREATE TABLE `contador_empleados` ( `id_incrementador` int(11) NOT NULL auto_increment , `id_empleado` int(11) NOT NULL , PRIMARY KEY (`id_incrementador`));

DROP TABLE IF EXISTS `corte_ventas_diario`;
CREATE TABLE `corte_ventas_diario` ( `id_registro` int(11) NOT NULL auto_increment , `folio_corte` int(11) NOT NULL , `id_sucursal` int(11) NOT NULL , `fecha_corte` date NOT NULL , `hora` time NOT NULL , `id_empleado_corte` int(11) NOT NULL , `total_vendido` decimal(10,2) NOT NULL , `total_articulos` int(11) NOT NULL , `id_registro_venta` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `cuentas_articulos`;
CREATE TABLE `cuentas_articulos` ( `id_registro` int(11) NOT NULL auto_increment , `id_cuenta_por_pagar` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `cantidad_codigo` int(30) NOT NULL , `num_serie` varchar(30) NOT NULL , `costo` decimal(10,2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `cuentas_por_pagar`;
CREATE TABLE `cuentas_por_pagar` ( `id_registro` int(11) NOT NULL auto_increment , `folio_orden_compra` int(11) NOT NULL , `id_proveedor` int(11) NOT NULL , `fecha` varchar(10) NOT NULL , `hora` varchar(12) NOT NULL , `id_estatus` int(11) NOT NULL , PRIMARY KEY (`folio_orden_compra`));

DROP TABLE IF EXISTS `cuentas_proveedor`;
CREATE TABLE `cuentas_proveedor` ( `id_cuenta` int(11) NOT NULL auto_increment , `id_proveedor` int(11) NOT NULL , `id_banco` int(11) NOT NULL , `num_cuenta` varchar(25) NOT NULL , `referencia` varchar(25) NOT NULL , `clabe` varchar(18) NOT NULL , PRIMARY KEY (`id_cuenta`));

DROP TABLE IF EXISTS `descripcion_vale_baterias`;
CREATE TABLE `descripcion_vale_baterias` ( `id_registro` int(11) NOT NULL auto_increment , `id_vale_bateria` int(11) NOT NULL , `origen` int(11) NOT NULL , `codigo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `descripcion_vale_refacciones`;
CREATE TABLE `descripcion_vale_refacciones` ( `id_registro` int(11) NOT NULL auto_increment , `id_vale_refaccion` int(11) NOT NULL , `origen` int(11) NOT NULL , `codigo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `descripcion_venta`;
CREATE TABLE `descripcion_venta` ( `id_registro` int(11) NOT NULL auto_increment , `id_sucursal` int(11) NOT NULL , `id_categoria` int(11) NOT NULL , `folio_num_venta` int(11) NOT NULL , `descripcion` varchar(100) NOT NULL , `cantidad` int(11) NOT NULL , `costo_unitario` decimal(10,2) NOT NULL , `pago_parcial` decimal(10,2) NOT NULL , `pago_parcial_2` decimal(10,2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `emisiones_otoacusticas`;
CREATE TABLE `emisiones_otoacusticas` ( `id_registro_emisiones` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `resultado` varchar(8) NOT NULL , `veces_que_realizo_estudio` varchar(5) NOT NULL , `protocolo_pase` varchar(5) NOT NULL , `fecha_prueba` date NOT NULL , `hora_prueba` varchar(12) NOT NULL , `tipo_emisiones` varchar(5) NOT NULL , `diagnostico` varchar(250) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `empleados`;
CREATE TABLE `empleados` ( `id_empleado` int(11) NOT NULL auto_increment , `nombre` varchar(30) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , `ocupacion` varchar(15) NOT NULL , `alias` varchar(15) NOT NULL , `calle` varchar(50) NOT NULL , `num_exterior` int(4) NOT NULL , `num_interior` varchar(3) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `colonia` varchar(50) NOT NULL , `id_departamento` int(11) NOT NULL , `id_ciudad` int(11) NOT NULL , `id_estado` int(11) NOT NULL , `genero` varchar(10) NOT NULL , `fecha_nacimiento` varchar(10) NOT NULL , `estado_civil` varchar(12) NOT NULL , PRIMARY KEY (`id_empleado`));

DROP TABLE IF EXISTS `en_sistema`;
CREATE TABLE `en_sistema` ( `id_sesion` int(11) NOT NULL auto_increment , `id_usuario` int(11) NOT NULL , `fecha_entrada` varchar(10) NOT NULL , `hora_entrada` varchar(11) NOT NULL , `fecha_salida` varchar(10) NOT NULL , `hora_salida` varchar(11) NOT NULL , `ip` varchar(20) NOT NULL , `mac` varchar(20) NOT NULL , PRIMARY KEY (`id_sesion`));

DROP TABLE IF EXISTS `entrada_almacen_general`;
CREATE TABLE `entrada_almacen_general` ( `id_registro` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `id_almacen` int(11) NOT NULL , `num_serie_cantidad` varchar(20) NOT NULL , `fecha_entrada` date NOT NULL , `hora` varchar(15) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` ( `id_estado` int(11) NOT NULL auto_increment , `estado` varchar(255) NULL , `id_pais` int(11) NOT NULL , PRIMARY KEY (`id_estado`));

DROP TABLE IF EXISTS `estados_movimientos`;
CREATE TABLE `estados_movimientos` ( `id_estado_movimiento` int(11) NOT NULL auto_increment , `estado_movimiento` varchar(15) NOT NULL , PRIMARY KEY (`id_estado_movimiento`));

DROP TABLE IF EXISTS `estatus_cuentas`;
CREATE TABLE `estatus_cuentas` ( `id_estatus_cuentas` int(11) NOT NULL auto_increment , `estatus_cuenta` varchar(25) NOT NULL , PRIMARY KEY (`id_estatus_cuentas`));

DROP TABLE IF EXISTS `estatus_moldes`;
CREATE TABLE `estatus_moldes` ( `id_estatus_moldes` int(11) NOT NULL auto_increment , `estado_molde` varchar(50) NOT NULL , PRIMARY KEY (`id_estatus_moldes`));

DROP TABLE IF EXISTS `estatus_ordenes`;
CREATE TABLE `estatus_ordenes` ( `id_estatus_orden` int(11) NOT NULL auto_increment , `estatus_orden` varchar(30) NOT NULL , PRIMARY KEY (`id_estatus_orden`));

DROP TABLE IF EXISTS `estatus_relaciones_aparatos`;
CREATE TABLE `estatus_relaciones_aparatos` ( `id_registro` int(11) NOT NULL auto_increment , `id_estatus_relacion` int(11) NOT NULL , `estatus_relacion` varchar(30) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `estatus_reparaciones`;
CREATE TABLE `estatus_reparaciones` ( `id_estatus_reparacion` int(11) NOT NULL auto_increment , `estado_reparacion` varchar(60) NOT NULL , PRIMARY KEY (`id_estatus_reparacion`));

DROP TABLE IF EXISTS `estilos`;
CREATE TABLE `estilos` ( `id_estilo` int(11) NOT NULL auto_increment , `estilo` varchar(60) NOT NULL , `foto` varchar(70) NOT NULL , `precio` decimal(10,2) NOT NULL , PRIMARY KEY (`id_estilo`));

DROP TABLE IF EXISTS `estubo_otros_trabajos`;
CREATE TABLE `estubo_otros_trabajos` ( `id_registro_otros_trabajos` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `ocupacion_anterior` varchar(250) NOT NULL , `desde_que_ano` int(4) NOT NULL , `hasta_que_ano` int(4) NOT NULL , `expuesto_a_ruido` varchar(2) NOT NULL , `utiliza_solventes_o_metales_pesados` varchar(2) NOT NULL , `otros_factores_riesgo_auditivo` varchar(250) NOT NULL , PRIMARY KEY (`id_registro_otros_trabajos`));

DROP TABLE IF EXISTS `estudio_audiometrico`;
CREATE TABLE `estudio_audiometrico` ( `id_registro_audiometrico` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `respuesta1` varchar(250) NOT NULL , `respuesta2` varchar(250) NOT NULL , `respuesta3` varchar(250) NOT NULL , `respuesta4` varchar(250) NOT NULL , `respuesta5` varchar(250) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `estudios`;
CREATE TABLE `estudios` ( `id_registro` int(11) NOT NULL auto_increment , `tipo` varchar(30) NOT NULL , `db` int(11) NOT NULL , `hz` int(11) NOT NULL , `oido` varchar(30) NOT NULL , `forma` varchar(30) NOT NULL , `respuesta` varchar(30) NOT NULL , `id_paciente` int(11) NOT NULL , `id_estudio` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `ficha_identificacion`;
CREATE TABLE `ficha_identificacion` ( `id_cliente` int(11) NOT NULL auto_increment , `nombre` varchar(30) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , `calle` varchar(30) NOT NULL , `num_exterior` int(5) NOT NULL , `num_interior` varchar(6) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `colonia` varchar(30) NOT NULL , `fecha_nacimiento` varchar(10) NOT NULL , `edad` int(11) NOT NULL , `genero` varchar(10) NOT NULL , `ocupacion` varchar(30) NOT NULL , `id_estado` int(11) NOT NULL , `id_ciudad` int(11) NOT NULL , `rfc` varchar(25) NOT NULL , PRIMARY KEY (`id_cliente`));

DROP TABLE IF EXISTS `ficha_tecnica`;
CREATE TABLE `ficha_tecnica` ( `id_paciente` int(11) NOT NULL auto_increment , `nombre` varchar(50) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , `genero` varchar(20) NOT NULL , PRIMARY KEY (`id_paciente`));

DROP TABLE IF EXISTS `folios_num_estudios`;
CREATE TABLE `folios_num_estudios` ( `id_proceso` int(11) NOT NULL , `folio_num_estudio` int(11) NOT NULL auto_increment , PRIMARY KEY (`folio_num_estudio`));

DROP TABLE IF EXISTS `folios_num_nota_reparacion`;
CREATE TABLE `folios_num_nota_reparacion` ( `id_proceso` int(11) NOT NULL , `folio_nota_reparacion` int(11) NOT NULL auto_increment , PRIMARY KEY (`folio_nota_reparacion`));

DROP TABLE IF EXISTS `folios_orden_compra`;
CREATE TABLE `folios_orden_compra` ( `id_proceso` int(11) NOT NULL , `folio_orden_compra` int(11) NOT NULL auto_increment , PRIMARY KEY (`folio_orden_compra`));

DROP TABLE IF EXISTS `forma_contacto_instituciones`;
CREATE TABLE `forma_contacto_instituciones` ( `id_forma_contacto` int(11) NOT NULL auto_increment , `id_contacto_institucion` int(11) NOT NULL , `tipo` varchar(8) NOT NULL , `descripcion` varchar(45) NOT NULL , PRIMARY KEY (`id_forma_contacto`));

DROP TABLE IF EXISTS `forma_contacto_proveedores`;
CREATE TABLE `forma_contacto_proveedores` ( `id_forma_contacto` int(11) NOT NULL auto_increment , `id_contacto_proveedor` int(11) NOT NULL , `tipo` varchar(8) NOT NULL , `descripcion` varchar(45) NOT NULL , PRIMARY KEY (`id_forma_contacto`));

DROP TABLE IF EXISTS `forma_contacto_sucursal`;
CREATE TABLE `forma_contacto_sucursal` ( `id_forma_contacto_sucursal` int(11) NOT NULL auto_increment , `tipo` varchar(30) NOT NULL , `descripcion` varchar(30) NOT NULL , `id_sucursal` int(11) NOT NULL , PRIMARY KEY (`id_forma_contacto_sucursal`));

DROP TABLE IF EXISTS `frecuencias`;
CREATE TABLE `frecuencias` ( `id_registro` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `identificador` int(2) NOT NULL , `frecuencia` varchar(8) NOT NULL , `snr` varchar(6) NOT NULL , `pasa` varchar(2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `historial_clinico`;
CREATE TABLE `historial_clinico` ( `id_registro` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `fecha` date NOT NULL , `atendio` int(11) NOT NULL , `notas` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `imagenes_videotoscopia`;
CREATE TABLE `imagenes_videotoscopia` ( `id_registro` int(11) NOT NULL auto_increment , `id_estudio` int(11) NOT NULL , `id_identificador` int(11) NOT NULL , `tipo` varchar(4) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `instituciones`;
CREATE TABLE `instituciones` ( `id_institucion` int(11) NOT NULL auto_increment , `institucion` varchar(50) NOT NULL , `calle` varchar(30) NOT NULL , `num_exterior` int(4) NOT NULL , `num_interior` varchar(6) NOT NULL , `colonia` varchar(30) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `id_estado` int(11) NOT NULL , `id_ciudad` int(11) NOT NULL , `descuento` int(3) NOT NULL , `costo_aparato` decimal(10,2) NOT NULL , `poliza` varchar(250) NOT NULL , `razon_social` varchar(50) NOT NULL , `rfc` varchar(30) NOT NULL , PRIMARY KEY (`id_institucion`));

DROP TABLE IF EXISTS `inventario_baterias_anterior`;
CREATE TABLE `inventario_baterias_anterior` ( `id_registro` int(11) NOT NULL auto_increment , `fecha_inventario` date NOT NULL , `id_almacen` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `inventario_dias_anteriores`;
CREATE TABLE `inventario_dias_anteriores` ( `id_registro` int(11) NOT NULL auto_increment , `fecha_inventario` date NOT NULL , `id_almacen` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `inventario_temporal_baterias_2`;
CREATE TABLE `inventario_temporal_baterias_2` ( `id_registro` int(11) NOT NULL auto_increment , `codigo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `tipo` varchar(15) NOT NULL , `id_almacen` int(11) NOT NULL , `fecha` date NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `inventarios`;
CREATE TABLE `inventarios` ( `id_registro` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_articulo` int(11) NOT NULL , `id_almacen` int(11) NOT NULL , `num_serie_cantidad` varchar(15) NOT NULL , `fecha_entrada` date NOT NULL , `hora` varchar(12) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `logo_audiometria`;
CREATE TABLE `logo_audiometria` ( `id_registro_logo_audiometria` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `umbral_percepcion_izq` int(11) NOT NULL , `umbral_percepcion_der` int(11) NOT NULL , `nivel_maxima_comodidad_izq` int(11) NOT NULL , `nivel_maxima_comodidad_der` int(11) NOT NULL , `nivel_incomodidad_izq` int(11) NOT NULL , `nivel_incomodidad_der` int(11) NOT NULL , `metodo` varchar(250) NOT NULL , `cooperacion` varchar(250) NOT NULL , `enmascarador` int(11) NOT NULL , `nivel_habla_izq` int(11) NOT NULL , `nivel_habla_der` int(11) NOT NULL , `porcentaje_izq` int(11) NOT NULL , `porcentaje_der` int(11) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `materiales`;
CREATE TABLE `materiales` ( `id_material` int(11) NOT NULL auto_increment , `material` varchar(60) NOT NULL , `descripcion` varchar(250) NOT NULL , `precio` decimal(10,2) NOT NULL , PRIMARY KEY (`id_material`));

DROP TABLE IF EXISTS `materiales_nivel1`;
CREATE TABLE `materiales_nivel1` ( `id_material_nivel1` int(11) NOT NULL auto_increment , `id_material` int(11) NOT NULL , `id_color` int(11) NOT NULL , PRIMARY KEY (`id_material_nivel1`));

DROP TABLE IF EXISTS `modelos`;
CREATE TABLE `modelos` ( `id_modelo` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_proveedor` int(11) NOT NULL , `modelo` varchar(45) NOT NULL , `id_tipo_modelo` int(11) NOT NULL , `comision` decimal(10,2) NOT NULL , PRIMARY KEY (`id_modelo`));

DROP TABLE IF EXISTS `modelos_2`;
CREATE TABLE `modelos_2` ( `id_modelo2` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_proveedor` int(11) NOT NULL , `modelo` varchar(45) NOT NULL , `id_tipo_modelo` int(11) NOT NULL , `comision` decimal(10,2) NOT NULL , PRIMARY KEY (`id_modelo2`));

DROP TABLE IF EXISTS `moldes`;
CREATE TABLE `moldes` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_molde` int(11) NOT NULL , `id_estudio` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `nombre` varchar(40) NOT NULL , `paterno` varchar(30) NOT NULL , `materno` varchar(30) NOT NULL , `tipo` varchar(15) NOT NULL , `descripcion` varchar(20) NOT NULL , `titular` varchar(30) NOT NULL , `calle` varchar(50) NOT NULL , `num_exterior` int(8) NOT NULL , `num_interior` varchar(5) NOT NULL , `codigo_postal` varchar(6) NOT NULL , `colonia` varchar(40) NOT NULL , `id_estado` int(11) NOT NULL , `id_ciudad` int(11) NOT NULL , `ciudad` varchar(150) NOT NULL , `fecha_entrada` date NOT NULL , `lado_oido` varchar(25) NOT NULL , `id_estilo` int(11) NOT NULL , `id_material` int(11) NOT NULL , `id_color` int(11) NOT NULL , `id_ventilacion` int(11) NOT NULL , `id_salida` int(11) NOT NULL , `costo` decimal(10,2) NOT NULL , `id_estatus_moldes` int(11) NOT NULL , `observaciones` varchar(250) NOT NULL , `fecha_nacimiento` varchar(10) NOT NULL , `adaptacion` varchar(2) NOT NULL , `reposicion` varchar(2) NOT NULL , `pago_parcial` decimal(10,2) NOT NULL , `pago_parcial_2` decimal(10,2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `moldes_elaboracion`;
CREATE TABLE `moldes_elaboracion` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_molde` int(11) NOT NULL , `elaboro` int(11) NOT NULL , `fecha_salida` date NOT NULL , `observaciones` varchar(250) NOT NULL , `son_de_adaptacion` int(2) NOT NULL , `cobro` decimal(10,2) NOT NULL , `recibio` varchar(20) NOT NULL , `quien_entrego` int(11) NOT NULL , `fecha_entrega` date NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `moldes_hecho_medida`;
CREATE TABLE `moldes_hecho_medida` ( `id_registro` int(11) NOT NULL auto_increment , `id_proveedor` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `nombre_cliente` varchar(60) NOT NULL , `id_estilo` int(11) NOT NULL , `id_material` int(11) NOT NULL , `id_color` int(11) NOT NULL , `id_salida` int(11) NOT NULL , `id_ventilacion` int(11) NOT NULL , `id_modelo` int(11) NOT NULL , `id_estudio` int(11) NOT NULL , `especiales` varchar(250) NOT NULL , `fecha_realizacion` date NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `movimientos`;
CREATE TABLE `movimientos` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` varchar(10) NOT NULL , `id_estado_movimiento` int(11) NOT NULL , `fecha` date NOT NULL , `hora` varchar(12) NOT NULL , `id_estatus_reparaciones` int(11) NOT NULL , `id_empleado` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `movimientos_moldes`;
CREATE TABLE `movimientos_moldes` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_molde` int(11) NOT NULL , `id_estado_movimiento` int(11) NOT NULL , `fecha` date NOT NULL , `hora` varchar(15) NOT NULL , `id_estatus_moldes` int(11) NOT NULL , `id_empleado` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `ordenes_de_compra`;
CREATE TABLE `ordenes_de_compra` ( `id_registro` int(11) NOT NULL auto_increment , `folio_orden_compra` int(11) NOT NULL , `fecha_orden` varchar(10) NOT NULL , `id_proveedor` int(11) NOT NULL , `id_estatus` int(11) NOT NULL , `id_categoria_producto` int(11) NOT NULL , `id_producto` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `fecha_entrega` varchar(10) NOT NULL , `precio_unitario` decimal(10,2) NOT NULL , PRIMARY KEY (`folio_orden_compra`));

DROP TABLE IF EXISTS `paciente_con_sordera_infantil`;
CREATE TABLE `paciente_con_sordera_infantil` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `fue_prematuro` varchar(2) NOT NULL , `tubo_bilirrubinas_altas` varchar(2) NOT NULL , `estubo_en_incubadora` varchar(2) NOT NULL , `otros_problemas_durante_embarazo` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `pago_consultas`;
CREATE TABLE `pago_consultas` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_venta` int(11) NOT NULL , `id_registro_descripcion_venta` int(11) NOT NULL , `id_subcategoria` int(11) NOT NULL , `id_empleado` int(11) NOT NULL , `fecha` date NOT NULL , `cantidad` int(11) NOT NULL , `pago` decimal(10,2) NOT NULL , `estado` varchar(25) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `pagos_adaptaciones_pacientes`;
CREATE TABLE `pagos_adaptaciones_pacientes` ( `id_registro` int(11) NOT NULL auto_increment , `id_registro_adaptaciones` int(11) NOT NULL , `fecha_pago` date NOT NULL , `cantidad` decimal(10,2) NOT NULL , `forma_pago` varchar(20) NOT NULL , `referencia` varchar(30) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `pagos_proveedores`;
CREATE TABLE `pagos_proveedores` ( `id_pago` int(11) NOT NULL auto_increment , `id_proveedor` int(11) NOT NULL , `folio_orden_compra` int(11) NOT NULL , `fecha_deposito` varchar(10) NOT NULL , `cantidad` decimal(10,2) NOT NULL , `cheque_transferencia` varchar(30) NOT NULL , `referencia` varchar(20) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_pago`));

DROP TABLE IF EXISTS `paises`;
CREATE TABLE `paises` ( `id_pais` int(11) NOT NULL auto_increment , `pais` varchar(50) NOT NULL , PRIMARY KEY (`id_pais`));

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE `proveedores` ( `id_proveedor` int(11) NOT NULL auto_increment , `proveedor` varchar(30) NOT NULL , `razon_social` varchar(50) NOT NULL , `calle` varchar(30) NOT NULL , `num_exterior` int(11) NOT NULL , `num_interior` varchar(6) NOT NULL , `colonia` varchar(30) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `id_ciudad` int(11) NOT NULL , `id_estado` int(11) NOT NULL , `id_pais` int(11) NOT NULL , `rfc` varchar(20) NOT NULL , `credito` decimal(10,2) NOT NULL , `plazos_pago` varchar(45) NOT NULL , PRIMARY KEY (`id_proveedor`));

DROP TABLE IF EXISTS `refacciones`;
CREATE TABLE `refacciones` ( `id_refaccion` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `id_proveedor` int(11) NOT NULL , `codigo` varchar(45) NOT NULL , `concepto` varchar(45) NOT NULL , `utilidad` decimal(10,2) NOT NULL , `descripcion` varchar(45) NOT NULL , PRIMARY KEY (`id_refaccion`));

DROP TABLE IF EXISTS `reflejos_acusticos`;
CREATE TABLE `reflejos_acusticos` ( `id_reflejos_acusticos` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `contra_ura_500` varchar(3) NOT NULL , `contra2_ura_500` varchar(3) NOT NULL , `contra_ura_1` varchar(3) NOT NULL , `contra2_ura_1` varchar(3) NOT NULL , `contra_ura_2` varchar(3) NOT NULL , `contra2_ura_2` varchar(3) NOT NULL , `contra_ura_4` varchar(3) NOT NULL , `contra2_ura_4` varchar(3) NOT NULL , `contra_caida_500` varchar(8) NOT NULL , `contra2_caida_500` varchar(8) NOT NULL , `contra_caida_1` varchar(8) NOT NULL , `contra2_caida_1` varchar(8) NOT NULL , `ipsi_ura_500` varchar(3) NOT NULL , `ipsi2_ura_500` varchar(3) NOT NULL , `ipsi_ura_1` varchar(3) NOT NULL , `ipsi2_ura_1` varchar(3) NOT NULL , `ipsi_ura_2` varchar(3) NOT NULL , `ipsi2_ura_2` varchar(3) NOT NULL , `ipsi_ura_4` varchar(3) NOT NULL , `ipsi2_ura_4` varchar(3) NOT NULL , `ipsi_caida_500` varchar(8) NOT NULL , `ipsi2_caida_500` varchar(8) NOT NULL , `ipsi_caida_1` varchar(8) NOT NULL , `ipsi2_caida_1` varchar(8) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `registro_moldes`;
CREATE TABLE `registro_moldes` ( `id_registro` int(11) NOT NULL auto_increment , `id_estudio` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `nombre_cliente` varchar(60) NOT NULL , `atendio` int(11) NOT NULL , `lado_oido` varchar(9) NOT NULL , `id_estilo` int(11) NOT NULL , `id_material` int(11) NOT NULL , `id_color` int(11) NOT NULL , `id_ventilacion` int(11) NOT NULL , `id_salida` int(11) NOT NULL , `especiales` varchar(250) NOT NULL , `fecha_realizacion` date NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `relacion_aparatos`;
CREATE TABLE `relacion_aparatos` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `id_ultimo_estudio` int(11) NOT NULL , `id_modelo` int(11) NOT NULL , `num_serie_cantidad` varchar(15) NOT NULL , `id_estatus_relacion` int(11) NOT NULL , `fecha_entrega` date NOT NULL , `realizo_venta` int(11) NOT NULL , `realizo_estudio` int(11) NOT NULL , `realizo_adaptacion` int(11) NOT NULL , `fecha_venta` date NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `reparaciones`;
CREATE TABLE `reparaciones` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `nombre` varchar(30) NOT NULL , `paterno` varchar(20) NOT NULL , `materno` varchar(20) NOT NULL , `tipo` varchar(15) NOT NULL , `descripcion` varchar(15) NOT NULL , `titular` varchar(15) NOT NULL , `calle` varchar(30) NOT NULL , `num_exterior` int(5) NOT NULL , `num_interior` varchar(6) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `colonia` varchar(30) NOT NULL , `id_estado` int(11) NOT NULL , `id_ciudad` int(11) NOT NULL , `fecha_entrada` date NOT NULL , `id_modelo` int(11) NOT NULL , `num_serie` varchar(25) NOT NULL , `id_estatus_reparaciones` int(11) NOT NULL , `descripcion_problema` varchar(250) NOT NULL , `reparacion` varchar(2) NOT NULL , `adaptacion` varchar(2) NOT NULL , `venta` varchar(2) NOT NULL , `aplica_garantia` varchar(2) NOT NULL , `no_reparado` varchar(2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `reparaciones_laboratorio`;
CREATE TABLE `reparaciones_laboratorio` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` int(11) NOT NULL , `presupuesto` decimal(10,2) NOT NULL , `tiempo_estimado_reparacion` int(11) NOT NULL , `autorizo_presupuesto` varchar(2) NOT NULL , `quien_autorizo` varchar(45) NOT NULL , `fecha_autorizacion` date NOT NULL , `fecha_salida` date NOT NULL , `reparado_por` int(11) NOT NULL , `costo` decimal(10,2) NOT NULL , `observaciones` varchar(250) NOT NULL , `fecha_entrega` date NOT NULL , `cobro` decimal(10,2) NOT NULL , `entrego` int(11) NOT NULL , `recibio` varchar(45) NOT NULL , `fecha_ultima_modificacion` date NOT NULL , `hora_ultima_modificacion` varchar(12) NOT NULL , PRIMARY KEY (`folio_num_reparacion`));

DROP TABLE IF EXISTS `residencias`;
CREATE TABLE `residencias` ( `id_residencia` int(11) NOT NULL auto_increment , `descripcion_residencia` varchar(50) NOT NULL , PRIMARY KEY (`id_residencia`));

DROP TABLE IF EXISTS `salidas`;
CREATE TABLE `salidas` ( `id_salida` int(11) NOT NULL auto_increment , `salida` varchar(70) NOT NULL , `precio` decimal(10,2) NOT NULL , `imagen` varchar(70) NOT NULL , PRIMARY KEY (`id_salida`));

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE `servicios` ( `id_servicio` int(11) NOT NULL auto_increment , `id_subcategoria` int(11) NOT NULL , `referencia` varchar(60) NOT NULL , `descripcion` varchar(100) NOT NULL , `precio` decimal(10,2) NOT NULL , PRIMARY KEY (`id_servicio`));

DROP TABLE IF EXISTS `servicios_reparaciones`;
CREATE TABLE `servicios_reparaciones` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` int(11) NOT NULL , `id_servicio` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `si_es_ruidoso`;
CREATE TABLE `si_es_ruidoso` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `fuente_ruido` varchar(250) NOT NULL , `tiempo_exposicion_diaria` int(11) NOT NULL , `id_registro_identificador` int(11) NOT NULL , `id_registro_tabla` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `si_ha_usado_auxiliar_auditivo`;
CREATE TABLE `si_ha_usado_auxiliar_auditivo` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `desde_que_ano` int(4) NOT NULL , `modelo` varchar(80) NOT NULL , `que_resultado_obtuvo` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `si_presenta_algun_padecimiento`;
CREATE TABLE `si_presenta_algun_padecimiento` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `descripcion` varchar(250) NOT NULL , `desde_que_ano` int(4) NOT NULL , `tratamiento` varchar(250) NOT NULL , `resultados` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `si_presenta_sordera`;
CREATE TABLE `si_presenta_sordera` ( `id_registro_` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `desde_que_ano` int(4) NOT NULL , `ha_usado_auxiliar_auditivo` varchar(2) NOT NULL , PRIMARY KEY (`id_registro_`));

DROP TABLE IF EXISTS `si_utiliza`;
CREATE TABLE `si_utiliza` ( `id_registro` int(11) NOT NULL auto_increment , `id_cliente` int(11) NOT NULL , `producto` varchar(250) NOT NULL , `id_registro_identificador` int(11) NOT NULL , `id_registro_tabla` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `subcategorias_productos`;
CREATE TABLE `subcategorias_productos` ( `id_subcategoria` int(11) NOT NULL auto_increment , `id_categoria` int(11) NOT NULL , `subcategoria` varchar(60) NOT NULL , PRIMARY KEY (`id_subcategoria`));

DROP TABLE IF EXISTS `subcategorias_productos_2`;
CREATE TABLE `subcategorias_productos_2` ( `id_subcategoria` int(11) NOT NULL auto_increment , `id_categoria` int(11) NOT NULL , `subcategoria` varchar(60) NOT NULL , PRIMARY KEY (`id_subcategoria`));

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE `sucursales` ( `id_sucursal` int(11) NOT NULL auto_increment , `nombre` varchar(45) NOT NULL , `calle` varchar(45) NOT NULL , `num_exterior` int(5) NOT NULL , `num_interior` varchar(6) NOT NULL , `colonia` varchar(45) NOT NULL , `codigo_postal` varchar(5) NOT NULL , `id_ciudad` int(11) NOT NULL , `id_estado` int(11) NOT NULL , PRIMARY KEY (`id_sucursal`));

DROP TABLE IF EXISTS `temporal_inventario_baterias`;
CREATE TABLE `temporal_inventario_baterias` ( `id_registro` int(11) NOT NULL auto_increment , `codigo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `tipo` varchar(15) NOT NULL , `id_almacen` int(11) NOT NULL , `fecha` date NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `timpanometria`;
CREATE TABLE `timpanometria` ( `id_timpanometria` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `frecuencia` int(11) NOT NULL , `tipo_derecho` varchar(3) NOT NULL , `tipo_izquierdo` varchar(3) NOT NULL , `admitancia_derecha` decimal(4,3) NOT NULL , `admitancia_izquierda` decimal(4,3) NOT NULL , `presion_maxima_derecha` int(4) NOT NULL , `presion_maxima_izquierda` int(4) NOT NULL , `gradiente_timpanometrico_derecha` decimal(4,3) NOT NULL , `gradiente_timpanometrico_izquierda` decimal(4,3) NOT NULL , `volumen_derecha` decimal(4,3) NOT NULL , `volumen_izquierda` decimal(4,3) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));

DROP TABLE IF EXISTS `tipos_modelos_aparatos`;
CREATE TABLE `tipos_modelos_aparatos` ( `id_tipo_modelo` int(11) NOT NULL auto_increment , `modelo_aparato` varchar(30) NOT NULL , PRIMARY KEY (`id_tipo_modelo`));

DROP TABLE IF EXISTS `uso_baterias`;
CREATE TABLE `uso_baterias` ( `id_uso_bateria` int(11) NOT NULL auto_increment , `uso_bateria` varchar(30) NOT NULL , PRIMARY KEY (`id_uso_bateria`));

DROP TABLE IF EXISTS `uso_refacciones`;
CREATE TABLE `uso_refacciones` ( `id_uso_refaccion` int(11) NOT NULL auto_increment , `uso` varchar(60) NOT NULL , PRIMARY KEY (`id_uso_refaccion`));

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` ( `id_usuario` int(11) NOT NULL auto_increment , `id_empleado` int(11) NOT NULL , `user` varchar(25) NOT NULL , `pass` varchar(16) NOT NULL , PRIMARY KEY (`id_usuario`));

DROP TABLE IF EXISTS `vale_bateria_temporal`;
CREATE TABLE `vale_bateria_temporal` ( `id_registro` int(11) NOT NULL auto_increment , `id_registro_inventario` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `id_registro_descripcion_vale_bateria` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `vale_refaccion_temporal`;
CREATE TABLE `vale_refaccion_temporal` ( `id_registro` int(11) NOT NULL auto_increment , `id_registro_inventario` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `id_registro_descripcion_vale_refaccion` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `vales_baterias`;
CREATE TABLE `vales_baterias` ( `id_vale_bateria` int(11) NOT NULL auto_increment , `fecha` date NOT NULL , `hora` varchar(12) NOT NULL , `uso_de_bateria` int(11) NOT NULL , `responsable` int(11) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_vale_bateria`));

DROP TABLE IF EXISTS `vales_refacciones`;
CREATE TABLE `vales_refacciones` ( `id_registro` int(11) NOT NULL auto_increment , `fecha` date NOT NULL , `hora` varchar(15) NOT NULL , `uso` int(11) NOT NULL , `responsable` int(11) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `vales_reparacion`;
CREATE TABLE `vales_reparacion` ( `id_registro` int(11) NOT NULL auto_increment , `folio_num_reparacion` int(11) NOT NULL , `codigo` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `costo_unitario` decimal(10,2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `venta_temporal`;
CREATE TABLE `venta_temporal` ( `id_registro` int(11) NOT NULL auto_increment , `id_registro_inventario` int(11) NOT NULL , `cantidad` int(11) NOT NULL , `id_registro_descripcion_venta` int(11) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` ( `id_registro` int(11) NOT NULL auto_increment , `id_sucursal` int(11) NOT NULL , `folio_num_venta` int(11) NOT NULL , `fecha` date NOT NULL , `hora` time NOT NULL , `descuento` int(3) NOT NULL , `vendedor` int(11) NOT NULL , `total` decimal(10,2) NOT NULL , PRIMARY KEY (`id_registro`));

DROP TABLE IF EXISTS `ventilaciones`;
CREATE TABLE `ventilaciones` ( `id_ventilacion` int(11) NOT NULL auto_increment , `nombre_ventilacion` varchar(60) NOT NULL , `calibre` varchar(45) NOT NULL , `precio` decimal(10,2) NOT NULL , `imagen` varchar(60) NOT NULL , PRIMARY KEY (`id_ventilacion`));

DROP TABLE IF EXISTS `videotoscopia`;
CREATE TABLE `videotoscopia` ( `id_videotoscopia` int(11) NOT NULL auto_increment , `id_nota_clinica` int(11) NOT NULL , `id_cliente` int(11) NOT NULL , `canal_derecho` varchar(250) NOT NULL , `membrana_derecha` varchar(250) NOT NULL , `canal_izquierdo` varchar(250) NOT NULL , `membrana_izquierda` varchar(250) NOT NULL , `observaciones` varchar(250) NOT NULL , PRIMARY KEY (`id_nota_clinica`));
