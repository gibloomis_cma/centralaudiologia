<?php
//funcion obtener ip
function getRealIP()
{
 
   if( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
   {
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
 
      // los proxys van añadiendo al final de esta cabecera
      // las direcciones ip que van "ocultando". Para localizar la ip real
      // del usuario se comienza a mirar por el principio hasta encontrar 
      // una dirección ip que no sea del rango privado. En caso de no 
      // encontrarse ninguna se toma como valor el REMOTE_ADDR
 
      $entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
 
      reset($entries);
      while (list(, $entry) = each($entries)) 
      {
         $entry = trim($entry);
         if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
         {
            // http://www.faqs.org/rfcs/rfc1918.html
            $private_ip = array(
                  '/^0\./', 
                  '/^127\.0\.0\.1/', 
                  '/^192\.168\..*/', 
                  '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
                  '/^10\..*/');
 
            $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
 
            if ($client_ip != $found_ip)
            {
               $client_ip = $found_ip;
               break;
            }
         }
      }
   }
   else
   {
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
   }
 
   return $client_ip;
 
}

    // SE INICIA SESION
	session_start();
	if(isset($_POST['login']))
    {
       	$us = $_POST['user'];
		$ps = $_POST['pass'];

        // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
		include('php/config.php');
		$en_sistema = mysql_query('SELECT id_usuario, id_empleado
								   FROM usuarios
								   WHERE user = "'.$us.'"
								   AND pass = "'.$ps.'"') or die(mysql_error());

		$row_ensistema = mysql_fetch_array($en_sistema);
		$id_user = $row_ensistema['id_usuario'];
		$id_empleado_usuario = $row_ensistema["id_empleado"];

        if($id_user != "")
        {
			echo $ip = getRealIP();
			$_SESSION['id_usuario'] = $id_user;
			$_SESSION['id_empleado_usuario'] = $id_empleado_usuario;
			$fecha_entrada = date('d-m-Y');
			$hra_entrada = date('H:i:s A');
			mysql_query('INSERT INTO en_sistema (id_usuario,fecha_entrada,hora_entrada, ip)
						 VALUES('.$id_user.',"'.$fecha_entrada.'","'.$hra_entrada.'", "'.$ip.'")') or die(mysql_error());
			$_SESSION['id_sesion'] = mysql_insert_id();
		}
        else
        {
			$_SESSION['id_usuario'] = "";
		}
	}
    else
    {
		$_SESSION['id_usuario'] = "";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistema Integral Administrativo | SIA</title>
<link rel="stylesheet" href="css/style3.css" type="text/css"/>
<!--Acordeon Lateral
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js' type='text/javascript'/></script>-->
<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type='text/javascript'>
//<![CDATA[
$(document).ready(function(){
  $("#accordion h3:first").addClass("active");
  $("#accordion div:not(:first)").hide();
  $("#accordion h3").click(function(){
    $(this).next("div").slideToggle("slow")
    .siblings("div:visible").slideUp("slow");
    $(this).toggleClass("active");
    $(this).siblings("h3").removeClass("active");
  });
});
</script>
<!--Cambio de Iframes INICIO-->
<Script language="JavaScript">
	function CambioValor(parametro,valor){
    	parent.MyIFrame.location.href=parametro;
	}
</Script>
<!--Cambio de Iframes FIN-->
<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$('.contenido_columna1').niceScroll({
			cursorcolor:"#dbe0e6",
			cursorwidth:"8px",
			cursorborder:"0px"
		});
	});
	$(document).ready(function(){
		$('.buscar3').niceScroll({
			cursorcolor:"#dbe0e6",
			cursorwidth:"8px",
			cursorborder:"0px"
		});
	});
</script>
<!-- <script language="javascript">
<!-- Se abre el comentario para ocultar el script de navegadores antiguos

function muestraReloj()
{
// Compruebo si se puede ejecutar el script en el navegador del usuario
if (!document.layers && !document.all && !document.getElementById) return;
// Obtengo la hora actual y la divido en sus partes
var fechacompleta = new Date();
var horas = fechacompleta.getHours();
var minutos = fechacompleta.getMinutes();
var segundos = fechacompleta.getSeconds();
var mt = "AM";
// Pongo el formato 12 horas
if (horas >= 12) {
mt = "PM";
horas = horas - 12;
}
if (horas == 0) horas = 12;
// Pongo minutos y segundos con dos dígitos
if (minutos <= 9) minutos = "0" + minutos;
if (segundos <= 9) segundos = "0" + segundos;
// En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
 cadenareloj = "<label>" + horas + ":" + minutos + ":" + segundos + " " + mt + "</label>";
 // Escribo el reloj de una manera u otra, según el navegador del usuario


if (document.layers) {
document.layers.spanreloj.document.write(cadenareloj);
document.layers.spanreloj.document.close();
}
else if (document.all) spanreloj.innerHTML = cadenareloj;
else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
// Ejecuto la función con un intervalo de un segundo
setTimeout("muestraReloj()", 1000);
}
// Fin del script
</script> -->

</head>
<body onload="javascript:document.session.user.focus();">
<div id="wrapp">
	<div id="header">
        <div class="division">
        	<div class="logo_principal2">
            	<a href="#" onclick="javascript:CambioValor('php/sia.php', 'columna2')">
                    <img src="img/logo.png" />
                </a>
            </div>
            <div class="logo2">
            	<img src="img/logo2.png" />
            </div>
        </div>
        <style>
			.tds{
				color:#FFF;
				font-weight:bold;
				font-size:12px;
				padding:5px 10px;
				text-align:right;
			}
			.login{
				width:400px;
				/*height:110px; */
				position:absolute;
				margin-top:40px;
				margin-left:620px;
				background-color:transparent;
			}
		</style>
        <div class="login">
        	<div class="tds" style="text-align:right; padding-bottom:5px;">
                <div id="spanreloj" style="float:right; font-size:16px"></div>
                <div style="float:right; font-size:16px; padding-right:20px;">
					<?php
                        include('php/fecha.php');
					?>

                </div>
            </div>
         </div><div class="login" style="margin-top:40px;">
            <?php
				if(!empty($_SESSION['id_usuario'])){
					$usuario=mysql_query('SELECT * FROM empleados, usuarios
											WHERE empleados.id_empleado = usuarios.id_empleado
											AND id_usuario='.$_SESSION['id_usuario'])
											or die(mysql_error());
					$row_usuario=mysql_fetch_array($usuario);
					$nombre= $row_usuario['nombre']." ".$row_usuario['paterno']." ".$row_usuario['materno'];
					$id_departamento=$row_usuario['id_departamento'];
					$sucursal=mysql_query('SELECT nombre as sucursal, ciudad FROM ciudades,sucursales, areas_departamentos
											WHERE areas_departamentos.id_sucursal=sucursales.id_sucursal
											AND sucursales.id_ciudad=ciudades.id_ciudad
											AND id_departamento='.$id_departamento)
											or die(mysql_error());
					$row_sucursales=mysql_fetch_array($sucursal);
					$sucursal=$row_sucursales['sucursal'];
					$ciudad=ucwords(strtolower($row_sucursales['ciudad']));
			?>
            <div class="tds" style="text-align:right; padding-bottom:5px; margin-top:30px">
            	<span style="font-weight:normal !important; font-size:10px;">
                	<b>Bienvenido(a): <?php echo $nombre; ?></b>
                </span><br />
                <?php echo "Sucursal ".$ciudad." ".$sucursal; ?>
            </div>
            <?php
				}
			?>
        </div>
        <div class="division2">
        </div>
    </div><!--Fin de header-->
	<div id="cuerpo">
    	<div id="columna1">
        	<div class="contenido_columna1">
			<!--Menu Laterak Inicio-->
            <?php
				if(!empty($_SESSION['id_usuario'])){
			?>
            <div id="accordion">
            <h3><span></span>Favoritos</h3>
            <div>

            </div>
            <h3 onclick="javascript:CambioValor('php/lista_inventario.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/lista_inventario.php', 'columna2')">
                    Inventarios
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_base_producto.php', 'columna2')"
                         title="Agregar base producto">
                          	Nuevo Producto
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/servicios_medicos.php', 'columna2')"
                         title="Servicios Medicos">
                          	Servicios Medicos
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/servicios_laboratorio.php', 'columna2')"
                         title="Servicios Laboratorio">
                          	Servicios Laboratorio
                        </a>
                    </li>
                  	<li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_marcas.php', 'columna2')"
                         title="Marcas">
                          	Agregar Marcas
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_tipo_aparato.php', 'columna2')"
                         title="Estilos de Aparato">
                          	Estilos de Aparato
                        </a>
                    </li>
                </ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/agregar_proveedor.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/agregar_proveedor.php', 'columna2')">
                    Proveedores
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_proveedores_orden.php', 'columna2')"
                        title="Ordenes de Compra">
                            Ordenes de Compra
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_proveedores_cuentas.php', 'columna2')"
                         title="Cuentas por Pagar">
                           Cuentas por Pagar
                        </a>
                    </li>
                    <!--<li>
                        <a href="#" onclick="javascript:CambioValor('lista_proveedores_facturas.php', 'columna2')"
                        title="Facturas">
                            Facturas
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="javascript:CambioValor('lista_proveedores_envios.php', 'columna2')"
                        title="Envios">
                            Envios
                        </a>
                    </li>-->
                </ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/agregar_pacientes.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/agregar_pacientes.php', 'columna2')">
                    Pacientes
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                   <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_pacientes_estudios.php', 'columna2')"
                        title="Estudios">
                            Estudios
                        </a>
                    </li>
                   <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_pacientes_relacion_aparatos.php', 'columna2')"
                        title="Relacion de Aparatos">
                            Relacion de Aparatos
                        </a>
                    </li>
                     <!--<li>
                        <a href="#"
                        onclick="javascript:CambioValor('lista_pacientes_recibo_conformidad.php', 'columna2')"
                        title="Recibos de Conformidad">
                            Recibos de conformidad
                        </a>
                    </li>-->
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_pacientes_adaptaciones.php', 'columna2')"
                        title="Adaptaciones">
                            Adaptaciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_instituciones.php', 'columna2')"
                        title="Estudios">
                            Instituciones
                        </a>
                    </li>
                </ul>
            </div>
            <?php if($_SESSION['id_usuario'] == 1 || $_SESSION['id_usuario'] == 2){ ?>
            <h3 onclick="javascript:CambioValor('php/agregar_personal.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/agregar_personal.php', 'columna2')">
                    Empleados
                </a>
            </h3>
            <div>
				<ul style="margin-left:15px;">
                   <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/usuarios.php', 'columna2')"
                        title="Estudios">
                            Usuarios
                        </a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_accesos.php', 'columna2')" title="Reporte de Accesos">
                            Reporte de Accesos
                       	</a>
                  	</li>
            	</ul>
            </div>
            <?php } ?>
            <h3 onclick="javascript:CambioValor('php/entrada_baterias.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/entrada_baterias.php', 'columna2')">
                    Baterias
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/vale_baterias.php', 'columna2')"
                        title="Vales de baterias">
                            Vales de Batería
                       	</a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_vale_baterias.php','columna2')"
                        title="Reimpresi&oacute;n de Vale de Bater&iacute;as">
                            Reimpresi&oacute;n de Vale
                        </a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/consulta_vales_baterias.php', 'columna2')"
                        title="Consulta Vales de baterias">
                            Consulta Vales de Batería
                       	</a>
                    </li>
            	</ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/entrada_refacciones.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/entrada_refacciones.php', 'columna2')">
                    Refacciones
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/vale_refacciones.php', 'columna2')"
                        title="Vale de Refacciones">
                        	Vale de Refacciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_vales_refacciones.php','columna2')"
                        title="Reimpresion de Vale de Refacciones">
                            Reimpresi&oacute;n de Vale
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/consulta_vales_refacciones.php', 'columna2')"
                        title="Consulta Vales de Refacciones">
                        	Consulta de Vales de Refacciones
                        </a>
                    </li>
                </ul>
            </div>
           	<h3 onclick="javascript:CambioValor('php/sia.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/sia.php', 'columna2')">
                    Laboratorio
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                  	<li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_reparaciones.php', 'columna2')"
                        title="Reparaciones">
                            Reparaciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_reparaciones_entregadas.php', 'columna2')"
                        title="Reparaciones Entregadas">
                            Reparaciones Entregadas
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_notas_reparaciones.php','columna2')"
                        title="Reimpresi&oacute;n Nota de Reparaci&oacute;n">
                            Reimpresi&oacute;n Nota de Reparaci&oacute;n
                        </a>
                    </li>
                   	<li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_moldes.php', 'columna2')"
                        title="Moldes">
                            Moldes
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_moldes_entregados.php', 'columna2')"
                        title="Moldes Entregados">
                            Moldes Entregados
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_notas_de_moldes.php','columna2')"
                        title="Reimpresi&oacute;n de Nota de Moldes">
                            Reimpresi&oacute;n Nota de Molde
                        </a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/catalogo.php', 'columna2')" title="Catalogo">
                    		Catalogo Moldes
               			</a>
                    </li>
                    <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/impresion_hecha_medida.php', 'columna2')"
                        title="Nuevo Molde">
                            Nuevo Molde
                       	</a>
                    </li>
                    <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_nuevo_material.php', 'columna2')"
                        title="Agregar Nuevo Material">
                            Nuevo Material
                       	</a>
                    </li>
                     <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_nueva_ventilacion.php', 'columna2')"
                        title="Agregar Nueva Ventilación">
                            Nueva Ventilacion
                       	</a>
                    </li>
                    <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_nuevos_colores.php', 'columna2')"
                        title="Agregar Nuevos Colores">
                            Nuevo color
                       	</a>
                    </li>
                     <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_nuevas_salidas.php', 'columna2')"
                        title="Agregar Nuevas Salidas">
                            Nueva salida
                       	</a>
                    </li>
                  	<li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_nuevo_estilo.php', 'columna2')"
                        title="Agregar Nuevo Estilo">
                            Agregar Nuevo Estilo
                       	</a>
                    </li>
                    <li>
                       <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/crear_nuevo_molde_catalogo.php', 'columna2')"
                        title="Crear Nuevo Catalogo de Molde">
                            Crear Nuevo Catalogo de Molde
                       	</a>
                    </li>
                </ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/sia.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/sia.php', 'columna2')">
                    Modificaciones
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_modificacion_reparaciones.php', 'columna2')"
                        title="Notas de Reparaciones">
                            Notas de Reparaciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_modificacion_moldes.php', 'columna2')"
                        title="Notas de Moldes">
                            Notas de Moldes
                        </a>
                    </li>
                </ul>
            </div>
          	<h3 onclick="javascript:CambioValor('php/lista_sucursales.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/lista_sucursales.php', 'columna2')">
                    Sucursales
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
					<li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_almacenes.php', 'columna2')"
                        title="Lista almacenes">
                            Almacenes
                       	</a>
                  	</li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_departamentos.php', 'columna2')"
                        title="Lista departamentos">
                            Departamentos
                       	</a>
                  	</li>
                </ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/nueva_venta2.php', 'columna2')"><span></span>
               	<a href="#" onclick="javascript:CambioValor('php/nueva_venta2.php', 'columna2')">
                    Ventas
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_tickets_ventas.php','columna2')"
                        title="Reimpresi&oacute;n de Ticket de Ventas">
                            Reimpresi&oacute;n Ticket de Ventas
                        </a>
                    </li>
					<li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/corte_ventas_2.php', 'columna2')"
                        title="Corte Ventas">
                            Corte Diario
                       	</a>
                  	</li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reimpresion_cortes_de_venta.php','columna2')"
                        title="Reimpresi&oacute;n Ticket Corte de Ventas">
                            Reimpresion Ticket Corte de Ventas
                        </a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/agregar_doctor_consultas.php', 'columna2')"
                        title="Agregar Doctor y Comisiones">
                            Agregar Doctor
                       	</a>
                  	</li>
                </ul>
            </div>
            <h3 onclick="javascript:CambioValor('php/sia.php', 'columna2')"><span></span>
                <a href="#" onclick="javascript:CambioValor('php/sia.php', 'columna2')">
                    Reportes
                </a>
            </h3>
            <div>
                <ul style="margin-left:15px;">
                    <li>
                   		<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_ticket_venta_diaria.php', 'columna2')"
                        title="Reporte Ticket Venta Diaria">
                            Reportes de Corte de Ventas Diarias
                	  	</a>
                    </li>
                    <li>
                   		<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_ventas_realizadas.php', 'columna2')"
                        title="Reporte de Ventas">
                            Reporte de Ventas
                	  	</a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/pago_consultas_2.php', 'columna2')"
                        title="Pago de Consultas">
                            Pago de Consultas
                       	</a>
                  	</li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_entrada_almacen_general.php', 'columna2')"
                        title="Reporte de Entradas">
                            Reporte de Entradas
                       	</a>
                  	</li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_de_faltantes.php', 'columna2')"
                        title="Reporte de Faltantes">
                            Reporte de Faltantes
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_inventario_refacciones.php', 'columna2')"
                        title="Inventario de Refacciones">
                            Inventario Refacciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_inventario_refacciones2.php', 'columna2')"
                        title="Inventario de Refacciones">
                            Inventario Refacciones Rango de Fechas
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_refacciones_salen.php', 'columna2')"
                        title="Reporte de Refacciones que Salen">
                            Refacciones que Salen
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_baterias_salen.php', 'columna2')"
                        title="Reporte de Bater&iacute;as que Salen">
                            Bater&iacute;as que Salen
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_inventario_baterias.php', 'columna2')"
                        title="Inventario de Baterias">
                            Inventario Bater&iacute;as
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_inventario_baterias2.php', 'columna2')"
                        title="Inventario de Baterias">
                            Inventario Bater&iacute;as Rango de Fechas
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_control_reparaciones.php', 'columna2')"
                        title="Lista de Control de Reparaciones">
                            Lista de Control de Reparaciones
                        </a>
                    </li>
                    <li>
                        <a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/lista_control_moldes.php', 'columna2')"
                        title="Lista de Control de Moldes">
                            Lista de Control de Moldes
                        </a>
                    </li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_reparaciones.php', 'columna2')" title="Reporte de Entradas">
                            Reporte de Reparaciones
                       	</a>
                  	</li>
                    <li>
                    	<a style="font-size:12px;" href="#" onclick="javascript:CambioValor('php/reporte_moldes.php', 'columna2')" title="Reporte de Entradas">
                            Reporte de Moldes
                       	</a>
                  	</li>
                </ul>
            </div>
            </div>
            <?php
				}
			?>
            </div><!-- Fin del contenido columna 1 -->
            <div class="buscar3">
            <?php
				if(empty($_SESSION['id_usuario'])){
			?>
            <form name="session" action="index.php" method="post">
                <label class="textos" style="color:#FFF">Usuario: </label><br />
                <input type="text" name="user" maxlength="25" size="20" /><br />
                <label class="textos" style="color:#FFF">Contraseña: </label><br />
                <input type="password" name="pass" maxlength="25" size="20" /><br />
                <input type="submit" name="login" value="Entrar" class="fondo_boton" style="margin-top:5px;" />
            </form>
            <?php
				}
			?>
            </div><!-- Fin de la clase buscar -->
        </div><!-- Fin de la columna 1 -->

        <div id="columna2">
            <iframe name="MyIFrame" src="php/sia.php" class="iNoticias" scrolling="no" marginwidth="0" marginheight="0"
            width="808pxpx" height="605px" style="margin-top:17px;">
            </iframe>
        </div><!-- Fin de la columna 2 -->
    </div><!--Fin de cuerpo-->
</div><!--Fin de wrapp-->
</body>
</html>