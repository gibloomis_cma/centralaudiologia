// JavaScript Document
$(document).ready(function(){
	$('#estado').change(function(){
		$.post('estados_ciudades.php',{id_estado:$(this).val()},function(data){
			$('#ciudad option').not(':first').remove();
			$('#ciudad').append(data);
		});
	}),
	$('#tipo').change(function(){
		$.post('descripcion.php',{variable:$(this).val()},function(data){
			$('#desc option').not(':first').remove();
			$('#desc').append(data);
			$('#desc').removeAttr('disabled'); 			
		})
	}),
	$.post('estados.php', {variable: 1}, function(data){
		$('#id_estado option').not(':first').remove();
		$('#id_estado').append(data);
		$('#id_estado').removeAttr('disabled');
	}),
	$('#pais').change(function(){
		$.post('estados.php',{variable:$('#pais').val()},function(data){
			$('#id_estado option').not(':first').remove();
			$('#id_estado').append(data);
			$('#id_estado').removeAttr('disabled');
		})
	}),
	$('#pais').change(function(){
		$.post('estados.php',{variable:$('#pais').val()},function(data){
			$('#id_estado2 option').not(':first').remove();
			$('#id_estado2').append(data);
			$('#id_estado2').removeAttr('disabled');
		})
	}),
	$('#id_empleado').change(function(){
			$('#entrego').val($('#id_empleado option:selected').val());	
	}),
	$('#desc3').change(function(){
			$.post('consulta_costo2.php',{variable:$('#desc3 option:selected').val()},function(data){
			$('#costo4').val(data);
		})	
	}),
	$('#reparacion').change(function(){
		$.post('consulta_costo3.php',{variable:$('#reparacion option:selected').val()},function(data){
			$('#costo5').val(data);
		});
                $.post('nombre_cliente_reparacion.php',{num_reparacion:$(this).val()},function(data){
                    $('#nombre_paciente').val(data);
                })
	}),
	$('#tipo_subcategoria').change(function(){
		$.post('buscar_refacciones.php',{variable:$(this).val()},function(data){
			$('#id_articulo option').not(':first').remove();
			$('#id_articulo').append(data);
			$('#id_articulo').removeAttr('disabled'); 			
		})	
	}),
	$('#id_articulo').change(function(){
		$.post('consulta_cantidad2.php',{variable:$(this).val(),variable2:$('#id_origen').val()},function(data){
			$('#cantidad option').not(':first').remove();
			$('#cantidad').append(data);
			$('#cantidad').removeAttr('disabled');
		})	
	}),
	$('#desc2').change(function(){
			$.post('consulta_num_series.php',{variable:$(this).val()},function(data){
			$('#num_serie option').not(':first').remove();
			$('#num_serie').append(data);
			$('#num_serie').removeAttr('disabled'); 
		})	
	}),
	$('#id_estado').change(function(){
		$.post('municipios.php',{variable:$(this).val()},function(data){
			$('#id_municipio option').not(':first').remove();
			$('#id_municipio').append(data);
			$('#id_municipio').removeAttr('disabled'); 			
		})	
	}),
	$('#id_estado2').change(function(){
		$.post('municipios.php',{variable:$(this).val()},function(data){
			$('#id_municipio option').not(':first').remove();
			$('#id_municipio').append(data);
			$('#id_municipio').removeAttr('disabled'); 			
		})	
	}),
	$('#subcategoria').change(function(){
			$.post('consulta_doctor.php',{variable:$('#subcategoria option:selected').val()},function(data){
			$('#doctor').val(data);
		})	
	}),
	$('#id_proveedor').change(function(){
		$.post('buscar_contactos.php',{variable:$(this).val()},function(data){
			$('#id_contacto option').not(':first').remove();
			$('#id_contacto').append(data);
			$('#id_contacto').removeAttr('disabled'); 			
		})	
	}),
	$('#id_institucion').change(function(){
		$.post('buscar_contactos_instituciones.php',{variable:$(this).val()},function(data){
			$('#id_contacto option').not(':first').remove();
			$('#id_contacto').append(data);
			$('#id_contacto').removeAttr('disabled'); 			
		})	
	}),
	$('#categoria').change(function(){
		$.post('consulta_subcategorias.php',{variable:$(this).val()},function(data){
			$('#subcategoria option').not(':first').remove();
			$('#subcategoria').append(data);
			$('#subcategoria').removeAttr('disabled'); 			
		})	
	}),
	$('#categoria1').change(function(){
		$.post('consulta_subcategorias.php',{variable:$(this).val()},function(data){
			$('#subcategoria1 option').not(':first').remove();
			$('#subcategoria1').append(data);
			$('#subcategoria1').removeAttr('disabled'); 			
		})	
	}),
	$('#protocolo_de_pase').change(function(){
		$.post('hacer_tabla.php',{variable:$(this).val()},function(data){
			$('#tabla_frecuencias').empty();
			$('#tabla_frecuencias').append(data);
		})	
	}),
	$('#protocolo_de_pase2').change(function(){
		$.post('eliminar_frecuencias.php',{variable:$(this).val(),variable2:$('#folio').val()},function(data){
			algo();
		})	
	}),
	function algo(){
		$.post('hacer_tabla.php',{variable:$('#protocolo_de_pase2 option:selected').val()},function(data){
			$('#tabla_frecuencias').empty();
			$('#tabla_frecuencias').append(data);
		})
	},
	$('#desc').change(function(){
		$.post('consulta_cantidad.php',{variable:$(this).val(),variable2:$('#sucursal').val()},function(data){
			$('#cantidad option').not(':first').remove();
			$('#cantidad').append(data);
			$('#cantidad').removeAttr('disabled');
		})	
	}),
	$('#desc').change(function(){
		$.post('consulta_costo.php',{variable:$('#desc option:selected').val()},function(data){
			$('#costo').val(data);
		})	
	}),
	$('#desc1').change(function(){
		$.post('consulta_cantidad4.php',{variable:$(this).val(),variable2:$('#sucursal1').val()},function(data){
			$('#cantidad2 option').not(':first').remove();
			$('#cantidad2').append(data);
			$('#cantidad2').removeAttr('disabled');
		})
	}),
	$('#desc1').change(function(){
		$.post('consulta_costo.php',{variable:$('#desc1 option:selected').val()},function(data){
			$('#costo1').val(data);
		})	
	}),
	$('#tipo_aparato').change(function(){
		$.post('consultar_modelos.php',{variable:$(this).val(),variable2:$('#sucursal').val()},function(data){
			$('#modelos option').not(':first').remove();
			$('#modelos').append(data);
			$('#modelos').removeAttr('disabled'); 
		})	
	}),
	$('#cliente').change(function(){
		$.post('consultar_ultimo_estudio.php',{variable:$(this).val()},function(data){
			$('#estudio').val(data);
			$.post('consultar_elaborador_estudio.php',{variable:data},function(data){
			$('#elaboro_estudio').val(data);
			})
		})	
	}),
	$('#modelos').change(function(){
		$.post('consultar_almacen.php',{variable:$(this).val()},function(data){
			$('#estatus_relacion').empty();
			$('#estatus_relacion').append(data);
		})	
	}),
	$('#editar').click(function(){
		$.post('modificar_datos_paciente.php',{variable:$('#paciente_reparacion').val()},function(data){
			$('#ficha_identificacion').empty();
			$('#ficha_identificacion').append(data);
		})
	}),
	$('.baterias_vale').change(function(){
		$.post('consulta_cantidad3.php',{variable:$(this).val(),variable2:$('#origen').val()},function(data){
			$('#cantidad2 option').not(':first').remove();
			$('#cantidad2').append(data);
			$('#cantidad2').removeAttr('disabled');
		})
	}),
	$('.baterias_vale2').change(function(){
		$.post('consulta_cantidad3.php',{variable:$(this).val(),variable2:$('#origen2').val()},function(data){
			$('#cantidad3 option').not(':first').remove();
			$('#cantidad3').append(data);
			$('#cantidad3').removeAttr('disabled');
		})
	}),
	$('#desc4').change(function(){
		$.post('consulta_cantidad.php',{variable:$(this).val(),variable2:$('#sucursal3').val()},function(data){
			$('#cantidad4 option').not(':first').remove();
			$('#cantidad4').append(data);
			$('#cantidad4').removeAttr('disabled');
		})	
	}),
	$('#desc4').change(function(){
		$.post('consulta_costo.php',{variable:$('#desc4 option:selected').val()},function(data){
			$('#costo3').val(data);
		})	
	}),
	$('#uso').change(function(){
		$.post('seleccionar_almacen.php',{variable:$('#uso option:selected').val(),variable2:$('#id_departamento_empleado').val()},function(data){
			$('.id_origen_bateria').val(data);
		})
	}),
	$.post('seleccionar_almacen.php',{variable:$('#uso1').val(),variable2:$('#id_departamento_empleado').val()},function(data){
		$('.id_origen_bateria2').val(data);
	}),
	$('#id_doctor').change(function(){
		$.post('consulta_subcategorias_faltantes.php',{variable:$('#id_doctor').val()},function(data){
			$('#servicios option').not(':first').remove();
			$('#servicios').append(data);
			$('#servicios').removeAttr('disabled');
		})
	}),
	$('#tipo_subcategoria2').change(function(){
		$.post('buscar_refacciones2.php',{variable:$(this).val(),variable2:$('#sucursal').val()},function(data){
			$('#id_articulo2 option').not(':first').remove();
			$('#id_articulo2').append(data);
			$('#id_articulo2').removeAttr('disabled'); 			
		})	
	}),
	$('#id_articulo2').change(function(){
		$.post('consulta_cantidad.php',{variable:$(this).val(),variable2:$('#sucursal').val()},function(data){
			$('#cantidad option').not(':first').remove();
			$('#cantidad').append(data);
			$('#cantidad').removeAttr('disabled');
		})	
	}),
	$('#id_articulo2').change(function(){
		$.post('consulta_costo.php',{variable:$('#id_articulo2 option:selected').val()},function(data){
			$('#costo').val(data);
		})	
	}),
	$('#desc3').change(function(){
		$.post('seleccionar_doctor_asignado.php',{variable:$('#desc3').val()},function(data){
			$('#doctor_asignado option').not(':first').remove();
			$('#doctor_asignado').append(data);
			$('#doctor_asignado').removeAttr('disabled');	
		})	
	}),
	$('#folio_vale_bateria').change(function(){
		$.post('informacion_vale_bateria.php',{id_vale_bateria:$(this).val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_folio').val(data[1]);
			$('#sucursal').val(data[2]);
			$('#responsable_folio').val(data[3]);
			$('#uso_vale_bateria').val(data[5]);
			$('#origen_vale_bateria').val(data[4]);
			$('#btn_reimprimir_vale').show();
		});
	}),
	$('#folio_vale_refaccion').change(function(){
		$.post('informacion_vale_refaccion.php',{id_vale_refaccion:$(this).val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_folio').val(data[1]);
			$('#sucursal').val(data[2]);
			$('#responsable_folio').val(data[3]);
			$('#uso_vale_refaccion').val(data[5]);
			$('#origen_vale_refaccion').val(data[4]);
			$('#btn_reimprimir_vale_refaccion').show();
		});
	}),
	$('#folio_ticket_venta').change(function(){
		$.post('informacion_ticket_venta.php',{folio_num_venta:$(this).val(),sucursal_venta:$('#sucursal_venta').val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_folio').val(data[1]);
			$('#sucursal').val(data[2]);
			$('#responsable_venta').val(data[3]);
			$('#descuento_venta').val(data[4]);
			$('#total_vendido').val(data[5]);
			$('#id_sucursal').val(data[6]);
			$('#id_empleado').val(data[7]);
			$('#btn_reimprimir_ticket').show();
		});
	}),
	$('#folio_corte_venta').change(function(){
		$.post('informacion_corte_venta.php',{folio_corte:$(this).val(),id_sucursal:$('#sucursal_empleado').val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_corte').val(data[1]);
			$('#sucursal').val(data[2]);
			$('#responsable_corte').val(data[3]);
			$('#total_vendido').val(data[4]);
			$('#total_articulos').val(data[5]);
			$('#fecha_corte_mysql').val(data[6]);
			$('#id_empleado_corte').val(data[7]);
			$('#btn_reimprimir_corte').show();
		});
	});
	$('#folio_nota_reparacion').change(function(){
		$.post('informacion_nota_reparacion.php',{folio_nota_reparacion:$(this).val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_folio').val(data[1]);
			$('#nombre_cliente').val(data[2]);
			$('#modelo').val(data[3]);
			$('#num_serie').val(data[4]);
			$('#estatus_reparacion').val(data[5]);
			$('#btn_reimprimir_reparacion').show();
		});
	});
	$('#folio_nota_molde').change(function(){
		$.post('informacion_nota_molde.php',{folio_nota_molde:$(this).val()},function(data){
			data = data.split("°");
			$('#num_folio').val(data[0]);
			$('#fecha_folio').val(data[1]);
			$('#nombre_cliente').val(data[2]);
			$('#lado_oido').val(data[3]);
			$('#adaptacion_reposicion').val(data[4]);
			$('#precio').val(data[5]);
			$('#btn_reimprimir_molde').show();
		});
	});
	$('#checkbox_no_registrado').click(function(){
		if ( $(this).attr('checked') == 'checked' )
		{
			$('#checkbox_adaptacion').attr('disabled', 'disabled');
			$('#checkbox_reposicion').attr('disabled', 'disabled');
		}
		else
			if ( $(this).attr('checked') != 'checked' )
			{
				$('#checkbox_adaptacion').removeAttr('disabled');
				$('#checkbox_reposicion').removeAttr('disabled');
			}
	});
	$('.activar_nombre').focusout(function(){
		if( $(this).val() != "" )
		{
			$('#checkbox_reposicion').attr('disabled', 'disabled');
		}
	});
	$('#checkbox_reposicion').click(function(){
		if ( $(this).attr('checked') == 'checked' ) 
		{
			$('#checkbox_adaptacion').attr('disabled','disabled');
			$('#checkbox_no_registrado').attr('disabled','disabled');
			$('.activar_nombre').attr('disabled', 'disabled');
			$('#nota_reposicion').removeAttr('disabled');
		}
		else
		{
			$('#checkbox_adaptacion').removeAttr('disabled');
			$('#checkbox_no_registrado').removeAttr('disabled');
			$('.activar_nombre').removeAttr('disabled');
			$('#nota_reposicion').attr('disabled','disabled');
		}
	});
	$('#nombre_paciente').focusout(function(){
		if ( $(this).val() != "" )
		{
			$('#check_reparacion').attr('disabled', 'disabled');
			$('#no_registrado').attr('disabled', 'disabled');
		}
		else
		{
			$('#check_reparacion').removeAttr('disabled');
			$('#no_registrado').removeAttr('disabled');
		}
	});
	$('#check_reparacion').click(function(){
		if ( $(this).attr('checked') == 'checked' )
		{
			$('#nombre_paciente').attr('disabled', 'disabled');
			$('#no_registrado').attr('disabled', 'disabled');
			$('#check_adaptacion').attr('disabled', 'disabled');
			$('#check_venta').attr('disabled', 'disabled');
			$('#datos_aparato').removeAttr('disabled');
		}
		else
		{
			$('#nombre_paciente').removeAttr('disabled');
			$('#no_registrado').removeAttr('disabled');
			$('#check_adaptacion').removeAttr('disabled');
			$('#check_venta').removeAttr('disabled');
			$('#datos_aparato').removeAttr('disabled');
			$('#datos_aparato').attr('disabled','disabled');
		}
	});
	$('#no_registrado').click(function(){
		if ( $(this).attr('checked') == 'checked' )
		{
			$('#nombre_paciente').attr('disabled','disabled');
			$('#check_adaptacion').attr('disabled','disabled');
			$('#check_reparacion').attr('disabled','disabled');
		}
		else
		{
			$('#nombre_paciente').removeAttr('disabled');
			$('#check_adaptacion').removeAttr('disabled');
			$('#check_reparacion').removeAttr('disabled');
		}
	});
	$('#check_venta').click(function(){
		if ( $(this).attr('checked') == 'checked' && $('#nombre_paciente').val() != "" )
		{
			$('#check_adaptacion').attr('disabled','disabled');
		}
		else
		{
			$('#check_adaptacion').removeAttr('disabled');
		}
	});
	$('#check_adaptacion').click(function(){
		if ( $(this).attr('checked') == 'checked' && $('#nombre_paciente').val() != "" )
		{
			$('#check_venta').attr('disabled','disabled');
		}
		else
		{
			$('#check_venta').removeAttr('disabled');
		}
	});
	$('#cantidad').focusout(function(){
		var cantidad
		cantidad = 0;
		var total = $('#total_venta').val();
		cantidad = $('#cantidad').val();
		var cambio = 0;

		if( Number(cantidad) == "" || Number(cantidad) < Number(total) )
		{
			$('#cambio').val('');
			$('#btn_aceptar').attr('disabled','disabled');
		}
		else
		{
			$('#cambio').val('');
			cambio = cantidad - total;
			$('#cambio').val('$' + cambio);
			$('#btn_aceptar').removeAttr('disabled');
		}
	});
	$('#cantidad').keypress(function(){
		var cantidad
		cantidad = 0;
		var total = $('#total_venta').val();
		cantidad = $('#cantidad').val();
		var cambio = 0;

		if( Number(cantidad) == "" || Number(cantidad) < Number(total) )
		{
			$('#cambio').val('');
			$('#btn_aceptar').attr('disabled','disabled');
		}
		else
		{
			$('#cambio').val('');
			cambio = cantidad - total;
			$('#cambio').val('$' + cambio);
			$('#btn_aceptar').removeAttr('disabled');
		}
	});
	$('#precio_total_servicios_laboratorio').val($('input[name="costo_Total"]').val());
	$('input[name="generar_presupuesto"]').click(function(){
		var preciototal = 0;
		var precio_refacciones = $('input[name="costo_Total"]').val();
		$('#modulo').find(":checkbox:checked").each(function(){			
			preciototal += parseFloat($(this).attr("id"));
   		});
		$('#presupuesto_servicios').val(parseFloat(preciototal));
		if($('input[name="costo_Total"]').val() == 0){
			$('#precio_total_servicios_laboratorio').val(parseFloat(preciototal));
		}else{
			$('#precio_total_servicios_laboratorio').val(parseFloat(preciototal) + parseFloat(precio_refacciones));
		}
	}),
	// Validar el Formulario de Reparaciones, cuando el paciente no esta registrado //
	$('.formulario_validacion').submit(function(){
		// Validar el campo nombre //
		if($('input[name="nombre"]').val()==""){
			alert("Escribe el nombre del Paciente");
			return false;	
		}
		// Validar las opciones de Modelos //
		if($('select[name="modelo"]').val() == 0 && $('select[name="marca"]').val() == 0 && $('input[name="marca_escritos"]').val() == ""){
			alert("Selecciona el modelo y escribe el Numero de Serie");
			return false;	
		}
		// Validar campos de opciones //
		if(($('select[name="modelo"]').val() != 0 && $('input[name="num_serie_escritos1"]').val() =="")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() != "")){
			alert("Faltan Datos del Aparato");
			return false;	
		}
		// Validar los checkbox y la descripcion del problema, debe haber un checkbox seleccionado o una descripcion escrita //
		if($('input[name="accion"]').val()!=""){
			var contador = 0;
			$('.posibles_problemas').find(":checkbox:checked").each(function(){
				contador += 1;		
   			})
			if(contador ==0 && $('textarea[name="problema"]').val() == ""){
				alert("Selecciona un posible problema o escribe el problema");
				return false;	
			}
		}
	}),
///// Funcion para habilitar y desabilitar los tipos de opciones ///////
	$('#opcion1').change(function(){
		if($('select[name="modelo"]').val() != 0){
			$('select[name="marca"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').attr('disabled' , 'disabled');
		}
		if($('select[name="modelo"]').val() == 0){
			$('select[name="marca"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('#opcion2').change(function(){
		if($('select[name="marca"]').val() != 0){
			$('select[name="modelo"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').attr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').attr('disabled' , 'disabled');
		}
		if($('select[name="marca"]').val() == 0){
			$('select[name="modelo"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').removeAttr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('#opcion3').change(function(){
		if($('input[name="marca_escritos"]').val() != ""){
			$('select[name="modelo"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').attr('disabled' , 'disabled');
			$('select[name="marca"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').attr('disabled' , 'disabled');
		}
		if($('input[name="marca_escritos"]').val() == ""){
			$('select[name="modelo"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').removeAttr('disabled' , 'disabled');
			$('select[name="marca"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('.formulario_validacion2').submit(function(){
		// Validar el campo nombre //
		if($('input[name="nombre"]').val()==""){
			alert("Escribe el nombre del Paciente");
			return false;	
		}
		// Validar las opciones de Modelos //
		if($('select[name="modelos"]').val() == 0 && $('select[name="modelo"]').val() == 0 && $('select[name="marca"]').val() == 0 && $('input[name="marca_escritos"]').val() == ""){
			alert("Selecciona el modelo y escribe el Numero de Serie");
			return false;	
		}
		// Validar los checkbox y la descripcion del problema, debe haber un checkbox seleccionado o una descripcion escrita //
		if($('input[name="accion"]').val()!=""){
			var contador = 0;
			$('.posibles_problemas2').find(":checkbox:checked").each(function(){
				contador += 1;		
   			})
			if(contador ==0 && $('textarea[name="problema"]').val() == ""){
				alert("Selecciona un posible problema o escribe el problema");
				return false;	
			}
		}
		// Validar campos de opciones //
		if(($('select[name="modelo"]').val() != 0 && $('input[name="num_serie_escritos1"]').val() =="")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() != "" && $('input[name="num_serie_escritos2"]').val() == "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() ==0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('select[name="marca"]').val() !=0 && $('select[name="tipo_aparato"]').val() !=0 && $('input[name="modelos_escritos"]').val() == "" && $('input[name="num_serie_escritos2"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() ==0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() != "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() != "" && $('input[name="num_serie_escritos3"]').val() == "")||
		($('input[name="marca_escritos"]').val() !="" && $('select[name="tipo_aparato2"]').val() !=0 && $('input[name="modelos_escritos2"]').val() == "" && $('input[name="num_serie_escritos3"]').val() != "")){
			alert("Faltan Datos del Aparato");
			return false;	
		}
	}),
	///// Funcion para habilitar y desabilitar los tipos de opciones ///////
	$('.opcion11').change(function(){
		if($('select[name="modelos"]').val() != 0){
			$('select[name="modelo"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').attr('disabled' , 'disabled');
			$('select[name="marca"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').attr('disabled' , 'disabled');
		}
		if($('select[name="modelos"]').val() == 0){
			$('select[name="modelo"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').removeAttr('disabled' , 'disabled');
			$('select[name="marca"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('.opcion22').change(function(){
		if($('select[name="modelo"]').val() != 0){
			$('select[name="marca"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').attr('disabled' , 'disabled');
			$('select[name="modelos"]').attr('disabled' , 'disabled');
		}
		if($('select[name="modelo"]').val() == 0){
			$('select[name="marca"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').removeAttr('disabled' , 'disabled');
			$('select[name="modelos"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('.opcion33').change(function(){
		if($('select[name="marca"]').val() != 0){
			$('select[name="modelo"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').attr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').attr('disabled' , 'disabled');
			$('select[name="modelos"]').attr('disabled' , 'disabled');
		}
		if($('select[name="marca"]').val() == 0){
			$('select[name="modelo"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').removeAttr('disabled' , 'disabled');
			$('input[name="marca_escritos"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato2"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos2"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos3"]').removeAttr('disabled' , 'disabled');
			$('select[name="modelos"]').removeAttr('disabled' , 'disabled');
		}
	}),
	$('.opcion44').change(function(){
		if($('input[name="marca_escritos"]').val() != ""){
			$('select[name="modelo"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').attr('disabled' , 'disabled');
			$('select[name="marca"]').attr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').attr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').attr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').attr('disabled' , 'disabled');
			$('select[name="modelos"]').attr('disabled' , 'disabled');
		}
		if($('input[name="marca_escritos"]').val() == ""){
			$('select[name="modelo"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos1"]').removeAttr('disabled' , 'disabled');
			$('select[name="marca"]').removeAttr('disabled' , 'disabled');
			$('select[name="tipo_aparato"]').removeAttr('disabled' , 'disabled');
			$('input[name="modelos_escritos"]').removeAttr('disabled' , 'disabled');
			$('input[name="num_serie_escritos2"]').removeAttr('disabled' , 'disabled');
			$('select[name="modelos"]').removeAttr('disabled' , 'disabled');
		}
	});
	var submitActor = null;
    var $form = $('.validacion_laboratorio' );
    var $submitActors = $form.find( 'input[type=submit]' );  
	$form.submit( function( event ){
		if ( null === submitActor ){
			 // If no actor is explicitly clicked, the browser will
			 // automatically choose the first in source-order
			 // so we do the same here
			submitActor = $submitActors[0];
		}
		if (submitActor.name == 'agregar_refaccion') {
			if($('select[name="subcategoria"]').val() == 0){
				alert("Selecciona el tipo de Refacción");
				return false;	
			}
			if($('select[name="descripcion"]').val() == 0){
				alert("Selecciona la descripción");
				return false;	
			}
			if($('input[name="cantidad"]').val() == ""){
				alert("Escribe la cantidad");
				return false;	
			}
		};
		if(submitActor.name == 'guardar_presupuesto'){
			//if($('input[name="presupuesto"]').val() == "" || $('input[name="presupuesto"]').val() == 0){
			if($('input[name="presupuesto"]').val() == 0 && $('#check_reparado').val()!='si'){
				alert("No se puede ir el presupuesto en ceros");
				return false;	
			}
			if($('input[name="estimado"]').val() == ""){
				alert("Escribe el tiempo estimado de la Reperación");
				return false;	
			}
			var contador = 0;
			$('#modulo').find(":checkbox:checked").each(function(){
				contador += 1;		
   			})
			if(contador != 0 && $('#presupuesto_servicios').val() == ""){
				alert("Haz clic en Generar Presupuesto");
				return false;	
			}
		};
		if(submitActor.name == 'autorizacion'){
			if($('input[name="quien_autorizo"]').val() == "")
			{
				if($('input[name="autorizo"]').attr('checked') == 'checked')
				{
					alert("Escribe el nombre de quien autorizo el presupuesto");
					$('#txt_quien_autorizo').focus();
					return false;
				}
				else
				{
					alert("Escribe el nombre de quien autorizo el presupuesto");
					$('#txt_quien_autorizo').focus();
					return false;
				}
			}
		};
		if(submitActor.name == 'aceptar'){
			if($('input[name="recibio"]').val() == ""){
				alert("Escribe el nombre de quien esta Recibiendo");
				return false;
			}
		};
    }),
	$submitActors.click( function( event )
	{
	submitActor = this;
	});
	$('.valida_nota_molde').submit(function(){
		if($('input[name="nombre"]').val() == ""){
			alert("Escribe el nombre del paciente");
			$('#nombre').focus();
			return false;
		}
		if($('select[name="estilos"]').val() == 0){
			alert("Selecciona el Estilo del Molde");
			$('#estilos').focus();
			return false;  
		}
		if($('select[name="materiales"]').val() == 0){
			alert("Selecciona el Material para el Estilo");
			$('#materiales').focus();
			return false;  
		}
		if($('select[name="colores"]').val() == 0){
			alert("Selecciona el Color para el Material");
			$('#colores').focus();
			return false;  
		}
		if($('select[name="ventilaciones"]').val() == 0){
			alert("Selecciona la Ventilacion para el Estilo");
			$('#ventilaciones').focus();
			return false;  
		}	  
		if($('select[name="salidas"]').val() == 0){
			alert("Selecciona la Salida para el Estilo");
			$('#salidas').focus();
			return false;  
		}
		if($('select[name="lado_oido"]').val() == 0){
			alert("Selecciona el lado del oido");
			$('#lado_oido').focus();
			return false;  
		}
	});
	$('.valida_nota_molde2').submit(function(){
		if($('input[name="nombre"]').val() == "")
		{
			alert("Escribe el nombre del paciente");
			$('#nombre').focus();
			return false;
		}
		if ( $('select[name="id_municipio"]').val() != 0 && $('input[name="ciudad"]').val() != "" ) 
		{
			alert('Solo se puede registrar una ciudad');
			$('#ciudad').val('');
			$('#id_municipio').focus();
			return false;
		}
		if($('select[name="estilos"]').val() == 0)
		{
			alert("Selecciona el Estilo del Molde");
			$('#estilos').focus();
			return false;  
		}
		if($('select[name="materiales"]').val() == 0)
		{
			alert("Selecciona el Material para el Estilo");
			$('#materiales').focus();
			return false;  
		}
		if($('select[name="colores"]').val() == 0)
		{
			alert("Selecciona el Color para el Material");
			$('#colores').focus();
			return false;  
		}
		if($('select[name="ventilaciones"]').val() == 0)
		{
			alert("Selecciona la Ventilacion para el Estilo");
			$('#ventilaciones').focus();
			return false;  
		}	  
		if($('select[name="salidas"]').val() == 0)
		{
			alert("Selecciona la Salida para el Estilo");
			$('#salidas').focus();
			return false;  
		}
		if($('select[name="lado_oido"]').val() == 0)
		{
			alert("Selecciona el lado del oido");
			$('#lado_oido').focus();
			return false;  
		}
	});
	$('.valida_agregar_producto').submit(function(){
		if($('select[name="id_almacen"]').val() == 0){
			alert("Selecciona el Almacen");
			return false;	
		}
		if($('input[name="cantidad"]').val() == ""){
			alert("Escribe la cantidad");
			return false;
		}
		if($('input[name="num_serie"]').val() == ""){
			alert("Escribe el Numero de Serie");
			return false;	
		}
	});
	$('#molde').change(function(){
		$.post('consulta_costo_molde.php',{variable:$('#molde option:selected').val()},function(data){
			data = data.split('°');
			$('#costo6').val(data[0]);
			$('#estatus').val(data[1]);
			if ( data[1] == "5" || data[1] == "6" || Number(data[2]) > 0 )
			{
				$('#pago_parcial').attr('disabled', 'disabled');
			}
			else
			{
				$('#pago_parcial').removeAttr('disabled');
				$('#pago_parcial').focus();
			}
			
		});
		$.post('nombre_cliente_molde.php',{num_molde:$(this).val()},function(data){
			$('#nombre_cliente_molde').val(data);
		});	
	});
        // FUNCION QUE VALIDA EL FOMULARIO DE REPARACIONES EN VENTAS
        $('#form_reparaciones_venta').submit(function(){
           var reparacion = $('#reparacion').val();
           var recibio = $('#txt_recibio').val();
           
           if( reparacion == 0 && recibio == "" )
               {
                   alert('Debe de seleccionar el número de folio de la reparación y colocar el nombre de quien recibio');
                   $('#reparacion').focus();
                   return false;
               }
           else
               if( reparacion == 0 )
                   {
                       alert('Seleccione el número de folio de la reparación');
                       $('#reparacion').focus();
                       return false;
                   }
           else
               if( recibio == "" )
                   {
                       alert('Ingrese el nombre de quien recibio el aparato para reparación');
                       $('#txt_recibio').focus();
                       return false;
                   }
        });
        // FUNCION QUE VALIDA EL FORMULARIO DE MOLDES EN VENTAS
        $('#form_ventas_moldes').submit(function(){
        	var molde = $('#molde').val();
        	var recibio_moldes = $('#recibio_moldes').val();
        	var costo_molde = $('#costo6').val();
        	var estatus = $('#estatus').val();

        	if( molde == 0 && recibio_moldes == "" )
        	{
        		alert('Ingrese los datos correspondientes para la venta del molde');
        		$('#molde').focus();
        		return false;
        	}
        	else
        		if( molde == 0 )
        		{
        			alert('Seleccione el número de folio del molde');
        			$('#molde').focus();
        			return false;
        		}
        	else
        		if( recibio_moldes == "" && ( estatus == "5" || estatus == "6" ) ) 
        		{
        			alert('Ingrese el nombre de quien recibio el molde');
        			$('#recibio_moldes').focus();
        			return false;
        		}
        	else
        	{
        		if( Number(pago_parcial) > Number(costo_molde) )
        		{
        			alert('La cantidad del pago parcial es incorrecta');
        			$('#pago_parcial').val('');
        			$('#pago_parcial').focus();
        			return false;
        		}
        	}
        });
        $('#observaciones_presupuesto').focus(function(){
			if( $(this).val() == "-Ninguna-" )
			{
				$(this).val("");
				$("#observaciones_presupuesto").css({
					"color" :  "#000",
                	"text-indent" : "5px"
				});
			}
		}),
		$('#observaciones_presupuesto').blur(function(){
			if( $(this).val() == "" )
			{
				$(this).val("-Ninguna-");
				$("#observaciones_presupuesto").css({ 
                	"text-indent" : "5px"
				});
			}
		});
		$('#descripcion_problema').focus(function(){
			if( $(this).val() == "La reparación es para venta" )
			{
				$(this).val("");
				$("#descripcion_problema").css({
					"color" :  "#000",
                	"text-indent" : "5px"
				});
			}
		}),
		$('#descripcion_problema').blur(function(){
			if( $(this).val() == "" )
			{
				$(this).val("La reparación es para venta");
				$("#descripcion_problema").css({ 
                	"text-indent" : "5px"
				});
			}
		});
		// FUNCION QUE VALIDA EL FORMULARIO DE ENTREGA EN MOLDES
		$('#form_detalles_moldes').submit(function(){
			var recibio = $('#recibio').val();

			if ( recibio == "" )
			{
				alert('Ingrese el nombre de la persona que recibe el molde');
				$('#recibio').focus();
				return false;
			}
		});
		// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT DE DETALLES REPARACIONES
		$('#form_detalles_reparaciones').submit(function(){
			var observaciones = $('#observaciones_presupuesto_garantia').val();
			var no_reparado = $('#check_reparado').val();

			if ( $('#check_reparado').attr('checked') == 'checked' && observaciones == "" )
			{
				alert('Debe de ingresar las observaciones correspondientes');
				$('#observaciones_presupuesto_garantia').focus();
				return false;
			}				
		});
		// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT DE DETALLES REPARACIONES
		$('#form_detalles_reparaciones').submit(function(){
			var se_cobra = $('#check_se_cobra').val();
			var observaciones = $('#observaciones_presupuesto_garantia').val();

			if ( $('#check_se_cobra').attr('checked') == 'checked' && observaciones == "" )
			{
				alert('Debe de ingresar las observaciones de la garantia');
				$('#observaciones_presupuesto_garantia').focus();
				return false;
			}				
		});
});