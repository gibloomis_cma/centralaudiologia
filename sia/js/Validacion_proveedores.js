﻿// VALIDACION DEL FORMULARIO AGREGAR PROVEEDORES
$(document).ready(function(){	
	$('#form_agregar_proveedor').validate({
		rules:
		{
			proveedor:
			{
				required:true
			},
			nombre:
			{
				required:true
			},
			calle:
			{
				required:true
			},
			num_int:
			{
				required:true
			},
			colonia:
			{
				required:true
			},
			id_estado:
			{
				required:true
			},
			id_municipio:
			{
				required:true
			},
			rfc:
			{
				required:true
			},
			credito:
			{
				required:true
			},
			plazo_pago:
			{
				required:true
			},
			codigo_postal:
			{
				required:true,
				digits:true
			},
			num_ext:
			{
				required:true,
				digits:true
			}
		},
		messages:
		{
			proveedor:
			{
				required:"Campo Obligatorio"
			},
			nombre:
			{
				required:"Campo Obligatorio"
			},
			calle:
			{
				required:"Campo Obligatorio"	
			},
			num_int:
			{
				required:"Campo Obligatorio"
			},
			colonia:
			{
				required:"Campo Obligatorio"
			},
			id_estado:
			{
				required:"Campo Obligatorio"
			},
			id_municipio:
			{
				required:"Campo Obligatorio"
			},
			rfc:
			{
				required:"Campo Obligatorio"
			},
			credito:
			{
				required:"Campo Obligatorio"
			},
			plazo_pago:
			{
				required:"Campo Obligatorio"
			},
			codigo_postal:
			{
				required:"Campo Obligatorio",
				digits:"Solo se permiten numeros"
			},
			num_ext:
			{
				required:"Campo Obligatorio",
				digits:"Solo se permiten numeros"
			}
		}
	})
})

// VALIDACION DEL FORMULARIO NUEVO PAGO PROVEEDORES
$(document).ready(function(){	
	$('#form_nuevo_pago').validate({
		rules:
		{
			fecha_deposito:
			{
				required:true
			},
			cantidad:
			{
				required:true,
				digits:true
			}
		},
		messages:
		{
			fecha_deposito:
			{
				required:"Campo Obligatorio"
			},
			digits:
			{
				required:"Campo Obligatorio"
				digits:"Solo se aceptan numeroa"
			}
		}
	})
})