﻿// FUNCION CON JQUERY QUE OBTIENE LOS MUNICIPIOS DE ACUERDO AL ESTADO SELECCIONADO
$(document).ready(function(){
	$('#id_estado2').change(function(){
		$.post('municipios.php',{variable:$(this).val()},function(data){
			$('#id_municipio2 option').not(':first').remove();
			$('#id_municipio2').append(data);
			$('#id_municipio2').removeAttr('disabled'); 			
		})	
	}),
	$("#tipo_paciente").change(function(){
		$.post("tipos_instituciones.php",{ variable:$(this).val() },function(data){$("#inst").html(data);})
		$('#inst').removeAttr('disabled');
	});
})
/********************************************************************************/
// FUNCION PARA EL BOTON TERMINAR DEL FORMULARIO AGREGAR NUEVO COLOR A MATERIALES
$(document).ready(function(){
	$('#terminar_agregar_colores').click(function(){
		var nombre_material = document.form_agregar_nuevo_color_a_material.txt_nombre_material.value
		var confirmar = confirm("¿Ha terminado de agregar colores al material " + nombre_material + "?");
		
		if( confirmar == true )
		{
			window.location.href = "agregar_nuevo_color_a_material.php";
		}
		else
		{
		}
	});
});
/********************************************************************************/
// FUNCION QUE MUESTRA LOS MODELOS DE ACUERDO AL PROVEEDOR SELECCIONADO EN EL FORMULARIO MOLDE HECHO A MEDIDA
$(document).ready(function(){
	$('#proveedores').change(function(){
		$.post('modelos_proveedores.php',{id_proveedor:$(this).val()},function(data){
			$('#modelos option').not(':first').remove();
			$('#modelos').append(data);
			$('#modelos').removeAttr('disabled');
		});
	});
});
/*********************************************************************************/
// FUNCION QUE OCULTA EL SELECT HECHO A LA MEDIDA SI LA OPCION SELECCIONADA ES SI EN EL SELECT DE IMPRESION
$(document).ready(function(){
	$('#impresion').change(function(){
		if( $('#impresion').val() == 'no' )
		{
			$('#hecho_medida').attr('disabled',true);
		}
		else
		{
			$('#hecho_medida').removeAttr('disabled');
		}
	}),
	$('#nombre_paciente').focusout(function(){
		$.post('estudios_pacientes.php',{nombre_paciente:$('#nombre_paciente').val()},function(data){
			$('#id_estudio option').not(':first').remove();
			$('#id_estudio').append(data);
			$('#id_estudio').removeAttr('disabled');
		})
	}),
	$('#nombre_paciente').focusout(function(){
		$.post('buscar_id_cliente.php',{nombre_paciente:$('#nombre_paciente').val()},function(data){
			$('#id_paciente_actual').val(data);
		})
	});
}); 
/********************************************************************************/
// FUNCION PARA EL BOTON TERMINAR DEL FORMULARIO AGREGAR COLORES A MATERIAL.PHP
$(document).ready(function(){
	$('#terminar_agregar_color').click(function(){
		var confirmar = confirm("¿Ha terminado de agregar los colores para el material?");
		if( confirmar == true )
		{
			window.location.href = "agregar_nuevo_material.php";
		}
		else
		{
		}
	});
});
/********************************************************************************/
// FUNCION QUE AGREGAR SELECT DE COLORES AL DAR CLICK
$(document).ready(function(){
	$('#agregar_color').click(function(){
		$.post('resultado_agregar_colores.php',{},function(data){
			$('#agregar_nuevo_color').append(data);
		});
	});
});
/********************************************************************************/
// FUNCION QUE AGREGAR SELECT DE MATERIALES AL DAR CLICK
$(document).ready(function(){
	$('#agregar_salida').click(function(){
		$.post('resultado_salidas_ventilaciones.php',{},function(data){
			$('#tabla_salida').append(data);
		});
	});
});
/********************************************************************************/
// FUNCION QUE AGREGAR SELECT DE VENTILACIONES AL DAR CLICK
$(document).ready(function(){
	$('#agregar_ventilacion').click(function(){
		$.post('resultados_ventilacion_material.php',{},function(data){
			$('#tabla_ventilaciones').append(data);
		});
	});
});
/********************************************************************************/
// FUNCION QUE AGREGAR SELECT DE SALIDAS AL DAR CLICK
$(document).ready(function(){
	$('#agregar').click(function(){
		$.post('resultado_materiales_estilo.php',{},function(data){
			$('#tabla_informacion').append(data);
		});
	});
});
/*********************************************************************************/
// FUNCION QUE MUESTRA LAS IMAGENES Y LA INFORMACION CORRESPONDIENTE DEL CATALOGO DE MOLDES
$(document).ready(function(){
	//$('.area_contenido1').niceScroll({
		//cursorcolor:"#0E1E8C",
		//cursorwidth:"8px"
	//}),
	$('#estilos').change(function(){
		$('#estilos_moldes').empty();
		$.post('imagen_estilos.php',{id_catalogo:$(this).val()},function(data){
			$('#imagenes_estilos').not(':first').remove();
			$('#estilos_moldes').append(data);
			$('#estilos_moldes').show('slow');
		}),
		$.post('materiales.php',{id_catalogo:$(this).val()},function(data){
			$('#materiales option').not(':first').remove();
			$('#materiales').append(data);
			$('#materiales').removeAttr('disabled');
		})
	}),
	$('#materiales').change(function(){
		$.post('ventilaciones.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val()},function(data){
			$('#ventilaciones option').not(':first').remove();
			$('#ventilaciones').append(data);
			$('#ventilaciones').removeAttr('disabled');
		}),
		$.post('colores.php',{id_material:$(this).val()},function(data){
			$('#colores option').not(':first').remove();
			$('#colores').append(data);
			$('#colores').removeAttr('disabled');
		})
	}),
	$('#colores').change(function(){
		$('.campo_color').empty();
		$.post('obtener_rgb.php',{id_color:$(this).val()},function(data){
			$('.campo_color').append(data);
		});
	}),
	$('#ventilaciones').change(function(){
		$('#imagen_ventilacion').empty();
		$.post('calibre_ventilaciones.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val(),id_ventilacion:$(this).val()},function(data){
			$('#calibre').val(data);
		}),
		$.post('imagen_ventilaciones.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val(),id_ventilacion:$(this).val()},function(data){
			$('#imagenes_ventilacion').not(':first').remove();
			$('#imagen_ventilacion').append(data);
			$('#imagen_ventilacion').show('slow');
		}),
		$.post('salidas.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val(),id_ventilacion:$(this).val()},function(data){
			$('#salidas option').not(':first').remove();
			$('#salidas').append(data);
			$('#salidas').removeAttr('disabled');
		})
	}),
	$('#salidas').change(function(){
		$('#imagen_salida').empty();
		$.post('imagen_salidas.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val(),id_ventilacion:$('#ventilaciones').val(),id_salida:$(this).val()},function(data){
			$('#imagenes_salida').not(':first').remove();
			$('#imagen_salida').append(data);
			$('#imagen_salida').show('slow');
		})
	});
});
/********************************************************************/
// FUNCION QUE MUESTRA POR DEFAULT LA PALABRA NINGUNA EN EL CAMPO DE OBSERVACIONES EN VALE DE REFACCIONES
$(document).ready(function(){
	$('#observaciones').focus(function(){
		if( $(this).val() == "-Ninguna-" )
		{
			$(this).val("");
			$("#observaciones").css({
				"color" :  "#000",
                "text-indent" : "5px"
			});
		}
	}),
	$('#observaciones').blur(function(){
		if( $(this).val() == "" )
		{
			$(this).val("-Ninguna-");
			$("#observaciones").css({ 
                "text-indent" : "5px"
			});
		}
	});
});
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DE SERVICIOS MEDICOS
$(document).ready(function(){
	$('#form_servicios_medicos').submit(function(){
		var doctor = $('#doctor_asignado').val();
		var cantidad = $('#cantidad_consultas').val();

		if ( doctor == 0 ) 
		{
			alert('Seleccione el doctor correspondiente');
			$('#doctor_asignado').focus();
			return false;
		}
		else
			if ( cantidad == "" ) 
			{
				alert('Ingrese la cantidad correspondiente a la descripción');
				$('#cantidad_consultas').focus();
				return false;
			}
	});
});
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO IMPRESION HECHA MEDIDA
function validaImpresionHechaMedida()
{
 	var impresion = document.impresion_molde.impresion.value
 	var hecho_medida = document.impresion_molde.hecho_medida.value
 	
 	if( impresion == 0 && hecho_medida == 0 )
 	{
 		alert("Seleccione las opciones correspondientes");
 		document.impresion_molde.impresion.focus();
 		return false;
 	}
 	else
 		if( impresion == 0 )
 		{
 			alert("Seleccione la opcion de impresión");
 			document.impresion_molde.impresion.focus();
 			return false;
 		}
 	else
 		if( impresion == "si" && hecho_medida == 0 )
 		{
 			alert("Seleccione la opcion de hecho a la medida");
 			document.impresion_molde.hecho_medida.focus();
 			return false;
 		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO NUEVO MOLDE
function validaNuevoMolde()
{
	var oido = document.nuevo_molde.oido.value
	var atendio = document.nuevo_molde.id_empleado.value
	var especiales = document.nuevo_molde.especiales.value
	
	if( oido == 0 && atendio == 0 && especiales == 0 )
	{
		alert("Seleccione las caracteristicas correspondientes");
		document.nuevo_molde.oido.focus();
		return false;
	}
	else
		if( oido == 0 )
		{
			alert("Seleccione el lado del oido correspondiente");
			document.nuevo_molde.oido.focus();
			return false;
		}
	else
		if( atendio == 0 )
		{
			alert("Seleccione el empleado que atendio");
			document.nuevo_molde.id_empleado.focus();
			return false;
		}
	else
		if( especiales == 0 )
		{
			alert("Escriba algunas caracteristicas de la operacion");
			document.nuevo_molde.especiales.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MOLDE HECHO A MEDIDA
function validaMoldeHechoAMedida()
{
	var proveedores = document.molde_hecho_a_medida.proveedores.value
	var modelos = document.molde_hecho_a_medida.modelos.value
	var especiales = document.molde_hecho_a_medida.especiales.value
	
	if( proveedores == 0 && modelos == 0 && especiales == 0 )
	{
		alert("Seleccione la información correspondiente");
		document.molde_hecho_a_medida.proveedores.focus();
		return false;
	}
	else
		if( proveedores == 0 )
		{
			alert("Seleccione el proveedor");
			document.molde_hecho_a_medida.proveedores.focus();
			return false;
		}
	else
		if( modelos == 0 ) 
		{
			alert("Seleccione el modelo correspondiente");
			document.molde_hecho_a_medida.modelos.focus();
			return false;
		}
	else
		if( especiales == 0 )
		{
			alert("Escriba alguna descripcion de la operación");
			document.molde_hecho_a_medida.especiales.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DE MOLDES (NUEVO MOLDE)
function validarNuevoMolde()
{
	// VARIABLES DEL FORMULARIO
	var estilo = document.form_nuevo_molde.estilos.value
	var material = document.form_nuevo_molde.materiales.value
	var color = document.form_nuevo_molde.colores.value
	var ventilacion = document.form_nuevo_molde.ventilaciones.value
	var salida = document.form_nuevo_molde.salidas.value
	
	if( estilo == 0 && material == 0 && color == 0 && ventilacion == 0 && salida == 0 )
	{
		alert("Seleccione las caracteristicas del molde");
		document.form_nuevo_molde.estilos.focus();
		return false;
	}
	else
		if( estilo == 0 )
		{
			alert("Seleccione el estilo del molde");
			document.form_nuevo_molde.estilos.focus();
			return false;
		}
	else
		if( material == 0 )
		{
			alert("Seleccione el material del molde");
			document.form_nuevo_molde.materiales.focus();
			return false;
		}
	else
		if( color == 0 )
		{
			alert("Seleccione el color del molde");
			document.form_nuevo_molde.colores.focus();
			return false;
		}
	else
		if( ventilacion == 0 )
		{
			alert("Seleccione la ventilación del molde");
			document.form_nuevo_molde.ventilaciones.focus();
			return false;
		}
	else
		if( salida == 0 )
		{
			alert("Seleccione el tipo de salida del molde");
			document.form_nuevo_molde.salidas.focus();
			return false;
		}
}
/*********************************************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT VALE DE REFACCIONES.PHP
/*
function validaValeRefacciones()
{
	var responsable = document.forma1.responsable.value
	var id_uso = document.forma1.id_uso.value
	var id_origen = document.forma1.id_origen.value
	var observaciones = document.forma1.observaciones.value
	var subcategoria = document.forma1.subcategoria.value
	var descripcion = document.forma1.descripcion.value
	var cantidad = document.forma1.cantidad.value
	
	if( responsable == 0 && id_uso == 0 && id_origen == 0 && observaciones.length == 0 && subcategoria == 0 && descripcion == 0 && cantidad == 0 )
	{
		alert("Seleccione la informacion correspondiente para generar el vale");
		document.forma1.responsable.focus();
		return false;
	}
	else
		if( responsable == 0 )
		{
			alert("Seleccione el responsable");
			document.forma1.responsable.focus();
			return false;
		}
	else
		if( id_uso == 0 )
		{
			alert("Seleccione el uso");
			document.forma1.id_uso.focus();
			return false;
		}
	else
		if( id_origen == 0 )
		{
			alert("Seleccione el origen");
			document.forma1.id_origen.focus();
			return false;
		}
	else
		if( observaciones.length == 0 )
		{
			alert("Ingrese algunas observaciones para el vale de refacciones");
			document.forma1.observaciones.focus();
			return false;
		}
	else
		if( subcategoria == 0 )
		{
			alert("Seleccione alguna subcategoria");
			document.forma1.subcategoria.focus();
			return false;
		}
	else
		if( descripcion == 0 )
		{
			alert("Seleccione la descripción");
			document.forma1.descripcion.focus();
			return false;
		}
	else
		if( cantidad == 0 )
		{
			alert("Seleccione la cantidad");
			document.forma1.cantidad.focus();
			return false;
		}
}*/
/*********************************************************************************************/
$(document).ready(function(){
	$('.btn_agregar').click(function(){
		var boton = "";
		boton = $('.btn_agregar').val();

		var responsable = $('#responsable').val();
		var id_uso = $('#id_uso').val();
		var origen = $('#id_origen').val();
		var subcategoria = $('#tipo_subcategoria').val();
		var descripcion = $('#id_articulo').val();
		var cantidad = $('#cantidad').val();

		if( origen == 0 && subcategoria == 0 && descripcion == 0 && cantidad == 0 && responsable == 0 && id_uso == 0 )
		{
			alert('Seleccione la información del producto a agregar en el vale');
			$('#origen').focus();
			return false;
		}
		else
			if ( responsable == 0 )
			{
				alert('Seleccione el responsable del vale');
				$('#responsable').focus();
				return false;
			}
		else
			if ( id_uso == 0 )
			{
				alert('Seleccione el uso del vale de refacciones');
				$('#id_uso').focus();
				return false;
			}
		else
			if ( origen == 0 )
			{
				alert('Seleccione el origen de la refacción');
				$('#id_origen').focus();
				return false;
			}
		else
			if ( subcategoria == 0 )
			{
				alert('Seleccione el tipo de refaccion');
				$('#tipo_subcategoria').focus();
				return false;
			}
		else
			if ( descripcion == 0 )
			{
				alert('Seleccione la refaccion para agregarla al vale de refaccion');
				$('#id_articulo').focus();
				return false;
			}
		else
			if ( cantidad == 0 )
			{
				alert('Seleccione la cantidad correspondiente');
				$('#cantidad').focus();
				return false;
			}
	});
});
/*********************************************************************************************/
$(document).ready(function(){
	$('.btn_agregar').click(function(){

		var responsable = $('#responsable').val();
		var uso = $('#uso').val();
		var origen = $('#origen').val();
		var desc1 = $('#desc1').val();
		var cantidad2 = $('#cantidad2').val();
		var origen2 = $('#origen2').val();
		var cantidad3 = $('#cantidad3').val();

		if( responsable == 0 && uso == 0 && origen == 0 && desc1 == 0 && cantidad2 == 0 )
		{
			alert('Seleccione la información del producto a agregar en el vale');
			$('#responsable').focus();
			return false;
		}
		else
			if(responsable == 0 && uso == 0 && origen == 0 && desc1 == 0 && cantidad2 == 0 && origen2 == 0 && cantidad3 == 0 )
			{
				alert('Seleccione la información del producto a agregar en el vale');
				$('#responsable').focus();
				return false;		
			}
		else
			if ( responsable == 0 )
			{
				alert('Seleccione el responsable del vale de baterias');
				$('#responsable').focus();
				return false;
			}
		else
			if ( uso == 0 )
			{
				alert('Seleccione el uso del vale');
				$('#uso').focus();
				return false;
			}
		else
			if ( origen == 0 )
			{
				alert('Seleccione el origen');
				$('#origen').focus();
				return false;
			}
		else
			if ( origen2 == 0 )
			{
				alert('Seleccione el origen');
				$('#origen2').focus();
				return false;
			}
		else
			if ( desc1 == 0 )
			{
				alert('Seleccione el tipo de bateria');
				$('#desc1').focus();
				return false;
			}
		else
			if ( cantidad2 == 0 )
			{
				alert('Seleccione la cantidad correspondiente');
				$('#cantidad2').focus();
				return false;
			}
		else
			if ( cantidad3 == 0 )
			{
				alert('Seleccione la cantidad correspondiente');
				$('#cantidad3').focus();
				return false;
			}
	});
	return false;
});
/********************************************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT VALE DE BATERAS.PHP
/*
function validaValeBaterias()
{
	var uso = document.forma1.uso.value
	var responsable = document.forma1.responsable.value
	var desc1 = document.forma1.desc1.value
	var cantidad2 = document.forma1.cantidad2.value
	
	if( uso == 0 && responsable == 0 && desc1 == 0 && cantidad2 == 0 )
	{
		alert("Seleccione las opciones correspondientes para generar el vale");
		document.forma1.uso.focus();
		return false;
	}
	else
		if( uso == 0 )
		{	
			alert("Seleccione el uso");
			document.forma1.uso.focus();
			return false;
		}
	else
		if( responsable == 0 )
		{
			alert("Seleccione el responsable");
			document.forma1.responsable.focus();
			return false;
		}
	else
		if( desc1 == 0 )
		{
			alert("Seleccione la descripcion");
			document.forma1.desc1.focus();
			return false;
		}
	else
		if( cantidad2 == 0 )
		{
			alert("Seleccione la cantidad");
			document.forma1.cantidad2.focus();
			return false;
		}	
}*/
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT AGREGAR BASE PRODUCTO
function validaAgregarBaseProductoModelos()
{
	var precio_venta = document.form3.precio_venta.value
	var precio_compra =	document.form3.precio_compra.value
	var comision = document.form3.comision.value
	
	if( precio_venta == "" && precio_compra == "" && comision == "" )
	{
		alert("Ingrese la comision del modelo");
		document.form3.comision.focus();
		return false;
	}
	else
		if( precio_venta == "" )
		{
			alert("Ingrese el precio de venta del producto");
			document.form3.precio_venta.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_venta) )
		{
			alert("Ingrese un precio de venta correcto");
			document.form3.precio_venta.value = "";
			document.form3.precio_venta.focus();
			return false;
		}
	else
		if( precio_compra == "" )
		{
			alert("Ingrese el precio de compra");
			document.form3.precio_compra.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_compra) )
		{
			alert("Ingrese un precio de compra correcto");
			document.form3.precio_compra.value = "";
			document.form3.precio_compra.focus();
			return false;
		}
	else
		if( comision == "" )
		{
			alert("Ingrese la comisión del modelo");
			document.form3.comision.value = "";
			document.form3.comision.focus();
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(comision) )
		{
			alert("Ingrese una comision correcta");
			document.form3.comision.value = "";
			document.form3.comision.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT AGREGAR BASE PRODUCTO
function validaAgregarBaseProductoBaterias()
{
	var precio_venta = document.form2.precio_venta.value
	var precio_compra =	document.form2.precio_compra.value
	
	if( precio_venta == "" && precio_compra == "" )
	{
		alert("Ingrese el precio de venta del producto");
		document.form2.precio_venta.focus();
		return false;
	}
	else
		if( precio_venta == "" )
		{
			alert("Ingrese el precio de venta del producto");
			document.form2.precio_venta.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_venta) )
		{
			alert("Ingrese un precio de venta correcto");
			document.form2.precio_venta.value = "";
			document.form2.precio_venta.focus();
			return false;
		}
	else
		if( precio_compra == "" )
		{
			alert("Ingrese el precio de compra");
			document.form2.precio_compra.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_compra) )
		{
			alert("Ingrese un precio de compra correcto");
			document.form2.precio_compra.value = "";
			document.form2.precio_compra.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DEL SCRIPT AGREGAR BASE PRODUCTO
function validaAgregarBaseProducto()
{
	var precio_venta = document.form1.precio_venta.value
	var precio_compra =	document.form1.precio_compra.value
	
	if( precio_venta == "" && precio_compra == "" )
	{
		alert("Ingrese el precio de venta del producto");
		document.form1.precio_venta.focus();
		return false;
	}
	else
		if( precio_venta == "" )
		{
			alert("Ingrese el precio de venta del producto");
			document.form1.precio_venta.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_venta) )
		{
			alert("Ingrese un precio de venta correcto");
			document.form1.precio_venta.value = "";
			document.form1.precio_venta.focus();
			return false;
		}
	else
		if( precio_compra == "" )
		{
			alert("Ingrese el precio de compra");
			document.form1.precio_compra.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(precio_compra) )
		{
			alert("Ingrese un precio de compra correcto");
			document.form1.precio_compra.value = "";
			document.form1.precio_compra.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR NUEVA VENTILACION
function validaAgregarNuevaVentilacion()
{
	var nombre_ventilacion = document.form_agregar_nueva_ventilacion.txt_nombre_ventilacion.value
	var calibre = document.form_agregar_nueva_ventilacion.txt_calibre_ventilacion.value
	var precio = document.form_agregar_nueva_ventilacion.precio.value
	var imagen_ventilacion = document.form_agregar_nueva_ventilacion.imagen_vent.value
	var extensiones_permitidas = new Array(".gif", ".jpg", ".png", ".jpeg");
	var extension = (imagen_ventilacion.substring(imagen_ventilacion.lastIndexOf("."))).toLowerCase();
	var permitida = false;
	
	if( nombre_ventilacion == "" && calibre == "" && imagen_ventilacion == ""  && precio == "" )
	{
		alert("Ingrese los datos correspondientes, para poder registrar la ventilación");
		document.form_agregar_nueva_ventilacion.txt_nombre_ventilacion.focus();
		return false;
	}
	else
		if( nombre_ventilacion == "" )
		{
			alert("Ingrese el nombre de la ventilación");
			document.form_agregar_nueva_ventilacion.txt_nombre_ventilacion.focus();
			return false;
		}
	else
		if( calibre == "" )
		{
			alert("Ingrese el calibre correspondiente a la ventilación");
			document.form_agregar_nueva_ventilacion.txt_calibre_ventilacion.focus();
			return false;
		}
	else
		if(precio == "" )
		{
			alert("Ingrese el precio correspondiente a la ventilación");
			document.form_agregar_nueva_ventilacion.precio.focus();
			return false;
		}
	else
		if( !/^([0-9])*[.]?[0-9]*$/.test(calibre) )
		{
			alert("Ingrese un calibre correcto para la ventilación");
			document.form_agregar_nueva_ventilacion.txt_calibre_ventilacion.focus();
			return false;
		}
	else
		if( imagen_ventilacion == "" )
		{
			alert("Seleccione la imagen correspondiente a la ventilación");
			document.form_agregar_nueva_ventilacion.imagen_vent.focus();
			return false;
		}
	else
		if( imagen_ventilacion != "" )
		{
			for(var i = 0; i < extensiones_permitidas.length; i++) 
			{ 
      			if (extensiones_permitidas[i] == extension) 
      			{ 
         			permitida = true; 
         			break; 
      			}
    		}
    		if ( !permitida ) 
    		{ 
          		alert("Comprueba la extensión de los archivos a subir. \nSólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join());
          		document.form_agregar_nueva_ventilacion.imagen_vent.focus();
          		return false;
    		}
		}	
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR NUEVO MATERIAL
function validaAgregarNuevoMaterial()
{
	var nombre_material = document.form_nuevo_material.txt_nombre_material.value
	var descripcion = document.form_nuevo_material.txt_descripcion.value
	
	if( nombre_material == "" && descripcion == "" )
	{
		alert("Ingrese la informacion correspondiente para registrar el nuevo material");
		document.form_nuevo_material.txt_nombre_material.focus();
		return false;
	}
	else
		if( nombre_material == "" )
		{
			alert("Ingrese el nombre del material");
			document.form_nuevo_material.txt_nombre_material.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR NUEVO COLOR
function validaAgregarNuevoColor()
{
	var nombre_color = document.form_agregar_nuevo_color.txt_nombre_color.value
	var color = document.form_agregar_nuevo_color.color_picker.value
	
	if( nombre_color == "" && color == "" )
	{
		alert("Ingrese los datos correspondientes para registrar el color");
		document.form_agregar_nuevo_color.txt_nombre_color.focus();
		return false;
	}
	else
		if( nombre_color == "" )
		{
			alert("Ingrese el nombre del color");
			document.form_agregar_nuevo_color.txt_nombre_color.focus();
			return false;
		}
	else
		if( color == "" )
		{
			alert("Seleccione el color a registrar");
			document.form_agregar_nuevo_color.color_picker.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR NUEVA SALIDA
function validaAgregarSalida()
{
	var nombre_salida = document.form_agregar_nueva_salida.txt_nombre_salida.value
	var imagen = document.form_agregar_nueva_salida.file_imagen.value
	var precio = document.form_agregar_nueva_salida.precio.value
	var extensiones_permitidas = new Array(".gif", ".jpg", ".png", ".jpeg");
	var extension = (imagen.substring(imagen.lastIndexOf("."))).toLowerCase();
	var permitida = false;
	
	if( nombre_salida == "" && imagen == "" && precio == "")
	{
		alert("Ingrese los datos correspondientes, para poder registrar la salida");
		document.form_agregar_nueva_salida.txt_nombre_salida.focus();
		return false;
	}
	else
		if( nombre_salida == "" )
		{
			alert("Ingrese el nombre de la salida");
			document.form_agregar_nueva_salida.txt_nombre_salida.focus();
			return false;
		}
	else
		if( precio == "" )
		{
			alert("Ingrese el precio de la salida");
			document.form_agregar_nueva_salida.precio.focus();
			return false;
		}
	else
		if( imagen == "" )
		{
			alert("Seleccione la imagen correspondiente a la salida");
			document.form_agregar_nueva_salida.file_imagen.focus();
			return false;
		}
	else
		if( imagen != "" )
		{
			for(var i = 0; i < extensiones_permitidas.length; i++) 
			{ 
      			if (extensiones_permitidas[i] == extension) 
      			{ 
         			permitida = true; 
         			break; 
      			}
    		}
    		if ( !permitida ) 
    		{ 
          		alert("Comprueba la extensión de los archivos a subir. \nSólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join());
          		document.form_agregar_nueva_salida.file_imagen.focus();
          		return false;
    		}
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR NUEVO ESTILO
function validaAgregarNuevoEstilo()
{
	var estilo = document.form_agregar_nuevo_estilo.txt_estilo.value
	var imagen = document.form_agregar_nuevo_estilo.imagen_upload.value
	var precio = document.form_agregar_nuevo_estilo.precio.value	
	var extensiones_permitidas = new Array(".gif", ".jpg", ".png", ".jpeg");
	var extension = (imagen.substring(imagen.lastIndexOf("."))).toLowerCase();
	var permitida = false;
	
	if( estilo == "" && imagen == ""  && precio ==0 )
	{
		alert("Ingrese los datos correspondientes, para poder registrar el estilo");
		document.form_agregar_nuevo_estilo.txt_estilo.focus();
		return false;
	}
	else
		if( estilo == "" )
		{
			alert("Ingrese el nombre del estilo");
			document.form_agregar_nuevo_estilo.txt_estilo.focus();
			return false;
		}
	else
		if(precio == 0)
		{
			alert("Ingrese el precio del estilo");
			document.form_agregar_nuevo_estilo.precio.focus();
			return false;
		}
	else
		if( imagen == "" )
		{
			alert("Seleccione la imagen correspondiente al estilo a registrar");
			document.form_agregar_nuevo_estilo.imagen_upload.focus();
			return false;
		}
	else
		if( imagen != "" )
		{
			for(var i = 0; i < extensiones_permitidas.length; i++) 
			{ 
      			if (extensiones_permitidas[i] == extension) 
      			{ 
         			permitida = true; 
         			break; 
      			} 
    		}
    		if ( !permitida ) 
    		{ 
          		alert("Comprueba la extensión de los archivos a subir. \nSólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join()); 
    			document.form_agregar_nuevo_estilo.imagen_upload.focus();
    			return false;
    		}
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MODIFICAR PROVEEDORES
function validarModificarProveedor()
{
	// VARIABLES DEL FORMULARIO
	var proveedor = document.form_modificar_proveedor.proveedor.value
	var razon_social = document.form_modificar_proveedor.razon_social.value
	var rfc = document.form_modificar_proveedor.rfc.value
	var credito = document.form_modificar_proveedor.credito.value
	var plazos_pago = document.form_modificar_proveedor.plazos_pago.value
	var calle = document.form_modificar_proveedor.calle.value
	var num_exterior = document.form_modificar_proveedor.num_exterior.value
	var num_interior = document.form_modificar_proveedor.num_interior.value
	var colonia = document.form_modificar_proveedor.colonia.value
	var codigo_postal = document.form_modificar_proveedor.codigo_postal.value
	var id_estado = document.form_modificar_proveedor.id_estado.value
	var id_municipio = document.form_modificar_proveedor.id_municipio.value
	var nuevo_pais = document.form_modificar_proveedor.pais_nuevo.value
	var nuevo_estado = document.form_modificar_proveedor.estado_nuevo.value
	var nuevo_ciudad = document.form_modificar_proveedor.municipio_nuevo.value
	var pais = document.form_modificar_proveedor.pais.value
	
	if( proveedor == "" && razon_social == "" && rfc == "" )
	{
		alert("Los campos se encuentran vacíos, ingrese la información correspondiente");
		document.form_modificar_proveedor.proveedor.focus();
		return false;
	}
	else
		if( proveedor == "" )
		{
			alert("Ingrese el nombre del proveedor");
			document.form_modificar_proveedor.proveedor.focus();
			return false;
		}
/*	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(proveedor) )
		{
			alert("El nombre " + proveedor + " no es valido, ingrese nuevamente un nombre");
			document.form_modificar_proveedor.proveedor.focus();
			return false;
		}*/
	else
		if( razon_social == "" )
		{
			alert("Ingrese la razón social del proveedor");
			document.form_modificar_proveedor.razon_social.focus();
			return false;
		}
	else
		if( rfc == "" )
		{
			alert("Ingrese el Registro Federal de Contribuyentes (RFC)");
			document.form_modificar_proveedor.rfc.focus();
			return false;
		}
	else
		if( credito == "" )
		{
			alert("Ingrese el crédito del proveedor");
			document.form_modificar_proveedor.credito.focus();
			return false;
		}
	else
		if( plazos_pago  == "" )
		{
			alert("Ingrese el plazo de pago que le otorga el proveedor");
			document.form_modificar_proveedor.plazos_pago.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(plazos_pago) )
		{
			alert("El valor " + plazos_pago + " no es valido ingrese una cantidad valida");
			document.form_modificar_proveedor.plazos_pago.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio del proveedor");
			document.form_modificar_proveedor.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_modificar_proveedor.num_exterior.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El número " + num_exterior + " no es valido, ingrese un número valido");
			document.form_modificar_proveedor.num_exterior.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_modificar_proveedor.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_modificar_proveedor.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El código " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_modificar_proveedor.codigo_postal.focus();
			return false;
		}
	else
			if(pais == 0 && nuevo_pais == ""){
			alert("Seleccionar el pais");
			document.form_modificar_proveedor.pais.focus();
			return false;
		}

	else
		if( estado == 0 && nuevo_estado == "")
		{
			alert("Seleccione el estado");
			document.form_modificar_proveedor.id_estado.focus();
			return false;
		}
	else
		if( ciudad == 0 && nuevo_ciudad == "")
		{
			alert("Seleccione el municipio");
			document.form_modificar_proveedor.id_municipio.focus();
			return false;
		}
	/*else
		if( id_municipio == 0 )
		{
			alert("Seleccione el municipio o la ciudad");
			document.form_modificar_proveedor.id_municipio.focus();
			return false;
		}*/
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MODIFICAR DEPARTAMENTO
function validarModificarDepartamento()
{
	// VARIABLE DEL FORMULARIO
	var departamento = document.form_modificar_departamento.departamento.value
	
	if( departamento == "" )
	{
		alert("Ingrese el nombre del departamento");
		document.form_modificar_departamento.departamento.focus();
		return false;
	}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MODIFICAR ALMACEN
function validarModificarAlmacen()
{
	// VARIABLE DEL FORMULARIO
	var almacen = document.form_modificar_almacen.almacen.value
	
	if( almacen == "" )
	{
		alert("Ingrese el nombre del almacen");
		document.form_modificar_almacen.almacen.focus();
		return false;
	}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO LISTA DE SUCURSALES
function validarAgregarSucursal()
{
	// VARIABLES DEL FORMULARIO
	var sucursal = document.form_agregar_sucursal.sucursal.value
	var calle = document.form_agregar_sucursal.calle.value
	var num_exterior = document.form_agregar_sucursal.num_exterior.value
	var	colonia = document.form_agregar_sucursal.colonia.value
	
	// VALIDACIONES DEL FORMULARIO
	if( sucursal == "" && calle == "" && num_exterior == "" && colonia == "" )
	{
		alert("Los campos se encuentran vacíos, ingrese la información correspondiente");
		document.form_agregar_sucursal.sucursal.focus();
		return false;
	}
	else
		if( sucursal == "" )
		{
			alert("Ingrese el nombre de la sucursal a registrar");
			document.form_agregar_sucursal.sucursal.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio donde se encuentra la sucursal");
			document.form_agregar_sucursal.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_agregar_sucursal.num_exterior.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El número " + num_exterior + " no es valido, ingrese un número valido");
			document.form_agregar_sucursal.num_exterior.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese el nombre de la colonia donde se encuentra la sucursal");
			document.form_agregar_sucursal.colonia.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO LISTA DEPARTAMENTOS
function validarAgregarDepartamento()
{
	// VARIABLE DEL FORMULARIO
	var departamento = document.form_agregar_departamento.departamento.value
	
	if( departamento == "" )
	{
		alert("Ingrese el nombre del departamento a registrar");
		document.form_agregar_departamento.departamento.focus();
		return false;
	}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO LISTA DE ALMACENES
function validarAgregarAlmacen()
{
	// VARIABLE DEL FORMULARIO
	var almacen = document.form_agregar_almacen.almacen.value
	
	if( almacen == "" )
	{
		alert("Ingrese el nombre del almacén a registrar");
		document.form_agregar_almacen.almacen.focus();
		return false;
	}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MODIFICAR PERSONAL
function validarModificarPersonal()
{
	// VARIABLES CON LOS DATOS DEL FORMULARIO
	var nombre = document.form_modificar_personal.nombre.value
	var paterno = document.form_modificar_personal.paterno.value
	var materno = document.form_modificar_personal.materno.value
	var ocupacion = document.form_modificar_personal.ocupacion.value
	var alias = document.form_modificar_personal.alias.value
	var fecha_nacimiento = document.form_modificar_personal.fecha_nacimiento.value
	var genero = document.form_modificar_personal.genero.value
	var id_departamento = document.form_modificar_personal.id_departamento.value
	var estado_civil = document.form_modificar_personal.estado_civil.value
	var calle = document.form_modificar_personal.calle.value
	var num_exterior = document.form_modificar_personal.num_exterior.value
	var num_interior = document.form_modificar_personal.num_interior.value
	var colonia = document.form_modificar_personal.colonia.value
	var codigo_postal = document.form_modificar_personal.codigo_postal.value
	var id_estado = document.form_modificar_personal.id_estado.value
	var id_municipio = document.form_modificar_personal.id_municipio.value

	// VALIDACIONES DE TODOS LOS CAMPOS DEL FORMULARIO
	if( nombre == "" && paterno == "" && materno == "" && ocupacion == "" && alias == "" && fecha_nacimiento == "" && genero == 0 && id_departamento == 0 && estado_civil == 0 && calle == "" && num_exterior == "" && num_interior == "" && colonia == "" && codigo_postal == "" && id_estado == 0 && id_municipio == 0 )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_modificar_personal.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del empleado");
			document.form_modificar_personal.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese nuevamente un nombre");
			document.form_modificar_personal.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del empleado");
			document.form_modificar_personal.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido " + paterno + " no es valido, ingrese un apellido paterno valido");
			document.form_modificar_personal.paterno.focus();
			return false;
		}
	else
		if( materno == "" )
		{
			alert("Ingrese el apellido materno del empleado");
			document.form_modificar_personal.materno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
		{
			alert("El apellido " + materno + " no es valido, ingrese un apellido materno valido");
			document.form_modificar_personal.materno.focus();
			return false;
		}
	else
		if( alias == "" )
		{
			alert("Ingrese el alias del empleado");
			document.form_modificar_personal.alias.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(alias) )
		{
			alert("El alias o nombre " + alias + " no es valido, ingrese nuevamente un alias");
			document.form_modificar_personal.alias.focus();
			return false;
		}
	else
		if( ocupacion == "" )
		{
			alert("Ingrese la ocupación del empleado a registrar");
			document.form_modificar_personal.ocupacion.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(ocupacion) )
		{
			alert("La ocupación " + ocupacion + " no es valida, ingrese una nueva ocupación");
			document.form_modificar_personal.ocupacion.focus();
			return false;
		}
	else
		if( fecha_nacimiento == "" )
		{
			alert("Ingrese la fecha de nacimiento del empleado");
			document.form_modificar_personal.fecha_nacimiento.focus();
			return false;
		}
	else
		if( fecha_nacimiento.length < 10 )
		{
			alert("La fecha de nacimiento es incorrecta, ingrese una nuevamente");
			document.form_modificar_personal.fecha_nacimiento.focus();
			return false;
		}
	else
		if( genero == 0 )
		{
			alert("Seleccione el genero del empleado");
			document.form_modificar_personal.genero.focus();
			return false;
		}
	else
		if( id_departamento == 0 )
		{
			alert("Seleccione el departamento en el que trabaja el empleado");
			document.form_modificar_personal.id_departamento.focus();
			return false;
		}
	else
		if( estado_civil == 0 )
		{
			alert("Seleccione el estado civil del empleado");
			document.form_modificar_personal.estado_civil.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio del empleado");
			document.form_modificar_personal.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_modificar_personal.num_exterior.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El número " + num_exterior + " no es valido, ingrese un número valido");
			document.form_modificar_personal.num_exterior.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_modificar_personal.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_modificar_personal.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El código " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_modificar_personal.codigo_postal.focus();
			return false;
		}
	else
		if( id_estado == 0 )
		{
			alert("Seleccione el estado");
			document.form_modificar_personal.id_estado.focus();
			return false;
		}
	else
		if( id_municipio == 0 )
		{
			alert("Seleccione el municipio o la ciudad");
			document.form_modificar_personal.id_municipio.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR PERSONAL
function validarAgregarPersonal()
{
	// VARIABLES CON LOS DATOS DEL FORMULARIO
	var nombre = document.form_agregar_personal.nombre.value
	var paterno = document.form_agregar_personal.paterno.value
	var materno = document.form_agregar_personal.materno.value
	var alias = document.form_agregar_personal.alias.value
	var ocupacion = document.form_agregar_personal.ocupacion.value
	var fecha_nacimiento = document.form_agregar_personal.fecha_nacimiento.value
	var id_departamento = document.form_agregar_personal.id_departamento.value
	var calle = document.form_agregar_personal.calle.value
	var num_ext = document.form_agregar_personal.num_ext.value
	var num_int = document.form_agregar_personal.num_int.value
	var colonia = document.form_agregar_personal.colonia.value
	var codigo_postal = document.form_agregar_personal.codigo_postal.value
	var id_estado = document.form_agregar_personal.id_estado.value
	var id_municipio = document.form_agregar_personal.id_municipio.value
	var sexo = document.form_agregar_personal.sexo.value
	var estado_civil = document.form_agregar_personal.estado_civil.value
	
	// VALIDACIONES DE TODOS LOS CAMPOS DEL FORMULARIO
	if( nombre == "" && paterno == "" && materno == "" && alias == "" && ocupacion == "" && fecha_nacimiento == "" && id_departamento == 0 && calle == "" && num_ext == "" && num_int == "" && colonia == "" && codigo_postal == "" && id_estado == 0 && id_municipio == 0 && sexo == 0 && estado_civil == 0 )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_agregar_personal.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del empleado");
			document.form_agregar_personal.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese nuevamente un nombre");
			document.form_agregar_personal.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del empleado");
			document.form_agregar_personal.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido " + paterno + " no es valido, ingrese un apellido paterno valido");
			document.form_agregar_personal.paterno.focus();
			return false;
		}
	else
		if( materno == "" )
		{
			alert("Ingrese el apellido materno del empleado");
			document.form_agregar_personal.materno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
		{
			alert("El apellido " + materno + " no es valido, ingrese un apellido materno valido");
			document.form_agregar_personal.materno.focus();
			return false;
		}
	else
		if( alias == "" )
		{
			alert("Ingrese el alias del empleado");
			document.form_agregar_personal.alias.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(alias) )
		{
			alert("El alias o nombre " + alias + " no es valido, ingrese nuevamente un alias");
			document.form_agregar_personal.alias.focus();
			return false;
		}
	else
		if( ocupacion == "" )
		{
			alert("Ingrese la ocupación del empleado a registrar");
			document.form_agregar_personal.ocupacion.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(ocupacion) )
		{
			alert("La ocupación " + ocupacion + " no es valida, ingrese una nueva ocupación");
			document.form_agregar_personal.ocupacion.focus();
			return false;
		}
	else
		if( fecha_nacimiento == "" )
		{
			alert("Ingrese la fecha de nacimiento del empleado");
			document.form_agregar_personal.fecha_nacimiento.focus();
			return false;
		}
	else
		if( fecha_nacimiento.length < 10 )
		{
			alert("La fecha de nacimiento es incorrecta, ingrese una nuevamente");
			document.form_agregar_personal.fecha_nacimiento.focus();
			return false;
		}
	else
		if( id_departamento == 0 )
		{
			alert("Seleccione el departamento en el que trabaja el empleado");
			document.form_agregar_personal.id_departamento.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio del empleado");
			document.form_agregar_personal.calle.focus();
			return false;
		}
	else
		if( num_ext == "" )
		{
			alert("Ingrese el número exterior");
			document.form_agregar_personal.num_ext.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_ext) )
		{
			alert("El número " + num_ext + " no es valido, ingrese un número valido");
			document.form_agregar_personal.num_ext.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_agregar_personal.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_agregar_personal.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El código " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_agregar_personal.codigo_postal.focus();
			return false;
		}
	else
		if( id_estado == 0 )
		{
			alert("Seleccione el estado");
			document.form_agregar_personal.id_estado.focus();
			return false;
		}
	else
		if( id_municipio == 0 )
		{
			alert("Seleccione el municipio o la ciudad");
			document.form_agregar_personal.id_municipio.focus();
			return false;
		}
	else
		if( sexo == 0 )
		{
			alert("Seleccione el genero del empleado");
			document.form_agregar_personal.sexo.focus();
			return false;
		}
	else
		if( estado_civil == 0 )
		{
			alert("Seleccione el estado civil del empleado");
			document.form_agregar_personal.estado_civil.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO MODIFICAR PACIENTES
function validarModificarPacientes()
{
	// VARIABLES CON LOS DATOS DEL FORMULARIO
	var nombre = document.form_modificar_paciente.nombre.value
	var paterno = document.form_modificar_paciente.paterno.value
	var materno = document.form_modificar_paciente.materno.value
	var fecha_nacimiento = document.form_modificar_paciente.fecha_nacimiento.value
	var edad = document.form_modificar_paciente.edad.value
	var genero = document.form_modificar_paciente.genero.value
	var ocupacion = document.form_modificar_paciente.ocupacion.value
	var estado_civil = document.form_modificar_paciente.estado_civil.value
	var calle = document.form_modificar_paciente.calle.value
	var num_exterior = document.form_modificar_paciente.num_exterior.value
	var num_interior = document.form_modificar_paciente.num_interior.value
	var colonia = document.form_modificar_paciente.colonia.value
	var codigo_postal = document.form_modificar_paciente.codigo_postal.value
	var id_estado = document.form_modificar_paciente.id_estado.value
	var id_municipio = document.form_modificar_paciente.id_municipio.value
	var rfc = document.form_modificar_paciente.rfc.value
	
	// VALIDACIONES DE TODOS LOS CAMPOS DEL FORMULARIO
	if( nombre == "" && paterno == "" && materno == "" && fecha_nacimiento == "" && edad == "" && genero == 0 && ocupacion == "" && estado_civil == 0 && calle == "" && num_exterior == "" && num_interior == "" && colonia == "" && codigo_postal == "" && id_estado == 0 && id_municipio == 0 )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_modificar_paciente.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del paciente");
			document.form_modificar_paciente.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese nuevamente un nombre");
			document.form_modificar_paciente.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del paciente");
			document.form_modificar_paciente.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido " + paterno + " no es valido, ingrese un apellido paterno valido");
			document.form_modificar_paciente.paterno.focus();
			return false;
		}
	else
		if( materno == "" )
		{
			alert("Ingrese el apellido materno del paciente");
			document.form_modificar_paciente.materno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
		{
			alert("El apellido " + materno + " no es valido, ingrese un apellido materno valido");
			document.form_modificar_paciente.materno.focus();
			return false;
		}
	else
		if( fecha_nacimiento == "" )
		{
			alert("Ingrese la fecha de nacimiento del paciente");
			document.form_modificar_paciente.fecha_nacimiento.focus();
			return false;
		}
	else
		if( fecha_nacimiento.length < 10 )
		{
			alert("La fecha de nacimiento es incorrecta, ingrese una nuevamente");
			document.form_modificar_paciente.fecha_nacimiento.focus();
			return false;
		}
	else
		if( edad == "" )
		{
			alert("Ingrese la edad del paciente");
			document.form_modificar_paciente.edad.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(edad) )
		{
			alert("La edad " + edad + " no es valida, ingrese nuevamente la edad");
			document.form_modificar_paciente.edad.focus();
			return false;
		}
	else
		if( genero == 0 )
		{
			alert("Seleccione el genero del paciente");
			document.form_modificar_paciente.genero.focus();
			return false;
		}
	else
		if( ocupacion == "" )
		{
			alert("Ingrese la ocupación del paciente");
			document.form_modificar_paciente.ocupacion.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(ocupacion) )
		{
			alert("La ocupación " + ocupacion + " no es valida, ingrese una nueva ocupación");
			document.form_modificar_paciente.ocupacion.focus();
			return false;
		}
	else
		if( estado_civil == 0 )
		{
			alert("Seleccione el estado civil del paciente");
			document.form_modificar_paciente.estado_civil.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio o calle donde vive el paciente");
			document.form_modificar_paciente.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_modificar_paciente.num_exterior.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El número " + num_exterior + " no es valido, ingrese un número valido");
			document.form_modificar_paciente.num_exterior.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia donde habita el paciente");
			document.form_modificar_paciente.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_modificar_paciente.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El código " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_modificar_paciente.codigo_postal.focus();
			return false;
		}
	else
		if( id_estado == 0 )
		{
			alert("Seleccione el estado");
			document.form_modificar_paciente.id_estado.focus();
			return false;
		}
	else
		if( id_municipio == 0 )
		{
			alert("Seleccione el municipio o la ciudad");
			document.form_modificar_paciente.id_municipio.focus();
			return false;
		}
	else
		/*if( rfc.length > 0 )*/
		if(rfc=="")
		{
			/*if(!/^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))$/.test(rfc))
			{*/
				alert("El RFC " + rfc + " no es correcto, ingrese un RFC valido");
				document.form_modificar_paciente.rfc.focus();
				return false;
			/*}*/
		}
}
/********************************************************************/
// FUNCION QUE REGRESA EL FORMULARIO AGREGAR CONTACTO PROVEEDOR A AGREGAR PROVEEDOR
function regresar()
{
	var confirmar
	confirmar = confirm('Termino El Registro Del Contacto Del Proveedor');
	if( confirmar == true )
	{
		window.location.replace('agregar_proveedor.php');
	}
	else
	{
	}
	 
}
/********************************************************************/
/********************************************************************/
// FUNCION QUE REGRESA EL FORMULARIO AGREGAR CONTACTO PROVEEDOR A AGREGAR PROVEEDOR
function regresar2()
{
	var confirmar
	confirmar = confirm('Termino El Registro Del Contacto De La Institucion');
	if( confirmar == true )
	{
		window.location.replace('agregar_instituciones.php');
	}
	else
	{
	}
	 
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR PACIENTES
function validarPacientes()
{
	// VARIABLES CON LOS VALORES DEL FORMULARIO
	var nombre = document.form_agregar_pacientes.nombre.value
	var paterno = document.form_agregar_pacientes.paterno.value
	var materno = document.form_agregar_pacientes.materno.value
	var ocupacion = document.form_agregar_pacientes.ocupacion.value
	var fecha_nacimiento = document.form_agregar_pacientes.fecha_nacimiento.value
	var edad = document.form_agregar_pacientes.edad.value
	var calle = document.form_agregar_pacientes.calle.value
	var num_ext = document.form_agregar_pacientes.num_ext.value
	var num_int = document.form_agregar_pacientes.num_int.value
	var colonia = document.form_agregar_pacientes.colonia.value
	var codigo_postal = document.form_agregar_pacientes.codigo_postal.value
	var estado = document.form_agregar_pacientes.id_estado.value
	var municipio = document.form_agregar_pacientes.id_municipio.value
	var genero = document.form_agregar_pacientes.genero.value
	var estado_civil = document.form_agregar_pacientes.estado_civil.value
	var rfc = document.form_agregar_pacientes.rfc.value
	var tipo_paciente = document.form_agregar_pacientes.tipo_paciente.value
	var inst = document.form_agregar_pacientes.inst.value
	
	if( nombre == "" && paterno == "" && materno == "" && ocupacion == "" && fecha_nacimiento == "" && edad == "" && calle == "" && num_ext == "" && num_int == "" && colonia == "" && codigo_postal == "" && estado == 0 && municipio == 0 && genero == 0 && estado_civil == 0 && rfc == "" && tipo_paciente == 0 && inst == 0 )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_agregar_pacientes.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del paciente");
			document.form_agregar_pacientes.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese nuevamente un nombre");
			document.form_agregar_pacientes.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del paciente");
			document.form_agregar_pacientes.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido " + paterno + " no es valido, ingrese un apellido paterno valido");
			document.form_agregar_pacientes.paterno.focus();
			return false;
		}
	else
		if( materno == "" )
		{
			alert("Ingrese el apellido materno del paciente");
			document.form_agregar_pacientes.materno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
		{
			alert("El apellido " + materno + " no es valido, ingrese un nuevo apellido materno");
			document.form_agregar_pacientes.materno.focus();
			return false;
		}
	else
		if( ocupacion == "" )
		{
			alert("Ingrese la ocupación del paciente");
			document.form_agregar_pacientes.ocupacion.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(ocupacion) )
		{
			alert("La ocupación " + ocupacion + " no es valida, ingrese una nueva ocupación");
			document.form_agregar_pacientes.ocupacion.focus();
			return false;
		}
	else
		if( fecha_nacimiento == "" )
		{
			alert("Ingrese la fecha de nacimiento del paciente");
			document.form_agregar_pacientes.fecha_nacimiento.focus();
			return false;
		}
	else
		if( fecha_nacimiento.length < 10 )
		{
			alert("Fecha de nacimiento incorrecta, ingrese una fecha valida");
			document.form_agregar_pacientes.fecha_nacimiento.focus();
			return false;
		}
	else
		if( edad == "" )
		{
			alert("Ingrese la edad que tiene el paciente a registrar");
			document.form_agregar_pacientes.edad.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(edad) )
		{
			alert("La edad " + edad + " no es valida, ingrese nuevamente la edad");
			document.form_agregar_pacientes.edad.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese el domicilio del paciente");
			document.form_agregar_pacientes.calle.focus();
			return false;
		}
	else
		if( num_ext == "" )
		{
			alert("Ingrese el Número exterior del domicilio del paciente");
			document.form_agregar_pacientes.num_ext.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_ext) )
		{
			alert("El número " + num_ext + " no es valido, ingrese un número valido");
			document.form_agregar_pacientes.num_ext.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_agregar_pacientes.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_agregar_pacientes.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El código " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_agregar_pacientes.codigo_postal.focus();
			return false;
		}
	else
		if( estado == 0 )
		{
			alert("Seleccione un estado");
			document.form_agregar_pacientes.id_estado.focus();
			return false;
		}
	else
		if( municipio == 0 )
		{
			alert("Seleccione el municipio o ciudad");
			document.form_agregar_pacientes.id_municipio.focus();
			return false;
		}
	else
		if( genero == 0 )
		{
			alert("Seleccione el genero del paciente");
			document.form_agregar_pacientes.genero.focus();
			return false;
		}
	else
		if( estado_civil == 0 )
		{
			alert("Seleccione el estado civil del paciente");
			document.form_agregar_pacientes.estado_civil.focus();
			return false;
		}
	else
		if( tipo_paciente == 0 )
		{
			alert("Seleccione el tipo de paciente");
			document.form_agregar_pacientes.tipo_paciente.focus();
			return false;
		}
	else
		/*if( rfc.length > 0 )*/
		if(rfc=="")
		{
			/*if( !/^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))$/.test(rfc) )
			{*/
				alert("El RFC " + rfc + " no es correcto, ingrese un RFC valido");
				document.form_agregar_pacientes.rfc.focus();
				return false;
			/*}*/
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR CONTACTOS DE PROVEEDOR
function validarContactoProveedor()
{
	// VARIABLES CON LOS VALORES DEL FORMULARIO AGREGAR CONTACTOS PROVEEDOR
	var nombre = document.form_agregar_contacto_proveedor.nombre.value
	var paterno = document.form_agregar_contacto_proveedor.paterno.value
	
	if( nombre == "" && paterno == "" )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_agregar_contacto_proveedor.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del contacto");
			document.form_agregar_contacto_proveedor.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese un nuevo nombre");
			document.form_agregar_contacto_proveedor.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del contacto");
			document.form_agregar_contacto_proveedor.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido paterno " + paterno + " no es valido, ingrese un nuevo apellido");
			document.form_agregar_contacto_proveedor.paterno.focus();
			return false;
		}
}
/********************************************************************/
// FUNCIONES QUE VALIDAN LOS CAMPOS DE TEXTO NOMBRE, APELLIDO PATERNO Y APELLIDO MATERNO
function validaNombre(nombre)
{
	if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
	{
		alert("El nombre " + nombre + " no es valido, ingrese un nuevo nombre");
		return false;
	}
}

function validaPaterno(paterno)
{
	if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
	{
		alert("El apellido paterno " + paterno + " no es valido, ingrese un nuevo apellido");
		return false;
	}
}

function validaMaterno(materno)
{
	if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
	{
		alert("El apellido materno " + materno + " no es valido, ingrese un nuevo apellido");
		return false;
	}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR CONTACTO DE INSTITUCIONES
function validarContacto()
{
	// DECLARACION DE VARAIABLES QUE CONTIENEN LOS VALORES DE LOS CAMPOS DE TEXTO DEL FORMULARIO
	var nombre = document.form_contacto_institucion.nombre.value
	var paterno = document.form_contacto_institucion.paterno.value
	var materno = document.form_contacto_institucion.materno.value
	var puesto = document.form_contacto_institucion.puesto.value	
	var departamento = document.form_contacto_institucion.departamento.value
	
	if( nombre == "" || paterno == "" || materno == "" || puesto == "" || departamento == "" )
	{
		alert("Algunos campos se encuentran vacios, ingrese la información correspondiente");
		document.form_contacto_institucion.nombre.focus();
		return false;
	}
	else
		if( nombre == "" )
		{
			alert("Ingrese el nombre del contacto");
			document.form_contacto_institucion.nombre.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nombre) )
		{
			alert("El nombre " + nombre + " no es valido, ingrese un nuevo nombre");
			document.form_contacto_institucion.nombre.focus();
			return false;
		}
	else
		if( paterno == "" )
		{
			alert("Ingrese el apellido paterno del contacto");
			document.form_contacto_institucion.paterno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(paterno) )
		{
			alert("El apellido paterno " + paterno + " no es valido, ingrese un nuevo apellido");
			document.form_contacto_institucion.paterno.focus();
			return false;
		}
	else
		if( materno == "" )
		{
			alert("Ingrese el apellido materno del contacto");
			document.form_contacto_institucion.materno.focus();
			return false;
		}
	else
		if( !/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(materno) )
		{
			alert("El apellido materno " + materno + " no es valido, ingrese un nuevo apellido");
			document.form_contacto_institucion.materno.focus();
			return false;
		}
	else
		if( puesto == "" )
		{
			alert("Ingrese el puesto del contacto a registrar");
			document.form_contacto_institucion.puesto.focus();
			return false;
		}
	else
		if( departamento == "" )
		{
			alert("Ingrese el nombre del departamento del contacto a registrar");
			document.form_contacto_institucion.departamento.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR INSTITUCIONES
function validarAgregarInstituciones()
{
	// VARIABLES QUE CONTIENEN LOS DATOS DEL FORMULARIO
	var institucion = document.form_agregar_instituciones.institucion.value
	var calle = document.form_agregar_instituciones.calle.value
	var num_exterior = document.form_agregar_instituciones.num_exterior.value
	var num_interior = document.form_agregar_instituciones.num_interior.value
	var colonia = document.form_agregar_instituciones.colonia.value
	var codigo_postal = document.form_agregar_instituciones.codigo_postal.value
	var estado = document.form_agregar_instituciones.id_estado.value
	var municipio = document.form_agregar_instituciones.id_municipio.value
	var descuento = document.form_agregar_instituciones.descuento.value
	var costo_aparato = document.form_agregar_instituciones.costo_aparato.value
	var poliza = document.form_agregar_instituciones.poliza.value
	var razon_social = document.form_agregar_instituciones.razon_social.value
	var rfc = document.form_agregar_instituciones.rfc.value
	
	if( institucion == "" && calle == "" && num_exterior == "" && num_interior == "" && colonia == "" && codigo_postal == "" && estado == 0 && municipio == 0 && descuento == "" && costo_aparato == "" && poliza == 0 && razon_social == "" && rfc == "" )
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_agregar_instituciones.institucion.focus();
		return false;
	}
	else
		if( institucion == "" )
		{
			alert("Ingrese el nombre de la Dependencia o Institución");
			document.form_agregar_instituciones.institucion.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese la calle donde se ubica la Institución");
			document.form_agregar_instituciones.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_agregar_instituciones.num_exterior.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El valor " + num_exterior + " no es valido, ingrese un número valido");
			document.form_agregar_instituciones.num_exterior.value = "";
			document.form_agregar_instituciones.num_exterior.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_agregar_instituciones.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_agregar_instituciones.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El valor " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_agregar_instituciones.codigo_postal.value = "";
			document.form_agregar_instituciones.codigo_postal.focus();
			return false;
		}
	else
		if( estado == 0 )
		{
			alert("Seleccione el estado");
			document.form_agregar_instituciones.id_estado.focus();
			return false;
		}
	else
		if( municipio == 0 )
		{
			alert("Seleccione el municipio o ciudad");
			document.form_agregar_instituciones.id_municipio.focus();
			return false;
		}
	else
		if( descuento == "" )
		{
			alert("Ingrese el descuento");
			document.form_agregar_instituciones.descuento.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(descuento) )
		{
			alert("El valor " + descuento + " no es valido, ingrese un descuento valido");
			document.form_agregar_instituciones.descuento.value = "";
			document.form_agregar_instituciones.descuento.focus();
			return false;
		}
	else
		if( costo_aparato == "" )
		{
			alert("El campo costo por aparato es obligatorio, ingrese un costo");
			document.form_agregar_instituciones.costo_aparato.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(costo_aparato) )
		{
			alert("El valor " + costo_aparato + " no es valido, ingrese un costo valido");
			document.form_agregar_instituciones.costo_aparato.value = "";
			document.form_agregar_instituciones.costo_aparato.focus();
			return false;
		}
	else
		if( poliza == 0 )
		{
			alert("Ingrese la poliza");
			document.form_agregar_instituciones.poliza.focus();
			return false;
		}
	else
		if( razon_social == "" )
		{
			alert("Ingrese la razón social de la institución");
			document.form_agregar_instituciones.razon_social.focus();
			return false;
		}
	else
		if( rfc == "" )
		{
			alert("Ingrese el RFC de la Institucion o dependencia");
			document.form_agregar_instituciones.rfc.focus();
			return false;
		}
	/*else
		if( !/^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))$/.test(rfc) )
		{
			alert("El RFC " + rfc + " no es correcto, ingrese un RFC valido");
			document.form_agregar_instituciones.rfc.focus();
			return false;
		}*/
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO AGREGAR PROVEEDOR
function validarAgregarProveedor()
{
	// DECALRACION DE VARIABLES QUE CONTIENEN LOS VALORES DE LOS CAMPOS DE TEXTO Y SELECTS DEL FORMULARIO
	var proveedor = document.form_agregar_proveedor.proveedor.value
	var nombre = document.form_agregar_proveedor.nombre.value
	var calle = document.form_agregar_proveedor.calle.value
	var num_exterior = document.form_agregar_proveedor.num_ext.value
	var num_interior = document.form_agregar_proveedor.num_int.value
	var colonia = document.form_agregar_proveedor.colonia.value
	var codigo_postal = document.form_agregar_proveedor.codigo_postal.value
	var pais = document.form_agregar_proveedor.pais.value
	var estado = document.form_agregar_proveedor.id_estado.value
	var ciudad = document.form_agregar_proveedor.id_municipio.value
	var rfc = document.form_agregar_proveedor.rfc.value
	var credito = document.form_agregar_proveedor.credito.value
	var plazo_pago = document.form_agregar_proveedor.plazo_pago.value
	var nuevo_pais = document.form_agregar_proveedor.pais_nuevo.value
	var nuevo_estado = document.form_agregar_proveedor.estado_nuevo.value
	var nuevo_ciudad = document.form_agregar_proveedor.municipio_nuevo.value
	
	// VALIDACIONES NECESARIAS PARA EL FORMULARIO
	if( proveedor == "" && nombre == "" && calle == "" && num_exterior == "" && num_interior == "" && colonia == "" && codigo_postal == "" && estado == 0 && ciudad == 0 && rfc == "" && credito == "" && plazo_pago == "" && pais == 0)
	{
		alert("Los campos se encuentran vacios, ingrese la información correspondiente");
		document.form_agregar_proveedor.proveedor.focus();
		return false;
	}
	else
		if( proveedor == "" )
		{
			alert("Ingrese el nombre del proveedor");
			document.form_agregar_proveedor.proveedor.focus();
			return false;
		}
	else
		if( nombre == "" )
		{
			alert("Ingrese la razón social del proveedor");
			document.form_agregar_proveedor.nombre.focus();
			return false;
		}
	else
		if( calle == "" )
		{
			alert("Ingrese la calle del proveedor a registrar");
			document.form_agregar_proveedor.calle.focus();
			return false;
		}
	else
		if( num_exterior == "" )
		{
			alert("Ingrese el número exterior");
			document.form_agregar_proveedor.num_ext.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_exterior) )
		{
			alert("El valor " + num_exterior + " no es valido, ingrese un número valido");
			document.form_agregar_proveedor.num_ext.focus();
			return false;
		}
	else
		if( colonia == "" )
		{
			alert("Ingrese la colonia");
			document.form_agregar_proveedor.colonia.focus();
			return false;
		}
	else
		if( codigo_postal == "" )
		{
			alert("Ingrese el código postal");
			document.form_agregar_proveedor.codigo_postal.focus();
			return false;
		}
	else
		if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(codigo_postal) )
		{
			alert("El valor " + codigo_postal + " no es valido, ingrese un código postal valido" );
			document.form_agregar_proveedor.codigo_postal.focus();
			return false;
		}
	else
			if(pais == 0 && nuevo_pais == ""){
			alert("Seleccionar el pais");
			document.form_agregar_proveedor.pais.focus();
			return false;
		}

	else
		if( estado == 0 && nuevo_estado == "")
		{
			alert("Seleccione el estado");
			document.form_agregar_proveedor.id_estado.focus();
			return false;
		}
	else
		if( ciudad == 0 && nuevo_ciudad == "")
		{
			alert("Seleccione el municipio");
			document.form_agregar_proveedor.id_municipio.focus();
			return false;
		}
	else
		/*if( rfc == "" )
		{
			alert("Ingrese el Registro Federal de Contribuyentes(RFC)");
			document.form_agregar_proveedor.rfc.focus();
			return false;
		}
	else*/		
		if( credito == "" )
		{
			alert("Ingrese el credito que le otorga el proveedor");
			document.form_agregar_proveedor.credito.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(credito) )
		{
			alert("El valor " + credito + " no es valido ingrese una cantidad valida");
			document.form_agregar_proveedor.credito.focus();
			return false;
		}
	else
		if( plazo_pago  == "" )
		{
			alert("Ingrese el plazo de pago que le otorga el proveedor");
			document.form_agregar_proveedor.plazo_pago.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(plazo_pago) )
		{
			alert("El valor " + plazo_pago + " no es valido ingrese un plazo de pago valido");
			document.form_agregar_proveedor.plazo_pago.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO NUEVO PAGO PROVEEDOR
function validarPagosProveedor()
{
	// DECLARACION DE VARIABLES QUE CONTIENEN LOS VALORES DE LOS INPUT'S DEL FORMULARIO
	var fecha_deposito = document.form_nuevo_pago.fecha_deposito.value
	var cantidad = document.form_nuevo_pago.cantidad.value
	var cheque_transferencia = document.form_nuevo_pago.cheque_transferencia.value
	var referencia = document.form_nuevo_pago.referencia.value
	var observaciones = document.form_nuevo_pago.observaciones.value
	
	// CONDICION QUE VALIDA SI EL FORMULARIO ESTA VACIO
	if( fecha_deposito == "" && cantidad == "" && cheque_transferencia == 0 && referencia == "" && observaciones == "" )
	{
		alert("Los campos estan vacios, porfavor ingrese la información correspondiente");
		document.form_nuevo_pago.fecha_deposito.focus();
		return false;
	}
	// SE VALIDA QUE EL CAMPO DE LA FECHA DE DEPOSITO NO ESTE VACIO
		if( fecha_deposito == "" )
		{
			alert("Ingrese la fecha de deposito");
			document.form_nuevo_pago.fecha_deposito.focus();
			return false;
		}
	else
		if( cantidad == "" )
		{
			alert("Ingrese la cantidad del pago");
			document.form_nuevo_pago.cantidad.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(cantidad) )
		{
			alert("El valor " + cantidad + " no es valido, ingrese una cantidad valida");
			document.form_nuevo_pago.cantidad.focus();
			return false;
		}
	else
		if( cheque_transferencia == 0 )
		{
			alert("Seleccione la forma de pago");
			document.form_nuevo_pago.cheque_transferencia.focus();
			return false;
		}
	else
		if( referencia == "" )
		{
			alert("Ingrese el número de referencia del pago");
			document.form_nuevo_pago.referencia.focus();
			return false;	
		}
	else
		if( !/^([0-9])*$/.test(referencia) )
		{
			alert("El valor " + referencia + " no es valido, ingrese un número de referencia valido");
			document.form_nuevo_pago.referencia.focus();
			return false;
		}
	else
		if( observaciones == "" )
		{
			alert("Ingrese algunas observaciones");
			document.form_nuevo_pago.observaciones.focus();
			return false;
		}
}
/********************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO FACTURAS PROVEEDORES
function validaFacturas()
{
	// VARIABLES DECLARADAS PARA OBTENER LOS VALORES DE LOS CAMPOS DEL FORMULARIO
	var fecha_facturacion = document.form_facturas.fecha_facturacion.value
	var num_folio = document.form_facturas.folio.value
	var cotizacion = document.form_facturas.cotizacion.value
	var facturado = document.form_facturas.facturado.value
	var fecha_recepcion = document.form_facturas.fecha_recepcion.value
	var estatus = document.form_facturas.id_estatus.value
	var descripcion = document.form_facturas.descripcion.value
	
	// VALIDACION PARA SABER SI EL FORMULARIO ESTA VACIO
	if( fecha_facturacion == "" && num_folio == "" && cotizacion == "" && facturado == "" && fecha_recepcion == "" && estatus == 0 && descripcion == 0 )
	{
		alert("Los campos se encuentran vacios, porfavor ingrese la información correspondiente");
		document.form_facturas.fecha_facturacion.focus();
		return false;
	}
	else
		if( fecha_facturacion == "" )
		{
			alert("Ingrese la fecha de facturacion");
			document.form_facturas.fecha_facturacion.focus();
			return false;
		}
	else
		if( num_folio == "" )
		{
			alert("Ingrese un número de folio valido");
			document.form_facturas.folio.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_folio) )
		{
			alert("Ingrese un número de folio valido");
			document.form_facturas.folio.focus();
			return false;
		}
	else
		if( cotizacion == "" )
		{
			alert("Ingrese la cantidad cotizada");
			document.form_facturas.cotizacion.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(cotizacion) )
		{
			alert("Ingrese la cantidad cotizada valida");
			document.form_facturas.cotizacion.focus();
			return false;
		}
	else
		if( facturado == "" )
		{
			alert("Ingrese la cantidad facturada");
			document.form_facturas.facturado.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(facturado) )
		{
			alert("Ingrese la cantidad facturada valida");
			document.form_facturas.facturado.focus();
			return false;
		}
	else
		if( fecha_recepcion == "" )
		{
			alert("Ingrese la fecha de recepcion");
			document.form_facturas.fecha_recepcion.focus();
			return false;
		}
	else
		if( estatus == 0 )
		{
			alert("Seleccione el estatus de la factura");
			document.form_facturas.id_estatus.focus();
			return false;
		}
	else
		if( descripcion == 0)
		{
			alert("Ingrese la descripción para la factura");
			document.form_facturas.descripcion.focus();
			return false;
		}	
}
/*******************************************************************/
// FUNCION QUE VALIDA EL FORMULARIO DE LOS ENVIOS A PROVEEDORES
function validaEnvios()
{
	// VARIABLES QUE CONTIENEN LOS DATOS DE LOS CAMPOS DEL FORMULARIO
	var paqueteria = document.form_envios.id_paqueteria.value
	var num_guia = document.form_envios.num_guia.value
	var empleado = document.form_envios.empleado.value
	var observaciones = document.form_envios.observaciones.value
	var producto = document.form_envios.producto.value
	var cantidad = document.form_envios.cantidad.value

	// SE VALIDA QUE EL FORMULARIO NO ESTE VACIO
	if( paqueteria == 0 && num_guia == "" && empleado == 0 && producto == 0 && cantidad == "" && observaciones == 0)
	{
		alert("Los campos se encuentran vacios, porfavor ingrese la información correspondiente");
		document.form_envios.id_paqueteria.focus();
		return false;
	}
	else
		if( paqueteria == 0 )
		{
			alert("Seleccione la paqueteria");
			document.form_envios.id_paqueteria.focus();
			return false;
		}
	else
		if( num_guia == "" )
		{
			alert("Ingrese el número de guia del envio");
			document.form_envios.num_guia.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(num_guia) )
		{
			alert("El valor" + num_guia + "no es valido, ingrese un número de guia valido");
			document.form_envios.num_guia.focus();
			return false;
		}
	else
		if( empleado == 0 )
		{
			alert("Seleccione a la persona quien envio el paquete");
			document.form_envios.empleado.focus();
			return false;	
		}
	else
		if( observaciones == 0 )
		{
			alert("Ingrese las observaciones correspondientes");
			document.form_envios.observaciones.focus();
			return false;
		}
	else
		if( producto == 0 )
		{
			alert("Seleccione los productos a enviar");
			document.form_envios.producto.focus();
			return false;
		}
	else
		if( cantidad == "" )
		{
			alert("Ingrese la cantidad de productos a enviar");
			document.form_envios.cantidad.focus();
			return false;
		}
	else
		if( !/^([0-9])*$/.test(cantidad) )
		{
			alert("El valor" + cantidad + "no es valido, ingrese una cantidad valida");
			document.form_envios.cantidad.focus();
			return false;
		}
}
/*******************************************************************/
// ARREGLO PARA ESTABLECER EL FORMATO DE LA FECHA A INGRESAR (DD/MM/AAAA)
var patron = new Array(2,2,4)

// FUNCION PARA VALIDAR QUE LA FECHA INGRESADA ES CORRECTA Y ADEMAS IR AGREGANDO LA 
// DIAGONAL(/) AL TERMINAR DE ESCRIBIR EL DIA, MES Y AÑO
function Validar(elem,separador,pat,numerico) {
    if(elem.valoranterior != elem.value) { 
valor = elem.value;
largo = valor.length;
valor = valor.split(separador);
valor2 = "";
 
    for(i=0; i<valor.length; i++) {
        valor2 += valor[i]; 
    }
 
    if(numerico){
        for(j=0; j<valor2.length; j++){
            if(isNaN(valor2.charAt(j))){
                letra = new RegExp(valor2.charAt(j),"g");
                valor2 = valor2.replace(letra,"");
            }
        }
    }
 
valor = "";
valor3 = new Array();
    for(n=0; n<pat.length; n++) {
        valor3[n] = valor2.substring(0,pat[n]);
        valor2 = valor2.substr(pat[n]);
    }
 
    for(q=0; q<valor3.length; q++) {
        if(q == 0) {
            valor = valor3[q];
        }else{
            if(valor3[q] != "") {
                if (valor3[1] > 12 ) {
                    valor = valor3[2];
                } else if (valor3[0] > 31) {
                    valor = valor3[1] + separador + valor3[2];
                }else{
                    valor += separador + valor3[q];
                }
 
            }
        }
    }
 
    elem.value = valor;
    elem.valoranterior = valor;
    }
}
/*******************************************************************/
// FUNCION QUE VALIDA SI LO ESCRITO EN UN CAMPO DE TEXTO ES UN NUMERO
function validarSiNumero(numero)
{
	// EXPRESION REGULAR QUE VALIDA LO INGRESADO EN EL CAMPO DE TEXTO
	if (!/^([0-9])*$/.test(numero))
	alert("El valor " + numero + " no es valido, ingrese un nuevo valor");
}
/*******************************************************************/
// FUNCION PARA VALIDAR EL CODIGO POSTAL INGRESADO
function validarCP(campo)
{
	if( !/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(campo) )
	{
		alert("El valor " + campo + " no es valido, ingrese un código postal valido" );
	}
}
/*******************************************************************/
// FUNCION PARA VALIDAR EL SOLO NUMEROS
function soloNumeros(evt){
		if(window.event){
			keynum=evt.keyCode;		
		}else{
			keynum=evt.keywitch
		}
		if(keynum>47 && keynum<58){
			return true;
		}else{
			return false;
		}
	}
/*******************************************************************/
// FUNCION PARA VALIDAR EL SOLO NUMEROS DECIMAL
function soloNumerosPunto(evt){	
		if(window.event){
			keynum=evt.keyCode;		
		}else{
			keynum=evt.keywitch
		}
		if((keynum>47 && keynum<58) || (keynum==46)){
			return true;
		}else{
			return false;
		}
	}
/*/ FUNCION CON JQUERY QUE OBTIENE LOS PRODUCTOS DE ACUERDO A LA CATEGORIA SELECCIONADO
$(document).ready(function(){
	$('#id_categoria').change(function(){
		$.post('productos.php',{variable:$(this).val()},function(data){
			$('#id_producto option').not(':first').remove();
			$('#id_producto').append(data);
			$('#id_producto').removeAttr('disabled'); 			
		})	
	})
})
/********************************************************************/
/*********************************************************************************/
// FUNCION QUE MUESTRA LAS IMAGENES Y LA INFORMACION CORRESPONDIENTE DEL CATALOGO DE MOLDES
$(document).ready(function(){
	var resultado = 0;
	$('#estilos').change(function(){
		resultado = 0;
		$('#estilos_moldes').empty();
		$.post('imagen_estilos2.php',{id_catalogo:$(this).val()},function(data){
			var estilo = $('#precio_estilo').val(data);
			
			var lado = $('#lado_oido').val();
			
			resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
			
			if(lado == 'bilateral'){
				resultado = resultado * 2;
			}
			
			$('#costo_total_molde').empty();
			$('#costo_total_molde').val(resultado);
		})
	}),
	$('#materiales').change(function(){
		$.post('materiales2.php',{id_material:$('#materiales').val()},function(data){
			$('#precio_material').val(data);
			
			var lado = $('#lado_oido').val();
			
			resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
			
			if(lado == 'bilateral'){
				resultado = resultado * 2;
			}
			
			$('#costo_total_molde').empty();
			$('#costo_total_molde').val(resultado);
		}),
		$.post('ventilaciones.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val()},function(data){
			$('#ventilaciones option').not(':first').remove();
			$('#ventilaciones').append(data);
			$('#ventilaciones').removeAttr('disabled');
		}),
		$.post('colores.php',{id_material:$(this).val()},function(data){
			$('#colores2 option').not(':first').remove();
			$('#colores2').append(data);
			$('#colores2').removeAttr('disabled');
		})
	}),
	$('#ventilaciones').change(function(){
		$.post('ventilaciones2.php',{id_ventilacion:$('#ventilaciones').val()},function(data){
			$('#precio_ventilacion').val(data);
			
			var lado = $('#lado_oido').val();
			
			resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
			if(lado == 'bilateral'){
				resultado = resultado * 2;
			}
			
			$('#costo_total_molde').empty();
			$('#costo_total_molde').val(resultado);
		}),
		$.post('salidas.php',{id_catalogo:$('#estilos').val(),id_material:$('#materiales').val(),id_ventilacion:$(this).val()},function(data){
			$('#salidas option').not(':first').remove();
			$('#salidas').append(data);
			$('#salidas').removeAttr('disabled');
		})
	}),
	$('#salidas').change(function(){
		$.post('imagen_salidas2.php',{id_salida:$('#salidas').val()},function(data){
			$('#precio_salida').val(data);
			
			var lado = $('#lado_oido').val();
			
			resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
			if(lado == 'bilateral'){
				resultado = resultado * 2;
			}
			
			$('#costo_total_molde').empty();
			$('#costo_total_molde').val(resultado);
		});
	}),
	$('#lado_oido').change(function(){
		var lado_odio_seleccionado = $('#lado_oido').val();
		if( lado_odio_seleccionado == 'bilateral' ) 
		{
			$('#costo_total_molde').empty();
			resultado = (Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val())) * 2;
			$('#costo_total_molde').val(resultado);
		}
		else
			if( lado_odio_seleccionado == 'derecho' )
			{
				$('#costo_total_molde').empty();
				resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
				$('#costo_total_molde').val(resultado);
			}
		else
			if( lado_odio_seleccionado == 'izquierdo' )
			{
				$('#costo_total_molde').empty();
				resultado = Number($('#precio_estilo').val()) + Number($('#precio_material').val()) + Number($('#precio_ventilacion').val()) + Number($('#precio_salida').val());
				$('#costo_total_molde').val(resultado);
			}
	}),
	// Validacion para agregar nuevo catalogo a moldes //
	$('.valida_catalogo').submit(function(){
		if($('select[name="id_estilo"]').val() == 0){
			alert("Selecciona el Estilo");
			return false;	
		}
	});
	// Valida los nuevos materiales, ventilaciones y salidas del catalogo molde //
	var submitActor = null;
    var $form = $('.valida_modificar_catalogo' );
    var $submitActors = $form.find( 'input[type=submit]' );  
	$form.submit( function( event ){
		if ( null === submitActor ){
			 // If no actor is explicitly clicked, the browser will
			 // automatically choose the first in source-order
			 // so we do the same here
			submitActor = $submitActors[0];
		}
		if (submitActor.name == 'guardar_nuevo_material') {
			if($('select[name="material_estilo"]').val() == 0){
				alert("Selecciona el nuevo Material");
				return false;	
			}
		};
    }),
	$submitActors.click( function( event )
	{
	submitActor = this;
	});
	// Validar nueva Ventilacion al Catalogo de Molde //
	$('.valida_ventilacion').submit(function(){
		if($('select[name="ventilacion_material"]').val()==0){
			alert("Selecciona la Ventilacion para el Material");
			return false;	
		}
	});	
	// Validar nueva Salida al Catalogo de Molde //
	$('.validar_salidas').submit(function(){
		if($('select[name="salidas_ventilacion"]').val()==0){
			alert("Selecciona la Salida para el Material");
			return false;	
		}
	});
});