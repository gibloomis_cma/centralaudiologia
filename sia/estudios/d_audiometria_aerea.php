<?php
	include('../php/config.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Estudio Audimetrico</title>
  <script type='text/javascript' src='js/jquery-git.js'></script>
  <link rel="stylesheet" type="text/css" href="/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="/css/result-light.css">
<style type="text/css">
	#datos form label{
		text-align:center;
	}
	#respuesta{
		 width:175px; 
		 border-top:1px solid #390;
	}
	img{
/*		position:absolute;*/
	}
	
	.est{
		background-color:#9CC;
	}
	table{
			border:1px solid #069;
		}
	table tr td{
		height:25px !important;
		padding:0 5px;
	}
	.opciones{		
		background-color:#999 !important;	
		line-height:45px; 		
		padding:10px 10px;
		border:#000 solid 1px;
		color:#003;
		font-weight:bold;
	}
	.opciones table{
		border:none;
	}
	.btns{
		background-color:#666;
		color:#FFF;
		font-weight:bold;
		padding:5px;
		border-radius:10px;
		cursor:pointer;	
	}
	.opciones table tr td{
		text-align:center;
		padding:10px;
	}
	.opciones table tr th{
		background-color:#CCC;
		color:#003;
		border:double 2px #FFF		
	}
	.btns2{
		background-color:#003
	}
</style>
<script type='text/javascript'>//<![CDATA[ 
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
					events: {
					click: function(e) {
						// find the clicked values and the series
						var x = e.xAxis[0].value,
							y = e.yAxis[0].value,
							series = this.series[0];
						//Condition 'x'					
						if(x<1 && x>=0){x=0, hz=125}
						if(x<2 && x>=1){x=1, hz=250}
						if(x<3 && x>=2){x=2, hz=500}
						if(x<4 && x>=3){x=3, hz=750}
						if(x<5 && x>=4){x=4, hz=1000}
						if(x<6 && x>=5){x=5, hz=1500}
						if(x<7 && x>=6){x=6, hz=2000}
						if(x<8 && x>=7){x=7, hz=3000}
						if(x<9 && x>=8){x=8, hz=4000}
						if(x<10 && x>=9){x=9, hz=6000}
						if(x>=10){x=10, hz=8000}
						//Condition 'y'		
						if(y<-10){y=-10}		
						if(y>=-10 && y<-5){y=-10}
						if(y>=-5 && y<0){y=-5}
						if(y>=0 && y<5){y=0}
						if(y>=5 && y<10){y=5}
						if(y>=10 && y<15){y=10}
						if(y>=15 && y<20){y=15}
						if(y>=20 && y<25){y=20}
						if(y>=25 && y<30){y=25}
						if(y>=30 && y<35){y=30}
						if(y>=35 && y<40){y=35}
						if(y>=40 && y<45){y=40}
						if(y>=45 && y<50){y=45}
						if(y>=50 && y<55){y=50}
						if(y>=55 && y<60){y=55}
						if(y>=60 && y<65){y=60}
						if(y>=65 && y<70){y=65}
						if(y>=70 && y<75){y=70}
						if(y>=75 && y<80){y=75}
						if(y>=80 && y<85){y=80}
						if(y>=85 && y<90){y=85}
						if(y>=90 && y<95){y=90}
						if(y>=95 && y<100){y=95}
						if(y>=100 && y<105){y=100}
						if(y>=105 && y<110){y=105}
						if(y>=110 && y<115){y=110}
						if(y>=115 && y<120){y=115}
						if(y>=120){y=120}
						// Add it					
						series.addPoint([x, y]);
						<?php
							//include('../php/config.php');
							//$consuta_existe=mysql_query('SELECT * FROM estudios WHERE hz=');
						?>
						document.getElementById('x').value=y;
						document.getElementById('y').value=hz;
						//window.location.href="grafica.php";
						document.form_aerea.submit() 				
						
					}
				},
                renderTo: 'container',
                marginRight: 50,
                marginBottom: 80
            },
            title: {
				style: {
						 color: '#006',
						 font: '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
					},
                text: 'Estudios Audiometricos',
                x: -20 //center
            },
            subtitle: {
				style: {
					 color: '#229',
					 font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
				  },
                text: 'Central de Audiología Morelia',
                x: -20
            },
            xAxis: {
				title: {
					text: 'Hz',
					style: {
						color: '#222',
						font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
					 }},
                categories: ['125','250', '500', '750', '1000', '1500','2000', '3000','4000', '6000', '8000'],
				plotBands: [
							{from: (0-0.02),to: (0+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (1-0.02),to: (1+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (2-0.02),to: (2+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (3-0.02),to: (3+0.02),color: 'rgba(200, 200, 200, .2)'},
							{from: (4-0.02),to: (4+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (5-0.02),to: (5+0.02),color: 'rgba(200, 200, 200, .2)'},
							{from: (6-0.02),to: (6+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (7-0.02),to: (7+0.02),color: 'rgba(200, 200, 200, .2)'},
							{from: (8-0.02),to: (8+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (9-0.02),to: (9+0.02),color: 'rgba(200, 200, 200, .2)'},
							{from: (10-0.02),to: (10+0.02),color: 'rgba(200, 200, 200, .9)'},
							{from: (11-0.02),to: (11+0.02),color: 'rgba(200, 200, 200, .9)'}
							]
            },
			yAxis: {			
				min :-15,	
				max:125,	
				tickInterval :10,
				tickLength : 5,
				reversed: true,
				title: {
					text: 'Db',
					style: {
						color: '#222',
						font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
					 }
			
			},
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+ this.y +'Db '+this.x+'Hz';
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'buttom',
                x: 0,
                y: 450,				
                borderWidth: 1,
				enabled:false
            },
            series: [{		
				//SERIES AUDIOMETRICA AEREA		
                name: 'Aérea',				
				color: '#F00',
				type: 'scatter',									
                data:[
						<?php //Primera columna
							for($x=125;$x<=8000;$x=$x+$x){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								if($y=="" and $x<8000){?>
						null,
						<?php
								}elseif($y!="" and $respuesta==""){
						?>
						{y: <?php echo $y; ?>, marker:{ symbol:'url(img_sim/D_Aerea_<?php echo $forma.$respuesta; ?>.png)'}},
						<?php
								}else{
						?>
						null,
						<?php	
								}	
								//Consulta los puntos secundarios						
								if($x==500 or $x==1000 or $x==2000 or $x==4000){	
									$x2=($x/2)+$x;									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2==""){									
							?>
						null,
						<?php
								}elseif($y2!="" and $respuesta2==""){
						?>
						{y: <?php echo $y2; ?>, marker:{ symbol:'url(img_sim/D_Aerea_<?php echo $forma2.$respuesta2; ?>.png)'}},
						<?php
								}else{
							
						?>
						null,
						<?php	
								}
						
							}
					
							}
						
						?>													
					]
            },{
				name: 'Aérea',
				color: '#F00',
				type: 'line',
				data:[
						<?php
							//Segunda columna
							$n=11;
							for($x=8000;$x>=125;$x=($x/2)){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								//Consulta los puntos secundarios	
								if($x==500 or $x==1000 or $x==2000 or $x==4000){
									$n--;
									$x2=$x+($x/2);									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2!="" and $respuesta2==""){
						?>
						[<?php echo $n.",".$y2; ?>],		
						<?php
									}elseif($respuesta2!=""){
						?>
						[<?php echo $n; ?>,null],
						<?php
									}	
								}	
								$n--;	
								if($y!="" and $respuesta==""){
									
						?>
						[<?php echo $n.",".$y; ?>],
						<?php
								}elseif($respuesta!=""){
						?>
						[<?php echo $n; ?>,null],
						<?php		
								}
							}
						?>
					],
				marker: { enabled: false } 
			},{
				name: 'Aérea',
				color: '#F00',
				type: 'scatter',
				data:[<?php //Primera columna
							for($x=125;$x<=8000;$x=$x+$x){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								if($y==""){?>
						null,
						<?php
								}elseif($y!="" and $respuesta!=""){
						?>
						{y: <?php echo $y; ?>, marker:{ symbol:'url(img_sim/D_Aerea_<?php echo $forma.$respuesta; ?>.png)'}},
						<?php
								}else{
						?>
						null,
						<?php	
								}	
								//Consulta los puntos secundarios						
								if($x==500 or $x==1000 or $x==2000 or $x==4000){	
									$x2=($x/2)+$x;									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2==""){									
							?>
						null,
						<?php
								}elseif($y2!="" and $respuesta2!=""){
						?>
						{y: <?php echo $y2; ?>, marker:{ symbol:'url(img_sim/D_Aerea_<?php echo $forma2.$respuesta2; ?>.png)'}},
						<?php
								}else{
							
						?>
						null,
						<?php	
								}
						
							}
							
							}	
							?>
					]
			
			},{
				//SERIES AUDIOMETRIA OSEA
                name: 'Oséa',
				type: 'scatter',
				color: '#F00',				
                data:[
						<?php //Primera columna
							for($x=125;$x<=8000;$x=$x+$x){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								if($y=="" and $x<8000){?>
						null,
						<?php
								}elseif($y!="" and $respuesta==""){
						?>
						{y: <?php echo $y; ?>, marker:{ symbol:'url(img_sim/D_Osea_<?php echo $forma.$respuesta; ?>.png)'}},
						<?php
								}else{
						?>
						null,
						<?php	
								}	
								//Consulta los puntos secundarios						
								if($x==500 or $x==1000 or $x==2000 or $x==4000){	
									$x2=($x/2)+$x;									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2==""){									
							?>
						null,
						<?php
								}elseif($y2!="" and $respuesta2==""){
						?>
						{y: <?php echo $y2; ?>, marker:{ symbol:'url(img_sim/D_Osea_<?php echo $forma2.$respuesta2; ?>.png)'}},
						<?php
								}else{
							
						?>
						null,
						<?php	
								}
						
							}
					
							}						
						?>
					],
         		enableMouseTracking: false
            },{
				name: 'Oséa',
				type: 'line',
                dashStyle: 'LongDash',
				color: '#F00',				
                data:[
						<?php //Segunda columna
							$n=11;
							for($x=8000;$x>=125;$x=($x/2)){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								//Consulta los puntos secundarios	
								if($x==500 or $x==1000 or $x==2000 or $x==4000){
									$n--;
									$x2=$x+($x/2);									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2!="" and $respuesta2==""){
						?>
						[<?php echo $n.",".$y2; ?>],		
						<?php
									}elseif($respuesta2!=""){
						?>
						[<?php echo $n; ?>,null],
						<?php
									}	
								}	
								$n--;	
								if($y!="" and $respuesta==""){
									
						?>
						[<?php echo $n.",".$y; ?>],
						<?php
								}elseif($respuesta!=""){
						?>
						[<?php echo $n; ?>,null],
						<?php		
								}
							}
						?>
					],
         		enableMouseTracking: false,
				marker: { enabled: false } 
			},{
				name: 'Oséa',
				color: '#F00',
				type: 'scatter',
				data:[<?php //Primera columna
							for($x=125;$x<=8000;$x=$x+$x){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								if($y==""){?>
						null,
						<?php
								}elseif($y!="" and $respuesta!=""){
						?>
						{y: <?php echo $y; ?>, marker:{ symbol:'url(img_sim/D_Osea_<?php echo $forma.$respuesta; ?>.png)'}},
						<?php
								}else{
						?>
						null,
						<?php	
								}	
								//Consulta los puntos secundarios						
								if($x==500 or $x==1000 or $x==2000 or $x==4000){	
									$x2=($x/2)+$x;									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2==""){									
							?>
						null,
						<?php
								}elseif($y2!="" and $respuesta2!=""){
						?>
						{y: <?php echo $y2; ?>, marker:{ symbol:'url(img_sim/D_Osea_<?php echo $forma2.$respuesta2; ?>.png)'}},
						<?php
								}else{
							
						?>
						null,
						<?php	
								}
						
							}
							
							}	
							?>
					],
				enableMouseTracking: false	
			},{
				//SERIES AUDIOMETRIA UMBRAL DE INCONFORT
                name: 'Umbral de Inconfort',
				type: 'scatter',
				color: '#F00',							
                data:[
						<?php //Primera columna
							for($x=125;$x<=8000;$x=$x+$x){
								//Consulta los puntos primarios
								$aerea=mysql_query('SELECT * FROM estudios WHERE tipo="Umbral" AND oido="D" AND hz='.$x)or die(mysql_error());
								$row_aerea=mysql_fetch_array($aerea);
								$forma=$row_aerea['forma'];
								$respuesta=$row_aerea['respuesta'];
								$y=$row_aerea['db'];
								if($respuesta!=""){
									$respuesta="_".$respuesta;
								}else{
									$respuesta=="";
								}
								if($y=="" and $x<8000){?>
						null,
						<?php
								}elseif($y!=""){
						?>
						{y: <?php echo $y; ?>, marker:{ symbol:'url(img_sim/D_Umbral<?php echo $respuesta; ?>.png)'}},
						<?php
								}else{
						?>
						null,
						<?php	
								}	
								//Consulta los puntos secundarios						
								if($x==500 or $x==1000 or $x==2000 or $x==4000){	
									$x2=($x/2)+$x;									
									$aerea2=mysql_query('SELECT * FROM estudios WHERE tipo="Umbral" AND oido="D" AND hz='.$x2)or die(mysql_error());
									$row_aerea2=mysql_fetch_array($aerea2);
									$forma2=$row_aerea2['forma'];
									$respuesta2=$row_aerea2['respuesta'];
									$y2=$row_aerea2['db'];
									if($respuesta2!=""){
										$respuesta2="_".$respuesta2;
									}else{
										$respuesta2=="";
									}
									if($y2==""){									
							?>
						null,
						<?php
								}elseif($y2!=""){
						?>
						{y: <?php echo $y2; ?>, marker:{ symbol:'url(img_sim/D_Umbral<?php echo $respuesta2; ?>.png)'}},
						<?php
								}else{
							
						?>
						null,
						<?php	
								}
						
							}
					
							}													
						?>												
					],
         		enableMouseTracking: false
            }]
        });
    });
});
//]]>  
</script>
</head>
<body>
<script src="js/highcharts.js"></script>
<script src="js/sexporting.js"></script>
<table>
	<tr>
    	<td><div id="container" style="width: 750px; height: 500px;"></div></td>
        <td valign="middle" align="center" style="padding-right:50px;">
        	<form name="form_aerea" method="post" action="procesa_d_audiometria.php">
            <div class="opciones">
                <input name="id_paciente" value="<?php echo $id_paciente; ?>" type="hidden" />
                <input name="folio" value="1" type="hidden" />
                <table>
                    <tr>
                        <th colspan="3">AUDIOMETRIA:</th>
                    </tr><tr>                	
                        <?php
                            if(isset($_GET['tipo'])){
                                if($_GET['tipo']==2){
                        ?>
                        <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />
                        <?php
                                }
                                if($_GET['tipo']==1){
                        ?>
                        <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />			
                        <?php
                                }
                                if($_GET['tipo']==3){
                        ?>
                        <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />			
                        <?php
                                }
                            }else{
                        ?>
                        <input type="hidden" value="1" name="tipo_e" />            
                        <?php
                            }
                        ?>
                        <td>
                            <input name="tipo" type="button" value="Aérea" class="btns btns2" />
                        </td><td>
                            <input name="tipo" type="button" value="Ósea" class="btns"
                            onclick="location.href='d_audiometria_osea.php?tipo=2'" />
                        </td><td>
                            <input name="tipo" type="button" value="Umbral de Inconfort" class="btns"
                            onclick="location.href='d_audiometria_umbral.php?tipo=3'" />
                        </td>           
                    </tr><tr>
                        <th colspan="3">OPCIONES:</th>
                    </tr><tr>
                        <td colspan="3">
                        <?php
                            if(isset($_GET['enma'])){
                                if($_GET['enma']=="Enmascarado"){
                        ?>
                        <input name="forma" type="radio" value="Sin Enmascarar"/>Sin Enmascarar &nbsp
                        <input name="forma" type="radio" value="Enmascarado" checked="checked" />Enmascarado
                         <?php
                                }else{
                        ?>
                        <input name="forma" type="radio" value="Sin Enmascarar" checked="checked"/>Sin Enmascarar &nbsp;
                        <input name="forma" type="radio" value="Enmascarado" />Enmascarado
                                 
                        <?php	
                                }
                            }else{
                        ?>
                        <input name="forma" type="radio" value="Sin Enmascarar" checked="checked"/>Sin Enmascarar &nbsp;
                        <input name="forma" type="radio" value="Enmascarado" />Enmascarado                       
                        <?php
                            }
                        ?>
                    </td>
                </tr><tr>
                    <td colspan="3">
                    	<?php
							if(isset($_GET['respuesta'])){
								if($_GET['respuesta']==1){
									$ck="";
								}else{
									$ck="checked";
								}
							}else{
								$ck="";
							}
						?>
                        <input name="respuesta" type="checkbox" <?php echo $ck; ?> value="Sin Respuesta" />Sin Respuesta   
                    </td>            
                </tr><tr>
                    <td colspan="3">
                        <label for="x">Db: </label><input type="text" name="db" id="x" size="4" readonly="readonly" style="text-align:center"> 
                        <label for="y">Hz: </label><input type="text" name="hz" id="y" size="4" readonly="readonly" style="text-align:center"><br />
                                     
                    </td>
                </tr><tr>
                    <td colspan="3">
                        <input name="tipo" type="hidden" value="Aerea">
                        <input name="oido" type="hidden" value="D" />
                        <input type="button" name="sel_oido" value="Oido Izquierdo"
                        onclick="window.location.href='i_audiometria.php'" class="btns" /> 
                        <input type="button" name="guardar" value="Terminar" class="btns"
                        onclick="window.location.href='d_audiometria.php'" /> 
                    </td>
                </tr>
            </table>
            </div>        
			</form>
		</td>
	</tr>
</table>
<table style="margin-left:60px; background:#999; color:#003">
	<tr>
    	<?php
			for($h=125;$h<=8000;$h=$h+$h){
		?>
        <td style="text-align:center; font-size:12px;">
			<?php					
                //BUSQUEDA DE PUNTO AEREO
                $c_aerea=mysql_query('SELECT db FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$h)or die(mysql_error());
                $row_c_aerea=mysql_fetch_array($c_aerea);
                $db_aerea=$row_c_aerea['db'];
                if($db_aerea==""){
                  $db_aerea="--";
                }
                echo "[".$db_aerea."]";
            ?>            
        </td>
        <?php
				if($h==500 or $h==1000 or $h==2000 or $h==4000){
					if($h== 500){$h2= 750;}
					if($h==1000){$h2=1500;}
					if($h==2000){$h2=3000;}
					if($h==4000){$h2=6000;}					
					//BUSQUEDA DE PUNTO AEREO
					$c_aerea=mysql_query('SELECT db FROM estudios WHERE tipo="Aerea" AND oido="D" AND hz='.$h2)or die(mysql_error());
					$row_c_aerea=mysql_fetch_array($c_aerea);
					$db_aerea=$row_c_aerea['db'];
					if($db_aerea==""){
					  $db_aerea="--";
					}
		?>
        <td style="text-align:center; font-size:12px;">
        	<?php echo "[".$db_aerea."]"; ?>
        </td>
        <?php
				}
			}
		?>
    </tr><tr>
    	<?php
			for($h=125;$h<=8000;$h=$h+$h){
		?>
        <td style="text-align:center">
        	<select name="<?php echo $h; ?>">
            	<option value="--">--</option>
            	<?php
					//LIMITACIONES CON LA OSEA
					$c_osea=mysql_query('SELECT db FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$h)or die(mysql_error());
					$row_c_osea=mysql_fetch_array($c_osea);
					$db_osea=$row_c_osea['db'];
					if($db_osea==""){
						$db_osea=-10;
					}
					//LIMITACIONES CON EL UMBRAL DE INCONFORT
					$c_umbral=mysql_query('SELECT db FROM estudios WHERE tipo="Umbral" AND oido="D" AND hz='.$h)or die(mysql_error());
					$row_c_umbral=mysql_fetch_array($c_umbral);
					$db_umbral=$row_c_umbral['db'];
					if($db_umbral==""){
						$db_umbral=120;
					}
					//CICLO DECIBELES PUNTOS PRINCIPALES
					for($d=$db_osea;$d<=$db_umbral;$d=$d+5){			
				?>
            	<option value="<?php echo $d; ?>">
                	<?php echo $d; ?>
                </option>
                <?php
					}
				?>
            </select>
        </td>
        <?php
				if($h==500 or $h==1000 or $h==2000 or $h==4000){
					if($h== 500){$h2= 750;}
					if($h==1000){$h2=1500;}
					if($h==2000){$h2=3000;}
					if($h==4000){$h2=6000;}
					//LIMITACIONES CON LA OSEA
					$c_osea=mysql_query('SELECT db FROM estudios WHERE tipo="Osea" AND oido="D" AND hz='.$h2)or die(mysql_error());
					$row_c_osea=mysql_fetch_array($c_osea);
					$db_osea=$row_c_osea['db'];
					if($db_osea==""){
						$db_osea=-10;
					}
					//LIMITACIONES CON EL UMBRAL DE INCONFORT
					$c_umbral=mysql_query('SELECT db FROM estudios WHERE tipo="Umbral" AND oido="D" AND hz='.$h2)or die(mysql_error());
					$row_c_umbral=mysql_fetch_array($c_umbral);
					$db_umbral=$row_c_umbral['db'];
					if($db_umbral==""){
						$db_umbral=120;
					}
		?>
        <td style="text-align:center">
        	<select name="db<?php echo $h2; ?>">
            	<option value="--">--</option>
				<?php
					for($d2=$db_osea;$d2<=$db_umbral;$d2=$d2+5){
				?>
            	<option value="<?php echo $d2; ?>">
                	<?php echo $d2; ?>
                </option>
                <?php
					}
				?>
            </select>
        </td>
        <?php
				}
			}
		?>
    </tr><tr>
    	<?php
			for($h=125;$h<=8000;$h=$h+$h){
		?>
        <td style="text-align:center; font-size:12px;">
        	<?php echo $h; ?>
        </td>
        <?php
				if($h==500 or $h==1000 or $h==2000 or $h==4000){
					if($h==500){$h2=750;}
					if($h==1000){$h2=1500;}
					if($h==2000){$h2=3000;}
					if($h==4000){$h2=6000;}
		?>
        <td style="text-align:center;font-size:12px;">
        	<?php echo $h2; ?>
        </td>
        <?php
				}
			}
		?>
    </tr><tr>
    	<td colspan="12">
        	<hr>
        </td>
    </tr><tr>
    	<td colspan="12" style="text-align:center">
        	<input type="button" name="guardar" value="Guardar" class="btns"
            onclick="window.location.href='d_audiometria.php'" /> 
        </td>
    </tr>
</table>
</body>
</html>