<?php
	include('../php/config.php');
	$tipo_e=$_POST['tipo_e'];
	$tipo = $_POST['tipo']; 
	$forma = $_POST['forma'];
	$oido=$_POST['oido'];
	$db = $_POST['db'];
	$hz = $_POST['hz'];	
	$id_paciente=$_POST['id_paciente'];
	$folio=$_POST['folio'];
	
	//No aceptar vacios
	if($hz=="" || $db==""){
?>
<script>
	window.location.href="audiometria_i.php?tipo=<?php echo $tipo_e."&oido=".$oido."&enma=".$forma; ?>";
</script>
<?php
	exit;		
	}
	
	//Limitaciones Audiometria Aerea
	$error_a="";
	if($tipo_e==1){
		$consulta_arriba_aerea=mysql_query('SELECT id_registro 
											FROM estudios 
											WHERE tipo = "Osea"
											 AND hz='.$hz.
											' AND db > '.$db.
											' AND oido="'.$oido.'"')
											or die(mysql_error());
		$row_consulta_arriba_aerea=mysql_fetch_array($consulta_arriba_aerea);
		$id_reg=$row_consulta_arriba_aerea['id_registro'];
		if($id_reg!=""){
			$error_a=$error_a."[Aerea<Osea]";
		}
		$consulta_abajo_aerea=mysql_query('SELECT id_registro 
											FROM estudios 
											WHERE tipo = "Umbral"
											 AND hz='.$hz.
											' AND db < '.$db.
											' AND oido="'.$oido.'"')
											or die(mysql_error());
		$row_consulta_abajo_aerea=mysql_fetch_array($consulta_abajo_aerea);
		$id_reg=$row_consulta_abajo_aerea['id_registro'];
		if($id_reg!=""){
			$error_a=$error_a."[Aerea>Umbral]";
		}
	}
	if($hz==125 && $db>=95 && $tipo=="Aerea"){		
		$error_a=$error_a."[Aerea==125hz|Aerea>=95db]";
	}
	if($hz==8000 && $db>=105 && $tipo=="Aerea"){
		$error_a=$error_a."[Aerea==8000hz|Aerea>=105db]";
	}
	if($error_a!=""){
?>
<script>
	window.location.href="audiometria_i.php?tipo=<?php echo $tipo_e."&oido=".$oido."&enma=".$forma."&error=".$error_a; ?>";
</script>
<?php
		exit;		
	}
	//Limitaciones Audiometria Osea
	$error_o="";
	if($tipo_e==2){
		$consulta_debajo_aerea=mysql_query('SELECT id_registro 
											FROM estudios 
											WHERE tipo = "Aerea"
											 AND hz='.$hz.
											' AND db < '.$db.
											' AND oido="'.$oido.'"')
											or die(mysql_error());
		$row_consulta_debajo_aerea=mysql_fetch_array($consulta_debajo_aerea);
		$id_reg=$row_consulta_debajo_aerea['id_registro'];
		if($id_reg!=""){
			$error_o=$error_o."[Osea<Aerea]";
		}
	}
	if($hz==125 && $tipo=="Osea"){
		$error_o=$error_o."[Osea==125hz]";
	}
	if($hz==8000 && $tipo=="Osea"){
		$error_o=$error_o."[Osea==8000hz]";
	}
	if($db>=75 && $tipo=="Osea"){
		$error_o=$error_o."[Osea>=75db]";
	}		
	if($error_o!=""){
?>
<script>
	window.location.href="audiometria_i.php?tipo=<?php echo $tipo_e."&oido=".$oido."&enma=".$forma."&error=".$error_o; ?>";
</script>
<?php
		exit;		
	}
	//Limitaciones Audiometria Umbral de Inconfort
	$error_u="";
	if($tipo_e==3){
		$consulta_arriba_aerea=mysql_query('SELECT id_registro 
											FROM estudios 
											WHERE tipo = "Aerea"
											 AND hz='.$hz.
											' AND db > '.$db.
											' AND oido="'.$oido.'"')
											or die(mysql_error());
		$row_consulta_arriba_aerea=mysql_fetch_array($consulta_arriba_aerea);
		$id_reg=$row_consulta_arriba_aerea['id_registro'];
		if($id_reg!=""){
			$error_u=$error_u."[Umbral>Aerea]";
		}
	}
	if($hz==125 && $db>=95 && $tipo=="Umbral"){		
		$error_u=$error_u."[Umbral==125hz|Umbral>=95db]";
	}
	if($hz==8000 && $db>=105 && $tipo=="Umbral"){
		$error_u=$error_u."[Umbral==8000hz|Umbral>=105db]";
	}
	if($error_u!=""){
?>
<script>
	window.location.href="audiometria_i.php?tipo=<?php echo $tipo_e."&oido=".$oido."&enma=".$forma."&error=".$error_u; ?>";
</script>
<?php
		exit;		
	}
	
	//Asigna Nulo O Sin respuesta
	if(!empty($_REQUEST['respuesta'])){
		$respuesta = $_REQUEST['respuesta'];		
	}else{
		$respuesta = "";	
	}	
	
	//Consulta punto existente
	$consulta_existe=mysql_query('SELECT id_registro 
								FROM estudios
								WHERE tipo="'.$tipo.'" 
								AND db='.$db.' 
								AND hz='.$hz.' 
								AND oido="'.$oido.'" 
								AND forma="'.$forma.'"
								AND respuesta="'.$respuesta.'"
								AND id_paciente='.$id_paciente)
								or die(mysql_error());
	$row_consulta_existe=mysql_fetch_array($consulta_existe);
	$id_registro=$row_consulta_existe['id_registro'];
	
	//Si existe Elimina el registro
	$accion="";
	if($id_registro!=""){
		mysql_query('DELETE FROM estudios WHERE id_registro='.$id_registro)or die(mysql_error());	
		$accion=$accion."Delete";	
		
	//Si no Consulta nuevamente ahora sin Decibeles
	}else{	
		$consulta_existe=mysql_query('SELECT id_registro 
									FROM estudios
									WHERE tipo="'.$tipo.'" 
									AND hz='.$hz.' 
									AND oido="'.$oido.'" 									
									AND id_paciente='.$id_paciente)
									or die(mysql_error());
		$row_consulta_existe=mysql_fetch_array($consulta_existe);
		$id_registro=$row_consulta_existe['id_registro'];	
			
		//Si existe Actualiza el registro
		if($id_registro!=""){
			mysql_query('UPDATE estudios SET db='.$db.', 
						respuesta="'.$respuesta.'", 
						forma="'.$forma.'"  
						WHERE id_registro='.$id_registro)
						or die(mysql_error());
			$accion=$accion."Update";	
			
		//Si no Inserta el nuevo registro
		}else{
			mysql_query('INSERT INTO estudios (tipo, forma, respuesta, db, hz, id_paciente, id_estudio, oido)
						VALUES("'.$tipo.'","'.$forma.'","'.$respuesta.'",'.$db.','.$hz.','.$id_paciente.','.$folio.',"'.$oido.'")')
						or die(mysql_error());
			$accion=$accion."Insert";	
		}
	}	

	if($tipo_e==""){
		$tipo_e=1;
	}	
?>
<script>
	window.location.href="audiometria_i.php?tipo=<?php echo $tipo_e."&oido=".$oido."&enma=".$forma."&accion=".$accion; ?>";
</script>