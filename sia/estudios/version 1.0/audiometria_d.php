<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Estudios | Audiometria (D)</title>
<script type="text/javascript">
<?php
for($i=0;$i<=130;$i=$i+5){
	for($j=125;$j<=8000;$j=$j+$j){
		$identificador = "dbhz_".$i."_".$j;
		$n = $i;
		if($i == 130){
			$n = -10;
		}
		if($i == 125){
			$n = -5;
		}
?>
function <?php echo $identificador; ?>(event){
	document.getElementById('x').value = <?php echo $n; ?>;
	document.getElementById('y').value = <?php echo $j; ?>;
}
<?php
		if($j==500 or $j==1000 or $j==2000 or $j==4000){
			if($j==500){
				$k = 750;	
			}
			if($j==1000){
				$k = $k*2;	
			}
			if($j==2000){
				$k = $k*2;	
			}
			if($j==4000){
				$k = $k*2;	
			}
			$identificador = "dbhz_".$i."_".$k;
?>
function <?php echo $identificador; ?>(event){
	document.getElementById('x').value = <?php echo $n; ?>;
	document.getElementById('y').value = <?php echo $k; ?>;
}
<?php
		}
	}
}
?>
</script>
<style type="text/css">
	#point_default{
		width:70px !important;		
		text-align:center;
	}
	#point_numeric{
		color:#000 !important;
		font-size:12px;
		font-weight:bold;
		text-align:center;		
	}
	#point_medio{
		width:60px !important;		
		text-align:center;
	}
	
	
	#point_default2{
		width:70px !important;
		background-image:url(img_sim/back_default.png);
		background-position:center;
		text-align:center;
	}
	#point_numeric1{
		color:#000 !important;
		font-size:12px;
		font-weight:bold;
		text-align:center;		
	}
	#point_numeric2{
		color:#000 !important;
		font-size:12px;
		font-weight:bold;
		text-align:center;		
	}
	#point_medio2{
		width:60px !important;
		background-image:url(img_sim/back_medio.png);
		background-position:center;
		text-align:center;
	}
	
	#point_medio:hover, #point_default:hover{
		cursor:pointer;
	}
	#grafica{
						
	}
	#datos{
 		float:left; 
		line-height:45px; 		
		padding:10px 10px;
		width:300px;

	}
	#datos form label{
		text-align:center;
	}
	#respuesta{
		 width:175px; 
		 border-top:1px solid #390;
	}
	img{
/*		position:absolute;*/
	}
	
	.est{
		background-color:#9CC;
	}
	table{
			border:1px solid #069;
		}
	table tr td{
		height:25px !important;
		padding:0 5px;
	}
</style>
</head>

<body>
<div id="contenido_estudios" style="height:455px;">
	<?php
		include('../php/config.php');
		if(isset($_GET['tipo'])){
			$tipo_e=$_GET['tipo'];
			if($tipo_e==2){
				$tipo="Osea";
				$position_o=" position:absolute; z-index:3";
				$position_a=" position:absolute; z-index:2";
				$position_u=" position:absolute; z-index:1";								
			}
			if($tipo_e==1){
				$tipo="Aerea";
				$position_o=" position:absolute; z-index:1";
				$position_a=" position:absolute; z-index:3";
				$position_u=" position:absolute; z-index:2";								
			}
			if($tipo_e==3){
				$tipo="Umbral";
				$position_o=" position:absolute; z-index:2";
				$position_a=" position:absolute; z-index:1";
				$position_u=" position:absolute; z-index:3";												
			}
		}else{
			$tipo="Aerea";
			$position_o=" position:absolute; z-index:2";
			$position_a=" position:absolute; z-index:3";
			$position_u=" position:absolute; z-index:1";											
			}
		if(isset($_GET['oido'])){
			$oido="D";
		}else{
			$oido="D";			
		}
        
        $paciente=mysql_query('SELECT * FROM ficha_tecnica')or die(mysql_error());
        $row_paciente=mysql_fetch_array($paciente);
        $id_paciente=$row_paciente['id_paciente'];
        $nombre=$row_paciente['nombre'];
        $paterno=$row_paciente['paterno'];
        $materno=$row_paciente['materno'];						
        $genero=$row_paciente['genero'];			
    ?>
	<h3>Estudio Audiométrico: </h3><br />
    <div style="float:left" class="grafica_cnt" id="grafica">
    
        <!--ESTUDIO AEREO-->
        <div style="float:left;<?php echo $position_a; ?>; background:none !important" id="grafica" class="Aerea">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Aerea"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Aerea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Aerea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Aerea"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Aerea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Aerea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--ESTUDIO OSEA-->
        <div style="float:left;<?php echo $position_o; ?>; background:none !important" id="grafica" class="Osea">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Osea"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Osea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Osea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Osea"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Osea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Osea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--ESTUDIO UMBRAL DE INCONFORT-->
        <div style="float:left;<?php echo $position_u; ?>; background:none !important" id="grafica" class="Umbral">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Umbral"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Umbral_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Umbral.png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Umbral"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Umbral_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Umbral.png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;" 
                            onclick="document.opciones.submit()">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--LINEAS-->
        <div style="float:left; position:absolute; z-index:0 background:none !important" id="grafica" class="Umbral">
        <table id="grafica">
            <?php
                $punto2="";
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric1">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }                            																
                        ?>
                        <td id="point_default2" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;" 
                            onclick="document.opciones.submit()">                        
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }                                
                        ?>
                        <td id="point_medio2" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;" 
                            onclick="document.opciones.submit()">                        
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric2" >
                            <?php echo $punto2; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric2" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto2; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
    </div>
    <div id="datos" style="margin-top:-25px; float:right;">
        <b>Paciente: </b><?php echo $nombre." ".$paterno." ".$materno; ?><br />
        <b>Genero: </b><?php echo $genero; ?>
        <form action="procesa_audiometria_d.php" method="post" name="opciones">
            <input name="id_paciente" value="<?php echo $id_paciente; ?>" type="hidden" />
            <input name="folio" value="1" type="hidden" />
            <label><b>TIPO:</b> </label><br />
            <?php
                if(isset($_GET['tipo'])){
                    if($_GET['tipo']==2){
            ?>
            <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />
            <input name="tipo" type="radio" value="Aerea" 
            onclick="location.href='audiometria_d.php?tipo=1'" />Aerea<br />
            <input name="tipo" type="radio" value="Osea" checked="checked" 
            onclick="location.href='audiometria_d.php?tipo=2'" />Osea<br />
            <input name="tipo" type="radio" value="Umbral"
            onclick="location.href='audiometria_d.php?tipo=3'" />Umbral de Inconfort<br />
            <?php
                    }
                    if($_GET['tipo']==1){
            ?>
            <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />
            <input name="tipo" type="radio" value="Aerea" checked="checked"
            onclick="location.href='audiometria_d.php?tipo=1'" />Aerea<br />
           <input name="tipo" type="radio" value="Osea" 
            onclick="location.href='audiometria_d.php?tipo=2'" />Osea<br />
            <input name="tipo" type="radio" value="Umbral"
            onclick="location.href='audiometria_d.php?tipo=3'" />Umbral de Inconfort<br />
            <?php
                    }
                    if($_GET['tipo']==3){
            ?>
            <input type="hidden" value="<?php echo $_GET['tipo']; ?>" name="tipo_e" />
            <input name="tipo" type="radio" value="Aerea" 
            onclick="location.href='audiometria_d.php?tipo=1'" />Aerea<br />
           <input name="tipo" type="radio" value="Osea" 
            onclick="location.href='audiometria_d.php?tipo=2'" />Osea<br />
            <input name="tipo" type="radio" value="Umbral" checked="checked"
            onclick="location.href='audiometria_d.php?tipo=3'" />Umbral de Inconfort<br />
            <?php
                    }
            }else{
        ?>
        <input type="hidden" value="1" name="tipo_e" />
        <input name="tipo" type="radio" value="Aerea" checked="checked"
        onclick="location.href='audiometria_d.php?tipo=1'" />Aerea<br />
        <input name="tipo" type="radio" value="Osea" 
            onclick="location.href='audiometria_d.php?tipo=2'" />Osea<br />
            <input name="tipo" type="radio" value="Umbral" 
            onclick="location.href='audiometria_d.php?tipo=3'" />Umbral de Inconfort<br />
        <?php
            }
        ?>
        <label><b>FORMA:</b> </label><br />
        <?php
            if(isset($_GET['enma'])){
                if($_GET['enma']=="Enmascarado"){
        ?>
        <input name="forma" type="radio" value="Enmascarado" checked="checked" />Enmascarado &nbsp;
        <input name="forma" type="radio" value="Sin Enmascarar"/>Sin Enmascarar<br /> 
         <?php
                }else{
        ?>
        <input name="forma" type="radio" value="Enmascarado" />Enmascarado &nbsp;
        <input name="forma" type="radio" value="Sin Enmascarar" checked="checked"/>Sin Enmascarar<br />             
        <?php	
                }
            }else{
        ?>
        <input name="forma" type="radio" value="Enmascarado" />Enmascarado &nbsp;
        <input name="forma" type="radio" value="Sin Enmascarar" checked="checked"/>Sin Enmascarar<br />                         
        <?php
            }
        ?>
        <input name="oido" type="hidden" value="D" /> 
        <div id="respuesta">           
        <input name="respuesta" type="checkbox" value="Sin Respuesta" />Sin Respuesta   
        </div>
        
        <label for="x">Db: </label><input type="text" name="db" id="x" size="4" readonly="readonly" style="text-align:center"> 
        <label for="y">Hz: </label><input type="text" name="hz" id="y" size="4" readonly="readonly" style="text-align:center"><br />
        <input type="button" name="sel_oido" value="Oido Izquierdo"
        onclick="window.location.href='audiometria_i.php'" /> 
    	</form>
	</div>
</div>
</body>
</html>