<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Estudios | Audiometria (I)</title>
<?php
	include('coordenadas.php');
?>
</head>

<body>
<div id="contenido_estudios" style="height:455px;">
	<?php
		include('../php/config.php');
		if(isset($_GET['tipo'])){
			$tipo_e=$_GET['tipo'];
			if($tipo_e==2){
				$tipo="Osea";
				$position_o=" position:absolute; z-index:3";
				$position_a=" position:absolute; z-index:2";
				$position_u=" position:absolute; z-index:1";												
			}
			if($tipo_e==1){
				$tipo="Aerea";
				$position_o=" position:absolute; z-index:1";
				$position_a=" position:absolute; z-index:3";
				$position_u=" position:absolute; z-index:2";								
			}
			if($tipo_e==3){
				$tipo="Umbral";
				$position_o=" position:absolute; z-index:2";
				$position_a=" position:absolute; z-index:1";
				$position_u=" position:absolute; z-index:3";												
			}
		}else{
			$tipo="Aerea";
			$position_o=" position:absolute; z-index:2";
			$position_a=" position:absolute; z-index:3";
			$position_u=" position:absolute; z-index:1";											
			}
		if(isset($_GET['oido'])){
			$oido="I";
		}else{
			$oido="I";			
		}
        
        $paciente=mysql_query('SELECT * FROM ficha_tecnica')or die(mysql_error());
        $row_paciente=mysql_fetch_array($paciente);
        $id_paciente=$row_paciente['id_paciente'];
        $nombre=$row_paciente['nombre'];
        $paterno=$row_paciente['paterno'];
        $materno=$row_paciente['materno'];						
        $genero=$row_paciente['genero'];			
    ?>
    <div style="float:left" class="grafica_cnt" id="grafica">

        <!--ESTUDIO AEREO-->
        <div style="float:left;<?php echo $position_a; ?>; background:none !important" id="grafica" class="Aerea">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Aerea"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Aerea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Aerea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$j."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Aerea"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Aerea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Aerea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$k."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--ESTUDIO OSEA-->
        <div style="float:left;<?php echo $position_o; ?>; background:none !important" id="grafica" class="Osea">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Osea"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Osea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Osea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$j."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Osea"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Osea_".$forma."_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Osea_".$forma.".png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$k."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--ESTUDIO UMBRAL DE INCONFORT-->
        <div style="float:left;<?php echo $position_u; ?>; background:none !important" id="grafica" class="Umbral">
        <table>
            <?php
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                            FROM estudios
                                                            WHERE db ='.$i.
                                                            ' AND hz ='.$j.
                                                            ' AND oido="'.$oido.
                                                            '" AND id_paciente='.$id_paciente.
                                                            ' AND tipo="Umbral"'
                                                            )or die(mysql_error());
                                $row_seleccionado = mysql_fetch_array($seleccionado);
                                //$tipo = $row_seleccionado['tipo'];
                                $forma = $row_seleccionado['forma'];
                                //$oido = $row_seleccionado['oido'];						
                                $res= $row_seleccionado['respuesta'];
                                $db = $row_seleccionado['db'];
                                $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Umbral_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Umbral.png";
                                    }
                                }else{
                                    $img = "";
                                }																			
                        ?>
                        <td id="point_default" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$j."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $seleccionado = mysql_query('SELECT tipo, forma, respuesta, db, hz, oido 
                                                                FROM estudios
                                                                WHERE db ='.$i.
                                                                ' AND hz ='.$k.
                                                                ' AND oido = "'.$oido.
                                                                '" AND id_paciente='.$id_paciente.
                                                                ' AND tipo="Umbral"'
                                                                
                                                                )or die(mysql_error());
                                    $row_seleccionado = mysql_fetch_array($seleccionado);
                                    //$oido = $row_seleccionado['oido'];
                                    $forma = $row_seleccionado['forma'];
                                    $res= $row_seleccionado['respuesta'];
                                    $db = $row_seleccionado['db'];
                                    $hz = $row_seleccionado['hz'];	
                                if($forma != ""){
                                    if($res!=""){
                                        $img="img_sim/".$oido."_Umbral_".$res.".png";
                                    }else{							
                                        $img="img_sim/".$oido."_Umbral.png";
                                    }
                                }else{
                                    $img = "";
                                }						
                        ?>
                        <td id="point_medio" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;"
                            onclick="<?php echo "dbhz_".$n."_".$k."_click()"; ?>;">
                            <img src="<?php echo $img; ?>" />
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--LINEAS-->
        <div style="float:left; position:absolute; z-index:0 background:none !important" id="grafica" class="Umbral">
        <table id="grafica">
            <?php
                $punto2="";
                $x = 0;
                for($i=-10;$i<=125;$i=$i+5){
                    if($x % 2 == 0){
                        $val = $i;
                    }else{
                        $val = "";
                    }
                ?>
                <tr>
                <td id="point_numeric1">
                    <?php echo $val; ?>
                </td>        
                <?php
                        if($i!=125){										
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }                            																
                        ?>
                        <td id="point_default2" title="<?php echo $i."Db, ".$j."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$j."(event)"; ?>;">                        
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }                                
                        ?>
                        <td id="point_medio2" title="<?php echo $i."Db, ".$k."Hz"; ?>" 
                            onMouseMove="<?php echo "dbhz_".$n."_".$k."(event)"; ?>;">                        
                        </td>
                        <?php	
                                }				
                            }
                        }else{
        
                            $x++;
                            for($j=125;$j<=8000;$j=$j+$j){
                                $n = $i;
                                if($i == -10){
                                    $n = 130;
                                }
                                if($i == -5){
                                    $n = 125;
                                }
                                $punto = $j;
                        ?>
                        <td id="point_numeric2" >
                            <?php echo $punto2; ?>
                        </td>
                        <?php	
                                if($j==500 or $j==1000 or $j==2000 or $j==4000){						
                                    if($j==500){
                                        $k = 750;	
                                    }
                                    if($j==1000){
                                        $k = $k*2;	
                                    }
                                    if($j==2000){
                                        $k = $k*2;	
                                    }
                                    if($j==4000){
                                        $k = $k*2;	
                                    }
                                    $punto = $k;							
                        ?>
                        <td id="point_numeric2" style="color:#999 !important; font-weight:normal;" >
                            <?php echo $punto2; ?>
                        </td>
                        <?php	
                                }				
                            }
                        }
                ?>
                </tr>
                <?php
                    }
                ?>
        </table>
        </div>
        
        <!--OPCIONES-->
        <div class="opciones">
            <input name="id_paciente" value="<?php echo $id_paciente; ?>" type="hidden" />
            <input name="folio" value="1" type="hidden" />
            <table>
            	<tr>
                	<th colspan="3">AUDIOMETRIA:</th>
                </tr><tr>                	
                    <td>
                        <input name="tipo" type="button" value="Aérea" class="btns"
                        onclick="location.href='audiometria_i_aerea.php?tipo=1'" />
                    </td><td>
                        <input name="tipo" type="button" value="Ósea" class="btns"
                		onclick="location.href='audiometria_i_osea.php?tipo=2'" />
                    </td><td>
                        <input name="tipo" type="button" value="Umbral de Inconfort" class="btns"
                		onclick="location.href='audiometria_i_umbral.php?tipo=3'" />
                    </td>           
            	</tr><tr>
            	<td colspan="3">
                    <label for="x">Db: </label><input type="text" name="db" id="x" size="4" readonly="readonly" style="text-align:center"> 
                    <label for="y">Hz: </label><input type="text" name="hz" id="y" size="4" readonly="readonly" style="text-align:center"><br />
                                 
            	</td>
            </tr><tr>
            	<td colspan="3">
            		<input name="oido" type="hidden" value="I" />
                    <input type="button" name="sel_oido" value="Oido Derecho"
                    onclick="window.location.href='audiometria_d.php'" class="btns" /> 
                    <input type="submit" name="guardar" value="Terminar" class="btns" />
            	</td>
            </tr>
        </table>
        </div>        
    	</form>
    </div>
</div>
</body>
</html>