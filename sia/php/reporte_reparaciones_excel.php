<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
	$totales=0;
	$hora = $_POST['hora'];
    $fecha_final_mysql = $_POST['fecha_final'];
    $fecha_final_separada = explode("-", $fecha_final_mysql);
    $fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
    $fecha_inicio_mysql = $_POST['fecha_inicio'];
    $fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	
	// SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=Reporte de Reparaciones.xls");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$contador = 0;
	$total_baterias_vales = 0;
	$total_global_vales = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Bater&iacute;as que Salen </title>
</head>
<body>
	<style>
		table tr td{
			border:1px solid #CCC
		}
	</style>
	<center>
        <table style="border:1px solid #CCC">
        	<tr>
                <td style="color:#000; font-size:22px; text-align:center;" colspan="8"> REPARACIONES </td>
            </tr>
            <tr>
                <td style="color:#000; font-size:16px; text-align:center;" colspan="8"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
            </tr>
            <tr>
                <th style="font-size:14px; background-color:#039; color:#FFF"> NOTA </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> FECHA DE ENTRADA </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> REPARACION </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> FECHA DE SALIDA </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> COSTO </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> NO REPARADO </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> GARANTIA </th>
                <th style="font-size:14px; background-color:#039; color:#FFF"> OBSERVACIONES </th>                                                
            </tr>
            <?php
                // SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
               $query_tecnicos = mysql_query("SELECT DISTINCT(id_empleado), alias
												FROM reparaciones, reparaciones_laboratorio, empleados
												WHERE empleados.id_empleado = reparaciones_laboratorio.reparado_por
												AND reparaciones.folio_num_reparacion = reparaciones_laboratorio.folio_num_reparacion												
												AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'")
												or die('Consulta Tecnicos');
                while($row_query_tecnicos=mysql_fetch_array($query_tecnicos)){
                    $id_empleado=$row_query_tecnicos['id_empleado'];
                    $alias=$row_query_tecnicos['alias'];								
            ?>
            <tr>
                <td colspan="8" style="font-size:14px; background-color:#093; color:#FFF"><?php echo ucwords($alias); ?></td>
            </tr>	
            <?php
                    // SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
                    $query_reparaciones_notas = "SELECT reparaciones.folio_num_reparacion, fecha_entrada, descripcion_problema, 
												fecha_salida, reparaciones_laboratorio.costo, observaciones, estado_reparacion, 
												no_reparado, aplica_garantia
												FROM reparaciones,reparaciones_laboratorio, estatus_reparaciones
												WHERE reparaciones.folio_num_reparacion = reparaciones_laboratorio.folio_num_reparacion
												AND reparaciones.id_estatus_reparaciones=estatus_reparaciones.id_estatus_reparacion												
												AND reparado_por=".$id_empleado."
												AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'
												ORDER BY fecha_salida DESC";
                            
                    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                    $resultado_reparaciones_notas = mysql_query($query_reparaciones_notas) or die(mysql_error());
                    $costos=0;
                    $registros=0;
                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                    while( $row_reparacion_nota = mysql_fetch_array($resultado_reparaciones_notas) ){
                            $contador++;
                            $registros++;
						$reparado = $row_reparacion_nota['no_reparado'];
						if($reparado!=""){
							$reparado="No Reparado";
						}
						$garantia = $row_reparacion_nota['aplica_garantia'];
                        $fecha_entrada = $row_reparacion_nota['fecha_entrada'];
                        $fecha_entrada_separada = explode("-", $fecha_entrada);
                        $fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
                        $folio_reparacion = $row_reparacion_nota['folio_num_reparacion'];										
                        $descripcion_problema = $row_reparacion_nota['descripcion_problema'];
                        if($descripcion_problema==""){
                            $descripcion_problema="--";
                        }
                        $fecha_salida = $row_reparacion_nota['fecha_salida'];										
                        $fecha_salida_separada = explode("-", $fecha_salida);
                        $fecha_salida_normal = $fecha_salida_separada[2]."/".$fecha_salida_separada[1]."/".$fecha_salida_separada[0];										
                        if($garantia==""){
							$costo = $row_reparacion_nota['costo'];
						}else{
							$costo=0.0;	
							$garantia="Garantía";
						}
                        $observaciones = $row_reparacion_nota['observaciones']; 
                        $costos+=$costo;
                        $totales+=$costo;
                            
            ?>
            <tr>
                <td style="font-size:12px; text-align:center;"> <?php  echo $folio_reparacion; ?> </td>
                <td style="font-size:12px; text-align:center;"> <?php  echo $fecha_entrada_normal; ?>  </td>
                <td style="font-size:12px;"> <?php  echo $descripcion_problema; ?> </td>
                <td style="font-size:12px; text-align:center;"> <?php  echo $fecha_salida_normal; ?> </td>                                               
                <td style="font-size:12px; text-align:right;"><?php echo "$".number_format($costo,2); ?> </td>
                <td style="font-size:12px; text-align:center;"> <?php  echo ucwords($reparado); ?>  </td>
                <td style="font-size:12px; text-align:center;"> <?php  echo ucwords($garantia); ?>  </td>
                <td style="font-size:12px;"> <?php  echo $observaciones; ?> </td>                                                
            </tr>
            <?php
                    }
            ?>
            <tr>
                <td colspan="8" style="font-size:12px; font-weight:lighter"><?php echo $registros." registros de detalle"; ?></td>
            </tr>
            <tr>
                <td colspan="4" style="padding-left:50px; font-size:12px; padding-bottom:50px;">Suma</td>
                <td style="font-size:12px; text-align:right;" valign="top"><?php echo "$".number_format($costos,2); ?></td>
                <td colspan="2"></td>
            </tr>
            <?php
                }
			?>
            <tr>                                        		
                <td colspan="6"></td>
                <td style="text-align:center; color:#ac1f1f;"> Total: <br /> <?php echo "$".number_format($totales,2); ?> </td>
                <td></td>
            </tr>
        </table>
	 </center>
</body>
</html>