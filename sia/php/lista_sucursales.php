<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Sucursales</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="">Seleccione</option><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label></td><td><input type="text" name="descripcion[]" /><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Sucursales
                </div>
            </div><!--Fin de fondo titulo-->
			<?php 
                include("config.php");
                if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                    $filtro = $_POST['filtro'];
                    $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                        FROM sucursales
                                                        WHERE nombre LIKE '%".$filtro."%' 
                                                        ORDER BY id_sucursal");														
                    $row_busqueda = mysql_fetch_array($res_busqueda);
                    $res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
                }else{
                    $res2="";
                }
			?>
        	<div class="buscar2">
             	<form name="busqueda" method="post" action="lista_sucursales.php">
                 	<label class="textos"><?php echo $res2; ?></label>
                    <input name="filtro" type="text" size="15" maxlength="15" />
                    <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
                </form>
         	</div><!-- Fin de la clase buscar -->
            <div class="area_contenido2">
            	<center>
                    <table>
                        <tr>
                            <th style="text-align:center;" colspan="5">Sucursales</th>
                        </tr><tr>
                        	<th>Sucursal</th>
                            <th>Direccion</th>
                            <th colspan="3">Contacto</th>
                        </tr>                        
						<?php 
							// QUERY QUE OBTIENE LOS almacenes
							if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
								$filtro = $_POST['filtro'];
								$consulta_sucursal = mysql_query("SELECT DISTINCT(id_sucursal), nombre, calle, colonia, num_exterior, num_interior
																		FROM sucursales
																		WHERE nombre LIKE '%".$filtro."%' 
																		ORDER BY id_sucursal");														
                    
							}else{
								$consulta_sucursal = mysql_query("SELECT * FROM sucursales") or die(mysql_error());
							}
							$n_sucursal=0;
							while($row = mysql_fetch_array($consulta_sucursal)){
								$nombre = $row["nombre"];								
								$direccion="C. ".$row['calle']." #".$row['num_exterior']." ".$row['num_interior']."<br/> Col. ".$row['colonia'];
								$id_sucursal = $row["id_sucursal"];
								$n_sucursal++;
						?>
                        <tr>
                        	<td id="alleft">
                                <label class="textos"><?php echo $nombre; ?></label>
                            </td><td id="alleft">
                                <label class="textos"><?php echo $direccion; ?></label>
                            </td><td>
                            	<table style="width:300px !important">
                                	<?php 
										$contacto_sucursal=mysql_query('SELECT tipo, descripcion 
																		FROM forma_contacto_sucursal 
																		WHERE id_sucursal='.$id_sucursal)
																		or die(mysql_error());
										while($row_sucursal=mysql_fetch_array($contacto_sucursal)){
											$tipo=$row_sucursal['tipo'];
											$descripcion=$row_sucursal['descripcion'];
									?>
                                	<tr>
                                    	<td id="alright">
											<label class="textos"><?php echo $tipo; ?>:</label>
                                        </td><td id="alleft">
											<label class="textos"><?php echo $descripcion; ?></label>
                                        </td>
                                    </tr>
                                    <?php
										}
									?>
                                </table>
                            </td><td id="alright">
                            	<a href="eliminar_sucursal.php?id_sucursal=<?php echo $id_sucursal; ?>" title="<?php echo $nombre; ?>">
                                	<img src="../img/delete.png">
                                </a>
                            </td><td id="alright">    
                                <a href="modificar_sucursal.php?id_sucursal=<?php echo $id_sucursal; ?>" title="<?php echo $nombre; ?>">
                                    <img src="../img/modify.png">
                                </a>
                            </td>                            
                        </tr> 
						<?php 
              				}
                			if($n_sucursal==0){
						?>  
                        <tr>
                            <td style="text-align:center;" colspan="5">
                                <label class="textos">"No hay sucursales registradas"</label>
                            </td>
                        </tr> 
						<?php
                            }
                        ?>
                    </table>
                </center>
                <br />
               
                    <div class="titulos">Agregar Nueva</div>
                <br />
                 <div class="contenido_proveedor">
                 	
                    <form name="forma1" action="proceso_guarda_sucursal.php" method="post">
                    <table>	
                    	<tr>
                        	<td id="alright">
                        		<label class="textos">Nombre: </label>
                            </td><td id="alleft" colspan="3">
                      			<input name="sucursal" type="text" style="width:200px;" />
                      		</td>
                       	</tr><tr>
                        	<td id="alright">
                        		<label class="textos">Calle: </label>
                        	</td><td id="alleft">
                        		<input name="calle" type="text" style="width:200px;" />
                        	</td><td id="alright">
                        		<label class="textos">Numero: </label>
                        	</td><td id="alleft">
                            	<label class="textos">Ext. </label>
                        		<input name="num_exterior" type="text" size="6" maxlength="6" />
                                <label class="textos"> Int. </label>
                                <input name="num_interior" type="text" size="6" maxlength="6" />
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Colonia: </label>
                        	</td><td id="alleft">
                        		<input name="colonia" type="text" style="width:200px;" />
                        	</td><td id="alright">
                        		<label class="textos">Codigo Postal: </label>
                        	</td><td id="alleft">
                                <input name="codigo_postal" type="text" size="6" maxlength="5" />
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Estado: </label>
                        	</td><td id="alleft">
                        		<select name="id_estado" id="id_estado" style="width:200px;">
                                	<option value="0">Estado</option>
								<?php
									$estados=mysql_query('SELECT * from estados')or die(mysql_error());
									while($row_estados=mysql_fetch_array($estados)){
								?>
                                <option value="<?php echo $row_estados['id_estado']; ?>"><?php echo utf8_encode($row_estados['estado']); ?></option>
                                <?php
									}
								?>
                                </select>
                        	</td><td id="alright">
                        		<label class="textos">Ciudad: </label>
                        	</td><td id="alleft">
                                <select name="id_municipio" id="id_municipio" style="width:200px;" disabled="disabled">
                                	<option value="0">Ciudad</option>								
                                </select>
                        	</td>
                        </tr><tr>
                        	<td colspan="3" id="alright">
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input type="text" style="width:200px" name="municipio" />
                            </td>
                        </tr><tr> 
                        	<td style="text-align:center" colspan="4">
                            	<label class="textos">- Contacto -</label>
                            </td>
                        </tr><tr>                          
                            <td style="text-align:center" colspan="4">                        	
                                <label class="textos">Tipo:</label>
                                <select name="tipo_telefono[]">
                                    <option value="">Seleccione</option>
                                    <option value="Telefono">Telefono</option>
                                    <option value="Celular">Celular</option>
                                    <option value="Correo">Correo</option>
                                    <option value="Fax">Fax</option>
                                </select>
                                <label class="textos">Descripción:</label>
                                <input name="descripcion[]" type="text" />                         	
                            </td>
                		</tr><tr>                        
                            <td style="text-align:center" colspan="4">                            
                                <div id="nuevo" style="margin-top:10px"></div>
                            </td>
                		</tr><tr>      
                            <td colspan="4" style="text-align:center">                  
                                <input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>
                            </td>
                   		</tr><tr>
                        	<td id="alright" colspan="4">
                                <input name="accion" type="reset" value="Cancelar" class="fondo_boton"/>
                                <input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                        	</td>
                        </tr>
                    </table>
                    </form>
                </div><!-- Fin de contenido proveedor -->
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>