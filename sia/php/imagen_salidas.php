<?php
	// CONEXION A LA BASE DE DATOS A TRAVES DEL ARCHIVO CONFIG.PHP
	include('config.php');
	
	// SE RECIBEN LAS VARIABLES A TRAVES DEL METODO POST
	$id_catalogo = $_POST['id_catalogo'];
	$id_material = $_POST['id_material'];
	$id_ventilacion = $_POST['id_ventilacion'];
	$id_salida = $_POST['id_salida'];
	
	// QUERY QUE OBTIENE ID_CAT_NIVEL1
	$query_cat_nivel1 = mysql_query("SELECT id_cat_nivel1
									 FROM cat_nivel1,catalogo,materiales
									 WHERE cat_nivel1.id_catalogo = catalogo.id_catalogo AND
										   cat_nivel1.id_material = materiales.id_material AND
										   cat_nivel1.id_catalogo = '$id_catalogo' AND
										   cat_nivel1.id_material = '$id_material'") or die (mysql_error());
	
	$row_cat_nivel1 = mysql_fetch_array($query_cat_nivel1);

/*--------------------------------------------------------------------------------------------------------------*/

	// SE DECLARA LA VARIABLE ANTES OBTENIDA EN EL QUERY
	$id_cat_nivel1 = $row_cat_nivel1['id_cat_nivel1'];

	// SE REALIZA EL QUERY QUE OBTIENE EL ID_CAT_NIVEL2
	$query_cat_nivel2 = mysql_query("SELECT id_cat_nivel2
								  FROM cat_nivel2,cat_nivel1,ventilaciones
								  WHERE cat_nivel2.id_cat_nivel1 = cat_nivel1.id_cat_nivel1 AND
                                  	    cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion AND
                                        cat_nivel2.id_ventilacion = '$id_ventilacion' AND
      							        cat_nivel2.id_cat_nivel1 = '$id_cat_nivel1'") or die (mysql_error());
    
    $row_cat_nivel2 = mysql_fetch_array($query_cat_nivel2);
    
/*----------------------------------------------------------------------------------------------------------------*/
	
	// SE DECLARA LA VARIABLE OBTENIDA EN EL QUERY ANTERIOR
	$id_cat_nivel2 = $row_cat_nivel2['id_cat_nivel2'];
	
	// QUERY QUE OBTIENE LA IMAGEN DE LA VENTILACION SELECCIONADA
	$query_imagen_salida = mysql_query("SELECT imagen
											 FROM salidas,cat_nivel3,cat_nivel2
											 WHERE cat_nivel3.id_cat_nivel2 = cat_nivel2.id_cat_nivel2 AND
      											   cat_nivel3.id_salida = salidas.id_salida AND
      											   cat_nivel3.id_cat_nivel2 = '$id_cat_nivel2' AND
      											   cat_nivel3.id_salida = '$id_salida'") or die (mysql_error());
											       
	$row_imagen_salida = mysql_fetch_array($query_imagen_salida);
	$imagen_salida = $row_imagen_salida['imagen'];
?>
	<div id="imagenes_salida">
		<img width="120" height="150" src="../moldes/salidas/<?php echo $imagen_salida; ?>">
	</div>
