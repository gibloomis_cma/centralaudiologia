<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
	$totales=0;
	$hora = $_POST['hora'];
    $fecha_final_mysql = $_POST['fecha_final'];
    $fecha_final_separada = explode("-", $fecha_final_mysql);
    $fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
    $fecha_inicio_mysql = $_POST['fecha_inicio'];
    $fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	
	// SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=Reporte de Moldes.xls");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$contador = 0;
	$total_baterias_vales = 0;
	$total_global_vales = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Bater&iacute;as que Salen </title>
</head>
<body>
	<style>
		table tr td{
			border:1px solid #CCC
		}
	</style>
	<center>
        <table> 
            <tr>
                <td style="color:#000; font-size:22px; text-align:center;" colspan="7"> MOLDES </td>
            </tr>
            <tr>
                <td style="color:#000; font-size:16px; text-align:center;" colspan="7"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
            </tr>
            <tr>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> NOTA </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> FECHA DE ENTRADA </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> CLIENTE </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> FECHA DE SALIDA </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> COSTO </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> TIPO </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> OBSERVACIONES </th>                                                
                        </tr>
                		<?php
							// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
							$query_tecnicos = mysql_query("SELECT DISTINCT(id_empleado), alias
												FROM moldes, moldes_elaboracion, empleados
												WHERE empleados.id_empleado = moldes_elaboracion.elaboro
												AND moldes.folio_num_molde = moldes_elaboracion.folio_num_molde																																												
												AND moldes.id_estatus_moldes <> 1
												AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'")
												or die(mysql_error());
							while($row_query_tecnicos=mysql_fetch_array($query_tecnicos)){
								$id_empleado=$row_query_tecnicos['id_empleado'];
								$alias=$row_query_tecnicos['alias'];								
						?>
                        <tr>
                            <td colspan="7" style="font-size:14px; background-color:#093; color:#FFF"><?php echo ucwords($alias); ?></td>
                        </tr>	
						<?php
								// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
								$query_moldes_notas = "SELECT moldes.folio_num_molde, fecha_entrada, moldes.nombre, moldes.paterno, 
														fecha_salida, costo, moldes_elaboracion.observaciones, estado_molde, lado_oido,
														adaptacion, reposicion, id_estilo, id_material, id_salida, moldes.materno
														FROM moldes,moldes_elaboracion, estatus_moldes
														WHERE moldes.folio_num_molde = moldes_elaboracion.folio_num_molde															
														AND elaboro=".$id_empleado."
														AND moldes.folio_num_molde = moldes_elaboracion.folio_num_molde															
														AND moldes.id_estatus_moldes=estatus_moldes.id_estatus_moldes															
														AND moldes.id_estatus_moldes <> 1
														AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'
														ORDER BY fecha_entrada DESC";
										
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$resultado_moldes_notas = mysql_query($query_moldes_notas) or die(mysql_error());
								$costos=0;$registros=0;$precio=0;
								// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
								while( $row_molde_nota = mysql_fetch_array($resultado_moldes_notas) ){
									$precio=0;
									$contador++;
									$registros++;
									$estado_molde=$row_molde_nota['estado_molde'];
									$lado_oido=ucwords(strtolower($row_molde_nota['lado_oido']));
									$fecha_entrada = $row_molde_nota['fecha_entrada'];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$folio_molde = $row_molde_nota['folio_num_molde'];										
									$cliente = ucwords(strtolower($row_molde_nota['nombre']." ".$row_molde_nota['paterno']." ".$row_molde_nota['materno']));	
									$fecha_salida = $row_molde_nota['fecha_salida'];										
									$fecha_salida_separada = explode("-", $fecha_salida);
									$fecha_salida_normal = $fecha_salida_separada[2]."/".$fecha_salida_separada[1]."/".$fecha_salida_separada[0];										
									$costo = $row_molde_nota['costo'];									
									$observaciones = "[".$lado_oido."] ".$row_molde_nota['observaciones']; 
									$reposicion = $row_molde_nota['reposicion'];
									$adaptacion = $row_molde_nota['adaptacion'];
									$id_estilo=$row_molde_nota['id_estilo'];
									$id_material=$row_molde_nota['id_material'];
									$id_salida=$row_molde_nota['id_salida'];
									if($adaptacion!=""){
										$tipo="[Adaptacion] ";
										//Precio del Estilo
										$precio_estilos=mysql_query('SELECT * FROM estilos')or die('Consulta Precio del Estilo');
										$row_estilos=mysql_fetch_array($precio_estilos);
										$precio+=$row_estilos['precio'];
										//Precio del Material
										$precio_materiales=mysql_query('SELECT * FROM materiales')or die('Consulta Precio del Material');
										$row_materiales=mysql_fetch_array($precio_materiales);
										$precio+=$row_materiales['precio'];
										//Precio de la Salida
										$precio_salidas=mysql_query('SELECT * FROM salidas')or die('Consulta Precio del Salidas');
										$row_salidas=mysql_fetch_array($precio_salidas);
										$precio+=$row_salidas['precio'];
									}else if($reposicion!=""){
										$tipo="[Reposicion] ";
										$costo=0;
									}else{
										$tipo="";
									}									 
									if($costo<=0 and $adaptacion!=""){
										//$observaciones.=$id_estilo." ".$id_material." ".$id_salida;
										$costo=$precio;
									}
									$costos+=$costo;
									$totales+=$costo;
									if($lado_oido=="Bilateral"){
										$registros++;
									}
										
						?>
                        <tr>
                            <td style="font-size:12px; text-align:center;" valign="middle"> <?php  echo $folio_molde; ?> </td>
                            <td style="font-size:12px; text-align:center;" valign="middle"> <?php  echo $fecha_entrada_normal; ?>  </td>
                            <td style="font-size:12px;" valign="middle"> <?php  echo $cliente; ?> </td>
                            <td style="font-size:12px; text-align:center;" valign="middle"> <?php  echo $fecha_salida_normal; ?> </td>                                               
                            <td style="font-size:12px; text-align:right;" valign="middle"><?php echo "$".number_format($costo,2); ?> </td>
                            <td style="font-size:12px; text-align:center;" valign="middle"> <?php  echo $tipo; ?> </td>  
                            <td style="font-size:12px;" valign="middle"> <?php  echo $observaciones; ?> </td>                                                
                        </tr>                       
						<?php
								}
                        ?>
                        <tr>
                            <td colspan="7" style="font-size:12px; font-weight:lighter"><?php echo $registros." registros de detalle"; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-left:50px; font-size:12px; padding-bottom:50px;">Suma</td>
                            <td style="font-size:12px; text-align:right;" valign="top"><?php echo "$".number_format($costos,2); ?></td>
                        </tr>
						<?php
							}
                        ?>
                        <tr>                                        		
                            <td colspan="6"></td>                            
                            <td style="text-align:center; color:#ac1f1f;"> Total: <br /> <?php echo "$".number_format($totales,2); ?> </td>
                        </tr>
					</table>
	 </center>
</body>
</html>