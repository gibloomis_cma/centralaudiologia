<?php
    // SE INICIA SESION
    session_start();

    // SE INICIALIZA EL FOLIO EN BLANCO
    $_SESSION["folio"] = "";

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");

    //include("metodo_cambiar_fecha.php");

    // SE IMPORTA EL ARCHIVO DE HACER INVENTARIO ANTERIOR.PHP
    include("hacer_inventario_anterior.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Nueva Venta </title>
    <link type="text/css" rel="stylesheet" href="../css/style3.css"/>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
    <!-- SCRIPT PARA TABS -->
    <script src="../js/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $("ul.tabs li").click(function(){//cada vez que se hace click en un li
                $("ul.tabs li").removeClass("active"); //removemos clase active de todos
                $(this).addClass("active"); //añadimos a la actual la clase active
                $(".tab_content").hide(); //escondemos todo el contenido

                var content = $(this).find("a").attr("href"); //obtenemos atributo href del link
                $(content).fadeIn(); // mostramos el contenido
                return false; //devolvemos false para el evento click
            });
            $('.agregar').click(function(){
                $('.descuento_agregar').val($('#descuento').val());
            });
        });
    </script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
    <script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
    <script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
    <!-- Fin de Script Tabs -->
    <!-- Estilo para el Tabs  -->
    <style>
    ul.tabs 
    {
        margin: 0;
        padding: 0;
        float: left;
        list-style: none;
        height: 28px;
        border-bottom: 1px solid #999;
        border-left: 1px solid #999;
        width: 100%;
    }

    ul.tabs li 
    {
        float: left;
        margin: 0;
        padding: 0;
        height: 27px;
        line-height: 29px;
        border: 1px solid #999;
        border-left: none;
        margin-bottom: -1px;
        overflow: hidden;
        position: relative;
        background: #e0e0e0;
    }

    ul.tabs li a 
    {
        text-decoration: none;
        color: #000;
        display: block;
        font-size: 1em;
        padding: 0 20px;
        border: 1px solid #fff;
        outline: none;
    }

    ul.tabs li a:hover 
    {
        background: #ccc;
    }

    html ul.tabs li.active, html ul.tabs li.active a:hover  
    {
        background: #fff;
        border-bottom: 1px solid #fff;
    }

    .tab_container 
    {
        border: 1px solid #999;
        border-top: none;
        overflow: hidden;
        clear: both;
        float: left; width:100%;
        background: #fff;
    }

    .tab_content 
    {
        padding: 20px;
        font-size: 1.2em;
    }
    </style>
    <!-- Fin de estilo Tabs -->
</head>
<?php 
    date_default_timezone_set('America/Monterrey');
    $script_tz = date_default_timezone_get();
    $fecha = date("d/m/Y");
?>
<body onload="muestraReloj()">
<div id="contenido_columna2">
    <div class="contenido_pagina">   
        <div class="fondo_titulo1">        
            <div class="categoria">
                Ventas
            </div><!-- FIN DIV CATEGORIA -->         
        </div><!-- FIN DIV FONDO TITULO 1 -->       
        <div class="area_contenido1">                
            <br/>            
            <div class="contenido_proveedor">                
                <center>
                <?php
                    /* SE CONSULTA EL DEPARTAMENTO DEL EMPLEADO QUE ENTRO EN EL SISTEMA */
                    $consulta_departamento_empleado = mysql_query("SELECT id_departamento FROM empleados 
                                                                   WHERE id_empleado=".$_SESSION["id_empleado_usuario"]) or die(mysql_error());
                    
                    /* SE ALMACENA EL RESULTADO DEL QUERY EN LA VARIABLE ROW_DEPARTAMENTO EMPLEADO */
                    $row_departamento_empleado = mysql_fetch_array($consulta_departamento_empleado);

                    /* EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE */
                    $id_departamento_empleado = $row_departamento_empleado["id_departamento"];

                    /* SE CONSULTA LA SUCURSAL DEL DEPARTAMENTO OBTENIDO */
                    $consulta_sucursal = mysql_query("SELECT id_sucursal FROM areas_departamentos 
                                                      WHERE id_departamento=".$id_departamento_empleado) or die(mysql_error());

                    /* SE ALMACENA EL RESULTADO DEL QUERY EN LA VARIABLE ROW SUCURSAL */
                    $row_sucursal = mysql_fetch_array($consulta_sucursal);

                    /* SE ALMACENA EN UNA VARIABLE EL RESULTADO FINAL */
                    $id_sucursal = $row_sucursal["id_sucursal"];
                ?>
            	<form name="form" method="post" action="proceso_guardar_nueva_venta.php">
                    <div id="spanreloj"></div>             
                    <!-- SE CONSULTA EL NUMERO DE FOLIO DE VENTA SIGUIENTE -->
                    <?php
                       	if( $_SESSION['folio'] == "" )
                        {
                        	$folio_num_venta_max = mysql_query('SELECT MAX(folio_num_venta+1) 
                                                                FROM ventas WHERE id_sucursal='.$id_sucursal)or die(mysql_error());
                            $row_folio_num_venta_max = mysql_fetch_array($folio_num_venta_max);
                            $_SESSION['folio'] = $row_folio_num_venta_max['MAX(folio_num_venta+1)'];
                            if($_SESSION["folio"] == NULL)
                            {
                            	$_SESSION["folio"] = 1;
                            }
                        }
                        else
                        {
            				$id_sucursal = $_GET["id_sucursal"];
            			}
              	     ?>
                    <!--- Fin -->
                    <table>
                	   <tr>
                    	   <th colspan="5">
                        	   Nueva Venta
                            </th>
                        </tr>
                        <tr>
                            <td id="alright">
                                <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" id="sucursal"/>
                                <label class="textos"> Venta: </label>
                            </td>
                            <td>
                                <input name="venta" readonly="readonly" value="<?php echo $_SESSION["folio"]?>"/>
                            </td>
                            <td id="alright">
                                <label class="textos"> Fecha: </label>
                            </td>
                            <td>
                                <input name="fecha" value="<?php echo $fecha; ?>" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td id="alright">
                                <label class="textos"> Descuento: </label>
                            </td>
                            <td>
                                <?php 
                                    // SE VALIDA SI LA VARIABLE RECIBIDA DESCUENTO POR METODO GET CONTIENE ALGO DIFERENTE A NADA
                                    if ( isset($_GET['descuento']) && $_GET['descuento'] != "" ) 
                                    {
                                    ?>
                                        <input name="descuento" id="descuento" type="text" size="3" maxlength="3" value="<?php echo $_GET['descuento']; ?>" />
                                    <?php
                                    }
                                    else 
                                    {
                                    ?>
                                        <input name="descuento" id="descuento" type="text" size="3" maxlength="3" />
                                    <?php
                                    }
                                ?>
                                <label class="textos"> % </label>
                            </td>
                            <td id="alright">
                                <label class="textos"> Vendedor: </label>
                            </td>
                            <td>
							    <?php
                                    $consulta_empleados = mysql_query('SELECT * FROM empleados, usuarios 
                                                                       WHERE empleados.id_empleado = usuarios.id_empleado 
                                                                       AND id_usuario = '.$_SESSION["id_usuario"]) or die(mysql_error());			
                                    $row = mysql_fetch_array($consulta_empleados);		
                                    $nombre = $row["nombre"];
                                    $paterno = $row["paterno"];
                                    $materno = $row["materno"];
                                    $id_empleado = $row["id_empleado"];	
                                    $alias = ucwords(strtolower($row['alias']));		
                                ?>
                                <input name="id_empleado" type="hidden" value="<?php echo $id_empleado; ?>"/>
                                <input name="empleado" value="<?php echo $alias; ?>" readonly="readonly"  title="<?php echo $nombre." ".$paterno." ".$materno; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <th> Cantidad </th>
                            <th colspan="2"> Descripción </th>
                            <th colspan="2"> Costo Unitario </th>
                        </tr>
					    <?php
                            $consulta_descripcion_venta = mysql_query("SELECT SUM(cantidad) as cantidad,id_registro,descripcion, costo_unitario, pago_parcial, pago_parcial_2
                                                                       FROM descripcion_venta
                                                                       WHERE folio_num_venta=".$_SESSION["folio"]." AND
																	   id_sucursal=".$id_sucursal." GROUP BY descripcion") or die(mysql_error());
                            $total=0;
                            while( $row4 = mysql_fetch_array($consulta_descripcion_venta) )
                            {
                                $id_registro_venta = $row4["id_registro"];
                                $descripcion = $row4["descripcion"];
                                $costo_unitario = $row4["costo_unitario"];
                                $cantidad = $row4["cantidad"];
                                $pago_parcial = $row4['pago_parcial'];
                                $total=$total+$costo_unitario;

                                if ( $pago_parcial != 0 )
                                {
                                    $costo_unitario = $pago_parcial;
                                }
                                else
                                {
                                    if($cantidad <= 3)
                                    {
                                        $costo_unitario = $costo_unitario;  
                                    }
                                    elseif($cantidad >= 4 && $cantidad <= 39)
                                    {
                                        $rebaja = ($costo_unitario * 10)/100;
                                        $costo_unitario = $costo_unitario - $rebaja;
                                    }
                                    else
                                    {
                                        $rebaja = ($costo_unitario * 20)/100;
                                        $costo_unitario = $costo_unitario - $rebaja;    
                                    }
                                }
								
                    ?>
                        <tr>
                            <td style="text-align:center"><?php echo $cantidad; ?></td>
                            <td colspan="2"><?php echo $descripcion; ?></td>
                            <td id="alright"><?php echo "$".number_format($costo_unitario,2); ?></td>
                            <td>
                                <a href="proceso_eliminar_producto_venta.php<?php echo "?id_registro_venta=".$id_registro_venta."&id_sucursal=".$id_sucursal; ?>">
                                    <img src="../img/delete.png" title="Eliminar"/>
                                </a>
                            </td>
                        </tr>
					    <?php
                            }	
                            $consulta_venta = mysql_query('SELECT COUNT(id_registro) as cnt
                                                         FROM descripcion_venta
                                                         WHERE folio_num_venta='.$_SESSION["folio"].' 
														 AND id_sucursal='.$id_sucursal) or die(mysql_error());
                            $row_consulta_venta = mysql_fetch_array($consulta_venta);
                            $cnt = $row_consulta_venta['cnt'];
                            if( $cnt != 0 )
                            {/*
                            ?>					
                            <tr>
                    	       <td colspan="4" id="alright" >
                        	       <label class="textos">Total:</label>
                                   <span style="color:#F00;">
                            		  <?php echo " $".number_format($total,2); ?>
                                   </span>
                                </td>*/
						    ?>
                            </tr>
                            <tr>
                    	       <td id="alright" colspan="5">
                        	       <input type="submit" name="accion" value="Aceptar" class="fondo_boton"/>
                                </td>
                            </tr>
					        <?php
                            }
                            else
                            {
                            ?>
                            <tr>
                                <td style="text-align:center" colspan="4"> &nbsp; </td>
                            </tr>
                            <?php
                            /*     */  }
                            ?>
                    </table>                          
            	</form>
            	</center> 
            </div><!-- Fin de contenido proveedor -->
            <div class="contenido_proveedor">
                <ul class="tabs">
                    <li class="active"> <a href="#tab1"> <label class="textos"> Refacciones/Laboratorio </label> </a> </li>
                    <li> <a href="#tab2"> <label class="textos"> Baterias </label> </a> </li>
                    <li> <a href="#tab5"> <label class="textos"> Auxiliares </label> </a> </li>
                    <li> <a href="#tab4"> <label class="textos"> Accesorios </label> </a> </li>
                    <li> <a href="#tab3"> <label class="textos"> Servicios Medicos </label> </a> </li>
                    <li> <a href="#tab6"> <label class="textos"> Reparaciones </label> </a> </li>
                    <li> <a href="#tab7"> <label class="textos"> Moldes </label> </a> </li>
                </ul>
                <div class="tab_container">
                    <!-- Agregar Refaccion a la venta -->
                    <div id="tab1" class="tab_content">
                        <br/>
                        <form name="forma1" action="proceso_guardar_nueva_venta.php" method="post">
                            <table>
                        	   <tr>
                            	   <td id="alright">
                                        <input name="descuento" class="descuento_agregar" type="hidden"/>
                                        <input name="venta" type="hidden" value="<?php echo $_SESSION["folio"]?>"/>
                                        <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" id="sucursal"/>
                                        <input name="categoria" type="hidden" value="1"/>
                                        <label class="textos">Tipo de Refaccion: </label>
                               	    </td>
                                    <td>
                                        <select name="subcategoria" id="tipo_subcategoria2" style="width:200px;">
                                            <option value="0" >Seleccione</option>
                           		    <?php 
										$consulta_refacciones = mysql_query("SELECT * 
                                                                             FROM subcategorias_productos 
																			 WHERE id_categoria= '1' 
																			 AND id_subcategoria <> 1") or die(mysql_error());
										while( $row = mysql_fetch_array($consulta_refacciones) )
                                        {
											$nombre_subcategoria = $row["subcategoria"];
											$id_subcategoria = $row["id_subcategoria"];
                               	    ?>
                                            <option value="<?php echo $id_subcategoria; ?>"> <?php echo $nombre_subcategoria; ?> </option>
                               	    <?php
                                      	}
                               	    ?> 
                                        </select>
                                    </td>
                                </tr>
                        	    <tr>
                                    <td id="alright">                           
                                        <label class="textos">Descripción: </label>
                            	    </td>
                                    <td>
                                        <select name="desc" id="id_articulo2" style="width:200px;" disabled="disabled">
                                            <option value="0" >Seleccione</option>
                                        </select>
                            	   </td>
                                </tr>
                                <tr>
                            	   <td id="alright">
                            		   <label class="textos">Cantidad: </label>
                            	   </td>
                                   <td>
                                        <select name="cantidad" id="cantidad">
                                            <option value="0">Seleccione</option>
                                        </select>
                            	   </td>                            
                        	    </tr>
                                <tr>
                            	   <td id="alright">
                            		  <label class="textos"> Costo Unitario: </label> 
                            	   </td>
                                   <td>
                            		  <label class="textos">$</label>
                                      <input name="costo" type="text" id="costo"/>
                            	   </td>
                        	    </tr>
                                <tr>
                            	   <td style="text-align:center" colspan="2">
                            		  <input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" />
                            	   </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <!-- Agregar Accesorio a la venta -->
                    <div id="tab4" class="tab_content" style="display:none;">
                        <br/>
                        <form name="forma1" action="proceso_guardar_nueva_venta.php" method="post">
                        <table>
                        	<tr>
                            	<td id="alright">
                                    <input name="descuento" class="descuento_agregar" type="hidden"/>
                                    <input name="venta" type="hidden" value="<?php echo $_SESSION["folio"]?>"/>
                                    <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" id="sucursal3"/>
                                    <input name="categoria" type="hidden" value="1"/>                            
                                    <label class="textos"> Descripción: </label>
                            	</td><td>
                                    <select name="desc" id="desc4">
                                        <option value="0" selected="selected"> Seleccione </option>
									<?php
                                        $resultado = mysql_query("SELECT descripcion, id_base_producto,referencia_codigo 
                                                                  FROM base_productos 
                                                                  WHERE id_categoria = 1 
                                                                  AND id_subcategoria = 1 
                                                                  AND activo = 'Si' 
                                                                  ORDER BY id_subcategoria") or die(mysql_error());
                                        
                                        while( $row2 = mysql_fetch_array($resultado) )
                                        {
                                            $descripcion = $row2["descripcion"];
                                            $id_base_producto = $row2["id_base_producto"];
                                            $codigo = $row2['referencia_codigo'];	
                                    ?>			
                                            <option value="<?php echo $descripcion; ?>"> <?php echo $codigo." -- ".$descripcion; ?> </option>	
                                    <?php 
                                        }
                                    ?>
                                    </select>
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Cantidad: </label>
                            	</td><td>
                                    <select name="cantidad" id="cantidad4">
                                        <option value="0">Seleccione</option>
                                    </select>
                            	</td>                            
                        	</tr><tr>
                            	<td id="alright">
                            		<label class="textos">Costo Unitario: </label> 
                            	</td><td>
                            		<label class="textos">$</label>
                                    <input name="costo" type="text" id="costo3" />
                            	</td>
                        	</tr><tr>
                            	<td style="text-align:center" colspan="2">
                            		<input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" />
                            	</td>
                            </tr>
                        </table>
                        </form>
                    </div>                    
<!--Agregar Baterias a la venta-->
                    <div id="tab2" class="tab_content"  style="display:none;">
                        <br/>
                        <form name="forma1" action="proceso_guardar_nueva_venta.php" method="post">
                        <table>
                        	<tr>
                            	<td id="alright">
                                    <input name="descuento" class="descuento_agregar" type="hidden"/>
                                    <input name="venta" type="hidden" value="<?php echo $_SESSION["folio"]?>"/>
                                    <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" id="sucursal1"/>
                                    <input name="categoria" type="hidden" value="4"/>
                                    <label class="textos">Descripción: </label>
                            	</td><td>
                                    <select name="desc" id="desc1">
                                        <option value="0" selected="selected">Seleccione</option>
							<?php
                                    $resultado2 = mysql_query("SELECT descripcion, id_base_producto FROM base_productos 
                                                                                                    WHERE id_categoria=4 AND activo='Si'")
                                                                                                    or die(mysql_error());
                                    while($row3 = mysql_fetch_array($resultado2)){
                                        $descripcion = $row3["descripcion"];
                                        $id_base_producto = $row3["id_base_producto"];	
                            ?>			
                                        <option value="<?php echo $descripcion; ?>">
                                            <?php echo $descripcion; ?>
                                        </option>	
                            <?php																									
                                    }
                            ?>
                                    </select>
                            	</td>
                        	</tr><tr>
                            	<td id="alright">
                            		<label class="textos">Cantidad: </label>
                            	</td><td>
                                    <select name="cantidad" id="cantidad2">
                                        <option value="0">Seleccione</option>
                                    </select>
                            	</td>
                        	</tr><tr>
                            	<td id="alright">
                            		<label class="textos">Costo Unitario: </label> 
                            	</td><td>
                                	<label class="textos">$</label>
                            		<input name="costo" type="text" id="costo1"/>
                        		</td>
                            </tr><tr>
                            	<td style="text-align:center" colspan="2">
                            		<input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" />
                            	</td>
                            </tr>
                        </table>
                        </form>
                    </div>
                    <!-- Agregar consultas a la venta -->
                    <div id="tab3" class="tab_content"  style="display:none;">
                    	<form name="forma1" id="form_servicios_medicos" action="proceso_guardar_nueva_venta.php" method="post">
                            <br/>
                            <table>
                        	   <tr>
                            	   <td id="alright">
                                    <input name="descuento" class="descuento_agregar" type="hidden"/>
                                        <input name="venta" value="<?php echo $_SESSION["folio"]?>" type="hidden" />
                                        <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" />
                                        <input name="categoria" type="hidden" value="2"/>
                                        <input name="fecha" value="<?php echo $fecha; ?>" type="hidden" />
                                        <label class="textos">Descripción: </label>
								    </td>
                                    <td>
                                    <select name="desc3" id="desc3">
                                        <option value="0" selected="selected"> --- Seleccione --- </option>
							     <?php
                                    $resultado = mysql_query("SELECT subcategoria, id_subcategoria 
                                                              FROM subcategorias_productos 
                                                              WHERE id_categoria = 2") or die(mysql_error());
                                    while( $row2 = mysql_fetch_array($resultado) )
                                    {
                                        $subcategoria = $row2["subcategoria"];
                                        $id_subcategoria = $row2["id_subcategoria"];
                                 ?>			
                                        <option value="<?php echo $id_subcategoria; ?>">
                                            <?php echo $subcategoria; ?>
                                        </option>	
							<?php 
                                    }
                            ?>
                                    </select>    
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Doctor: </label>
                    			</td><td>
                                    <select name="id_doctor" id="doctor_asignado">
                                        <option value="0">Seleccione</option>
                                    </select>
                                </td></tr><tr>
                            	<td id="alright">
                            		<label class="textos">Cantidad: </label>
                            	</td><td>
                           			<input name="cantidad" id="cantidad_consultas" type="text" size="5" maxlength="5" />
                            	</td>
                        	</tr><tr>
                            	<td id="alright">
                            		<label class="textos">Costo Unitario: </label>
                            	</td><td>
                                    <label class="textos">$</label> 
                                    <input name="costo4" type="text" id="costo4"/>
                            	</td>
                        	</tr><tr>
                            	<td style="text-align:center" colspan="2">
                            		<input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" />
                           	 	</td>
                            </tr>
                        </table>
                        </form>
                	</div>
                    <!-- Agregar notas de reparaciones a la venta -->
                    <div id="tab6" class="tab_content" style="display:none;">
                        <br/>
                        <form name="forma1" id="form_reparaciones_venta" action="proceso_guardar_nueva_venta.php" method="post">
                            <table>
                                <tr>
                                    <input name="descuento" class="descuento_agregar" type="hidden"/>
                                    <td> <input name="categoria" type="hidden" value="100"/> </td>
                                    <td> <input name="entrego" type="hidden" id="entrego" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>"/> </td>
                                    <td> <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" /> </td>
                                    <td> <input name="fecha" value="<?php echo $fecha; ?>" type="hidden" /> </td>
                                    <td> <input name="venta" value="<?php echo $_SESSION["folio"]?>" type="hidden" /> </td>
                                </tr>
                                <tr>
                                    <td align="right"> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    <td>
                                        <select name="desc4" id="reparacion">
                                            <option value="0" selected="selected"> Seleccione </option>
                                        <?php
                                        if( $id_sucursal == 1 )
                                        {
                                            $consulta_notas_reparaciones = mysql_query("SELECT folio_num_reparacion,id_estatus_reparaciones
                                                                                        FROM reparaciones
                                                                                        WHERE id_estatus_reparaciones = '9'
                                                                                        AND reparacion = ''
                                                                                        AND adaptacion = ''
                                                                                        AND venta = ''
                                                                                        AND aplica_garantia = ''
                                                                                        ORDER BY folio_num_reparacion DESC") or die(mysql_error());
                                            
                                            while( $row5 = mysql_fetch_array($consulta_notas_reparaciones) )
                                            {
                                                $folio_num_reparacion = $row5["folio_num_reparacion"];
                                                $id_estatus_reparaciones = $row5["id_estatus_reparaciones"];
                                            ?>
                                                <option value="<?php echo $folio_num_reparacion; ?>"> <?php echo "Folio de Reparacion N° ".$folio_num_reparacion; ?> </option>
                                            <?php
                                            }
                                        }
                                        else 
                                        {
                                            $consulta_notas_reparaciones = mysql_query("SELECT folio_num_reparacion,id_estatus_reparaciones
                                                                                        FROM reparaciones
                                                                                        WHERE id_estatus_reparaciones='14'
                                                                                        AND reparacion = ''
                                                                                        AND adaptacion = ''
                                                                                        AND venta = ''
                                                                                        AND aplica_garantia = ''
                                                                                        ORDER BY folio_num_reparacion DESC") or die(mysql_error());
                                            
                                            while( $row5 = mysql_fetch_array( $consulta_notas_reparaciones ) )
                                            {
                                                $folio_num_reparacion = $row5["folio_num_reparacion"];
                                                $id_estatus_reparaciones = $row5["id_estatus_reparaciones"];
                                            ?>		
                                                <option value="<?php echo $folio_num_reparacion; ?>"> <?php echo "Folio de Reparacion N° ".$folio_num_reparacion; ?> </option>
                                            <?php
                                            }
                                        }
                                        ?>
                                        </select>
                                    </td>
                                    <td> <label class="textos"> Cantidad: </label> </td>
                                    <td> <input name="cantidad" type="text" size="5" maxlength="5" value="1" readonly="readonly"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Nombre Cliente: </label> </td>
                                    <td> <input type="text" name="nombre_paciente" id="nombre_paciente" readonly="readonly" size="35"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Costo Total: </label>  </td>
                                    <td> $ <input name="costo5" type="text" id="costo5" readonly="readonly"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Recibio: </label> </td>
                                    <td> <input name="recibio" id="txt_recibio" type="text" size="35" maxlength="35" /> </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: right;">
                                        <input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" />
                                    </td>
                                </tr>
                            </table>
                    </form>
                    </div>
                    <!-- Agregar notas de moldes a la venta -->
                    <div id="tab7" class="tab_content"  style="display:none;">
                        <br/>
                        <form name="forma1" id="form_ventas_moldes" action="proceso_guardar_nueva_venta.php" method="post">
                            <table>
                                <tr>
                                    <input name="descuento" class="descuento_agregar" type="hidden"/>
                                    <td> <input name="categoria" type="hidden" value="200"/> </td>
                                    <td> <input name="entrego" type="hidden" id="entrego" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>"/> </td>
                                    <td> <input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" /> </td>
                                    <td> <input name="fecha" value="<?php echo $fecha; ?>" type="hidden" /> </td>
                                    <td> <input name="venta" value="<?php echo $_SESSION["folio"]?>" type="hidden" /> </td>
                                    <td> <input type="hidden" name="estatus" id="estatus" /> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    <td>
                                        <select name="nota_molde" id="molde">
                                            <option value="0" selected="selected"> Seleccione </option>
                                    <?php
                                        if( $id_sucursal == 1 )
                                        {
                                            $consulta_notas_moldes = mysql_query("SELECT folio_num_molde, id_estatus_moldes
                                                                                  FROM moldes
                                                                                  WHERE (id_estatus_moldes = '5'
                                                                                  OR id_estatus_moldes = '1')
                                                                                  AND costo - pago_parcial <> 0
                                                                                  AND adaptacion = ''
                                                                                  AND reposicion = ''
                                                                                  ORDER BY folio_num_molde DESC") or die(mysql_error());
                                                
                                            while($row5 = mysql_fetch_array($consulta_notas_moldes))
                                            {
                                                $folio_num_molde = $row5["folio_num_molde"];
                                                $id_estatus_moldes = $row5["id_estatus_moldes"];
                                            ?>      
                                                <option value="<?php echo $folio_num_molde; ?>"> <?php echo "Folio de Molde N° ".$folio_num_molde; ?> </option>
                                            <?php
                                            }
                                        }
                                        else
                                        {
                                            $consulta_notas_moldes = mysql_query("SELECT folio_num_molde, id_estatus_moldes
                                                                                  FROM moldes
                                                                                  WHERE (id_estatus_moldes = '6'
                                                                                  OR id_estatus_moldes = '4')
                                                                                  AND costo - pago_parcial <> 0
                                                                                  AND adaptacion = ''
                                                                                  AND reposicion = ''
                                                                                  ORDER BY folio_num_molde DESC")or die(mysql_error());
                                                
                                            while($row5 = mysql_fetch_array($consulta_notas_moldes))
                                            {
                                                $folio_num_molde = $row5["folio_num_molde"];
                                                $id_estatus_moldes = $row5["id_estatus_moldes"];
                                            ?>      
                                                <option value="<?php echo $folio_num_molde; ?>"> <?php echo "Folio de Molde N° ".$folio_num_molde; ?> </option>
                                            <?php
                                            }
                                        }   
                                        ?>
                                        </select>
                                    </td>
                                    <td> <label class="textos"> Cantidad: </label> </td>
                                    <td> <input name="cantidad" type="text" size="5" maxlength="5" value="1" readonly="readonly"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Nombre Cliente: </label> </td>
                                    <td> <input type="text" name="nombre_cliente_molde" readonly="readonly" id="nombre_cliente_molde" size="35"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Costo Total: </label>  </td>
                                    <td> $ <input name="costo6" type="text"  id="costo6" readonly="readonly"/> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Pago Parcial </label> </td>
                                    <td> $ <input type="text" name="pago_parcial" id="pago_parcial" /> </td>
                                </tr>
                                <tr>
                                    <td> <label class="textos"> Recibio: </label> </td>
                                    <td> <input name="recibio" id="recibio_moldes" type="text" size="35" maxlength="35" /> </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align:right;"> <input type="submit" name="accion" value="Agregar" class="fondo_boton agregar" /> </td>
                                </tr>         
                            </table>
                        </form>
                    </div>
                </div><!--Fin de tab_container-->
            </div><!--Fin de contenido proveedor-->	
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>
