<?php
	include("config.php");
	include("metodo_cambiar_fecha.php");
	$id_estatus_molde = $_POST["id_estatus_moldes"];
	$fecha_normal = $_POST["fecha_actual"];
	$fecha_entrada_mysql=cambiaf_a_mysql($fecha_normal);
	$hora = $_POST["hora_real"];
	$folio_num_molde = $_POST["folio_molde"];
	$id_empleado = $_POST["id_empleado_sistema"];
	$fecha_entrega = $_POST['fecha_entrega'];
	$fecha_entrega_separada = explode("/", $fecha_entrega);
	$fecha_entrega_mysql = $fecha_entrega_separada[2]."-".$fecha_entrega_separada[1]."-".$fecha_entrega_separada[0];
	$hora_entrega = $_POST['hora'];
	$recibio = $_POST['recibio'];

	/* Variables para guardar fabricacion */
	$id_empleado_elaboro = $_POST["quien_elaboro"];
	$fecha_fabricacion = $_POST["fecha_fabricacion"];
	$fecha_fabricacion_mysql=cambiaf_a_mysql($fecha_fabricacion);
	$observaciones = $_POST["observaciones"];
	
	if($_POST["cambiar_estatus"] == "Cambiar Estatus"){
		/* Si id_estatus_molde es igual a 2.-Laboratorio, En fabricacion */
		if($id_estatus_molde == 2){
			/* Se actualiza el estatus del molde */
			mysql_query("UPDATE moldes SET id_estatus_moldes=".$id_estatus_molde."
						WHERE folio_num_molde=".$folio_num_molde)or die(mysql_error());
			/* Movimiento del molde */				
			mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,
														 id_estatus_moldes, id_empleado)
												VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."',
														'".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			header('Location: detalles_moldes.php?folio='.$folio_num_molde);
		}
		/* Si id_estatus_molde es igual a 5.-Recepcion Matriz, Elaborado */
		if($id_estatus_molde == 5){
			/* Se actualiza el estatus del molde */
			mysql_query("UPDATE moldes SET id_estatus_moldes=".$id_estatus_molde."
						WHERE folio_num_molde=".$folio_num_molde)or die(mysql_error());
			/* Movimiento del molde */				
			mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,
														 id_estatus_moldes, id_empleado)
												VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."',
														'".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			header('Location: lista_moldes.php');				
		}
		/* Si id_estatus_molde es igual a 5.-Recepcion Ocolusen, Elaborado */
		if($id_estatus_molde == 6){
			/* Se actualiza el estatus del molde */
			mysql_query("UPDATE moldes SET id_estatus_moldes=".$id_estatus_molde."
						WHERE folio_num_molde=".$folio_num_molde)or die(mysql_error());
			/* Movimiento del molde */				
			mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,
														 id_estatus_moldes, id_empleado)
												VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."',
														'".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			header('Location: lista_moldes.php');				
		}
		/* Si id_estatus_molde es igual a 9.-Almacen*/
		if($id_estatus_molde == 9){
			/* Se actualiza el estatus del molde */
			mysql_query("UPDATE moldes SET id_estatus_moldes=".$id_estatus_molde."
						WHERE folio_num_molde=".$folio_num_molde)or die(mysql_error());
			/* Movimiento del molde */				
			mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,
														 id_estatus_moldes, id_empleado)
												VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."',
														'".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			header('Location: lista_moldes.php');				
		}
	}
	if($_POST["guardar_fabricacion"] == "Guardar Fabricacion"){
		/* Se actualiza el estatus del molde */
		mysql_query("UPDATE moldes SET id_estatus_moldes='8'
						WHERE folio_num_molde=".$folio_num_molde)or die(mysql_error());
		/* Se guardan los demas datos del molde */				
		mysql_query("INSERT INTO moldes_elaboracion(folio_num_molde, elaboro, fecha_salida, observaciones)
											VALUES('".$folio_num_molde."','".$id_empleado_elaboro."',
													'".$fecha_fabricacion_mysql."','".$observaciones."')")or die(mysql_error());
																/* Movimiento del molde */				
		mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,
													 id_estatus_moldes, id_empleado)
											VALUES('".$folio_num_molde."','1','".$fecha_fabricacion_mysql."','".$hora."',
													'8','".$id_empleado_elaboro."')") or die(mysql_error());
		header('Location: detalles_moldes.php?folio='.$folio_num_molde);
	}

	if( $_POST['aceptar'] == "Aceptar" )
	{
		// SE ACTUALIZA EL ESTATUS DEL MOLDE EN LA TABLA MOLDES
		$query_actualizar_moldes = "UPDATE moldes
									SET id_estatus_moldes = '7'
									WHERE folio_num_molde = '$folio_num_molde'";

		// SE EJECUTA EL QUERY
		mysql_query($query_actualizar_moldes) or die(mysql_error());

		// SE REALIZA QUERY QUE INSERTA EL MOVIMIENTO DEL MOLDE
		$query_movimiento_molde = "INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora, id_estatus_moldes, id_empleado)
								   VALUES('$folio_num_molde','3','$fecha_entrada_mysql','$hora_entrega','7','$id_empleado')";

		// SE EJECUTA EL QUERY
		mysql_query($query_movimiento_molde) or die(mysql_error());

		// SE REALIZA QUERY QUE ACTUALIZA LA TABLA DE MOLDES ELABORACION
		$query_moldes_elaboracion = "UPDATE moldes_elaboracion
									 SET fecha_entrega = '$fecha_entrega_mysql',
									 recibio = '$recibio',
									 quien_entrego = '$id_empleado'
									 WHERE folio_num_molde = '$folio_num_molde'";

		// SE EJECUTA EL QUERY
		mysql_query($query_moldes_elaboracion) or die(mysql_error());

		header('Location: lista_moldes.php');
	}

?>