<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$contador = 0;
	$total_baterias_vales = 0;
	$total_global_vales = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Bater&iacute;as que Salen </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/demos.css"/>
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ){
					var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					    instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width: 600px;">
                    Reporte de Bater&iacute;as que Salen
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <div class="contenido_proveedor">
                    <center>
                        <form name="form" action="reporte_baterias_salen.php" method="post">
                            <div class="demo">
                                <label for="from" class="textos"> Fecha Inicio: </label> &nbsp;
                                <input type="text" id="from" name="from"/> &nbsp;&nbsp;
                                <label for="to" class="textos"> Fecha Fin: </label> &nbsp;
                                <input type="text" id="to" name="to"/>
                                &nbsp;
                                 <input type="submit" name="accion" value="Buscar" title="Buscar" class="fondo_boton"/>
                            </div><!-- End demo -->
                        </form>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
                        <br />
                    </center>
                </div><!--Fin de contenido proveedor-->
                <center>
	            <?php
					// SE VALIDA SI EL USUARIO HA OPRIMIDO EL BOTON DE BUSCAR
					if( isset($_POST['accion']) && $_POST['accion'] == "Buscar"){
						// SE RECIBEN LAS VARIABLES DEL FORMULARIO
						$fecha_inicio = $_POST['from'];
						$fecha_inicio_separada = explode("/", $fecha_inicio);
						$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
						$fecha_fin = $_POST['to'];
						$fecha_fin_separada = explode("/", $fecha_fin);
						$fecha_fin_mysql = $fecha_fin_separada[2]."-".$fecha_fin_separada[1]."-".$fecha_fin_separada[0];
					?>
            		<table>
                		<tr>
                    		<td style="color:#000; font-size:22px; text-align:center;"> BATER&Iacute;AS QUE SALEN </td>
                        </tr><tr>
                    		<td style="color:#000; font-size:16px; text-align:center;"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_fin; ?> </td>
                        </tr>
                    </table>
                    <table style="width:750px;" >
                		<tr>
                    		<th style="font-size:10px;"> Fecha </th>
                            <th style="font-size:10px;"> C&oacute;digo </th>
                            <th style="font-size:10px;"> Concepto </th>
                            <th style="font-size:10px;"> Cantidad </th>
                            <th style="font-size:10px;"> Origen </th>
                            <th style="font-size:10px;"> Destino </th>
                            <th style="font-size:10px;"> Responsable </th>
                            <th style="font-size:10px;"> Observaciones </th>
                            <th style="font-size:10px;"> Costo Total </th>
                        </tr>
                        <?php
							// SE REALIZA QUERY QUE OBTIENE TODAS LAS REFACCIONES QUE SALEN DE VALES DE REFACCIONES
							$query_baterias_vales = "SELECT fecha,uso_de_bateria,responsable,observaciones,origen,codigo,cantidad
													FROM vales_baterias,descripcion_vale_baterias
													WHERE vales_baterias.id_vale_bateria = descripcion_vale_baterias.id_vale_bateria
													AND uso_de_bateria IN (1,2)
													AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_fin_mysql'
													ORDER BY fecha DESC";
							
							// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
							$resultado_baterias_vales = mysql_query($query_baterias_vales) or die(mysql_error());
							
							// SE REALIZA UN CICLO PARA MOSTRAR TODAS LAS REFACCIONES OBTENIDAS
							while( $row_baterias_vale = mysql_fetch_array($resultado_baterias_vales)){
								$contador++;
								$fecha = $row_baterias_vale['fecha'];
								$fecha_separada = explode("-", $fecha);
								$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
								$uso = $row_baterias_vale['uso_de_bateria'];
								
								// SE REALIZA QUERY QUE OBTIENE EL USO DEL VALE DE REFACCION
								$query_uso_vale = "SELECT uso_bateria
													FROM uso_baterias
													WHERE id_uso_bateria = '$uso'";
																		
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
								$resultado_uso_vale = mysql_query($query_uso_vale) or die(mysql_error());
								$row_uso_vale = mysql_fetch_array($resultado_uso_vale);
								$destino = $row_uso_vale['uso_bateria'];
								
								$responsable = $row_baterias_vale['responsable'];
								$observaciones = $row_baterias_vale['observaciones'];
								$origen = $row_baterias_vale['origen'];
								
								// SE REALIZA QUERY QUE OBTIENE EL ORIGEN DEL VALE DE REFACCION
								$query_uso_vale2 = "SELECT almacen 
													FROM almacenes 
													WHERE id_almacen = '$origen'";
																		
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
								$resultado_uso_vale2 = mysql_query($query_uso_vale2) or die(mysql_error());
								$row_uso_vale2 = mysql_fetch_array($resultado_uso_vale2);
								$origen_descripcion = $row_uso_vale2['almacen'];
								
								$codigo = $row_baterias_vale['codigo'];
								$cantidad_bateria_vale = $row_baterias_vale['cantidad'];
								
								// SE REALIZA QUERY QUE OBTIENE LOS DATOS DE CADA UNA DE LAS REFACCIONES QUE SALEN
								$query_datos_refacciones = "SELECT referencia_codigo,descripcion,precio_compra
															FROM base_productos
															WHERE id_base_producto = '$codigo'";
								
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$resultado_datos_refaccion = mysql_query($query_datos_refacciones) or die(mysql_error());
								$row_datos_refaccion = mysql_fetch_array($resultado_datos_refaccion);
								$referencia_codigo = $row_datos_refaccion['referencia_codigo'];
								$descripcion = $row_datos_refaccion['descripcion'];
								$precio_compra = $row_datos_refaccion['precio_compra'];
								$total_baterias_vales = $precio_compra * $cantidad_bateria_vale;
								$total_global_vales += $total_baterias_vales;
								
								// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO RESPONSABLE
								$query_nombre_empleado_responsable = "SELECT nombre
																	  FROM empleados
																	  WHERE id_empleado = '$responsable'";
																													
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
								$resultado_nombre_empleado = mysql_query($query_nombre_empleado_responsable) or die(mysql_error());
								$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
								$nombre_empleado_vale = $row_nombre_empleado['nombre'];
                        ?>
                    	<tr>
                    		<td style="font-size:9px; text-align:center;"> <?php  echo $fecha_normal; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $referencia_codigo; ?>  </td>
                            <td style="font-size:9px;"> <?php  echo $descripcion; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $cantidad_bateria_vale; ?> </td>
                            <td style="font-size:9px;"> <?php  echo $origen_descripcion; ?> </td>
                            <td style="font-size:9px;"> <?php  echo $destino; ?></td>
                            <td style="font-size:9px;"> <?php  echo $nombre_empleado_vale; ?> </td>
                            <td style="font-size:9px;"> <?php  echo $observaciones;?>  </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo "$".number_format($total_baterias_vales,2); ?> </td>
                        </tr>
                        <tr>
                    		<td colspan="9"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                        </tr>
                        <?php
                            }                                                
                        // SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
                            $query_ventas_notas = "SELECT fecha,ventas.folio_num_venta AS folio, descripcion, 
                                                        cantidad,vendedor, sucursales.nombre as sucursal
                                                        FROM descripcion_venta,ventas, sucursales
                                                        WHERE ventas.folio_num_venta = descripcion_venta.folio_num_venta
                                                        AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_fin_mysql'
                                                        AND id_categoria=4
                                                        AND descripcion_venta.id_sucursal=sucursales.id_sucursal
                                                        AND ventas.id_sucursal=sucursales.id_sucursal
                                                        AND descripcion_venta.id_sucursal=ventas.id_sucursal
                                                        ORDER BY fecha DESC";
                            
                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                            $resultado_ventas_notas = mysql_query($query_ventas_notas) or die(mysql_error());
                            // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                            $total_global_notas=0;
                            while($row_venta_nota = mysql_fetch_array($resultado_ventas_notas)){
                                $contador++;
                                $fecha_entrada = $row_venta_nota['fecha'];
                                $fecha_entrada_separada = explode("-", $fecha_entrada);                             
                                $fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
                                $folio_venta = $row_venta_nota['folio'];
                                $codigo_venta = $row_venta_nota['descripcion'];
                                $cantidad = $row_venta_nota['cantidad'];
                                $id_empleado = $row_venta_nota['vendedor']; 
                                $sucursal=$row_venta_nota['sucursal'];                                                          
                                // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE CADA UNA DE LAS REFACCIONES QUE SALEN
                                $query_datos_ventas = "SELECT referencia_codigo,descripcion,precio_compra
                                                            FROM base_productos
                                                            WHERE descripcion = '".$codigo_venta."'";
                                
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                                $resultado_datos_venta = mysql_query($query_datos_ventas) or die(mysql_error());
                                $row_datos_venta = mysql_fetch_array($resultado_datos_venta);
                                $referencia_codigo = $row_datos_venta['referencia_codigo'];
                                $descripcion = $row_datos_venta['descripcion'];
                                $precio_compra = $row_datos_venta['precio_compra'];                             
                                $total_venta = $precio_compra * $cantidad;
                                $total_global_notas += $total_venta;                                
                                // SE REALIZA QUERY QUE OBTIENE EL RESPONSABLE
                               
                                // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO RESPONSABLE
                                $query_nombre_empleado_responsable = "SELECT nombre
                                                                    FROM empleados
                                                                    WHERE id_empleado = '".$id_empleado."'";
                                                                                                                    
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                $resultado_nombre_empleado = mysql_query($query_nombre_empleado_responsable) or die(mysql_error());
                                $row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
                                $nombre_empleado_nota = $row_nombre_empleado['nombre'];
                                ?>
                        <tr>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $fecha_entrada_normal; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $referencia_codigo; ?>  </td>
                            <td style="font-size:9px;"> <?php  echo $codigo_venta; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $cantidad; ?></td>
                            <td style="font-size:9px;"> <?php echo $sucursal; ?> </td>
                            <td style="font-size:9px;"> Venta N° <?php echo $folio_venta; ?> </td>
                            <td style="font-size:9px;"> <?php  echo $nombre_empleado_nota; ?> </td>
                            <td style="font-size:9px;">  </td>
                            <td style="font-size:9px; text-align:right"><?php  echo "$".number_format($total_venta,2); ?> </td>
                        </tr>
                        <tr>
                            <td colspan="9"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                        </tr>
                        <?php
                            }

//-------------                             
                            $total_global = $total_global_notas + $total_global_vales;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:center; color:#ac1f1f;"> Total: <br /> <?php echo "$".number_format($total_global,2); ?> </td>
                        </tr>
                        <?php
                            if($contador == 0){
                        ?>
                        <tr>
                            <td colspan="9" style="text-align:center;"> <h3> No existen registros entre el <?php echo $fecha_inicio; ?> y el <?php echo $fecha_fin; ?> </h3> </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </table>                
                    <br /><br />
                    <?php
						// SE VALIDA SI EL CONTADOR ES MAYOR A CERO
						if($contador > 0){
					?>
            		<div id="opciones" style="float:right; margin-right:70px;">
                        <form name="form_reporte" style="float:right; margin-left:20px;" method="post" action="reporte_baterias_salen_excel.php">
                            <div id="spanreloj"></div>
                            <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                            <input type="hidden" name="fecha_final" value="<?php echo $fecha_fin_mysql; ?>" />
                            <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                        </form> &nbsp;&nbsp;
                        <a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_reporte_baterias_salen.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_fin_mysql; ?>','Reporte de Baterias que Salen','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1250, height=700');"> <img src="../img/print icon.png"/> </a>
                    </div> <!-- FIN DIV OPCIONES -->
                <?php
						}
					}
				?>
                <br /><br />
                </center>
                <br />
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>