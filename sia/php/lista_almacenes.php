<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Almacenes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">      
  	<div id="contenido_columna2">    
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Almacenes
                </div>
            </div><!--Fin de fondo titulo-->
			<?php 
				include("config.php");
				if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
					$filtro = $_POST['filtro'];
					$res_busqueda = mysql_query("SELECT COUNT(*) 
														FROM almacenes
														WHERE almacen LIKE '%".$filtro."%' 
														ORDER BY id_almacen");														
					$row_busqueda = mysql_fetch_array($res_busqueda);
					$res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
				}else{
					$res2="";
				}
			?>         
           	<div class="buscar2">
            	<form name="busqueda" method="post" action="lista_almacenes.php">
                	<label class="textos"><?php echo $res2; ?></label>
                    <input name="filtro" type="text" size="15" maxlength="15" />
                    <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />							
                </form>
          	</div><!-- Fin de la clase buscar --> 
            <div class="area_contenido2">
            	<center>
   				<?php
					if(!isset($_POST['filtro'])){	
				?>
                <table>
                    <tr>
                        <th style="text-align:center;">Sucursal</th>
                        <th style="text-align:center;">Almacen</th>
                    </tr>
					<?php 
						$consulta_lista_sucursal = mysql_query("SELECT id_sucursal, nombre FROM sucursales")or die(mysql_error());
						while($row_lista_sucursales = mysql_fetch_array($consulta_lista_sucursal)){
							$id_sucursal = $row_lista_sucursales["id_sucursal"];
							$nombre_sucursal = $row_lista_sucursales["nombre"];
					?>
                    <tr>
                        <td style="text-align:center;">
                            <label class="textos"><?php echo $nombre_sucursal; ?></label>
                        </td>
                         <td id="alright">
                        	<table style="width:500px !important;">
    					<?php 
     						$consulta_almacen = mysql_query("SELECT * FROM almacenes WHERE id_sucursal=".$id_sucursal) 
																							or die(mysql_error());
				
                			while($row = mysql_fetch_array($consulta_almacen)){
                  				$almacen = $row["almacen"];
                    			$id_almacen = $row["id_almacen"];
   						?>
                        		<tr>
                                	<td id="alleft">
										<label class="textos"><?php echo $almacen."<br />"; ?></label>
                            		</td><td id="alright">
                                        <a href="modificar_almacen.php?id_almacen=<?php echo $id_almacen; ?>">
                                            <img src="../img/modify.png" />
                                        </a>
                            		</td>
                            	</tr>
   						<?php 					
							}
						?>
                        	</table>
                        </td>
                    </tr>
   					<?php 
                    	}                 
					?>
                </table>
				<?php
                    }else{
                ?>
                <table style="border-collapse:collapse;">
                    <tr>
                        <th style="text-align:center;">Sucursal</th>
                        <th style="text-align:center;">Almacen</th>
                    </tr>
    				<?php
						$filtro = $_POST['filtro'];					
                    	$consulta_almacenes = mysql_query("SELECT id_sucursal
                                                                FROM almacenes
                                                                WHERE almacen LIKE '%".$filtro."%'  
                                                                GROUP BY id_sucursal");
						$n_almacen=0;
						while($row = mysql_fetch_array($consulta_almacenes)){
							$id_sucursal = $row["id_sucursal"];
							$consulta_nombre_sucursales=mysql_query("SELECT nombre FROM sucursales 
																					WHERE id_sucursal=".$id_sucursal)
																					or die(mysql_error());
							$row_nombre_sucursal=mysql_fetch_array($consulta_nombre_sucursales);
							$nombre_sucursal = $row_nombre_sucursal["nombre"];
							$n_almacen++;
						?>
                    <tr>
                        <td style="text-align:center; border-bottom-style:ridge;">
                            <label class="textos"><?php echo $nombre_sucursal; ?></label>
                        </td>
                        <td id="alright" style="border-bottom-style:ridge;">
                            <table style="width:500px;">
    							<?php
									$consulta_almacenes2 = mysql_query("SELECT DISTINCT(id_almacen), almacen, id_sucursal
																		FROM almacenes
																		WHERE almacen LIKE '%".$filtro."%' and id_sucursal='$id_sucursal' 
																		ORDER BY id_sucursal");
									while($row2 = mysql_fetch_array($consulta_almacenes2)){
										$almacen = $row2["almacen"];
										$id_almacen = $row2["id_almacen"];

   								?>
                                <tr>
                                	<td id="alleft">
                                        <label class="textos">
                                            <?php echo $almacen."<br />"; ?>
                                        </label>
   									</td><td id="alright">
                                        <a href="modificar_almacen.php?id_almacen=<?php echo $id_almacen; ?>">
                                            <img src="../img/modify.png" />
                                        </a>
									</td>
                                </tr>    
								<?php 					
									}							
								?>
                        	</table>
                        </td>  
                   	</tr>                      
					<?php
                        }
                        if($n_almacen==0){
                    ?>
                    <tr>
                        <td style="text-align:center;" colspan="2">
                            <label class="textos">"No hay almacenes registrados"</label>
                        </td>
                    </tr> 
					<?php
              				}
						}
					?>
                </table>
                </center>
                <br />                    
                <div class="contenido_proveedor">
                    <form name="forma1" action="proceso_guarda_almacen.php" method="post">
                    <table>
                    	<tr>
                        	<th colspan="2">
                            	Nuevo Almacen
                            </th>
                        </tr><tr>
                        	<td id="alright">
                    			<label class="textos">Sucursal: </label>
                        	</td><td id="alleft">
                                <select name="sucursal" style="width:200px;">
                                    <option value="0">Seleccione</option>
                                    <?php
                                        $consulta_sucursales = mysql_query("SELECT id_sucursal, nombre FROM sucursales") or die(mysql_error());
                                        while($row_sucursales = mysql_fetch_array($consulta_sucursales)){
                                            $nombre_sucursal = $row_sucursales["nombre"];
                                            $id_sucursal = $row_sucursales["id_sucursal"];
                                    ?>
                                    <option value="<?php echo $id_sucursal; ?>"><?php echo $nombre_sucursal; ?></option>
                                    <?php 		
                                        }
                                    ?>
                                </select>
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Almacen: </label>
                        	</td><td id="alleft">
                        		<input name="almacen" id="almacen" type="text"  style="width:200px;" />
                        	</td>
                    	</tr><tr>
                        	<td id="alright" colspan="2">
                    			<input name="cancelar" type="reset" value="Cancelar" class="fondo_boton"/>
                    			<input name="guarda" type="submit" value="Guardar" class="fondo_boton"/>
                    		</td>
                    	</tr>
                    </table>
                    </form>
            	</div><!-- Fin de contenido proveedor -->
        	</div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>