<?php
	error_reporting(0);

	// SE INICIA SESION PARA EL USUARIO QUE ENTRO AL SISTEMA
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Reparaciones </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript">
		$(function() {
     		//call the function onload, default to page 1
     		getdata( 1 );
		});

		function getdata( pageno ){                     
     		var targetURL = 'lista_reparaciones_entregadas_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons

     		$('#tabla_lista_reparaciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
     		$('#tabla_lista_reparaciones').load( targetURL,{
     			filtro: "<?php echo $_POST['filtro']; ?>"
     		} ).hide().fadeIn('slow');
		}    
		function getdata2( ){   
			var valor = $('#paginas option:selected').attr('value');
			var targetURL = 'lista_reparaciones_entregadas_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
			$('#tabla_lista_reparaciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
			$('#tabla_lista_reparaciones').load( targetURL,{
				filtro: "<?php echo $_POST['filtro']; ?>"
			} ).hide().fadeIn('slow');                  
			
		} 
</script>
	<style type="text/css">
		div.contenedor 
		{
			position: relative;
			width: 140px;
			left: 107px;
		}
		
		#input 
		{
			font-family: Arial;
			color: #000;
			font-size: 10pt;
			width: 140px;
		}

		div.fill 
		{
			font-family: Arial;
			font-size: 10pt;
			display: none;
			width: 197px;
			color: #CCC;
			background-color: #CCC;
			border: 1px solid #999;
			overflow: auto;
			height: auto;
			top: -1px;
		}

		tr.fill 
		{
			font-family: Arial;
			font-size: 8pt;
			color: #FFF;
			background-color: #a2a2a3;
			border: 1px solid #a2a2a3;
		}

		#tr 
		{
			font-family: Arial;
			font-size: 8pt;
			background-color: #d9dcdf;
			color: #000;
			border: 1px solid #d9dcdf;
		}
		.tamano_letra
		{
			font-size:10px;	
		}

		.titulo
		{
			font-size: 11px;
		}
	</style>
</head>
<body>
	<div id="wrapp">
		<div id="contenido_columna2">
			<div class="contenido_pagina">
				<div class="fondo_titulo1">
            		<div class="categoria">
            			Reparaciones
            		</div>
        		</div><!--Fin de fondo titulo-->
    			<?php 
					date_default_timezone_set('America/Monterrey');
					$script_tz = date_default_timezone_get();
					$fecha = date("d/m/Y");
					$hora = date("h:i:s A");
            		if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
            		{
                		$filtro = $_REQUEST['filtro'];

                		// SE REALIZA QUERY PARA OBTENER EL RESULTADO DE LA BUSQUEDA REALIZADA
                		$res_busqueda = mysql_query("SELECT count(DISTINCT(folio_num_reparacion)) AS contador
                                                     FROM reparaciones, base_productos_2
                                                     WHERE (nombre LIKE '%".$filtro."%' 
													 OR paterno LIKE '%".$filtro."%'
													 OR materno LIKE '%".$filtro."%'
													 OR num_serie LIKE '%".$filtro."%'
													 OR folio_num_reparacion LIKE '%".$filtro."%'
													 OR base_productos_2.descripcion LIKE '%".$filtro."%')
													 AND base_productos_2.id_base_producto2 = reparaciones.id_modelo
													 AND id_estatus_reparaciones = 3") or die(mysql_error());

                		// SE ALMACENA EN UNA VARIABLE EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
						$row_busqueda = mysql_fetch_array($res_busqueda);
						$busqueda += $row_busqueda["contador"];

						// SE VALIDA SI LA BUSQUEDA OBTUVO RESULTADOS
						if( $busqueda == 0 )
						{
							$busqueda = 0;	
						}
                		$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
            			}
            			else
            			{
               				$res2 = "";
            			}
				?> 
        			<div class="buscar2">
						<form name="busqueda" method="post" action="lista_reparaciones_entregadas.php">
                        	<label class="textos"> <?php echo $res2; ?> </label>
                        	<input type="text" name="filtro" size="15" maxlength="15" />
                        	<input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
                    	</form>
       				</div><!-- FIN DIV BUSCAR 2 -->
                    <div class="area_contenido2">
    					<center>
    						<table>
    							<tr>
    								<th colspan="8"> REPARACIONES ENTREGADAS </th>
    							</tr>
    						</table>
							<div id="tabla_lista_reparaciones">
								<img src="../img/ajax-loader.gif"/>
							</div>
                      	</center>
                        <br />
        			</div><!-- Fin de area contenido -->
			</div><!--Fin de contenido pagina-->
		</div><!--Fin de contenido columna 2-->
	</div><!--Fin de wrapp-->
</body>
</body>
</html>