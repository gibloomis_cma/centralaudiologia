<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL FOLIO DE CORTE A TRAVES DE METODO POST
	$folio_corte = $_POST['folio_corte'];
	$id_sucursal_empleado = $_POST['id_sucursal'];

	// SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DEL CORTE DE VENTAS DE ACUERDO AL FOLIO DE CORTE RECIBIDO
	$query_corte_venta = "SELECT folio_corte,id_sucursal,id_empleado_corte,total_vendido,total_articulos,fecha_corte
						  FROM corte_ventas_diario
						  WHERE folio_corte = '$folio_corte'
						  AND id_sucursal = '$id_sucursal_empleado'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_corte_venta = mysql_query($query_corte_venta) or die(mysql_error());

	//SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_corte_venta = mysql_fetch_array($resultado_corte_venta);

	// SE DECLARAN VARIABLES CON EL RESULTADO OBTENIDO DEL QUERY
	$num_folio_corte = $row_corte_venta['folio_corte'];
	$id_sucursal = $row_corte_venta['id_sucursal'];
	$id_empleado = $row_corte_venta['id_empleado_corte'];
	$total_vendido = $row_corte_venta['total_vendido'];
	$total_articulos = $row_corte_venta['total_articulos'];
	$fecha_corte = $row_corte_venta['fecha_corte'];
	$fecha_corte_separada = explode("-", $fecha_corte);
	$fecha_corte_normal = $fecha_corte_separada[2]."/".$fecha_corte_separada[1]."/".$fecha_corte_separada[0];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL DE DONDE SE REALIZO EL CORTE DE VENTAS
	$query_nombre_sucursal = "SELECT nombre
							  FROM sucursales
							  WHERE id_sucursal = '$id_sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_sucursal = mysql_query($query_nombre_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_nombre_sucursal = mysql_fetch_array($resultado_nombre_sucursal);

	// SE DECLARA UNA VARIABLE CON EL RESULTADO FINAL DEL QUERY
	$nombre_sucursal = $row_nombre_sucursal['nombre'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL RESPONSABLE QUE REALIZO EL CORTE DE VENTAS
	$query_nombre_empleado = "SELECT CONCAT(nombre,' ',paterno) AS nombre_completo
							  FROM empleados
							  WHERE id_empleado = '$id_empleado'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_empleado = mysql_query($query_nombre_empleado) or die(mysql_error());

	// SE ALMACENA EL RESULTADO OBTENIDO EN FORMA DE ARREGLO
	$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);

	// SE DECLARA UNA VARIABLE CON EL RESULTADO FINAL OBTENIDO
	$nombre_empleado = $row_nombre_empleado['nombre_completo'];

	// SE IMPRIMEN LAS VARIABLES PARA MOSTRARLAS EN PANTALLA
	echo $num_folio_corte;
	echo "°";
	echo $fecha_corte_normal;
	echo "°";
	echo ucfirst(strtolower($nombre_sucursal));
	echo "°";
	echo $nombre_empleado;
	echo "°";
	echo "$".number_format($total_vendido,2);
	echo "°";
	echo $total_articulos;
	echo "°";
	echo $fecha_corte;
	echo "°";
	echo $id_empleado;
?>