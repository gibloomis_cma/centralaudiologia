<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Departamentos</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">    
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Departamentos
                </div>
            </div><!--Fin de fondo titulo-->
			<?php 
				include("config.php");
				if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
					$filtro = $_POST['filtro'];
					$res_busqueda = mysql_query("SELECT COUNT(*) 
														FROM areas_departamentos
														WHERE departamentos LIKE '%".$filtro."%' 
														ORDER BY id_departamento");														
					$row_busqueda = mysql_fetch_array($res_busqueda);
					$res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
				}else{
					$res2="";
				}
			?>
            <div class="buscar2">
                <form name="busqueda" method="post" action="lista_departamentos.php">
                	<label class="textos"><?php echo $res2; ?></label>
                    <input name="filtro" type="text" size="15" maxlength="15" />
                    <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
                </form>
            </div><!-- Fin de la clase buscar -->
            <div class="area_contenido2">
                <center>
    			<?php
					if(!isset($_POST['filtro'])){	
				?>
                <table>
                    <tr>
                        <th style="text-align:center;">Sucursal</th>
                        <th style="text-align:center;">Departamentos</th>
                    </tr>
					<?php 
						$consulta_lista_sucursal = mysql_query("SELECT id_sucursal, nombre FROM sucursales")
																						or die(mysql_error());
						while($row_lista_sucursales = mysql_fetch_array($consulta_lista_sucursal)){
							$id_sucursal = $row_lista_sucursales["id_sucursal"];
							$nombre_sucursal = $row_lista_sucursales["nombre"];
					?>
                    <tr>
                        <td style="text-align:center;">
							<label class="textos"><?php echo $nombre_sucursal; ?></label>
                        </td>
                        <td id="alright">
                        	<table style="width:500px;">
							<?php
                                $consulta_departamentos = mysql_query("SELECT * FROM areas_departamentos 
                                                                                    WHERE id_sucursal=".$id_sucursal) 
                                                                                    or die(mysql_error());
                    
                                while($row = mysql_fetch_array($consulta_departamentos)){
                                    $departamentos = $row["departamentos"];
                                    $id_departamento = $row["id_departamento"];
                            ?>
                        		<tr>
                                	<td id="alleft">
                                    	<label class="textos">
											<?php echo $departamentos; ?>
                                        </label>
                                    </td><td id="alright">
                                    	<a href="modificar_departamentos.php?id_departamento=<?php echo $id_departamento; ?>">
                                        	<img src="../img/modify.png" title="<?php echo $departamentos; ?>" />
                                        </a>
                                    </td>
                            	</tr>                            
							<?php 					
                                }
                            ?>
                        	</table>
                        </td>
                    </tr>
                    <?php 
                    	}                 
                    ?>
                </table>
    			<?php
					}else{
				?>
                <table>
                    <tr>
                        <th style="text-align:center;">Sucursal</th>
                        <th style="text-align:center;">Departamentos</th>
                    </tr>
    				<?php
						$filtro = $_POST['filtro'];					
                    	$consulta_departamentos = mysql_query("SELECT DISTINCT(id_sucursal)
                                                                FROM areas_departamentos
                                                                WHERE departamentos LIKE '%".$filtro."%'  
                                                                ORDER BY id_sucursal");
						$n_departamento=0;
						while($row = mysql_fetch_array($consulta_departamentos)){
							$id_sucursal = $row["id_sucursal"];
							$consulta_nombre_sucursales=mysql_query("SELECT nombre FROM sucursales 
																					WHERE id_sucursal=".$id_sucursal)
																					or die(mysql_error());
							$row_nombre_sucursal=mysql_fetch_array($consulta_nombre_sucursales);
							$nombre_sucursal = $row_nombre_sucursal["nombre"];
							$n_departamento++;
					?>
                    <tr>
                        <td style="text-align:center;">
							<label class="textos"><?php echo $nombre_sucursal; ?></label>
                        </td><td>
                        	<table style="width:500px;">
							<?php
                                $consulta_departamentos2 = mysql_query("SELECT DISTINCT(id_departamento), departamentos, id_sucursal
                                                                                    FROM areas_departamentos
                                                                                    WHERE departamentos LIKE '%".$filtro."%' AND
                                                                                     id_sucursal='$id_sucursal' 
                                                                                    ORDER BY id_sucursal");
                                while($row2 = mysql_fetch_array($consulta_departamentos2)){
                                    $departamentos = $row2["departamentos"];
                                    $id_departamento = $row2["id_departamento"];
    
                            ?>
                        		<tr>
                                	<td id="alleft">
                                    	<label class="textos">
											<?php echo $departamentos; ?>
                                        </label>
                                    </td><td id="alright">
                                    	<a href="modificar_departamentos.php?id_departamento=<?php echo $id_departamento; ?>">
                                        	<img src="../img/modify.png" title="<?php echo $departamentos; ?>" />
                                        </a>
                                    </td>
                            	</tr>                            
							<?php 						
								}
							?>
                        	</table>
                        </td>  
                    </tr>                      
    				<?php
						}
						if($n_departamento==0){
					?>
                    <tr>
                        <td style="text-align:center;" colspan="2">
                            <label class="textos">"No hay departamentos registrados"</label>
                        </td>
                    </tr> 
				<?php
              			}
					}
				?>
                </table> 
                </center>
                <br />                
                <div class="contenido_proveedor">
                    <form name="forma1" action="proceso_guarda_departamento.php" method="post">
                    <table>
                    	<tr>
                        	<th colspan="2">
                            	Agregar Departamento
                            </th>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Sucursal</label>
                        	</td><td id="alleft">
                                <select name="sucursal" style="width:200px;">
                                    <option value="0">Seleccione</option>
                                    <?php
                                        $consulta_sucursales = mysql_query("SELECT id_sucursal, nombre FROM sucursales") or die(mysql_error());
                                        while($row_sucursales = mysql_fetch_array($consulta_sucursales)){
                                            $nombre_sucursal = $row_sucursales["nombre"];
                                            $id_sucursal = $row_sucursales["id_sucursal"];
                                    ?>
                                    <option value="<?php echo $id_sucursal; ?>"><?php echo $nombre_sucursal; ?></option>
                                    <?php 		
                                        }
                                    ?>
                                </select>
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Departamento: </label>
                        	</td><td id="alleft">
                        		<input name="departamento" type="text" style="width:200px;" />
                   		</td><tr>
                        	<td colspan="2" id="alright">
                                <input name="cancelar" type="reset" value="Cancelar" class="fondo_boton"/>
                                <input name="guarda" type="submit" value="Guardar" class="fondo_boton"/>
                        	</td>
                        </tr>
                    </table>                        
                    </form>
             	</div><!-- Fin de contenido proveedor -->
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>