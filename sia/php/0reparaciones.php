<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Reparaciones </title>
</head>
<body>
<style type="text/css">
	table{
		border:1px solid #900;		
		margin: 0 auto;
		border-collapse: collapse;
	}
	table tr th{
		background-color: #900;
		color: #FFF;
		padding: 5px 10px;
	}
	table tr td{
		color: #000;
		padding: 5px 10px;
		border:1px solid #900;
		text-align: center;
	}
	.aligns{
		text-align: left;
	}
</style>
<table>
	<tr>
		<th>FOLIO</th>
		<th>FECHA DE SALIDA</th>
		<th>REPARADO POR</th>
		<th>NO REPARADO</th>
		<th>ESTATUS</th>
		<th>MOVIMIENTOS</th>
	</tr>
<?php
	include('config.php');
	//Consulta de las reparaciones de Noviembre
	$errores=mysql_query('SELECT no_reparado, fecha_salida, reparado_por, reparaciones.folio_num_reparacion as nofolio, estado_reparacion
						  FROM reparaciones, reparaciones_laboratorio, estatus_reparaciones
						  WHERE reparaciones.folio_num_reparacion = reparaciones_laboratorio.folio_num_reparacion
						  AND  fecha_entrada >=  "2012-11-01"
						  AND reparaciones.id_estatus_reparaciones = estatus_reparaciones.id_estatus_reparacion
						  ORDER BY fecha_salida')
						  or die(mysql_error());
	$cambio_color=0;
	while($row_errores=mysql_fetch_array($errores)){		
		$folio=$row_errores['nofolio'];
		$fecha_salida=$row_errores['fecha_salida'];
		$reparado_por=$row_errores['reparado_por'];
		$no_reparado=$row_errores['no_reparado'];
		$estatus=$row_errores['estado_reparacion'];
		//Poner guiones cuando no este reparado
		if ($no_reparado!="") {
			$no_reparado="--";			
		}
		//Consulta de los datos del empleado que reparo
		$datos_empleado=mysql_query('SELECT * FROM empleados WHERE id_empleado='.$reparado_por)or die('Datos del Empleado');
		$row_empleado=mysql_fetch_array($datos_empleado);
		$empleado=$row_empleado['nombre']." ".$row_empleado['paterno']." ".$row_empleado['materno'];
		//Cambiar el color de los tr
		if($cambio_color==0){
			$colors=" style='background-color:#CCC'";
			$cambio_color=1;
		}else{
			$colors=" style='background-color:#EEE'";
			$cambio_color=0;
		}
		//Consulta de movimientos de la reparacion
		$movimientos=mysql_query('SELECT fecha, nombre, paterno, materno, estado_reparacion, id_estatus_reparacion, empleados.id_empleado as id
								  FROM movimientos, estatus_reparaciones, empleados
								  WHERE movimientos.id_empleado = empleados.id_empleado
								  AND id_estatus_reparaciones = id_estatus_reparacion
								  AND folio_num_reparacion='.$folio)
								  or die('Consulta de movimientos de la reparacion');
		$lista="";$imprime=0;
		while($row_movimientos=mysql_fetch_array($movimientos)){
			$id=$row_movimientos['id_estatus_reparacion'];
			$estado=$row_movimientos['estado_reparacion'];
			$fecha=$row_movimientos['fecha'];
			$movio=$row_movimientos['id'].".- ".$row_movimientos['nombre']." ".$row_movimientos['paterno']." ".$row_movimientos['materno'];				
			//Detalles del movimiento		
			if($id==8 or $id==12){
				$lista.=$estado."<br/>".$fecha;
				$imprime=1;			
			}
			if($id==5){
				$lista.=$movio."<br/> ";				
			}
		}
		if($imprime!=1){
			$lista="";
		}
		echo "<tr ".$colors.">
				<td>".$folio."</td>
				<td>".$fecha_salida."</td>
				<td class='aligns'>".$empleado."</td>
				<td>".$no_reparado."</td>
				<td class='aligns'>".$estatus."</td>
				<td class='aligns'>".$lista."</td>
			  </tr>
			 ";
	}
?>
</table>
</body>
</html>