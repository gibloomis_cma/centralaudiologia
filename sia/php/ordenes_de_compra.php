<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8/iso-8859-1" />
	<title>Lista de Ordenes de Compra</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<!-- orden_de_compra.php -->
	<form name="forma1" id="forma1" method="post" action="orden_de_compra.php">
		<div id="contenido_columna2">
			<div class="contenido_pagina">    
				<div class="fondo_titulo1">        
					<div class="categoria">
						Proveedores
					</div><!-- Fin DIV categoria -->           
				</div><!-- Fin de fondo titulo1 -->
					<div class="area_contenido1">
						<br />
							<div class="contenido">								
									<div class="etiqueta">
                                    <label class="textos">
										<?php
                                        	include("config.php");	
											$cuentas=mysql_query('SELECT folio_orden_compra 
																FROM cuentas_por_pagar')or die(mysql_error());									
											while($row_cuentas=mysql_fetch_array($cuentas)){
												$folio_orden_compra=$row_cuentas['folio_orden_compra'];
												mysql_query('UPDATE ordenes_de_compra 
															SET id_estatus = 1 
															WHERE folio_orden_compra='.$folio_orden_compra)
															or die(mysql_error());
											}
										// SE GUARDA EN UNA VARIABLE EL ID DEL PROVEEDOR 
											$id_proveedor = $_GET['id_proveedor'];
										// SE REALIZA EL QUERY QUE SELECCIONA EL NOMBRE DEL PROVEEDOR DE ACUERDO AL ID SELECCIONADO
											$query_nombre_proveedor = mysql_query("SELECT razon_social 
																					FROM proveedores 
																					WHERE id_proveedor = ".$id_proveedor) 
																					or die (mysql_error());
											$row = mysql_fetch_array($query_nombre_proveedor);
											$nombre_proveedor = $row['razon_social'];
										// SE IMPRIME EN PANTALLA EL NOMBRE DEL PROVEEDOR CORRESPONDIENTE
											echo $nombre_proveedor;
										// SE REALIZA EL QUERY PARA OBTENER LAS ORDENES DE COMPRA DE ACUERDO AL PROVEEDOR SELECCIONADO
											$ordenes_compra = mysql_query("SELECT folio_orden_compra, SUM(cantidad),
																			fecha_orden, estatus_orden 
																			FROM ordenes_de_compra, estatus_ordenes 
																			WHERE id_proveedor = ".$id_proveedor." 
																			AND id_estatus_orden = id_estatus  																	 																			GROUP BY folio_orden_compra 
																			ORDER BY estatus_orden") 
																			or die (mysql_error());											
										?>
                                        </label>
									</div> <!-- FIN DIV CLASS ETIQUETA -->
							</div> <!-- FIN DIV CLASS CONTENIDO -->
							<br /><br />
							<center>	
                            	<style type="text/css">
									table tr td{
										text-align:center !important;										
									}									
                                </style>							
                                <table>
                                    <tr>
                                        <th colspan="5">
                                            Ordenes de Compra
                                        </th>
                                    </tr><tr>
                                        <th>Folio</th>
                                        <th>Fecha de Orden</th>
                                        <th>Estatus</th>
                                        <th colspan="2">Articulos</th>                                        
                                    </tr>
                                <?php
                                    while( $row = mysql_fetch_array($ordenes_compra)){
                                        $folio = $row['folio_orden_compra'];
                                        $fecha_orden = $row['fecha_orden'];
                                        $estatus = $row['estatus_orden'];
                                        $cantidad = $row['SUM(cantidad)'];
                                    ?>		
                                    <tr>
                                        <td>                                         
											<?php echo $folio; ?>
                                        </td>
                                        <td> <?php echo $fecha_orden; ?> </td>
                                        <td> <?php echo $estatus; ?> </td>
                                        <td> <?php echo $cantidad; ?> </td>                                        	 
                                        <td id="alleft">
                                        	<a href="detalles_orden_compra.php?folio=<?php echo $folio?>&id_proveedor=<?php echo $id_proveedor; ?>" target="_self" >
                                            	<img src="../img/info.png" title="Folio <?php echo $folio; ?>"/>
                                            </a>                                       	
											<?php
												if($estatus=="No Enviada"){
											?>                                            
                                            <a href="eliminar_orden_compra.php?folio=<?php echo $folio; ?>&id_proveedor=<?php echo $id_proveedor; ?>" target="_self" >
                                            	<img src="../img/delete.png" title="Folio <?php echo $folio ?>"/>
                                            </a>
                                            <?php
												}
											?>
                                        </td>                                      
                                    </tr>
                                <?php
                                    }
                                ?>	<tr>
                                		<td colspan="5">
                                        	<hr />
                                        </td>
                                	</tr><tr>
                                    	<td style="text-align:center" colspan="5">
                                            <input type="hidden" name="proveedor" value="<?php echo $id_proveedor; ?>" />
                                            <input type="button" name="volver" value="Volver"
                                            onclick="window.location.href='lista_proveedores_orden.php'" class="fondo_boton" />
                                            <input name="accion" type="submit" value="Generar Nueva Orden" 
                                            class="fondo_boton" title="Generar Nueva Orden"/>
                                        </td>
                                    </tr>
                                </table>									                               
							</center>
					</div><!--Fin de area contenido-->
			</div><!--Fin de contenido pagina-->
		</div><!--Fin de contenido columna2-->
	</form>
</body>
</html>