<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agregar Base Producto</title>
<script src="../js/jquery.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>

<script>
$(document).ready(function()
{
	$("ul.tabs li").click(function()     //cada vez que se hace click en un li
       {
		$("ul.tabs li").removeClass("active"); //removemos clase active de todos
		$(this).addClass("active"); //añadimos a la actual la clase active
		$(".tab_content").hide(); //escondemos todo el contenido
 
		var content = $(this).find("a").attr("href"); //obtenemos atributo href del link
		$(content).fadeIn(); // mostramos el contenido
		return false; //devolvemos false para el evento click
	});
});
</script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
</head>
<body>
<div id="wrapp">
<div id="contenido_columna2">
	<div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Inventario
            </div>
        </div><!--Fin de fondo_titulo1-->
        <div class="area_contenido1">
<!-----------------------------Aqui esta el formulario para crear base de producto---------------------------->
        	<div class="titulos" style="margin-top:8px;">
                Agregar Nuevo
            </div>
            <div class="contenido_proveedor" style="margin-top:8px;">
        		<ul class="tabs">
                    <li class="active"><a href="#tab1"><label class="textos">Refacciones/Laboratorio</label></a></li>
                    <li><a href="#tab2"><label class="textos">Baterias</label></a></li>
                    <li><a href="#tab3"><label class="textos">Auxiliares</label></a></li>
                    <li><a href="#tab4"><label class="textos">Accesorios</label></a></li>
                </ul>
                <div class="tab_container">
                    <div id="tab1" class="tab_content">    
                    	<table>                    
                        <form name="form1" action="procesa_nuevo_base_producto.php" method="post" onsubmit="return validaAgregarBaseProducto()">
                            <input name="categoria1" value="1" type="hidden" />
                            <tr>
                                <td id="alright">
                                    <label class="textos">Subcategoria: </label>
                                </td><td id="alleft">
                                    <select name="subcategoria1" id="subcategoria1" style="width:300px">
                                        <option value="">Seleccione</option>
                                        <?php
                                            include('config.php');
                                            $consulta_subcategorias = mysql_query("SELECT subcategoria, id_subcategoria
                                                                                    FROM subcategorias_productos
                                                                                    WHERE id_categoria = 1 AND id_subcategoria <> 1") 
                                                                                    or die(mysql_error());
                                            while($row = mysql_fetch_array($consulta_subcategorias)){
                                                $subcategoria = $row["subcategoria"];
                                                $id_subcategoria = $row["id_subcategoria"];
                                        ?>
                                        <option value="<?php echo $id_subcategoria; ?>">
                                            <?php echo $subcategoria; ?>
                                        </option>
                                        <?php 
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr><tr>
                                <td id="alright">
                                    <label class="textos">Proveedores: </label>
                                </td><td id="alleft">
                                    <select name="proveedor" style="width:300px">
                                        <option value="0">Seleccione</option>
                                        <?php
                                            $consulta_proveedores = mysql_query("SELECT razon_social, id_proveedor 
                                                                                FROM proveedores") or die(mysql_error());
                                            while($row3 = mysql_fetch_array($consulta_proveedores)){
                                                $id_proveedor = $row3["id_proveedor"];
                                                $razon_social = $row3["razon_social"];
                                        ?>
                                        <option value="<?php echo $id_proveedor; ?>">
                                        <?php echo $razon_social; ?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr><tr>
								<td id="alright">
                            		<label class="textos">Codigo: </label>
                            	</td><td id="alleft">
                            		<input name="codigo" type="text" maxlength="30" style="width:300px" />
                            	</td>
                            </tr><tr>
                                <td id="alright">
                            		<label class="textos">Concepto: </label>
                            	</td><td id="alleft">
                            		<input name="concepto" type="text" maxlength="69" style="width:300px" />
                            	</td>
                       		</tr><tr>
                            	<td id="alright">                        
                            		<label class="textos">Precio de venta: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_venta" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>    
                            	<td id="alright">                                	
                            		<label class="textos">Precio de compra: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_compra" type="text" size="10" maxlength="10"/>                            
                            	</td>
							</tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                            	<td id="alright">
                            		<label class="textos">Descripcion: </label>
                            	</td><td id="alleft">
                            		<textarea name="descripcion"  rows="3" style="width:300px"></textarea>
                            	</td>
                        	</tr><tr>
                            	<td colspan="2" id="alright">
                                	<input type="hidden" name="reporte"  value="Reporte de Refacciones" class="fondo_boton" 
                                    onclick="window.location.href='reporte_00.php'"/>
                            		<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                            	</td>
                            </tr>
                        </form>
                        </table>                        
                    </div>
                    <div id="tab2" class="tab_content"  style="display:none;">
                    	<table>
                        <form name="form2" action="procesa_nuevo_base_producto.php" method="post" onsubmit="return validaAgregarBaseProductoBaterias()">
                            <input name="categoria1" value="4" type="hidden" />
                            <input name="subcategoria1" value="55" type="hidden" id="subcategoria1" />
							<tr>
                            	<td id="alright">
                            		<label class="textos">Codigo: </label>
                            	</td><td id="alleft">
                            		<input name="codigo" type="text" style="width:300px" />
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Descripcion: </label>
                            	</td><td id="alleft">
                            		<input name="descripcion" type="text"  style="width:300px" />
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Precio de venta: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_venta" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Precio de compra: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_compra" type="text" size="10" maxlength="10"/>
                            	</td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                            	<td id="alright" colspan="2">
                            		<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                            	</td>
                            </tr>
                        </form>
                        </table>
                    </div>
                    <div id="tab3" class="tab_content"  style="display:none;">
                    	<table>
                        <form name="form3" action="procesa_nuevo_base_producto.php" method="post" onsubmit="return validaAgregarBaseProductoModelos()">
                            <input name="categoria1" value="5" type="hidden" />
                            <tr>
                            	<td id="alright">
                            		<label class="textos">Subcategoria</label>
                                </td><td id="alleft">
                                    <select name="subcategoria1" id="subcategoria1" style="width:300px" >
                                        <option value="">Seleccione</option>
                                        <?php
                                            $consulta_subcategorias = mysql_query("SELECT subcategoria, id_subcategoria
                                                                FROM subcategorias_productos
                                                                WHERE id_categoria = 5") 
                                                                or die(mysql_error());
                                            while($row = mysql_fetch_array($consulta_subcategorias)){
                                                $subcategoria = $row["subcategoria"];
                                                $id_subcategoria = $row["id_subcategoria"];
                                        ?>
                                        <option value="<?php echo $id_subcategoria; ?>">
                                            <?php echo $subcategoria; ?>
                                        </option>
                                        <?php 
                                            }
                                        ?>
                                    </select>
                     			</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Proveedores: </label>
                            	</td><td id="alleft">
                                    <select name="proveedor" style="width:300px" >
                                        <option value="0">Seleccione</option>
                                        <?php
                                            $consulta_proveedores = mysql_query("SELECT razon_social, id_proveedor 
                                                                                FROM proveedores") or die(mysql_error());
                                            while($row3 = mysql_fetch_array($consulta_proveedores)){
                                                $id_proveedor = $row3["id_proveedor"];
                                                $razon_social = $row3["razon_social"];
                                        ?>
                                        <option value="<?php echo $id_proveedor; ?>">
                                            <?php echo $razon_social; ?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                      			</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Codigo: </label>
                            	</td><td id="alleft">
                            		<input name="codigo" type="text" style="width:300px" />
                           		</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Modelo: </label>
                            	</td><td id="alleft">
                            		<input name="modelo" type="text" style="width:300px" maxlength="45"/>
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Tipo de Aparato: </label>
                            	</td><td id="alleft">
                                    <select name="tipo_aparato" style="width:300px" >
                                        <option value="0">Seleccione</option>
                                        <?php
                                            $consulta_tipo_aparatos = mysql_query("SELECT id_tipo_modelo, modelo_aparato
                                                                FROM tipos_modelos_aparatos")
                                                                 or die(mysql_error());
                                            while($row5 = mysql_fetch_array($consulta_tipo_aparatos)){
                                                $id_tipo_modelo = $row5["id_tipo_modelo"];
                                                $modelo_aparato = $row5["modelo_aparato"];
                                        ?>
                                        <option value="<?php echo $id_tipo_modelo; ?>">
                                            <?php echo $modelo_aparato; ?>
                                        </option>
                                        <?php 
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr><tr>
                            	<td id="alright">
		                            <label class="textos">Comision: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="comision" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>
                            	<td id="alright">
                            		<label class="textos">Precio de venta: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_venta" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>
                            	<td id="alright">
		                            <label class="textos">Precio de compra: </label>
                            	</td><td id="alleft">
                                    <label class="textos">$</label>
                                    <input name="precio_compra" type="text" size="10" maxlength="10"/>
                            	</td>
                       		</tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                            	<td colspan="2" id="alright">
                            		<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                            	</td>
                            </tr>                        
                        </form>
                        </table>
            		</div>
                    <div id="tab4" class="tab_content" style="display:none;">    
                    	<table>                    
                        <form name="form1" action="procesa_nuevo_base_producto.php" method="post" onsubmit="return validaAgregarBaseProducto()">
                            <input name="categoria1" value="1" type="hidden" />
                            <input name="subcategoria1" value="1" type="hidden" />
                            <tr>
                                <td id="alright">
                                    <label class="textos">Proveedores: </label>
                                </td><td id="alleft">
                                    <select name="proveedor" style="width:300px">
                                        <option value="0">Seleccione</option>
                                        <?php
                                            $consulta_proveedores = mysql_query("SELECT razon_social, id_proveedor 
                                                                                FROM proveedores") or die(mysql_error());
                                            while($row3 = mysql_fetch_array($consulta_proveedores)){
                                                $id_proveedor = $row3["id_proveedor"];
                                                $razon_social = $row3["razon_social"];
                                        ?>
                                        <option value="<?php echo $id_proveedor; ?>">
                                        <?php echo $razon_social; ?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr><tr>
								<td id="alright">
                            		<label class="textos">Codigo: </label>
                            	</td><td id="alleft">
                            		<input name="codigo" type="text" maxlength="30" style="width:300px" />
                            	</td>
                            </tr><tr>
                                <td id="alright">
                            		<label class="textos">Concepto: </label>
                            	</td><td id="alleft">
                            		<input name="concepto" type="text" maxlength="69" style="width:300px" />
                            	</td>
                       		</tr><tr>
                            	<td id="alright">                        
                            		<label class="textos">Precio de venta: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_venta" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>    
                            	<td id="alright">                                	
                            		<label class="textos">Precio de compra: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_compra" type="text" size="10" maxlength="10"/>                            
                            	</td>
							</tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                                <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" value="0" size="5" /> </td>
                            </tr>
                            <tr>
                            	<td id="alright">
                            		<label class="textos">Descripcion: </label>
                            	</td><td id="alleft">
                            		<textarea name="descripcion"  rows="3" style="width:300px"></textarea>
                            	</td>
                        	</tr><tr>
                            	<td colspan="2" id="alright">                                	
                            		<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                            	</td>
                            </tr>
                        </form>
                        </table>                        
                    </div>
        		</div><!--Fin de tab_container-->
                <br />&nbsp;
            </div><!--Fin de contenido proveedor-->  
        </div><!-- Fin de area contenido -->
	</div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div>
</body>
</html>