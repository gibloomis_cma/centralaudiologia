<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Nuevo Estilo </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Estilos
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO 1 -->
							<div class="area_contenido1">
								<br/>
								<br/>
								<?php
									include('config.php');
									$query_estilos = mysql_query("SELECT id_estilo, estilo, precio
																  FROM estilos
																  ORDER BY id_estilo ASC") or die (mysql_error());
								?>
								<center>
								<table>
										<tr>
											<th width="120"> N° de Estilo </th>
											<th> Nombre del Estilo </th>
                                            <th width="80"> Precio </th>
                                            <th width="25"></th>
										</tr>
									<?php
										while( $row_estilos = mysql_fetch_array($query_estilos))
										{
											$id_estilo = $row_estilos['id_estilo'];
											$estilo = $row_estilos['estilo'];
											$precio = $row_estilos['precio'];
									?>
											<tr>
												<td id="centrado"> <?php echo $id_estilo; ?> </td>
												<td> <?php echo $estilo; ?> </td>
                                                <td> <?php echo "$".number_format($precio,2); ?> </td>
                                                <td><a href="modificar_estilo.php?id_estilo=<?php echo $id_estilo; ?>">
                                            		<img src="../img/modify.png" />
                                                 </a></td>
									    	</tr>
									<?php
										}
									?>
								</table> <!-- FIN DE TABLA -->
								</center>
								<br/><br/>
								<div class="titulos"> Agregar Nuevo Estilo </div><!-- FIN DIV TITULOS -->
								<center><br/>
								<form id="form_agregar_nuevo_estilo" name="form_agregar_nuevo_estilo" method="post" action="procesa_agregar_nuevo_estilo.php" enctype="multipart/form-data" onsubmit=" return validaAgregarNuevoEstilo()">
									<table>
										<tr>
											<td id="alright"> <label class="textos"> Nombre del Estilo: </label> </td>
											<td id="alleft"> <input type="text" name="txt_estilo" id="txt_estilo"/> </td>
										</tr><tr>
                                        	<td id="alright">
                                            	<label class="textos"> Precio: </label>
                                           	</td>
                                            <td id="alleft">
                                            	$<input name="precio" type="text" size="10" maxlength="10" />
                                            </td>
                                        </tr><tr>
											<td id="alright"> <label class="textos"> Imagen del Estilo: </label> </td>
											<td id="alleft"> <input type="file" name="imagen_upload" id="imagen_upload" accept="image" style="height:25px;"/> </td>
										</tr>
										<tr>
											<td colspan="3"> 
												<p align="right">
													<br/>
													<input type="reset" name="cancelar" id="cancelar" value="Cancelar" title="Cancelar" class="fondo_boton"/>
													<input type="submit" name="accion" id="accion" value="Guardar" title="Guardar" class="fondo_boton"/> <br/> 
												</p>
											</td>
										</tr>
									</table>
								</form>
								</center>
							</div><!-- FIN DIV AREA CONTENIDO 1 -->
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html>