<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Ventilaciones Estilo </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Ventilaciones
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO1 -->
						<div class="area_contenido1">
							<br/>
							<br/>
							<?php
								include('config.php');
								$id_catalogo = $_GET['id_catalogo'];
								$query_estilo = mysql_query("SELECT estilo,foto
															 FROM estilos,catalogo
															 WHERE estilos.id_estilo = catalogo.id_estilo 
															 AND id_catalogo ='$id_catalogo'") or die (mysql_error());
								$row_estilo = mysql_fetch_array($query_estilo);
								$estilo = $row_estilo['estilo'];
								$imagen = $row_estilo['foto'];
								
								$query_materiales = mysql_query("SELECT id_cat_nivel1,material
																 FROM cat_nivel1,materiales
																 WHERE cat_nivel1.id_material = materiales.id_material
																 AND id_catalogo = '$id_catalogo'") or die (mysql_error());
							?>
							<div class="titulos"> Estilo </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td> <label class="textos"> Nombre del Estilo: </label> </td>
											<td>  &nbsp;<label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $estilo; ?> </label> </td>
											<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
											<td> <img width="100" height="100" src="../moldes/estilos/<?php echo $imagen; ?>"/> </td>
										</tr>
									</table>
								</center>
								<br/>
									<center>
									<table>
                                    	<tr><th colspan="3">Materiales</th></tr>
										<tr>
											<th width="120"> N° Material </th>
											<th> Material </th>
                                            <th width="180"></th>
										</tr>
										<?php
										while( $row_material = mysql_fetch_array($query_materiales) )
										{
											$id_material = $row_material['id_cat_nivel1'];
											$material = $row_material['material'];
										?>
										<tr>
											<td id="centrado"><a href="agregar_ventilaciones_material.php?id_catalogo=<?php echo $id_catalogo?>&id_material=<?php echo $id_material; ?>"> <?php echo $id_material; ?></a> </td>
											<td> <?php echo ucwords(strtolower($material)); ?> </td>
											<td><a  href="agregar_ventilaciones_material.php?id_catalogo=<?php echo $id_catalogo?>&id_material=<?php echo $id_material; ?>">
                                                  	Agregar Ventilaciones
                                           	</a> </td>
                                        </tr>
										<?php
										}
										?>
										<tr>
											<td colspan="2"> <br/> </td>
										</tr>
										<tr>
											<td colspan="3"> <center> <a href="agregar_materiales_estilo.php?id_catalogo=<?php echo $id_catalogo; ?>"> <input type="button" name="terminar" id="terminar" value="Terminar" title="Terminar" class="fondo_boton"/> </a> </center> </td>
										</tr>
									</table><!-- FIN DE TABLA -->
									</center>
									<br/>
								</div><!-- FIN DIV AREA CONTENIDO1 -->					
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>

</html>
