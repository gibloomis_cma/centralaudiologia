<!-- include style -->
 <link rel="stylesheet" type="text/css" href="../css/style_pagination.css" />

<?php
    //open database
    include 'config.php';
    
    //include our awesome pagination
    //class (library)
    include '../librerias/ps_pagination.php';

         //query all data anyway you want
    $sql = "SELECT id_base_producto, id_subcategoria, descripcion, activo 
				FROM base_productos 
				WHERE id_categoria=1 
				ORDER BY activo DESC";

    //execute query and get and
    $rs = mysql_query($sql) or die(mysql_error());

    //now, where gonna use our pagination class
    //this is a significant part of our pagination
    //i will explain the PS_Pagination parameters
    //$conn is a variable from our config_open_db.php
    //$sql is our sql statement above
    //3 is the number of records retrieved per page
    //4 is the number of page numbers rendered below
    //null - i used null since in dont have any other
    //parameters to pass (i.e. param1=valu1&param2=value2)
    //you can use this if you're gonna use this class for search
    //results since you will have to pass search keywords
    $pager = new PS_Pagination( $link, $sql, 20, 3, null );

    //our pagination class will render new
    //recordset (search results now are limited
    //for pagination)
    $rs = $pager->paginate(); 

    //get retrieved rows to check if
    //there are retrieved data
    $num = mysql_num_rows( $rs );

    if( $num >= 1 )
    {     
    ?>
        <table>
            <tr>
                <th width="320px">Refacciones</th>
                <th width="100px">Cantidad</th>
                <th width="80px">Activo</th>
            </tr>
			<?php    
                //looping through the records retrieved class='data-tr' align='center'
				while($row = mysql_fetch_array($rs)){
								$id_base_producto = $row["id_base_producto"];
								$id_subcategoria = $row["id_subcategoria"];
								$descripcion = $row["descripcion"];
								$activo = $row["activo"];
							
								$consulta_inventario_baterias = mysql_query("SELECT SUM(num_serie_cantidad) FROM inventarios 
																					WHERE id_articulo='".$id_base_producto."' 
																					AND id_subcategoria='".$id_subcategoria."'")
																					 or die(mysql_error());
								while($row2 = mysql_fetch_array($consulta_inventario_baterias)){	
									$cantidades_totales = $row2["SUM(num_serie_cantidad)"];
									if($cantidades_totales==""){
										$cantidades_totales=0;
									}
						?>	
                		<tr>
                    		<td style="text-align:left; padding-top:4px;"><?php echo $descripcion; ?></td>
                    		<td style="text-align:center"><?php echo $cantidades_totales; ?></td>
                    		<td style="text-align:center"><?php echo $activo; ?></td>
               			</tr>
                        <?php
								}
				}
                            
    ?>
        </table>
    <?php
    }
    else
    {
        // if no records found
        echo "<h3> No hay refacciones registradas</h3>";
    }

    //page-nav class to control
    //the appearance of our page 
    //number navigation
    echo "<div class='page-nav'>";
         //display our page number navigation
         echo $pager->renderFullNav();
    echo "</div>";

?>