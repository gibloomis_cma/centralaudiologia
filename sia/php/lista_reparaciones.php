<?php
	error_reporting(0);

	// SE INICIA SESION PARA EL USUARIO QUE ENTRO AL SISTEMA
	session_start();

	// SE INICIALIZA LA VARIABLE DE SESION FOLIO EN BLANCO
	$_SESSION["folio"] = "";

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");

	// SE IMPORTA LA LIBRERIA DE PAGINACION
    //require_once '../librerias/PHPPaging.lib.php';
        
    // SE INICIALIZAN LOS VALORES CORRESPONDIENTES A LA PAGINACION
    // $paging = new PHPPaging();
    // $paging->verPost(true);
    // $paging->porPagina(10);
    // $paging->linkSeparador('&nbsp;&nbsp;');
    // $paging->linkClase('paginas');
    // $paging->mostrarPrimera('Primera');
    // $paging->mostrarUltima('Ultima');

     // SE REALIZA EL QUERY QUE OBTIENE LOS NOMBRES DE LOS CLIENTES REGISTRADOS
    $query_clientes_registrados = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre
				 				   FROM ficha_identificacion";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_clientes_registrados = mysql_query($query_clientes_registrados) or die(mysql_error());

	// SE REALIZA QUERY QUE OBTIENE LAS REPARACIONES QUE HAN SIDO ENTREGADAS
	$query_reparaciones_entregadas = "SELECT folio_num_reparacion,CONCAT(nombre,' ',paterno,' ',materno) AS nombre
									  FROM reparaciones
									  WHERE id_estatus_reparaciones = 3
									  ORDER BY folio_num_reparacion DESC";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_reparaciones_entregadas = mysql_query($query_reparaciones_entregadas) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Reparaciones </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript">
		$(function(){
		    <?php
		        while($row = mysql_fetch_array($resultado_clientes_registrados))
		        {
		            $nombre = $row['nombre'];
		            $elementos[] = '"'.$nombre.'"';
		        }
		        $arreglo = implode(", ", $elementos);
		    ?>	
		    var availableTags = new Array(<?php echo $arreglo; ?>);
		    $("#nombre_paciente").autocomplete({
		        source: availableTags
		    });
		});

 		$(function() {
     		//call the function onload, default to page 1
     		getdata( 1 );
		});

		function getdata( pageno ){                     
     		var targetURL = 'lista_reparaciones_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons

     		$('#tabla_lista_reparaciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
     		$('#tabla_lista_reparaciones').load( targetURL,{
     			filtro: "<?php echo $_POST['filtro']; ?>"
     		} ).hide().fadeIn('slow');
		}  
		
		function getdata2( ){   
			var valor = $('#paginas option:selected').attr('value');
			var targetURL = 'lista_reparaciones_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
			$('#tabla_lista_reparaciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
			$('#tabla_lista_reparaciones').load( targetURL,{
				filtro: "<?php echo $_POST['filtro']; ?>"
			} ).hide().fadeIn('slow');                  
			
		}   
	</script>
	<style type="text/css">
		div.contenedor 
		{
			position: relative;
			width: 140px;
			left: 107px;
		}
		
		#input 
		{
			font-family: Arial;
			color: #000;
			font-size: 10pt;
			width: 140px;
		}

		div.fill 
		{
			font-family: Arial;
			font-size: 10pt;
			display: none;
			width: 197px;
			color: #CCC;
			background-color: #CCC;
			border: 1px solid #999;
			overflow: auto;
			height: auto;
			top: -1px;
		}

		tr.fill 
		{
			font-family: Arial;
			font-size: 8pt;
			color: #FFF;
			background-color: #a2a2a3;
			border: 1px solid #a2a2a3;
		}

		#tr 
		{
			font-family: Arial;
			font-size: 8pt;
			background-color: #d9dcdf;
			color: #000;
			border: 1px solid #d9dcdf;
		}
		.tamano_letra
		{
			font-size:10px;	
		}

		.titulo
		{
			font-size: 11px;
		}
	</style>
</head>
<body>
<div id="wrapp">
			<div id="contenido_columna2">
				<div class="contenido_pagina">
					<div class="fondo_titulo1">
            			<div class="categoria">
            				Reparaciones
            			</div>
        			</div><!--Fin de fondo titulo-->
    				<?php 
						date_default_timezone_set('America/Monterrey');
						$script_tz = date_default_timezone_get();
						$fecha = date("d/m/Y");
						$hora = date("h:i:s A");
            			if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
            			{
                			$filtro = $_REQUEST['filtro'];

                			// SE REALIZA QUERY PARA OBTENER EL RESULTADO DE LA BUSQUEDA REALIZADA
                			$res_busqueda = mysql_query("SELECT count(DISTINCT(folio_num_reparacion)) AS contador
                                                    	 FROM reparaciones, base_productos_2
                                                    	 WHERE (nombre LIKE '%".$filtro."%' 
														 OR paterno LIKE '%".$filtro."%'
														 OR materno LIKE '%".$filtro."%'
														 OR num_serie LIKE '%".$filtro."%'
														 OR folio_num_reparacion LIKE '%".$filtro."%'
														 OR base_productos_2.descripcion LIKE '%".$filtro."%')
														 AND base_productos_2.id_base_producto2 = reparaciones.id_modelo
														 AND id_estatus_reparaciones <> 3") or die(mysql_error());

                			// SE ALMACENA EN UNA VARIABLE EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
							$row_busqueda = mysql_fetch_array($res_busqueda);
							$busqueda += $row_busqueda["contador"];

							// SE VALIDA SI LA BUSQUEDA OBTUVO RESULTADOS
							if( $busqueda == 0 )
							{
								$busqueda = 0;	
							}
                			$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
            			}
            			else
            			{
               				$res2 = "";
            			}
					?> 
        			<div class="buscar2">
						<form name="busqueda" method="post" action="lista_reparaciones.php">
                        	<label class="textos"> <?php echo $res2; ?> </label>
                        	<input type="text" name="filtro" size="15" maxlength="15" />
                        	<input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
                    	</form>
       				</div><!-- FIN DIV BUSCAR 2 -->
                    <div class="area_contenido2">
    					<center>
    						<table>
    							<tr>
    								<th colspan="8"> REPARACIONES </th>
    							</tr>
    						</table>
							<div id="tabla_lista_reparaciones">
								<img src="../img/ajax-loader.gif"/>
							</div>
                      	</center>
                        <br />
                        <div class="titulos"> Agregar Nueva Reparaci&oacute;n </div>
                        <div class="contenido_proveedor">
                        	<br/>
                        <form name="aceptar" action="lista_reparaciones.php" method="post">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                        	<table>
                        		<tr>
                        			<td> <label class="textos"> Buscar Cliente: </label> <input type="text" name="paciente" id="nombre_paciente" size="35" maxlength="35" /> </td>
                        		</tr>
                        		<tr>
                        			<td> <input type="checkbox" name="boletin" id="no_registrado" value="ON" /> <label class="textos"> No, se encuentra registrado </label> </td>
                        		</tr>
                        		<tr>
                        			<td> 
                        				<input type="checkbox" name="check_reparacion" id="check_reparacion" value="si"/> <label class="textos"> Garant&iacute;a de Reparaci&oacute;n </label> 
										&nbsp;&nbsp;
                        				<select name="datos_aparato" id="datos_aparato" disabled="disabled">
                        					<option value="0"> - - - Seleccionar - - - </option>
                        			<?php
                        				// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                        				while ( $row_reparaciones_entregadas = mysql_fetch_array($resultado_reparaciones_entregadas) )
                        				{
                        					$folio_reparacion_entregada = $row_reparaciones_entregadas['folio_num_reparacion'];
                        					$nombre_paciente_reparacion_entregada = $row_reparaciones_entregadas['nombre'];
                        				?>
                        					<option value="<?php echo $folio_reparacion_entregada; ?>"> <?php echo $folio_reparacion_entregada." - ".ucwords(strtolower($nombre_paciente_reparacion_entregada)); ?> </option>
                        				<?php
                        				}
                    				?>
                        				</select>
                        			</td>
                        		</tr>
                        		<tr>
                        			<td> <input type="checkbox" name="check_adaptacion" id="check_adaptacion" value="si"/> <label class="textos"> Garant&iacute;a de Adaptaci&oacute;n </label> </td>
                        		</tr>
                        		<tr>
                        			<td> <input type="checkbox" name="check_venta" id="check_venta" value="si"/> <label class="textos"> Venta </label> </td>
                        		</tr>
                        		<tr>
                        			<td> <input type="submit" name="accion" value="Aceptar" class="fondo_boton" /> </td>
                        		</tr>
                        	</table>
                        </form>
   			<?php 
				$paciente= $_REQUEST["paciente"];
				$accion = $_REQUEST["accion"];
				$no_registrado = $_REQUEST['boletin'];
				$check_adaptacion = $_REQUEST['check_adaptacion'];
				$check_reparacion = $_REQUEST['check_reparacion'];
				$datos_aparato = $_REQUEST['datos_aparato'];
				$check_venta = $_REQUEST['check_venta'];
				
				if($accion == "Aceptar")
				{
					if( $paciente <> "" )
					{
						$consulta_datos_paciente = mysql_query("SELECT id_cliente, nombre, paterno, materno, calle, num_exterior, num_interior, codigo_postal, colonia, id_ciudad, id_estado, edad, ocupacion
																FROM ficha_identificacion") or die(mysql_error());
						
						while($row5 = mysql_fetch_array($consulta_datos_paciente))
						{
							$nombre = $row5["nombre"];
							$materno = $row5["materno"];
							$paterno = $row5["paterno"];
							$id_cliente = $row5["id_cliente"];
							$calle = $row5["calle"];
							$num_exterior = $row5["num_exterior"];
							$num_interior = $row5["num_interior"];
							$codigo_postal = $row5["codigo_postal"];
							$colonia = $row5["colonia"];
							$id_estado = $row5["id_estado"];
							$id_ciudad = $row5["id_ciudad"];
							$edad = $row5["edad"];
							$ocupacion = $row5["ocupacion"];
							$nombre_completo = $nombre." ".$paterno." ".$materno;
							
							/* Si el paciente se encuentra registrado se hace este procedimiento */
							if( $paciente == $nombre_completo && $no_registrado == "" && $check_adaptacion == "" && $check_reparacion == "" && $datos_aparato == "" && $check_venta == "" )
							{
					?>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
   							<?php
								if($_SESSION['folio'] == "" )
								{
									$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
															  FROM folios_num_nota_reparacion') or die(mysql_error());
									$row_folio_max = mysql_fetch_array($folio_max);
									$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
									if( $_SESSION['folio'] == 0 )
									{
										$_SESSION['folio'] = 1;
									}
								}
								
							?>
                       	<form name="guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion2">
                        	<input type="hidden" name="variable"  value="1" />
                            <input type="hidden" name="id_empleado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                        	<input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                        	<table>
                        		<tr>
                        			<td width="70"> <label class="textos"> N° de Nota: </label> </td>
                        			<td> <input type="text" name="nota" id="nota" readonly="readonly" size="5" maxlength="5" value="<?php echo $_SESSION['folio']; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        			<td width="70" style="text-align:right;"> <label class="textos"> Fecha: </label> </td>
                        			<td> <input type="text" name="fecha_actual" id="fecha_actual" readonly="readonly" size="15" maxlength="15" value="<?php echo $fecha; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        		</tr>
                        	</table>
                        	<div id="spanreloj"></div>
                        	<br/>
                        	<div id="ficha_identificacion"> <!-- INICIO DE DIV FICHA DE IDENTIFICACION -->
                        		<input type="hidden" name="id_cliente" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                            	<input type="hidden" name="variable_sin_editar" value="1" />
                        		<table>
                        			<tr>
                        				<td> <label class="textos"> Nombre Completo: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Direcci&oacute;n: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $calle." #".$num_exterior." Int: ".$num_interior." Col. ". $colonia." ".$codigo_postal; ?> </label> </td>
                        			</tr>
                        		<?php
									$consulta_ciudad = mysql_query("SELECT ciudad 
																    FROM ciudades 
																	WHERE id_estado = ".$id_estado." 
																	AND id_ciudad = ".$id_ciudad) or die(mysql_error());
									$row4 = mysql_fetch_array($consulta_ciudad);
									$ciudad = utf8_encode($row4["ciudad"]);
									$consulta_estados = mysql_query("SELECT estado 
																	 FROM estados 
																	 WHERE id_estado = ".$id_estado) or die(mysql_error());
									$row3 = mysql_fetch_array($consulta_estados); 
									$estado = utf8_encode($row3["estado"]);
								?>   
                        			<tr>
                        				<td> <label class="textos"> Ciudad: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ciudad)); ?> </label> </td>
                        				<td> <label class="textos"> Estado: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($estado)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Edad: </label> </td>
                        				<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $edad." años"; ?> </label> </td>
                        				<td> <label class="textos"> Ocupaci&oacute;n: </label>  </td>
                        				<td colspan="3"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ocupacion)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td colspan="6" class="textos"> Forma de Contacto: </td>
                        			</tr>
                        		<?php
                        			$consulta_forma_contacto = mysql_query("SELECT * 
                        													FROM contactos_clientes 
																			WHERE id_cliente = ".$id_cliente) or die(mysql_error());
						
									while( $row2 = mysql_fetch_array($consulta_forma_contacto) )
									{
										$id_contactos_cliente = $row2["id_contactos_cliente"];
										$tipo = $row2["tipo"];
										$descripcion = $row2["descripcion"];
										$titular = $row2["titular"];
								?>
                                	<tr>
                                    	<td> <label class="textos"> Tipo: </label> </td>
                                    	<td width="50"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $tipo; ?> </label> </td>
                                    	<td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    	<td width="80"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $descripcion; ?> </label> </td>
                                    	<td> <label class="textos"> Titular: </label> </td>
                                    	<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $titular; ?> </label> </td>
                                	</tr>
   								<?php 
									}
								?>
									<tr>
										<td colspan="6" style="text-align:right;"> <input type="button" name="editar" value="Editar Datos Paciente" class="fondo_boton" id="editar" /> </td>
									</tr>
                        		</table>
                        	</div><!-- FIN DIV FICHA DE IDENTIFICACION -->
                        	<br/>hola
                         	<input type="hidden" name="folio_reparacion" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                         	<input type="hidden" name="fecha" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />                         	
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 1: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo y N° de Serie: </label> </td>
                            		<td>
                            			<select name="modelos" id="modelos" class="opcion11">
                                			<option value="0" selected="selected"> Seleccione Modelo </option>
    	 							<?php
	 									$consulta_auxiliares_paciente = mysql_query("SELECT * FROM relacion_aparatos
																					 WHERE id_cliente = ".$id_cliente." 
																				     AND id_estatus_relacion = 0") or die(mysql_error());
										while( $row_auxiliares_paciente = mysql_fetch_array($consulta_auxiliares_paciente) )
										{
											$id_modelo = $row_auxiliares_paciente["id_modelo"];
											$num_serie_cantidad = $row_auxiliares_paciente["num_serie_cantidad"];
											$id_registro_relacion = $row_auxiliares_paciente["id_registro"];
											$consulta_descripcion_base_productos = mysql_query("SELECT descripcion FROM base_productos_2 
																							    WHERE id_base_producto2 =".$id_modelo) or die(mysql_error());
											$row_descripcion_base_productos = mysql_fetch_array($consulta_descripcion_base_productos);
											$modelo = $row_descripcion_base_productos["descripcion"];
	 								?>
     									<option value="<?php echo $id_modelo."-".$num_serie_cantidad; ?>"> <?php echo $modelo." / ".$num_serie_cantidad; ?> </option>
     								<?php
										}
	 								?>
                            </select>
                            		</td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 2: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos">Marca / Modelo / Tipo: </label> </td>
                            		<td>	
                            			<select name="modelo" class="opcion22">
                            				<option value="0"> Seleccione Modelo </option>
                     				<?php 
										$consulta_modelos_tipo = mysql_query("SELECT id_modelo, modelo_aparato, modelo, subcategoria,subcategorias_productos.id_subcategoria 
																			  FROM modelos,tipos_modelos_aparatos,subcategorias_productos 
																			  WHERE modelos.id_tipo_modelo = tipos_modelos_aparatos.id_tipo_modelo
																			  AND modelos.id_subcategoria = subcategorias_productos.id_subcategoria
																			  ORDER BY subcategoria ASC, modelo") or die(mysql_error());
										while( $row_modelo_tipo=mysql_fetch_array($consulta_modelos_tipo) )
										{
											$modelo_aparato = $row_modelo_tipo["modelo_aparato"];
											$subcategorias = $row_modelo_tipo["subcategoria"];
											$id_subcategoria_modelo = $row_modelo_tipo["id_subcategoria"];
											$modelo = $row_modelo_tipo["modelo"];
											$id_modelo = $row_modelo_tipo["id_modelo"];
											$consulta_base_producto = mysql_query("SELECT id_base_producto FROM base_productos
																		           WHERE id_articulo=".$id_modelo." AND
																				   id_subcategoria = ".$id_subcategoria_modelo) or die(mysql_error());
											$row_base_producto = mysql_fetch_array($consulta_base_producto);
											$id_base_producto = $row_base_producto["id_base_producto"];
									?>
                        					<option value="<?php echo $id_base_producto; ?>"><?php echo $subcategorias.".-  ".$modelo."  /  ".$modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos1" /> </td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 3: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Marca: </label> </td>
                            		<td>
                            			<select name="marca" class="opcion33">
                            				<option value="0"> Seleccione Marca </option>
                      				<?php
										$consulta_marcas = mysql_query("SELECT * FROM subcategorias_productos 
																		WHERE id_categoria = '5' ORDER BY subcategoria ASC") or die(mysql_error());
										while( $row_marcas = mysql_fetch_array($consulta_marcas) )
										{
											$subcategoria_marca = $row_marcas["subcategoria"];
											$id_subcategoria = $row_marcas["id_subcategoria"];
									?>
                        					<option value="<?php echo $id_subcategoria; ?>"><?php echo $subcategoria_marca; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Estilo: </label> </td>
                            		<td>	
                            			 <select name="tipo_aparato">
                            				<option value="0"> Seleccione Estilo </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos 
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while( $row_tipo_aparato = mysql_fetch_array($consulta_tipo_aparato) )
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo: </label> </td>
                            		<td> <input type="text" name="modelos_escritos" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos2" /> </td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 4: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Marca: </label> </td>
                            		<td> <input type="text" name="marca_escritos" class="opcion44" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Estilo: </label> </td>
                            		<td>
                            			<select name="tipo_aparato2">
                            				<option value="0"> Seleccione Estilo </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while( $row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato) )
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo: </label> </td>
                            		<td> <input type="text" name="modelos_escritos2" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos3" /> </td>
                            	</tr>
                            </table>
                            <br/>
                            <label class="textos">Accesorios: </label><br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos">Posibles problemas:</label>
                       	<div class="posibles_problemas2">     
                            <table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                       	</div>
                        <br /><br />
                            <label class="textos"> Descripci&oacute;n del Problema: </label><br />
                            <textarea name="problema" rows="4" cols="50" style="resize:none;"></textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion"/>
                        </p>
                    </form>
 					<?php
							}
							/* Si el paciente se encuentra registrado y la reparacion es para venta se hace este procedimiento */
							elseif( $paciente == $nombre_completo && $no_registrado == "" && $check_adaptacion == "" && $check_reparacion == "" && $datos_aparato == "" && $check_venta == "si" )
							{
					?>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
   							<?php
								if($_SESSION['folio'] == "" )
								{
									$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
															  FROM folios_num_nota_reparacion') or die(mysql_error());
									$row_folio_max = mysql_fetch_array($folio_max);
									$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
									if( $_SESSION['folio'] == 0 )
									{
										$_SESSION['folio'] = 1;
									}
								}
								
							?>
                       	<form name="guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion2">
                        	<input type="hidden" name="variable"  value="1" />
                            <input type="hidden" name="id_empleado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                        	<input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                        	<table>
                        		<tr>
                        			<td width="70"> <label class="textos"> N° de Nota: </label> </td>
                        			<td> <input type="text" name="nota" id="nota" readonly="readonly" size="5" maxlength="5" value="<?php echo $_SESSION['folio']; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        			<td width="70" style="text-align:right;"> <label class="textos"> Fecha: </label> </td>
                        			<td> <input type="text" name="fecha_actual" id="fecha_actual" readonly="readonly" size="15" maxlength="15" value="<?php echo $fecha; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        		</tr>
                        	</table>
                        	<div id="spanreloj"></div>
                        	<br/>
                        	<div id="ficha_identificacion"> <!-- INICIO DE DIV FICHA DE IDENTIFICACION -->
                        		<input type="hidden" name="id_cliente" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                            	<input type="hidden" name="variable_sin_editar" value="1" />
                        		<table>
                        			<tr>
                        				<td> <label class="textos"> Nombre Completo: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Direcci&oacute;n: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $calle." #".$num_exterior." Int: ".$num_interior." Col. ". $colonia." ".$codigo_postal; ?> </label> </td>
                        			</tr>
                        		<?php
									$consulta_ciudad = mysql_query("SELECT ciudad 
																    FROM ciudades 
																	WHERE id_estado = ".$id_estado." 
																	AND id_ciudad = ".$id_ciudad) or die(mysql_error());
									$row4 = mysql_fetch_array($consulta_ciudad);
									$ciudad = utf8_encode($row4["ciudad"]);
									$consulta_estados = mysql_query("SELECT estado 
																	 FROM estados 
																	 WHERE id_estado = ".$id_estado) or die(mysql_error());
									$row3 = mysql_fetch_array($consulta_estados); 
									$estado = utf8_encode($row3["estado"]);
								?>   
                        			<tr>
                        				<td> <label class="textos"> Ciudad: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ciudad)); ?> </label> </td>
                        				<td> <label class="textos"> Estado: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($estado)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Edad: </label> </td>
                        				<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $edad." años"; ?> </label> </td>
                        				<td> <label class="textos"> Ocupaci&oacute;n: </label>  </td>
                        				<td colspan="3"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ocupacion)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td colspan="6" class="textos"> Forma de Contacto: </td>
                        			</tr>
                        		<?php
                        			$consulta_forma_contacto = mysql_query("SELECT * 
                        													FROM contactos_clientes 
																			WHERE id_cliente = ".$id_cliente) or die(mysql_error());
						
									while( $row2 = mysql_fetch_array($consulta_forma_contacto) )
									{
										$id_contactos_cliente = $row2["id_contactos_cliente"];
										$tipo = $row2["tipo"];
										$descripcion = $row2["descripcion"];
										$titular = $row2["titular"];
								?>
                                	<tr>
                                    	<td> <label class="textos"> Tipo: </label> </td>
                                    	<td width="50"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $tipo; ?> </label> </td>
                                    	<td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    	<td width="80"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $descripcion; ?> </label> </td>
                                    	<td> <label class="textos"> Titular: </label> </td>
                                    	<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $titular; ?> </label> </td>
                                	</tr>
   								<?php 
									}
								?>
									<tr>
										<td colspan="6" style="text-align:right;"> <input type="button" name="editar" value="Editar Datos Paciente" class="fondo_boton" id="editar" /> </td>
									</tr>
                        		</table>
                        	</div><!-- FIN DIV FICHA DE IDENTIFICACION -->
                        	<br/>
                         	<input type="hidden" name="folio_reparacion" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                         	<input type="hidden" name="fecha" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 1: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo y N° de Serie: </label> </td>
                            		<td>
                            			<select name="modelos" id="modelos" class="opcion11">
                                			<option value="0" selected="selected"> Seleccione Modelo </option>
    	 							<?php
	 									$consulta_auxiliares_paciente = mysql_query("SELECT * FROM relacion_aparatos
																					 WHERE id_cliente = ".$id_cliente." 
																				     AND id_estatus_relacion = 0") or die(mysql_error());
										while( $row_auxiliares_paciente = mysql_fetch_array($consulta_auxiliares_paciente) )
										{
											$id_modelo = $row_auxiliares_paciente["id_modelo"];
											$num_serie_cantidad = $row_auxiliares_paciente["num_serie_cantidad"];
											$id_registro_relacion = $row_auxiliares_paciente["id_registro"];
											$consulta_descripcion_base_productos = mysql_query("SELECT descripcion FROM base_productos_2 
																							    WHERE id_base_producto2 =".$id_modelo) or die(mysql_error());
											$row_descripcion_base_productos = mysql_fetch_array($consulta_descripcion_base_productos);
											$modelo = $row_descripcion_base_productos["descripcion"];
	 								?>
     									<option value="<?php echo $id_modelo."-".$num_serie_cantidad; ?>"> <?php echo $modelo." / ".$num_serie_cantidad; ?> </option>
     								<?php
										}
	 								?>
                            </select>
                            		</td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 2: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos">Marca / Modelo / Tipo: </label> </td>
                            		<td>	
                            			<select name="modelo" class="opcion22">
                            				<option value="0"> Seleccione Modelo </option>
                     				<?php 
										$consulta_modelos_tipo = mysql_query("SELECT id_modelo, modelo_aparato, modelo, subcategoria,subcategorias_productos.id_subcategoria 
																			  FROM modelos,tipos_modelos_aparatos,subcategorias_productos 
																			  WHERE modelos.id_tipo_modelo = tipos_modelos_aparatos.id_tipo_modelo
																			  AND modelos.id_subcategoria = subcategorias_productos.id_subcategoria
																			  ORDER BY subcategoria ASC, modelo") or die(mysql_error());
										while( $row_modelo_tipo=mysql_fetch_array($consulta_modelos_tipo) )
										{
											$modelo_aparato = $row_modelo_tipo["modelo_aparato"];
											$subcategorias = $row_modelo_tipo["subcategoria"];
											$id_subcategoria_modelo = $row_modelo_tipo["id_subcategoria"];
											$modelo = $row_modelo_tipo["modelo"];
											$id_modelo = $row_modelo_tipo["id_modelo"];
											$consulta_base_producto = mysql_query("SELECT id_base_producto FROM base_productos
																		           WHERE id_articulo=".$id_modelo." AND
																				   id_subcategoria = ".$id_subcategoria_modelo) or die(mysql_error());
											$row_base_producto = mysql_fetch_array($consulta_base_producto);
											$id_base_producto = $row_base_producto["id_base_producto"];
									?>
                        					<option value="<?php echo $id_base_producto; ?>"><?php echo $subcategorias.".-  ".$modelo."  /  ".$modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos1" /> </td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 3: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Marca: </label> </td>
                            		<td>
                            			<select name="marca" class="opcion33">
                            				<option value="0"> Seleccione Marca </option>
                      				<?php
										$consulta_marcas = mysql_query("SELECT * FROM subcategorias_productos 
																		WHERE id_categoria = '5' ORDER BY subcategoria ASC") or die(mysql_error());
										while( $row_marcas = mysql_fetch_array($consulta_marcas) )
										{
											$subcategoria_marca = $row_marcas["subcategoria"];
											$id_subcategoria = $row_marcas["id_subcategoria"];
									?>
                        					<option value="<?php echo $id_subcategoria; ?>"><?php echo $subcategoria_marca; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Estilo: </label> </td>
                            		<td>	
                            			 <select name="tipo_aparato">
                            				<option value="0"> Seleccione Estilo </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos 
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while( $row_tipo_aparato = mysql_fetch_array($consulta_tipo_aparato) )
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo: </label> </td>
                            		<td> <input type="text" name="modelos_escritos" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos2" /> </td>
                            	</tr>
                            </table>
                            <table>
                            	<tr>
                            		<td colspan="2"> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 4: </label> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Marca: </label> </td>
                            		<td> <input type="text" name="marca_escritos" class="opcion44" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Estilo: </label> </td>
                            		<td>
                            			<select name="tipo_aparato2">
                            				<option value="0"> Seleccione Estilo </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while( $row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato) )
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo: </label> </td>
                            		<td> <input type="text" name="modelos_escritos2" /> </td>
                            	</tr>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos3" /> </td>
                            	</tr>
                            </table>
                            <br/>
                            <label class="textos">Accesorios: </label><br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos">Posibles problemas:</label>
                       	<div class="posibles_problemas2">     
                            <table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                       	</div>
                        <br /><br />
                            <label class="textos"> Descripci&oacute;n del Problema: </label><br />
                            <textarea name="problema" rows="4" cols="50" style="resize:none;"></textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion"/>
                        </p>
                    </form>
 					<?php
							} // FIN CONDICION CUANDO ES PARA VENTA
							/* Si el paciente se encuentra registrado y la reparacion es para venta se hace este procedimiento */
							elseif( $paciente == $nombre_completo && $no_registrado == "" && $check_adaptacion == "si" && $check_reparacion == "" && $datos_aparato == "" && $check_venta == "" )
							{
					?>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
   							<?php
								if($_SESSION['folio'] == "" )
								{
									$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
															  FROM folios_num_nota_reparacion') or die(mysql_error());
									$row_folio_max = mysql_fetch_array($folio_max);
									$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
									if( $_SESSION['folio'] == 0 )
									{
										$_SESSION['folio'] = 1;
									}
								}
								
							?>
                       	<form name="guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion2">
                        	<input type="hidden" name="variable"  value="1" />
                            <input type="hidden" name="id_empleado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                        	<input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                        	<table>
                        		<tr>
                        			<td width="70"> <label class="textos"> N° de Nota: </label> </td>
                        			<td> <input type="text" name="nota" id="nota" readonly="readonly" size="5" maxlength="5" value="<?php echo $_SESSION['folio']; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        			<td width="70" style="text-align:right;"> <label class="textos"> Fecha: </label> </td>
                        			<td> <input type="text" name="fecha_actual" id="fecha_actual" readonly="readonly" size="15" maxlength="15" value="<?php echo $fecha; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        		</tr>
                        	</table>
                        	<div id="spanreloj"></div>
                        	<br/>
                        	<div id="ficha_identificacion"> <!-- INICIO DE DIV FICHA DE IDENTIFICACION -->
                        		<input type="hidden" name="id_cliente" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                            	<input type="hidden" name="variable_sin_editar" value="1" />
                        		<table>
                        			<tr>
                        				<td> <label class="textos"> Nombre Completo: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Direcci&oacute;n: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $calle." #".$num_exterior." Int: ".$num_interior." Col. ". $colonia." ".$codigo_postal; ?> </label> </td>
                        			</tr>
                        		<?php
									$consulta_ciudad = mysql_query("SELECT ciudad 
																    FROM ciudades 
																	WHERE id_estado = ".$id_estado." 
																	AND id_ciudad = ".$id_ciudad) or die(mysql_error());
									$row4 = mysql_fetch_array($consulta_ciudad);
									$ciudad = utf8_encode($row4["ciudad"]);
									$consulta_estados = mysql_query("SELECT estado 
																	 FROM estados 
																	 WHERE id_estado = ".$id_estado) or die(mysql_error());
									$row3 = mysql_fetch_array($consulta_estados); 
									$estado = utf8_encode($row3["estado"]);
								?>   
                        			<tr>
                        				<td> <label class="textos"> Ciudad: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ciudad)); ?> </label> </td>
                        				<td> <label class="textos"> Estado: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($estado)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Edad: </label> </td>
                        				<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $edad." años"; ?> </label> </td>
                        				<td> <label class="textos"> Ocupaci&oacute;n: </label>  </td>
                        				<td colspan="3"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ocupacion)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td colspan="6" class="textos"> Forma de Contacto: </td>
                        			</tr>
                        		<?php
                        			$consulta_forma_contacto = mysql_query("SELECT * 
                        													FROM contactos_clientes 
																			WHERE id_cliente = ".$id_cliente) or die(mysql_error());
						
									while( $row2 = mysql_fetch_array($consulta_forma_contacto) )
									{
										$id_contactos_cliente = $row2["id_contactos_cliente"];
										$tipo = $row2["tipo"];
										$descripcion = $row2["descripcion"];
										$titular = $row2["titular"];
								?>
                                	<tr>
                                    	<td> <label class="textos"> Tipo: </label> </td>
                                    	<td width="50"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $tipo; ?> </label> </td>
                                    	<td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    	<td width="80"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $descripcion; ?> </label> </td>
                                    	<td> <label class="textos"> Titular: </label> </td>
                                    	<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $titular; ?> </label> </td>
                                	</tr>
   								<?php 
									}
								?>
									<tr>
										<td colspan="6" style="text-align:right;"> <input type="button" name="editar" value="Editar Datos Paciente" class="fondo_boton" id="editar" /> </td>
									</tr>
                        		</table>
                        	</div><!-- FIN DIV FICHA DE IDENTIFICACION -->
                        	<br/>
                         	<input type="hidden" name="folio_reparacion" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                         	<input type="hidden" name="fecha" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
                            <table>
                            	<tr>
                            		<td style="width:150px;"> <label class="textos"> Modelo y N° de Serie: </label> </td>
                            		<td>
                            			<select name="modelos" id="modelos" class="opcion11">
                                			<option value="0" selected="selected"> Seleccione Modelo </option>
    	 							<?php
	 									$consulta_auxiliares_paciente = mysql_query("SELECT * FROM relacion_aparatos
																					 WHERE id_cliente = ".$id_cliente." 
																				     AND id_estatus_relacion = 0") or die(mysql_error());
										while( $row_auxiliares_paciente = mysql_fetch_array($consulta_auxiliares_paciente) )
										{
											$id_modelo = $row_auxiliares_paciente["id_modelo"];
											$num_serie_cantidad = $row_auxiliares_paciente["num_serie_cantidad"];
											$id_registro_relacion = $row_auxiliares_paciente["id_registro"];
											$consulta_descripcion_base_productos = mysql_query("SELECT descripcion FROM base_productos_2 
																							    WHERE id_base_producto2 =".$id_modelo) or die(mysql_error());
											$row_descripcion_base_productos = mysql_fetch_array($consulta_descripcion_base_productos);
											$modelo = $row_descripcion_base_productos["descripcion"];
	 								?>
     									<option value="<?php echo $id_modelo."-".$num_serie_cantidad; ?>"> <?php echo $modelo." / ".$num_serie_cantidad; ?> </option>
     								<?php
										}
	 								?>
                            </select>
                            		</td>
                            	</tr>
                            </table>
                            <hr style="background-color:#e6e6e6; height:3px; border:none;">
                            <br/>
                            <label class="textos">Accesorios: </label><br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos">Posibles problemas:</label>
                       	<div class="posibles_problemas2">     
                            <table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                       	</div>
                        <br /><br />
                            <label class="textos"> Descripci&oacute;n del Problema: </label><br />
                            <textarea name="problema" rows="4" cols="50" style="resize:none;"></textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion"/>
                        </p>
                    </form>
 					<?php
							}
						}
					} // FIN IF PACIENTE == ""
					// SI EL PACIENTE NO SE ENCUENTRA REGISTRADO
					elseif( $paciente == "" && $no_registrado == "ON" && $check_adaptacion == "" && $check_reparacion == "" && $check_venta == "" )
					{ 
					?>
    					<hr style="background-color:#e6e6e6; height:3px; border:none;">
    				<?php
						if( $_SESSION['folio'] == "" )
						{
							// QUERY QUE OBTIENE EL NUMERO DE FOLIO MAXIMO
							$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
													  							FROM folios_num_nota_reparacion') or die(mysql_error());
							
							// VARIABLE CON EL RESULTADO QUE SE OBTUVO DEL QUERY
							$row_folio_max = mysql_fetch_array($folio_max);
							
							// SE PASA EL RESULTADO OBTENIDO A LA VARIABLE DE SESION DEL FOLIO
							$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
						}
						// SE VALIDA SI LA VARIABLE DE SESION ES IGUAL A 0
						if( $_SESSION['folio'] == 0 )
						{
								$_SESSION['folio'] = 1;
						}
					?>
    					<form name="form_guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion">
                        	<input name="id_cliente" type="hidden" value="0" />
                            <input name="id_empleado" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                            <input name="variable" value="2" type="hidden" />
                            <input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                            <label class="textos">Nº Nota: </label>
                            <input  name="folio_reparacion" type="text" value="<?php echo $_SESSION['folio']; ?>" size="4" maxlength="5" readonly="readonly" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" />
                    		<label class="textos">Fecha: </label>	
                           	<input name="fecha" type="text" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
                            <div id="spanreloj"></div>
                   		<br />
                            <label class="textos">Nombre: </label>
                            <input name="nombre" type="text" size="24" maxlength="20" />
                            <label class="textos">Paterno: </label>
                            <input name="paterno" type="text" size="18" maxlength="15" />
                            <label class="textos">Materno: </label>
                            <input name="materno" type="text" size="18" maxlength="15" />
                        <br /><br />
                            <label class="textos">Calle: </label>
                            <input type="text" name="calle"  />
                            <label class="textos">Num. exterior: </label>
                            <input type="textos" name="num_exterior" />
                            <label class="textos">Num. Interior: </label>
                            <input type="text" name="num_interior" />
                        <br /><br />
                            <label class="textos">Codigo postal: </label>
                            <input type="text" name="codigo_postal" />
                            <label class="textos">Colonia: </label>
                            <input type="text" name="colonia" />
                        <br /><br />
                            <label class="textos">Estado: </label>
                            <select id="id_estado" name="id_estado">
                                <option value="0" selected="selected"> -- Estado -- </option>
	<?php
                        $consulta_estados = mysql_query("SELECT id_estado, estado FROM estados");
                        while( $row3 = mysql_fetch_array($consulta_estados)){ 
                            $id_estado = $row3["id_estado"];
                            $estado = $row3["estado"];	
   	?>
                                <option value="<?php echo $id_estado; ?>">
                                    <?php echo utf8_encode($estado); ?> 
                                </option>
    <?php
                        }
    ?>
                        	</select>
                            <label class="textos">Ciudad: </label>
                            <select name="id_municipio" id="id_municipio"/>
                                <option value="0" selected="selected">--Municipio--</option>
                    		</select>
                     	<br /><br />
                        	<label class="textos">Forma de Contacto:</label><br />
                             <select name="tipo_telefono"/>
                                <option value="" selected="selected">Seleccione</option>
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Correo">Correo</option>
                                <option value="Fax">Fax</option>
                            </select>
                            <label class="textos">Descripcion: </label>
                            <input name="descripcion" type="text" />
                            <label class="textos">Titular: </label>
                            <input name="titular" type="text"/>
                      	<br /><br />
                            <label class="textos">Opcion 1</label><br />
                            <label class="textos">Marca / Modelo / Tipo: </label>
                            <select name="modelo" id="opcion1">
                            	<option value="0">Seleccione</option>
                     	<?php 
							$consulta_modelos_tipo=mysql_query("SELECT id_modelo, modelo_aparato, modelo, subcategoria, 
																		subcategorias_productos.id_subcategoria 
																FROM modelos, tipos_modelos_aparatos,subcategorias_productos 
																WHERE modelos.id_tipo_modelo=tipos_modelos_aparatos.id_tipo_modelo
																AND modelos.id_subcategoria=subcategorias_productos.id_subcategoria
																ORDER BY subcategoria ASC, modelo")
																or die(mysql_error());
							while($row_modelo_tipo=mysql_fetch_array($consulta_modelos_tipo)){
								$modelo_aparato=$row_modelo_tipo["modelo_aparato"];
								$subcategorias = $row_modelo_tipo["subcategoria"];
								$id_subcategoria = $row_modelo_tipo["id_subcategoria"];
								$modelo = $row_modelo_tipo["modelo"];
								$id_modelo = $row_modelo_tipo["id_modelo"];
								$consulta_base_producto=mysql_query("SELECT id_base_producto FROM base_productos
																		WHERE id_articulo=".$id_modelo." AND 
																		id_subcategoria=".$id_subcategoria)or die(mysql_error());
								$row_base_producto=mysql_fetch_array($consulta_base_producto);
								$id_base_producto=$row_base_producto["id_base_producto"];
								
						?>
                        		<option value="<?php echo $id_base_producto; ?>"><?php echo $subcategorias.".-  ".$modelo."  /  ".$modelo_aparato; ?></option>
                        <?php
							}
						?>
                            </select>
                            <label class="textos">Num. serie: </label><input name="num_serie_escritos1" type="text" />
                        <br /><br />
                        	<label class="textos">Opcion 2</label><br />
                            <label class="textos">Marca: </label>
                            <select name="marca" id="opcion2">
                            	<option value="0">Seleccione</option>
                      	<?php
							$consulta_marcas=mysql_query("SELECT * FROM subcategorias_productos 
																	WHERE id_categoria='5' 
																	ORDER BY subcategoria ASC")or die(mysql_error());
							while($row_marcas=mysql_fetch_array($consulta_marcas)){
								$subcategoria_marca = $row_marcas["subcategoria"];
								$id_subcategoria = $row_marcas["id_subcategoria"];
						?>
                        		<option value="<?php echo $id_subcategoria; ?>"><?php echo $subcategoria_marca; ?></option>
                        <?php
							}
						?>
                            </select>
                            <label class="textos">Estilo: </label>
                            <select name="tipo_aparato">
                            	<option value="0">Seleccione</option>
                      	<?php 
							$consulta_tipo_aparato=mysql_query("SELECT * FROM tipos_modelos_aparatos 
																		ORDER BY modelo_aparato ASC")or die(mysql_error());
							while($row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato)){
								$modelo_aparato=$row_tipo_aparato["modelo_aparato"];
								$id_tipo_aparato=$row_tipo_aparato["id_tipo_modelo"];	
						?>
                        		<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        <?php
							}
						?>
                            </select>
                            <label class="textos">Modelo: </label><input name="modelos_escritos" type="text" />
                            <label class="textos">Num. serie: </label><input name="num_serie_escritos2" type="text" />
                        <br /><br />
                        	<label class="textos">Opcion 3</label><br />
                            <label class="textos">Marca: </label><input name="marca_escritos" type="text" id="opcion3" />
                            <label class="textos">Estilo: </label>
                            <select name="tipo_aparato2">
                            	<option value="0">Seleccione</option>
                      	<?php 
							$consulta_tipo_aparato=mysql_query("SELECT * FROM tipos_modelos_aparatos 
																		ORDER BY modelo_aparato ASC")or die(mysql_error());
							while($row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato)){
								$modelo_aparato=$row_tipo_aparato["modelo_aparato"];
								$id_tipo_aparato=$row_tipo_aparato["id_tipo_modelo"];	
						?>
                        		<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        <?php
							}
						?>
                            </select>
                            <label class="textos">Modelo: </label><input name="modelos_escritos2" type="text" />
                            <label class="textos">Num. serie: </label><input name="num_serie_escritos3" type="text" />
                       	<br /><br />
                        
                            <label class="textos">Accesorios: </label><br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos">Posibles problemas:</label>
                         <div class="posibles_problemas">   
                         	<table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                        </div>
                        <br /><br />
                            <label class="textos">Descripcion del Problema: </label><br />
                            <textarea name="problema" rows="2" cols="60"></textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion" id="guardar_reparacion"/>
                        </p>
                        </form>
					<?php	
					} // FIN CONDICION PACIENTE == ""
					// SE HACEN LAS VALIDACIONES PARA SABER SI LA REPARACION ES DE VENTA
					elseif( $paciente == "" && $no_registrado == "ON" && $check_adaptacion == "" && $check_reparacion == "" && $check_venta == "si" )
					{ 
					?>
    					<hr style="background-color:#e6e6e6; height:3px; border:none;">
    				<?php
						if( $_SESSION['folio'] == "" )
						{
							// QUERY QUE OBTIENE EL NUMERO DE FOLIO MAXIMO
							$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
													  FROM folios_num_nota_reparacion') or die(mysql_error());
							
							// VARIABLE CON EL RESULTADO QUE SE OBTUVO DEL QUERY
							$row_folio_max = mysql_fetch_array($folio_max);
							
							// SE PASA EL RESULTADO OBTENIDO A LA VARIABLE DE SESION DEL FOLIO
							$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
						}
						// SE VALIDA SI LA VARIABLE DE SESION ES IGUAL A 0
						if( $_SESSION['folio'] == 0 )
						{
								$_SESSION['folio'] = 1;
						}
					?>
    					<form name="form_guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion">
                        	<input type="hidden" name="id_cliente" value="0" />
                            <input type="hidden" name="id_empleado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                            <input type="hidden" name="variable" value="2" />
                            <input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                            <table>
                            	<tr>
                            		<td> <label class="textos"> Nº Nota: </label> </td>
                            		<td> <input type="text" name="folio_reparacion" value="<?php echo $_SESSION['folio']; ?>" size="4" maxlength="5" readonly="readonly" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                            		<td> <label class="textos"> Fecha: </label> </td>
                            		<td> <input type="text" name="fecha"  value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> <div id="spanreloj"></div> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Nombre: </label> <br/> <input type="text" name="nombre" size="18" maxlength="18" /> </td>
                            		<td> <label class="textos"> Apellido Paterno: </label> <br/> <input type="text" name="paterno" size="18" maxlength="18" /> </td>
                            		<td> <label class="textos"> Apellido Materno: </label> <br/> <input type="text" name="materno" size="18" maxlength="18" /> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Calle: </label> <br/> <input type="text" name="calle"/> </td>
                            		<td> <label class="textos"> N° Exterior: </label> <br/> <input type="text" name="num_exterior" /> </td>
                            		<td> <label class="textos"> N° Interior: </label> <br/> <input type="text" name="num_interior" /> </td>
                            		<td> <label class="textos"> CP: </label> <br/> <input type="text" name="codigo_postal" /> </td>
                            	</tr>
                            	<tr> 
                            		<td> <label class="textos"> Colonia: </label> <br/> <input type="text" name="colonia" /> </td>
                            		<td> 
                            			<label class="textos"> Estado: </label> <br/> 
                            			<select id="id_estado" name="id_estado">
                                			<option value="0" selected="selected"> -- Estado -- </option>
									<?php
										// QUERY QUE OBTIENE LOS ESTADOS DE LA BASE DE DATOS
                        				$consulta_estados = mysql_query("SELECT id_estado, estado FROM estados");
                        				
                        				// SE REALIZA UN CICLO PARA MOSTRAR CADA UNO DE LOS ESTADOS
                        				while( $row3 = mysql_fetch_array($consulta_estados))
                        				{ 
                            				$id_estado = $row3["id_estado"];
                            				$estado = $row3["estado"];	
   									?>
                                			<option value="<?php echo $id_estado; ?>"> <?php echo utf8_encode(ucfirst(strtolower($estado))); ?> </option>
    								<?php
                        				}
    								?>
                        				</select>
                            		</td>
                            		<td colspan="2"> 
                            			<label class="textos"> Ciudad: </label> <br/>
                            			<select name="id_municipio" id="id_municipio"/>
                                			<option value="0" selected="selected"> - - Ciudad - - </option>
                    					</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> 
                            			<label class="textos"> Forma de Contacto: </label> <br /> 
                            			<select name="tipo_telefono"/>
                                			<option value="" selected="selected"> Seleccione </option>
                                			<option value="Telefono"> Telefono </option>
                                			<option value="Celular"> Celular </option>
                                			<option value="Correo"> Correo </option>
                                			<option value="Fax"> Fax </option>
                            			</select>
                            		</td>
                            		<td> <label class="textos"> Descripci&oacute;n: </label> <br/> <input type="text" name="descripcion"/> </td>
                            		<td> <label class="textos"> Titular: </label> <br/> <input type="text" name="titular"/> </td>
                            	</tr>
                            </table>
                            <br/>
                            <hr style="background-color:#e6e6e6; height:3px; border:none;">
                            <table>
                            	<tr>
                            		<td> <label class="textos" style="font-size:16px; color:#000;"> Opcion N° 1: </label> <br /> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Marca / Modelo / Tipo: </label> </td>
                            		<td> 
                            			<select name="modelo" id="opcion1">
                            				<option value="0"> - - - Seleccione - - - </option>
                     			<?php 
									$consulta_modelos_tipo = mysql_query("SELECT id_modelo, modelo_aparato, modelo, subcategoria, subcategorias_productos.id_subcategoria 
																		  FROM modelos, tipos_modelos_aparatos,subcategorias_productos 
																		  WHERE modelos.id_tipo_modelo = tipos_modelos_aparatos.id_tipo_modelo
																		  AND modelos.id_subcategoria = subcategorias_productos.id_subcategoria
																		  ORDER BY subcategoria ASC, modelo") or die(mysql_error());
									while($row_modelo_tipo = mysql_fetch_array($consulta_modelos_tipo))
									{
										$modelo_aparato = $row_modelo_tipo["modelo_aparato"];
										$subcategorias = $row_modelo_tipo["subcategoria"];
										$id_subcategoria = $row_modelo_tipo["id_subcategoria"];
										$modelo = $row_modelo_tipo["modelo"];
										$id_modelo = $row_modelo_tipo["id_modelo"];
										$consulta_base_producto = mysql_query("SELECT id_base_producto 
																			   FROM base_productos
																			   WHERE id_articulo = ".$id_modelo." 
																			   AND id_subcategoria = ".$id_subcategoria) or die(mysql_error());
										$row_base_producto = mysql_fetch_array($consulta_base_producto);
										$id_base_producto = $row_base_producto["id_base_producto"];
								
								?>
                        					<option value="<?php echo $id_base_producto; ?>"><?php echo $subcategorias.".-  ".$modelo."  /  ".$modelo_aparato; ?></option>
                        		<?php
									}
								?>
                            			</select>
                            		</td>
                            	</tr>
                                <br/>
                            	<tr>
                            		<td> <label class="textos"> N° serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos1"/> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 2: </label><br /> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Marca: </label> </td>
                            		<td> 
                            			<select name="marca" id="opcion2">
                            				<option value="0"> - - - Seleccione - - - </option>
                      				<?php
										$consulta_marcas = mysql_query("SELECT * FROM subcategorias_productos 
																	WHERE id_categoria='5' 
																	ORDER BY subcategoria ASC") or die(mysql_error());
										while($row_marcas=mysql_fetch_array($consulta_marcas))
										{
											$subcategoria_marca = $row_marcas["subcategoria"];
											$id_subcategoria = $row_marcas["id_subcategoria"];
									?>
		                        			<option value="<?php echo $id_subcategoria; ?>"><?php echo $subcategoria_marca; ?></option>
                        			<?php
										}
									?>
                            			</select> 
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Estilo: </label> </td>
                            		<td>
                            			<select name="tipo_aparato">
                            				<option value="0"> - - - Seleccione - - - </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos 
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while($row_tipo_aparato = mysql_fetch_array($consulta_tipo_aparato))
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Modelo: </label>  </td>
                            		<td> <input type="text" name="modelos_escritos"/> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> N° Serie: </label>  </td>
                            		<td> <input type="text" name="num_serie_escritos2" /> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos" style="font-size:16px; color:#000;"> Opci&oacute;n N° 3: </label> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Marca: </label> </td>
                            		<td> <input type="text" name="marca_escritos" id="opcion3" /> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Estilo: </label> </td>
                            		<td>
                            			<select name="tipo_aparato2">
                            				<option value="0"> - - - Seleccione - - - </option>
                      				<?php 
										$consulta_tipo_aparato = mysql_query("SELECT * FROM tipos_modelos_aparatos 
																			  ORDER BY modelo_aparato ASC") or die(mysql_error());
										while($row_tipo_aparato = mysql_fetch_array($consulta_tipo_aparato))
										{
											$modelo_aparato = $row_tipo_aparato["modelo_aparato"];
											$id_tipo_aparato = $row_tipo_aparato["id_tipo_modelo"];	
									?>
                        					<option value="<?php echo $id_tipo_aparato; ?>"><?php echo $modelo_aparato; ?></option>
                        			<?php
										}
									?>
                            			</select>
                            		</td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Modelo: </label> </td>
                            		<td> <input type="text" name="modelos_escritos2"/> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> N° Serie: </label> </td>
                            		<td> <input type="text" name="num_serie_escritos3"/> </td>
                            	</tr>
                            </table>
                            <br/>
                            <hr style="background-color:#e6e6e6; height:3px; border:none;">              
                            <label class="textos">Accesorios: </label><br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos">Posibles problemas:</label>
                         <div class="posibles_problemas">   
                         	<table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                        </div>
                        <br /><br />
                            <label class="textos">Descripci&oacute;n del Problema: </label><br />
                            <textarea name="problema" id="descripcion_problema" rows="4" cols="50" style="resize:none;">La reparación es para venta</textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion" id="guardar_reparacion"/>
                        </p>
                        </form>
	<?php	
					} // FIN CONDICION PARA VENTA NO REGISTRADO
					/* Si el paciente se encuentra registrado y la reparacion tiene garantia de reparacion se hace este procedimiento */
					if( $no_registrado == "" && $check_adaptacion == "" && $check_reparacion == "si" && $datos_aparato != 0 && $check_venta == "" )
					{
					?>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
   							<?php
   								// SE REALIZA QUERY QUE OBTIENE LOS DATOS DEL PACIENTE DE LA TABLA REPARACIONES
   								$query_datos_paciente = "SELECT id_cliente,nombre,paterno,materno,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_paciente,tipo,descripcion,titular,calle,num_exterior,num_interior,codigo_postal,colonia,id_estado,id_ciudad,id_modelo,num_serie
   														 FROM reparaciones
   														 WHERE folio_num_reparacion = '$datos_aparato'";

   								// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
   								$resultado_datos_paciente = mysql_query($query_datos_paciente) or die(mysql_error());
   								$row_datos_paciente = mysql_fetch_array($resultado_datos_paciente);
   								$id_cliente = $row_datos_paciente['id_cliente'];
   								$nombre_completo = $row_datos_paciente['nombre_completo_paciente'];
   								$nombre = $row_datos_paciente['nombre'];
   								$paterno = $row_datos_paciente['paterno'];
   								$materno = $row_datos_paciente['materno'];
   								$calle = $row_datos_paciente['calle'];
   								$num_exterior = $row_datos_paciente['num_exterior'];
   								$num_interior = $row_datos_paciente['num_interior'];
   								$colonia = $row_datos_paciente['colonia'];
   								$codigo_postal = $row_datos_paciente['codigo_postal'];
   								$id_ciudad = $row_datos_paciente['id_ciudad'];
   								$id_estado = $row_datos_paciente['id_estado'];
   								$id_modelo = $row_datos_paciente['id_modelo'];
   								$num_serie = $row_datos_paciente['num_serie'];

   								// SE REALIZA QUERY QUE OBTIENE LA MARCA Y EL MODELO DEL APARATO
   								$query_modelo = "SELECT descripcion, subcategoria, id_articulo, base_productos_2.id_subcategoria AS id_subcategoria
												 FROM base_productos_2, subcategorias_productos_2
												 WHERE id_base_producto2 = '$id_modelo' 
												 AND subcategorias_productos_2.id_subcategoria = base_productos_2.id_subcategoria";

								$resultado_query_modelo = mysql_query($query_modelo) or die(mysql_error());
								$row_query_modelo = mysql_fetch_array($resultado_query_modelo);
								$descripcion_modelo = $row_query_modelo['descripcion'];
								$subcategoria_modelo = $row_query_modelo['subcategoria'];
								$id_articulo_modelo = $row_query_modelo['id_articulo'];
								$id_subcategoria_modelo = $row_query_modelo['id_subcategoria'];

								// CONSULTA TIPO DE APARATO
								$query_tipo_aparato = "SELECT modelo_aparato 
													   FROM tipos_modelos_aparatos, modelos_2
													   WHERE id_modelo2 = '$id_articulo_modelo' 
													   AND id_subcategoria = '$id_subcategoria_modelo' 
													   AND modelos_2.id_tipo_modelo = tipos_modelos_aparatos.id_tipo_modelo";

								$resultado_query_tipo_aparato = mysql_query($query_tipo_aparato) or die(mysql_error());
								$row_tipo_aparato = mysql_fetch_array($resultado_query_tipo_aparato);
								$tipo_de_aparato = $row_tipo_aparato['modelo_aparato'];

								if($_SESSION['folio'] == "" )
								{
									$folio_max = mysql_query('SELECT MAX(folio_nota_reparacion+1) 
															  FROM folios_num_nota_reparacion') or die(mysql_error());
									$row_folio_max = mysql_fetch_array($folio_max);
									$_SESSION['folio'] = $row_folio_max['MAX(folio_nota_reparacion+1)'];
									if( $_SESSION['folio'] == 0 )
									{
										$_SESSION['folio'] = 1;
									}
								}
								
							?>
                       	<form name="guardar" action="proceso_guardar_nota_reparacion.php" method="post" class="formulario_validacion2">
                        	<input type="hidden" name="variable"  value="1" />
                            <input type="hidden" name="id_empleado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                        	<input type="hidden" name="txt_adaptacion" value="<?php echo $check_adaptacion; ?>" />
                            <input type="hidden" name="txt_reparacion" value="<?php echo $check_reparacion; ?>" />
                            <input type="hidden" name="txt_venta" value="<?php echo $check_venta; ?>" />
                            <input type="hidden" name="folio_anterior" value="<?php echo $datos_aparato; ?>" />
                            <input type="hidden" name="nombre" value="<?php echo $nombre; ?>" />
                            <input type="hidden" name="paterno" value="<?php echo $paterno; ?>" />
                            <input type="hidden" name="materno" value="<?php echo $materno; ?>" />
                            <input type="hidden" name="calle" value="<?php echo $calle; ?>" />
                            <input type="hidden" name="num_exterior" value="<?php echo $num_exterior; ?>" />
                            <input type="hidden" name="num_interior" value="<?php echo $num_interior; ?>" />
                            <input type="hidden" name="codigo_postal" value="<?php echo $codigo_postal; ?>" />
                            <input type="hidden" name="colonia" value="<?php echo $colonia; ?>" />
                            <input type="hidden" name="id_estado" value="<?php echo $id_estado; ?>" />
                            <input type="hidden" name="id_municipio" value="<?php echo $id_municipio; ?>" />
                        	<table>
                        		<tr>
                        			<td width="70"> <label class="textos"> N° de Nota: </label> </td>
                        			<td> <input type="text" name="nota" id="nota" readonly="readonly" size="5" maxlength="5" value="<?php echo $_SESSION['folio']; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        			<td width="70" style="text-align:right;"> <label class="textos"> Fecha: </label> </td>
                        			<td> <input type="text" name="fecha_actual" id="fecha_actual" readonly="readonly" size="15" maxlength="15" value="<?php echo $fecha; ?>" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                        		</tr>
                        	</table>
                        	<div id="spanreloj"></div>
                        	<br/>
                        	<div id="ficha_identificacion"> <!-- INICIO DE DIV FICHA DE IDENTIFICACION -->
                        		<input type="hidden" name="id_cliente" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                            	<input type="hidden" name="variable_sin_editar" value="1" />
                        		<table>
                        			<tr>
                        				<td> <label class="textos"> Nombre Completo: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($nombre_completo)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td> <label class="textos"> Direcci&oacute;n: </label> </td>
                        				<td colspan="5"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $calle." #".$num_exterior." Int: ".$num_interior." Col. ". $colonia." ".$codigo_postal; ?> </label> </td>
                        			</tr>
                        		<?php
									$consulta_ciudad = mysql_query("SELECT ciudad 
																    FROM ciudades 
																	WHERE id_estado = ".$id_estado." 
																	AND id_ciudad = ".$id_ciudad) or die(mysql_error());
									$row4 = mysql_fetch_array($consulta_ciudad);
									$ciudad = utf8_encode($row4["ciudad"]);
									$consulta_estados = mysql_query("SELECT estado 
																	 FROM estados 
																	 WHERE id_estado = ".$id_estado) or die(mysql_error());
									$row3 = mysql_fetch_array($consulta_estados); 
									$estado = utf8_encode($row3["estado"]);
								?>   
                        			<tr>
                        				<td> <label class="textos"> Ciudad: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($ciudad)); ?> </label> </td>
                        				<td> <label class="textos"> Estado: </label> </td>
                        				<td colspan="2"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo ucwords(strtolower($estado)); ?> </label> </td>
                        			</tr>
                        			<tr>
                        				<td colspan="6" class="textos"> Forma de Contacto: </td>
                        			</tr>
                        		<?php
                        			$consulta_forma_contacto = mysql_query("SELECT * 
                        													FROM contactos_clientes 
																			WHERE id_cliente = ".$id_cliente) or die(mysql_error());
						
									while( $row2 = mysql_fetch_array($consulta_forma_contacto) )
									{
										$id_contactos_cliente = $row2["id_contactos_cliente"];
										$tipo = $row2["tipo"];
										$descripcion = $row2["descripcion"];
										$titular = $row2["titular"];
								?>
                                	<tr>
                                    	<td> <label class="textos"> Tipo: </label> </td>
                                    	<td width="50"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $tipo; ?> </label> </td>
                                    	<td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                    	<td width="80"> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $descripcion; ?> </label> </td>
                                    	<td> <label class="textos"> Titular: </label> </td>
                                    	<td> <label style="color:#000; text-align:center; font-weight:bold;"> <?php echo $titular; ?> </label> </td>
                                	</tr>
   								<?php 
									}
								?>
                        		</table>
                        	</div><!-- FIN DIV FICHA DE IDENTIFICACION -->
                        	<br/>
                        	<hr style="background-color:#e6e6e6; height:3px; border:none;">
                         	<input type="hidden" name="folio_reparacion" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                         	<input type="hidden" name="fecha" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
                            <table>
                            	<tr>
                            		<td> <label class="textos"> Estilo: </label> </td>
                            		<td> <label> <?php echo ucfirst($tipo_de_aparato); ?> </label> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Marca: </label> </td>
                            		<td> <label> <?php echo ucfirst($subcategoria_modelo); ?> </label> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> Modelo: </label> </td>
                            		<td> <label> <?php echo ucfirst($descripcion_modelo); ?> </label> </td>
                            	</tr>
                            	<tr>
                            		<td> <label class="textos"> N° de Serie: </label> </td>
                            		<td> <label> <?php echo strtoupper($num_serie); ?> </label> </td>
                            	</tr>
                            	<tr>
                            		<td> <input type="hidden" name="modelo" value="<?php echo $id_modelo; ?>" /> </td>
                            		<td> <input type="hidden" name="num_serie_escritos1" value="<?php echo $num_serie; ?>" /> </td>
                            	</tr>
                            </table>
                            <br/>
                            <hr style="background-color:#e6e6e6; height:3px; border:none;">
                            <label class="textos"> Accesorios: </label> <br />
                            Estuche: <input type="checkbox" name="estuche" value="1" />
                            &nbsp; &nbsp;
                            Molde: <input type="checkbox" name="molde" value="1" />
                            &nbsp; &nbsp;
                            Pila: <input type="checkbox" name="pila" value="1" />
                        <br /><br />
                        <label class="textos"> Posibles problemas: </label>
                       	<div class="posibles_problemas2">     
                            <table>
                            	<tr>
                            		<td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td width="20"></td>
                                    <td width="145"></td>
                                    <td></td>
                           		</tr><tr>
                                	<td id="alright">No se oye nada</td>
                                    <td id="alleft"><input type="checkbox" name="no_se_oye" value="1" /></td>
									<td id="alright">Portapila</td>
                                    <td id="alleft"><input name="portapila" type="checkbox" value="1" /></td>
                                    <td id="alright">Circuito</td>
                                    <td id="alleft"><input name="circuito" type="checkbox" value="1" />
                                </tr><tr>
                                	<td id="alright">Se oye distorcionado</td>
                                    <td id="alleft"><input type="checkbox" name="se_oye_dis" value="1" /></td>
                                    <td id="alright">Micrófono</td>
                                    <td id="alleft"><input name="microfono" type="checkbox" value="1" /></td>
                                    <td id="alright">Tono</td>
                                    <td id="alleft"><input name="tono" type="checkbox" value="1" /></td>
                                </tr><tr>
                                	<td id="alright">Falla de vez en cuando</td>
                                    <td id="alleft"><input type="checkbox" name="falla" value="1" /></td>
                                    <td id="alright">Receptor</td>
                                    <td id="alleft"><input name="receptor" type="checkbox" value="1" /></td>
                                    <td id="alright">Caja</td>
                                    <td id="alleft"><input name="caja" type="checkbox" value="1" /></td>
                               	</tr><tr>
                                    <td id="alright">Gasta mucha pila</td>
                                    <td id="alleft"><input type="checkbox" name="gasta_pila" value="1" /></td>
                              		<td id="alright">Control de Volumen</td>
                                    <td id="alleft"><input name="control_volumen" type="checkbox" value="1" /></td>
                                    <td id="alright">Contactos de pila</td>
                                    <td id="alleft"><input name="contactos_pila" type="checkbox" value="1" /></td>
                                </tr><tr>
                                    <td id="alright">Cayo al piso</td>
                                    <td id="alleft"><input type="checkbox" name="cayo_piso" value="1" /></td>
                                    <td id="alright">Switch</td>
                                    <td id="alleft"><input name="switch" type="checkbox" value="1" /></td>
                                    <td id="alright">Cambio de tubo al molde</td>
                                    <td id="alleft"><input name="cambio_tubo" type="checkbox" value="1" /></td>
                                </tr>
                                <tr>
                                	<td id="alright">Diagnostico</td>
                                    <td id="alleft"><input name="diagnostico" type="checkbox" value="1" /></td>
									<td id="alright">Bobina Telefonica</td>
                                    <td id="alleft"><input name="bobina" type="checkbox" value="1" /></td>
                                    <td id="alright">Cableado</td>
                                    <td id="alleft"><input name="cableado" type="checkbox" value="1" /></td>
                               	</tr>
                                <tr>
                                    <td id="alright">Limpieza</td>
                                    <td id="alleft"><input name="limpieza" type="checkbox" value="1" /></td>
                                </tr>
                            </table>
                       	</div>
                        <br /><br />
                            <label class="textos"> Descripci&oacute;n del Problema: </label><br />
                            <textarea name="problema" rows="4" cols="50" style="resize:none;"></textarea>
                        <p align="right">
                        	<input type="hidden" name="hora" value="<?php echo $hora; ?>" size="10" maxlength="10" readonly="readonly" />
                            <input name="accion" class="fondo_boton" type="submit" value="Guardar Reparacion"/>
                        </p>
                    </form>
 					<?php
						} // FIN CONDICION CUANDO ES GARANTIA DE ADAPTACION
				} // FIN CONDICION ACCION == "ACEPTAR"
	?>
						</div><!--Fin de contenido proveedor-->
        			</div><!-- Fin de area contenido -->
				</div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
<a href="0reparaciones.php">-0-</a>
</body>
</html>