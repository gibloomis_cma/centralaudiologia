<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_reparaciones = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo,fecha_entrada,id_estatus_reparaciones,folio_num_reparacion,num_serie,id_modelo, reparacion, adaptacion, venta, aplica_garantia
                		   FROM reparaciones
                		   WHERE id_estatus_reparaciones <> 3
                		   ORDER BY fecha_entrada DESC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_query_reparaciones = mysql_query($query_reparaciones) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Control de Reparaciones </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css" />
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora_reparaciones' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
	<div id="wrapp">       
		<div id="contenido_columna2">
    		<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:600px;">
                		Lista de Control de Reparaciones
           			</div>
        		</div><!--Fin de fondo titulo-->
        		<div class="area_contenido1">
            		<div class="contenido_proveedor">
            		<br />
               			<div class="titulos"> Lista de Control de Reparaciones </div>
            		<br />
            		<table style="width:700px;">
            			<tr>
            				<th style="font-size:11px;"> N° Nota </th>
            				<th style="font-size:11px;"> Nombre Completo </th>
            				<th style="font-size:11px;"> Marca </th>
            				<th style="font-size:11px;"> Modelo </th>
            				<th style="font-size:11px;"> N° de Serie </th>
            				<th style="font-size:11px;"> Precio de Reparaci&oacute;n </th>
            				<th style="font-size:11px;"> Estatus </th>
            			</tr>
            		<?php
            			// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            			while ( $row_reparacion = mysql_fetch_array($resultado_query_reparaciones) )
            			{
            				$nombre_completo = $row_reparacion['nombre_completo'];
            				$num_serie = $row_reparacion["num_serie"];
            				$id_modelo = $row_reparacion["id_modelo"];
            				$estado_reparacion = $row_reparacion["id_estatus_reparaciones"];
            				$garantia_reparacion = $row_reparacion['reparacion'];
            				$garantia_adaptacion = $row_reparacion['adaptacion'];
            				$garantia_venta = $row_reparacion['venta'];
            				$aplica_garantia = $row_reparacion['aplica_garantia'];
            				$fecha_entrada = $row_reparacion["fecha_entrada"];
            				$fecha_entrada_separada = explode("-", $fecha_entrada);
            				$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
            				$folio_num_reparacion = $row_reparacion["folio_num_reparacion"];

            				// SE REALIZA QUERY QUE OBTIENE EL ESTATUS DE LA REPARACION
            				$consulta_estatus_nota_reparacion = mysql_query("SELECT * 
            																 FROM estatus_reparaciones
                                                             				 WHERE id_estatus_reparacion = '$estado_reparacion'") or die(mysql_error());    
                    
                    		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
            				$row_estatus_nota_reparacion = mysql_fetch_array($consulta_estatus_nota_reparacion);
            				$estatus_reparacion = $row_estatus_nota_reparacion["estado_reparacion"];

            				// SE REALIZA QUERY QUE OBTIENE LOS DATOS DEL APARATO
            				$consulta_descripcion_modelo = mysql_query("SELECT descripcion, subcategoria 
                                                        				FROM base_productos_2,subcategorias_productos_2
                                                        				WHERE id_base_producto2 = '$id_modelo' 
                                                        				AND base_productos_2.id_subcategoria = subcategorias_productos_2.id_subcategoria") or die(mysql_error());
                    		
                    		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
            				$row_descripcion_modelo = mysql_fetch_array($consulta_descripcion_modelo);
            				$subcategoria_consultada = $row_descripcion_modelo["subcategoria"];
            				$descripcion_modelo = $row_descripcion_modelo["descripcion"];
                    
                    		// SE REALIZA QUERY QUE OBTIENE EL PRESUPUESTO DE LA REPARACION
            				$consulta_presupuesto = mysql_query("SELECT presupuesto 
                                                 				 FROM reparaciones_laboratorio 
                                                 				 WHERE folio_num_reparacion = '$folio_num_reparacion'") or die(mysql_error());
                    
            				$row_presupuesto = mysql_fetch_array($consulta_presupuesto);
            				$presupuesto = $row_presupuesto["presupuesto"];                                     
    				?>
            			<tr>
                			<td style="text-align:center; font-size:10px;"> <?php echo $folio_num_reparacion; ?> </td>
                			<td style="font-size:10px;"> <?php echo $nombre_completo; ?> </td>
                			<td style="text-align:center; font-size:10px;"> <?php echo $subcategoria_consultada; ?> </td>
                			<td style="text-align:center; font-size:10px;"> <?php echo $descripcion_modelo; ?> </td>
                			<td style="text-align:center; font-size:10px;"> <?php echo $num_serie; ?> </td>
                			<td style="text-align:center; font-size:10px;">
                    		<?php
                        		if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                        		{
                            		echo "$ 0.00";
                        		}
                        		else
                        		{
                            		echo "$ ".number_format($presupuesto,2);
                        		}
                    		?>
                			</td>
                			<td style="font-size:10px;"> <?php echo $estatus_reparacion; ?> </td>
            			</tr>
            			<tr>
            				<td colspan="7"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
            			</tr>
            		<?php
            			}
            		?>
            		</table>
            		<br/>
            		<center>
	            		<div id="opciones">
                            <form name="form_reparaciones" style="float:right; margin-left:15px;" method="post" action="lista_control_reparaciones_excel.php">
                                <div id="spanreloj"></div>
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <a href="#" title="Imprimir Reporte" style="float:right;" onclick="window.open('imprimir_lista_control_reparaciones.php','Lista de Control de Reparaciones','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1100, height=700');"> <img src="../img/print icon.png"/> </a>
                        </div> <!-- FIN DIV OPCIONES -->
            		</center>
            		<br/>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>