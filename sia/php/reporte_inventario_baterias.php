<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

    // SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DE LAS REFACCIONES
    $query_refacciones = "SELECT id_base_producto,referencia_codigo,descripcion,precio_compra
                          FROM base_productos
                          WHERE id_categoria = 4
                          ORDER BY descripcion ASC";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_refacciones = mysql_query($query_refacciones) or die(mysql_error());

    // SE DECLARA VARIABLE PARA CALCULAR EL COSTO TOTAL
    $total_en_almacen = 0;
    $costo = 0;
    $general = 0;
    $matriz = 0;
    $laboratorio = 0;
    $ocolusen = 0;
    $general_total = 0;
    $contador = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Inventario de Bater&iacute;as </title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css">
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";

            // Pongo el formato 12 horas
            if ( horas >= 12 )
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;

            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;

            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";

            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers)
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;

            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">
	<div id="contenido_columna2">
    	<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria" style="width: 500px;">
                	Inventario de Bater&iacute;as
           		</div>
        	</div><!--Fin de fondo titulo-->
        	<div class="area_contenido1">
            	<div class="contenido_proveedor">
                    <br />
                        <table style="width: 700px;">
                            <tr>
                                <td style="font-size: 18px; color: #000; text-align: center;"> INVENTARIO DE BATERIAS </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 760px; margin-left: -38px;">
                            <tr>
                                <th style="font-size: 10px;"> C&oacute;digo </th>
                                <th style="font-size: 10px;"> Concepto </th>
                                <th style="font-size: 10px;"> Inventario Total </th>
                                <th style="font-size: 10px;"> Almac&eacute;n General </th>
                                <th style="font-size: 10px;"> Almac&eacute;n Recepci&oacute;n Matriz </th>
                                <!--<th style="font-size: 10px;"> Almac&eacute;n Laboratorio</th>-->
                                <th style="font-size: 10px;"> Almac&eacute;n Recepci&oacute;n Ocolusen </th>
                                <th style="font-size: 10px;"> Costo Total </th>
                            </tr>
                    <?php
                        // SE REALIZA CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                        while( $row_refacciones = mysql_fetch_array($resultado_refacciones) )
                        {
                            $id_base_producto = $row_refacciones['id_base_producto'];
                            $codigo = $row_refacciones['referencia_codigo'];
                            $concepto = $row_refacciones['descripcion'];
                            $precio_compra = $row_refacciones['precio_compra'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO TOTAL DE CADA REFACCION
                            $query_inventario_total = "SELECT SUM(num_serie_cantidad) AS inventario_total
                                                       FROM inventarios
                                                       WHERE id_articulo = '$id_base_producto'";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_inventario_total = mysql_query($query_inventario_total) or die(mysql_error());
                            $row_inventario_total = mysql_fetch_array($resultado_inventario_total);
                            $inventario_total = $row_inventario_total['inventario_total'];
                            $costo = $inventario_total * $precio_compra;
                            $total_en_almacen = $total_en_almacen + $costo;

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION PERO DE EL ALMACEN GENERAL
                            $query_almacen_general = "SELECT SUM(num_serie_cantidad) AS inventario_almacen_general
                                                      FROM inventarios
                                                      WHERE id_articulo = '$id_base_producto'
                                                      AND id_almacen = 1";

                            // SE EJECUTA EL QUERY DE ALMACEN GENERAL Y SE OBTIENE EL RESULTADO
                            $resultado_almacen_general = mysql_query($query_almacen_general) or die(mysql_error());
                            $row_almacen_general = mysql_fetch_array($resultado_almacen_general);
                            $inventario_almacen_general = $row_almacen_general['inventario_almacen_general'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION PERO DE EL ALMACEN RECEPCION MATRIZ
                            $query_recepcion_matriz = "SELECT SUM(num_serie_cantidad) AS inventario_recepcion_matriz
                                                       FROM inventarios
                                                       WHERE id_articulo = '$id_base_producto'
                                                       AND id_almacen = 2";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_recepcion_matriz = mysql_query($query_recepcion_matriz) or die(mysql_error());
                            $row_recepcion_matriz = mysql_fetch_array($resultado_recepcion_matriz);
                            $inventario_recepcion_matriz = $row_recepcion_matriz['inventario_recepcion_matriz'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO EN EL ALMACEN LABORATORIO
                            $query_laboratorio = "SELECT SUM(num_serie_cantidad) AS inventario_laboratorio
                                                  FROM inventarios
                                                  WHERE id_articulo = '$id_base_producto'
                                                  AND id_almacen = 3";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_laboratorio = mysql_query($query_laboratorio) or die(mysql_error());
                            $row_laboratorio = mysql_fetch_array($resultado_laboratorio);
                            $inventario_laboratorio = $row_laboratorio['inventario_laboratorio'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION DEL ALMACEN RECEPCION OCOLUSEN
                            $query_inventario_ocolusen = "SELECT SUM(num_serie_cantidad) AS inventario_ocolusen
                                                          FROM inventarios
                                                          WHERE id_articulo = '$id_base_producto'
                                                          AND id_almacen = 4";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_inventario_ocolusen = mysql_query($query_inventario_ocolusen) or die(mysql_error());
                            $row_inventario_ocolusen = mysql_fetch_array($resultado_inventario_ocolusen);
                            $inventario_ocolusen = $row_inventario_ocolusen['inventario_ocolusen'];

                            $general_total = $general_total + $inventario_total;
                            $general = $general + $inventario_almacen_general;
                            $matriz = $matriz + $inventario_recepcion_matriz;
                            $laboratorio = $laboratorio + $inventario_laboratorio;
                            $ocolusen = $ocolusen + $inventario_ocolusen;

                            // SE VALIDA SI EL INVENTARIO TOTAL ES MAYOR A 0
                            if( $inventario_total > 0 )
                            {
                              $contador++;
                            ?>
                                <tr>
                                    <td style="font-size: 10px; text-align: center;"> <?php echo $codigo; ?> </td>
                                    <td style="font-size: 10px;"> <?php echo $concepto; ?> </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_total) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_total;
                                            }
                                         ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_almacen_general) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_almacen_general;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_recepcion_matriz) )
                                            {
                                                echo "0";
                                            }else
                                            {
                                                echo $inventario_recepcion_matriz;
                                            }
                                        ?>
                                    </td>
                                    <!--Comentarios sobre Laboratorio
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php /*
                                            if( is_null($inventario_laboratorio) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_laboratorio;
                                            }*/
                                        ?>
                                    </td>-->
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_ocolusen) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_ocolusen;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; width: 75px; text-align: center;"> <?php echo "$ ".number_format($costo,2); ?> </td>
                                </tr>
                                <tr>
                                    <td colspan="8"> <hr style="background-color:#e6e6e6; height:2px; border:none;"> </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $general_total; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $general; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $matriz; ?> </td>
                            <!--<td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php /* echo $laboratorio; */ ?> </td>-->
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $ocolusen; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total en inventario: <br /> <?php echo "$ ".number_format($total_en_almacen,2); ?> </td>
                        </tr>
                        </table>
                        <br /><br />
                        <?php
                            if( $contador > 0 )
                            {
                            ?>
                                <div id="opciones" style="float:right;">
                                    <form name="form_inventario" style="float:right; margin-left:20px;" method="post" action="inventario_baterias_excel.php">
                                        <div id="spanreloj"></div>
                                        <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                                    </form> &nbsp;&nbsp;
                                    <a href="#" title="Imprimir Reporte" style=" margin-left:15px;" onclick="window.open('imprimir_inventario_baterias.php','Reporte de Inventario de Bater&iacute;as','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1150, height=700');"> <img src="../img/print icon.png"/> </a>
                                </div> <!-- FIN DIV OPCIONES -->
                                <br /><br />
                            <?php
                            }
                        ?>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>