<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Descripcion Aparatos</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">
			<div id="contenido_columna2">
				<div class="contenido_pagina">
					<div class="fondo_titulo1">
            			<div class="categoria">
            				Pacientes
            			</div>
                    </div><!--Fin de fondo titulo-->                        
                    <div class="area_contenido1">
                        <div class="contenido_proveedor">
                        <br />
  			<?php
				include("config.php");
				include("metodo_cambiar_fecha.php");
				$id_registro_relacion = $_GET["id_registro"];
				$consulta_datos_relacion_aparatos = mysql_query("SELECT * FROM relacion_aparatos 
																			WHERE id_registro=".$id_registro_relacion)
																			or die(mysql_error());
				$row_datos_relacion_aparatos=mysql_fetch_array($consulta_datos_relacion_aparatos);
				$id_ultimo_estudio = $row_datos_relacion_aparatos["id_ultimo_estudio"];
				$id_modelo = $row_datos_relacion_aparatos["id_modelo"];
				$num_serie_cantidad = $row_datos_relacion_aparatos["num_serie_cantidad"];
				$id_estatus_relacion = $row_datos_relacion_aparatos["id_estatus_relacion"];
				$fecha_venta = $row_datos_relacion_aparatos["fecha_venta"];
				$realizo_venta = $row_datos_relacion_aparatos["realizo_venta"];
				$realizo_estudio = $row_datos_relacion_aparatos["realizo_estudio"];
				$realizo_adaptacion = $row_datos_relacion_aparatos["realizo_adaptacion"];
				$observaciones = $row_datos_relacion_aparatos["observaciones"];
				$id_cliente = $row_datos_relacion_aparatos["id_cliente"];
				$fecha_entrega = $row_datos_relacion_aparatos["fecha_entrega"];
				
				$fecha_normal = cambiaf_a_normal($fecha_venta);
				$fecha_entrega_normal = cambiaf_a_normal($fecha_entrega);
				
				$consulta_datos_cliente=mysql_query("SELECT nombre, paterno, materno FROM ficha_identificacion
																						WHERE id_cliente=".$id_cliente)
																						or die(mysql_error());
				$row_datos_cliente = mysql_fetch_array($consulta_datos_cliente);
				$nombre = $row_datos_cliente["nombre"];
				$paterno = $row_datos_cliente["paterno"];
				$materno = $row_datos_cliente["materno"];
				$consulta_modelos=mysql_query("SELECT modelo FROM modelos 
																		WHERE id_modelo=".$id_modelo)
																		or die(mysql_error());
				$row_modelos=mysql_fetch_array($consulta_modelos);
				$modelo = $row_modelos["modelo"];
				
				$consultar_realizo_estudio = mysql_query("SELECT nombre, materno, paterno FROM empleados
																							WHERE id_empleado=".$realizo_estudio)
																							or die(mysql_error());
				$row_realizo_estudio = mysql_fetch_array($consultar_realizo_estudio);
				$nombre_realizo_estudio = $row_realizo_estudio["nombre"];
				$paterno_realizo_estudio = $row_realizo_estudio["paterno"];
				$materno_realizo_estudio = $row_realizo_estudio["materno"];
				
				$consultar_realizo_venta = mysql_query("SELECT nombre, materno, paterno FROM empleados
																						WHERE id_empleado=".$realizo_venta)
																						or die(mysql_error());
				$row_realizo_venta = mysql_fetch_array($consultar_realizo_venta);
				$nombre_realizo_venta = $row_realizo_venta["nombre"];
				$paterno_realizo_venta = $row_realizo_venta["paterno"];
				$materno_realizo_venta = $row_realizo_venta["materno"];
			?>
            			<fieldset>
                        	<legend>Datos del aparato del cliente</legend>
                        	<label class="textos">Paciente: </label>
                        	<?php echo $nombre." ".$paterno." ".$materno; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               				<label class="textos">Folio del estudio para la asignación del aparato: </label>
                      		<?php echo $id_ultimo_estudio; ?>
                   		<br /><br />
							<label class="textos">Fecha de Venta: </label>
							<?php echo $fecha_normal; ?>                     
   						<br /><br />
                        	<label class="textos">Modelo y num. serie: </label>
                            <?php echo $modelo." / ".$num_serie_cantidad; ?>
                           
	<br /><br />
                            <label class="textos">Estatus: </label>
      		<?php
				$consulta_estatus_relacion_aparatos = mysql_query("SELECT * FROM estatus_relaciones_aparatos 
																			WHERE id_estatus_relacion=".$id_estatus_relacion)
																			or die(mysql_error());
				$row_estatus_relacion_aparatos=mysql_fetch_array($consulta_estatus_relacion_aparatos);
					$id_estatus_relacion_consultada = $row_estatus_relacion_aparatos["id_estatus_relacion"];
					$estatus_relacion = $row_estatus_relacion_aparatos["estatus_relacion"];
			?>
            					
							<?php echo $estatus_relacion; ?>
                       	<br /><br />
                    		<label class="textos">Quien elaboro el estudio: </label>
                            <?php echo $nombre_realizo_estudio." ".$paterno_realizo_estudio." ".$materno_realizo_estudio; ?>
        				<br /><br />
                        	<label class="textos">Quien hizo la venta: </label>
                        	<?php echo $nombre_realizo_venta." ".$paterno_realizo_venta." ".$materno_realizo_venta; ?>
                      	<br /><br />
                        	<label class="textos">Quien hizo la adaptacion: </label>
        	<?php
				if($realizo_adaptacion == 0){
						echo "No se ha hecho la adaptacion.";
				}else{
					$consultar_realizo_adaptacion = mysql_query("SELECT nombre, materno, paterno FROM empleados
																						WHERE id_empleado=".$realizo_adaptacion)
																						or die(mysql_error());
					$row_realizo_adaptacion = mysql_fetch_array($consultar_realizo_adaptacion);
					$nombre_realizo_adaptacion = $row_realizo_adaptacion["nombre"];
					$paterno_realizo_adaptacion= $row_realizo_adaptacion["paterno"];
					$materno_realizo_adaptacion = $row_realizo_adaptacion["materno"];
					echo $nombre_realizo_adaptacion." ".$paterno_realizo_adaptacion." ".$materno_realizo_adaptacion;
					
			?>
            			&nbsp;&nbsp;&nbsp;&nbsp;
                        	<label class="textos">Fecha de Entrega:</label>
                            <?php echo $fecha_entrega_normal; ?>
            <?php
				}
			?>

                            
                       	<br /><br />
                        	<label class="textos">Observaciones:</label>
       						<?php echo $observaciones; ?>
                            <p align="right">
                            	<a href="descripcion_aparato_cliente.php?id_paciente=<?php echo $id_cliente; ?>">
                            		<input name="accion" type="button" value="Volver" class="fondo_boton" />
                                </a>
                            </p>
                        </fieldset>
                        </div><!--Fin de contenido proveedor-->
                	</div><!--Fin area contenido-->
				</div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>