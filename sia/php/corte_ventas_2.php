<?php
	// SE INICIALIZA ERROR_REPORTING PARA NO MOSTRAR LOS ERRORES DE LAS VARIABLES
	error_reporting(0);

	// SE INICIA SESION
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE INCLUYE EL ARCHIVO PARA CAMBIAR LA FECHA A NORMAL Y A MYSQL
	//include("metodo_cambiar_fecha.php");

	// SE ESTABLECE LA FECHA DE ACUERDO A LA ZONA
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();

	// SE OBTIENE LA FECHA DEL SISTEMA Y SE CONVIERTE EN FORMATO MYSQL
	$fechaSistema = date("d/m/Y");
	$fechaSistema_separada = explode("/", $fechaSistema);
	$fecha_mysql = $fechaSistema_separada[2]."-".$fechaSistema_separada[1]."-".$fechaSistema_separada[0];

	// SE ALMACENA EN UNA VARIABLE EL ID DEL EMPLEADO QUE HA INICIADO SESION
	$id_empleado_sesion = $_SESSION['id_empleado_usuario'];

	// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO DEL EMPLEADO QUE ENTRO EN EL SISTEMA
	$consulta_departamento_empleado = "SELECT id_departamento 
									   FROM empleados
									   WHERE id_empleado = '$id_empleado_sesion'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_departamento_empleado = mysql_query($consulta_departamento_empleado) or die(mysql_error());

	// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
	$row_departamento_empleado = mysql_fetch_array($resultado_departamento_empleado);

	// SE ALMACENA EL RESULTADO FINAL EN UNA VARIABLE
	$id_departamento_empleado = $row_departamento_empleado["id_departamento"];

	// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO OBTENIDO
	$consulta_sucursal = "SELECT id_sucursal 
						  FROM areas_departamentos 
						  WHERE id_departamento = '$id_departamento_empleado'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_consulta_sucursal = mysql_query($consulta_sucursal) or die(mysql_error());

	// SE OBTIENE EL RESULTADO EN FORMA DE ARREGLO Y SE ALMACENA EN UNA VARIABLE
	$row_sucursal = mysql_fetch_array($resultado_consulta_sucursal);

	// SE ALMACENA EL RESULTADO FINAL DEL QUERY EN UNA VARIABLE
	$id_sucursal = $row_sucursal["id_sucursal"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Corte Diario de Ventas </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css" />
	<script language="javascript">
		<!-- Se abre el comentario para ocultar el script de navegadores antiguos
		function muestraReloj()
		{		
			// Compruebo si se puede ejecutar el script en el navegador del usuario
			if (!document.layers && !document.all && !document.getElementById) return;
				// Obtengo la hora actual y la divido en sus partes
				var fechacompleta = new Date();
				var horas = fechacompleta.getHours();
				var minutos = fechacompleta.getMinutes();
				var segundos = fechacompleta.getSeconds();
				var mt = "AM";
				// Pongo el formato 12 horas
				if (horas >= 12) 
				{
					mt = "PM";
					horas = horas - 12;
				}
				if (horas == 0) horas = 12;
				// Pongo minutos y segundos con dos dígitos
				if (minutos <= 9) minutos = "0" + minutos;
				if (segundos <= 9) segundos = "0" + segundos;
				// En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
 				cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
 				// Escribo el reloj de una manera u otra, según el navegador del usuario

				if (document.layers) 
				{
					document.layers.spanreloj.document.write(cadenareloj);
					document.layers.spanreloj.document.close();
				}
				else if (document.all) spanreloj.innerHTML = cadenareloj;
				else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
				// Ejecuto la función con un intervalo de un segundo
				setTimeout("muestraReloj()", 1000);
		}	
		// Fin del script -->
	</script>
</head>
<body onLoad="muestraReloj()">
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Ventas
            </div>   
        </div><!--Fin de fondo titulo-->
        <div class="area_contenido1">
        	<br />
            <div class="contenido_proveedor">
				<?php
					// SE REALIZA QUERY QUE OBTIENE EL FOLIO MAXIMO +1 DE LA TABLA DE CORTE DE VENTAS DIARIO
					$consulta_corte_venta_diario = "SELECT MAX(folio_corte+1) AS folio_corte 
													FROM corte_ventas_diario 
													WHERE id_sucursal = '$id_sucursal'";

					// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EL CUAL SE ALMACENA EN UNA VARIABLE
					$resultado_corte_venta_diario = mysql_query($consulta_corte_venta_diario) or die(mysql_error());

					// SE ALMACENA EL RESULTADO OBTENIDO EN FORMA DE ARREGLO
					$row_corte_venta_diario = mysql_fetch_array($resultado_corte_venta_diario);

					// EL RESULTADO FINAL ES ALMACENADO EN UNA VARIABLE
					$folio_corte_venta = $row_corte_venta_diario["folio_corte"];

					// SI ES EL PRIMER FOLIO, ES IGUAL A 1 YA QUE EL QUERY REGRESA NULL COMO RESULTADO
					if( $folio_corte_venta == NULL)
					{
						$folio_corte_venta = 1;
					}
				?>
            		<center>
                		<label style="font-size:18px; font-weight:bold;" class="textos">
                    		Corte Diario de Ventas 
                		</label>
                	</center>
                	<br />
               		<table>
                    	<tr>
                        	<td style="text-align:center" colspan="4">
                            	<label class="textos" style="font-size:16px;"> JORGE LEAL Y CIA. S.A. DE C.V. </label>
                            	<br />
                            	<label class="textos" style="font-size:14px;"> Sebastian Lerdo de Tejada 186 </label>
                            	<br />
                                <label class="textos" style="font-size:14px;"> Col. Centro &nbsp;&nbsp;&nbsp; Morelia &nbsp;&nbsp;&nbsp; Michoacan </label>
                            	<br />
                                <label class="textos" style="font-size:14px;">  CP. 58000 </label>
                            	<br />
                                <label class="textos" style="font-size:14px;">RFC: JLE010622GK2 </label>
                            	<br />
                                <label class="textos" style="font-size:14px;"> Telefono: (443) 312-5994 </label>
                            	<br /><br />
                        	</td>
                    	</tr>
                    	<tr>
                    		<td colspan="4" style="text-align:center;">
								<?php
									if( $id_sucursal == 1 )
									{
										echo "Sucursal Matriz";	
									}
									else
									{
										echo "Sucursal Ocolusen";	
									}
					 			?>
					 		</td>
					 	</tr>
                    	<tr>
                        	<td id="alright">
                            	<label class="textos"> N° Folio de Corte de Venta: </label>
                            	<?php echo $folio_corte_venta; ?>
                        	</td>
                        	<td></td>
                        	<td id="alright">
                            	<label class="textos"> Fecha: </label>
                        	</td>
                        	<td>
                            	<?php include('fecha.php'); ?>                            
                        	</td>
                    	</tr>
						<tr>
                    		<td colspan="4" style="text-align:center;">
								<?php
									// SE REALIZA QUERY QUE OBTIENE EL ULTIMO FOLIO DEL CORTE DE VENTAS Y EL ULTIMO REGISTRO DE VENTAS
									$query_ultimo_corte = "SELECT MAX(folio_corte) AS ultimo_folio
														   FROM corte_ventas_diario
														   WHERE id_sucursal = '$id_sucursal'";

									// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
									$resultado_ultimo_corte = mysql_query($query_ultimo_corte) or die(mysql_error());
									$row_ultimo_corte = mysql_fetch_array($resultado_ultimo_corte);
									$ultimo_corte = $row_ultimo_corte['ultimo_folio'];

									// SE REALIZA QUERY QUE OBTIENE LA FECHA Y HORA DEL ULTIMO CORTE DE VENTAS REALIZADO
									$query_fecha_hora_corte = "SELECT fecha_corte,hora,id_registro_venta
															   FROM corte_ventas_diario
															   WHERE folio_corte = '$ultimo_corte'
															   AND id_sucursal = '$id_sucursal'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
									$resultado_fecha_hora_corte = mysql_query($query_fecha_hora_corte) or die(mysql_error());
									$row_fecha_hora_corte = mysql_fetch_array($resultado_fecha_hora_corte);
									$fecha_ultimo_corte = $row_fecha_hora_corte['fecha_corte'];
									$hora_ultimo_corte = $row_fecha_hora_corte['hora'];
									$id_registro_venta_ultimo_corte = $row_fecha_hora_corte['id_registro_venta'];

									// SE REALIZA QUERY QUE OBTIENE LOS FOLIOS MAXIMOS DE VENTA
									$query_folio_venta = "SELECT folio_num_venta
														   FROM ventas
														   WHERE id_registro = '$id_registro_venta_ultimo_corte'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
									$resultado_folio_venta = mysql_query($query_folio_venta) or die(mysql_error());
									$row_folio_venta = mysql_fetch_array($resultado_folio_venta);
									$folio_venta = $row_folio_venta['folio_num_venta'];

				
									// SE REALIZA QUERY QUE OBTIENE DATOS DE LA TABLA DE VENTAS PARA SABER SI SE HAN REALIZADO VENTAS EL DIA EN QUE SE DESEA REALIZAR EL CORTE DE VENTA
									$consulta_si_hay_ventas = "SELECT COUNT(folio_num_venta) 
															   FROM ventas
															   WHERE fecha >='$fecha_ultimo_corte'
															  
															   AND folio_num_venta > '$folio_venta'
															   AND id_sucursal = '$id_sucursal'";

									// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EL CUAL SE ALMACENA EN UNA VARIABLE
									$resultado_si_hay_ventas = mysql_query($consulta_si_hay_ventas) or die(mysql_error());

									// SE ALMACENA EL RESULTADO EN UNA VARIABLE EN FORMA DE ARREGLO
									$row3 = mysql_fetch_array($resultado_si_hay_ventas);

									// SE ALMACENA EL RESULTADO FINAL EN UNA VARIABLE
									//$contador_ventas = $row3["COUNT(folio_num_venta)"];
									//if( $contador_ventas == 0 )
									//{
										
									//}
									//else
									//{
								?>
                			</td>
                		</tr>
                	</table>
            		<br />
                	<table style="border-collapse:collapse;">
                    	<tr>
                        	<th width="150px"> Concepto </th>
                        	<th width="150px"> Cantidad de Art&iacute;culos </th>
                        	<th width="100px"> Total </th>
                    	</tr>
						<?php
							// SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LAS VENTAS REALIZADAS
							
							$consulta_ventas_diaria = "SELECT folio_num_venta, descuento, total, vendedor 
													   FROM ventas 
													   WHERE fecha >= '$fecha_ultimo_corte'
													   AND id_registro > '$id_registro_venta_ultimo_corte'
													   AND id_sucursal = '$id_sucursal'";

							// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO
							$resultado_ventas_diaria = mysql_query($consulta_ventas_diaria) or die(mysql_error()); 

							// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
							while( $row = mysql_fetch_array($resultado_ventas_diaria) ) 
							{
								$vendedor = $row["vendedor"];

								// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO DEL VENDEDOR
								$consulta_departamento_vendedor = "SELECT id_departamento 
																   FROM empleados 
																   WHERE id_empleado = '$vendedor'";

								// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
								$resultado_departamento_vendedor = mysql_query($consulta_departamento_vendedor) or die(mysql_error());

								// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
								$row_departamento_vendedor = mysql_fetch_array($resultado_departamento_vendedor);

								// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
								$id_departamento_vendedor = $row_departamento_vendedor["id_departamento"];
								
								// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO
								$consulta_sucursal = "SELECT id_sucursal 
													  FROM areas_departamentos
													  WHERE id_departamento = '$id_departamento_vendedor'";

								// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
								$resultado_sucursal = mysql_query($consulta_sucursal) or die(mysql_error());

								// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
								$row_sucursal = mysql_fetch_array($resultado_sucursal);

								// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
								$id_sucursal_venta = $row_sucursal["id_sucursal"];

								// SE VALIDA SI EL ID DE LA SUCURSAL ES IGUAL A 1
								if( $id_sucursal_venta == 1 )
								{
									$folio_num_venta = $row["folio_num_venta"];
									$descuento = $row["descuento"];
									$total = $row["total"];
									$totalTotal += $total;

									$consulta_ventas_diaria2 = "SELECT id_registro, cantidad, costo_unitario, id_categoria, pago_parcial
																FROM descripcion_venta 
																WHERE folio_num_venta = '$folio_num_venta' AND id_sucursal = '$id_sucursal_venta'";

									$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

									while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
									{
										$categoria = $row2["id_categoria"];
										$id_registro = $row2["id_registro"];
										$costo_unitario = $row2["costo_unitario"];
										$cantidad = $row2["cantidad"];
										$pago_parcial = $row2['pago_parcial'];

										if ( $categoria == 200 && $pago_parcial != 0 ) 
										{
											$sumaTotal = $pago_parcial;
											$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
											$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
										}
										else
										{
											$sumaTotal = ($cantidad * $costo_unitario);
											$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
											$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
										}

										// SE VALIDA A QUE CATEGORIA PERTENECE EL PRODUCTO
										if($categoria == 1)
										{
											$sumaTotal1 += $sumaTotalNueva;
											$cantidad1 += $cantidad;
										}
										if($categoria == 2)
										{
											$sumaTotal2 += $sumaTotalNueva;
											$cantidad2 += $cantidad;	
										}
										if($categoria == 4)
										{
											$sumaTotal3 += $sumaTotalNueva;
											$cantidad3 += $cantidad;
										}
										if($categoria == 100)
										{
											$sumaTotal4 += $sumaTotalNueva;
											$cantidad4 += $cantidad;	
										}
										if($categoria == 200)
										{
											$sumaTotal5 += $sumaTotalNueva;
											$cantidad5 += $cantidad;	
										}
									}
								}
								// SE VALIDA SI EL ID SUCURSAL ES DIFERENTE A 1
								else
								{
									$folio_num_venta = $row["folio_num_venta"];
									$descuento = $row["descuento"];
									$total = $row["total"];
									$totalTotal += $total;
									$consulta_ventas_diaria2 = "SELECT cantidad, costo_unitario, id_categoria,pago_parcial
																FROM descripcion_venta 
																WHERE folio_num_venta = '$folio_num_venta' 
																AND id_sucursal = '$id_sucursal_venta'";

									$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

									while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
									{
										$categoria = $row2["id_categoria"];
										$id_registro = $row2["id_registro"];
										$costo_unitario = $row2["costo_unitario"];
										$cantidad = $row2["cantidad"];
										$pago_parcial = $row2['pago_parcial'];

										if ( $categoria == 200 && $pago_parcial != 0 ) 
										{
											$sumaTotal = $pago_parcial;
											$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
											$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
										}
										else
										{
											$sumaTotal = ($cantidad * $costo_unitario);
											$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
											$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
										}
										if( $categoria == 1 )
										{
											$sumaTotal1 += $sumaTotalNueva;
											$cantidad1 += $cantidad;
										}
										if( $categoria == 2 )
										{
											$sumaTotal2 += $sumaTotalNueva;
											$cantidad2 += $cantidad;	
										}
										if( $categoria == 4 )
										{
											$sumaTotal3 += $sumaTotalNueva;
											$cantidad3 += $cantidad;
										}
										if( $categoria == 100 )
										{
											$sumaTotal4 += $sumaTotalNueva;
											$cantidad4 += $cantidad;	
										}
										if( $categoria == 200 )
										{
											$sumaTotal5 += $sumaTotalNueva;
											$cantidad5 += $cantidad;	
										}
									} 
								}
							}
						?>
                    	<tr>
                        	<td> Refacciones </td>
                        	<td style="text-align:center;"> <?php echo $cantidad1; ?> </td>
                        	<td id="alright"> <?php echo "$".number_format($sumaTotal1,2); ?> </td>
                    	</tr>
                    	<tr>
                        	<td> Baterias </td>
                        	<td style="text-align:center;"> <?php echo $cantidad3; ?> </td>
                        	<td id="alright"> <?php echo "$".number_format($sumaTotal3,2); ?> </td>
                    	</tr>
                    	<tr>
                        	<td> Consultas </td>
                        	<td style="text-align:center;"> <?php echo $cantidad2; ?> </td>
                        	<td id="alright"> <?php echo "$".number_format($sumaTotal2,2); ?> </td>
                    	</tr>
                    	<tr>
                        	<td> Reparaciones </td>
                        	<td style="text-align:center;"> <?php echo $cantidad4; ?> </td>
                        	<td id="alright"> <?php echo "$".number_format($sumaTotal4,2); ?> </td>
                    	</tr>
                    	<tr>
                        	<td> Moldes </td>
                        	<td style="text-align:center;"> <?php echo $cantidad5; ?> </td>
                        	<td id="alright"> <?php echo "$".number_format($sumaTotal5,2); ?> </td>
                    	</tr>
                    	<tr>
                    		<td colspan="3">
                        		<hr />
                        	</td>
                    	</tr>
                	</table>
                	<?php
                		// SE REALIZA QUERY QUE OBTIENE EL ULTIMO ID DE REGISTRO DE LAS VENTAS
                		$query_ultimo_id_registro = "SELECT MAX(id_registro) AS id_registro
                									 FROM ventas";

                		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
                		$resultado_ultimo_id_registro = mysql_query($query_ultimo_id_registro) or die(mysql_error());
                		$row_ultimo_id_registro = mysql_fetch_array($resultado_ultimo_id_registro);
                		$ultimo_id_registro = $row_ultimo_id_registro['id_registro'];
                	?>
                	<center>
                		<form name="forma1" method="post" action="generar_corte_venta_diario.php">
                			<?php $cantidadTotal = $cantidad1 + $cantidad2 + $cantidad3 + $cantidad4 + $cantidad5;?> 
                			<input type="hidden" name="folio_corte_venta" value="<?php echo $folio_corte_venta; ?>" />
                			<input type="hidden" name="sucursal" value="<?php echo $id_sucursal; ?>" />
                			<input type="hidden" name="fecha_corte" value="<?php echo $fechaSistema; ?>" />
                			<input type="hidden" name="empleado_corte" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                			<input type="hidden" name="ultimo_id_registro" value="<?php echo $ultimo_id_registro; ?>" />
                			<label class="textos"> Cantidad Total de Articulos: <?php echo $cantidadTotal; ?>
                				<input type="hidden" name="total_articulos" value="<?php echo $cantidadTotal; ?>" />
                				<br />
                				<br />
                				Total: <span style="color:#F00"> <?php echo "$".number_format($totalTotal,2); ?> </span>
                			</label>
                			<input type="hidden" name="ultimo_corte_realizado" value="<?php echo $ultimo_corte; ?>" />
                			<input type="hidden" name="total_venta" value="<?php echo $totalTotal; ?>" />
                			<br /><br />
                			<div id="spanreloj"></div>
                			<textarea cols="30" rows="3" name="firma_encargada" style="resize:none;"></textarea>
                			<br />
                			<label class="textos"> Firma de Encargado(a) </label>
           	 				<p align="right">
                				<input name="accion" type="submit" value="Guardar y Generar Ticket" class="fondo_boton" />
            				</p>
            			</form>
                	</center>
            </div><!--Fin de contenido proveedor-->
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>