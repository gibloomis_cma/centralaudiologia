<?php
	session_start();

    // SE RECIBE LA VARIABLE GET EN UNA VARIABLE
    $folio_num_reparacion = $_GET['folio_reparacion'];

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include('config.php');

    // SE REALIZA QUERY QUE OBTIENE LA LISTA DE PROVEEDORES REGISTRADOS EN EL SISTEMA
    $query_lista_proveedores = "SELECT id_proveedor,razon_social
                                FROM proveedores
                                ORDER BY razon_social ASC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_lista_proveedores = mysql_query($query_lista_proveedores) or die(mysql_error());

    // QUERY QUE OBTIENE EL FOLIO MAXIMO DE ORDENES DE COMPRA
    $query_folio_max = "SELECT MAX(folio_orden_compra)+1 AS folio
                        FROM ordenes_de_compra";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_folio_max = mysql_query($query_folio_max) or die(mysql_error());
    $row_folio_max = mysql_fetch_array($resultado_folio_max);
    $folio_orden = $row_folio_max['folio'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orden de Compra</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script src="../js/ui/jquery.ui.core.js"></script>
<script src="../js/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../css/themes/base/demos.css">
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $('#fecha_de_entrega').datepicker({
            dateFormat: "dd-mm-yy",
            dayNamesMin: ["Do","Lu","Ma","Mie","Jue","Vie","Sab"],
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
        });
    });
</script>
</head>
<body>        
<div id="contenido_columna2">
    <div class="contenido_pagina">                
        <div class="fondo_titulo1">                    
            <div class="categoria">
                Proveedores
            </div><!-- Fin de categoria -->                        
        </div><!-- Fin de fondo titulo 1 -->                    
        <div class="area_contenido1">
            <br />
            <form name="forma1" action="procesa_orden_de_compra_refaccion.php" method="post">                        
            <div class="titulos"> Orden de Compra </div>
            <br />
            <div class="contenido_proveedor">
                <center>
            	<table>
                    <tr>
                        <td> <label class="textos"> Proveedor: </label> </td>
                        <td>
                            <select name="proveedor" id="proveedor">
                                <option value="0"> - - - Seleccione - - - </option>
                        <?php
                            // SE REALIZA UN CICLO PARA MOSTRAR LA LISTA DE PROVEEDORES OBTENIDA
                            while ( $row_proveedor = mysql_fetch_array($resultado_lista_proveedores) )
                            {
                                $id_proveedor = $row_proveedor['id_proveedor'];
                                $razon_social = $row_proveedor['razon_social'];
                            ?>
                                <option value="<?php echo $id_proveedor; ?>"> <?php echo strtoupper(strtolower($razon_social)); ?> </option>
                            <?php
                            }
                        ?>
                            </select>
                        </td>
                        <td> <label class="textos"> N° de Folio: </label> </td>
                        <td> <input type="text" name="folio_orden_compra" id="folio_orden_compra"value="<?php echo $folio_orden; ?>" size="10" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                    </tr>
                    <tr>
                        <td> <label class="textos"> Fecha Orden: </label> </td>
                        <td> <input type="text" name="fecha_de_orden" id="fecha_de_orden" size="10" readonly="readonly" value="<?php echo date('d-m-Y'); ?>" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                        <td> <label class="textos"> Fecha Entrega: </label> </td>
                        <td> <input type="text" name="fecha_de_entrega" id="fecha_de_entrega" size="10" /> </td>
                    </tr>
                </table> 
                </center>              
                <center>
                <table>
                    <tr>
                        <th> Producto </th>
                        <th colspan="2"> Cantidad </th>                       
                    </tr>
                    <?php
						$lista_productos = mysql_query("SELECT codigo,cantidad,descripcion
                                                        FROM vales_reparacion,base_productos
                                                        WHERE folio_num_reparacion = '$folio_num_reparacion'
                                                        AND codigo = id_base_producto") or die(mysql_error());

                        while ( $row_lista_productos = mysql_fetch_array($lista_productos) )
                        {
                            $codigo = $row_lista_productos['codigo'];
                            $cantidad = $row_lista_productos['cantidad'];
                            $descripcion = $row_lista_productos['descripcion'];

                            $query_inventario = "SELECT SUM(num_serie_cantidad) AS total
                                                 FROM inventarios
                                                 WHERE id_articulo = '$codigo'
                                                 AND id_almacen = 1";
                            $resultado_inventario = mysql_query($query_inventario) or die(mysql_error());
                            $row_inventario = mysql_fetch_array($resultado_inventario);
                            $inventario = $row_inventario['total'];

                            if ( $inventario == 0 )
                            {
                            ?>
                                <tr>
                                    <td> <?php echo ucfirst(strtolower($descripcion)); ?> </td>
                                    <td style="text-align:center;"> <input type="text" name="cantidad[]" id="cantidad" value="1" /> </td>
                                    <td> <input type="hidden" name="codigo[]" id="codigo" value="<?php echo $codigo; ?>" /> </td>
                                </tr>
                            <?php 
                            }
                        }					
					?>
                </table>
                </center>
                <hr />
                <p align="center">
                	<input type="button" name="volver" value="Volver" onclick="window.location.href='detalles_reparaciones2.php?folio=<?php echo $folio_num_reparacion; ?>'" class="fondo_boton" />
                    <input name="accion" type="submit" value="Generar Orden" class="fondo_boton"/>
                </p>
            </div><!--Fin de contenido proveedor-->
        </form>
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>