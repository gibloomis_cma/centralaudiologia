<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8/iso-8859-1" />
	<title>Cuentas por Pagar</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
	<div id="contenido_columna2">
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria">
					Proveedores
				</div><!-- Fin DIV categoria -->    
			</div><!--Fin de fondo titulo-->
			<div class="area_contenido1">
				<br />
				<div class="contenido">
					<!--<div class="nom_proveedor"><label class="textos">Nombre del Proveedor: </label></div>-->
						<div class="etiqueta"><label class="textos">
							<?php
							// SE IMPORTA EL ARCHIVO QUE INCLUYE LA CONEXION A LA BASE DE DATOS
								include("config.php");
							// SE OBTIENE EL ID DEL PROVEEDOR POR MEDIO DEL METODO GET
								$id_proveedor = $_GET['id_proveedor'];
							// SE REALIZA EL QUERY PARA OBTENER EL NOMBRE DEL PROVEEDOR DE ACUERDO AL ID OBTENIDO ANTERIORMENTE
								$query_nombre_proveedor = mysql_query("SELECT razon_social 
																		FROM proveedores 
																		WHERE id_proveedor = $id_proveedor") 
																		or die (mysql_error());
								$row = mysql_fetch_array( $query_nombre_proveedor );
								echo $nombre_proveedor = $row['razon_social'];
							// SE REALIZA EL QUERY DE TODAS LA CUENTAS POR PAGAR DE ACUERDO AL PROVEEDOR SELECCIONADO								
								$cuentas_por_pagar = mysql_query("SELECT cuentas_por_pagar.id_registro as id, folio_orden_compra, fecha, estatus_cuenta
															FROM proveedores,cuentas_por_pagar,estatus_cuentas 
															WHERE cuentas_por_pagar.id_proveedor = proveedores.id_proveedor 
															AND estatus_cuentas.id_estatus_cuentas = cuentas_por_pagar.id_estatus 
															AND cuentas_por_pagar.id_proveedor = '".$id_proveedor."' 
															AND estatus_cuenta <> 'Pagado' 
															GROUP BY folio_orden_compra 
															ORDER BY fecha") or die (mysql_error());
							?>	</label>
						</div><!--Fin de etiqueta-->
				</div><!--Fin de contenido-->
                <br />
                <br />
                <center>
                    <table>
                    	<tr>
                        	<th colspan="5">Cuentas por pagar</th>
                        </tr><tr>
                            <th>Folio</th>
                            <th>Fecha de Orden</th>
                            <th>Estatus</th>
                            <th colspan="2">Saldo</th>
                        </tr>                  
                    <?php
						$cuentas=0;
                        while($row = mysql_fetch_array($cuentas_por_pagar)){
							$cuentas++;
							$id_registro=$row['id'];
                            $folio = $row['folio_orden_compra'];
                            $fecha_de_orden = $row['fecha'];
                            $estatus = $row['estatus_cuenta'];
                    ?>   
                        <tr>
                            <td><a href="detalles_orden_compra.php?id_proveedor=<?php echo $id_proveedor."&folio=".$folio."&x=0"; ?>"><?php echo $folio; ?></a></td>
                            <td><?php echo $fecha_de_orden; ?> </td>
                            <td><?php echo $estatus; ?> </td>
                            <?php
                            $c_saldo = mysql_query('SELECT SUM(cantidad_codigo*costo) as saldo
                                    FROM cuentas_articulos
                                    WHERE id_cuenta_por_pagar = '.$id_registro)
									or die(mysql_error());
                            $row_saldo = mysql_fetch_array($c_saldo);
                            $saldo=$row_saldo['saldo'];							
                            $c_abono = mysql_query('SELECT SUM(cantidad)
                                                FROM pagos_proveedores 
                                                WHERE id_proveedor = '.$id_proveedor.' 
                                                AND folio_orden_compra = '.$folio.' 
                                                GROUP BY folio_orden_compra')
                                                or die(mysql_error());
                            $row_abono = mysql_fetch_array($c_abono);
                            $abono = $row_abono['SUM(cantidad)'];
							$res=$saldo - $abono;
							if($res>0){							
                            ?>
                            <td id="alright"><?php echo "$".number_format($res,2); ?></td>                                                                        
                            <td id="alright">
                                <a href="proveedor_pagos.php?id_proveedor=<?php echo
                                 $id_proveedor."&folio=".$folio; ?>" target="_self"> 
                                    <img src="../img/info.png" title="<?php echo "Folio ".$folio;  ?>" />
                                </a>
                            </td>                                
                            <?php
							}else{
							?>
                            <td id="alright"><?php echo "$".number_format($res,2); ?></td>                           
                            <td id="alright">
                                <a href="proveedor_pagos.php?id_proveedor=<?php echo
                                 $id_proveedor."&folio=".$folio."&x=0"; ?>" target="_self"> 
                                    <img src="../img/info.png" title="<?php echo "Folio ".$folio;  ?>"
                                </a>
                            </td>
                            <?php
							}
							?>                            
                        </tr>
                    <?php
                        }
						if($cuentas==0){
					?>
                    	<tr>
                            <td colspan="5" style="text-align:center"><label class="textos">No hay cuentas por pagar registradas</label></td>                            
                        </tr>
                    <?php
						}
                    ?>                        
                    	<tr>
                        	<td colspan="5">
                            	<hr />
                            </td>
                        </tr><tr>
                        	<td colspan="5" style="text-align:center">
                            	<input type="button" name="volver" value="Volver" class="fondo_boton"
                                onclick="window.location.href='lista_proveedores_cuentas.php'" />
                                <input type="button" name="nuevo" value="Agregar cuenta" class="fondo_boton"
                                onclick="window.location.href='cuenta_por_pagar_nueva.php?id_proveedor=<?php echo $id_proveedor ?>'" />
                            </td>
                        </tr>
                    </table>
                </center>
			</div><!--Fin de area contenido-->
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna2-->
</body>
</html>