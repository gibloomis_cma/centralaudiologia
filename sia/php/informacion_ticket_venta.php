<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL FOLIO DE VENTA POR METODO POST
	$folio_num_venta = $_POST['folio_num_venta'];
	$sucursal_venta = $_POST['sucursal_venta'];

	// SE REALIZA QUERY QUE OBTIENE LA INFORMACION DE LA VENTA DE ACUERDO AL FOLIO RECIBIDO
	$query_venta = "SELECT id_sucursal,folio_num_venta,fecha,descuento,vendedor,total
					FROM ventas
					WHERE folio_num_venta = '$folio_num_venta'
					AND id_sucursal = '$sucursal_venta'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO
	$resultado_query_venta = mysql_query($query_venta) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_venta = mysql_fetch_array($resultado_query_venta);

	// SE DECLARAN VARIABLES CON EL RESULTADO FINAL DE QUERY
	$folio_num_venta = $row_venta['folio_num_venta'];
	$id_sucursal = $row_venta['id_sucursal'];
	$fecha = $row_venta['fecha'];
	$fecha_separada = explode("-", $fecha);
	$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
	$descuento = $row_venta['descuento'];
	$vendedor = $row_venta['vendedor'];
	$total = $row_venta['total'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL EN DONDE SE LLEVO A CABO LA VENTA
	$query_sucursal = "SELECT nombre
					   FROM sucursales
					   WHERE id_sucursal = '$id_sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);
	$nombre_sucursal = $row_sucursal['nombre'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE QUIEN REALIZO LA VENTA
	$query_empleado = "SELECT CONCAT(nombre,' ',paterno) AS nombre_empleado
					   FROM empleados
					   WHERE id_empleado = '$vendedor'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_empleado = mysql_query($query_empleado) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_empleado = mysql_fetch_array($resultado_empleado);
	$nombre_empleado = $row_empleado['nombre_empleado'];

	// SE IMPRIMEN LAS VARIABLES PARA MOSTRARLAS EN EL FORMULARIO
	echo $folio_num_venta;
	echo "°";
	echo $fecha_normal;
	echo "°";
	echo $nombre_sucursal;
	echo "°";
	echo $nombre_empleado;
	echo "°";
	echo $descuento."%";
	echo "°";
	echo $total;
	echo "°";
	echo $id_sucursal;
	echo "°";
	echo $vendedor;
?>