<?php
	// SE EVITA MOSTRAR ERRORES PARA QUE NO SE VEAN AL MOMENTO DE IMPRIMIR EL TICKET DE VENTA
	error_reporting(0);

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE ESTABLECE LA ZONA HORARIA PARA PODER ESTABLECER LA FECHA
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();

	// SE ESTABLECE LA HORA DE ACUERDO A LA DEL SISTEMA
	$hora = date("h:i:s A");	

	// SE RECIBEN LAS VARIABLES CON LA INFORMACION DEL FORMULARIO
	$accion = $_POST["accion"];	
	$fecha = $_POST["fecha"];
	$folio_num_venta = $_POST["folio"];
	$descuento = $_POST["descuento"];
	$vendedor = $_POST["vendedor"];
	$total_venta = $_POST["total"];
	$sucursal = $_POST["sucursal"];
	$hora_venta = $_POST['hora_venta'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA SUCURSAL DE ACUERDO AL ID DE LA SUCURSAL OBTENIDO
	$query_sucursal = "SELECT nombre,calle,num_exterior,num_interior,colonia,codigo_postal,id_ciudad,id_estado
					   FROM sucursales
					   WHERE id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIBLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);

	$nombre_sucursal = $row_sucursal['nombre'];
	$calle_sucursal = $row_sucursal['calle'];
	$num_exterior_sucursal = $row_sucursal['num_exterior'];
	$num_interior_sucursal = $row_sucursal['num_interior'];
	$colonia_sucursal = $row_sucursal['colonia'];
	$codigo_postal_sucursal = $row_sucursal['codigo_postal'];
	$id_ciudad_sucursal = $row_sucursal['id_ciudad'];
	$id_estado_sucursal = $row_sucursal['id_estado'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL ESTADO DE ACUERDO AL OBTENIDO
	$query_estado_sucursal = "SELECT estado
							  FROM estados
							  WHERE id_estado = '$id_estado_sucursal'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_estado_sucursal = mysql_query($query_estado_sucursal) or die(mysql_error());
	$row_estado_sucursal = mysql_fetch_array($resultado_estado_sucursal);
	$nombre_estado_sucursal = $row_estado_sucursal['estado'];

	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DE LA CIUDAD DE ACUERDO AL ID DEL ESTADO Y DE LA CIUDAD OBTENIDOS
	$query_ciudad_sucursal = "SELECT ciudad
							  FROM ciudades
							  WHERE id_ciudad = '$id_ciudad_sucursal'
							  AND id_estado = '$id_estado_sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_ciudad_sucursal = mysql_query($query_ciudad_sucursal) or die(mysql_error());
	$row_ciudad_sucursal = mysql_fetch_array($resultado_ciudad_sucursal);
	$nombre_ciudad_sucursal = $row_ciudad_sucursal['ciudad'];


	/* SE VALIDA CUAL BOTON FUE OPRIMIDO POR EL USUARIO */	
	/* SE VALIDA SI EL BOTON REGRESAR FUE OPRIMIDO */
	//if( $accion == "Regresar" )
	//{
	//	header('Location: nueva_venta2.php');
	//}
	
	/* SE VALIDA SI EL BOTON ACEPTAR FUE OPRIMIDO */
	if( $accion == "Aceptar" )
	{	
		/* SE REALIZA UN QUERY QUE OBTIENE LOS DATOS DE LA VENTA DE ACUERDO AL FOLIO DE VENTA Y A LA SUCURSAL */
		$consulta_datos_venta = "SELECT id_categoria, descripcion, cantidad, id_registro, costo_unitario, pago_parcial, pago_parcial_2
								 FROM descripcion_venta
								 WHERE folio_num_venta = '$folio_num_venta' 
								 AND id_sucursal = '$sucursal'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
		$resultado_datos_venta = mysql_query($consulta_datos_venta) or die(mysql_error());

		while( $row = mysql_fetch_array($resultado_datos_venta) )
		{
			$categoria = $row["id_categoria"];
			$descripcion = $row["descripcion"];
			$cantidad = $row["cantidad"];
			$id_registro = $row["id_registro"];
			$costo_unitario = $row["costo_unitario"];
			$pago_parcial = $row['pago_parcial'];
			$pago_parcial_2 = $row['pago_parcial_2'];
			
			/* SE VALIDA LA CATEGORIA DE LA DESCRIPCION DE LA VENTA */
			if( $categoria == 100 )
			{
				$variable_nueva = explode(" ",$descripcion);
				for( $i=0; $i<=count($variable_nueva); $i++ )
				{
					$variable_nueva[$i];
					$consulta_datos_reparacion = mysql_query("SELECT folio_num_reparacion 
															  FROM reparaciones
														 	  WHERE folio_num_reparacion LIKE '%".$variable_nueva[$i]."'") or die(mysql_error());

					$row4 = mysql_fetch_array($consulta_datos_reparacion);
					$folio_num_reparacion = $row4["folio_num_reparacion"];
					
					mysql_query("UPDATE reparaciones SET id_estatus_reparaciones= '3'
								 WHERE folio_num_reparacion='".$folio_num_reparacion."'") or die(mysql_error());
															
				}
				mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
							 VALUES('".$folio_num_reparacion."','3','".$fecha."','".$hora."','3','".$vendedor."')") or die(mysql_error());
			}

			if( $categoria == 200 )
			{
				$variable_nueva2 = explode(" ",$descripcion);
				$variable_nueva2[4];
				$consulta_datos_moldes = mysql_query("SELECT folio_num_molde,pago_parcial,costo,pago_parcial_2,id_estatus_moldes FROM moldes
													  WHERE folio_num_molde LIKE '%".$variable_nueva2[4]."'") or die(mysql_error());

				$row5 = mysql_fetch_array($consulta_datos_moldes);
				$folio_num_molde = $row5["folio_num_molde"];
				$pago_parcial_1 = $row5['pago_parcial'];
				$pago_parcial_2_consultado = $row5['pago_parcial_2'];
				$costo_consultado = $row5['costo'];
				$id_estatus_moldes = $row5['id_estatus_moldes'];
				$total_por_molde = $pago_parcial_1 + $pago_parcial_2;

				if ( $total_por_molde == $costo_consultado && $id_estatus_moldes == '5' || $id_estatus_moldes == '6'  )
				{
					mysql_query("UPDATE moldes SET id_estatus_moldes = '7'
							 	 WHERE folio_num_molde='".$folio_num_molde."'") or die(mysql_error());

					mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora, id_estatus_moldes, id_empleado)
							 	 VALUES('".$folio_num_molde."','3','".$fecha."','".$hora."','7','".$vendedor."')") or die(mysql_error());
				}
				elseif( $pago_parcial_1 == $costo_consultado && $id_estatus_moldes == '5' || $id_estatus_moldes == '6' )
				{
					mysql_query("UPDATE moldes SET id_estatus_moldes = '7'
							 	 WHERE folio_num_molde='".$folio_num_molde."'") or die(mysql_error());

					mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora, id_estatus_moldes, id_empleado)
							 	 VALUES('".$folio_num_molde."','3','".$fecha."','".$hora."','7','".$vendedor."')") or die(mysql_error());
				}
			}

			elseif( $categoria == 2 )
			{
				$consulta_empleado_servicio = mysql_query("SELECT id_empleado, id_subcategoria FROM pago_consultas 
														   WHERE id_registro_descripcion_venta=".$id_registro) or die(mysql_error());

				$row_empleado_servicio = mysql_fetch_array($consulta_empleado_servicio);
				$id_empleado_servicio = $row_empleado_servicio["id_empleado"];
				$id_subcategoria_servicio = $row_empleado_servicio["id_subcategoria"];

				$consulta_comision = mysql_query("SELECT comision FROM comisiones_consultas 
												  WHERE id_subcategoria=".$id_subcategoria_servicio." AND id_empleado=".$id_empleado_servicio) or die(mysql_error());
				
				$row_comision = mysql_fetch_array($consulta_comision);
				$comision = $row_comision["comision"];
				$costo_unitario = $cantidad * $costo_unitario;
				if( $descuento > 0 )
				{
					$descuento_por_venta = ($costo_unitario * $descuento)/100;
					$costo_descuento = $costo_unitario - $descuento_por_venta;
					$comision_empleado = ($costo_descuento * $comision)/100;
					$comision_total = $costo_descuento - $comision_empleado;	
				}
				else
				{
					$comision_empleado = ($costo_unitario * $comision)/100;
					$comision_total = $costo_unitario - $comision_empleado;
				}

				mysql_query("UPDATE pago_consultas SET pago='".$comision_total."'
							 WHERE id_registro_descripcion_venta=".$id_registro)or die(mysql_error());
			}
		}
		$consulta_datos_venta = mysql_query("SELECT costo_unitario, descripcion, SUM(cantidad) AS cantidad
											 FROM descripcion_venta 
											 WHERE folio_num_venta='".$folio_num_venta."' AND id_sucursal='".$sucursal."' GROUP BY descripcion") or die(mysql_error());
		
		while( $row3 = mysql_fetch_array($consulta_datos_venta) ) 
		{								
			$cantidad = $row3["cantidad"];
			$descripcion = $row3["descripcion"];
			$costo_unitario = $row3["costo_unitario"];
			
			if( $cantidad <= 3 )
			{
				$costo_unitario = $costo_unitario;	
			}
			elseif( $cantidad >= 4 && $cantidad <= 39 )
			{
				$rebaja = ($costo_unitario * 10)/100;
				$costo_unitario = $costo_unitario - $rebaja;
				mysql_query("UPDATE descripcion_venta SET costo_unitario='".$costo_unitario."'
							 WHERE descripcion='".$descripcion."' AND folio_num_venta=".$folio_num_venta." AND id_sucursal=".$sucursal) or die(mysql_error());
			}
			else
			{
				$rebaja = ($costo_unitario * 20)/100;
				$costo_unitario = $costo_unitario - $rebaja;
				mysql_query("UPDATE descripcion_venta SET costo_unitario='".$costo_unitario."'
							 WHERE descripcion='".$descripcion."'  AND folio_num_venta=".$folio_num_venta." AND id_sucursal=".$sucursal) or die(mysql_error());
			}
		}

		mysql_query("INSERT INTO ventas(id_sucursal, folio_num_venta, fecha, hora, descuento, vendedor, total)
					 VALUES('".$sucursal."','".$folio_num_venta."','".$fecha."','".$hora_venta."','".$descuento."','".$vendedor."','".$total_venta."')") or die(mysql_error());
		
		$consulta_todo_descripcion_venta = mysql_query("SELECT * FROM descripcion_venta 
														WHERE folio_num_venta=".$folio_num_venta." AND id_sucursal=".$sucursal) or die(mysql_error());
		
		while( $row_todo_descripcion_venta = mysql_fetch_array($consulta_todo_descripcion_venta) )
		{ 
			$id_registro_descripcion_venta = $row_todo_descripcion_venta["id_registro"];	
			mysql_query("DELETE FROM venta_temporal 
						 WHERE id_registro_descripcion_venta=".$id_registro_descripcion_venta) or die(mysql_error());
		}
		?>
    	<script language='javascript' type='text/javascript'>
			alert('La venta ha sido generada exitosamente !');
			window.open("impresion_ticket_venta.php?folio_num_venta=<?php echo $folio_num_venta; ?>&sucursal=<?php echo $sucursal; ?>&vendedor=<?php echo $vendedor; ?>&fecha=<?php echo $fecha; ?>", "Ticket de Ventas", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href = 'nueva_venta2.php';
			//window.location.href = 'ticket_venta.php?folio_num_venta=<?php echo $folio_num_venta; ?>&sucursal=<?php echo $sucursal; ?>&vendedor=<?php echo $vendedor; ?>&fecha=<?php echo $fecha; ?>';
		</script>
	<?php
	}
?>