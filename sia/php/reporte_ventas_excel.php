<?php
    // SE RECIBEN LAS VARIABLES DEL FORMULARIO
	$sucursal = $_POST['id_sucursal'];
    $hora_recibida = $_POST['hora'];
	$fecha_final_mysql = $_POST['fecha_final'];
	$fecha_final_separada = explode("-", $fecha_final_mysql);
	$fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
	$fecha_inicio_mysql = $_POST['fecha_inicio'];
	$fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];

    // SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=reporte_de_ventas.xls");

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");

    // EN CASO DE LLEGAR LAS DOS SUCURSALES
    if($sucursal==3){
        $consultaSucursal="";
        $consultaSucursal2="";        

    }else{
        $consultaSucursal=" WHERE id_sucursal = '$sucursal' ";
        $consultaSucursal2=" AND id_sucursal = '$sucursal' ";
    }
     // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL A LA QUE PERTENECE EL REPORTE    
    $query_nombre_sucursal = "SELECT nombre
                              FROM sucursales".$consultaSucursal;

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_nombre_sucursal = mysql_query($query_nombre_sucursal) or die(mysql_error());
    $nombre_sucursal="";
    $sucu=0;
    while($row_nombre_sucursal = mysql_fetch_array($resultado_nombre_sucursal)){
        $sucu++;
        if($sucu==1){
            $nombre_sucursal.= $row_nombre_sucursal['nombre'];
        }else{
            $nombre_sucursal.= " & ".$row_nombre_sucursal['nombre'];
        }
       
    }

    // SE REALIZA QUERY QUE OBTIENE LAS VENTAS DE ACUERDO A LA FECHA INDICADA Y A LA SUCURSAL
    $query_ventas = "SELECT id_sucursal,folio_num_venta,fecha,hora,descuento,vendedor
                     FROM ventas
                     WHERE fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'".$consultaSucursal2."
                     ORDER BY fecha ASC";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_venta = mysql_query($query_ventas) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>  </title>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
               <!-- <div class="categoria" style="width: 550px;">
                    Reporte de Pago de Consultas
                </div> -->
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <center>
                    <table border="1">
                        <tr>
                            <td colspan="10" style="text-align: center; font-size: 26px; font-weight: bold; background-color: #7f7f7f; color: #FFF;"> Reporte de Ventas </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="text-align: center; font-size: 18px; background-color: #7f7f7f; color: #FFF;"> Del: <?php echo $fecha_inicio; ?> &nbsp;&nbsp; Al: <?php echo $fecha_final; ?> </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="text-align: center; font-size: 18px; background-color: #7f7f7f; color: #FFF;"> Sucursal: <?php echo $nombre_sucursal; ?> </td>
                        </tr>
                    </table>
                    <table border="1">
                        <tr>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> N° de Folio </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Descripci&oacute;n </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Categoria </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Cantidad </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Costo Unitario </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Descuento </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Vendedor </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Fecha </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Hora </th>
                            <th style="text-align: center; font-size: 14px; font-weight: bold;"> Costo Total </th>
                        </tr>
                <?php
                    // SE DECLARA UNA VARIABLE PARA IR SUMANDO EL COSTO TOTAL
                    $costo_total = 0;
                    $costo_descuento = 0;
                    $costo_con_descuento = 0;
                    $cantidad_total_de_productos = 0;
                    $cantidad_total_total = 0;

                    // SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS RESULTADO OBTENIDOS
                    while( $row_venta = mysql_fetch_array($resultado_venta) )
                    {
                        $id_sucursal = $row_venta['id_sucursal'];
                        $folio_venta = $row_venta['folio_num_venta'];
                        $descuento = $row_venta['descuento'];
                        $vendedor = $row_venta['vendedor'];
                        $fecha = $row_venta['fecha'];
                        $fecha_separada = explode("-", $fecha);
                        $fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
                        $hora = $row_venta['hora'];

                        // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL VENDEDOR
                        $query_nombre_vendedor = "SELECT alias
                                                  FROM empleados
                                                  WHERE id_empleado = '$vendedor'";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO OBTENIDO
                        $resultado_nombre_vendedor = mysql_query($query_nombre_vendedor) or die(mysql_error());
                        $row_nombre_vendedor = mysql_fetch_array($resultado_nombre_vendedor);
                        $nombre_vendedor = $row_nombre_vendedor['alias'];

                        // SE REALIZA QUERY QUE OBTIENE LA DESCRIPCION DE LAS VENTAS
                        $query_descripcion_venta = "SELECT descripcion,cantidad,costo_unitario,id_categoria
                                                    FROM descripcion_venta
                                                    WHERE folio_num_venta = '$folio_venta'
                                                    AND id_sucursal = '$id_sucursal'";

                        // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
                        $resultado_descripcion_venta = mysql_query($query_descripcion_venta) or die(mysql_error());

                        // SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS RESULTADOS
                        while( $row_descripcion_venta = mysql_fetch_array($resultado_descripcion_venta) )
                        {
                            $descripcion = $row_descripcion_venta['descripcion'];
                            //$descripcion_separada = explode(" ", $descripcion);
                            //$descripcion_folio = $descripcion_separada[2];
                            $cantidad = $row_descripcion_venta['cantidad'];
                            $costo_unitario = $row_descripcion_venta['costo_unitario'];
                            $id_categoria = $row_descripcion_venta['id_categoria'];
                            $costo_total = $cantidad * $costo_unitario;
                            $costo_descuento = ($costo_total * $descuento)/100;
                            $costo_con_descuento = $costo_total - $costo_descuento;

                            // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA CATEGORIA A LA QUE PERTENECE LA VENTA
                            $query_nombre_categoria = "SELECT categoria
                                                       FROM categorias_productos
                                                       WHERE id_categoria = '$id_categoria'";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_nombre_categoria = mysql_query($query_nombre_categoria) or die(mysql_error());
                            $row_nombre_categoria = mysql_fetch_array($resultado_nombre_categoria);
                            $nombre_categoria = $row_nombre_categoria['categoria'];
                            $cantidad_total_de_productos = $cantidad_total_de_productos + $cantidad;
                            $cantidad_total_total = $cantidad_total_total + $costo_con_descuento;

                        ?>
                            <tr>
                                <td style="text-align: center;"> <?php echo $folio_venta; ?> </td>
                                <td style="text-align: center;"> <?php echo $descripcion; ?> </td>
                                <td style="text-align: center;">
                                    <?php 
                                        //Buscar si existe un molde                                      
                                        $buscarMolde = "molde";
                                        $resultadoMolde = strpos($descripcion, $buscarMolde);
                                        //Buscar si existe reparacion
                                        $buscarReparacion = "reparacion";
                                        $resultadoReparacion = strpos($descripcion, $buscarReparacion);
                                        //Condiciones si encontro Molde
                                        if($resultadoMolde != false){                                            
                                            echo "Molde";
                                        }
                                        //Condicion si encontro Reparacion
                                        if($resultadoReparacion != false){
                                            echo "Reparaci&oacute;n";
                                        }else{
                                            echo $nombre_categoria;
                                        }
                                    ?>
                                </td>
                                <td style="text-align: center;"> <?php echo $cantidad; ?> </td>
                                <td style="text-align: center;"> <?php echo "$ ".number_format($costo_unitario,2); ?> </td>
                                <td style="text-align: center;"> <?php echo $descuento." %"; ?> </td>
                                <td style="text-align: center;"> <?php echo $nombre_vendedor; ?> </td>
                                <td style="text-align: center;"> <?php echo $fecha_normal; ?> </td>
                                <td style="text-align: center;"> <?php echo $hora; ?> </td>
                                <td style="text-align: center;"> <?php echo "$ ".number_format($costo_con_descuento,2); ?> </td>
                            </tr>
                        <?php
                        }

                    }
                ?>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Cantidad Total <br /> <?php echo $cantidad_total_de_productos; ?> </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Total <br /> <?php echo "$ ".number_format($cantidad_total_total,2); ?>  </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="text-align: center; font-size: 12px; font-weight: bold;"> Fecha y hora de impresi&oacute;n: &nbsp; <?php echo date('d/m/Y'); ?> &nbsp;&nbsp; <?php echo $hora_recibida; ?> </td>
                        </tr>
                    </table>
                </center>
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>