<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Ticket Venta </title>

	<style>
		body
		{
			font-size:14px;	
			font:"Courier New";
		}

		#cont
		{
			margin:0px auto;
			text-align:center;		
			padding-bottom:5px;
		}

		#impresion
		{
			text-align:center;
			width:220px;
			font-size:14px;	
			font:"Courier New";
		}

		#impresion table
		{
			margin:0px auto;
			font:"Courier New";
			font-size:12px;	
			font-size:10px;
		}

		#impresion table tr th
		{
			font-size:12px;	
			font:"Courier New";
		}

		#impresion table tr td
		{
			text-align:right;
			padding:5px 5px;
			font-size:12px;	
			font:"Courier New";
		}

		span
		{
			text-decoration:underline;
			font-size:14px;
			font:"Courier New";	
		}
	</style>
	</head>
	<body onload="javascript: window.print();">
<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBEN LAS VARIABLES A TRAVES DEL METODO GET ENVIADAS DESDE EL FORMULARIO
	$folio_num_venta = $_GET['folio_num_venta'];
	$sucursal = $_GET['sucursal'];
	$vendedor = $_GET['vendedor'];
	$fecha = $_GET['fecha'];
	$fecha_separada = explode("-", $fecha);
	$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];

	// QUERY QUE OBTIENE EL TOTAL DE LA VENTA DE LA TABLA DE VENTAS
	$query_total_venta = "SELECT total
						  FROM ventas
						  WHERE id_sucursal = '$sucursal'
						  AND folio_num_venta = '$folio_num_venta'
						  AND vendedor = '$vendedor'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE
	$resultado_total_venta = mysql_query($query_total_venta) or die(mysql_error());
	$row_total_venta = mysql_fetch_array($resultado_total_venta);
	$total_venta = $row_total_venta['total'];
	
	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO QUE HIZO LA VENTA
	$query_nombre_empleado = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
							  FROM empleados
							  WHERE id_empleado = '$vendedor'";

	// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
	$resultado_nombre_empleado = mysql_query($query_nombre_empleado) or die(mysql_error());

	$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
	$nombre_empleado = $row_nombre_empleado['nombre_empleado'];
?>
<div id="cont">
<div id="impresion">
JORGE LEAL Y CIA. S.A. DE C.V.<br />
Sebastian Lerdo de Tejada 186<br /> 
Col. Centro     Morelia     Michoacan<br />
CP. 58000 <br/> 
RFC: JLE010622GK2 <br />
Telefono: (443) 312-5994<br />
Ticket de Venta<br />
Vendedor: <?php echo $nombre_empleado; ?><br />
Fecha: <?php echo $fecha_normal; ?><br />
<table>	
    <tr>
    	<td style="text-align:center; font-weight:bold;"> Descripción </td>
        <td style="font-weight:bold; text-align:center;"> Cantidad </td>
    </tr>
    <tr>
    	<td colspan="2">-------------------------------------------------</td>
    </tr>
	<?php
		// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA VENTA
		$venta = "SELECT id_sucursal,id_categoria,folio_num_venta,descripcion,cantidad,costo_unitario
				  FROM descripcion_venta
				  WHERE folio_num_venta = '$folio_num_venta'
				  AND id_sucursal = '$sucursal'";

		// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
		$resultado_venta = mysql_query($venta) or die(mysql_error());

		// SE DECLARA UNA VARIABLE PARA IR HACIENDO EL TOTAL
		$x=0;

		// SE REALIZA UN CICLO PARA MOSTRAR LA DESCRIPCION DE LA VENTA
		while( $row_descripcion_venta = mysql_fetch_array($resultado_venta) )
		{
			$descripcion = $row_descripcion_venta['descripcion'];
			$cantidad = $row_descripcion_venta['cantidad'];
			$costo_unitario = $row_descripcion_venta['costo_unitario'];
	?>
			<tr> 
    			<td style="text-align:left;"> <?php echo $descripcion; ?> </td>
        		<td style="text-align:left;"> <?php echo "(".$cantidad.")"." ".number_format($costo_unitario,2); ?> </td>
    		</tr>
	<?php
		}
	?>
	<tr>
    	<td> </td>
        <td style="text-align:center; font-weight:bold;"> Total: <?php echo "$".number_format($total_venta,2); ?> </td>
    </tr>
</table>
</div>
<p>&nbsp;&nbsp;</p>
</div>
</body>
</html>