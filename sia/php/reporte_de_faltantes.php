<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE LAS CATEGORIAS
    $query_proveedores = "SELECT id_proveedor,proveedor
    					 FROM proveedores
    					 ORDER BY id_proveedor ASC";
    
    // SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
    $resultado_query_proveedores = mysql_query($query_proveedores) or die(mysql_error());

    function array_sort_by_column(&$arr, $col, $dir = SORT_DESC)
    {
        $sort_col = array();
        foreach ($arr as $key=> $row)
        {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }
    //array_sort_by_column($arreglo, 'a_pedir');
    //foreach($arreglo AS $var)
    //{
    //    echo $var['a_pedir']."<br>";
    //}
	//echo "<pre>";
	//    print_r($arreglo);
	//echo "</pre>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Faltantes de Inventario </title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css">
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="wrapp">       
	<div id="contenido_columna2">
    	<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria" style="width:405px">
                	Reporte de Faltantes
           		</div>
        	</div><!--Fin de fondo titulo-->
        	<div class="buscar2">
                <form name="Buscar" action="reporte_de_faltantes.php" method="post">
                    <select name="proveedor" id="proveedor">
                        <option value="0"> Proveedores </option>
                <?php
                    // SE REALIZA UN CICLO PARA MOSTRAR LAS CATEGORIAS
                    while( $row_proveedor = mysql_fetch_array($resultado_query_proveedores) )
                    {
                        $id_proveedor = $row_proveedor['id_proveedor'];
                        $proveedor = $row_proveedor['proveedor'];
                ?>
                        <option value="<?php echo $id_proveedor; ?>"> <?php echo ucfirst(strtolower($proveedor)); ?> </option>
                <?php
                    }
                ?>
                    </select>
                    <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height: 25px;"/>
                </form><!-- FIN DIV FORM BUSCAR -->
            </div><!-- FIN DIV BUSCAR 2 -->
        	<div class="area_contenido1">
            	<div class="contenido_proveedor">
            	<center>
            		<?php
            			// SE DECLARA UN CONTADOR
            			$contador = 0;

            			// SE RECIBEN LAS VARIABLES DE LA BUSQUEDA
            			$id_proveedor_busqueda = $_POST['proveedor'];

            			// SE VALIDA SI SE HA BUSCADO EL REPORTE POR ALGUN PROVEEDOR
            			if ( isset($_POST['buscar']) && $id_proveedor_busqueda != 0 )
            			{
            				// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES DE CADA PROVEEDOR
							$query_refacciones_proveedor = "SELECT id_base_producto,inventario_minimo,inventario_maximo,refacciones.id_subcategoria AS id_subcategoria,codigo,concepto
															FROM base_productos,refacciones,proveedores
															WHERE referencia_codigo = codigo
															AND base_productos.id_subcategoria = refacciones.id_subcategoria
															AND activo = 'Si'
															AND refacciones.id_proveedor = proveedores.id_proveedor
															AND proveedores.id_proveedor = '$id_proveedor_busqueda'
															ORDER BY concepto ASC";
            			}
            			else
            			{
            				// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES DE CADA PROVEEDOR
							$query_refacciones_proveedor = "SELECT id_base_producto,inventario_minimo,inventario_maximo,refacciones.id_subcategoria AS id_subcategoria,codigo,concepto
															FROM base_productos,refacciones
															WHERE referencia_codigo = codigo
															AND base_productos.id_subcategoria = refacciones.id_subcategoria
															AND activo = 'Si'
															ORDER BY concepto ASC";
            			}
            		?>
						    <table style="width:720px;">
					    <?php
					    	if ( $id_proveedor_busqueda != 0 )
					    	{
					    		// SE REALIZA QUERY QUE OBTIENE LA RAZON SOCIAL DEL PROVEEDOR SELECCIONADO
					    		$query_razon_social = "SELECT razon_social
					    							   FROM proveedores
					    							   WHERE id_proveedor = '$id_proveedor_busqueda'";

					    		// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
					    		$resultado_razon_social = mysql_query($query_razon_social) or die(mysql_error());
					    		$row_razon_social = mysql_fetch_array($resultado_razon_social);
					    		$razon_social = $row_razon_social['razon_social'];
					    ?>
					    		<tr>
					    			<th colspan="5"> <?php echo $razon_social; ?> </th>
					    		</tr>
                            <table>
					    <?php
					    	}
							// SE DECLARA LA VARIABLE DE TIPO ARREGLO
							$arreglo_registros = array();
							$arreglo = array();

							// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
							$resultado_refacciones_proveedor = mysql_query($query_refacciones_proveedor) or die(mysql_error());

							// SE REALIZA UN CICLO PARA CONTINUAR CON EL PROCESO
							while ( $row_refaccion = mysql_fetch_assoc($resultado_refacciones_proveedor) )
							{
							    $id_base_producto = $row_refaccion['id_base_producto'];
							    $inventario_minimo = $row_refaccion['inventario_minimo'];
							    $inventario_maximo = $row_refaccion['inventario_maximo'];
							    $id_subcategoria = $row_refaccion['id_subcategoria'];
							    $codigo = $row_refaccion['codigo'];
								$concepto = $row_refaccion['concepto'];

								// SE REALIZA QUERY QUE OBTIENE EL STOCK ACTUAL
								$query_stock = "SELECT SUM(num_serie_cantidad) AS cantidad  
								                FROM inventarios
								                WHERE id_articulo = '$id_base_producto' 
								                AND id_subcategoria = '$id_subcategoria'";

								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
								$resultado_query_stock = mysql_query($query_stock) or die(mysql_error());
								$row_stock = mysql_fetch_array($resultado_query_stock);
								$stock_actual = $row_stock['cantidad'];
								$a_pedir = $inventario_maximo - $stock_actual;

								if ( $stock_actual < $inventario_minimo )
								{
									$arreglo_registros = array(
										"concepto" => $concepto,
										"codigo" => $codigo,
										"stock_actual" => $stock_actual,
										"inventario_maximo" => $inventario_maximo,
										"a_pedir" => $a_pedir 
									);
									$arreglo[] = $arreglo_registros;
									$contador++;
								}
							}
                            array_sort_by_column($arreglo, 'a_pedir');
                        ?>
                        <table style="width: 720px;">
                            <tr>
						        <th style="font-size:12px;"> Refacci&oacute;n </th>
						        <th style="font-size:12px;"> C&oacute;digo </th>
						        <th style="font-size:12px;"> Stock Actual </th>
						        <th style="font-size:12px;"> Inventario M&aacute;ximo </th>
						        <th style="font-size:12px;"> Cantidad a Pedir  </th>
						    </tr>
                    <?php
                        foreach($arreglo AS $var)
                        {
                        ?>
                            <tr>
							    <td> <?php echo $var['concepto']; ?> </td>
							    <td style="text-align:center;"> <?php echo $var['codigo'] ?> </td>
							    <td style="text-align:center;">
							        <?php
							            if ( is_null($var['stock_actual']) )
							            {
							                echo "0";
							            }
							            else
							            {
							                echo $var['stock_actual'];
							            }
							        ?>
							    </td>
							    <td style="text-align:center;"> <?php echo $var['inventario_maximo']; ?> </td>
							    <td style="text-align:center;"> <?php echo $var['a_pedir']; ?> </td>
							</tr>
							<tr>
							    <td colspan="5"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
							</tr>
                        <?php
                        }
                        if( $contador == 0 )
                        {
                        ?>
                            <tr>
							    <td colspan="5" style="text-align:center;"> <h3> No existen refacciones con cantidad menor al Stock M&iacute;nimo </h3> </td>
							</tr>
                        <?php
                        }
                        ?>
                        </table>
            	</center>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>