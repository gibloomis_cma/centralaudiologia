<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Inventario</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script language="javascript">
<!-- Se abre el comentario para ocultar el script de navegadores antiguos
function muestraReloj()
{
// Compruebo si se puede ejecutar el script en el navegador del usuario
if (!document.layers && !document.all && !document.getElementById) return;
// Obtengo la hora actual y la divido en sus partes
var fechacompleta = new Date();
var horas = fechacompleta.getHours();
var minutos = fechacompleta.getMinutes();
var segundos = fechacompleta.getSeconds();
var mt = "AM";
// Pongo el formato 12 horas
if (horas >= 12) {
mt = "PM";
horas = horas - 12;
}
if (horas == 0) horas = 12;
// Pongo minutos y segundos con dos dígitos
if (minutos <= 9) minutos = "0" + minutos;
if (segundos <= 9) segundos = "0" + segundos;
// En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
 cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
 // Escribo el reloj de una manera u otra, según el navegador del usuario
if (document.layers) {
document.layers.spanreloj.document.write(cadenareloj);
document.layers.spanreloj.document.close();
}
else if (document.all) spanreloj.innerHTML = cadenareloj;
else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
// Ejecuto la función con un intervalo de un segundo
setTimeout("muestraReloj()", 1000);
}
// Fin del script -->
</script>
</head>
<?php 
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha = date("d/m/Y");
?>
<body onLoad="muestraReloj()"> 
<div id="wrapp">     
<div id="contenido_columna2">
	<div class="contenido_pagina">
		<div class="fondo_titulo1">
			<div class="categoria">
                Inventario
            </div>
		</div><!--Fin de fondo titulo-->  
		<div class="area_contenido1">
		<?php
                include("config.php");
                include("metodo_cambiar_fecha.php");
                $id_base_producto = $_GET["id_base_producto"];
                $consulta_datos_base_producto = mysql_query("SELECT id_categoria, id_articulo, id_subcategoria, descripcion,
                                                                     referencia_codigo 
                                                                FROM base_productos
                                                                WHERE id_base_producto=".$id_base_producto) 
                                                                or die(mysql_error());
                $row = mysql_fetch_array($consulta_datos_base_producto);
                $id_categoria = $row["id_categoria"];
                $articulo = $row["id_articulo"];
                $id_subcategoria = $row["id_subcategoria"];
                $referencia_codigo = $row["referencia_codigo"];
                $descripcion = $row["descripcion"];
                $consulta_subcategoria = mysql_query("SELECT * FROM subcategorias_productos
                                                                WHERE id_subcategoria=".$id_subcategoria)
                                                                 or die(mysql_error());
                $row3 = mysql_fetch_array($consulta_subcategoria);
                $subcategoria = $row3["subcategoria"];
        ?>
            <div class="contenido_proveedor" style="margin-top:5px;">
                <table>
                    <tr>
                        <th colspan="4">Datos del Productos</th>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Categoria: </label>
                        </td><td id="alleft">
                        <?php 
                            if($id_categoria == 1){
                                echo "Refacciones";
                            }elseif($id_categoria == 4){
                                echo "Baterias";
                            }else{
                                echo "Modelos";
                            }
                        ?>
                        </td>
                        <?php if($id_categoria == 4){ ?>
                        <td></td>
                        <td></td>
                        <?php }else{ ?>
                        <td id="alright">
                            <label class="textos">Subcategoria: </label> 
                        </td><td id="alleft">
                            <?php echo $subcategoria; ?>
                        </td>
                        <?php }?>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Codigo: </label> 
                        </td><td id="alleft">
                            <?php echo $referencia_codigo; ?>
                        </td><td id="alright">
                            <label class="textos">Producto: </label> 
                        </td><td id="alleft">
                            <?php echo $descripcion; ?>
                        </td>
                   </tr>
                </table><br />         	
                <center>
                <table>
                    <tr>
                    	<th>Sucursal</th>
                        <th>Almacen</th>
                        <th>Fecha entrada / hora</th>
   						<?php
                 			if($id_categoria <> 5){
    					?>
                        <th>Cantidad</th>
    					<?php
                    		}else{
   						?>
                        <th>Num. Serie</th>
                        <th></th>
    					<?php
                   			}
    					?>
                    </tr>
    				<?php
					if($id_categoria == 4 or $id_categoria == 1){
						
						$consulta_productos_inventario = mysql_query("SELECT  fecha_entrada, hora,id_almacen, SUM(num_serie_cantidad) AS num_serie_cantidad
																			FROM inventarios
																			WHERE id_articulo='".$id_base_producto."' 
																			GROUP BY id_almacen") 
																			or die(mysql_error());
					}else{
						$consulta_productos_inventario = mysql_query("SELECT fecha_entrada, hora,id_almacen, 
																			num_serie_cantidad,id_registro
																			FROM inventarios
																			WHERE id_articulo='".$id_base_producto."'
																			ORDER BY id_almacen ASC") 
																			or die(mysql_error());
					}
						$n=0;
						while($row2 = mysql_fetch_array($consulta_productos_inventario)){
							$fecha_entrada = $row2["fecha_entrada"];
							$id_registro = $row2["id_registro"];
							$hora = $row2["hora"];
							$id_almacen = $row2["id_almacen"];
							$num_serie_cantidad = $row2["num_serie_cantidad"];
							
							$fecha_normal = cambiaf_a_normal($fecha_entrada);
							$consulta_almacen = mysql_query("SELECT * FROM almacenes WHERE id_almacen=".$id_almacen) or die(mysql_error());
							$row5 = mysql_fetch_array($consulta_almacen);
							$descripcion_almacen = $row5["almacen"];
							$id_sucursal= $row5["id_sucursal"];
							$consulta_sucursal=mysql_query("SELECT nombre FROM sucursales 
																			WHERE id_sucursal=".$id_sucursal)
																			or die(mysql_error());
							$row_sucursal_nombre=mysql_fetch_array($consulta_sucursal);
							$nombre_sucursal=$row_sucursal_nombre["nombre"];
							$n++;
   					?>
                    <tr>
                    	<td><?php echo $nombre_sucursal; ?></td>
                        <td><?php echo $descripcion_almacen; ?></td>
                        <td style="text-align:center;"><?php echo $fecha_normal; ?> <?php echo $hora; ?></td>
                        <td><?php echo $num_serie_cantidad; ?></td>
                  	<?php 
						if($id_categoria == 5){
					?>
                        <td>
                        	<a href="modificar_num_serie.php?id_registro_inventario=<?php echo $id_registro; ?>&id_base_producto=<?php echo $id_base_producto; ?>">
                        		<img src="../img/modify.png" />
                        	</a>
                       	</td>
                    <?php
						}
					?>
                    </tr>
     				<?php
               			}
						if($n==0){
     				?>
                    <tr>
                    	<td style="text-align:center" colspan="3">
                    		<label class="textos">No hay productos en existencia</label>
                        </td>
                    </tr>                    
                    <?php
						}
					?>
                </table>
                </center>
            </div><!--Fin de contenido proveedor-->
      		<br />            
            <div class="contenido_proveedor">
            <form name="forma1" action="proceso_agregar_producto.php" method="post" class="valida_agregar_producto">
            <input name="id_subcategoria" type="hidden" value="<?php echo $id_subcategoria; ?>" />
            <input name="id_categoria" type="hidden" value="<?php echo $id_categoria; ?>" />
            <input name="id_base_producto" type="hidden" value="<?php echo $id_base_producto; ?>" />
            <table>
            	<tr>
                	<th colspan="4">
                    	Agregar Producto
                    </th>
                </tr><tr>
                	<td id="alright">
                		<label class="textos">Almacen: </label>
                	</td><td id="alleft">
                    	<select name="id_almacen" style="width:200px;">
                            <option value="0">Seleccione</option>
            				<?php
								$consulta_almacenes = mysql_query("SELECT * FROM almacenes") or die(mysql_error());
								while($row4 = mysql_fetch_array($consulta_almacenes)){
									$id_almacen = $row4["id_almacen"];
									$almacen = $row4["almacen"];
            				?>
                            <option value="<?php echo $id_almacen; ?>">
                                <?php echo $almacen; ?>
                            </option>
            				<?php
                        		}
            				?>
                        </select>
                    </td>
                </tr><tr>
                	<td id="alright">
                <label class="textos">Fecha: </label>
                	</td><td id="alleft">
                        <input name="fecha_entrada" type="text" value="<?php echo $fecha; ?>" readonly="readonly" style="width:200px;" />
                        <div id="spanreloj"></div>
                    </td>                
            	</tr><tr>
                	
				   	<?php
                		if($id_categoria == "5"){
				    ?>
                    <td id="alright">
                        <label class="textos">Num. Serie: </label>
                    </td><td id="alleft">
                        <input name="num_serie" type="text" style="width:200px;" />
   					</td>
					<?php
                		}else{
    				?>
                    <td id="alright">
                        <label class="textos">Cantidad: </label>
                    </td><td id="alleft">
                        <input name="cantidad" type="text" style="width:200px;" />
                    </td>
					<?php
        				}
				    ?>
                </tr><tr>
                	<td id="alright">            
		                <label class="textos">Observaciones: </label>
                	</td><td id="alleft" colspan="3">
        		        <textarea name="observaciones"  style="width:200px;" rows="4"></textarea>
                	</td>
                </tr><tr>
                	<td id="alright" colspan="4">
                        <input name="volver" value="Volver" class="fondo_boton" type="button" 
                        onclick="window.location.href='lista_inventario.php'" />
                        <input name="accion" type="submit" value="Guardar" class="fondo_boton" />
                	</td>
                </tr>
            </table>
            </form>
            </div><!--Fin de contenido proveedor-->
        </div><!-- Fin de area contenido --> 
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div>
</body>
</html>