<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include('config.php');

    // SE VALIDA SI EL BOTON DE DATOS PERSONALES HA SIDO OPRIMIDO
    if( isset($_POST['btn_datos_personales']) && $_POST['btn_datos_personales'] == "Guardar Cambios" )
    {
        // SE RECIBEN LAS VARIABLES DEL FORMULARIO
        $id_empleado_sistema = $_POST['id_empleado_sistema'];
        $hora_real = $_POST['hora_real'];
        $folio_molde = $_POST['folio_molde'];
        $fecha_actual = $_POST['fecha_actual'];
        $nombre_paciente = $_POST['nombre_paciente'];
        $paterno_paciente = $_POST['paterno_paciente'];
        $materno_paciente = $_POST['materno_paciente'];
        $calle = $_POST['calle'];
        $num_exterior = $_POST['num_exterior'];
        $num_interior = $_POST['num_interior'];
        $colonia = $_POST['colonia'];
        $codigo_postal = $_POST['codigo_postal'];
        $telefono = $_POST['telefono'];
        $estado = $_POST['estado'];
        $ciudad = $_POST['ciudad'];

        // SE REALIZA EL QUERY QUE HACE EL UPDATE DE LOS DATOS GENERALES DEL PACIENTE
        $query_update_datos_paciente = "UPDATE moldes
                                        SET nombre = '$nombre_paciente', paterno = '$paterno_paciente', materno = '$materno_paciente', descripcion = '$telefono', calle = '$calle', num_exterior = '$num_exterior', num_interior = '$num_interior', codigo_postal = '$codigo_postal', colonia = '$colonia', id_estado = '$estado', id_ciudad = '$ciudad'
                                        WHERE folio_num_molde = '$folio_molde'";

        // SE VALIDA SI EL QUERY SE EJECUTO CORRECTAMENTE
        if( mysql_query($query_update_datos_paciente) )
        {
        ?>
            <script type="text/javascript" language="javascript">
                 alert('Los datos del paciente <?php echo $nombre_paciente ?> han sido modificados con exito !');
                 window.location.href = 'detalles_moldes_modificacion.php?folio='+<?php echo $folio_molde; ?>;
            </script>
        <?php
        }
        else
        {
            echo mysql_error();
        }
    }
    elseif( isset($_POST['btn_datos_moldes']) && $_POST['btn_datos_moldes'] == "Guardar Cambios" )
    {
        // SE RECIBEN LAS VARIABLES DEL FORMULARIO
        $id_empleado_sistema = $_POST['id_empleado_sistema'];
        $hora_real = $_POST['hora_real'];
        $fecha_actual = $_POST['fecha_actual'];
        $fecha_actual_separada = explode("/", $fecha_actual);
        $fecha_actual_mysql = $fecha_actual_separada[2]."-".$fecha_actual_separada[1]."-".$fecha_actual_separada[0];
        $nombre_paciente = $_POST['nombre_paciente'];
        $folio_molde = $_POST['folio_molde'];
        $estilo = $_POST['estilo'];
        $material = $_POST['material'];
        $color = $_POST['color'];
        $ventilacion = $_POST['ventilacion'];
        $calibre = $_POST['calibre'];
        $salida = $_POST['salida'];
        $lado_oido = $_POST['lado_oido'];
        $costo_total = $_POST['costo_total'];
        $costo_total_molde = $_POST['costo_total_molde'];
        $pago_parcial = $_POST['pago_parcial'];
        $adaptacion = strtolower($_POST['adaptacion']);
        $reposicion = strtolower($_POST['reposicion']);
        $estatus_moldes = $_POST['estatus_moldes'];
        $observaciones = $_POST['observaciones'];

        // SE VALIDA SI LA ADAPTACION = NO, SE COLOCA EN BLANCO
        if( $adaptacion == "no" )
        {
            $adaptacion = "";
        }

        // SE VALIDA SI LA REPOSICION = NO, SE COLOCA EN BLANCO
        if( $reposicion == "no" )
        {
            $reposicion = "";
        }
		
		if($costo_total == 0){
			$costo_total = $costo_total_molde;
		}

        // SE REALIZA QUERY QUE OBTIENE EL ESTATUS ANTERIOR DEL MOLDE
        $query_estatus_anterior = "SELECT id_estatus_moldes
                                   FROM moldes
                                   WHERE folio_num_molde = '$folio_molde'";

        // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
        $resultado_estatus_anterior = mysql_query($query_estatus_anterior) or die(mysql_error());
        $row_estatus_anterior = mysql_fetch_array($resultado_estatus_anterior);
        $estatus_anterior = $row_estatus_anterior['id_estatus_moldes'];


        // SE REALIZA EL QUERY QUE HACE EL UPDATE DE LOS DATOS DEL MOLDE DEL PACIENTE
        $query_update_datos_molde = "UPDATE moldes
                                     SET lado_oido = '$lado_oido', id_estilo = '$estilo', id_material = '$material', id_color = '$color', id_ventilacion = '$ventilacion', id_salida = '$salida', costo = '$costo_total', id_estatus_moldes = '$estatus_moldes', observaciones = '$observaciones', adaptacion = '$adaptacion', reposicion = '$reposicion'
                                     WHERE folio_num_molde = '$folio_molde'";

        // SE REALIZA QUERY QUE HACE UN INSERT EN LA TABLA MOVIMIENTOS MOLDES PARA REGISTRAR EL NUEVO MOVIMIENTO
        $query_nuevo_movimiento = "INSERT INTO movimientos_moldes(folio_num_molde,id_estado_movimiento,fecha,hora,id_estatus_moldes,id_empleado)
                                   VALUES('$folio_molde','1','$fecha_actual_mysql','$hora_real','$estatus_moldes','$id_empleado_sistema')";

        // SE VALIDA SI EL ESTATUS ES DIFERENTE AL ANTERIOR QUE TENIA EL MOLDE
        if( $estatus_moldes == $estatus_anterior )
        {
            // SE EJECUTA SOLO EL QUERY DEL UPDATE Y SE VALIDA SI SE EJECUTO CORRECTAMENTE
            if( mysql_query($query_update_datos_molde) )
            {
            ?>
                <script type="text/javascript" language="javascript">
                    alert('Los datos del molde del paciente <?php echo $nombre_paciente; ?> han sido modificados con exito !');
                    window.location.href = 'detalles_moldes_modificacion.php?folio='+<?php echo $folio_molde; ?>;
                </script>
            <?php
            }
            else
            {
                echo mysql_error();
            }
        }
        else
        {
            // SE EJECUTA EL QUERY DEL UPDATE Y SE VALIDA PARA SABER SI SE HA EJECUTADO CORRECTAMENTE
            if( mysql_query($query_update_datos_molde) )
            {
                // SE EJECUTA EL QUERY DEL INSERT EN LA TABLA MOVIMIENTOS MOLDES
                if( mysql_query($query_nuevo_movimiento) )
                {
                ?>
                    <script type="text/javascript" language="javascript">
                        alert('Los datos del molde del paciente <?php echo $nombre_paciente; ?> han sido modificados con exito !');
                        window.location.href = 'detalles_moldes_modificacion.php?folio='+<?php echo $folio_molde; ?>;
                    </script>
                <?php
                }
                else
                {
                    echo mysql_error();
                }
            }
            else
            {
                echo mysql_error();
            }
        }

    }
    elseif( isset($_POST['btn_datos_recibio']) && $_POST['btn_datos_recibio'] == "Guardar Cambios" )
    {
        // SE RECIBEN LAS VARIABLES DEL FORMULARIO
        $empleado = $_POST['empleado'];
        $hora_real = $_POST['hora_real'];
        $folio_molde = $_POST['folio_molde'];
        $fecha_actual = $_POST['fecha_actual'];
        $fecha_actual_separada = explode("/", $fecha_actual);
        $fecha_actual_mysql = $fecha_actual_separada[2]."-".$fecha_actual_separada[1]."-".$fecha_actual_separada[0];

        // SE REALIZA QUERY QUE OBTIENE EL ID DEL REGISTRO A MODIFICAR
        $query_id_registro = "SELECT id_registro
                              FROM movimientos_moldes
                              WHERE folio_num_molde = '$folio_molde'
                              AND id_estado_movimiento = 2
                              AND id_estatus_moldes = 2
                              ORDER BY id_registro DESC
                              LIMIT 1";

        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
        $resultado_id_registro = mysql_query($query_id_registro) or die(mysql_error());
        $row_id_registro = mysql_fetch_array($resultado_id_registro);
        $id_registro = $row_id_registro['id_registro'];

        // SE REALIZA EL QUERY QUE HACE EL UPDATE A LA TABLA DE MOVIMIENTOS MOLDES
        $query_update_nuevo_recibio = "UPDATE movimientos_moldes
                                       SET fecha = '$fecha_actual_mysql', hora = '$hora_real', id_empleado = '$empleado'
                                       WHERE id_registro = '$id_registro'";

        // SE EJECUTA EL QUERY Y SE VALIDA SI SE HA EJECUTA EXITOSAMENTE
        if( mysql_query($query_update_nuevo_recibio) )
        {
        ?>
            <script type="text/javascript" language="javascript">
                alert('Se ha cambiado con exito al empleado !');
                window.location.href = 'detalles_moldes_modificacion.php?folio='+<?php echo $folio_molde; ?>;
            </script>
        <?php
        }
        else
        {
            echo mysql_error();
        }
    }
    elseif( isset($_POST['btn_datos_elaboracion']) && $_POST['btn_datos_elaboracion'] == "Guardar Cambios" )
    {
        // SE RECIBEN LAS VARIABLES DEL FORMULARIO
        $empleado = $_POST['empleado'];
        $observaciones = $_POST['observaciones'];
        $id_empleado_sistema = $_POST['id_empleado_sistema'];
        $hora_real = $_POST['hora_real'];
        $folio_molde = $_POST['folio_molde'];
        $fecha_actual = $_POST['fecha_actual'];
        $fecha_actual_separada = explode("/", $fecha_actual);
        $fecha_actual_mysql = $fecha_actual_separada[2]."-".$fecha_actual_separada[1]."-".$fecha_actual_separada[0];

        // SE REALIZA QUERY QUE HACE UN UPDATE EN LA TABLA DE MOLDES ELABORACION
        $query_update_moldes_elaboracion = "UPDATE moldes_elaboracion
                                            SET elaboro = '$empleado', observaciones = '$observaciones'
                                            WHERE folio_num_molde = '$folio_molde'";

        // SE EJECUTA EL QUERY Y SE VALIDA SI SE HA EJECUTADO CORRECTAMETE EL QUERY
        if( mysql_query($query_update_moldes_elaboracion) )
        {
        ?>
            <script type="text/javascript" language="javascript">
                alert('Los datos de elaboracion se han modificado con exito !');
                window.location.href = 'detalles_moldes_modificacion.php?folio='+<?php echo $folio_molde; ?>;
            </script>
        <?php
        }
        else
        {
            echo mysql_error();
        }
    }


?>