<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8/iso-8859-1" />
	<title>Cuentas por Pagar</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
	<div id="contenido_columna2">
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria">
					Proveedores
				</div><!-- Fin DIV categoria -->    
			</div><!--Fin de fondo titulo-->
			<div class="area_contenido1">
				<br />
				<div class="contenido">
					<!--<div class="nom_proveedor"><label class="textos">Nombre del Proveedor: </label></div>-->
						<div class="etiqueta"><label class="textos">
							<?php
							// SE IMPORTA EL ARCHIVO QUE INCLUYE LA CONEXION A LA BASE DE DATOS
								include("config.php");
							// SE OBTIENE EL ID DEL PROVEEDOR POR MEDIO DEL METODO GET
								$id_proveedor = $_GET['id_proveedor'];
							// SE REALIZA EL QUERY PARA OBTENER EL NOMBRE DEL PROVEEDOR DE ACUERDO AL ID OBTENIDO ANTERIORMENTE
								$query_nombre_proveedor = mysql_query("SELECT razon_social 
																		FROM proveedores 
																		WHERE id_proveedor = $id_proveedor") 
																		or die (mysql_error());
								$row = mysql_fetch_array( $query_nombre_proveedor );
								echo $nombre_proveedor = $row['razon_social'];
							// SE REALIZA EL QUERY DE TODAS LA CUENTAS POR PAGAR DE ACUERDO AL PROVEEDOR SELECCIONADO								
								$cuentas_por_pagar = mysql_query("SELECT cuentas_por_pagar.id_registro as id, folio_orden_compra, fecha, estatus_cuenta
															FROM proveedores,cuentas_por_pagar,estatus_cuentas 
															WHERE cuentas_por_pagar.id_proveedor = proveedores.id_proveedor 
															AND estatus_cuentas.id_estatus_cuentas = cuentas_por_pagar.id_estatus 
															AND cuentas_por_pagar.id_proveedor = '".$id_proveedor."' 
															AND estatus_cuenta <> 'Pagado' 
															GROUP BY folio_orden_compra 
															ORDER BY fecha") or die (mysql_error());
							?>	</label>
						</div><!--Fin de etiqueta-->
				</div><!--Fin de contenido-->
                <br />
                <br />
                <center>                       	
                    <table>
                    	<tr>
                    		<th colspan="4">Nueva Cuenta</th>                            
                        </tr><tr>
                        	<form method="get" action="cuenta_por_pagar_nueva.php">
                        	<td style="text-align:center" colspan="4">Orden de compra:                           	
                            	<input type="hidden" name="id_proveedor" value="<?php echo $id_proveedor; ?>" />
                            	<?php
									if(isset($_GET['folio'])){
										$folio=$_GET['folio'];
										$value='value="Cambiar"';										
									}else{
										$value='value="Cargar"';
										$folio=0;										
									}
									if($folio==0){
										$value='value="Cargar"';
									}
								?>
                            	<select name="folio" >
                                	<option value="0">Seleccionar</option>
                                    <?php
										$ordenes=mysql_query('SELECT folio_orden_compra
																FROM ordenes_de_compra
																WHERE id_proveedor='.$id_proveedor.
																' GROUP BY folio_orden_compra')or die(mysql_error());
										while($row_ordenes=mysql_fetch_array($ordenes)){
											$folio_orden=$row_ordenes['folio_orden_compra'];											
											$cuentas_c=mysql_query('SELECT id_registro FROM cuentas_por_pagar
																	WHERE folio_orden_compra='.$folio_orden)
																	or die(mysql_error());
											$row_cuentas_c=mysql_fetch_array($cuentas_c);
											$id_registro=$row_cuentas_c['id_registro'];
											if($id_registro==""){
												if($folio_orden==$folio){
													$selected='selected="selected"';
												}else{
													$selected="";
												}
									?>
                                    <option value="<?php echo $folio_orden; ?>" <?php echo $selected; ?> ><?php echo $folio_orden; ?></option>
                                    <?php	
											}
										}
									?>                                                                      
                                </select>                                
                                <input type="submit" name="cargar" <?php echo $value; ?> class="fondo_boton" />
                            
                            </td>
                            </form>
                        </tr>
                        <?php
							if($folio!=0){
								$general=mysql_query('SELECT fecha_orden, fecha_entrega,
														cantidad, precio_unitario, fecha_orden 
														FROM ordenes_de_compra 
														WHERE folio_orden_compra='.$folio)or die(mysql_error());
								$row_general=mysql_fetch_array($general);
								$fecha_orden=$row_general['fecha_orden'];
								$fecha_entrega=$row_general['fecha_entrega'];
						?>
                        <tr>
                        	<th>Fecha de Orden:</th>
                            <th>Fecha de Entrega:</th>
                            <th colspan="2">Estatus:</th>
                        </tr>
                        <form action="procesa_cuenta_por_pagar_nueva.php" method="post">
                        <tr>                        	
                        	<td style="text-align:center">
                            	<input type="hidden" name="id_proveedor" value="<?php echo $id_proveedor; ?>" />
                            	<input type="hidden" name="folio" value="<?php echo $folio; ?>" />
								<input type="text" name="fecha_entrega" value="<?php echo $fecha_orden ?>" size="12" style="text-align:center" readonly="readonly" />
                            </td><td style="text-align:center">
								<input type="text" name="fecha_entrega" value="<?php echo $fecha_entrega ?>" size="12" style="text-align:center" />
                            </td><td style="text-align:center" colspan="2">
                            	<select name="id_estatus">
                                	<option value="1">Entregado, No Pagado</option>
                                	<option value="2">No Entregado, No Pagado</option>                                    
                                </select>
                            </td>
                        </tr><tr>
                            <th>Categoria</th>
                            <th>Producto</th>                                        
                            <th>Cantidad</th>
                            <th>Precio Unitario</th>
                        </tr>
                        <?php
								$productos=mysql_query('SELECT base_productos.id_categoria as id_cate, categoria, 
														id_base_producto, descripcion, referencia_codigo, cantidad, precio_unitario
														FROM base_productos, ordenes_de_compra, categorias_productos
														WHERE base_productos.id_categoria = categorias_productos.id_categoria
														AND id_base_producto=ordenes_de_compra.id_producto
														AND folio_orden_compra='.$folio)
														or die(mysql_error());
								$deuda=0;
								while($row_productos=mysql_fetch_array($productos)){
									$descripcion=$row_productos['descripcion'];
									$referencia_codigo=$row_productos['referencia_codigo'];
									$cantidad=$row_productos['cantidad'];
									$precio_unitario=$row_productos['precio_unitario'];
									$id_base=$row_productos['id_base_producto'];
									$id_categoria=$row_productos['id_cate'];
									$categoria=$row_productos['categoria'];
									if($id_categoria==1){
										$imprime=$referencia_codigo;
									}else{
										$imprime=$descripcion;	
									}
									$deuda=$deuda+($precio_unitario*$cantidad);
						?>
						
                            	
                                	<tr>
                                        <td>
											<?php echo $categoria; ?>
                                        </td><td>
                                        	<input type="hidden" name="cate_<?php echo $id_base; ?>" value="<?php echo $id_categoria; ?>" />
                                            <input type="checkbox" name="prod_<?php echo $id_base; ?>" checked="checked" />
                                        
                                            <?php echo $imprime; ?>
                                        </td><td style="text-align:center">
                                        	<input type="text" name="cantidad_<?php echo $id_base; ?>" value="<?php echo $cantidad; ?>" 
                                            size="5" maxlength="5" style="text-align:center" />
                                        </td><td id="alright"> 
                                        	<input type="hidden" name="costo_<?php echo $id_base; ?>" value="<?php echo $precio_unitario; ?>"  />
                                            <?php echo "$".number_format($precio_unitario,2); ?>
                                        </td>
                            		</tr>
						<?php
									
								}
						?>
                        			<tr>
                                        <td colspan="4" style="text-align:center">
                                            <input type="button" name="volver" value="Volver" class="fondo_boton"
                                            onclick="window.location.href='cuentas_por_pagar.php?id_proveedor=<?php echo $id_proveedor; ?>'" />
                                            <input type="submit" name="nuevo" value="Guardar" class="fondo_boton" />
                                        </td>
                                    </tr>
                        			<!--<tr>
                                    	<td colspan="6" id="alright">
                                        	<hr />
                                        	<label class="textos">Total: </label><?php //echo "$".number_format($deuda,2); ?>
                                        </td>
                                    </tr>-->
                        <?php
							}else{
						?>
                        			<tr>
                                        <td colspan="4" style="text-align:center">
                                            <input type="button" name="volver" value="Volver" class="fondo_boton"
                                            onclick="window.location.href='cuentas_por_pagar.php?id_proveedor=<?php echo $id_proveedor; ?>'" />
                                        </td>
                                    </tr>
                        <?php
							}
						?>
                        </form>
                    </table>
                </center>
			</div><!--Fin de area contenido-->
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna2-->
</body>
</html>