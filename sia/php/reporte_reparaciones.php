<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$total_refaccion = 0;
	$total_refaccion_vales = 0;
	$contador = 0;
	$total_global_notas = 0;
	$total_global_vales = 0;
	$total_global = 0;
	$totales=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Reparaciones</title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/demos.css"/>
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ){
					var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					    instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width: 600px;">
                    Reporte de Reparaciones
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <div class="contenido_proveedor">
                    <center>
                        <form name="form" action="reporte_reparaciones.php" method="post">
                            <div class="demo">
                                <label for="from" class="textos"> Fecha Inicio: </label> &nbsp;
                                <input type="text" id="from" name="from"/> &nbsp;&nbsp;
                                <label for="to" class="textos"> Fecha Fin: </label> &nbsp;
                                <input type="text" id="to" name="to"/>
                                &nbsp;
                                 <input type="submit" name="accion" value="Buscar" title="Buscar" class="fondo_boton"/>
                            </div><!-- End demo -->
                        </form>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
                        <br />
                    </center>
                </div><!--Fin de contenido proveedor-->
                <center>
	            <?php
					// SE VALIDA SI EL USUARIO HA OPRIMIDO EL BOTON DE BUSCAR
					if( isset($_POST['accion']) && $_POST['accion'] == "Buscar"){
						// SE RECIBEN LAS VARIABLES DEL FORMULARIO
						$fecha_inicio = $_POST['from'];
						$fecha_inicio_separada = explode("/", $fecha_inicio);
						$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
						$fecha_fin = $_POST['to'];
						$fecha_fin_separada = explode("/", $fecha_fin);
						$fecha_fin_mysql = $fecha_fin_separada[2]."-".$fecha_fin_separada[1]."-".$fecha_fin_separada[0];
				?>
                    <table>
                        <tr>
                        	<td style="color:#000; font-size:22px; text-align:center;"> REPARACIONES </td>
                        </tr>
                        <tr>
                           	<td style="color:#000; font-size:16px; text-align:center;"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_fin; ?> </td>
                        </tr>
                    </table>
                    <br /> 
                    <table>
                        <tr>
                            <th style="font-size:10px;"> NOTA </th>
                            <th style="font-size:10px;"> FECHA DE ENTRADA </th>
                            <th style="font-size:10px;"> REPARACION </th>
                            <th style="font-size:10px;"> FECHA DE SALIDA </th>
                            <th style="font-size:10px;"> COSTO </th>
                            <th style="font-size:10px;"> NO REPARADOS </th>
                            <th style="font-size:10px;"> GARANTIA </th>
                            <th style="font-size:10px;"> OBSERVACIONES </th>                                                
                        </tr>
                		<?php
							// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
							$query_tecnicos = mysql_query("SELECT DISTINCT(id_empleado), alias
												FROM reparaciones, reparaciones_laboratorio, empleados
												WHERE empleados.id_empleado = reparaciones_laboratorio.reparado_por
												AND reparaciones.folio_num_reparacion = reparaciones_laboratorio.folio_num_reparacion														
												AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_fin_mysql."'")
												or die('Consulta Tecnicos');
							while($row_query_tecnicos=mysql_fetch_array($query_tecnicos)){
								$id_empleado=$row_query_tecnicos['id_empleado'];
								$alias=$row_query_tecnicos['alias'];								
						?>
                        <tr>
                            <td colspan="8" style="font-size:14px; background-color:#003; color:#FFF"><?php echo ucwords($alias); ?></td>
                        </tr>	
						<?php
								// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
								$query_reparaciones_notas = "SELECT reparaciones.folio_num_reparacion, fecha_entrada, descripcion_problema, 
															fecha_salida, reparaciones_laboratorio.costo, observaciones, estado_reparacion, 
															no_reparado, aplica_garantia
															FROM reparaciones,reparaciones_laboratorio, estatus_reparaciones
															WHERE reparaciones.folio_num_reparacion = reparaciones_laboratorio.folio_num_reparacion
															AND reparaciones.id_estatus_reparaciones=estatus_reparaciones.id_estatus_reparacion															
															AND reparado_por=".$id_empleado."
															AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_fin_mysql."'
															ORDER BY fecha_salida DESC";
										
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$resultado_reparaciones_notas = mysql_query($query_reparaciones_notas) or die(mysql_error());
								$costos=0;
								$registros=0;
								// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
								while( $row_reparacion_nota = mysql_fetch_array($resultado_reparaciones_notas) ){
										$contador++;
										$registros++;
									$fecha_entrada = $row_reparacion_nota['fecha_entrada'];
									$reparado = $row_reparacion_nota['no_reparado'];
									if($reparado!=""){
										$reparado="No Reparado";
									}
									$garantia = $row_reparacion_nota['aplica_garantia'];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$folio_reparacion = $row_reparacion_nota['folio_num_reparacion'];										
									$descripcion_problema = $row_reparacion_nota['descripcion_problema'];
									if($descripcion_problema==""){
										$descripcion_problema="--";
									}
									$fecha_salida = $row_reparacion_nota['fecha_salida'];										
									$fecha_salida_separada = explode("-", $fecha_salida);
									$fecha_salida_normal = $fecha_salida_separada[2]."/".$fecha_salida_separada[1]."/".$fecha_salida_separada[0];										
									if($garantia==""){
										$costo = $row_reparacion_nota['costo'];
									}else{
										$costo=0.0;
										$garantia="Garantía";
									}
									$observaciones = $row_reparacion_nota['observaciones']; 
									$costos+=$costo;
									$totales+=$costo;
										
						?>
                        <tr>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $folio_reparacion; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $fecha_entrada_normal; ?>  </td>
                            <td style="font-size:9px;"> <?php  echo $descripcion_problema; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $fecha_salida_normal; ?> </td>                                               
                            <td style="font-size:9px; text-align:right;"><?php echo "$".number_format($costo,2); ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo ucwords($reparado); ?>  </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo ucwords($garantia); ?>  </td>
                            <td style="font-size:9px;"> <?php  echo $observaciones; ?> </td>                                                
                        </tr>
                        <tr>
                            <td colspan="8"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                        </tr>
						<?php
								}
                        ?>
                        <tr>
                            <td colspan="8" style="font-size:12px; font-weight:lighter"><?php echo $registros." registros de detalle"; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-left:50px; font-size:9px; padding-bottom:50px;">Suma</td>
                            <td style="font-size:9px; text-align:right;" valign="top"><?php echo "$".number_format($costos,2); ?></td>
                        </tr>
						<?php
							}
                        ?>
                        
<!--Aqui iba el codigo de reparados sin empleado-->                        
                        <tr>                                        		
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:center; color:#ac1f1f;"> Total: <br /> <?php echo "$".number_format($totales,2); ?> </td>
                        </tr>
					</table>
					<br /><br />
					<?php
						// SE VALIDA SI EL CONTADOR ES MAYOR A CERO
						if( $contador > 0){
					?>
                        <div id="opciones" style="float:right; margin-right:70px;">
                            <form name="form_reporte" style="float:right; margin-left:20px;" method="post" action="reporte_reparaciones_excel.php">
                                <div id="spanreloj"></div>
                                <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                <input type="hidden" name="fecha_final" value="<?php echo $fecha_fin_mysql; ?>" />
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <!--<a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_reporte_reparaciones_salen.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_fin_mysql; ?>','Reporte de Refacciones que Salen','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1250, height=700');"> <img src="../img/print icon.png"/> </a>-->
                        </div> <!-- FIN DIV OPCIONES -->
					<?php
						}
                    }
            		?>
                <br /><br />
                </center>
                <br />
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>