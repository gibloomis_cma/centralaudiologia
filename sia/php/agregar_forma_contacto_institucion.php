<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Contactos de Proveedor</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css" />
<script type="text/javascript" src="../js/Validacion.js"></script>
<style type="text/css">
	table tr td{
		/*border:1px solid #F66;*/
	}
</style>
</head>
<body>
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Instituciones
            </div>
        </div><!--Fin de fondo titulo-->                            
		<?php
            // SE IMPORTA EL ARCHIVO DE LA CONEXION A LA BASE DE DATOS
            include("config.php");
            $id_institucion = $_GET["id_institucion"];           
			$id_contacto2 = $_GET["id_contacto"];
            $query_datos_institucion = mysql_query("SELECT razon_social,calle,num_exterior,
												num_interior,colonia,codigo_postal,id_ciudad,
												id_estado,rfc,descuento,costo_aparato,poliza 
												FROM instituciones                                                   
											  	WHERE id_institucion = ".$id_institucion)
												or die(mysql_error());
            $row = mysql_fetch_array($query_datos_institucion);            
			$nombre_dependencia = $row["razon_social"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$colonia = $row["colonia"];
			$codigo_postal = $row["codigo_postal"];
			$id_ciudad = $row["id_ciudad"];
			$id_estado = $row["id_estado"];
			$rfc = $row["rfc"];
			$descuento = $row["descuento"];
			$costo = $row["costo_aparato"];
			$poliza = $row["poliza"];			
        ?>
         <center>
         <!--<form name="form_contacto_institucion" id="form_contacto_institucion" 
         action="procesa_agregar_contacto_institucion.php" method="post" onsubmit="return validarContacto()" >
        	 <!--<input  name="id_institucion" value="<?php //echo $id_institucion; ?>" type="hidden"/>-->
         <div class="area_contenido1">                          
            <br />
                <div class="contenido_proveedor">                                   
                    <table width="624px">
                    	<tr>
                        	<th colspan="4">Datos Generales</th>
                        </tr>
                    	<tr>
                        	<td style="text-align:right">               
			                    <label class="textos">Nombre de la Dependencia: </label>
                            </td><td style="text-align:left" colspan="3">
            			        <?php echo $nombre_dependencia; ?>
                            </td>
                        </tr><tr>                        
                            <td style="text-align:right">                           
                				<label class="textos">Dirección: </label>
                            </td>
							<td style="text-align:left" colspan="3">
							<?php 						
                                if($num_interior == ""){
                                    $interior = "";
                                }else{
                                    $interior = " Int. ".$num_interior;
                                }
                                echo $calle." #".$num_exterior.$interior." Col. ".$colonia." C.P. ".$codigo_postal;
                            ?>                            
                            </td>                            
                        </tr><tr>
                        	<td style="text-align:right">
                                <label class="textos">Estado: </label>
                            </td><td style="text-align:left">
							<?php
                                $consulta_estados = mysql_query("SELECT estado FROM estados 
                                                                WHERE id_estado=".$id_estado);
                                $row3 = mysql_fetch_array($consulta_estados);
                                $estado_consultado = $row3["estado"];	
                                echo utf8_encode($estado_consultado); 
                            ?>
                            </td><td style="text-align:right">
                                <label class="textos">Ciudad: </label>                         	
							</td><td style="text-align:left">
							<?php
                                $consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades
                                                                WHERE id_ciudad=".$id_ciudad);
                                $row4 = mysql_fetch_array($consulta_ciudad);
                                $ciudad_consultado = ucwords(strtolower($row4["ciudad"]));	
                                echo utf8_encode($ciudad_consultado); 
                            ?>                                                       
                            </td>
                        </tr><tr>
                        	<td style="text-align:right">
                            	<label class="textos">RFC: </label>
                            </td><td style="text-align:left">
								<?php echo strtoupper($rfc); ?>
                            </td><td style="text-align:right">                                
                                <label class="textos">Descuento: </label>
                            </td><td style="text-align:left">                                
                                <?php echo $descuento."%"; ?>                                
                    		</td>
                        </tr><tr>
                        	<td style="text-align:right">
                            	<label class="textos">Costo por aparato: </label>                                
                            </td><td style="text-align:left" colspan="3">
								<?php echo $costo; ?>                           		
                            </td>
                        </tr><tr>                 
                        	<td style="text-align:right">
                            	<label class="textos">Poliza: </label>
                            </td>
							<td style="text-align:justify">
                            	<?php echo $poliza; ?>
                            </td>
                        </tr>
                     </table>
                    <br />    
                </div>                              
            	<table>      
					<tr>
                    	<th colspan="2">Contacto(s)</th>
                	</tr>
                <!--onsubmit="return validarFormaContactoProveedor()" -->                                      
                <?php
                    $contactos = mysql_query('SELECT id_contacto_institucion, nombre, 
                                            paterno, materno, puesto, departamento
                                            FROM contactos_instituciones
                                            WHERE id_institucion ='.$id_institucion)
                                            or die(mysql_error());
                    $n_contactos=0;
                    while($row_contactos = mysql_fetch_array($contactos)){
                        $id_contacto = $row_contactos['id_contacto_institucion'];
                        $nombre = $row_contactos['nombre'];
                        $materno = $row_contactos['materno'];
                        $paterno = $row_contactos['paterno'];
                        $puesto = $row_contactos['puesto'];
                        $departamento = $row_contactos['departamento'];
                        $n_contactos++;
                        if($id_contacto2 != $id_contacto){
                ?>      
                	<tr>          
                        <td style="border-bottom:1px solid #666">
                        	<table style="width:300px !important ">
                            	<tr>
                                	<td colspan="2" style="text-align:center">
										<?php echo utf8_encode($nombre." ".$paterno." ".$materno); ?>
                            		</td>
                            	</tr><tr>
                                	<td style="text-align:right; width:80px !important;">
                                    	<label class="textos">Puesto: </label>
                                    </td><td style="text-align:left">
										<?php echo $puesto; ?>
                                    </td>
                            	</tr><tr>
									<td style="text-align:right;">
                                    	<label class="textos">Departamento: </label>
                                    </td><td>
										<?php echo $departamento; ?>
                                    </td>
                        		</tr>
                        	</table>
                        </td><td style=" border-bottom:1px solid #666">                   
                            <div>                        
                <?php 
                            $formas_contacto = mysql_query('SELECT id_forma_contacto, tipo, descripcion
                                                        FROM forma_contacto_instituciones
                                                        WHERE id_contacto_institucion ='.$id_contacto)
                                                        or die(mysql_error());
                            $n_forma_contacto=0;
                            while($row_formas_contacto = mysql_fetch_array($formas_contacto)){
                                $id_forma_contacto = $row_formas_contacto['id_forma_contacto'];
                                $tipo = $row_formas_contacto['tipo'];
                                $descripcion = $row_formas_contacto['descripcion'];
                                $n_forma_contacto++;
                ?>
                            <div id="cleft">
                                <label class="textos"><?php echo $tipo; ?>:</label>
                            </div>
                            <div id="cright">
                                <?php echo $descripcion; ?>
                            </div>
                <?php					
                            }//END WHILE CONSULTA FORMAS CONTACTO			
                ?>      	
                            </div>
                        </td>            
                    </tr>                
				<?php
                        }
                    }//END WHILE($row_contactos = mysql_fetch_array($contactos))
                ?>
                    <tr>                    
                <?php
                    $contactos = mysql_query('SELECT id_contacto_institucion, nombre, 
                                            paterno, materno, puesto, departamento
                                            FROM contactos_instituciones
                                            WHERE id_contacto_institucion ='.$id_contacto2)
                                            or die(mysql_error());
                    $n_contactos=0;
                    $row_contactos = mysql_fetch_array($contactos);
                    $id_contacto = $row_contactos['id_contacto_institucion'];
                    $nombre = $row_contactos['nombre'];
                    $materno = $row_contactos['materno'];
                    $paterno = $row_contactos['paterno'];
					$puesto = $row_contactos['puesto'];
                    $departamento = $row_contactos['departamento'];
                ?>
                        <td>
                        	<table style="width:300px !important ">
                            	<tr>
                                	<td colspan="2" style="text-align:center">
										<?php echo utf8_encode($nombre." ".$paterno." ".$materno); ?>
                            		</td>
                            	</tr><tr>
                                	<td style="text-align:right; width:80px !important;">
                                    	<label class="textos">Puesto: </label>
                                    </td><td style="text-align:left">
										<?php echo $puesto; ?>
                                    </td>
                            	</tr><tr>
									<td style="text-align:right">
                                    	<label class="textos">Departamento: </label>
                                    </td><td>
										<?php echo $departamento; ?>
                                    </td>
                        		</tr>
                        	</table>
                        </td><td>
                            <div>              
                <?php 
                            $formas_contacto = mysql_query('SELECT id_forma_contacto, tipo, descripcion
                                                        FROM forma_contacto_instituciones
                                                        WHERE id_contacto_institucion ='.$id_contacto2)
                                                        or die(mysql_error());
                            $n_forma_contacto=0;
                            while($row_formas_contacto = mysql_fetch_array($formas_contacto)){
                                $id_forma_contacto = $row_formas_contacto['id_forma_contacto'];
                                $tipo = $row_formas_contacto['tipo'];
                                $descripcion = $row_formas_contacto['descripcion'];
                                $n_forma_contacto++;
                ?>
                            <div id="cleft">
                                <label class="textos"><?php echo $tipo; ?>:</label>
                            </div>
                            <div id="cright">
                                <?php echo $descripcion; ?>
                            </div>
                            <br />                            
                <?php					
                            }//END WHILE CONSULTA FORMAS CONTACTO			
                ?>      	
                            </div> 
                        </td>
                    </tr><tr>
                     <form name="form_agregar_form_contacto_institucion" 
                    id="form_agregar_forma_contacto_institucion" 
                    action="procesa_agregar_forma_contacto_institucion.php" 
                    method="post" style="border:1px solid #099" >
                        <input name="id_contacto" type="hidden" value="<?php echo $id_contacto2; ?>"/>
                        <input name="id_institucion" type="hidden" value="<?php echo $id_institucion; ?>"/>
                        <td colspan="2" style="text-align:center">
                            <label class="textos">Tipo:</label>                    
                            <select name="tipo_telefono">
                                <option value="" selected="selected">Seleccione</option>
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Correo">Correo</option>
                                <option value="Fax">Fax</option>                    
                            </select>                     
                        <label class="textos">Descripción:</label>
                        <input name="descripcion" type="text" />
                        <div id="nuevo"></div>
                        </td>
                    </tr><tr>
                        <td colspan="2" style="text-align:right">	                        
                        <?php
							if($n_forma_contacto!=0){
            			?>                
                        	<input type="button"  value="Terminar" title="" class="fondo_boton"
                            onclick="location.href='agregar_contacto_institucion.php?id_institucion=<?php echo $id_institucion; ?>'" />                
            			<?php
                			}
						?>                        				
	                        <!--<input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>-->
                            <input name="accion" type="submit" value="Guardar" class="fondo_boton" title="Guardar"/>
                        </td>
                    </form>
                    </tr>
                </table>                                                                      
                <!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
            </center>
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</body>
</html>