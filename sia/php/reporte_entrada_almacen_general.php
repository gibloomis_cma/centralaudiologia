<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Consulta de Entradas a Almacen General </title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css">
	<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
	<script src="../js/jquery-1.7.1.js"></script>
	<script src="../js/ui/jquery.ui.core.js"></script>
	<script src="../js/ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../css/themes/base/demos.css">
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == "from" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
	<script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">       
	<div id="contenido_columna2">
    	<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria" style="width:405px">
                	Reporte de Entradas
           		</div>
        	</div><!--Fin de fondo titulo-->
        	<div class="area_contenido1">
            	<div class="contenido_proveedor">
            	<br />
               		<div class="titulos"> Consulta de Entradas a Almacen General </div>
            	<br />
                	<form name="form" action="reporte_entrada_almacen_general.php" method="post"> 
                	<center>
                    	<label class="textos">Categoria:</label>
                        <select name="categoria">
                        	<option value="0">Seleccione</option>
                        	<option value="1">Refacciones</option>
                            <option value="4">Baterias</option>
                            <option value="5">Auxiliares</option>
                        </select>
                    <div class="demo">
                    	<label for="from" class="textos">De: </label>
                    	<input type="text" id="from" name="from"/>
                    	<label for="to" class="textos">A: </label>
                    	<input type="text" id="to" name="to"/>
                	</div><!-- End demo -->
                    </center>
            	<p align="center">
                	<input type="submit" name="accion" value="Aceptar" class="fondo_boton"/>
            	</p> 
                	</form>
            	</div><!--Fin de contenido proveedor-->
 				<?php
					include("config.php");
					//include("metodo_cambiar_fecha.php");
					$accion = $_POST["accion"];
					$fechaInicio = $_POST["from"];
					$fecha_inicio_separada = explode("/", $fechaInicio);
					$fechaFinal = $_POST["to"];
					$fecha_final_separada = explode("/", $fechaFinal);
					$id_categoria = $_POST["categoria"];

					if( $accion == "Aceptar" )
					{
						if( isset($fechaInicio) && $fechaInicio != "" and isset($fechaFinal) && $fechaFinal !=" " )
						{
							$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
							$fecha_final_mysql = $fecha_final_separada[2]."-".$fecha_final_separada[1]."-".$fecha_final_separada[0];
  				?>	
    				<center>
	                    <style>
							td
							{
								font-size:10px;
							}
							th
							{
								font-size:11px;
							}
						</style>
                    <table>
                    	<tr>
                    		<td colspan="6" style="text-align:center; font-size:14px;"> 
                    			<?php 
                    				if ( $id_categoria == 1 )
                    				{
                    				?>
                    					<label> <?php echo "REFACCIONES QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    				elseif( $id_categoria == 4 )
                    				{
                    				?>
                    					<label> <?php echo "BATERIAS QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    				else
                    				{
                    				?>
                    					<label> <?php echo "AUXILIARES QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    			?> 
                    		</td>
                    	</tr>
                    	<tr>
                    		<td colspan="6" style="text-align:center; font-size:14px;"> <label> DEL: <?php echo $fechaInicio; ?> &nbsp;&nbsp; AL: <?php echo $fechaFinal; ?> </label> </td>
                    	</tr>
                        <tr>
                        	<th style="font-size:10px;">Fecha de Entrada</th>
                            <th style="font-size:10px;">C&oacute;digo</th>
                            <th style="font-size:10px;">Concepto</th>
                            <th style="font-size:10px;">Cantidad</th>
                            <th style="font-size:10px;"> Observaciones </th>
                            <th style="font-size:10px;"> Costo Total </th>
                        </tr>
   						<?php 
							$buscar_subcategorias = mysql_query("SELECT id_subcategoria FROM subcategorias_productos 
													 			 WHERE id_categoria = ".$id_categoria) or die(mysql_error());
	
							$n_entradas = 0;
							$costo_total = 0;
							$total_entro = 0;
							$total_piezas = 0;
							while($row_subcategorias = mysql_fetch_array($buscar_subcategorias))
							{
								$id_subcategorias = $row_subcategorias["id_subcategoria"];	
								$consulta_folios_venta = mysql_query("SELECT entrada_almacen_general.id_registro AS id_registro, entrada_almacen_general.num_serie_cantidad AS num_serie_cantidad, entrada_almacen_general.fecha_entrada AS fecha_entrada, entrada_almacen_general.hora AS hora, entrada_almacen_general.id_articulo AS id_articulo, entrada_almacen_general.id_subcategoria AS id_subcategoria, observaciones, referencia_codigo,precio_compra
														  			  FROM entrada_almacen_general, inventarios,base_productos
														  			  WHERE entrada_almacen_general.fecha_entrada BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
														  			  AND entrada_almacen_general.id_almacen = '1'
														  			  AND entrada_almacen_general.id_subcategoria = '$id_subcategorias'
														  			  AND entrada_almacen_general.id_subcategoria = inventarios.id_subcategoria
														  			  AND entrada_almacen_general.id_almacen = inventarios.id_almacen
														  			  AND entrada_almacen_general.fecha_entrada = inventarios.fecha_entrada
														  			  AND entrada_almacen_general.id_articulo = inventarios.id_articulo
														  			  AND entrada_almacen_general.hora = inventarios.hora
														  			  AND entrada_almacen_general.id_articulo = id_base_producto
														  			  AND inventarios.id_articulo = id_base_producto
														  			  ORDER BY id_registro DESC") or die(mysql_error());

								while($row_folios_venta = mysql_fetch_array($consulta_folios_venta))
								{
									$num_serie_cantidad = $row_folios_venta["num_serie_cantidad"];
									$fecha_entrada = $row_folios_venta["fecha_entrada"];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$hora = $row_folios_venta["hora"];
									$id_registro = $row_folios_venta["id_registro"];
									$id_articulo = $row_folios_venta["id_articulo"];
									$id_subcategoria = $row_folios_venta["id_subcategoria"];
									$observaciones = $row_folios_venta['observaciones'];
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$precio_compra = $row_folios_venta['precio_compra'];
									$codigo = $row_folios_venta['referencia_codigo'];
									$n_entradas++;
									$costo_total = $num_serie_cantidad * $precio_compra;
									$total_entro = $total_entro + $costo_total;
									$total_piezas = $total_piezas + $num_serie_cantidad;
									$consulta_base_producto = mysql_query("SELECT descripcion 
																		   FROM base_productos 
																		   WHERE id_base_producto=".$id_articulo) or die(mysql_error());
									$row_base_producto = mysql_fetch_array($consulta_base_producto);
									$descripcion_base_producto = $row_base_producto["descripcion"];
					
								?>
		    						<tr>
		                                <td style="font-size:10px; text-align:center;"><?php echo $fecha_entrada_normal." ".$hora; ?></td>
		                                <td style="font-size:10px; text-align:center;"> <?php echo $codigo; ?> </td>
		                                <td style="font-size:10px;"><?php echo $descripcion_base_producto; ?></td>
		                                <td style="font-size:10px; text-align:center;"><?php echo $num_serie_cantidad; ?></td>
		                                <td style="font-size:10px;"><?php echo $observaciones; ?></td>
		                                <td style="font-size:10px;"> <?php echo "$".number_format($costo_total,2); ?> </td>
		                            </tr>
		                            <tr>
		                            	<td colspan="6"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
		                            </tr>
  								<?php
							}
						}					
				if($n_entradas==0)
				{
				?>
                    <tr>
                        <td style="text-align:center;" colspan="4">
                            <label class="textos">"No hay Entradas registradas"</label>
                        </td>
                    </tr>        
				<?php		
				}
				else
				{
				?>
					<tr>
		                <td>  </td>
		                <td>  </td>
		                <td style="text-align:right;"> <label class="textos" style="font-size:11px; color:#b60b0b;"> Total Piezas: </label> </td>
		                <td style="text-align:center;"> <label class="textos" style="font-size:11px; color:#b60b0b;"> <?php echo $total_piezas; ?> </label> </td>
		                <td style="text-align:right;"> <label class="textos" style="font-size:11px; color:#b60b0b;"> Total que Entro: </label> </td>
		                <td style="text-align:center;"> <label class="textos" style="font-size:11px; color:#b60b0b;"> <?php echo "$".number_format($total_entro,2); ?> </label> </td>
		            </tr>
					</table>
                        </center>
                    	<br />
					<center>
						<div id="opciones" style="margin-bottom:25px;">
                            <form name="form_entradas" style="float:right; margin-left:15px; margin-right:65px;" method="post" action="lista_entradas_excel.php">
                                <div id="spanreloj"></div>
                                <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                <input type="hidden" name="fecha_final" value="<?php echo $fecha_final_mysql; ?>" />
                                <input type="hidden" name="id_categoria" value="<?php echo $id_categoria; ?>" />
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <a href="#" title="Imprimir Reporte" style="float:right;" onclick="window.open('imprimir_lista_entradas.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_final_mysql; ?>&id_categoria=<?php echo $id_categoria; ?>','Lista de Entradas al Almacen General','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1050, height=700');"> <img src="../img/print icon.png"/> </a>
                        </div> <!-- FIN DIV OPCIONES -->
					</center>
					<br/>
				<?php
				}
				?>
                        
    <?php
			}
		}
	?>
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>