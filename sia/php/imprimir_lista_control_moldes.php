<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_moldes = "SELECT folio_num_molde, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_cliente, fecha_entrada, id_estatus_moldes, costo, adaptacion, reposicion, pago_parcial
                     FROM moldes
                     WHERE id_estatus_moldes <> 7
                     ORDER BY fecha_entrada DESC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_query_moldes = mysql_query($query_moldes) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Control de Moldes </title>
	<!--<link type="text/css" rel="stylesheet" href="../css/style3.css" />-->
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
    <script type="text/javascript" language="javascript">
        function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  d + '/' + m + '/' + y + ' --- ' + h + ':' + min + ':' + sec;
            window.print();
        }
    </script>
</head>
<body onload="hora()">
    <center>
        <table>
            <tr>
                <td colspan="4" id="hora" style="text-align:center; font-size:16px; font-weight:bold;"> LISTA DE CONTROL DE MOLDES </td>
            </tr>
        </table>
    </center>
	<table style="width:1000px;" border="1">
        <tr>
            <th style="font-size:14px;"> N° Molde </th>
            <th style="font-size:14px;"> Nombre Completo </th>
            <th style="font-size:14px;"> Costo </th>
            <th style="font-size:14px;"> Estatus </th>
        </tr>
        <?php
            // SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            while ( $row_molde = mysql_fetch_array($resultado_query_moldes) )
            {
                $folio_num_molde = $row_molde["folio_num_molde"];
                $nombre_cliente = $row_molde['nombre_cliente'];
                $estado_molde = $row_molde["id_estatus_moldes"];
                $fecha_entrada = $row_molde["fecha_entrada"];
                $costo_molde = $row_molde['costo'];
                $pago_parcial = $row_molde['pago_parcial'];
                $adaptacion = $row_molde['adaptacion'];
                $reposicion = $row_molde['reposicion'];
            
                $consulta_estatus_nota_molde = mysql_query("SELECT * FROM estatus_moldes
                                                            WHERE id_estatus_moldes = ".$estado_molde) or die(mysql_error());   
                            
                $row_estatus_nota_molde = mysql_fetch_array($consulta_estatus_nota_molde);
                $estatus_molde = $row_estatus_nota_molde["estado_molde"];                                 
        ?>
                <tr>
                    <td style="font-size:12px; text-align:center;"> <?php echo  $folio_num_molde; ?> </td>
                    <td style="font-size:12px; text-align:center;"> <?php echo $nombre_cliente; ?> </td>
                    <td style="font-size:12px; text-align:center;"> 
                        <?php 
                            if($adaptacion == "si" && $reposicion == "")
                            { 
                                echo "$ 0.00"; 
                            }
                            elseif( $reposicion == "si" && $adaptacion == "" )
                            { 
                                echo "$ 0.00"; 
                            }
                            else
                            { 
                                echo "$ ".number_format($costo_molde - $pago_parcial,2); 
                            } 
                        ?> 
                    </td>
                    <td style="font-size:12px; text-align:center;"> <?php echo $estatus_molde; ?> </td>
                </tr>
        <?php
            }
        ?>
    </table>
</body>
</html>