<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label><input type="text" name="descripcion[]" /><label class="textos"> Titular: </label><input name="titular[]" type="text" /><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Pacientes
            </div>
         </div><!--Fin de fondo titulo-->
        <div class="area_contenido1">
		<?php
            include("config.php");
            $id_cliente = $_GET["id_cliente"];
            $consulta_datos_cliente = mysql_query("SELECT * FROM ficha_identificacion 
                                                WHERE id_cliente=".$id_cliente) 
												or die(mysql_error());
            $row = mysql_fetch_array($consulta_datos_cliente);
            $nombre = $row["nombre"];
            $paterno = $row["paterno"];
            $materno = $row["materno"];
            $fecha_nacimiento = $row["fecha_nacimiento"];
            $genero = $row["genero"];
            $calle = $row["calle"];
            $num_exterior = $row["num_exterior"];
            $num_interior = $row["num_interior"];
            $codigo_postal = $row["codigo_postal"];
            $colonia = $row["colonia"];
            $id_estado = $row["id_estado"];
            $id_ciudad = $row["id_ciudad"];
            $edad = $row["edad"];
            $ocupacion = $row["ocupacion"];
            $rfc = $row["rfc"];
        ?>
        <form action="proceso_modificar_paciente.php" method="post" name="form_modificar_paciente" onsubmit="return validarModificarPacientes()">
        <br />	
            <div class="contenido_proveedor">
            <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                <table>
                    <tr>
                    	<th colspan="4">
                        	Modificar Ficha de Identificación
                        </th>
                    </tr><tr>
                        <td id="alright">
                        	<label class="textos">Nombre(s): </label>
                        </td><td id="alright">
                        	<input name="nombre" type="text" value="<?php echo $nombre; ?>" size="30"/>
                        </td><td id="alright">
                        	<label class="textos">Edad: </label>
                        </td><td>
                        	<input name="edad" type="text" size="3" maxlength="3" value="<?php echo $edad; ?>"/>
                        </td>
                    </tr>
                        <td id="alright">
                        	<label class="textos">Paterno: </label>
                        </td><td>
                        	<input name="paterno" type="text" value="<?php echo $paterno; ?>" size="30" />
                        </td><td id="alright">
                        	<label class="textos">Materno: </label>
                        </td><td>
                        	<input name="materno" type="text" value="<?php echo $materno; ?>" size="30" />
                        </td>
                    </tr><tr>                    
                        <td id="alright">
                        	<label class="textos">Ocupacion: </label>
                        </td><td>
                        	<input name="ocupacion" type="text" value="<?php echo $ocupacion; ?>" size="30"/>
                        </td><td id="alright">
                        	<label class="textos">Fecha de Nacimiento: </label>
                        </td><td>
                            <input name="fecha_nacimiento" type="text" size="10" maxlength="10" 
                            value="<?php echo $fecha_nacimiento; ?>" onkeyup="Validar(this,'/',patron,true)"/>
                        </td>
                    </tr><tr>
                        <td id="alright">
                        	<label class="textos">Genero:</label>
                        </td><td>
                        	<select name="genero" title="Genero" style="width:200px">
							<?php 
                                if($genero == "Femenino"){
                            ?>
                                <option value="Femenino" selected="selected">Femenino</option>
                                <option value="Masculino">Masculino</option>
							<?php 
                                }else{
                            ?>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino" selected="selected">Masculino</option>
							<?php 
                                }
                            ?>
                        	</select>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">RFC: </label>
                        </td><td>
                         	<input name="rfc" type="text" value="<?php echo $rfc; ?>" size="30"/>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td style="text-align:center" colspan="4">
                        	--Dirección--
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Calle: </label>
                        </td><td>
                        	<input name="calle" type="text" value="<?php echo $calle; ?>" size="30"/>
                        </td><td id="alright">
                        	<label class="textos">Numero: </label>
                        </td><td>
                        	<label class="textos">Ext. </label>
                        	<input name="num_exterior" type="text" size="5" maxlength="5" value="<?php echo $num_exterior; ?>"/>
                            <label class="textos">Int. </label>
                            <input name="num_interior" type="text" size="6" maxlength="6" value="<?php echo $num_interior; ?>"/>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Colonia:</label>
                        </td><td>
                        	<input name="colonia" type="text" size="30" maxlength="30" value="<?php echo $colonia; ?>"/>
                        </td><td id="alright">
                        	<label class="textos">Codigo Postal: </label>
                        </td><td>
                        	<input name="codigo_postal" type="text" size="5" maxlength="5" value="<?php echo $codigo_postal; ?>"/>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Estado: </label>
                        </td><td>
                        	<select id="id_estado" name="id_estado" title="Estados" style="width:200px;" >
							<?php
                                $consulta_estados = mysql_query("SELECT * FROM estados");
                                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                	$id_estado_consultada = $row3["id_estado"];
                                    $estado = $row3["estado"];
                                    if($id_estado<>"" && $id_estado == $id_estado_consultada){
                            ?>
                                <option value="<?php echo $id_estado; ?>" selected="selected">
                                     <?php echo utf8_encode($estado); ?> 
                                </option>
							<?php
                                	}else{
                            ?>       
                                <option value="<?php echo $id_estado_consultada; ?>"><?php echo utf8_encode($estado); 				?>
							<?php 	} 
                                }
                            ?>
                            </select>
                        </td><td id="alright">
                        	<label class="textos">Ciudad: </label>
                        </td><td>
                        	<select id="id_municipio" name="id_municipio" title="Municipios" style="width:200px" >
                            	<option value="0">Agregar Ciudad</option>
							<?php
                                $consulta_ciudad = mysql_query("SELECT ciudad, id_ciudad FROM ciudades WHERE id_estado='$id_estado'");
                                while( $row4 = mysql_fetch_array($consulta_ciudad)){ 
                                    $id_ciudad_consultada = $row4["id_ciudad"];
                                    $ciudad = ucwords(strtolower($row4["ciudad"]));
                                    if($id_ciudad<>"" && $id_ciudad == $id_ciudad_consultada){
                            ?>
                                <option value="<?php echo $id_ciudad; ?>" selected="selected">
                                     <?php echo utf8_encode($ciudad); ?> 
                                </option>
							<?php
                                	}else{
                            ?>       
                                <option value="<?php echo $id_ciudad_consultada; ?>">
                                    <?php echo utf8_encode($ciudad); ?>
                                </option>
							<?php 	
									} 
                                }
                            ?>
                            </select>
                        </td>
                    </tr><tr>
                    	<td colspan="3" id="alright">
                        	<label class="textos">Otra:</label>
                        </td><td>
                        	<input name="otra" type="text"  size="30"/>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td style="text-align:center" colspan="4">
                        	--Forma(s) de Contacto--
                        </td>
                    </tr>
					<?php
                    	$frmas[0]="Telefono";
						$frmas[1]="Celular";
						$frmas[2]="Correo";
						$frmas[3]="Fax";				
						$consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
															WHERE id_cliente='$id_cliente'") or die(mysql_error());
						while($row2 = mysql_fetch_array($consulta_forma_contacto)){
							$id_contactos_cliente = $row2["id_contactos_cliente"];
							$tipo = $row2["tipo"];
							$descripcion = $row2["descripcion"];
							$titular = $row2["titular"];
                	?>
                    <tr>
                    	<td colspan="4" style="text-align:center">
                        	<input name="id_contactos_cliente[]" type="hidden" value="<?php echo $id_contactos_cliente; ?>"/>
                			<label class="textos">Tipo: </label>
							<select name="tipo_telefono[]">
							<?php 
								for($i=0;$i<4;$i++){
									if($tipo == $frmas[$i]){
            				?>
                            
                                <option value="<?php echo $frmas[$i]; ?>" selected="selected"><?php echo $frmas[$i]; ?></option>
							<?php
                                	}else{
                            ?>
                                <option value="<?php echo $frmas[$i]; ?>"><?php echo $frmas[$i]; ?></option>
							<?php
                                	}
								}
                            ?>
                            </select>
                            <label class="textos"> Descripción: </label>
                            <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>"/>
                            <label class="textos"> Titular: </label>
                            <input name="titular[]" type="text" value="<?php echo $titular; ?>"/>
                        </td>
                    </tr>
                    <?php
						}
					?>
                    <tr>
                    	<td colspan="4" style="text-align:center">
                        	<div id="nuevo"></div>
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">
                        	<input type="button" value="Agregar" class="fondo_boton" onclick="agregar();" />                        	
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">
                        	<label class="textos">Tipo de contacto: </label>
                            <select name="tipo_paciente" id="tipo_paciente" title="Tipo de Paciente" style="width:200px;">
                            <?php
								$institucion_rel=mysql_query('SELECT id_institucion 
															FROM clientes_instituciones
															WHERE id_cliente='.$id_cliente)
															or die(mysql_error());
								$row_institucion_re=mysql_fetch_array($institucion_rel);
								$id_institucion=$row_institucion_re['id_institucion'];
								if($id_institucion!=""){
									$var1=' selected="selected"';
									$var2="";
								}else{
									$var1="";
									$var2=' selected="selected"';
								}
							?>
                                <option value="Directo">Directo</option>
                                <option value="Institucion" <?php echo $var1; ?>>Institucion</option>
                            </select>
                         	<select name="inst" id="inst" title="Institución" style="width:200px;">
                            	<?php
									if($var1==""){									
								?>
                                <option value="0" selected="selected">Sin Asignar Institución</option>
                                <?php
									}else{
										$instituciones=mysql_query('SELECT id_institucion, institucion
																	FROM instituciones')
																	or die(mysql_error());
										while($row_instituciones=mysql_fetch_array($instituciones)){
											$id_institucion2=$row_instituciones['id_institucion'];
											$institucion=$row_instituciones['institucion'];
											if($id_institucion==$id_institucion2){
												$sel= ' selected="selected"';
											}else{
												$sel="";
											}
								?>
                                <option value="<?php echo $id_institucion2 ?>" <?php echo $sel; ?>><?php echo $institucion; ?></option>
                                <?php
										}
									}
								?>
                            </select>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td id="alright" colspan="4">
                        	<input name="volver" type="button" value="Volver" class="fondo_boton"
                            onclick="window.location.href='detalles_paciente.php?id_cliente=<?php echo $id_cliente; ?>'" />
                        	<input name="accion" type="submit" value="Modificar" class="fondo_boton"/>
                        </td>
                    </tr>
                </table>
        	</form>      				
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>