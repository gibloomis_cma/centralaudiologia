<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consultas</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
<script src="../js/jquery-1.7.1.js"></script>
<script src="../js/ui/jquery.ui.core.js"></script>
<script src="../js/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../css/themes/base/demos.css">
<script>
	$(function() {
		var dates = $( "#from, #to" ).datepicker({
			defaultDate: "",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
</script>
</head>
<body>
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Ventas
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <div class="titulos">Pago de Consultas</div>
                <div class="contenido_proveedor">
                <center>
                    <form name="form" action="pago_consultas.php" method="post">
                    <div class="demo">
                        <label class="textos">Empleado: </label>
                        <select name="empleado">
                            <option value="0">Seleccione</option>
   	<?php
				include("config.php");
				$consulta_doctores = mysql_query("SELECT nombre, paterno, materno, ocupacion, id_empleado
														FROM empleados WHERE ocupacion 
														LIKE '%Dr%'") or die(mysql_error());
				while($row2 = mysql_fetch_array($consulta_doctores)){
					$nombre = $row2["nombre"];
					$paterno = $row2["paterno"];	
					$materno = $row2["materno"];
					$ocupacion = $row2["ocupacion"];
					$id_empleado = $row2["id_empleado"];
	?>			
							<option value="<?php echo $id_empleado; ?>">
								<?php echo $ocupacion." ".$nombre." ".$paterno." ".$materno; ?>
							</option>	
	<?php 
				}
	?>
                    	</select>
                  <br /><br />
                        <label for="from" class="textos">De: </label>
                        <input type="text" id="from" name="from"/>
                        <label for="to" class="textos"> A: </label>
                        <input type="text" id="to" name="to"/>
                    </div><!-- End demo -->
                    <p align="center">
                        <input type="submit" name="accion" value="Aceptar" class="fondo_boton"/>
                    </p>
                    </form>
                <hr />
                <br />
                </center>
                </div><!--Fin de contenido proveedor-->
	<?php
				include("metodo_cambiar_fecha.php");
				$accion = $_POST["accion"];
				if($accion == "Aceptar"){
					$fechaInicio = $_POST["from"];
					$fechaFinal = $_POST["to"];
					$doctor = $_POST["empleado"];	
					$fechaInicio_mysql = cambiaf_a_mysql($fechaInicio);
					$fechaFinal_mysql = cambiaf_a_mysql($fechaFinal);
					
					if($doctor == "0"){	//Muestra todas las consultas en No pagadas en general					
	?> 
                        <div class="contenido_proveedor">
                        <form name="forma2" method="post" action="guardar_pago_consultas.php">
                        <label class="textos" style="margin-left:225px; font-size:15px;">Del:  <?php echo $fechaInicio; ?>
                    &nbsp;&nbsp;&nbsp;
                        Al: <?php echo $fechaFinal; ?></label>
                    <br /><br />      
  	<?php								
						$contador_pago_consultas = mysql_query("SELECT COUNT(id_registro)                         
																		FROM pago_consultas
																		WHERE fecha BETWEEN '".$fechaInicio_mysql."'
																		AND '".$fechaFinal_mysql."' AND 
																		estado = 'No pagado' 
																		GROUP BY id_subcategoria")
																		or die(mysql_error());
						$row_contador=mysql_fetch_array($contador_pago_consultas);
						$contador_consultas = $row_contador["COUNT(id_registro)"];
						
						if($contador_consultas == 0){// Si contador es igual a cero, quiere decir que no hay consultas para 
														// pagar en ese rango de fechas							
	?>
                            <table align="center">
                                <tr>
                                    <th width="40">Registro</th>
                                    <th width="110">Fecha</th>
                                    <th width="40">Cantidad</th>
                                    <th width="110">Pago</th>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align:center;">
                                        <label class="textos">No hay consultas para pagar en ese rango de Fechas</label>
                                    </td>
                                </tr>
                            </table>
   	<?php
								
						}else{							// Si hay consultas en ese rango de fechas las mostrara
							$consulta_pagos_consultas = mysql_query("SELECT * FROM pago_consultas
																			WHERE fecha BETWEEN '".$fechaInicio_mysql."'
																			AND '".$fechaFinal_mysql."' AND 
																			estado = 'No pagado' 
																			GROUP BY id_subcategoria")
																			or die(mysql_error());										
							while($row = mysql_fetch_array($consulta_pagos_consultas)){
								$id_subcategoria = $row["id_subcategoria"];
								$id_registro = $row["id_registro"];									
								$consulta_subcategoria = mysql_query("SELECT subcategoria, id_subcategoria 
																			FROM subcategorias_productos
																			WHERE id_subcategoria=".$id_subcategoria) 
																			or die(mysql_error());
								$row2 = mysql_fetch_array($consulta_subcategoria);
								$subcategoria[] = $row2["subcategoria"];
								$id_subcategoria_consultada[] = $row2["id_subcategoria"];
							}
							?>
                            <?php
							for($i=0; $i < count($subcategoria); $i++){	
								$cantidadTotal = 0;
								$pagoTotal = 0; 
	?>			                        		
                                
                                <table align="center">
                                    <tr>
                                        <th colspan="4"><?php echo $subcategoria[$i]; ?></th>
                                    </tr>
                                    <tr>
                                        <th width="40">Registro</th>
                                        <th style="width:110px;">Fecha</th>
                                        <th style="width:40px;">Cantidad</th>
                                        <th style="width:110px;">Pago</th>
                                    </tr>	
	<?php							
								$consulta_pagos_consultas2 = mysql_query("SELECT * 
																				FROM pago_consultas 
																				WHERE id_subcategoria=
																				'".$id_subcategoria_consultada[$i]."'
																				AND estado='No pagado' AND 
																				fecha BETWEEN '".$fechaInicio_mysql."'
																				 AND '".$fechaFinal_mysql."'")
																				 or die(mysql_error());
								while($row3 = mysql_fetch_array($consulta_pagos_consultas2)){
									$fecha = $row3["fecha"];
									$cantidad = $row3["cantidad"];
									$pago = $row3["pago"];
									$estado = $row3["estado"];
									$id_registro = $row3["id_registro"];								
									$fecha_normal = cambiaf_a_normal($fecha);								
									$totalpago= ($pago * $cantidad);
									$cantidadTotal += $cantidad;
									$pagoTotal += $totalpago; 
	?>
                                <input name="registro[]" type="hidden" value="<?php echo $id_registro; ?>" />
                                    <tr>
                                        <td><?php echo $id_registro; ?></td>
                                        <td><?php echo $fecha_normal; ?></td>
                                        <td><?php echo $cantidad; ?></td>
                                        <td>$<?php echo number_format($totalpago,2); ?></td>
                                        <td width="70"><?php echo $estado; ?></td>
                                    </tr>
	<?php
								}
	?>
                                    <tr>
                                    	<td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right;">Total</td>
                                        <td><?php echo $cantidadTotal; ?></td>
                                        <td>$<?php echo number_format($pagoTotal,2); ?></td>
                                        <td></td>
                                    </tr>
                                </table>
							<br />
	<?php
							}	
	?>
                        <p align="right">
                            <input name="accion" type="submit" value="Guardar e Imprimir" class="fondo_boton" />
                        </p>
                        </form>
                        </div>
  	<?php
						}
					}else{ 		//Si consulta un determinado Doctor
	?>
                         <div class="contenido_proveedor">
                         <form name="forma2" method="post" action="guardar_pago_consultas.php">
                            <label class="textos" style="margin-left:225px; font-size:15px;">Del:  <?php echo $fechaInicio; ?>
                            &nbsp;&nbsp;&nbsp;	Al: <?php echo $fechaFinal; ?></label>
                        <br /><br /> 						
	<?php
						$consulta_subcategorias=mysql_query("SELECT COUNT(id_subcategoria) FROM consultas
																WHERE id_empleado='".$doctor."'");
						$row_subcategoria2 = mysql_fetch_array($consulta_subcategorias);
						$id_subcategoria_doctor = $row_subcategoria2["COUNT(id_subcategoria)"];
							
						if($id_subcategoria_doctor == 0){	// Si contador es igual a cero, quiere decir que no hay 
															//consultas para pagar en ese rango de fechas de determinado doctor	
							
							$consulta_doctores = mysql_query("SELECT nombre, paterno, materno, ocupacion, genero
																FROM empleados WHERE id_empleado=".$doctor) 
																or die(mysql_error());
							$row2 = mysql_fetch_array($consulta_doctores);
							$nombre = $row2["nombre"];
							$paterno = $row2["paterno"];	
							$materno = $row2["materno"];
							$ocupacion = $row2["ocupacion"];
							$genero = $row2["genero"];
	?>
                            <center>
	<?php					if($genero == "Masculino"){
	
	?>
                                <label class="textos">No esta registrado el 
                                    <?php echo $ocupacion." ".$nombre." ".$paterno." ".$materno; ?> 
                                    para dar consultas
                                </label>
   	<?php
							}else{
	?>
                                <label class="textos">No esta registrada La 
                                    <?php echo $ocupacion." ".$nombre." ".$paterno." ".$materno; ?> 
                                    para dar consultas
                            	</label>
   <?php
							}
   ?>
                            </center>
    <?php
						}else{
							$consulta_subcategorias2=mysql_query("SELECT id_subcategoria FROM consultas
																WHERE id_empleado='".$doctor."'");
							$row_subcategoria3 = mysql_fetch_array($consulta_subcategorias2);
							$id_subcategoria_doctor2 = $row_subcategoria3["id_subcategoria"];
							$contador_pago_consultas = mysql_query("SELECT COUNT(id_registro) FROM pago_consultas
																			WHERE fecha BETWEEN '".$fechaInicio_mysql."'
																			AND '".$fechaFinal_mysql."' AND 
																			estado = 'No pagado' AND 
																			id_subcategoria='".$id_subcategoria_doctor2."'
																			GROUP BY id_subcategoria")
																			or die(mysql_error());
							$row_contador=mysql_fetch_array($contador_pago_consultas);
							$contador_consultas = $row_contador["COUNT(id_registro)"];
							$consulta_subcategorias=mysql_query("SELECT id_subcategoria FROM consultas
																WHERE id_empleado='".$doctor."'");
							$row_subcategoria2 = mysql_fetch_array($consulta_subcategorias);
							$id_subcategoria_doctor = $row_subcategoria2["id_subcategoria"];
							
							$consulta_descripcion_subcategoria = mysql_query("SELECT subcategoria, id_subcategoria 
																		FROM subcategorias_productos
																		WHERE id_subcategoria=".$id_subcategoria_doctor) 
																		or die(mysql_error());
							$row2 = mysql_fetch_array($consulta_descripcion_subcategoria);
							$subcategoria = $row2["subcategoria"];
							$id_subcategoria_consultada = $row2["id_subcategoria"];
							
							if($contador_consultas == 0){
							// Si contador es igual a cero, quiere decir que no hay consultas para pagar en ese rango de fechas							
	?>
                                <table align="center">
                                    <tr>
                                        <th colspan="4"><?php echo $subcategoria; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Registro</th>
                                        <th width="110px">Fecha</th>
                                        <th width="40px">Cantidad</th>
                                        <th width="110px">Pago</th>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align:center;">
                                            <label class="textos">No hay consultas para pagar en ese rango de Fechas</label>
                                        </td>
                                    </tr>
                                </table>
   	<?php
							}else{
										// Si hay consultas en ese rango de fechas de determinado doctor								
								$consulta_subcategorias=mysql_query("SELECT id_subcategoria FROM consultas
																WHERE id_empleado='".$doctor."'");
								$row_subcategoria2 = mysql_fetch_array($consulta_subcategorias);
								$id_subcategoria_doctor = $row_subcategoria2["id_subcategoria"];
								$consulta_descripcion_subcategoria = mysql_query("SELECT subcategoria, id_subcategoria 
																		FROM subcategorias_productos
																		WHERE id_subcategoria=".$id_subcategoria_doctor) 
																		or die(mysql_error());
								$row2 = mysql_fetch_array($consulta_descripcion_subcategoria);
								$subcategoria = $row2["subcategoria"];
								$id_subcategoria_consultada = $row2["id_subcategoria"];
	?>
     
                                <table align="center">
                                    <tr>
                                        <th colspan="4"><?php echo $subcategoria; ?></th>
                                    </tr>
                                    <tr>
                                        <th width="40">Registro</th>
                                        <th style="width:110px;">Fecha</th>
                                        <th style="width:40px;">Cantidad</th>
                                        <th style="width:110px;">Pago</th>
                                    </tr>	
    <?php
								$consulta_pagos_consultas = mysql_query("SELECT * FROM pago_consultas
																			WHERE id_subcategoria='".$id_subcategoria_doctor."'
																			 AND estado='No pagado' AND 
																			fecha BETWEEN '".$fechaInicio_mysql."'
																			 AND '".$fechaFinal_mysql."' ORDER by fecha")
																			or die(mysql_error());
								while($row_subcategoria = mysql_fetch_array($consulta_pagos_consultas)){
									$fecha = $row_subcategoria["fecha"];
									$cantidad = $row_subcategoria["cantidad"];
									$pago = $row_subcategoria["pago"];
									$estado = $row_subcategoria["estado"];
									$id_registro = $row_subcategoria["id_registro"];								
									$fecha_normal = cambiaf_a_normal($fecha);
									$totalpago= ($pago * $cantidad);								
									$cantidadTotal += $cantidad;
									$pagoTotal += $totalpago; 
	?>
                                    <input name="registro[]" type="hidden" value="<?php echo $id_registro; ?>" />
                                    <tr>
                                        <td><?php echo $id_registro; ?></td>
                                        <td><?php echo $fecha_normal; ?></td>
                                        <td><?php echo $cantidad; ?></td>
                                        <td>$<?php echo number_format($totalpago,2); ?></td>
                                        <td width="70"><?php echo $estado; ?></td>
                                    </tr>
	<?php
								}
	?>
									<tr>
									<td>&nbsp;</td>
									</tr>
									<tr>
										<td></td>
										<td style="text-align:right;">Total</td>
										<td><?php echo $cantidadTotal; ?></td>
										<td>$<?php echo number_format($pagoTotal,2); ?></td>
										<td></td>
									</tr>
								</table>
                            <p align="right">
								<input name="accion" type="submit" value="Guardar e Imprimir" class="fondo_boton" />
							</p> 
    <?php
							}
						}
	?>
                       </form> 
                                       </div><!--Fin de contenido proveedor-->                   
   	<?php
					}
				}
	?>       

            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>