<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Agregar Nuevo Estilo </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria" style="width:505px;">
								Crear Catalogo de Moldes
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO 1 -->
							<div class="area_contenido1">
								<br/>
								<br/>
								<?php
									include('config.php');
									$query_estilos = mysql_query("SELECT id_estilo, estilo
																  FROM estilos
																  ORDER BY id_estilo ASC") or die (mysql_error());
								?>
								<center>
								<table>
										<tr>
											<th width="120"> N° de Estilo </th>
											<th> Nombre del Estilo </th>
                                            <th width="25"></th>
										</tr>
									<?php
										while( $row_estilos = mysql_fetch_array($query_estilos))
										{
											$id_estilo = $row_estilos['id_estilo'];
											$estilo = $row_estilos['estilo'];
									?>
											<tr>
												<td id="centrado"> <?php echo $id_estilo; ?> </td>
												<td> <?php echo $estilo; ?> </td>
                                                <td><a href="modificar_catalogo_moldes.php?id_estilo=<?php echo $id_estilo; ?>">
                                            		<img src="../img/modify.png" />
                                                 </a></td>
									    	</tr>
									<?php
										}
									?>
								</table> <!-- FIN DE TABLA -->
								</center>
								<br/><br/>
								<div class="titulos"> Agregar Nuevo Catalogo de Molde</div><!-- FIN DIV TITULOS -->
								<center><br/>
								<form  name="forma_nuevo_catalogo" method="post" action="procesa_agregar_nuevo_catalogo.php" class="valida_catalogo">
									<table>
										<tr>
											<td id="alright"> <label class="textos"> Nombre del Estilo: </label> </td>
											<td id="alleft">
                                            	<select name="id_estilo">
                                            		<option value="0">-- Seleccione --</option>
                                     	<?php
											$query_estilos2 = mysql_query("SELECT id_estilo, estilo
										  									FROM estilos
										 									ORDER BY id_estilo ASC") or die (mysql_error());
											while( $row_estilos2 = mysql_fetch_array($query_estilos2))
											{
												$id_estilo = $row_estilos2['id_estilo'];
												$estilo = $row_estilos2['estilo'];
										?>
                                        			<option value="<?php echo $id_estilo; ?>">
                                                    	<?php echo $estilo; ?>
                                                    </option>    
                                     	<?php
											}
										?>
                                            	</select>
                                            </td>
										</tr>
										<tr>
											<td colspan="3"> 
												<p align="right">
													<br/>
													<input type="reset" name="cancelar" id="cancelar" value="Cancelar" title="Cancelar" class="fondo_boton"/>
													<input type="submit" name="accion" id="accion" value="Guardar" title="Guardar" class="fondo_boton"/> <br/> 
												</p>
											</td>
										</tr>
									</table>
								</form>
								</center>
							</div><!-- FIN DIV AREA CONTENIDO 1 -->
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html>