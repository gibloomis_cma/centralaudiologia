<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Material</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Materiales
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
 	<?php 
		include("config.php");
		$id_material=$_GET["id_material"];
		$consulta_materiales=mysql_query("SELECT * FROM materiales WHERE id_material=".$id_material)or die(mysql_error());
		$row_materiales=mysql_fetch_array($consulta_materiales);
		$nombre_material = $row_materiales['material'];
		$precio = $row_materiales['precio'];
		$descripcion = $row_materiales['descripcion'];
	?>
            <br />
            	<div class="titulos">Modificar Materiales</div> 
            	<div class="contenido_poroveedor">
            <br />
            	<center>
                    <table>                    
                    <form name="form1" action="procesa_agregar_nuevo_material.php" method="post">
                    <input name="id_material" type="hidden" value="<?php echo $id_material; ?>" />
                        <tr>
                            <tr>
                            <td id="alright">
                                <label class="textos">Nombre del Material: </label>
                            </td><td id="alleft" colspan="2">
                                <input name="txt_nombre_material" type="text" value="<?php echo $nombre_material; ?>" size="30" maxlength="30"/>
                            </td>
                        </tr><tr>
                            <td id="alright">
                                <label class="textos"> Precio: </label>
                            </td>
                            <td id="alleft" colspan="2">
                                $<input name="precio" type="text" size="10" maxlength="10" value="<?php echo $precio; ?>" />
                            </td>
                        </tr><tr>
                            <td id="alright">
                                <label class="textos"> Descripción: </label>
                            </td>
                            <td id="alleft" colspan="2">
                                <textarea name="txt_descripcion" type="text" rows="2" cols="30"><?php echo $descripcion; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="3" style="text-align:center">Colores del Material</td>
                        </tr>
     	<?php
			// SE REALIZA EL QUERY QUE OBTIENE LOS COLORES QUE SE ACABAN DE AGREGAR AL MATERIAL
			$query_colores_material = mysql_query("SELECT color,r,g,b, id_material_nivel1
													FROM colores,materiales_nivel1
													WHERE materiales_nivel1.id_color = colores.id_color AND
													id_material = '$id_material'") or die (mysql_error());
			while( $row_colores_material = mysql_fetch_array($query_colores_material)){
				$id_material_nivel1 = $row_colores_material['id_material_nivel1'];
				$color = $row_colores_material['color'];
				$r = $row_colores_material['r'];
				$g = $row_colores_material['g'];
				$b = $row_colores_material['b'];
		?>
                        <tr>
                            <td> <label class="textos"> <?php echo $color; ?> </label> </td>
                            <td> <input type="text" name="color" id="color" size="5" readonly="readonly" style=" border:none; background:rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b ?>)"/> </td>
                            <td id="alright"><a href="eliminar_color.php?id_material_color=<?php echo $id_material_nivel1; ?>&id_material=<?php echo $id_material; ?>">
                            	<img src="../img/delete.png" />
                            </a></td>
                        </tr>
		<?php
        	}
			// QUERY QUE OBTIENE TODOS LOS COLORES
			$query_colores = mysql_query("SELECT id_color,color
										  FROM colores
										  ORDER BY id_color ASC") or die (mysql_error());
        ?>
        				<tr>
                        <td> <label class="textos"> Nombre Color: </label> </td>
						<td>
                            <select name="colores">
                                <option value="0"> --- Seleccione Color --- </option>
                            <?php 
                            while( $row_color = mysql_fetch_array($query_colores)){
                                $id_color = $row_color['id_color'];
                                $nombre_color = $row_color['color'];
                            ?>
                                <option value="<?php echo $id_color; ?>"> <?php echo ucwords(strtolower($nombre_color)); ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                        </td>
                        <td> <input type="submit" name="accion" value="Guardar Nuevo Color"  class="fondo_boton"/> </td>
                        </tr>
                        <tr>
                            <td colspan="3" id="alright">
                            	<input name="accion" type="submit" value="Volver" class="fondo_boton" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="accion" type="submit" value="Modificar" class="fondo_boton" />
                            </td>
                        </tr>
                    </form>
                    </table>
                </center>
            	</div><!--Fin de contenido proveedor-->
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>