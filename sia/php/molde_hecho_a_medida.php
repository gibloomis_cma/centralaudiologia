<?php
	error_reporting(0);
	$id_estudio_correcto = $_GET['id_estudio_correcto'];
	$id_paciente = $_GET['id_paciente'];
	$estilo = $_GET['estilo'];
	$material = $_GET['material'];
	$color = $_GET['color'];
	$ventilacion = $_GET['ventilacion'];
	$salida = $_GET['salida'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Moldes </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div><!-- FIN DIV CATEGORIA -->
            </div><!-- FIN DIV FONDO TITULO1 -->
            <div class="area_contenido1">
            <br/>
                <div class="titulos">
                    Nuevo Molde
                </div><!-- FIN DIV TITULOS -->
            <br/>
              	<div class="contenido_proveedor">
                <form id="molde_hecho_a_medida" name="molde_hecho_a_medida" method="post" action="procesa_molde_hecho_a_medida.php" onsubmit="return validaMoldeHechoAMedida()" >
			<?php
                // SE IMPORTA EL ARCHIVO DE LA CONEXION A LA BASE DE DATOS
                include("config.php");
                
                $query_proveedores = mysql_query("SELECT id_proveedor,proveedor
                                                  FROM proveedores
                                                  ORDER BY id_proveedor ASC") or die (mysql_error());
            ?>
                    <table>
                        <tr>
                            <td> N° de Estudio: </td>
			<?php
                if( $id_estudio_correcto != ""){
            ?>
                            <td> <input type="text" name="estudio" id="estudio" readonly="readonly" value="<?php echo $id_estudio_correcto; ?>"/> </td>
			<?php
                }
            ?>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Nombre del Paciente: </td>
			<?php
                if( $id_paciente != ""){
                    $query_nombre_paciente = mysql_query("SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo
                                                          FROM ficha_identificacion
                                                          WHERE id_cliente = $id_paciente") or die (mysql_error());
                    $row_nombre_paciente = mysql_fetch_array($query_nombre_paciente);
                    $nombre_paciente_consultado = ucwords($row_nombre_paciente['nombre_completo']);
            ?>
                          	<td> <input type="text" name="paciente" id="paciente" readonly="readonly" value="<?php echo $nombre_paciente_consultado; ?>"/> </td>
			<?php
                }
            ?>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Fecha de Realización: </td>
                            <?php
                                $fecha_actual = date("d/m/Y");
                            ?>
                            <td> <input type="text" name="fecha_realizacion" id="fecha_realizacion" readonly="readonly" value="<?php echo $fecha_actual; ?>"/> </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Proveedor: </td>
                            <td>
                                <select id="proveedores" name="proveedores">
                                    <option value="0"> --- Seleccione Proveedor --- </option>
			<?php
                while( $row_proveedor = mysql_fetch_array($query_proveedores)){
                    $id_proveedor = $row_proveedor['id_proveedor'];
                    $nombre_proveedor = $row_proveedor['proveedor'];
            ?>
                                    <option value="<?php echo $id_proveedor; ?>"> <?php echo ucwords(strtolower(($nombre_proveedor))); ?> </option>
			<?php
                }
            ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Modelo: </td>
                            <td>
                                <select id="modelos" name="modelos" disabled="disabled">
                                    <option value="0"> --- Seleccione Modelo ---</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Especiales: </td>
                            <td>
                                <textarea cols="30" rows="2" name="especiales" id="especiales"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center> <input type="submit" name="accion" value="Guardar" title="Guardar" class="fondo_boton"/> </center> </td>
                        </tr>
                        <input type="hidden" name="estilo" id="estilo" value="<?php echo $estilo; ?>"/>
                        <input type="hidden" name="material" id="material" value="<?php echo $material; ?>"/>
                        <input type="hidden" name="color" id="color" value="<?php echo $color; ?>"/> 
                        <input type="hidden" name="ventilacion" id="ventilacion" value="<?php echo $ventilacion; ?>"/>
                        <input type="hidden" name="salida" id="salida" value="<?php echo $salida; ?>"/>
                        <input type="hidden" name="id_paciente" id="num_paciente" value="<?php echo $id_paciente; ?>"/> 
                    </table><!-- FIN DE TABLA -->
				</form>
            	</div><!-- Fin de contenido proveedor -->
        	</div><!-- FIN DIV AREA CONTENIDO1 -->
        </div><!-- FIN DIV CONTENIDO PAGINA -->
    </div><!-- FIN DIV CONTENIDO COLUMNA2 -->
</div><!-- FIN DIV WRAPP -->
</body>
</html>