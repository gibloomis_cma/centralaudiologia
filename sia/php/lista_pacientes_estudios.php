<?php
	error_reporting(0);

	// SE INICIA SESION PARA EL USUARIO QUE ENTRO AL SISTEMA
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
<script type="text/javascript">
	$(function() {
    	//call the function onload, default to page 1
    	getdata( 1 );
	});
		
	function getdata( pageno ){                     
    	var targetURL = 'lista_pacientes_estudios_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_pacientes_estudios').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_pacientes_estudios').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow');
	}
	
	function getdata2( ){   
		var valor = $('#paginas option:selected').attr('value');
		var targetURL = 'lista_pacientes_estudios_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_pacientes_estudios').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_pacientes_estudios').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow');                  
   		
	}      
</script>
</head>
<body>
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Pacientes
                </div>
            </div><!--Fin de fondo titulo-->
            <?php 
						date_default_timezone_set('America/Monterrey');
						$script_tz = date_default_timezone_get();
						$fecha = date("d/m/Y");
						$hora = date("h:i:s A");
            			if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
            			{
                			$filtro = $_REQUEST['filtro'];

                			// SE REALIZA QUERY PARA OBTENER EL RESULTADO DE LA BUSQUEDA REALIZADA
                			$res_busqueda = mysql_query("SELECT COUNT(DISTINCT(id_cliente)) AS contador
                                                                FROM ficha_identificacion
                                                                WHERE nombre LIKE '%".$filtro."%' 
                                                                OR paterno LIKE '%".$filtro."%' 
                                                                OR materno LIKE '%".$filtro."%'")
                                                                or die(mysql_error());

                			// SE ALMACENA EN UNA VARIABLE EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
							$row_busqueda = mysql_fetch_array($res_busqueda);
							$busqueda += $row_busqueda["contador"];

							// SE VALIDA SI LA BUSQUEDA OBTUVO RESULTADOS
							if( $busqueda == 0 )
							{
								$busqueda = 0;	
							}
                			$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
            			}
            			else
            			{
               				$res2 = "";
            			}
					?>
           
            <div class="buscar2">
                <form name="busqueda" method="post" action="lista_pacientes_estudios.php">
                    <label class="textos"><?php echo $res2; ?></label>
                    <input name="filtro" type="text" size="15" maxlength="15" />
                    <input name="buscar" type="submit" value="Buscar" class="fondo_boton" style="height:25px;" />
                </form>
            </div><!-- Fin de la clase buscar -->
                <div class="area_contenido2"> 
                <center>                    
                <div id="tabla_lista_pacientes_estudios">
					<img src="../img/ajax-loader.gif"/>
				</div>
                </center>
                <br />
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</body>
</html>