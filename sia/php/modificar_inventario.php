<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Modificar Inventario </title>
    <link type="text/css" rel="stylesheet" href="../css/style3.css">
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"> </script>
    <script type="text/javascript" language="javascript" src="../js/funcion.js"> </script>
</head>
<body>    
<div id="wrapp">   
<div id="contenido_columna2">
	<div class="contenido_pagina">
		 <div class="fondo_titulo1">
			<div class="categoria">
            	Inventario
            </div>
		</div><!--Fin de fondo titulo-->
        <div class="area_contenido">
		<?php
            // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
			include("config.php");

            // SE OBTIENE EL ID DEL PRODUCTO POR METODO GET
			$id_base_producto = $_GET["id_base_producto"];

            // SE REALIZA QUERY PARA OBTENER LOS DATOS DEL PRODUCTO
			$consulta_datos_base_producto = mysql_query("SELECT id_categoria, id_articulo ,id_subcategoria, precio_venta, precio_compra, referencia_codigo,inventario_minimo,inventario_maximo
														 FROM base_productos
														 WHERE id_base_producto=".$id_base_producto) Or die(mysql_error());
			
            $row = mysql_fetch_array($consulta_datos_base_producto);
            $id_categoria = $row["id_categoria"];
            $articulo = $row["id_articulo"];
            $id_subcategoria = $row["id_subcategoria"];
			$precio_compra = $row["precio_compra"];
			$precio_venta = $row["precio_venta"];
			$referencia_codigo = $row["referencia_codigo"];
            $inventario_minimo = $row['inventario_minimo'];
            $inventario_maximo = $row['inventario_maximo'];
            
            if( $id_categoria == "1" )
            {	
            	$consulta_datos_refaccion = mysql_query("SELECT * 
                                                         FROM refacciones
                                                         WHERE id_refaccion = ".$articulo) or die(mysql_error());
                
                $row2 = mysql_fetch_array($consulta_datos_refaccion);
                $concepto = $row2["concepto"];
                $utilidad = $row2["utilidad"];
                $descripcion = $row2["descripcion"];
                $codigo = $row2["codigo"];
                $id_proveedor = $row2["id_proveedor"];
            
   		?>
            <div class="titulos" style="margin-top:8px;"> Refacciones </div>
            <div class="contenido_proveedor" style="margin-top:8px;">
                <center>
                <form name="form" action="proceso_modificar_inventario.php" method="post">
                    <input name="id_base_producto" type="hidden" value="<?php echo $id_base_producto; ?>" />
                    <input name="id_articulo" type="hidden" value="<?php echo $articulo; ?>" />
                    <input name="categoria1" type="hidden" value="<?php echo $id_categoria; ?>" />
                    <table>
                        <tr>
                            <td id="alright">
                                <label class="textos">Subcategoria: </label>
                            </td><td id="alleft">
                                <select name="subcategoria1" id="subcategoria1"  style="width:300px">
                                    <option value="">Seleccione</option>
                                    <?php
                                                           
                                        $consulta_subcategorias = mysql_query("SELECT subcategoria, id_subcategoria
                                                                                    FROM subcategorias_productos
                                                                                    WHERE id_categoria=1") 
                                                                                    or die(mysql_error());
                                        while($row4 = mysql_fetch_array($consulta_subcategorias)){
                                            $subcategoria = $row4["subcategoria"];
                                            $id_subcategoria_consultado = $row4["id_subcategoria"];
                                            if($id_subcategoria<>"" && $id_subcategoria == $id_subcategoria_consultado){
                                    ?>
                                    <option value="<?php echo $id_subcategoria; ?>" selected="selected">
                                        <?php echo $subcategoria; ?>
                                    </option>
                                    <?php
                                            }else{
                                    ?>       
                                    <option value="<?php echo $id_subcategoria_consultado; ?>">
                                        <?php echo utf8_encode($subcategoria); ?>
                                    </option>
                                    <?php 	
                                            } 
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr><tr>
                            <td id="alright">
                                <label class="textos">Proveedores: </label>
                            </td><td id="alleft">
                                <select name="proveedor" style="width:300px">
                                    <option value="0">Seleccione</option>
                                    <?php
                                        $consulta_proveedores = mysql_query("SELECT razon_social, id_proveedor 
                                                                                FROM proveedores") or die(mysql_error());
                                        while($row3 = mysql_fetch_array($consulta_proveedores)){
                                            $id_proveedor_consultado = $row3["id_proveedor"];
                                            $razon_social = $row3["razon_social"];
                                            if($id_proveedor<>"" && $id_proveedor == $id_proveedor_consultado){
                                    ?>
                                    <option value="<?php echo $id_proveedor; ?>" selected="selected">
                                        <?php echo $razon_social; ?>
                                    </option>
                                    <?php
                                            }else{
                                    ?>       
                                    <option value="<?php echo $id_proveedor_consultado; ?>">
                                        <?php echo utf8_encode($razon_social); ?>
                                    </option>
                                    <?php 	
                                            } 
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr><tr>
                            <td id="alright">
                                <label class="textos">Codigo: </label>
                            </td><td id="alleft">
                                <input name="codigo" type="text" value="<?php echo $codigo; ?>" maxlength="30" style="width:300px"/>
                            </td>
                        </tr><tr>                                
                            <td id="alright">
                                <label class="textos">Concepto: </label>
                            </td><td id="alleft">
                                <input name="concepto" type="text" maxlength="70" value="<?php echo $concepto; ?>"  style="width:300px"/>
                            </td>
                        </tr><tr>                                
                            <td id="alright">
                                <label class="textos">Precio de venta: </label>
                            </td><td id="alleft">                        
                                <label class="textos">$</label>
                                <input name="precio_venta" type="text" size="10" maxlength="10" value="<?php echo $precio_venta; ?>"/>
                            </td>
                        </tr><tr>                                
                            <td id="alright">                                                       
                                <label class="textos">Precio de compra: </label>
                            </td><td id="alleft">
                                <label class="textos">$</label>
                                <input name="precio_compra" type="text" size="10" maxlength="10" value="<?php echo $precio_compra; ?>"/>
                            </td>
                        </tr>
                        <tr>                                
                            <td id="alright"> 
                                                       
                                <label class="textos">Utilidad: </label>
                            </td>
                            <td id="alleft">
                                <label class="textos">$<?php echo $utilidad; ?></label>                                
                            </td>
                        </tr>
                        <tr>
                            <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" size="5" value="<?php echo $inventario_minimo; ?>" /> </td>
                        </tr>
                        <tr>
                            <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" size="5" value="<?php echo $inventario_maximo; ?>" /> </td>
                        </tr>
                        <tr>                                
                            <td id="alright">                         
                                <label class="textos">Descripcion: </label>
                            </td>
                            <td id="alleft">
                                <textarea name="descripcion"  style="width:300px" rows="3"><?php echo $descripcion; ?></textarea>
                            </td>
                        </tr>
                        <tr>                                
                            <td id="alright" colspan="2"> 
                            <input name="volver" value="Volver" class="fondo_boton" type="button" 
                            onclick="window.location.href='lista_inventario.php'" />
                            <input name="accion" type="submit" value="Modificar" class="fondo_boton"/>
                            </td>
                        </tr>
                    </table>
                </form>
                </center>
            </div><!--Fin de contenido proveedor-->
		<?php
            }elseif($id_categoria == "4"){

                $consulta_datos_bateria = mysql_query("SELECT * FROM baterias
                                                                WHERE id_bateria =".$articulo) 
                                                                or die(mysql_error());
                $row8 = mysql_fetch_array($consulta_datos_bateria);
                $codigo = $row8["referencia"];
                $descripcion = $row8["descripcion"];    
        ?>
            <div class="titulos" style="margin-top:8px;">Baterias</div>
            <div class="contenido_proveedor" style="margin-top:8px;">
            	<center>
                <form name="form" action="proceso_modificar_inventario.php" method="post">
                    <input name="id_base_producto" type="hidden" value="<?php echo $id_base_producto; ?>" />
                    <input name="id_articulo" type="hidden" value="<?php echo $articulo; ?>" />
                    <input name="categoria1" value="<?php echo $id_categoria; ?>" type="hidden" />
                    <input name="subcategoria1" id="subcategoria1" value="55" type="hidden" />
                    <table>
						<tr>
                        	<td id="alright">
                        		<label class="textos">Codigo: </label>
                        	</td><td id="alleft">
                        		<input name="codigo" type="text" value="<?php echo $codigo; ?>" style="width:300px;"/>
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Descripcion: </label>
                        	</td><td id="alleft">
                  				<input name="descripcion" type="text" value="<?php echo $descripcion; ?>" style="width:300px;" maxlength="28" />
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Precio de venta: </label>
                        	</td><td id="alleft">
                        		<input name="precio_venta" type="text" size="10" maxlength="10" value="<?php echo $precio_venta; ?>"/>
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Precio de compra: </label>
                        	</td><td id="alleft">
                        		<input name="precio_compra" type="text" size="10" maxlength="10" value="<?php echo $precio_compra; ?>"/>
                        	</td>
                        </tr>
                        <tr>
                            <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" size="5" value="<?php echo $inventario_minimo; ?>" /> </td>
                        </tr>
                        <tr>
                            <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" size="5" value="<?php echo $inventario_maximo; ?>" /> </td>
                        </tr>
                        <tr>
                        	<td id="alright" colspan="2">
                                <input name="volver" value="Volver" class="fondo_boton" type="button" 
                                onclick="window.location.href='lista_inventario.php'" />							
                    			<input name="accion" type="submit" value="Modificar" class="fondo_boton"/>
                        	</td>
                        </tr>                   
                    </table>	
                </form>
                </center>
            </div><!--Fin de contenido proveedor-->
		<?php    
			}else{
				$consulta_datos_modelos = mysql_query("SELECT * FROM modelos
																WHERE id_modelo =".$articulo) 
																or die(mysql_error());
				$row10 = mysql_fetch_array($consulta_datos_modelos);
				$id_proveedor = $row10["id_proveedor"];
				$modelo = $row10["modelo"];
				$id_tipo_modelo = $row10["id_tipo_modelo"];
				$comision = $row10["comision"];
        ?>
            <div class="titulos" style="margin-top:8px;">Modelos</div>
            <div class="contenido_proveedor"  style="margin-top:8px;">
            	<center>
                <form name="form" action="proceso_modificar_inventario.php" method="post">
                    <input name="id_base_producto" type="hidden" value="<?php echo $id_base_producto; ?>" />
                    <input name="id_articulo" type="hidden" value="<?php echo $articulo; ?>" />
                    <input name="categoria1" value="<?php echo $id_categoria; ?>" type="hidden" />
                    <table>
                    	<tr>
                        	<td id="alright">
                    			<label class="textos">Subcategoria</label>
                    		</td><td id="alleft">
                                <select name="subcategoria1" id="subcategoria1" style="width:300px">
                                    <option value="">Seleccione</option>
                                    <?php
                                        $consulta_subcategorias = mysql_query("SELECT subcategoria, id_subcategoria
                                                                                    FROM subcategorias_productos
                                                                                    WHERE id_categoria=5") 
                                                                                    or die(mysql_error());
                                        while($row9 = mysql_fetch_array($consulta_subcategorias)){
                                            $subcategoria = $row9["subcategoria"];
                                            $id_subcategoria_consultado = $row9["id_subcategoria"];
                                            if($id_subcategoria<>"" && $id_subcategoria == $id_subcategoria_consultado){
                                    ?>
                                    <option value="<?php echo $id_subcategoria; ?>" selected="selected">
                                        <?php echo $subcategoria; ?>
                                    </option>
                                    <?php
                                            }else{
                                    ?>       
                                    <option value="<?php echo $id_subcategoria_consultado; ?>">
                                         <?php echo utf8_encode($subcategoria); ?>
                                    </option>
                                    <?php 	
                                            } 
                                        }
                                    ?>
                                    </select>
                        		</td>
                        	</tr><tr>
                            	<td id="alright">
                        			<label class="textos">Proveedores: </label>
                        		</td><td id="alleft">
                                    <select name="proveedor" style="width:300px">
                                        <option value="0">Seleccione</option>
                                        <?php
                                            $consulta_proveedores = mysql_query("SELECT razon_social, id_proveedor 
                                                                                    FROM proveedores") or die(mysql_error());
                                            while($row3 = mysql_fetch_array($consulta_proveedores)){
                                                $id_proveedor_consultado = $row3["id_proveedor"];
                                                $razon_social = $row3["razon_social"];
                                                if($id_proveedor<>"" && $id_proveedor == $id_proveedor_consultado){
                                        ?>
                                        <option value="<?php echo $id_proveedor; ?>" selected="selected">
                                            <?php echo $razon_social; ?>
                                        </option>
                                        <?php
                                                }else{
                                        ?>       
                                        <option value="<?php echo $id_proveedor_consultado; ?>">
                                            <?php echo utf8_encode($razon_social); ?>
                                        </option>
                                        <?php 	
                                                } 
                                            }
                                        ?>
                                    </select>
                                </td>
                        	</tr><tr>
                            	<td id="alright">
                        			<label class="textos">Codigo: </label>
                        		</td><td id="alleft">
                        			<input name="referencia" type="text" size="45" maxlength="45" value="<?php echo $referencia_codigo; ?>" style="width:300px" />
                        		</td>
                        	</tr><tr>
                            	<td id="alright">
                        			<label class="textos">Modelo: </label>
                        		</td><td id="alleft">
                        			<input name="modelo" type="text" size="45" maxlength="45" value="<?php echo $modelo; ?>" style="width:300px" />
                        		</td>
                        	</tr><tr>
                            	<td id="alright">
                        			<label class="textos">Tipo de Aparato: </label>
                        		</td><td id="alleft">
                                    <select name="tipo_aparato" style="width:300px">
                                        <option value="0">Seleccione</option>
               							<?php
											$consulta_tipo_aparatos = mysql_query("SELECT id_tipo_modelo, modelo_aparato
																						FROM tipos_modelos_aparatos")
																						or die(mysql_error());
											while($row5 = mysql_fetch_array($consulta_tipo_aparatos)){
												$id_tipo_modelo_consultado = $row5["id_tipo_modelo"];
												$modelo_aparato = $row5["modelo_aparato"];
												if($id_tipo_modelo<> "" && $id_tipo_modelo == $id_tipo_modelo_consultado){
                						?>
                                        <option value="<?php echo $id_tipo_modelo; ?>" selected="selected">
                                            <?php echo $modelo_aparato; ?>
                                        </option>
                						<?php 
                                   				}else{
                						?>       
                                        <option value="<?php echo $id_tipo_modelo_consultado; ?>">
                                            <?php echo $modelo_aparato; ?>
                                        </option>
                						<?php
												}
											}
                						?>                             
                                    </select>
								</td>
                            </tr><tr>
                            	<td id="alright">
                        			<label class="textos">Comision: </label>
                                </td><td id="alleft">
                                	<label class="textos">$</label>
                                    <input name="comision" type="text" value="<?php echo $comision; ?>" size="10" maxlength="10"/>
                    			</td>
                            </tr><tr>
                            	<td id="alright">
                    				
                                    <label class="textos">Precio de venta: </label>
                        		</td><td id="alleft">
                        			<label class="textos">$</label>
                                    <input name="precio_venta" type="text" size="10" maxlength="10" value="<?php echo $precio_venta; ?>"/>
                        		</td>
                        	</tr><tr>
                            	<td id="alright">                        			
                                    <label class="textos">Precio de compra: </label>
                        		</td><td id="alleft">
                        			<label class="textos">$</label>
                                    <input name="precio_compra" type="text" size="10" maxlength="10" value="<?php echo $precio_compra; ?>"/>
                        		</td>
                            </tr>
                            <tr>
                            <td id="alright"> <label class="textos"> Inventario M&iacute;nimo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_minimo" id="inventario_minimo" size="5" value="<?php echo $inventario_minimo; ?>" /> </td>
                        </tr>
                        <tr>
                            <td id="alright"> <label class="textos"> Inventario M&aacute;ximo: </label> </td>
                            <td id="alleft"> <input type="text" name="inventario_maximo" id="inventario_maximo" size="5" value="<?php echo $inventario_maximo; ?>" /> </td>
                        </tr>
                            <tr>
                            	<td colspan="2" id="alright">
                                    <input name="volver" value="Volver" class="fondo_boton" type="button" 
                                    onclick="window.location.href='lista_inventario.php'" />
                                    <input name="accion" type="submit" value="Modificar" class="fondo_boton"/>
                        		</td>
                           	</tr>
						</table>
                   	</form>
                   	</center>
				</div><!--Fin de contenido proveedor-->
			<?php
            	}
            ?>                    
        </div><!-- Fin de area contenido -->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div>
</body>
</html>