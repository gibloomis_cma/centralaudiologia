<!-- include style -->
 <link rel="stylesheet" type="text/css" href="../css/style_pagination.css" />

<?php
    //open database
    include 'config.php';
    
    //include our awesome pagination
    //class (library)
    include '../librerias/ps_pagination.php';

    $filtro = $_POST['filtro'];

    if ( $filtro != "" )
    {
        // query all data anyway you want
        $sql = "SELECT DISTINCT(id_cliente), nombre, paterno, materno, fecha_entrada, id_estatus_reparaciones, folio_num_reparacion, num_serie, id_modelo, reparacion, adaptacion, venta,aplica_garantia
                FROM reparaciones, base_productos_2
                WHERE (nombre LIKE '%".$filtro."%'
                OR paterno LIKE '%".$filtro."%' 
                OR materno  LIKE '%".$filtro."%' 
                OR folio_num_reparacion LIKE '%".$filtro."%'
                OR num_serie LIKE '%".$filtro."%'
                OR base_productos_2.descripcion LIKE '%".$filtro."%')
                AND base_productos_2.id_base_producto2 = reparaciones.id_modelo
                AND id_estatus_reparaciones = 3
                ORDER BY fecha_entrada DESC";
    }
    else
    {
        //query all data anyway you want
        $sql = "SELECT id_cliente,nombre,paterno,materno,fecha_entrada,id_estatus_reparaciones,folio_num_reparacion,num_serie,id_modelo, reparacion, adaptacion, venta, aplica_garantia
                FROM reparaciones
                WHERE id_estatus_reparaciones = 3
                ORDER BY fecha_entrada DESC";
    }

    //execute query and get and
    $rs = mysql_query($sql) or die(mysql_error());

    //now, where gonna use our pagination class
    //this is a significant part of our pagination
    //i will explain the PS_Pagination parameters
    //$conn is a variable from our config_open_db.php
    //$sql is our sql statement above
    //3 is the number of records retrieved per page
    //4 is the number of page numbers rendered below
    //null - i used null since in dont have any other
    //parameters to pass (i.e. param1=valu1&param2=value2)
    //you can use this if you're gonna use this class for search
    //results since you will have to pass search keywords
    $pager = new PS_Pagination( $link, $sql, 15, 3, null );

    //our pagination class will render new
    //recordset (search results now are limited
    //for pagination)
    $rs = $pager->paginate(); 

    //get retrieved rows to check if
    //there are retrieved data
    $num = mysql_num_rows( $rs );

    if( $num >= 1 )
    {     
    ?>
        <table>
            <tr>
                <th class="titulo" width="45"> Nº Nota </th>
                <th class="titulo" width="1000"> Nombre Completo </th>
                <th class="titulo"> Marca </th>
                <th class="titulo" width="100"> Modelo </th>
                <th class="titulo" width="70"> N° Serie </th>
                <th class="titulo" width="80"> Precio de Reparaci&oacute;n </th>
                <th class="titulo" width="180"> Estatus </th>
                <th class="titulo" width="10"> </th>
            </tr>
    <?php    
        //looping through the records retrieved class='data-tr' align='center'
        while( $row = mysql_fetch_array( $rs ) )
        {
            $id_cliente = $row["id_cliente"];
            $nombre = $row["nombre"];
            $paterno = $row["paterno"];
            $materno = $row["materno"];
            $num_serie = $row["num_serie"];
            $id_modelo = $row["id_modelo"];
            $estado_reparacion = $row["id_estatus_reparaciones"];
            $garantia_reparacion = $row['reparacion'];
            $garantia_adaptacion = $row['adaptacion'];
            $garantia_venta = $row['venta'];
            $aplica_garantia = $row['aplica_garantia'];
            $fecha_entrada = $row["fecha_entrada"];
            $fecha_entrada_separada = explode("-", $fecha_entrada);
            $fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
            $folio_num_reparacion = $row["folio_num_reparacion"];
            $consulta_estatus_nota_reparacion = mysql_query("SELECT * FROM estatus_reparaciones
                                                             WHERE id_estatus_reparacion=".$estado_reparacion) or die(mysql_error());    
                    
            $row_estatus_nota_reparacion = mysql_fetch_array($consulta_estatus_nota_reparacion);
            $estatus_reparacion = $row_estatus_nota_reparacion["estado_reparacion"];

            $consulta_descripcion_modelo = mysql_query("SELECT descripcion, subcategoria 
                                                        FROM base_productos_2,subcategorias_productos_2
                                                        WHERE id_base_producto2 =".$id_modelo." 
                                                        AND base_productos_2.id_subcategoria = subcategorias_productos_2.id_subcategoria") or die(mysql_error());
                    
            $row_descripcion_modelo = mysql_fetch_array($consulta_descripcion_modelo);
            $subcategoria_consultada = $row_descripcion_modelo["subcategoria"];
            $descripcion_modelo = $row_descripcion_modelo["descripcion"];
                    
            $consulta_presupuesto = mysql_query("SELECT presupuesto 
                                                 FROM reparaciones_laboratorio 
                                                 WHERE folio_num_reparacion=".$folio_num_reparacion) or die(mysql_error());
                    
            $row_presupuesto = mysql_fetch_array($consulta_presupuesto);
            $presupuesto = $row_presupuesto["presupuesto"];                                     
    ?>
            <tr>
                <td class="tamano_letra"> <?php echo $folio_num_reparacion ?> </td>
                <td class="tamano_letra"> <?php echo ucwords($nombre." ".$paterno." ".$materno); ?> </td>
                <td class="tamano_letra" style="text-align:center;"> <?php echo ucfirst($subcategoria_consultada); ?> </td>
                <td class="tamano_letra" style="text-align:center;"> <?php echo ucfirst($descripcion_modelo); ?> </td>
                <td class="tamano_letra" style="text-align:center;"> <?php echo strtoupper($num_serie); ?> </td>
                <td class="tamano_letra" style="text-align:center;">
                    <?php
                        if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                        {
                            echo "$ 0.00";
                        }
                        else
                        {
                            echo number_format($presupuesto,2);
                        }
                    ?>
                </td>
                <td class="tamano_letra"> <?php echo ucwords($estatus_reparacion); ?> </td>
                <td> <a href="detalles_reparaciones2.php?folio=<?php echo $folio_num_reparacion; ?>"> <img src="../img/modify.png" /> </a> </td>
            </tr>   
    <?php
        }       
    ?>
        </table>
    <?php
    }
    else
    {
        // if no records found
        echo "<h3> No existen reparaciones registradas ! </h3>";
    }

    //page-nav class to control
    //the appearance of our page 
    //number navigation
    echo "<div class='page-nav'>";
         //display our page number navigation
         echo $pager->renderFullNav();
    echo "</div>";

?>