<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL ID DEL VALE POR METODO POST PARA REALIZAR EL QUERY
	$id_vale_bateria = $_POST['id_vale_bateria'];

	// SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DEL VALE
	$query_vale_bateria = "SELECT uso_bateria,fecha,id_sucursal,origen, CONCAT(nombre,' ', paterno) AS responsable 
						   FROM vales_baterias, uso_baterias, empleados,areas_departamentos, descripcion_vale_baterias
						   WHERE uso_de_bateria = id_uso_bateria
						   AND responsable = id_empleado
						   AND vales_baterias.id_vale_bateria = descripcion_vale_baterias.id_vale_bateria
 						   AND areas_departamentos.id_departamento = empleados.id_departamento
						   AND vales_baterias.id_vale_bateria = '$id_vale_bateria'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EL CUAL SE ALMACENA EN UNA VARIABLE
	$resultado_vale_bateria = mysql_query($query_vale_bateria) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_vale_bateria = mysql_fetch_array($resultado_vale_bateria);

	// SE DECLARAN VARIABLES CON EL RESULTADO FINAL DEL QUERY
	$folio_vale = $id_vale_bateria;
	$fecha = $row_vale_bateria['fecha'];
	$fecha_separada = explode("-", $fecha);
	$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
	$uso_bateria = $row_vale_bateria['uso_bateria'];
	$origen = $row_vale_bateria['origen'];
	$sucursal = $row_vale_bateria['id_sucursal'];
	$responsable = $row_vale_bateria['responsable'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL
	$query_sucursal = "SELECT nombre
					   FROM sucursales
					   WHERE id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);
	$nombre_sucursal = $row_sucursal['nombre'];

	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL ORIGEN(ALMACEN)
	$query_almacen = "SELECT almacen
					  FROM almacenes
					  WHERE id_sucursal = '$sucursal'
					  AND id_almacen = '$origen'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO DEL QUERY
	$resultado_almacen = mysql_query($query_almacen) or die(mysql_error());

	$row_almacen = mysql_fetch_array($resultado_almacen);
	$nombre_almacen = $row_almacen['almacen'];

	// SE IMPRIMEN LAS VARIABLES PARA MOSTRARLAS EN PANTALLA
	echo $folio_vale;
	echo "°";
	echo $fecha_normal;
	echo "°";
	echo $nombre_sucursal;
	echo "°";
	echo $responsable;
	echo "°";
	echo $nombre_almacen;
	echo "°";
	echo $uso_bateria;
?>