<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL NUM DE NOTA DE REPARACION POR METODO POST
	$folio_num_reparacion = $_POST['folio_nota_reparacion'];

	// SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DE LA NOTA DE REPARACION
	$query_nota_reparacion = "SELECT folio_num_reparacion,fecha_entrada,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_cliente,id_modelo,num_serie,id_estatus_reparaciones
							  FROM reparaciones
							  WHERE folio_num_reparacion = '$folio_num_reparacion'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nota_reparacion = mysql_query($query_nota_reparacion) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
	$row_nota_reparacion = mysql_fetch_array($resultado_nota_reparacion);

	// SE DECLARAN VARIABLES CON EL RESULTADO FINAL
	$folio_num_reparacion = $row_nota_reparacion['folio_num_reparacion'];
	$fecha_entrada = $row_nota_reparacion['fecha_entrada'];
	$fecha_separada = explode("-", $fecha_entrada);
	$fecha_entrada_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
	$nombre_cliente = $row_nota_reparacion['nombre_cliente'];
	$id_modelo = $row_nota_reparacion['id_modelo'];
	$num_serie = $row_nota_reparacion['num_serie'];
	$estatus_reparaciones = $row_nota_reparacion['id_estatus_reparaciones'];

	// SE REALIZA QUERY PARA OBTENER EL MODELO DEL AUXILIAR
	$consulta_modelo = "SELECT subcategoria 
						FROM base_productos_2, subcategorias_productos
						WHERE id_base_producto2 = '$id_modelo'
						AND subcategorias_productos.id_subcategoria = base_productos_2.id_subcategoria";
	
	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_consulta_modelo = mysql_query($consulta_modelo) or die(mysql_error());

	// SE DECLARA UNA VARIABLE CON EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
	$row_consulta_modelo = mysql_fetch_array($resultado_consulta_modelo);

	// SE DECLARAN VARIABLES CON EL RESULTADO
	$subcategoria = $row_consulta_modelo['subcategoria'];

	// SE REALIZA QUERY QUE OBTIENE EL ESTADO DE REPARACION
	$query_estado_reparacion = "SELECT estado_reparacion
								FROM estatus_reparaciones
								WHERE id_estatus_reparacion = '$estatus_reparaciones'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_estado_reparacion = mysql_query($query_estado_reparacion) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_estado_reparacion = mysql_fetch_array($resultado_estado_reparacion);
	$estado_reparacion = $row_estado_reparacion['estado_reparacion'];

	// SE IMPRIMEN LAS VARIABLES PARA MOSTRARLAS EN PANTALLA
	echo $folio_num_reparacion;
	echo "°";
	echo $fecha_entrada_normal;
	echo "°";
	echo $nombre_cliente;
	echo "°";
	echo $subcategoria;
	echo "°";
	echo $num_serie;
	echo "°";
	echo $estado_reparacion;
?>