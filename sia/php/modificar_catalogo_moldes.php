<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// EL ID DEL ESTILO RECIBIDO POR METODO GET ES ALMACENADO EN UNA VARIABLE
	$id_estilo = $_GET['id_estilo'];

	// SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DEL ESTILO DE ACUERDO AL ID RECIBIDO
	$consulta_estilos = "SELECT estilo
						 FROM estilos
						 WHERE id_estilo = '$id_estilo'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_estilos = mysql_query($consulta_estilos) or die(mysql_error());

	// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO EN UNA VARIABLE
	$row_estilos = mysql_fetch_array($resultado_estilos);

	// EL NOMBRE DEL ESTILO OBTENIDO DEL QUERY SE ALMACENA EN UNA VARIABLE
	$nombre_estilo = $row_estilos['estilo'];

	// SE REALIZA EL QUERY QUE OBTIENE EL ID DEL CATALOGO DE ACUERDO AL ID DEL ESTILO OBTENIDO
	$consulta_catalogo_estilo = "SELECT id_catalogo
								 FROM catalogo
								 WHERE id_estilo = '$id_estilo'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_catalogo_estilo = mysql_query($consulta_catalogo_estilo) or die(mysql_error());

	// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
	$row_catalogo_estilo = mysql_fetch_array($resultado_catalogo_estilo);

	// EL ID DEL CATALOGO ES ALMACENADO EN UNA VARIABLE
	$id_catalogo_estilo = $row_catalogo_estilo['id_catalogo'];

	// SE VALIDA SI EXISTE YA UN CATALOGO CREADO PARA EL ESTILO CONSULTADO
	if ( $id_catalogo_estilo == "" ) 
	{
	?>
		<script type="text/javascript" language="javascript">
			alert('No existe catalogo para el estilo <?php echo $nombre_estilo; ?>');
			window.location.href = 'crear_nuevo_molde_catalogo.php';
		</script>
	<?php
	}

	// SE REALIZA QUERY QUE OBTIENE LOS MATERIALES DE LA BASE DE DATOS
	$query_materiales = "SELECT id_material,material
						 FROM materiales
						 ORDER BY id_material ASC";

	// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE
	$resultado_materiales = mysql_query($query_materiales) or die(mysql_error());

	// SE REALIZA QUERY QUE MUESTRA LOS MATERIALES DE ACUERDO AL ID CATALOGO DEL ESTILO
	$query_materiales_estilo = "SELECT materiales.id_material, material, id_cat_nivel1
                                FROM materiales, cat_nivel1
                                WHERE cat_nivel1.id_catalogo = '$id_catalogo_estilo' 
                                AND materiales.id_material = cat_nivel1.id_material";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_materiales_estilo = mysql_query($query_materiales_estilo) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Modificar C&aacute;talogo de Moldes </title>
    <link rel="stylesheet" type="text/css" href="../css/style3.css"/>
    <link rel="stylesheet" type="text/css" href="../css/jquery.treeview.css"/>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery-cookie.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.treeview.js"></script>
    <script type="text/javascript" language="javascript" src="../js/demo.js"></script>
    <script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
</head>
<body>
	<div id="wrapp">
		<div id="contenido_columna2">
			<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:505px;">
						Modificar C&aacute;talogo Moldes
					</div><!-- FIN DIV CATEGORIA -->
				</div><!-- FIN DIV FONDO TITULO1 -->
				<div class="area_contenido1">
					<div class="contenido_proveedor">
						<br/>
						<div class="titulos"> Datos del c&aacute;talogo del estilo <?php echo strtolower($nombre_estilo); ?> </div>
						<br/>
						<ul id="red" class="treeview-red" style="margin-left:50px; width:650px;">
							<form name="form1" action="procesa_modificar_catalogo_molde.php" method="post" class="valida_modificar_catalogo">
								<input type="hidden" name="id_estilo" value="<?php echo $id_estilo; ?>" />
                        		<input type="hidden" name="id_catalogo_estilo" value="<?php echo $id_catalogo_estilo; ?>" />
							<li> <span> Estilo <?php echo ucfirst(strtolower($nombre_estilo)); ?> <a href="eliminar_estilo.php?id_estilo=<?php echo $id_estilo; ?>"> <img src="../img/delete.png" /> </a> </span>
								<ul>
									<li> <span style="font-size:16px;"> Guardar nuevo material </span>
										<ul>
											<li>
												<label class="textos"> Nombre del Material: </label>
												<select name="material_estilo">
													<option value="0"> --- Seleccione Material --- </option>
											<?php
												// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY DE MATERIALES
												while ( $row_materiales = mysql_fetch_array($resultado_materiales) ) 
												{
													$id_material = $row_materiales['id_material'];
													$material = $row_materiales['material'];
												?>
													<option value="<?php echo $id_material; ?>"> <?php echo ucfirst(strtolower($material)); ?> </option>
												<?php
												}
											?>
												</select> &nbsp;
												<input type="submit" name="guardar_nuevo_material" value="Guardar Nuevo Material" class="fondo_boton" />
											</li>
										</ul>
									</li>
							</form>
							<form name="form3" action="procesa_modificar_catalogo_molde.php" method="post" class="valida_modificar_catalogo"> 
								<input type="hidden" name="id_estilo" value="<?php echo $id_estilo; ?>" />
                         		<input type="hidden" name="id_catalogo_estilo" value="<?php echo $id_catalogo_estilo; ?>" />  
									<li> <span style="font-size:16px;"> Materiales </span>
									<?php
										// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS MATERIALES DEL ESTILO ACTUAL
										while ( $row_materiales_estilo = mysql_fetch_array($resultado_materiales_estilo) ) 
										{
											$id_cat_nivel1 = $row_materiales_estilo['id_cat_nivel1'];
											$material_estilo = $row_materiales_estilo['material'];
										?>
											<input type="hidden" name="id_cat_nivel1" value="<?php echo $id_cat_nivel1; ?>" />
											<ul>
												<li> <span> <?php echo ucfirst(strtolower($material_estilo)); ?>  <a href="eliminar_material_estilo.php?id_cat_nivel1=<?php echo $id_cat_nivel1; ?>&id_estilo=<?php echo $id_estilo; ?>"> <img src="../img/delete.png" /> </a>  </span>
													<ul>
														<li> <span style="font-size:16px;"> Guardar nueva ventilaci&oacute;n </span> 
															<ul>
																<li> 
																	<label class="textos"> Nombre de la ventilaci&oacute;n: </label> 
																	<select name="ventilacion_material">
																		<option value="0"> --- Seleccione Ventilaci&oacute;n </option>
																<?php
																	// SE REALIZA EL QUERY QUE OBTIENE LAS VENTILACIONES DE LA BASE DE DATOS
																    $consulta_ventilaciones = "SELECT id_ventilacion, nombre_ventilacion, calibre
																                               FROM ventilaciones 
																                               ORDER BY id_ventilacion";

																    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
																    $resultado_ventilaciones = mysql_query($consulta_ventilaciones) or die(mysql_error());

																	// SE REALIZA UN CICLO PARA MOSTRAR LOS RESULTADOS DEL QUERY
																	while ( $row_ventilaciones = mysql_fetch_array($resultado_ventilaciones) ) 
																	{
																		$id_ventilaciones = $row_ventilaciones["id_ventilacion"];
                                    									$nombre_ventilacion_consultada = $row_ventilaciones["nombre_ventilacion"];
                                    									$calibre_consultado = $row_ventilaciones["calibre"];
                                    								?>
                                    									<option value="<?php echo $id_ventilaciones; ?>"> <?php echo utf8_encode(ucfirst(strtolower($nombre_ventilacion_consultada)))." | ".$calibre_consultado; ?> </option>
                                    								<?php
																	}
																?>
																	</select>
																	<input type="submit" name="guardar_ventilacion" value="Guardar Nueva Ventilacion" class="fondo_boton" />
																</li>
															</ul>
														</li>
							</form>
							<form name="form4" action="procesa_modificar_catalogo_molde.php" method="post" class="valida_modificar_catalogo"> 
                        		<input type="hidden" name="id_estilo" value="<?php echo $id_estilo; ?>" />
                    			<input type="hidden" name="id_catalogo_estilo" value="<?php echo $id_catalogo_estilo; ?>" />
                    			<?php
                    				// SE CONSULTAN LAS VENTILACIONES PARA CADA UNO DE LOS MATERIALES
                    				$query_ventilacion_material = "SELECT nombre_ventilacion, calibre, ventilaciones.id_ventilacion,id_cat_nivel2
																   FROM ventilaciones, cat_nivel2
																   WHERE id_cat_nivel1 = '$id_cat_nivel1' 
																   AND cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion";

									// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE
                    				$resultado_ventilacion_material = mysql_query($query_ventilacion_material) or die (mysql_error());
                    			?>  
														<li> <span style="font-size:16px;"> Ventilaciones </span>
													<?php
														// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
														while ( $row_ventilacion_material = mysql_fetch_array($resultado_ventilacion_material) ) 
														{
															$id_cat_nivel2 = $row_ventilacion_material["id_cat_nivel2"];
						      								$nombre_ventilacion = $row_ventilacion_material["nombre_ventilacion"];
						      								$calibe = $row_ventilacion_material["calibre"];
														?>
															<input name="id_cat_nivel2" type="hidden" value="<?php echo $id_cat_nivel2; ?>" />
															<ul>
																<li> <span> <?php echo utf8_encode($nombre_ventilacion)." | ".$calibe; ?>  <a href="eliminar_ventilacion_material.php?id_cat_nivel2=<?php echo $id_cat_nivel2; ?>&id_estilo=<?php echo $id_estilo; ?>"> <img src="../img/delete.png" /> </a> </span>
																	<ul>
																		<li> <span style="font-size:16px;"> Guardar nueva salida </span>
																			<ul>
																				<li>
																					<label class="textos"> Nombre de la salida: </label>
																					<select name="salida_ventilacion">
																						<option value="0"> --- Seleccione Salida --- </option>
																				<?php
																					// SE REALIZA EL QUERY QUE OBTIENE LAS SALIDAS PARA LAS VENTILACIONES DE LA BASE DE DATOS
																				    $consulta_salidas = "SELECT salida, id_salida
																				    					 FROM salidas";

																				    // SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO OBTENIDO
																				    $resultado_consulta_salidas = mysql_query($consulta_salidas) or die(mysql_error());
																			    
																					// SE REALIZA UN CICLO QUE MUESTRA EL RESULTADO OBTENIDO DEL QUERY
																					while ( $row_salidas = mysql_fetch_array($resultado_consulta_salidas) ) 
																					{
																						$id_salida_consultada = $row_salidas["id_salida"];
																						$nombre_salida_consultada = $row_salidas["salida"];
																					?>
																						<option value="<?php echo $id_salida_consultada; ?>"> <?php echo ucfirst(strtolower($nombre_salida_consultada)); ?> </option>
																					<?php
																					}
																				?>
																					</select>
																					<input type="submit" name="guardar_salida" value="Guardar Nueva Salida" class="fondo_boton" />
																				</li>
																			</ul>
																		</li>
																		<li> <span style="font-size:16px;"> Salidas </span>
																		<?php
																			// SE REALIZA EL QUERY QUE OBTIENE LAS SALIDAS DE CADA UNA DE LAS VENTILACIONES
																			$query_salida_ventilacion = "SELECT salida, salidas.id_salida, id_cat_nivel3
																										 FROM salidas, cat_nivel3
																										 WHERE id_cat_nivel2 = '$id_cat_nivel2' 
																										 AND cat_nivel3.id_salida = salidas.id_salida";

																			// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
																			$resultado_salida_ventilacion = mysql_query($query_salida_ventilacion) or die (mysql_error());

																			// SE REALIZA UN CICLO PARA MOSTRAR LAS SALIDAS DE LA VENTILACION CORRESPONDIENTE
																			while ( $row_salida_ventilacion=mysql_fetch_array($resultado_salida_ventilacion) ) 
																			{
																				$id_cat_nivel3 = $row_salida_ventilacion["id_cat_nivel3"];
																				$salida = $row_salida_ventilacion["salida"];
																			?>
																				<input type="hidden" name="id_cat_nivel3" value="<?php echo $id_cat_nivel3; ?>" /> 
																				<ul>
																					<li> <span> <?php echo ucfirst(strtolower($salida)); ?> <a href="eliminar_salida_material.php?id_cat_nivel3=<?php echo $id_cat_nivel3; ?>&id_estilo=<?php echo $id_estilo; ?>"> <img src="../img/delete.png" /> </a> </span> </li>
																				</ul>
																			<?php
																			}
																		?>
																		</li>
																	</ul>
																</li>
															</ul>	
														<?php
														}
													?> 
														</li>
													</ul>
												</li>
											</ul>
										<?php
										}
									?>
							</form>
									</li>
								</ul>
							</li>
						</ul><!-- FIN DIV RED -->
						<form name="form2" action="procesa_modificar_catalogo_molde.php" method="post" class="valida_modificar_catalogo"> 
                        	<table>
                        		<tr>
                        			<td id="alright">
                    					<input name="accion" type="submit" value="Volver" class="fondo_boton" title="Volver" />  
                   					</td>
                          		</tr>
                     		</table>
                    	</form>
					</div><!-- FIN DIV CONTENIDO PROVEEDOR -->
				</div><!-- FIN DIV AREA CONTENIDO 1 -->
			</div><!-- FIN DIV CONTENIDO PAGINA -->
		</div><!-- FIN DIV CONTENIDO COLUMNA 2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html>