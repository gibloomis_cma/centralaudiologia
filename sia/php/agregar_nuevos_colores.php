<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Colores Nuevos </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<link type="text/css" rel="stylesheet" href="../css/colorPicker.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/colorPicker.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
			<div id="contenido_columna2">
						<div class="contenido_pagina">
							<div class="fondo_titulo1">
								<div class="categoria">
									Colores
								</div><!-- FIN DIV CATEGORIA -->
							</div><!-- FIN DIV FONDO TITULO1 -->
								<div class="area_contenido1">
									<br/>
									<?php 
										// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
										include("config.php");
										// SE REALIZA EL QUERY QUE OBTIENE LOS COLORES QUE EXISTEN EN LA BASE DE DATOS
										$query_colores = mysql_query("SELECT id_color,color,r,g,b
																	  FROM colores
																	  ORDER BY id_color ASC") or die (mysql_error());
									?>
									<center>
										<table>
											<tr>
												<th> N° de Color </th>
												<th> Nombre del Color </th>
                                                <th>  </th>
											</tr>
										<?php
											while( $row_color = mysql_fetch_array($query_colores) )
											{
												$id_color = $row_color['id_color'];
												$nombre_color = $row_color['color'];
												$r = $row_color['r'];
												$g = $row_color['g'];
												$b = $row_color['b'];
										?>
											<tr>
												<td style="text-align: center;"> <?php echo $id_color; ?> </td>
												<td> <?php echo utf8_encode(ucwords(strtolower($nombre_color))); ?> </td>
												<td style="text-align: center;"> <input type="text" name="color" id="color" size="5" readonly="readonly" style="border:none; border: 1px #000 solid; background:rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b ?>)"/> </td>
											</tr>
										<?php
											} 
										?>
										</table>
									</center>
									<br/>
									<div class="titulos"> Agregar Nuevo </div>
									<br/>
									<center>
										<form name="form_agregar_nuevo_color" id="form_agregar_nuevo_color" method="post" action="procesa_agregar_nuevos_colores.php" onsubmit="return validaAgregarNuevoColor()">
											<table>
												<tr>
													<td> <label class="textos"> Nombre Color: </label> </td>
													<td> <input type="text" name="txt_nombre_color" id="txt_nombre_color"/> </td>
												</tr>
												<tr>
													<td colspan="2"> <br/> </td>
												</tr>
												<tr>
													<td> <label class="textos"> Seleccione Color: </label> </td>
													<td> <input type="text" name="color_picker" id="color_picker" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/> </td>
												</tr>
												<tr>
													<td colspan="2"> <br/> </td>
												</tr>
												<tr>
													<td colspan="2">
														<p align="right">
															<input type="reset" name="cancelar" id="cancelar" class="fondo_boton" value="Cancelar" title="Cancelar"/>
															<input type="submit" name="accion" id="accion" class="fondo_boton" value="Aceptar" title="Aceptar"/>
														</p>
													</td>
												</tr>
											</table>
										</form>
									</center>
								</div><!-- FIN DIV AREA CONTENIDO 1 -->
						</div><!-- FIN DIV CONTENIDO PAGINA -->
					</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
		</div><!-- FIN DIV COLUMNA2 -->
</body>

</html>
