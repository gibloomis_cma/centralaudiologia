<?php
	error_reporting(0);
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBEN LAS VARIABLES DEL FORMULARIO POR METODO GET
	$sucursal = $_GET['sucursal'];
	$empleado_corte = $_GET['responsable'];
	$fecha_corte_mysql = $_GET['fecha_corte'];
	$fecha_separada = explode("-", $fecha_corte_mysql);
	$fecha_corte_mysql = $fecha_separada[0]."-".$fecha_separada[2]."-".$fecha_separada[1];
	$fecha_corte_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
	$folio_corte = $_GET['folio_corte'];
	$folio_corte_penultimo = $folio_corte - 1;

	// SE REALIZA QUERY QUE OBTIENE INFORMACION DEL CORTE DE VENTA A REIMPRIMIR
	$query_informacion_actual = "SELECT id_registro_venta
								 FROM corte_ventas_diario
								 WHERE folio_corte = '$folio_corte'
								 AND id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_informacion_actual = mysql_query($query_informacion_actual) or die(mysql_error());
	$row_actual = mysql_fetch_array($resultado_informacion_actual);
	$registro_venta_actual = $row_actual['id_registro_venta'];

	// SE REALIZA QUERY QUE OBTIENE INFORMACION DE UN CORTE DE VENTAS ANTERIOR
	$query_informacion_corte = "SELECT id_registro_venta
								FROM corte_ventas_diario
								WHERE folio_corte = '$folio_corte_penultimo'
								AND id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY ANTERIOR Y SE OBTIENE EL RESULTADO
	$resultado_informacion_corte = mysql_query($query_informacion_corte) or die(mysql_error());
	$row_informacion_corte = mysql_fetch_array($resultado_informacion_corte);
	$registro_venta_penultimo = $row_informacion_corte['id_registro_venta'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA SUCURSAL DE ACUERDO AL ID DE LA SUCURSAL OBTENIDO
	$query_sucursal = "SELECT nombre
					   FROM sucursales
					   WHERE id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIBLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);

	$nombre_sucursal = $row_sucursal['nombre'];

	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO QUE HIZO LA VENTA
	$query_nombre_empleado = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
							  FROM empleados
							  WHERE id_empleado = '$empleado_corte'";

	// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
	$resultado_nombre_empleado = mysql_query($query_nombre_empleado) or die(mysql_error());

	$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
	$nombre_empleado = $row_nombre_empleado['nombre_empleado'];

	// QUERY QUE OBTIENE DE QUE TICKET A QIE TICKET ES EL CORTE DE VENTA
	$query_ticket_a_ticket = "SELECT MAX(folio_num_venta) AS max_folio, MIN(folio_num_venta) AS min_folio 
							  FROM ventas 
							  WHERE id_registro > '$registro_venta_penultimo' AND id_registro <= '$registro_venta_actual' 
							  AND id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO CORRESPONDIENTE
	$resultado_ticket_a_ticket = mysql_query($query_ticket_a_ticket) or die(mysql_error());
	$row_ticket_a_ticket = mysql_fetch_array($resultado_ticket_a_ticket);
	$folio_max = $row_ticket_a_ticket['max_folio'];
	$folio_min = $row_ticket_a_ticket['min_folio'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Corte de Ventas Diario </title>
	<style>
		body
		{
			font-size:14px;	
			font-family:Arial;
		}

		#cont
		{
			margin:0px auto;
			text-align:center;		
			padding-bottom:5px;
		}

		#impresion
		{
			text-align:center;
			width:220px;
			font-size:14px;	
			font-family:Arial;
		}

		#impresion table
		{
			margin:0px auto;
			font-family:Arial;
			font-size:12px;	
			font-size:10px;
		}

		#impresion table tr th
		{
			font-size:12px;	
			font-family:Arial;
		}

		#impresion table tr td
		{
			text-align:right;
			padding:5px 5px;
			font-size:12px;	
			font-family:Arial;
		}

		span
		{
			text-decoration:underline;
			font-size:14px;
			font-family:Arial;
		}
	</style>
</head>
<body onload="javascript: window.print();">
	<div id="cont">
		<div id="impresion">
			CORTE DIARIO DE VENTAS <br />
			Sucursal <?php echo ucwords(strtolower($nombre_sucursal)); ?> <br/>
			N° Folio de Corte: <?php echo $folio_corte; ?> <br/>
			Fecha de Corte: <?php echo $fecha_corte_normal; ?> <br/> 
			Tickets <?php echo $folio_min. " - " .$folio_max."<br/>"; ?>
			<br/>
			<table>
				<tr>
					<td style="text-align:center; font-family:Arial;"> Cantidad de Articulos </td>
					<td style="text-align:center; font-family:Arial;"> Total </td>
				</tr>
		<?php
			// SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LAS VENTAS REALIZADAS
			$consulta_ventas_diaria = "SELECT folio_num_venta, descuento, total, vendedor 
									   FROM ventas 
									   WHERE fecha >= '$fecha_corte_mysql'
									   AND id_registro > '$registro_venta_penultimo' AND id_registro <= '$registro_venta_actual'
									   AND id_sucursal = '$sucursal'";

			// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO
			$resultado_ventas_diaria = mysql_query($consulta_ventas_diaria) or die(mysql_error());

			// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
			while( $row = mysql_fetch_array($resultado_ventas_diaria) ) 
			{
				$vendedor = $row["vendedor"];

				// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO DEL VENDEDOR
				$consulta_departamento_vendedor = "SELECT id_departamento 
												   FROM empleados 
												   WHERE id_empleado = '$vendedor'";

				// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
				$resultado_departamento_vendedor = mysql_query($consulta_departamento_vendedor) or die(mysql_error());

				// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
				$row_departamento_vendedor = mysql_fetch_array($resultado_departamento_vendedor);

				// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
				$id_departamento_vendedor = $row_departamento_vendedor["id_departamento"];
								
				// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO
				$consulta_sucursal = "SELECT id_sucursal 
									  FROM areas_departamentos
									  WHERE id_departamento = '$id_departamento_vendedor'";

				// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
				$resultado_sucursal = mysql_query($consulta_sucursal) or die(mysql_error());

				// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
				$row_sucursal = mysql_fetch_array($resultado_sucursal);

				// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
				$id_sucursal_venta = $row_sucursal["id_sucursal"];

				// SE VALIDA SI EL ID DE LA SUCURSAL ES IGUAL A 1
				if( $id_sucursal_venta == 1 )
				{
					$folio_num_venta = $row["folio_num_venta"];
					$descuento = $row["descuento"];
					$total = $row["total"];
					$totalTotal += $total;

					$consulta_ventas_diaria2 = "SELECT id_registro, cantidad, costo_unitario, id_categoria, pago_parcial
												FROM descripcion_venta 
												WHERE folio_num_venta = '$folio_num_venta' AND id_sucursal = '$sucursal'";

					$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

					while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
					{
						$categoria = $row2["id_categoria"];
						$id_registro = $row2["id_registro"];
						$costo_unitario = $row2["costo_unitario"];
						$cantidad = $row2["cantidad"];
						$pago_parcial = $row2['pago_parcial'];

						if ( $categoria == 200 && $pago_parcial != 0 ) 
						{
							$sumaTotal = $pago_parcial;
							$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
							$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
						}
						else
						{
							$sumaTotal = ($cantidad * $costo_unitario);
							$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
							$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
						}
						// SE VALIDA A QUE CATEGORIA PERTENECE EL PRODUCTO
						if($categoria == 1)
						{
							$sumaTotal1 += $sumaTotalNueva;
							$cantidad1 += $cantidad;
						}
						if($categoria == 2)
						{
							$sumaTotal2 += $sumaTotalNueva;
							$cantidad2 += $cantidad;	
						}
						if($categoria == 4)
						{
							$sumaTotal3 += $sumaTotalNueva;
							$cantidad3 += $cantidad;
						}
						if($categoria == 100)
						{
							$sumaTotal4 += $sumaTotalNueva;
							$cantidad4 += $cantidad;	
						}
						if($categoria == 200)
						{
							$sumaTotal5 += $sumaTotalNueva;
							$cantidad5 += $cantidad;	
						}
					}
				}
				// SE VALIDA SI EL ID SUCURSAL ES DIFERENTE A 1
				else
				{
					$folio_num_venta = $row["folio_num_venta"];
					$descuento = $row["descuento"];
					$total = $row["total"];
					$totalTotal += $total;
					$consulta_ventas_diaria2 = "SELECT cantidad, costo_unitario, id_categoria,pago_parcial
												FROM descripcion_venta 
												WHERE folio_num_venta = '$folio_num_venta' 
												AND id_sucursal = '$sucursal'";

					$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

					while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
					{
						$categoria = $row2["id_categoria"];
						$id_registro = $row2["id_registro"];
						$costo_unitario = $row2["costo_unitario"];
						$cantidad = $row2["cantidad"];
						$pago_parcial = $row2['pago_parcial'];

						if ( $categoria == 200 && $pago_parcial != 0 ) 
						{
							$sumaTotal = $pago_parcial;
							$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
							$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
						}
						else
						{
							$sumaTotal = ($cantidad * $costo_unitario);
							$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
							$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
						}

						// SE VALIDA A QUE CATEGORIA PERTENECE EL PRODUCTO
						if( $categoria == 1 )
						{
							$sumaTotal1 += $sumaTotalNueva;
							$cantidad1 += $cantidad;
						}
						if( $categoria == 2 )
						{
							$sumaTotal2 += $sumaTotalNueva;
							$cantidad2 += $cantidad;	
						}
						if( $categoria == 4 )
						{
							$sumaTotal3 += $sumaTotalNueva;
							$cantidad3 += $cantidad;
						}
						if( $categoria == 100 )
						{
							$sumaTotal4 += $sumaTotalNueva;
							$cantidad4 += $cantidad;	
						}
						if( $categoria == 200 )
						{
							$sumaTotal5 += $sumaTotalNueva;
							$cantidad5 += $cantidad;	
						}
					} 
				}
			}
		?>
                    	<tr>
                    		<td colspan="2" style="text-align:left; font-family:Arial;"> Refacciones </td>
                    	</tr>
						<tr> 
    						<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad1; ?> </td>
    						<td style="text-align:center; font-family:Arial;"> <?php echo "$ ".number_format($sumaTotal1,2); ?> </td>
    					</tr>
    					<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
    					<tr>
    						<td colspan="2" style="text-align:left; font-family:Arial;"> Baterias </td>
    					</tr>
    					<tr>
    						<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad3; ?> </td>
    						<td style="text-align:center; font-family:Arial;"> <?php echo "$ ".number_format($sumaTotal3,2); ?> </td>
    					</tr>
    					<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left; font-family:Arial;"> Consultas </td>
						</tr>
						<tr>
							<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad2; ?> </td>
							<td style="text-align:center; font-family:Arial;"> <?php echo "$ ".number_format($sumaTotal2,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left; font-family:Arial;"> Reparaciones </td>
						</tr>
						<tr>
							<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad4; ?> </td>
							<td style="text-align:center; font-family:Arial;"> <?php echo "$ ".number_format($sumaTotal4,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left; font-family:Arial;"> Moldes </td>
						</tr>
						<tr>
							<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad5; ?> </td>
							<td style="text-align:center; font-family:Arial;"> <?php echo "$ ".number_format($sumaTotal5,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<?php
								$total_de_articulos = 0;
								$total_de_articulos = $cantidad1 + $cantidad3 + $cantidad2 + $cantidad4 + $cantidad5;
								$total_dinero = $sumaTotal1 + $sumaTotal3 + $sumaTotal2 + $sumaTotal4 + $sumaTotal5;
							?>
							<td style="text-align:center; font-family:Arial;"> Total de Articulos: <br/> <?php echo $total_de_articulos; ?> </td>
        					<td style="text-align:center; font-family:Arial;"> Total: <br/> <?php echo "$".number_format($total_dinero,2); ?>  </td>
    					</tr>
					</table>
					<br/><br/><br/>
					<table>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center; font-family:Arial;"> 
								Responsable <br/>
								<?php echo $nombre_empleado; ?> 
							</td>
						</tr>
					</table>
				</div>
				<p>&nbsp;&nbsp;</p>
			</div>
		</body>
	</html>