<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Refacciones</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
<script src="../js/jquery-1.7.1.js"></script>
<script src="../js/ui/jquery.ui.core.js"></script>
<script src="../js/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../css/themes/base/demos.css">
<script>
	$(function() {
		var dates = $( "#from, #to" ).datepicker({
			defaultDate: "",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
</script>
</head>
<body>
<div id="wrapp">
	<div id="contenido_columna2">
		<div class="contenido_pagina">
    		<div class="fondo_titulo1">
        		<div class="categoria">
            		Refacciones
            	</div>
 			</div><!--Fin de fondo titulo-->	
            <div class="area_contenido1">
            	<div class="contenido_proveedor">
                <br />
                	<div class="titulos">Consulta vale de Refacciones</div>
               	<br />
                	<form name="form" action="consulta_vales_refacciones.php" method="post"> 
						<center>
                        <div class="demo">
							<label for="from" class="textos">Del</label>
							<input type="text" id="from" name="from"/>
							<label for="to" class="textos">Al</label>
							<input type="text" id="to" name="to"/>
						</div><!-- End demo -->
                        </center>
					<p align="center">
                        <input type="submit" name="accion" value="Aceptar" class="fondo_boton"/>
                    </p> 
					</form>
                    </div><!--Fin de contenido proveedor-->
  	<?php
					include("config.php");
					$accion = $_POST["accion"];
					$fechaInicio = $_POST["from"];
					$fecha_inicio_separada = explode("/", $fechaInicio);
					$fechaFinal = $_POST["to"];
					$fecha_final_separada = explode("/", $fechaFinal);
					if($accion == "Aceptar"){
						if(isset($fechaInicio) && $fechaInicio != "" and isset($fechaFinal) && $fechaFinal !=" "){
	?>
							<div class="contenido_proveedor">
                            <center>
                            <table border="1" style="border-collapse:collapse; border-style:solid;" width="100%">
                           		<tr>
                                	<th width="30px" style="font-size:12px;">Nº</th>
                                    <th width="70px" style="font-size:12px;">Fecha</th>
                                	<th width="100px" style="font-size:12px;">Hora</th>
                                    <th width="195px" style="font-size:12px;">Origen / Cantidad / Descripcion</th>
                                    <th width="68px" style="font-size:12px;">Uso</th>
                                    <th width="68px" style="font-size:12px;">Responsable</th>
                                    <th style="font-size:12px;"> Observaciones </th>
                                </tr>
 	<?php
							//include("metodo_cambiar_fecha.php");
							$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
							$fecha_final_mysql = $fecha_final_separada[2]."-".$fecha_final_separada[1]."-".$fecha_final_separada[0];
							$consulta_vales_refacciones = mysql_query("SELECT * FROM vales_refacciones
																					WHERE fecha BETWEEN '".$fecha_inicio_mysql."'
																					 AND '".$fecha_final_mysql."' ORDER BY id_registro DESC")
																					  or die(mysql_error());											
							while($row = mysql_fetch_array($consulta_vales_refacciones)){
								$num_vale = $row["id_registro"];
								$fecha = $row["fecha"];
								$fecha_separada = explode("-", $fecha);
								$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
								$hora = $row["hora"];
								$uso = $row["uso"];
								$responsable = $row["responsable"];
								$observaciones = $row['observaciones'];
								$consulta_responsable = mysql_query("SELECT alias FROM empleados
																						WHERE id_empleado='$responsable'") 
																						or die(mysql_error());														
								$row2 = mysql_fetch_array($consulta_responsable);
								$alias = $row2["alias"];
								$consulta_uso = mysql_query("SELECT uso FROM uso_refacciones
																			WHERE id_uso_refaccion='$uso'") 
																			or die(mysql_error());														
								$row4 = mysql_fetch_array($consulta_uso);
								$uso_descripcion = $row4["uso"];
								$consulta_refaccion = mysql_query("SELECT descripcion FROM base_productos
																						WHERE id_base_producto='$descripcion'") 
																						or die(mysql_error());														
								$row5 = mysql_fetch_array($consulta_refaccion);	
								$descripcion_refaccion = $row5["descripcion"];
	?> 
                                <tr>
                                	<td style="font-size:10px;"><?php echo $num_vale; ?></td>
                                    <td style="font-size:10px;"><?php echo $fecha_normal; ?></td>
                                    <td style="font-size:10px;"><?php echo $hora; ?></td> 
                                    <td style="font-size:10px;">                                  	
	<?php 
									$consulta_descripcion_vale = mysql_query("SELECT codigo , cantidad, origen 
																					FROM descripcion_vale_refacciones
																					WHERE id_vale_refaccion="
																					.$num_vale) or die(mysql_error());
									while($row10 = mysql_fetch_array($consulta_descripcion_vale)){
										$codigo = $row10["codigo"];
										$cantidad = $row10["cantidad"];
										$origen = $row10["origen"];		
										$consulta_descripcion_refaccion = mysql_query("SELECT descripcion 
																							FROM base_productos
																							WHERE id_base_producto=".$codigo) 
																							or die(mysql_error());
										$row6 = mysql_fetch_array($consulta_descripcion_refaccion);
										$descripcion = $row6["descripcion"];
										
										$consulta_almacen = mysql_query("SELECT almacen FROM almacenes 
																							WHERE id_almacen=".$origen)
										 													or die(mysql_error());
										$row11 = mysql_fetch_array($consulta_almacen);
										$almacen = $row11["almacen"];
 										echo "<label class='textos'>".$almacen."</label><br />";
                                   		echo $cantidad." ".$descripcion."<hr>";
									}
	?>
    								</td>
                                    <td style="font-size:10px;"><?php echo $uso_descripcion; ?></td>
                                    <td style="font-size:10px;"><?php echo $alias; ?></td>
                                    <td style="font-size:10px;"> <?php echo ucfirst(strtolower($observaciones)); ?> </td>
                               	</tr>
   	<?php
								}
	?>                   
                            </table>
                            </center>
                            <br />
                            </div><!--Fin de contenido proveedor-->
	<?php
						}else{
	?>
                    		<center>
                            <table style="border-collapse:collapse;">
                            	<tr>
                                    <th width="30px">Nº</th>
                                    <th width="70px">Fecha</th>
                                    <th width="70px">Hora</th>
                                    <th>Origen</th>
                                    <th width="180x">Cantidad/Descripcion</th>
                                    <th width="70px">Uso</th>
                                    <th width="70px">Responsable</th>
                            	</tr>
                            	<tr>
                            		<td colspan="7" style="padding:8px; text-align:center;">
									<?php echo "<label class='textos'>No se encontraron resultados</label>"; ?>
                                	</td>
                            	</tr>
                    		</table>
                            </center>
    <?php
						}
					return;
					} 
	?>   
        	</div><!--Fin de area contenido-->     
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>