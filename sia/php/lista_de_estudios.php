<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Estudios</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Estudios
            </div>
        </div><!--Fin de fondo titulo-->  
        <br />                     
        <div class="area_contenido2">
            <center>
            <form name="forma1" action="guardar_cambios.php" method="post">
            <?php
                include("config.php");
                include("metodo_cambiar_fecha.php");
                $id_cliente = $_GET["id_paciente"];
                $consulta_datos_paciente = mysql_query("SELECT * FROM ficha_identificacion
                                                        WHERE id_cliente=".$id_cliente) 
                                                        or die(mysql_error());
                $row = mysql_fetch_array($consulta_datos_paciente);
                $nombre = $row["nombre"];
                $paterno = $row["paterno"];
                $materno = $row["materno"];
                $calle = $row["calle"];
                $num_exterior = $row["num_exterior"];
                $num_interior = $row["num_interior"];
                $codigo_postal = $row["codigo_postal"];
                $colonia = $row["colonia"];
                $edad = $row["edad"];
                $ocupacion = $row["ocupacion"];
                $id_estado = $row["id_estado"];
                $id_ciudad = $row["id_ciudad"];
                $estado_civil = $row["estado_civil"];
                if($num_interior!=""){
					$num_interior="-".ucwords($num_interior);
				}
                $consulta_anamnesis = mysql_query("SELECT COUNT(id_anamnesis) 
                                                    FROM anamnesis 
                                                    WHERE id_cliente=".$id_cliente) 
                                                    or die(mysql_error());
                $row2 = mysql_fetch_array($consulta_anamnesis);
                
                $anamnesis = $row2["COUNT(id_anamnesis)"];	
            ?>
            <!--<div class="contenido_proveedor">-->
            <table>
                <tr>
                    <th colspan="2">
                        Ficha de Identificación
                    </th>
                </tr><tr>
                    <td id="alright">
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <label class="textos">Nombre:  </label>
                    </td><td>
                        <?php echo $nombre." ".$paterno." ".$materno; ?>
                    </td>                                 
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Dirección: </label>
                    </td><td>
                        <?php echo "C. ".$calle." #".$num_exterior.$num_interior." Col. ". $colonia." C.P. ".$codigo_postal; ?>
                    </td>
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Ubicación: </label>
                    </td><td>
                    <?php
                        $consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades 
                                                                        WHERE id_estado=".$id_estado." 
                                                                        AND id_ciudad=".$id_ciudad) 
                                                                        or die(mysql_error());
                        $row4 = mysql_fetch_array($consulta_ciudad);
                        $ciudad = ucwords(strtolower($row4["ciudad"]));
                        $consulta_estados = mysql_query("SELECT estado FROM estados 
                                                        WHERE id_estado=".$id_estado)or die(mysql_error());
                        $row3 = mysql_fetch_array($consulta_estados); 
                        $estado = utf8_encode($row3["estado"]);
                        echo utf8_encode($ciudad).", ".$estado;
                    ?>
                    </td>
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Edad: </label>
                    </td><td>
                        <?php echo $edad." Años"; ?>
                    </td>
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Ocupacion: </label>
                    </td><td>
                        <?php echo $ocupacion; ?>
                    </td>
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Estado civil: </label>
                    </td><td>
                        <?php echo $estado_civil; ?>
                    </td>
                </tr><tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr><tr>
                    <td colspan="2" style="text-align:center">
                        <label class="textos">Forma de Contacto:</label>
                    </td>
                </tr><tr>
                    <td colspan="2">
                        <table>	
                        <?php
                            $consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
                                                                    WHERE id_cliente=".$id_cliente)
                                                                    or die(mysql_error());

                            while($row2 = mysql_fetch_array($consulta_forma_contacto)){
                                $id_contactos_cliente = $row2["id_contactos_cliente"];
                                $tipo = $row2["tipo"];
                                $descripcion = $row2["descripcion"];
                                $titular = $row2["titular"];
                        ?>
                            <tr>
                                <td id="alright">
                                    <label class="textos">Tipo: </label>
                                </td><td>
                                    <?php echo $tipo; ?>
                                </td><td id="alright">
                                    <label class="textos">Descripcion: </label>
                                </td><td>
                                    <?php echo $descripcion; ?>
                                </td><td id="alright">
                                    <label class="textos">Titular: </label>
                                </td><td>
                                    <?php echo $titular; ?>
                                </td>
                            </tr>
                        <?php 
                            }
                        ?>
                        </table>
                    </td>
                </tr><tr>
                    <td colspan="2" id="alright">
                        <input name="accion" type="submit" value="Volver" class="fondo_boton" />
                        <input name="accion" type="submit" value="Editar" class="fondo_boton" />
                    </td>
                </tr><tr>
                    <td colspan="2" style="text-align:center">
                    <?php 
                        if($anamnesis == 0){
                            $path="cuestionario_anamnesis.php?id_paciente=".$id_cliente;
                            $tit="Hacer Anamnesis";
                        }else{
                            $path="ver_anamnesis.php?id_paciente=".$id_cliente;
                            $tit="Ver la Anamnesis";
                        }
                    ?>
                        <input type="button" value="<?php echo $tit; ?>" class="fondo_boton"
                        onclick="window.location.href='<?php echo $path; ?>'" /><br /><br />
                    </td>
                </tr>
                <?php
                    $contador = mysql_query("SELECT COUNT(id_registro) FROM historial_clinico
                                                    WHERE id_cliente=".$id_cliente)
                                                    or die(mysql_error());
                    $row_contador = mysql_fetch_array($contador);
                    $contador_estudios = $row_contador["COUNT(id_registro)"];
                    if($contador_estudios !=0){
                ?>
                <tr>
                    <th colspan="2">
                        Estudios
                    </th>
                </tr><tr>
                    <th>N°</th>
                    <th>Fecha</th>
                </tr>
                <?php
                        $consulta_estudios = mysql_query("SELECT * FROM historial_clinico 
                                                                WHERE id_cliente=".$id_cliente) 
                                                                or die(mysql_error());
                        $n=0;
                        while($row3 = mysql_fetch_array($consulta_estudios)){
                        $notas_clinicas = $row3["id_nota_clinica"];
                        $fecha = $row3["fecha"];
                        $fecha_normal = cambiaf_a_normal($fecha);
                        $n++;
                ?>
                <tr>                          	
                    <td style="text-align:center">                                	
                        <label class="textos">
                            <?php echo $n." [Folio: ".$notas_clinicas."]"; ?>
                        </label>
                    </td><td style="text-align:center">
                        <label class="textos"><?php echo $fecha_normal; ?></label>
                    </td>
                </tr>
                <?php
                        }
                    }
                ?>                                
            </table>                        
            </form>
            </center>
        </div><!-- Fin de area contenido -->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido clolumna 2-->
</body>
</html>