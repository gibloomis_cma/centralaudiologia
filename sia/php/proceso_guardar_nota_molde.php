<?php
	// SE DECLARA LA OPCION PARA NO MOSTRAR NOTICE CUANDO LAS VARIABLES NO SE ENCUENTREN CARGADAS CON DATOS
	error_reporting(0);

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
	
	// SE RECIBEN LOS VALORES DEL FORMULARIO DE REGISTRO DE REPARACION
	$folio_num_molde = $_POST["folio_molde"];
	$nota_anterior = $_POST['nota_anterior'];
	$observaciones = $_POST["observaciones"];
	$hora = $_POST["hora_real"];
	$fecha_entrada = $_POST["fecha"];
	$fecha_entrada_separada = explode("/", $fecha_entrada);
	$fecha_entrada_mysql = $fecha_entrada_separada[2]."-".$fecha_entrada_separada[1]."-".$fecha_entrada_separada[0];
	
	// SE RECIBE LA VARIABLE DE ADAPTACION
	$adaptacion = $_POST['adaptacion'];

	// SE RECIBE LA VARIABLE DE REPOSICION
	$reposicion = $_POST['reposicion'];

	// SE RECIBE EL ID DEL EMPLEADO Y DEL CLIENTE
	$id_cliente = $_POST["id_cliente"];
	$id_empleado = $_POST["id_empleado"];
	
	// SE RECIBE LA VARIABLE PARA SABER SI ESTA REGISTRADO O NO EL PACIENTE
	$variable = $_POST["variable"];
	
	// VARIABLE QUE SE RECIBE PARA SABER SI LOS DATOS DEL PACIENTE FUERON MODIFICADOS O NO
	$variable_sin_editar = $_POST["variable_sin_editar"];
	
	// SE RECIBEN LOS DATOS DEL MOLDE
	$lado_oido = $_POST["lado_oido"];
	$id_estilo = $_POST["estilos"];
	$id_material = $_POST["materiales"];
	$id_color = $_POST["colores"];
	$id_ventilacion = $_POST["ventilaciones"];
	$id_salida = $_POST["salidas"];
	$costo = $_POST["costo_total_molde"];
	
	// SE RECIBEN LOS DATOS DEL PACIENTE
	$id_cliente = $_POST["id_cliente"];
	$nombre = $_POST["nombre"];
	$paterno = $_POST["paterno"];
	$materno = $_POST["materno"];
	$calle = $_POST["calle"];
	$num_exterior = $_POST["num_exterior"];
	$num_interior = $_POST["num_interior"];
	$codigo_postal = $_POST["codigo_postal"];
	$colonia = $_POST["colonia"];
	$id_estado = $_POST["id_estado"];
	$id_ciudad = $_POST["id_municipio"];
	$ciudad = $_POST['ciudad'];
	$edad = $_POST["edad"];
	$coupacion = $_POST["ocupacion"];
	$estado_civil = $_POST["estado_civil"];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	$fecha_nacimiento_separada = explode("/", $fecha_nacimiento);
	$fecha_nacimiento_mysql = $fecha_nacimiento_separada[2]."-".$fecha_nacimiento_separada[1]."-".$fecha_nacimiento_separada[0];
	
	// SE RECIBEN LOS DATOS DEL CONTACTO DEL PACIENTE
	$id_contacto = $_POST["id_contactos_cliente"];
	$telefono = $_POST["descripcion"];
	$titular = $_POST["titular"];
	$tipo = $_POST["tipo_telefono"];
	
	// SE REALIZA UN QUERY QUE OBTIENE EL DEPARTAMENTO DEL EMPLEADO QUE ENTRO EN EL SISTEMA
	$consulta_departamento_empleado = mysql_query("SELECT id_departamento 
												   FROM empleados 
												   WHERE id_empleado=".$id_empleado) or die(mysql_error());

	$row_departamento_empleado = mysql_fetch_array($consulta_departamento_empleado);
	$id_departamento_empleado = $row_departamento_empleado["id_departamento"];

	// SE REALIZA UN QUERY QUE OBTIENE LA SUCURSAL DEL DEPARTAMENTO
	$consulta_sucursal = mysql_query("SELECT id_sucursal 
									  FROM areas_departamentos 
									  WHERE id_departamento=".$id_departamento_empleado) or die(mysql_error());
	
	$row_sucursal = mysql_fetch_array($consulta_sucursal);
	$id_sucursal = $row_sucursal["id_sucursal"];

	// SE VALIDA DE QUE SUCURSAL ES EL MOLDE
	if( $id_sucursal == 1 )
	{
		$id_estatus_molde = 1;	
	}
	else
	{
		$id_estatus_molde = 4;
	}

	// SE VALIDA SI EL USUARIO OPRIMIO EL BOTON GUARDAR MOLDE
	if($_POST["guardar_molde"] == "Guardar Molde")
	{
		// SE VALIDA SI EL CLIENTE ESTA REGISTRADO
		if( $variable == 1 )
		{
			if( $variable_sin_editar == 1 )
			{
				if ( $id_cliente != 0 ) 
				{
					// SE REALIZA QUERY QUE VERIFICA SI EXISTEN CONTACTOS DEL PACIENTE A REGISTRAR
					$query_contactos_paciente = "SELECT COUNT(id_contactos_cliente) AS contacto
												 FROM contactos_clientes
												 WHERE id_cliente = '$id_cliente'";
					
					// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
					$resultado_contactos_paciente = mysql_query($query_contactos_paciente) or die(mysql_error());
					$row_contactos_paciente = mysql_fetch_array($resultado_contactos_paciente);
					$total_contactos = $row_contactos_paciente['contacto'];
					
					// SE VALIDA SI EL PACIENTE TIENE CONTACTOS REGISTRADOS
					if ( $total_contactos > 0 ) 
					{
						// SE REALIZA UN QUERY QUE OBTIENE LOS DATOS DEL PACIENTE
						$consulta_datos_paciente = "SELECT nombre, paterno, materno, calle, num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad, tipo, descripcion,titular
													FROM ficha_identificacion, contactos_clientes
													WHERE ficha_identificacion.id_cliente = '$id_cliente' 
													AND contactos_clientes.id_cliente = ficha_identificacion.id_cliente";

						// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
						$resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());

						// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
						$row_datos_paciente=mysql_fetch_array($resultado_datos_paciente);
						$nombre_paciente = $row_datos_paciente["nombre"];
						$paterno_paciente = $row_datos_paciente["paterno"];
						$materno_paciente = $row_datos_paciente["materno"];
						$calle_paciente = $row_datos_paciente["calle"];
						$num_exterior_paciente = $row_datos_paciente["num_exterior"];
						$num_interior_paciente = $row_datos_paciente["num_interior"];
						$codigo_postal_paciente = $row_datos_paciente["codigo_postal"];
						$colonia_paciente = $row_datos_paciente["colonia"];
						$id_estado_paciente = $row_datos_paciente["id_estado"];
						$id_ciudad_paciente = $row_datos_paciente["id_ciudad"];
						$tipo = $row_datos_paciente["tipo"];
						$descripcion = $row_datos_paciente["descripcion"];
						$titular = $row_datos_paciente["titular"];
					}
					else
					{
						// SE REALIZA UN QUERY QUE OBTIENE LOS DATOS DEL PACIENTE
						$consulta_datos_paciente = "SELECT nombre, paterno, materno, calle, num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad
													FROM ficha_identificacion
													WHERE id_cliente = '$id_cliente'";

						// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
						$resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());

						// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
						$row_datos_paciente=mysql_fetch_array($resultado_datos_paciente);
						$nombre_paciente = $row_datos_paciente["nombre"];
						$paterno_paciente = $row_datos_paciente["paterno"];
						$materno_paciente = $row_datos_paciente["materno"];
						$calle_paciente = $row_datos_paciente["calle"];
						$num_exterior_paciente = $row_datos_paciente["num_exterior"];
						$num_interior_paciente = $row_datos_paciente["num_interior"];
						$codigo_postal_paciente = $row_datos_paciente["codigo_postal"];
						$colonia_paciente = $row_datos_paciente["colonia"];
						$id_estado_paciente = $row_datos_paciente["id_estado"];
						$id_ciudad_paciente = $row_datos_paciente["id_ciudad"];
						$tipo = "";
						$descripcion = "";
						$titular = "";
					}
				}
				else
				{
					// SE REALIZA UN QUERY QUE OBTIENE LOS DATOS DEL PACIENTE
					$consulta_datos_paciente = "SELECT nombre, paterno, materno, calle, num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad, ciudad, tipo, descripcion,titular,fecha_nacimiento
												FROM moldes
												WHERE folio_num_molde = '$nota_anterior'";

					// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
					$resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());

					// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
					$row_datos_paciente=mysql_fetch_array($resultado_datos_paciente);
					$nombre_paciente = $row_datos_paciente["nombre"];
					$paterno_paciente = $row_datos_paciente["paterno"];
					$materno_paciente = $row_datos_paciente["materno"];
					$calle_paciente = $row_datos_paciente["calle"];
					$num_exterior_paciente = $row_datos_paciente["num_exterior"];
					$num_interior_paciente = $row_datos_paciente["num_interior"];
					$codigo_postal_paciente = $row_datos_paciente["codigo_postal"];
					$colonia_paciente = $row_datos_paciente["colonia"];
					$id_estado_paciente = $row_datos_paciente["id_estado"];
					$id_ciudad_paciente = $row_datos_paciente["id_ciudad"];
					$tipo = $row_datos_paciente["tipo"];
					$descripcion = $row_datos_paciente["descripcion"];
					$titular = $row_datos_paciente["titular"];
					$fecha_nacimiento_anterior = $row_datos_paciente['fecha_nacimiento'];
				}
				/* actualizar la variable folio por si alguien mas registro una nota antes*/
				$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_num_molde+1) FROM moldes"));
				$folio_num_molde = $act_folio[0];
				
				// SE REALIZA UN QUERY TIPO INSERT PARA GUARDAR LOS DATOS DEL MOLDE
				mysql_query("INSERT INTO moldes(folio_num_molde, id_cliente, nombre, paterno, materno, tipo, descripcion, titular, calle, num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad, fecha_entrada, lado_oido, id_estilo, id_material, id_color, id_ventilacion, id_salida, costo, id_estatus_moldes, observaciones, adaptacion, reposicion,fecha_nacimiento)
							 VALUES('".$folio_num_molde."','".$id_cliente."','".$nombre_paciente."','".$paterno_paciente."','".$materno_paciente."','".$tipo."','".$descripcion."','".$titular."','".$calle_paciente."','".$num_exterior_paciente."','".$num_interior_paciente."','".$codigo_postal_paciente."','".$colonia_paciente."','".$id_estado_paciente."','".$id_ciudad_paciente."','".$fecha_entrada_mysql."','".$lado_oido."','".$id_estilo."','".$id_material."','".$id_color."','".$id_ventilacion."','".$id_salida."','".$costo."','".$id_estatus_molde."','".$observaciones."','".$adaptacion."','".$reposicion."','".$fecha_nacimiento_anterior."')") or die(mysql_error());
				
				// SE REALIZA UN QUERY TIPO INSERT PARA GUARDAR EL MOVIMIENTO DEL MOLDE
				mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora,id_estatus_moldes, id_empleado)
							 VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."','".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			?>	
				<!-- SE REALIZA UN ALERT PARA INFORMARLE AL USUARIO LA ACCION REALIZADA -->		
    			<script language='javascript' type='text/javascript'> 
					alert('Se guardo exitosamente la nota del Molde N°: <?php echo $folio_num_molde; ?>');
					window.open("formato_moldes.php?folio_num_molde=<?php echo $folio_num_molde; ?>", "Nota de Molde", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");
					window.location.href = 'lista_moldes.php';
				</script>
			<?php
			}
			// SE VALIDA SI LOS DATOS DEL PACIENTE FUERON MODIFICADOS
			else
			{
				// SE REALIZA UN QUERY TIPO UPDATE PARA ALMACENAR LOS CAMBIOS HECHOS EN LOS DATOS DEL PACIENTE
				mysql_query("UPDATE ficha_identificacion 
							 SET nombre = '$nombre', paterno = '$paterno', materno = '$materno', calle = '$calle', num_exterior = '$num_exterior', num_interior = '$num_interior', codigo_postal = '$codigo_postal', colonia = '$colonia', id_estado = '$id_estado', id_ciudad = '$id_ciudad', edad = '$edad', ocupacion = '$ocupacion', estado_civil = '$estado_civil'
							 WHERE id_cliente=".$id_cliente) or die(mysql_error());

				// SE REALIZA UN CICLO PARA EDITAR LOS DATOS DE CONTACTO DEL PACIENTE
				for( $i = 0; $i <= count($telefono); $i++ )
				{
					$telefono[$i];
					$tipo[$i];
					$titular[$i];
					$id_contacto[$i];

					// SE REALIZA EL QUERY DEL UPDATE PARA ALMACENAR LA INFORMACION DE CONTACTOS
					mysql_query("UPDATE contactos_clientes 
								 SET tipo= '".$tipo[$i]."', descripcion= '".$telefono[$i]."', titular= '".$titular[$i]."'
								 WHERE id_cliente= '".$id_cliente."'
								       AND id_contactos_cliente= '".$id_contacto[$i]."'") or die(mysql_error());

					$tel = $telefono[$i];
					$tip = $tipo[$i];
					$titu = $titular[$i];
				}
				/* actualizar la variable folio por si alguien mas registro una nota antes*/
				$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_num_molde+1) FROM moldes"));
				$folio_num_molde = $act_folio[0];

				// SE REALIZA UN QUERY EL TIPO INSERT PARA GUARDAR LOS DATOS DEL MOLDE
				mysql_query("INSERT INTO moldes(folio_num_molde, id_cliente, nombre, paterno, materno, tipo, descripcion, titular, calle, num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad, fecha_entrada, lado_oido, id_estilo, id_material, id_color, id_ventilacion, id_salida, costo, id_estatus_moldes, observaciones, adaptacion, reposicion)
							 VALUES('".$folio_num_molde."','".$id_cliente."','".$nombre."','".$paterno."','".$materno."','".$tip."','".$tel."','".$titu."','".$calle."','".$num_exterior."','".$num_interior."','".$codigo_postal."','".$colonia."','".$id_estado."','".$id_ciudad."','".$fecha_entrada_mysql."','".$lado_oido."','".$id_estilo."','".$id_material."','".$id_color."','".$id_ventilacion."','".$id_salida."','".$costo."','".$id_estatus_molde."','".$observaciones."','".$adaptacion."','".$reposicion."')") or die(mysql_error());

				// SE REALIZA UN QUERY DEL TIPO INSERT PARA GUARDAR LOS MOVIMIENTOS DEL MOLDE				
				mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora, id_estatus_moldes, id_empleado)
							 VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."','".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
			?>
				<!-- SE MUESTRA UN ALERT PARA MOSTRARLE AL USUARIO LA ACCION REALIZADA -->
    			<script language='javascript' type='text/javascript'> 
					alert('Se guardo exitosamente la nota del Molde N°: <?php echo $folio_num_molde; ?>');
					window.open("formato_moldes.php?folio_num_molde=<?php echo $folio_num_molde; ?>", "Nota de Molde", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");
					window.location.href = 'lista_moldes.php';
				</script>
			<?php 
			}
		}
	}
	// SI EL PACIENTE NO SE ENCUENTRA REGISTRADO SE REALIZA EL SIGUIENTE PROCEDIMIENTO
	elseif ( $_POST['accion'] == "Guardar Reparacion" && $variable == 2 ) 
	{
		/* actualizar la variable folio por si alguien mas registro una nota antes*/
		$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_num_molde+1) FROM moldes"));
		$folio_num_molde = $act_folio[0];
		
		// SE REALIZA UN QUERY TIPO INSERT PARA GUARDAR LOS DATOS DEL PACIENTE Y DEL MOLDE
		mysql_query("INSERT INTO moldes(folio_num_molde, id_cliente, nombre, paterno, materno, tipo, descripcion, titular, calle, num_exterior, num_interior, colonia, id_estado, id_ciudad, ciudad, fecha_entrada, lado_oido, id_estilo, id_material, id_color, id_ventilacion, id_salida, costo, id_estatus_moldes, observaciones, fecha_nacimiento)
					 VALUES('".$folio_num_molde."','".$id_cliente."','".$nombre."','".$paterno."','".$materno."','".$tipo."','".$telefono."','".$titular."','".$calle."','".$num_exterior."','".$num_interior."','".$colonia."','".$id_estado."','".$id_ciudad."','".$ciudad."','".$fecha_entrada_mysql."','".$lado_oido."','".$id_estilo."','".$id_material."','".$id_color."','".$id_ventilacion."','".$id_salida."','".$costo."','".$id_estatus_molde."','".$observaciones."','".$fecha_nacimiento_mysql."')") or die(mysql_error());

		// SE REALIZA UN QUERY DEL TIPO INSERT PARA GUARDAR LOS MOVIMIENTOS DEL MOLDE				
		mysql_query("INSERT INTO movimientos_moldes(folio_num_molde, id_estado_movimiento, fecha, hora, id_estatus_moldes, id_empleado)
					 VALUES('".$folio_num_molde."','2','".$fecha_entrada_mysql."','".$hora."','".$id_estatus_molde."','".$id_empleado."')") or die(mysql_error());
	?>
		<!-- SE MUESTRA UN ALERT PARA INFORMARLE AL USUARIO QUE LA ACCION SE REALIZO CORRECTAMENTE -->
		<script language='javascript' type='text/javascript'> 
			alert('Se guardo exitosamente la nota del Molde N°: <?php echo $folio_num_molde; ?>');
			window.open("formato_moldes.php?folio_num_molde=<?php echo $folio_num_molde; ?>", "Nota de Molde", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");
			window.location.href = 'lista_moldes.php';
		</script>
	<?php
	}
?>