<?php
error_reporting(0);
	include("config.php");
	include("metodo_cambiar_fecha.php");
		
	$accion = $_POST["accion"];
	$folio_reparacion = $_POST["folio_reparacion"];
	$variable = $_POST["variable"];
	$variable_sin_editar = $_POST["variable_sin_editar"];
		
	/* Datos de paciente*/ 
	$id_cliente = $_POST["id_cliente"];
	$nombre = $_POST["nombre"];
	$paterno = $_POST["paterno"];
	$materno = $_POST["materno"];
	$calle = $_POST["calle"];
	$num_exterior = $_POST["num_exterior"];
	$num_interior = $_POST["num_interior"];
	$codigo_postal = $_POST["codigo_postal"];
	$colonia = $_POST["colonia"];
	$id_estado = $_POST["id_estado"];
	$id_ciudad = $_POST["id_municipio"];
	$edad = $_POST["edad"];
	$coupacion = $_POST["ocupacion"];
	$estado_civil = $_POST["estado_civil"];
	
	/* Datos del apararato */
	$fecha = $_POST["fecha"];
	$fecha_entrada = cambiaf_a_mysql($fecha);
	$descripcion = $_POST["problema"];
	
	$hora = $_POST["hora"];
	
	$id_empleado = $_POST["id_empleado"];	
	/* Modelos segun las opciones */
	$modelos = $_POST["modelos"];
	$modelo = $_POST["modelo"];
	$marca = $_POST["marca"];
	$marca_escritos = $_POST["marca_escritos"];
		
	/* Datos de contacto Paciente */
	$id_contacto = $_POST["id_contactos_cliente"];
	$telefono = $_POST["descripcion"];
	$titular = $_POST["titular"];
	$tipo = $_POST["tipo_telefono"];		
	
	/* Datos del accesorio y problemas del aparato */
	$estuche = $_POST["estuche"];
	$molde = $_POST["molde"];
	$pila = $_POST["pila"];
	$no_se_oye = $_POST["no_se_oye"];
	$portapila = $_POST["portapila"];
	$circuito = $_POST["circuito"];
	$se_oye_dis = $_POST["se_oye_dis"];
	$microfono = $_POST["microfono"];
	$tono = $_POST["tono"];
	$falla = $_POST["falla"];
	$receptor = $_POST["receptor"];
	$caja = $_POST["caja"];
	$gasta_pila = $_POST["gasta_pila"];
	$control_volumen = $_POST["control_volumen"];
	$contactos_pila = $_POST["contactos_pila"];
	$cayo_piso = $_POST["cayo_piso"];
	$switch = $_POST["switch"];
	$cambio_tubo = $_POST["cambio_tubo"];
	$diagnostico = $_POST["diagnostico"];
	$bobina = $_POST["bobina"];
	$cableado = $_POST["cableado"];
	$limpieza = $_POST["limpieza"];
	$garantia_adaptacion = $_POST['txt_adaptacion'];
	$garantia_reparacion = $_POST['txt_reparacion'];
	$garantia_venta = $_POST['txt_venta'];
	$folio_anterior = $_POST['folio_anterior'];
	
	/* Consulta el departamento del empleado que entro en el sistema */
	$consulta_departamento_empleado=mysql_query("SELECT id_departamento FROM empleados 
																		WHERE id_empleado=".$id_empleado)
																		or die('Obtener Departamento');
	$row_departamento_empleado=mysql_fetch_array($consulta_departamento_empleado);
	$id_departamento_empleado=$row_departamento_empleado["id_departamento"];
	/* Consulta la sucursal del departamento */
	$consulta_sucursal=mysql_query("SELECT id_sucursal FROM areas_departamentos 
														WHERE id_departamento=".$id_departamento_empleado)
														or die('Obtener Sucursar');
	$row_sucursal=mysql_fetch_array($consulta_sucursal);
	$id_sucursal=$row_sucursal["id_sucursal"];
	if($id_sucursal==1){
		$id_estatus_reparacion = 15;	
	}else{
		$id_estatus_reparacion = 16;
	}
	if( $accion == "Guardar Reparacion" )
	{
		/* Ya esta registrado el paciente que viene a la reparacion */
		if($variable == "1")
		{
			/* Si modelos es diferente de 0, entonces se hace lo siguiente */
			if($modelos != 0)
			{
				/* Saca los datos del auxiliar */
				$modelo_paciente = explode("-",$modelos);
				$id_modelo = $modelo_paciente[0];
				$num_serie = $modelo_paciente[1];
			/* Si modelo es diferente de 0, entonces se hace lo siguiente */
			}
			elseif($modelo != 0 && $_POST["num_serie_escritos1"] != "")
			{
				$id_modelo = $modelo;
				$num_serie = $_POST["num_serie_escritos1"];
			/* Si marca es diferente de 0, entonces se hace lo siguiente */
			}
			elseif($marca != 0 && $_POST["tipo_aparato"] != 0 && $_POST["modelos_escritos"] != "" && $_POST["num_serie_escritos2"] !="")
			{
				$tipo_aparato = $_POST["tipo_aparato"];
				$modelos_escritos = $_POST["modelos_escritos"];
				$num_serie = $_POST["num_serie_escritos2"];
				/* Guarda el nuevo modelo en la tabla modelos */
				mysql_query("INSERT INTO modelos_2(id_subcategoria, modelo, id_tipo_modelo)
										VALUES('".$marca."','".$modelos_escritos."','".$tipo_aparato."')")
										or die('Guardar Modelo');
				/* Se saca el ultimo id modelo registrado */
				$id_modelo_nuevo=mysql_insert_id();
				/* Se guarda en la base productos el nuevo modelo, pero en estado Inactivo */
				mysql_query("INSERT INTO base_productos_2(id_categoria, id_subcategoria, id_articulo, descripcion, activo)
												VALUES('5','".$marca."','".$id_modelo_nuevo."','".$modelos_escritos."','No')")
												or die('Guardar en Base Productos');
				/* El nuevo id del modelo es: */
				$id_modelo=mysql_insert_id();
			}elseif($_POST["marca_escritos"] != "" && $_POST["tipo_aparato2"] != 0 && $_POST["modelos_escritos2"] != "" && $_POST["num_serie_escritos3"] != ""){
				$marca_escritos = $_POST["marca_escritos"];
				$tipo_aparato = $_POST["tipo_aparato2"];
				$modelos_escritos = $_POST["modelos_escritos2"];
				$num_serie = $_POST["num_serie_escritos3"];
				/* Guarda la nueva marca en la tabla subcategorias_productos */
				mysql_query("INSERT INTO subcategorias_productos_2(id_categoria, subcategoria)
														VALUES('5','".$marca_escritos."')")or die('Guardar Subcategoria');
				/* Se saca el ultimo id subcategoria registrado de categoria 5 */
				$id_subcategoria_nueva=mysql_insert_id();
				/* Guarda el nuevo modelo en la tabla modelos */
				mysql_query("INSERT INTO modelos_2(id_subcategoria, modelo, id_tipo_modelo)
										VALUES('".$id_subcategoria_nueva."','".$modelos_escritos."','".$tipo_aparato."')")
										or die('Guardar Modelo 2');
				/* Se saca el ultimo id modelo registrado */
				$id_modelo_nuevo=mysql_insert_id();
				/* Se guarda en la base productos el nuevo modelo, pero en estado Inactivo */
				mysql_query("INSERT INTO base_productos_2(id_categoria, id_subcategoria, id_articulo, descripcion, activo)
												VALUES('5','".$id_subcategoria_nueva."','".$id_modelo_nuevo."',
												'".$modelos_escritos."','No')")or die('Guardar en Base Productos 2');
				/* El nuevo id del modelo es: */
				$id_modelo=mysql_insert_id();
			}
			/* Si no se editaron los campos del paciente, hay que consultarlos*/
			if($variable_sin_editar == 1)
			{
				if ( $id_cliente != 0 )
				{
					/* Consulta datos del paciente */
					$consulta_datos_paciente = mysql_query("SELECT nombre, paterno, materno, calle, num_exterior, num_interior,codigo_postal, colonia, id_estado, id_ciudad
															FROM ficha_identificacion
															WHERE id_cliente=".$id_cliente) or die('Consultar Ficha de Identificacion');
				}
				else
				{
					/* Consulta datos del paciente */
					$consulta_datos_paciente = mysql_query("SELECT nombre, paterno, materno, calle, num_exterior, num_interior,codigo_postal, colonia, id_estado, id_ciudad
															FROM reparaciones
															WHERE folio_num_reparacion = '$folio_anterior'") or die('Consultar Reparaciones del Paciente');
				}								

				
				$row_datos_paciente=mysql_fetch_array($consulta_datos_paciente);
				$nombre_paciente = $row_datos_paciente["nombre"];
				$paterno_paciente = $row_datos_paciente["paterno"];
				$materno_paciente = $row_datos_paciente["materno"];
				$calle_paciente = $row_datos_paciente["calle"];
				$num_exterior_paciente = $row_datos_paciente["num_exterior"];
				$num_interior_paciente = $row_datos_paciente["num_interior"];
				$codigo_postal_paciente = $row_datos_paciente["codigo_postal"];
				$colonia_paciente = $row_datos_paciente["colonia"];
				$id_estado_paciente = $row_datos_paciente["id_estado"];
				$id_ciudad_paciente = $row_datos_paciente["id_ciudad"];
				
				/* Guardar los datos de la reparacion */ 
				mysql_query("INSERT INTO reparaciones(folio_num_reparacion, id_cliente, nombre, paterno, materno, calle,
													  num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad,
													  fecha_entrada, id_modelo, num_serie, id_estatus_reparaciones,
													  descripcion_problema, reparacion, adaptacion, venta)
												VALUES('".$folio_reparacion."','".$id_cliente."','".$nombre_paciente."',
														'".$paterno_paciente."','".$materno_paciente."',
														'".$calle_paciente."','".$num_exterior_paciente."',
														'".$num_interior_paciente."','".$codigo_postal_paciente."',
														'".$colonia_paciente."','".$id_estado_paciente."',
														'".$id_ciudad_paciente."','".$fecha_entrada."','".$id_modelo."',
														'".$num_serie."','".$id_estatus_reparacion."','".$descripcion."','".$garantia_reparacion."','".$garantia_adaptacion."','".$garantia_venta."')")
														or die('Guardar Reparacion del Paciente');
				/* actualizar la variable folio por si alguien mas registro una nota antes*/
				$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_nota_reparacion+1) FROM folios_num_nota_reparacion"));
				$folio_reparacion = $act_folio[0];
				
				/*Guardar el numero de folio de la reparacion */									
				mysql_query("INSERT INTO folios_num_nota_reparacion
							VALUES('1', ".$folio_reparacion.")") or die('Sumar Folio');
							
				/* Se guardan los accesorios dejados y problemas del Auxiliar */
				mysql_query("INSERT INTO accesorios_problemas(folio_num_reparacion, estuche, molde, pila, no_se_oye,
															 portapila, circuito, se_oye_dis, microfono, tono, falla, receptor,
															 caja, gasta_pila, control_volumen, contactos_pila, cayo_piso,
															 switch, cambio_tubo, diagnostico, bobina, cableado, limpieza)
													VALUES('".$folio_reparacion."','".$estuche."','".$molde."','".$pila."',
															'".$no_se_oye."','".$portapila."','".$circuito."',
															'".$se_oye_dis."','".$microfono."','".$tono."','".$falla."',
															'".$receptor."','".$caja."','".$gasta_pila."',
															'".$control_volumen."','".$contactos_pila."','".$cayo_piso."',
															'".$switch."','".$cambio_tubo."','".$diagnostico."','".$bobina."',
															'".$cableado."','".$limpieza."')")or die(mysql_error());
				/* Movimiento del auxiliar */				
				mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
													 id_estatus_reparaciones, id_empleado)
											VALUES('".$folio_reparacion."','2','".$fecha_entrada."','".$hora."',
													'".$id_estatus_reparacion."','".$id_empleado."')") or die('Guardar Movimiento');
?>			
    			<script language='javascript' type='text/javascript'> 
					alert('Se guardo exitosamente la nota de reparación N°: <?php echo $folio_reparacion; ?>');
					window.open("formato_reparacion.php?folio_num_reparacion=<?php echo $folio_reparacion; ?>", "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");

					window.location.href='lista_reparaciones.php'
				</script>
<?php 
			}
			else
			{
					/* Editar datos del paciente */
					mysql_query("UPDATE ficha_identificacion SET nombre='".$nombre."',
																paterno='".$paterno."',
																materno='".$materno."',
																calle='".$calle."',
																num_exterior='".$num_exterior."',
																num_interior='".$num_interior."',
																codigo_postal='".$codigo_postal."',
																colonia='".$colonia."',
																id_estado='".$id_estado."',
																id_ciudad='".$id_ciudad."',
																edad='".$edad."',
																ocupacion='".$ocupacion."',
																estado_civil='".$estado_civil."'
															WHERE id_cliente=".$id_cliente)or die('Editar Datos del Paciente');
					/* Editar los contactos de los pacientes */
					for($i=0; $i<=count($telefono); $i++){
						$telefono[$i];
						$tipo[$i];
						$titular[$i];
						$id_contacto[$i];
						mysql_query("UPDATE contactos_clientes SET tipo= '".$tipo[$i]."',
																	descripcion= '".$telefono[$i]."',
																	titular= '".$titular[$i]."'        
																WHERE id_cliente= '".$id_cliente."'
																AND id_contactos_cliente= '".$id_contacto[$i]."'") 
																or die('Contactos del Cliente');
					}
					/* actualizar la variable folio por si alguien mas registro una nota antes*/
					$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_nota_reparacion+1) FROM folios_num_nota_reparacion"));
					$folio_reparacion = $act_folio[0];
					
					/*Guardar el numero de folio de la reparacion */									
					mysql_query("INSERT INTO folios_num_nota_reparacion
								VALUES('1', ".$folio_reparacion.")") or die('Sumar Folio');
					/* Guardar los datos de la reparacion */ 
					mysql_query("INSERT INTO reparaciones(folio_num_reparacion, id_cliente, nombre, paterno, materno, calle,
														  num_exterior, num_interior, codigo_postal, colonia, id_estado, id_ciudad,
														  fecha_entrada, id_modelo, num_serie, id_estatus_reparaciones,
														  descripcion_problema, reparacion, adaptacion, venta)
													VALUES('".$folio_reparacion."','".$id_cliente."','".$nombre."',
															'".$paterno."','".$materno."','".$calle."','".$num_exterior."',
															'".$num_interior."','".$codigo_postal."','".$colonia."',
															'".$id_estado."','".$id_ciudad."','".$fecha_entrada."',
															'".$id_modelo."','".$num_serie."','".$id_estatus_reparacion."',
															'".$descripcion."','".$garantia_reparacion."','".$garantia_adaptacion."','".$garantia_venta."')") 
													or die('Guardar Reparacion');
					/* Se guardan los accesorios dejados y problemas del Auxiliar */
					mysql_query("INSERT INTO accesorios_problemas(folio_num_reparacion, estuche, molde, pila, no_se_oye,
																 portapila, circuito, se_oye_dis, microfono, tono, falla, receptor,
																 caja, gasta_pila, control_volumen, contactos_pila, cayo_piso,
																 switch, cambio_tubo, diagnostico, bobina, cableado, limpieza)
														VALUES('".$folio_reparacion."','".$estuche."','".$molde."','".$pila."',
																'".$no_se_oye."','".$portapila."','".$circuito."',
																'".$se_oye_dis."','".$microfono."','".$tono."','".$falla."',
																'".$receptor."','".$caja."','".$gasta_pila."',
																'".$control_volumen."','".$contactos_pila."','".$cayo_piso."',
																'".$switch."','".$cambio_tubo."','".$diagnostico."','".$bobina."',
																'".$cableado."','".$limpieza."')")or die('Guardar Accesorios de la Reparacion');
					/* Movimiento del auxiliar */				
					mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
														 id_estatus_reparaciones, id_empleado)
												VALUES('".$folio_reparacion."','2','".$fecha_entrada."','".$hora."',
														'".$id_estatus_reparacion."','".$id_empleado."')") or die('Guardar Movimiento 2');							
	?>
	   				<script language='javascript' type='text/javascript'> 
						alert('Se guardo exitosamente la nota de reparación N°: <?php echo $folio_reparacion; ?>');
						window.open("formato_reparacion.php?folio_num_reparacion=<?php echo $folio_reparacion; ?>", "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");
						window.location.href='lista_reparaciones.php'
					</script>
	<?php 
			}
		}
		elseif($variable == 2 )
		{			
			if($modelo != 0 && $_POST["num_serie_escritos1"] != "")
			{
				$id_modelo = $modelo;
				$num_serie = $_POST["num_serie_escritos1"];
			/* Si marca es diferente de 0, entonces se hace lo siguiente */
			}
			elseif($marca != 0 && $_POST["tipo_aparato"] != 0 && $_POST["modelos_escritos"] != "" && $_POST["num_serie_escritos2"] !="")
			{
				$tipo_aparato = $_POST["tipo_aparato"];
				$modelos_escritos = $_POST["modelos_escritos"];
				$num_serie = $_POST["num_serie_escritos2"];
				/* Guarda el nuevo modelo en la tabla modelos */
				mysql_query("INSERT INTO modelos_2(id_subcategoria, modelo, id_tipo_modelo)
										VALUES('".$marca."','".$modelos_escritos."','".$tipo_aparato."')")
										or die(mysql_error());
				/* Se saca el ultimo id modelo registrado */
				$id_modelo_nuevo=mysql_insert_id();
				/* Se guarda en la base productos el nuevo modelo, pero en estado Inactivo */
				mysql_query("INSERT INTO base_productos_2(id_categoria, id_subcategoria, id_articulo, descripcion, activo)
												VALUES('5','".$marca."','".$id_modelo_nuevo."','".$modelos_escritos."','No')")
												or die(mysql_error());
				/* El nuevo id del modelo es: */
				$id_modelo=mysql_insert_id();
			}
			elseif($_POST["marca_escritos"] != "" && $_POST["tipo_aparato2"] != 0 && $_POST["modelos_escritos2"] != "" && $_POST["num_serie_escritos3"] != "")
			{
				$marca_escritos = $_POST["marca_escritos"];
				$tipo_aparato = $_POST["tipo_aparato2"];
				$modelos_escritos = $_POST["modelos_escritos2"];
				$num_serie = $_POST["num_serie_escritos3"];
				/* Guarda la nueva marca en la tabla subcategorias_productos */
				mysql_query("INSERT INTO subcategorias_productos_2(id_categoria, subcategoria)
														VALUES('5','".$marca_escritos."')")or die(mysql_error());
				/* Se saca el ultimo id subcategoria registrado de categoria 5 */
				$id_subcategoria_nueva=mysql_insert_id();
				/* Guarda el nuevo modelo en la tabla modelos */
				mysql_query("INSERT INTO modelos_2(id_subcategoria, modelo, id_tipo_modelo)
										VALUES('".$id_subcategoria_nueva."','".$modelos_escritos."','".$tipo_aparato."')")
										or die(mysql_error());
				/* Se saca el ultimo id modelo registrado */
				$id_modelo_nuevo=mysql_insert_id();
				/* Se guarda en la base productos el nuevo modelo, pero en estado Inactivo */
				mysql_query("INSERT INTO base_productos_2(id_categoria, id_subcategoria, id_articulo, descripcion, activo)
												VALUES('5','".$id_subcategoria_nueva."','".$id_modelo_nuevo."',
												'".$modelos_escritos."','No')")or die(mysql_error());
				/* El nuevo id del modelo es: */
				$id_modelo=mysql_insert_id();
			}
			/* actualizar la variable folio por si alguien mas registro una nota antes*/
			$act_folio = mysql_fetch_array(mysql_query("SELECT MAX(folio_nota_reparacion+1) FROM folios_num_nota_reparacion"));
			$folio_reparacion = $act_folio[0];
			
			/*Guardar el numero de folio de la reparacion */											
			mysql_query("INSERT INTO folios_num_nota_reparacion
							VALUES('1', ".$folio_reparacion.")") or die(mysql_error());
			mysql_query("INSERT INTO reparaciones(folio_num_reparacion, id_cliente, nombre, paterno, materno, tipo,
												 descripcion, titular, calle, num_exterior, num_interior, codigo_postal,
												 colonia, id_estado, id_ciudad, fecha_entrada, id_modelo, num_serie,
												  id_estatus_reparaciones, descripcion_problema, reparacion, adaptacion, venta)
											VALUES('".$folio_reparacion."','".$id_cliente."','".$nombre."','".$paterno."',
													'".$materno."','".$tipo."','".$telefono."','".$titular."','".$calle."',
													'".$num_exterior."','".$num_interior."','".$codigo_postal."',
													'".$colonia."','".$id_estado."','".$id_ciudad."','".$fecha_entrada."',
													'".$id_modelo."','".$num_serie."','".$id_estatus_reparacion."',
													'".$descripcion."','".$garantia_reparacion."','".$garantia_adaptacion."','".$garantia_venta."')") or die(mysql_error());		
			/* Se guardan los accesorios dejados y problemas del Auxiliar */
				mysql_query("INSERT INTO accesorios_problemas(folio_num_reparacion, estuche, molde, pila, no_se_oye,
															 portapila, circuito, se_oye_dis, microfono, tono, falla, receptor,
															 caja, gasta_pila, control_volumen, contactos_pila, cayo_piso,
															 switch, cambio_tubo, diagnostico, bobina, cableado, limpieza)
													VALUES('".$folio_reparacion."','".$estuche."','".$molde."','".$pila."',
															'".$no_se_oye."','".$portapila."','".$circuito."',
															'".$se_oye_dis."','".$microfono."','".$tono."','".$falla."',
															'".$receptor."','".$caja."','".$gasta_pila."',
															'".$control_volumen."','".$contactos_pila."','".$cayo_piso."',
															'".$switch."','".$cambio_tubo."','".$diagnostico."','".$bobina."',
															'".$cableado."','".$limpieza."')")or die(mysql_error());
			/* Movimiento del auxiliar */				
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
												 id_estatus_reparaciones, id_empleado)
											VALUES('".$folio_reparacion."','2','".$fecha_entrada."','".$hora."',
													'".$id_estatus_reparacion."','".$id_empleado."')") or die(mysql_error());
?>				
   			<script language='javascript' type='text/javascript'> 
				alert('Se guardo exitosamente la nota de reparación N°: <?php echo $folio_reparacion; ?>');
				window.open("formato_reparacion.php?folio_num_reparacion=<?php echo $folio_reparacion; ?>", "nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=1000, height=800");
				window.location.href='lista_reparaciones.php'
			</script>
<?php
		}
}		
?>