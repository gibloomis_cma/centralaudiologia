<?php
	include("config.php");
	include("metodo_cambiar_fecha.php");
	$accion = $_POST["accion"];	 
	 /* Datos Generales */
	$id_cliente = $_POST["id_paciente"];
	$id_empleado = $_POST["elaboro"];
	$fecha = $_POST["fecha"];
	$fecha_mysql = cambiaf_a_mysql($fecha);
	/* Datos ANTECEDENTES DE FAMILIARES CONSANGUINEOS */
	$sordera = $_POST["sordera"];
	$hipertencion = $_POST["hipertencion"];
	$diabeticos =  $_POST["diabeticos"];
	$con_sordera_infantil = $_POST["con_sordera_infantil"];
	$otros = $_POST["otros"];
	
	$fue_prematuro = $_POST["fue_prematuro"];
	$tubo_bilirrubinas_altas = $_POST["tubo_bilirrubinas_altas"];
	$estubo_en_incubadora = $_POST["estubo_en_incubadora"];
  	$otros_problemas_durante_embarazo = $_POST["otros_problemas_durante_embarazo"];
	
	/* Datos ANTECEDENTES PERSONALES NO PATOLOGICOS */	
	$lugar_residencia = $_POST["lugar_residencia"];
	$ocupacion_actual = $_POST["ocupacion_actual"];
	$desde_el_año = $_POST["desde_el_año"];
	$expuesto_a_ruido = $_POST["expuesto_a_ruido"];
	$utiliza_solventes_o_metales_actual = $_POST["utiliza_solventes_o_metales_actual"];
	$otros_factores_riesgo_auditivo = $_POST["otros_factores_riesgo_auditivo"];
	$otros_trabajos_expuesto_riesgos = $_POST["otros_trabajos_expuesto_factores_riesgo"];
	$practica_alguna_actividad_expuesto_riesgos = $_POST["otras_actividades_expuesto_factores_riesgo"];
	
	$fuente_del_ruido = $_POST["fuente_del_ruido"];
	$tiempo_exposicion_diario = $_POST["tiempo_exposicion_diario"];
	$producto = $_POST["producto"];
	
	/* Datos trabajos anteriores */
	$ocupacion_anterior = $_POST["ocupacion_anterior"];
	$desde_que_año = $_POST["desde_que_año"];
	$hasta_el_año = $_POST["hasta_el_año"];
	$expuesto_a_ruido_anterior = $_POST["expuesto_a_ruido_anterior"];
	$utiliza_solventes_o_metales_anterior = $_POST["utiliza_solventes_o_metales_anterior"];
	$otros_factores_riesgo_auditivo_anterior = $_POST["otros_factores_riesgo_auditivo_anterior"];
	$utiliza_solventes_o_metales_anterior2 = $_POST["utiliza_solventes_o_metales_anterior2"];
	$expuesto_a_ruido_anterior2 = $_POST["expuesto_a_ruido_anterior2"];
	
	$fuente_del_ruido_anterior = $_POST["fuente_del_ruido_anterior"];
	$tiempo_exposicion_diario_anterior = $_POST["tiempo_exposicion_diario_anterior"];
	$producto_anterior = $_POST["producto_anterior"];
	
	/* Datos actividades extras */
	$actividad = $_POST["actividad"];
	$desde_que_año_actividad = $_POST["desde_que_año_actividad"];
	$expuesto_a_ruido_actividad = $_POST["expuesto_a_ruido_actividad"];
	$utiliza_solventes_o_metales_actividad = $_POST["utiliza_solventes_o_metales_actividad"];
	$expuesto_a_ruido_actividad2 = $_POST["expuesto_a_ruido_actividad2"];
	$utiliza_solventes_o_metales_actividad2 = $_POST["utiliza_solventes_o_metales_actividad2"];
	$otros_factores_riesgo_auditivo_actividad = $_POST["otros_factores_riesgo_auditivo_actividad"];
	
	$fuente_del_ruido_actividad = $_POST["fuente_del_ruido_actividad"];
	$tiempo_exposicion_diario_actividad = $_POST["tiempo_exposicion_diario_actividad"];
	$producto_actividad = $_POST["producto_actividad"];
	
	/* Datos ANTECEDENTES PERSONALES PATOLOGICOS.*/
	$presenta_sordera = $_POST["presenta_sordera"];
	$hipertencion_personales = $_POST["hipertencion_personales"];
	$diabeticos_personales = $_POST["diabeticos_personales"];
	$infecciones_garganta = $_POST["infecciones_garganta"];
	$cirugia_traumatismo = $_POST["cirugia_traumatismo"];
	
	$desde_que_año_sordo = $_POST["desde_que_año_sordo"];
	$ha_usado_auxiliar = $_POST["ha_usado_auxiliar"];
	
	$desde_que_año_auxiliar = $_POST["desde_que_año_auxiliar"];
	$modelo = $_POST["modelo"];
	$que_resultado_obtuvo = $_POST["que_resultado_obtuvo"];
	
	$descripcion = $_POST["descripcion"];
	$desde_que_año_cirugia_traumatismo = $_POST["desde_que_año_cirugia_traumatismo"];
	$tratamiento = $_POST["tratamiento"];
	$resultados = $_POST["resultados"];	
	if($accion == "Guardar"){
		mysql_query("INSERT INTO anamnesis(id_cliente, fecha_elaboracion, elaboro)
								VALUES('".$id_cliente."','".$fecha_mysql."','".$id_empleado."')") 
								or die(mysql_error());						
		mysql_query("INSERT INTO antecedentes_familiares_consaguineos(id_cliente, sordera, hipertencion, diabeticos, 
																		con_sordera_infantil, otros)
																VALUES('".$id_cliente."','".$sordera."',
																		'".$hipertencion."','".$diabeticos."',
																		'".$con_sordera_infantil."','".$otros."')") 
																		or die(mysql_error());		 
		if($con_sordera_infantil == "Si"){
			mysql_query("INSERT INTO paciente_con_sordera_infantil(id_cliente, fue_prematuro, tubo_bilirrubinas_altas,
																	estubo_en_incubadora,
																	 otros_problemas_durante_embarazo)
															VALUES('".$id_cliente."','".$fue_prematuro."',
																	'".$tubo_bilirrubinas_altas."',
																	'".$estubo_en_incubadora."',
																	'".$otros_problemas_durante_embarazo."')") 
																	or die(mysql_error());
		
		}
		mysql_query("INSERT INTO antecedentes_personales_no_patologicos(id_cliente, lugar_residencia, ocupacion_actual,
																		 desde_el_ano, expuesto_a_ruido, 
																		 utiliza_solventes_o_metales_actual,
																		  otros_factores_riesgo_auditivo,
																		   otros_trabajos_expuesto_riesgos,
																		   practica_alguna_actividad_expuesto_riesgos)
																VALUES('".$id_cliente."','".$lugar_residencia."',
																		'".$ocupacion_actual."','".$desde_el_año."',
																		'".$expuesto_a_ruido."',
																		'".$utiliza_solventes_o_metales_actual."',
																		'".$otros_factores_riesgo_auditivo."',
																		'".$otros_trabajos_expuesto_riesgos."',
																		'".$practica_alguna_actividad_expuesto_riesgos."')") 
																		or die(mysql_error());
		$variable_antecedentes_no_patologicos = mysql_insert_id();						
		if($expuesto_a_ruido == "Si"){
			$variable_identificador_trabajo_actual = 1;
			mysql_query("INSERT INTO si_es_ruidoso(id_cliente, fuente_ruido, tiempo_exposicion_diaria,
													id_registro_identificador, id_registro_tabla)
											VALUES('".$id_cliente."','".$fuente_del_ruido."',
													'".$tiempo_exposicion_diario."',
													'".$variable_identificador_trabajo_actual."',
													'".$variable_antecedentes_no_patologicos."')") 
													or die(mysql_error());	
													
		}
		if($utiliza_solventes_o_metales_actual == "Si"){
			mysql_query("INSERT INTO si_utiliza(id_cliente, producto, id_registro_identificador, id_registro_tabla)
										VALUES('".$id_cliente."','".$producto."',
										'".$variable_identificador_trabajo_actual."',
										'".$variable_antecedentes_no_patologicos."')") 
										or die(mysql_error());
		}
		if($otros_trabajos_expuesto_riesgos == "Si"){
			$variable_identificador_trabajo_anterior = 2;		
			for($i=0; $i<count($ocupacion_anterior); $i++){
				$ocupacion_anterior[$i];
				$desde_que_año[$i];
				$hasta_el_año[$i];
				$expuesto_a_ruido_anterior[$i];
				$utiliza_solventes_o_metales_anterior[$i];
				$otros_factores_riesgo_auditivo_anterior[$i];
				$fuente_del_ruido_anterior[$i];
				$tiempo_exposicion_diario_anterior[$i];
				$producto_anterior[$i];
				$expuesto_a_ruido_anterior2[$i];
				$utiliza_solventes_o_metales_anterior2[$i];			
				if($i==0){
					mysql_query("INSERT INTO estubo_otros_trabajos(id_cliente, ocupacion_anterior, desde_que_ano,
															 	hasta_que_ano, expuesto_a_ruido, 
																 utiliza_solventes_o_metales_pesados, 
															 	otros_factores_riesgo_auditivo)
														VALUES('".$id_cliente."','".$ocupacion_anterior[$i]."',
																'".$desde_que_año[$i]."','".$hasta_el_año[$i]."',
																'".$expuesto_a_ruido_anterior[$i]."',
																'".$utiliza_solventes_o_metales_anterior[$i]."',
																'".$otros_factores_riesgo_auditivo_anterior[$i]."')") 
																or die(mysql_error());
					$variable_otros_trabajos = mysql_insert_id();											
					if($expuesto_a_ruido_anterior[$i] == "Si"){
						mysql_query("INSERT INTO si_es_ruidoso(id_cliente, fuente_ruido, tiempo_exposicion_diaria,
															id_registro_identificador, id_registro_tabla)
													VALUES('".$id_cliente."','".$fuente_del_ruido_anterior[$i]."',
															'".$tiempo_exposicion_diario_anterior[$i]."',
															'".$variable_identificador_trabajo_anterior."',
															'".$variable_otros_trabajos."')") 
															or die(mysql_error());	
															
					}
					if($utiliza_solventes_o_metales_anterior[$i] == "Si"){
						mysql_query("INSERT INTO si_utiliza(id_cliente, producto, id_registro_identificador, 
															id_registro_tabla)
													VALUES('".$id_cliente."','".$producto_anterior[$i]."',
															'".$variable_identificador_trabajo_anterior."',
															'".$variable_otros_trabajos."')") 
															or die(mysql_error());															
					}
				}
				else{
					mysql_query("INSERT INTO estubo_otros_trabajos(id_cliente, ocupacion_anterior, desde_que_ano,
															 	hasta_que_ano, expuesto_a_ruido, 
																 utiliza_solventes_o_metales_pesados, 
															 	otros_factores_riesgo_auditivo)
														VALUES('".$id_cliente."','".$ocupacion_anterior[$i]."',
																'".$desde_que_año[$i]."','".$hasta_el_año[$i]."',
																'".$expuesto_a_ruido_anterior2[$i-1]."',
																'".$utiliza_solventes_o_metales_anterior2[$i-1]."',
																'".$otros_factores_riesgo_auditivo_anterior[$i]."')") 
																or die(mysql_error());
					$variable_otros_trabajos2 = mysql_insert_id();																										
					if($expuesto_a_ruido_anterior2[$i-1] == "Si"){
						mysql_query("INSERT INTO si_es_ruidoso(id_cliente, fuente_ruido, tiempo_exposicion_diaria,
															id_registro_identificador, id_registro_tabla)
													VALUES('".$id_cliente."','".$fuente_del_ruido_anterior[$i]."',
															'".$tiempo_exposicion_diario_anterior[$i]."',
															'".$variable_identificador_trabajo_anterior."',
															'".$variable_otros_trabajos2."')") 
															or die(mysql_error());	
															
					}
					if($utiliza_solventes_o_metales_anterior2[$i-1] == "Si"){
						mysql_query("INSERT INTO si_utiliza(id_cliente, producto, id_registro_identificador, 
															id_registro_tabla)
													VALUES('".$id_cliente."','".$producto_anterior[$i]."',
															'".$variable_identificador_trabajo_anterior."',
															'".$variable_otros_trabajos2."')") 
															or die(mysql_error());
					}
				}
			}
		}		
		if($practica_alguna_actividad_expuesto_riesgos == "Si"){
			$variable_identificador_actividad = 3;			
			for($i=0; $i<count($actividad); $i++){
				$actividad[$i];
				$desde_que_año_actividad[$i];
				$expuesto_a_ruido_actividad[$i];
				$utiliza_solventes_o_metales_actividad[$i];
				$expuesto_a_ruido_actividad2[$i];
				$utiliza_solventes_o_metales_actividad2[$i];
				$otros_factores_riesgo_auditivo_actividad[$i];
				
				$fuente_del_ruido_actividad[$i];
				$tiempo_exposicion_diario_actividad[$i];
				$producto_actividad[$i];
				if($i==0){
					mysql_query("INSERT INTO actividades_extra_laborales(id_cliente, actividad, desde_que_ano,
															 	expuesto_a_ruido, 
																 utiliza_solventes_o_metales, 
															 	otros_factores_riesgo_auditivo)
														VALUES('".$id_cliente."','".$actividad[$i]."',
																'".$desde_que_año_actividad[$i]."',
																'".$expuesto_a_ruido_actividad[$i]."',
																'".$utiliza_solventes_o_metales_actividad[$i]."',
																'".$otros_factores_riesgo_auditivo_actividad[$i]."')") 
																or die(mysql_error());
					$variable_actividades_extras = mysql_insert_id();												
					if($expuesto_a_ruido_actividad[$i] == "Si"){
						mysql_query("INSERT INTO si_es_ruidoso(id_cliente, fuente_ruido, tiempo_exposicion_diaria,
															id_registro_identificador, id_registro_tabla)
													VALUES('".$id_cliente."','".$fuente_del_ruido_actividad[$i]."',
															'".$tiempo_exposicion_diario_actividad[$i]."',
															'".$variable_identificador_actividad."',
															'".$variable_actividades_extras."')") 
															or die(mysql_error());	
															
					}
					if($utiliza_solventes_o_metales_actividad[$i] == "Si"){
						mysql_query("INSERT INTO si_utiliza(id_cliente, producto, id_registro_identificador,
															id_registro_tabla)
													VALUES('".$id_cliente."','".$producto_actividad[$i]."',
															'".$variable_identificador_actividad."',
															'".$variable_actividades_extras."')") 
															or die(mysql_error());														
					}					
				}/* Fin del IF */
				else{
					mysql_query("INSERT INTO actividades_extra_laborales(id_cliente, actividad, desde_que_ano,
															 	expuesto_a_ruido, 
																 utiliza_solventes_o_metales, 
															 	otros_factores_riesgo_auditivo)
														VALUES('".$id_cliente."','".$actividad[$i]."',
																'".$desde_que_año_actividad[$i]."',
																'".$expuesto_a_ruido_actividad2[$i-1]."',
																'".$utiliza_solventes_o_metales_actividad2[$i-1]."',
																'".$otros_factores_riesgo_auditivo_actividad[$i]."')") 
																or die(mysql_error());
					$variable_actividades_extras2 = mysql_insert_id();																
					if($expuesto_a_ruido_actividad2[$i-1] == "Si"){
						mysql_query("INSERT INTO si_es_ruidoso(id_cliente, fuente_ruido, tiempo_exposicion_diaria,
															id_registro_identificador, id_registro_tabla)
													VALUES('".$id_cliente."','".$fuente_del_ruido_actividad[$i]."',
															'".$tiempo_exposicion_diario_actividad[$i]."',
															'".$variable_identificador_actividad."',
															'".$variable_actividades_extras2."')") 
															or die(mysql_error());																
					}
					if($utiliza_solventes_o_metales_actividad2[$i-1] == "Si"){
						mysql_query("INSERT INTO si_utiliza(id_cliente, producto, id_registro_identificador,
															id_registro_tabla)
													VALUES('".$id_cliente."','".$producto_actividad[$i]."',
															'".$variable_identificador_actividad."',
															'".$variable_actividades_extras2."')") 
															or die(mysql_error());
															
					}					
				} /* Fin de ELSE*/		
			} /* Fin del FOR */	
		} /* Fin del IF */		
		mysql_query("INSERT INTO antecedentes_personales_patologicos(id_cliente, presenta_sordera,
																	 hipertencion_personales, diabeticos_personales,
																	  infecciones_garganta, cirugia_traumatismo)
															VALUES('".$id_cliente."','".$presenta_sordera."',
																	'".$hipertencion_personales."',
																	'".$diabeticos_personales."',
																	'".$infecciones_garganta."',
																	'".$cirugia_traumatismo."')") 
																	or die(mysql_error());
																	
		if($presenta_sordera == "Si"){
			mysql_query("INSERT INTO si_presenta_sordera(id_cliente, desde_que_ano, ha_usado_auxiliar_auditivo)
												VALUES('".$id_cliente."','".$desde_que_año_sordo."',
														'".$ha_usado_auxiliar."')") or die(mysql_error());													
			if($ha_usado_auxiliar == "Si"){
				for($i=0; $i<count($modelo); $i++){
					$desde_que_año_auxiliar[$i];
					$modelo[$i];
					$que_resultado_obtuvo[$i];					
					mysql_query("INSERT INTO si_ha_usado_auxiliar_auditivo(id_cliente, desde_que_ano, modelo,
																			 que_resultado_obtuvo)
																	VALUES('".$id_cliente."',
																			'".$desde_que_año_auxiliar[$i]."',
																			'".$modelo[$i]."',
																			'".$que_resultado_obtuvo[$i]."')")
																			or die(mysql_error());
				} /* Fin del FOR */	
			}/* Fin del IF */ 
		}		
		if($cirugia_traumatismo == "Si"){
			for($i=0; $i<count($descripcion); $i++){
				$descripcion[$i];
				$desde_que_año_cirugia_traumatismo[$i];
				$tratamiento[$i];
				$resultados[$i];				
				mysql_query("INSERT INTO si_presenta_algun_padecimiento(id_cliente, descripcion, desde_que_ano,
																		tratamiento, resultados)
																VALUES('".$id_cliente."','".$descripcion[$i]."',
																		'".$desde_que_año_cirugia_traumatismo[$i]."',
																		'".$tratamiento[$i]."','".$resultados[$i]."')") 
																		or die(mysql_error());
			} /* Fin del FOR */	
		} /* Fin del IF */
?>
<script  language='javascript' type='text/javascript'> 
	alert('La Anamnesis se registro exitosamente');
	window.location.href='lista_de_estudios.php?id_paciente=<?php echo $id_cliente; ?>'
</script>
<?php															
	} /* Fin del IF de accion */
?>