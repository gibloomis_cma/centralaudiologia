<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Agregar Contacto Proveedores</title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
</head>

<body>        
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Proveedores
                </div>
            </div><!--Fin de fondo titulo-->                    
		<?php
            // SE IMPORTA EL ARCHIVO DE LA CONEXION A LA BASE DE DATOS
            include("config.php");
            $id_proveedor = $_GET["id_proveedor"];
            $query_datos_proveedor = mysql_query("SELECT razon_social,calle,num_exterior,
												num_interior,colonia,codigo_postal,id_ciudad, id_pais,
												id_estado,rfc,credito,plazos_pago 
												FROM proveedores                                                   
											  	WHERE id_proveedor = ".$id_proveedor)
												or die(mysql_error());
            $row = mysql_fetch_array($query_datos_proveedor);            
			$nombre_dependencia = $row["razon_social"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$colonia = $row["colonia"];
			$codigo_postal = $row["codigo_postal"];
			$id_ciudad = $row["id_ciudad"];
			$id_estado = $row["id_estado"];
			$id_pais = $row["id_pais"];
			$rfc = $row["rfc"];
			$credito = $row["credito"];
			$plazos_pago = $row["plazos_pago"];
        ?>
         <center>        
         	<div class="area_contenido1"><br />                          
            	<div class="contenido_proveedor">                 
                	<table>
                    <tr>
                        <th colspan="4">Datos Generales</th>
                    </tr><tr>
                        <td style="text-align:right">               
                            <label class="textos">Nombre de la Dependencia: </label>
                        </td><td style="text-align:left" colspan="3">
                            <?php echo $nombre_dependencia; ?>
                        </td>
                    </tr><tr>                        
                        <td style="text-align:right">                           
                            <label class="textos">Dirección: </label>
                        </td>
                        <td style="text-align:left" colspan="3">
                        <?php 						
                            if($num_interior == ""){
                                $interior = "";
                            }else{
                                $interior = " Int. ".$num_interior;
                            }
                            echo $calle." #".$num_exterior.$interior." Col. ".$colonia." C.P. ".$codigo_postal;
                        ?>                            
                        </td>                            
                    </tr><tr>
                        	<td style="text-align:right"><label class="textos">Pais: </label></td>
                            <td style="text-align:left">
							<?php
                                $consulta_paises = mysql_query("SELECT pais FROM paises 
                                                                WHERE id_pais=".$id_pais);
                                $row15 = mysql_fetch_array($consulta_paises);
                                $pais_consultado = $row15["pais"];	
                                echo $pais_consultado; 
                            ?>
                        </tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Estado: </label>
                        </td><td style="text-align:left">
                        <?php
                            $consulta_estados = mysql_query("SELECT estado FROM estados 
                                                            WHERE id_estado=".$id_estado);
                            $row3 = mysql_fetch_array($consulta_estados);
                            $estado_consultado = $row3["estado"];	
                            echo utf8_encode($estado_consultado); 
                        ?>
                        </td><td style="text-align:right">
                            <label class="textos">Ciudad: </label>                         	
                        </td><td style="text-align:left">
                        <?php
                            $consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades
                                                            WHERE id_ciudad=".$id_ciudad);
                            $row4 = mysql_fetch_array($consulta_ciudad);
                            $ciudad_consultado = ucwords(strtolower($row4["ciudad"]));	
                            echo utf8_encode($ciudad_consultado); 
                        ?>                                                       
                        </td>
                    </tr><tr>
                        <td style="text-align:right">
                            <label class="textos">RFC: </label>
                        </td><td style="text-align:left">
                            <?php echo strtoupper($rfc); ?>
                        </td><td style="text-align:right">                                
                            <label class="textos">Credito: </label>
                        </td><td style="text-align:left">                                
                            <?php echo "$".number_format($credito,2); ?>                                
                        </td>
                    </tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Plazos de Pago: </label>                                
                        </td><td style="text-align:left" colspan="3">
                            <?php echo $plazos_pago; ?>
                            <label> dias </label>
                        </td>
                    </tr>                   
                 </table>                   
            	</div><!--Fin de contenido proveedor-->
            	<table>      
					<tr>
                    	<th colspan="3">Contacto(s)</th>
                	</tr>
                <!--onsubmit="return validarFormaContactoProveedor()" -->                                      
                <?php
                    $contactos = mysql_query('SELECT id_contacto_proveedor, nombre, 
                                            paterno, materno
                                            FROM contactos_proveedores
                                            WHERE id_proveedor ='.$id_proveedor)
                                            or die(mysql_error());
                    $n_contactos=0;
                    while($row_contactos = mysql_fetch_array($contactos)){
                        $id_contacto = $row_contactos['id_contacto_proveedor'];
                        $nombre = $row_contactos['nombre'];
                        $materno = $row_contactos['materno'];
                        $paterno = $row_contactos['paterno'];
                        $n_contactos++;
                ?>      
                	<tr>          
                        <td style="width:30%">
                            <?php echo $nombre." ".$paterno." ".$materno; ?>
                        </td><td>                   
                            <div>                        
                <?php 
                            $formas_contacto = mysql_query('SELECT id_forma_contacto, tipo, descripcion
                                                        FROM forma_contacto_proveedores
                                                        WHERE id_contacto_proveedor ='.$id_contacto)
                                                        or die(mysql_error());
                            $n_forma_contacto=0;
                            while($row_formas_contacto = mysql_fetch_array($formas_contacto)){
                                $id_forma_contacto = $row_formas_contacto['id_forma_contacto'];
                                $tipo = $row_formas_contacto['tipo'];
                                $descripcion = $row_formas_contacto['descripcion'];
                                $n_forma_contacto++;
                ?>
                            <div id="cleft">
                                <label class="textos"><?php echo $tipo; ?>:</label>
                            </div>
                            <div id="cright">
                                <?php echo $descripcion; ?>
                            </div><br />                           
                <?php					
                            }//END WHILE CONSULTA FORMAS CONTACTO			
                ?>      	
                            </div>                            
                        </td><td id="alright">
                        	<a href="eliminar_contacto_proveedor.php?id_contacto_proveedor=<?php echo $id_contacto."&id_proveedor=".$id_proveedor; ?>">
                           		<img src="../img/delete.png" />
                            </a>
                        </td>                                 
                    </tr>                
				<?php
                        }
                ?>
                    </tr><tr>
                    	<td style="text-align:right" colspan="3">
                        	<input name="volver" type="button" value="Volver" class="fondo_boton" title="Guardar"
                            onclick="window.location.href='agregar_proveedor.php'"/>
                            <input name="editar" type="button" value="Editar" class="fondo_boton" title="Guardar"
                            onclick="window.location.href='modificar_proveedores.php?id_proveedor=<?php echo $id_proveedor;  ?>'"/>
                        </td>
                    </tr>
                </table>                                                                      
                <!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
            </center>
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</body>
</html>