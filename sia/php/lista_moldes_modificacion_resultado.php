<!-- include style -->
 <link rel="stylesheet" type="text/css" href="../css/style_pagination.css" />

<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include 'config.php';
    
    //include our awesome pagination
    //class (library)
    include '../librerias/ps_pagination.php';

    $filtro = $_POST['filtro'];

    if ( $filtro != "" )
    {
        // query all data anyway you want
        $sql = "SELECT DISTINCT(folio_num_molde), id_cliente, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_cliente, fecha_entrada, id_estatus_moldes,costo,adaptacion,reposicion,pago_parcial
                FROM moldes
                WHERE (nombre LIKE '%".$filtro."%'
                OR paterno LIKE '%".$filtro."%' 
                OR materno  LIKE '%".$filtro."%' 
                OR folio_num_molde LIKE '%".$filtro."%')
                AND id_estatus_moldes <> 7
                ORDER BY fecha_entrada DESC";
    }
    else
    {
        //query all data anyway you want
        $sql = "SELECT folio_num_molde, id_cliente, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_cliente, fecha_entrada, id_estatus_moldes, costo, adaptacion, reposicion, pago_parcial
                FROM moldes
                WHERE id_estatus_moldes <> 7
                ORDER BY fecha_entrada DESC";
    }

    //execute query and get and
    $rs = mysql_query($sql) or die(mysql_error());

    //now, where gonna use our pagination class
    //this is a significant part of our pagination
    //i will explain the PS_Pagination parameters
    //$conn is a variable from our config_open_db.php
    //$sql is our sql statement above
    //3 is the number of records retrieved per page
    //4 is the number of page numbers rendered below
    //null - i used null since in dont have any other
    //parameters to pass (i.e. param1=valu1&param2=value2)
    //you can use this if you're gonna use this class for search
    //results since you will have to pass search keywords
    $pager = new PS_Pagination( $link, $sql, 20, 3, null );

    //our pagination class will render new
    //recordset (search results now are limited
    //for pagination)
    $rs = $pager->paginate(); 

    //get retrieved rows to check if
    //there are retrieved data
    $num = mysql_num_rows( $rs );

    if( $num >= 1 )
    {     
    ?>
        <table>
            <tr>
                <th> Nº Molde </th>
                <th> Nombre Completo </th>
                <th> Costo </th>
                <th> Estatus </th>
                <th> </th>
            </tr>
    <?php    
        //looping through the records retrieved class='data-tr' align='center'
        while( $row = mysql_fetch_array( $rs ) )
        {
            $id_cliente = $row["id_cliente"];
            $folio_num_molde = $row["folio_num_molde"];
            $nombre_cliente = $row['nombre_cliente'];
            $estado_molde = $row["id_estatus_moldes"];
            $fecha_entrada = $row["fecha_entrada"];
            $costo_molde = $row['costo'];
            $pago_parcial = $row['pago_parcial'];
            $adaptacion = $row['adaptacion'];
            $reposicion = $row['reposicion'];
            
            $consulta_estatus_nota_molde = mysql_query("SELECT * FROM estatus_moldes
                                                        WHERE id_estatus_moldes = ".$estado_molde) or die(mysql_error());   
                            
            $row_estatus_nota_molde = mysql_fetch_array($consulta_estatus_nota_molde);
            $estatus_molde = $row_estatus_nota_molde["estado_molde"];                                 
    ?>
            <tr>
                <td style="text-align:center; font-size:12px;"> <?php echo $folio_num_molde; ?> </td>
                <td style="font-size:12px;"> <?php echo ucwords(strtolower($nombre_cliente)); ?> </td>
                <td style="text-align:center; font-size:12px;"> 
                    <?php 
                        if($adaptacion == "si" && $reposicion == "")
                        { 
                            echo "$ 0.00"; 
                        }
                        elseif( $reposicion == "si" && $adaptacion == "" )
                        { 
                            echo "$ 0.00"; 
                        }
                        else
                        { 
                            echo "$ ".number_format($costo_molde - $pago_parcial,2); 
                        } 
                    ?> 
                </td>
                <td style="font-size:12px;"> <?php echo $estatus_molde; ?> </td>
                <td> <a href="detalles_moldes_modificacion.php?folio=<?php echo $folio_num_molde; ?>">
                        <img src="../img/modify.png" />
                     </a>
                </td>
            </tr>   
    <?php
        }       
    ?>
        </table>
    <?php
    }
    else
    {
        // if no records found
        echo "<h3> No existen moldes registrados ! </h3>";
    }

    //page-nav class to control
    //the appearance of our page 
    //number navigation
    echo "<div class='page-nav'>";
         //display our page number navigation
         echo $pager->renderFullNav();
    echo "</div>";

?>