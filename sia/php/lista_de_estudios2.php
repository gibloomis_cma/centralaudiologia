<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Estudios</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">   
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Estudios
                </div>              
            </div><!--Fin de fondo titulo-->  
            <br />                     
            <div class="area_contenido2">
  	<?php
				include("config.php");	
				include("metodo_cambiar_fecha.php");					
				$id_cliente = $_GET["id_paciente"];
				$consulta_datos_paciente = mysql_query("SELECT * FROM ficha_identificacion 
																	WHERE id_cliente=".$id_cliente) 
																	or die(mysql_error());
				$row = mysql_fetch_array($consulta_datos_paciente);
				$nombre = $row["nombre"];
				$paterno = $row["paterno"];
				$materno = $row["materno"];
				$calle = $row["calle"];
				$num_exterior = $row["num_exterior"];
				$num_interior = $row["num_interior"];
				$codigo_postal = $row["codigo_postal"];
				$colonia = $row["colonia"];
				$edad = $row["edad"];
				$ocupacion = $row["ocupacion"];
				$id_estado = $row["id_estado"];
				$id_ciudad = $row["id_ciudad"];
				$estado_civil = $row["estado_civil"];					
				$consulta_anamnesis = mysql_query("SELECT COUNT(id_anamnesis) FROM anamnesis 
																				WHERE id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row2 = mysql_fetch_array($consulta_anamnesis);				
				$anamnesis = $row2["COUNT(id_anamnesis)"];	
	?>
                <div class="contenido_proveedor">
                    <form name="forma1" action="guardar_cambios.php" method="post">    
                        <fieldset>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <legend><label class="textos" style="font-size:13px;">Ficha de Identificacion</label></legend>
                        <label class="textos">Nombre Completo:  </label>&nbsp;
                        <?php echo $nombre." ".$paterno." ".$materno; ?>
                    <br /><br />
                        <label class="textos">Calle: </label>
                        <input name="calle" value="<?php echo $calle; ?>" type="text" />
                        <label class="textos">Num. Exterior: </label>
                        <input name="num_exterior" type="text" value="<?php echo $num_exterior; ?>" size="6" maxlength="6" />
                        <label class="textos">Num. Interior: </label>
                        <input name="num_interior" type="text" value="<?php echo $num_interior; ?>" size="4" maxlength="4" />
                    
                        <label class="textos">Codigo Postal: </label>
                        <input name="codigo_postal" value="<?php echo $codigo_postal; ?>" type="text" />
                    <br /><br />
                        <label class="textos">Colonia: </label>
                        <input name="colonia" value="<?php echo $colonia; ?>" type="text" />
                        <label class="textos">Estado: </label>
                        <select id="id_estado" name="id_estado">
                        	<option value="0" selected="selected"> -- Estado -- </option>
	<?php
                $consulta_estados = mysql_query("SELECT * FROM estados");
                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                    $id_estado_consultada = $row3["id_estado"];
                    $estado = $row3["estado"];
                    if($id_estado<>"" && $id_estado == $id_estado_consultada){
 	?>
							<option value="<?php echo $id_estado; ?>" selected="selected">
                            	<?php echo utf8_encode($estado); ?> 
                            </option>
	<?php
					}else{
	?>       
                        	<option value="<?php echo $id_estado_consultada; ?>"><?php echo utf8_encode($estado); 									
	?>
	<?php 	
					} 
				}
	?>
       					</select>
                        <label class="textos">Ciudad: </label>
                        <select id="id_municipio" name="id_municipio">
							<option value="0" selected="selected"> -- Municipio -- </option>
	<?php
				$consulta_ciudad = mysql_query("SELECT * FROM ciudades WHERE id_estado=".$id_estado) or die(mysql_error());
				while( $row4 = mysql_fetch_array($consulta_ciudad)){ 
					$id_ciudad_consultada = $row4["id_ciudad"];
					$ciudad = $row4["ciudad"];
					if($id_ciudad<>"" && $id_ciudad == $id_ciudad_consultada){
	?>
							<option value="<?php echo $id_ciudad; ?>" selected="selected">
                            	<?php echo utf8_encode($ciudad); ?> 
                            </option>
	<?php
					}else{
	?>       
                        	<option value="<?php echo $id_ciudad_consultada; ?>">
								<?php echo utf8_encode($ciudad); ?>
                            </option>
	<?php 	
					} 
				}
	?>
       					</select>   
                 	<br /><br />
                        <label class="textos">Edad: </label>
                        <input name="edad" type="text" value="<?php echo $edad; ?>" />
                        <label class="textos">Ocupacion: </label>
                        <input name="ocupacion" type="text" value="<?php echo $ocupacion; ?>" />
                        <label class="textos">Estado civil: </label>                        
                        <select name="estado_civil">
   	<?php
						$civil[0] = "";
						$civil[1] = "Casado(a)";
						$civil[2] = "Soltero(a)";
						$civil[3] = "Divorciado(a)";
						$civil[4] = "Separado(a)";						 
						for($i=0; $i<5; $i++){
							if($estado_civil == $civil[$i]){							
   	?>                                                           
                			<option value="<?php echo $estado_civil; ?>" selected="selected">
                            	<?php echo $estado_civil; ?>
                          	</option>
   	<?php 
                            }else{
    ?>                            
                            <option value="<?php echo $civil[$i]; ?>">
                                <?php echo $civil[$i]; ?>
                            </option>
  	<?php
                            }
                    	}
	?>
    					</select>
                 	<br /><br />
                        <label class="textos">Forma de Contacto</label>
                   	<br />
   	<?php
                $consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
																	WHERE id_cliente=".$id_cliente) 
																	or die(mysql_error());					
				while($row2 = mysql_fetch_array($consulta_forma_contacto)){
					$id_contactos_cliente = $row2["id_contactos_cliente"];
					$tipo = $row2["tipo"];
					$descripcion = $row2["descripcion"];
					$titular = $row2["titular"];
	?>
                        <input name="id_contactos_cliente[]" type="hidden" value="<?php echo $id_contactos_cliente; ?>"/>
                        <label class="textos">Tipo:</label>
                        <select name="tipo_telefono[]">
   	<?php
						$tipos[0] = "";
						$tipos[1] = "Telefono";
						$tipos[2] = "Celular";
						$tipos[3] = "Correo";
						$tipos[4] = "Fax";						 
						for($i=0; $i<5; $i++){
							if($tipo == $tipos[$i]){							
   	?>                                                           
                			<option value="<?php echo $tipo; ?>" selected="selected">
                            	<?php echo $tipo; ?>
                          	</option>
   	<?php 
                            }else{
    ?>                            
                            <option value="<?php echo $tipos[$i]; ?>">
                                <?php echo $tipos[$i]; ?>
                            </option>
  	<?php
                            }
                    	}
	?>
    					</select>
                        <label class="textos">Descripcion: </label>
                        <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>" />
                        <label class="textos">Titular: </label>
                        <input name="titular[]" type="text" value="<?php echo $titular; ?>" />
                  	<br /><br />
  	<?php 
				}
	?>
                   	<p align="right">
                        <input name="accion" type="submit" value="Volver" class="fondo_boton" />
                    &nbsp;&nbsp;&nbsp;
                    	<input name="accion" type="submit" value="Guardar Cambios" class="fondo_boton" />
                    </p>
                   		</fieldset>
                  	</form>
                <br />
                <center>
  	<?php 
				if($anamnesis == 0){
	?>
					<a href="cuestionario_anamnesis.php?id_paciente=<?php echo $id_cliente; ?>">
                        Hacer la Anamnesis
                    </a>	
  	<?php
				}else{
	?>
					<a href="ver_anamnesis.php?id_paciente=<?php echo $id_cliente; ?>">
						Ver la Anamnesis
					</a>
	<?php
				}
	?>
                <br /><br />
  	<?php
				$contador = mysql_query("SELECT COUNT(id_registro) FROM historial_clinico
																		WHERE id_cliente=".$id_cliente)
																		or die(mysql_error());
				$row_contador = mysql_fetch_array($contador);
				$contador_estudios = $row_contador["COUNT(id_registro)"];
				if($contador_estudios !=0){
               		$consulta_estudios = mysql_query("SELECT * FROM historial_clinico 
															WHERE id_cliente=".$id_cliente) 
															or die(mysql_error());
					$row3 = mysql_fetch_array($consulta_estudios);
			
					$notas_clinicas = $row3["id_nota_clinica"];
					$fecha = $row3["fecha"];
					$fecha_normal = cambiaf_a_normal($fecha);
	?>
                    <table style="border-collapse:collapse; width:350px;">
                        <tr>
                            <th width="80">Nº Estudio</th>
                            <th width="150">Fecha del Estudio</th>
                        </tr>
                        
                        <tr>
                            <td  style="text-align:center;"><?php echo $notas_clinicas; ?></td>
                            <td style="text-align:center;"><?php echo $fecha_normal; ?></td>
                        </tr>        
                    </table>
    <?php
				}
	?>
				</center>
                <br />    
                </div><!-- Fin de contenido proveedor -->
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>