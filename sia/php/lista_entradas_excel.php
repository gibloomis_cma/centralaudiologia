<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBEN LAS VARIABLES DEL FORMULARIO POR METODO POST
	$fecha_inicio_mysql = $_POST['fecha_inicio'];
	$fecha_final_mysql = $_POST['fecha_final'];
	$id_categoria = $_POST['id_categoria'];
	$hora = $_POST['hora'];
	$fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio_normal = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	$fecha_final_separada = explode("-", $fecha_final_mysql);
	$fecha_final_normal = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];

	// SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=lista_entradas.xls");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<!--<link rel="stylesheet" href="../css/style3.css" type="text/css">
	<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
	<script src="../js/jquery-1.7.1.js"></script>
	<script src="../js/ui/jquery.ui.core.js"></script>
	<script src="../js/ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../css/themes/base/demos.css">-->
</head>
<body>
<div id="wrapp">       
	<div id="contenido_columna2">
    	<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria" style="width:405px">
                	
           		</div>
        	</div><!--Fin de fondo titulo-->
        	<div class="area_contenido1">
            	<div class="contenido_proveedor">
            	<br />
               		<!--<div class="titulos"> Consulta de Entradas a Almacen General </div>-->
            	<br />
            	</div><!--Fin de contenido proveedor-->
    				<center>
	                    <style>
							td
							{
								font-size:10px;
							}
							th
							{
								font-size:11px;
							}
						</style>
                    <table border="1">
                    	<tr>
                    		<td colspan="6" style="text-align:center; color:#FFF; font-size:22px; background-color:#7f7f7f;"> 
                    			<?php 
                    				if ( $id_categoria == 1 )
                    				{
                    				?>
                    					<label> <?php echo "REPORTE DE REFACCIONES QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    				elseif( $id_categoria == 4 )
                    				{
                    				?>
                    					<label> <?php echo "REPORTE DE BATERIAS QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    				else
                    				{
                    				?>
                    					<label> <?php echo "REPORTE DE AUXILIARES QUE ENTRAN"; ?> </label>
                    				<?php
                    				}
                    			?> 
                    		</td>
                    	</tr>
                    	<tr>
                    		<td colspan="6" style="text-align:center; font-size:18px; color:#FFF; background-color:#7f7f7f;"> <label> DEL: <?php echo $fecha_inicio_normal; ?> &nbsp;&nbsp; AL: <?php echo $fecha_final_normal; ?> </label> </td>
                    	</tr>
                        <tr>
                        	<th style="font-size:14px; background-color:#7f7f7f; color:#FFF;">Fecha de Entrada</th>
                            <th style="font-size:14px; background-color:#7f7f7f; color:#FFF;">C&oacute;digo</th>
                            <th style="font-size:14px; background-color:#7f7f7f; color:#FFF;">Concepto</th>
                            <th style="font-size:14px; background-color:#7f7f7f; color:#FFF;">Cantidad</th>
                            <th style="font-size:14px; background-color:#7f7f7f; color:#FFF;"> Observaciones </th>
                            <th style="font-size:14px; background-color:#7f7f7f; color:#FFF;"> Costo Total </th>
                        </tr>
   						<?php 
							$buscar_subcategorias = mysql_query("SELECT id_subcategoria FROM subcategorias_productos 
													 			 WHERE id_categoria = ".$id_categoria) or die(mysql_error());
	
							$n_entradas = 0;
							$costo_total = 0;
							$total_entro = 0;
							$total_piezas = 0;
							while($row_subcategorias = mysql_fetch_array($buscar_subcategorias))
							{
								$id_subcategorias = $row_subcategorias["id_subcategoria"];	
								$consulta_folios_venta = mysql_query("SELECT entrada_almacen_general.id_registro AS id_registro, entrada_almacen_general.num_serie_cantidad AS num_serie_cantidad, entrada_almacen_general.fecha_entrada AS fecha_entrada, entrada_almacen_general.hora AS hora, entrada_almacen_general.id_articulo AS id_articulo, entrada_almacen_general.id_subcategoria AS id_subcategoria, observaciones, referencia_codigo,precio_compra
														  			  FROM entrada_almacen_general, inventarios,base_productos
														  			  WHERE entrada_almacen_general.fecha_entrada BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
														  			  AND entrada_almacen_general.id_almacen = '1'
														  			  AND entrada_almacen_general.id_subcategoria = '$id_subcategorias'
														  			  AND entrada_almacen_general.id_subcategoria = inventarios.id_subcategoria
														  			  AND entrada_almacen_general.id_almacen = inventarios.id_almacen
														  			  AND entrada_almacen_general.fecha_entrada = inventarios.fecha_entrada
														  			  AND entrada_almacen_general.id_articulo = inventarios.id_articulo
														  			  AND entrada_almacen_general.hora = inventarios.hora
														  			  AND entrada_almacen_general.id_articulo = id_base_producto
														  			  AND inventarios.id_articulo = id_base_producto
														  			  ORDER BY id_registro DESC") or die(mysql_error());

								while($row_folios_venta = mysql_fetch_array($consulta_folios_venta))
								{
									$num_serie_cantidad = $row_folios_venta["num_serie_cantidad"];
									$fecha_entrada = $row_folios_venta["fecha_entrada"];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$hora = $row_folios_venta["hora"];
									$id_registro = $row_folios_venta["id_registro"];
									$id_articulo = $row_folios_venta["id_articulo"];
									$id_subcategoria = $row_folios_venta["id_subcategoria"];
									$observaciones = $row_folios_venta['observaciones'];
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$precio_compra = $row_folios_venta['precio_compra'];
									$codigo = $row_folios_venta['referencia_codigo'];
									$n_entradas++;
									$costo_total = $num_serie_cantidad * $precio_compra;
									$total_entro = $total_entro + $costo_total;
									$total_piezas = $total_piezas + $num_serie_cantidad;
									$consulta_base_producto = mysql_query("SELECT descripcion 
																		   FROM base_productos 
																		   WHERE id_base_producto=".$id_articulo) or die(mysql_error());
									$row_base_producto = mysql_fetch_array($consulta_base_producto);
									$descripcion_base_producto = $row_base_producto["descripcion"];
					
								?>
		    						<tr>
		                                <td style="font-size:12px; text-align:center;"><?php echo $fecha_entrada_normal." ".$hora; ?></td>
		                                <td style="font-size:12px; text-align:center;"> <?php echo $codigo; ?> </td>
		                                <td style="font-size:12px;"><?php echo $descripcion_base_producto; ?></td>
		                                <td style="font-size:12px; text-align:center;"><?php echo $num_serie_cantidad; ?></td>
		                                <td style="font-size:12px;"><?php echo $observaciones; ?></td>
		                                <td style="font-size:12px;"> <?php echo "$".number_format($costo_total,2); ?> </td>
		                            </tr>
  								<?php
								}
							}					
				if($n_entradas==0)
				{
				?>
                    <tr>
                        <td style="text-align:center;" colspan="4">
                            <label class="textos">"No hay Entradas registradas"</label>
                        </td>
                    </tr>        
				<?php		
				}
				else
				{
				?>
					<tr>
						<td> <br/> </td>
					</tr>
					<tr>
		                <td style="color:#b60b0b; font-size:12px;"> Fecha y hora de impresi&oacute;n: </td>
		                <td style="color:#b60b0b; font-size:12px;"> <?php echo date('d/m/Y')." ".$hora; ?> </td>
		                <td style="text-align:right; color:#b60b0b; font-weight:bold; font-size:12px;"> <label> Total Piezas: </label> </td>
		                <td style="text-align:center; color:#b60b0b; font-weight:bold; font-size:12px;"> <label> <?php echo $total_piezas; ?> </label> </td>
		                <td style="text-align:right; color:#b60b0b; font-weight:bold; font-size:12px;"> <label> Total que Entro: </label> </td>
		                <td style="text-align:center; color:#b60b0b; font-weight:bold; font-size:12px;"> <label> <?php echo "$".number_format($total_entro,2); ?> </label> </td>
		            </tr>
					</table>
                        </center>
				<?php
				}
	?>
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>