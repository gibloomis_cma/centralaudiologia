<?php
		include("config.php");
		//include("metodo_cambiar_fecha.php");
		$folio_num_molde = $_GET["folio_num_molde"];

		// SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LA NOTA DE MOLDE A IMPRIMIR
		$query_nota_molde = "SELECT *
							 FROM moldes 
							 WHERE folio_num_molde = '$folio_num_molde'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
		$resultado_nota_molde = mysql_query($query_nota_molde) or die(mysql_error());

		// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
		$row_nota_molde = mysql_fetch_array($resultado_nota_molde);

		// SE DECLARAN VARIABLES CON EL RESULTADO FINAL DEL QUERY
		$id_cliente = $row_nota_molde["id_cliente"];
		$nombre = $row_nota_molde["nombre"];
		$paterno = $row_nota_molde["paterno"];
		$materno = $row_nota_molde["materno"];
		$telefono = $row_nota_molde["descripcion"];
		$calle = $row_nota_molde["calle"];
		$num_exterior = $row_nota_molde["num_exterior"];
		$num_interior = $row_nota_molde["num_interior"];
		$codigo_postal = $row_nota_molde["codigo_postal"];
		$colonia = $row_nota_molde["colonia"];
		$id_estado = $row_nota_molde["id_estado"];
		$id_ciudad = $row_nota_molde["id_ciudad"];
		$ciudad = $row_nota_molde['ciudad'];
		$fecha_entrada = $row_nota_molde["fecha_entrada"];
		$fecha_entrada_separada = explode("-", $fecha_entrada);
		$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
		$lado_oido = $row_nota_molde['lado_oido'];
		$id_estilo = $row_nota_molde['id_estilo'];
		$id_material = $row_nota_molde['id_material'];
		$id_color = $row_nota_molde['id_color'];
		$id_ventilacion = $row_nota_molde['id_ventilacion'];
		$id_salida = $row_nota_molde['id_salida'];
		$costo = $row_nota_molde['costo'];
		$observaciones = $row_nota_molde['observaciones'];
		$adaptacion = $row_nota_molde['adaptacion'];
		$reposicion = $row_nota_molde['reposicion'];
		$fecha_nacimiento = $row_nota_molde['fecha_nacimiento'];

		if ( $id_cliente == 0 && $fecha_nacimiento != "" ) 
		{
			$fecha_nacimiento_separada = explode("-", $fecha_nacimiento);
			$year = date('Y');
			$nacimiento = $fecha_nacimiento_separada[0];
			$edad = $year - $nacimiento;
		}
		elseif( $id_cliente != 0 && $fecha_nacimiento == "" )
		{
			// SE REALIZA QUERY QUE OBTIENE LA FECHA DE NACIMIENTO DE ACUERDO AL ID DEL CLIENTE OBTENIDO
			$query_fecha_nacimiento = "SELECT fecha_nacimiento
									   FROM ficha_identificacion
									   WHERE id_cliente = '$id_cliente'";

			// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO
			$resultado_query_fecha_nacimiento = mysql_query($query_fecha_nacimiento) or die(mysql_error());

			// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO 
			$row_fecha_nacimiento = mysql_fetch_array($resultado_query_fecha_nacimiento);
			$fecha_nacimiento = $row_fecha_nacimiento['fecha_nacimiento'];
			$fecha_nacimiento_separada = explode("/", $fecha_nacimiento);
			$year = date('Y');
			$nacimiento = $fecha_nacimiento_separada[2];
			$edad = $year - $nacimiento;
		}
		
		// SE REALIZA QUERY PARA OBTENER EL NOMBRE DEL ESTADO DE ACUERDO AL ID OBTENIDO ANTERIORMENTE
		$query_estado = "SELECT estado
						 FROM estados
						 WHERE id_estado = '$id_estado'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_estado = mysql_query($query_estado) or die(mysql_error());

		// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
		$row_estado = mysql_fetch_array($resultado_query_estado);

		// SE ALMACENA EL RESULTADO EN UNA VARIABLE FINAL
		$estado = $row_estado['estado'];

		// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA CIUDAD EN DONDE VIVE EL CLIENTE
		$query_ciudad = "SELECT ciudad
						 FROM ciudades
						 WHERE id_ciudad = '$id_ciudad' AND id_estado = '$id_estado'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_ciudad = mysql_query($query_ciudad) or die(mysql_error());

		// SE ALMACENA EL RESULTADO OBTENIDO EN FORMA DE ARREGLO
		$row_ciudad = mysql_fetch_array($resultado_query_ciudad);

		// SE DECLARA UNA VARIABLE CON EL NOMBRE DE LA CIUDAD
		$ciudad_consulada = $row_ciudad['ciudad'];

		// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL ESTILO
		$query_nombre_estilo = "SELECT estilo 
								FROM estilos,catalogo
								WHERE estilos.id_estilo = catalogo.id_estilo
								AND id_catalogo = '$id_estilo'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_estilo = mysql_query($query_nombre_estilo) or die(mysql_error());
		$row_nombre_estilo = mysql_fetch_array($resultado_query_estilo);
		$nombre_estilo = $row_nombre_estilo['estilo'];

		// SE REALIZA UERY QUE OBTIENE EL NOMBRE DEL MATERIAL
		$query_material = "SELECT material
						   FROM materiales
						   WHERE id_material = '$id_material'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_material = mysql_query($query_material) or die(mysql_error());
		$row_material = mysql_fetch_array($resultado_query_material);
		$nombre_material = $row_material['material'];

		// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL COLOR
		$query_color = "SELECT color
						FROM colores
						WHERE id_color = '$id_color'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_color = mysql_query($query_color) or die(mysql_error());
		$row_color = mysql_fetch_array($resultado_query_color);
		$nombre_color = $row_color['color'];

		// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA VENTILACION
		$query_ventilacion = "SELECT nombre_ventilacion,calibre
							  FROM ventilaciones
							  WHERE id_ventilacion = '$id_ventilacion'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_ventilacion = mysql_query($query_ventilacion) or die(mysql_error());
		$row_ventilacion = mysql_fetch_array($resultado_query_ventilacion);
		$nombre_ventilacion = $row_ventilacion['nombre_ventilacion'];
		$calibre = $row_ventilacion['calibre'];

		// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SALIDA
		$query_salida = "SELECT salida
						 FROM salidas
						 WHERE id_salida = '$id_salida'";

		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
		$resultado_query_salida = mysql_query($query_salida) or die(mysql_error());
		$row_salida = mysql_fetch_array($resultado_query_salida);
		$nombre_salida = $row_salida['salida'];

		// SE OBTIENE EL QUERY QUE OBTIENE QUIEN ATENDIO AL CLIENTE
		$query_recibio = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado_recibio,fecha,hora
						  FROM movimientos_moldes,empleados
						  WHERE folio_num_molde = '$folio_num_molde' 
						  AND id_estado_movimiento = 2
						  AND id_estatus_moldes = 1
						  AND empleados.id_empleado = movimientos_moldes.id_empleado";

		// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
		$resultado_query_recibio = mysql_query($query_recibio) or die(mysql_error());
		$row_query_recibio = mysql_fetch_array($resultado_query_recibio);
		$recibio = $row_query_recibio['nombre_empleado_recibio'];
		$fecha_recibio_mysql = $row_query_recibio['fecha'];
		$hora_recibio = $row_query_recibio['hora'];
		$fecha_recibio_separada = explode("-", $fecha_recibio_mysql);
		$fecha_recibio = $fecha_recibio_separada[2]."/".$fecha_recibio_separada[1]."/".$fecha_recibio_separada[0];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Nota de Molde </title>
	<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
	<script>
		$(document).ready(function(){
			$('input[name="imprimir"]').click(function(){
				$(this).hide();
				window.print(); 
				$(this).show();
			});
		});
	</script>
	<style>
		.fondo_boton
		{
			background-image:url(../img/fondo_boton.jpg);
			color:#FFF;
			height: 23px;
			font-weight:bold;
			box-shadow: 2px 2px 7px rgb(0, 0, 0);
			-webkit-box-shadow: 2px 2px 7px rgb(0, 0, 0);
			-moz-box-shadow: 2px 2px 7px rgb(0, 0, 0);
		}

		.fondo_boton:hover
		{
			cursor:pointer;
			box-shadow: 2px 2px 10px rgb(0, 0, 0);	
			-webkit-box-shadow: 2px 2px 10px rgb(0, 0, 0);
			-moz-box-shadow: 2px 2px 10px rgb(0, 0, 0);
		}

		#contenido_formato
		{
			margin:0px auto;
			width:750px;
			margin-bottom: 20px;
		}

		.contenido_copia
		{
			width:750px;
			float:left;
		}

		.contenido_original
		{
			width:750px;
			float:left;	
		}

		label
		{
			font-size:10px;
			font-family:Arial, Helvetica, sans-serif;
		}

		span
		{
			font-size:14px;
			font-family:Arial, Helvetica, sans-serif;
			font-weight:bold;
		}

		#header
		{
			height:121px;
			width:750px;
			float:left;
		}

		.datos_control
		{
			height:100px;
			width:95px;
			float:left;
			margin-left: -27px;
		}

		.imagen
		{
			height:70px;
			float:left;
		}

		.textos_principal
		{
			float:left;
			text-align:center;
			width:515px;
			height:70px;
		}

		#contenido
		{
			margin: auto;
			width:520px;
			margin-bottom: 30px;
			background-color: yellow;
		}

		.textos
		{
			color:#6d6d6d;
			text-align:center;
			font-weight:bold;
			font-size:11px;
			font-family:Arial, Helvetica, sans-serif;
		}

		.recibimos
		{
			background-color:#ececec;
			border:1px solid;
			padding:3px;
		}

		.division1
		{
			height:65px;
			float:left;
			width:240px;
			margin-top:-45px;
			margin-left:170px;
		}

		.division2
		{
			height:65px;
			width:240px;
			float:left;
			margin-top:-45px;
			margin-left: 0px;
		}

		#alleft
		{
			text-align:left !important;
		}

		#alright
		{
			text-align:right !important;
		}

		.contenido_problemas
		{
			border:solid 1px;	
		}

		.contenido_fechas
		{
			width:600px;
			float: left;
			height:60px;
			margin-left: 90px;
			margin-top: 0px;
		}

		.contenido_garantia
		{
			border:1px solid;
			background-color:#ececec;
			float:left;
			width:518px;
			margin-top: 15px;
		}

		.datos_cliente
		{
			border: 1px solid;
			background-color: #ececec;
			/*float: left;*/
			margin: auto;
			width: 600px;
			margin-top: 15px;
			margin-bottom: 10px;
		}

		#descripcion_empresa
		{
			font-size:9px; 
			font-family:Arial, Helvetica, sans-serif;
		}

		#contenido_general
		{
			float: left;
			width: 750px;
			margin-top: -5px;
		}

		#datos_molde
		{
			margin: auto;
			width: 600px;
			margin-bottom: 15px;
			margin-top: 5px;
		}

		#datos_importantes
		{
			margin:auto;
			width: 518px;
			margin-top: 0px;
		}
	</style>
</head>
<body>
<div id="contenido_formato">
    <div class="contenido_copia">
    	<div id="header">
        	<div class="imagen">
        		<img src="../img/logo_reparacion.PNG" style="float:left; margin-left:5px; margin-top:5px;" width="160" />
            </div> <!-- FIN DIV IMAGEN -->
            <div class="textos_principal">
            	<center>
        			<span> Central de Audiolog&iacute;a </span> <br />
                    <label id="descripcion_empresa">Distribuidor de los aud&iacute;fonos m&aacute;s modernos de Europa y los E.U.A. <br /> Mayoreo y Menudeo. </label> <br/>
           			<b> MOLDE </b>
           		</center>
            </div> <!-- FIN DIV TEXTOS PRINCIPAL -->         
            <div class="datos_control">
           		<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top:3px;">
               		<label style="font-size:14px; font-weight:bold;"> Control: </label>
              	</div>
                <center>
                    <input name="folio" type="text" value="<?php echo $folio_num_molde; ?>" size="12" maxlength="9" readonly="readonly" style="text-align:center; border:none;" />
               	</center>
              	<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top:5px;">
                	<label style="font-size:14px; font-weight:bold;"> Fecha: </label>
                </div>
                <center>
            		<input name="fecha_entrada" type="text" readonly="readonly" value="<?php echo $fecha_entrada_normal; ?>" size="12" maxlength="9" style="text-align:center; border:none;" />
               	</center>
    		</div><!-- FIN DIV DATOS CONTROL -->
                <div class="division1">
	                <center>
	                	<label style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> Matriz </label> <br/>
	                	<label style="font-size:9px; font-family:Arial, Helvetica, sans-serif;">
	                    	Seb. Lerdo de Tejada 186, Centro, Morelia Mich.<br />
	                        Tel. 01 (443) 312-5984, 312-5994.<br />
	                        Lada sin costo 01 800 509 1499 <br />
	                        www.centraldeaudiologia.com                     
	                	</label>
	                </center>
                </div><!-- FIN DIV DIVISION 1 -->
                <div class="division2">
	                <center>
	                	<label style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> Sucursal </label> <br/>
	                	<label style="font-size:9px; font-family:Arial;">
	                    	Fray Antonio de San Miguel 1400, <br />
	                        Col Floresta Ocolusen, Morelia Mich.<br />
	                       	Tel. 01 (443) 147-2012<br />
	                        E-mail: contacto@centraldeaudiologia.com
	                	</label>
	               	</center>
                </div><!-- FIN DIV DIVISION 2 -->
        </div> <!-- FIN DIV HEADER -->
        <div id="contenido_general">
        	<div class="datos_cliente">
        		<table width="600">
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Nombre: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Tel&eacute;fono: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo $telefono; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Direcci&oacute;n: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo ucwords(strtolower($calle." # ".$num_exterior." ".$num_interior." ".$colonia)); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Edad: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo $edad." años"; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Estado: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo utf8_encode(ucwords(strtolower($estado))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Ciudad: </label> </td>	
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php if($id_ciudad == 0){ echo $ciudad; } else{ echo ucfirst(strtolower($ciudad_consulada)); } ?> </label> </td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS CLIENTE -->
        	<div id="datos_importantes">
        		<table width="518">
        			<tr>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Adaptaci&oacute;n: </label> </td>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Reposici&oacute;n: </label> </td>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Oido: </label> </td>
        			</tr>
        			<tr>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> S&iacute; </label>
        					<?php 
        						// SE VALIDA SI LA NOTA ES DE ADAPTACION
        						if ( $adaptacion == "si" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> No </label>
        					<?php
        						// SE VALIDA SI LA NOTA ES DE ADAPTACION
        						if ( $adaptacion == "" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> S&iacute; </label>
        					<?php 
        						// SE VALIDA SI LA NOTA ES DE REPOSICION
        						if ( $reposicion == "si" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> No </label>
        					<?php
        						// SE VALIDA SI LA NOTA ES DE REPOSICION
        						if ( $reposicion == ""  )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> Izq. </label>
        					<?php 
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "izquierdo" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> Der. </label>
        					<?php
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "derecho" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> Bilateral </label>
        					<?php
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "bilateral" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS IMPORTANTES -->
        	<div id="datos_molde">
        		<table width="600">
        			<tr>
        				<th colspan="4" style="font-size:12px; text-align:center;"> Caracteristicas del Molde </th>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Estilo: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_estilo))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Material: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_material))); ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Color: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_color))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Ventilaci&oacute;n: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_ventilacion)))." ".$calibre; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Salida: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_salida))); ?> </label> </td>
        				<td colspan="2"> <label style="font-weight:bold; font-size:10px;"> Observaciones: </label> &nbsp; <label> <?php echo utf8_encode(ucfirst(strtolower($observaciones))); ?> </label> </td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS MOLDE -->
        </div><!-- FIN DIV CONTENIDO GENERAL -->
        <div class="contenido_fechas">
                <div style="float:left; width:480px;">
	                <table width="518">
	                	<tr>
	                        <td width="50" id="alright"> <label class="textos">Recibio: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo ucwords(strtolower($recibio)); ?> </td>
	                        <td width="30" id="alright"> <label class="textos">Fecha: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo $fecha_recibio; ?> </td>
	                        <td width="30" id="alright"> <label class="textos">Hora: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo $hora_recibio; ?> </td>
	                    </tr>
	                    <tr>
	                        <td width="50" id="alright"> <label class="textos">Fabrico: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                        <td width="30" id="alright"> <label class="textos">Fecha: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                        <td width="30" id="alright"> <label class="textos">Hora: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                    </tr>
	                    <tr>
	                        <td width="50" id="alright"><label class="textos">Entrego: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                        <td width="30" id="alright"><label class="textos">Fecha: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                        <td width="30" id="alright"><label class="textos">Hora: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                    </tr>
	                </table>
                </div>
                <div style="height:57px; width:100px; float:right; margin-left:12px; margin-right:-40px; margin-top:10px; padding-top:4px;">
	                <table width="100%" border="1" style="border-collapse:collapse;">
	                    <tr>
	                       	<td align="center"> <label class="textos" style="font-size:14px;"> Total: </label> </td>
	                    </tr>
	                    <tr>
	                    	<td align="center">
	                        	$
	                        	<?php 
	                        		// SE VALIDA SI ES DE ADAPTACION O REPOSICION EL COSTO TOTAL ES 0 SINO SE COLOCA EL COSTO DEL MOLDE
	                        		if ( $adaptacion == "si" || $reposicion == "si" )
	                        		{
	                        			echo "0.00";
	                        		}
	                        		else
	                        		{
	                        			echo number_format($costo,2);
	                        		}
	                        	?>
	                        </td> 
	                    </tr>
	                </table>
                </div>
        </div><!-- FIN DIV CONTENIDO FECHAS -->
    	<div id="contenido">
            <div class="contenido_garantia">
	            <center>
	            	<label style="font-size:7px; font-family:Arial, Helvetica, sans-serif;">
		            	NOTAS IMPORTANTES:<br />
			            CONSERVE ESTE RECIBO YA QUE ES EL COMPROBANTE PARA RECLAMAR SU MOLDE, SI LO EXTRAVIA, 
			            NO NOS HACEMOS RESPONSABLES.<br />
						NO NOS HACEMOS RESPONSABLES POR TRABAJOS DESPUES DE 30 DIAS.
					</label>
	            </center>
            </div><!-- FIN DIV CONTENIDO GARANTIA -->
        </div><!-- FIN DIV CONTENIDO -->
    </div> <!-- FIN DIV CONTENIDO COPIA -->
</div> <!-- FIN DIV CONTENIDO FORMATO -->

<div id="contenido_formato">
	<div style="float:left; width:750px; margin-top:40px; margin-bottom:40px; height:5px; border-bottom:2px dashed #000;"><br/></div>
    <div class="contenido_copia">
    	<div id="header">
        	<div class="imagen">
        		<img src="../img/logo_reparacion.PNG" style="float:left; margin-left:5px; margin-top:5px;" width="160" />
            </div> <!-- FIN DIV IMAGEN -->
            <div class="textos_principal">
            	<center>
        			<span> Central de Audiolog&iacute;a </span> <br />
                    <label id="descripcion_empresa">Distribuidor de los aud&iacute;fonos m&aacute;s modernos de Europa y los E.U.A. <br /> Mayoreo y Menudeo. </label> <br/>
           			<b> MOLDE </b>
           		</center>
            </div> <!-- FIN DIV TEXTOS PRINCIPAL -->         
            <div class="datos_control">
           		<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top:3px;">
               		<label style="font-size:14px; font-weight:bold;"> Control: </label>
              	</div>
                <center>
                    <input name="folio" type="text" value="<?php echo $folio_num_molde; ?>" size="12" maxlength="9" readonly="readonly" style="text-align:center; border:none;" />
               	</center>
              	<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top:5px;">
                	<label style="font-size:14px; font-weight:bold;"> Fecha: </label>
                </div>
                <center>
            		<input name="fecha_entrada" type="text" readonly="readonly" value="<?php echo $fecha_entrada_normal; ?>" size="12" maxlength="9" style="text-align:center; border:none;" />
               	</center>
    		</div><!-- FIN DIV DATOS CONTROL -->
                <div class="division1">
	                <center>
	                	<label style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> Matriz </label> <br/>
	                	<label style="font-size:9px; font-family:Arial, Helvetica, sans-serif;">
	                    	Seb. Lerdo de Tejada 186, Centro, Morelia Mich.<br />
	                        Tel. 01 (443) 312-5984, 312-5994.<br />
	                        Lada sin costo 01 800 509 1499 <br />
	                        www.centraldeaudiologia.com                     
	                	</label>
	                </center>
                </div><!-- FIN DIV DIVISION 1 -->
                <div class="division2">
	                <center>
	                	<label style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> Sucursal </label> <br/>
	                	<label style="font-size:9px; font-family:Arial;">
	                    	Fray Antonio de San Miguel 1400, <br />
	                        Col Floresta Ocolusen, Morelia Mich.<br />
	                       	Tel. 01 (443) 147-2012<br />
	                        E-mail: contacto@centraldeaudiologia.com
	                	</label>
	               	</center>
                </div><!-- FIN DIV DIVISION 2 -->
        </div> <!-- FIN DIV HEADER -->
        <div id="contenido_general">
        	<div class="datos_cliente">
        		<table width="600">
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Nombre: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Tel&eacute;fono: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo $telefono; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Direcci&oacute;n: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo ucwords(strtolower($calle." # ".$num_exterior." ".$num_interior." ".$colonia)); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Edad: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo $edad." años"; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Estado: </label> </td>
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php echo utf8_encode(ucwords(strtolower($estado))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Ciudad: </label> </td>	
        				<td> <label style="font-size:10px; text-decoration:underline;"> <?php if($id_ciudad == 0){ echo $ciudad; } else{ echo ucfirst(strtolower($ciudad_consulada)); } ?> </label> </td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS CLIENTE -->
        	<div id="datos_importantes">
        		<table width="518">
        			<tr>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Adaptaci&oacute;n: </label> </td>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Reposici&oacute;n: </label> </td>
        				<td style="text-align:center;"> <label style="font-weight:bold; font-size:12px;"> Oido: </label> </td>
        			</tr>
        			<tr>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> S&iacute; </label>
        					<?php 
        						// SE VALIDA SI LA NOTA ES DE ADAPTACION
        						if ( $adaptacion == "si" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> No </label>
        					<?php
        						// SE VALIDA SI LA NOTA ES DE ADAPTACION
        						if ( $adaptacion == "" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> S&iacute; </label>
        					<?php 
        						// SE VALIDA SI LA NOTA ES DE REPOSICION
        						if ( $reposicion == "si" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> No </label>
        					<?php
        						// SE VALIDA SI LA NOTA ES DE REPOSICION
        						if ( $reposicion == ""  )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        				<td style="text-align:center;">
        					<label style="font-weight:bold;"> Izq. </label>
        					<?php 
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "izquierdo" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> Der. </label>
        					<?php
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "derecho" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        					<label style="font-weight:bold;"> Bilateral </label>
        					<?php
        						// SE VALIDA EL LADO DEL OIDO A FABRICAR
        						if ( $lado_oido == "bilateral" )
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
        						<?php
        						}
        						else
        						{
        						?>
        							<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
        						<?php
        						}
        					?>
        				</td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS IMPORTANTES -->
        	<div id="datos_molde">
        		<table width="600">
        			<tr>
        				<th colspan="4" style="font-size:12px; text-align:center;"> Caracteristicas del Molde </th>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Estilo: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_estilo))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Material: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_material))); ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Color: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_color))); ?> </label> </td>
        				<td> <label style="font-weight:bold; font-size:10px;"> Ventilaci&oacute;n: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_ventilacion)))." ".$calibre; ?> </label> </td>
        			</tr>
        			<tr>
        				<td> <label style="font-weight:bold; font-size:10px;"> Salida: </label> </td>
        				<td> <label> <?php echo utf8_encode(ucwords(strtolower($nombre_salida))); ?> </label> </td>
        				<td colspan="2"> <label style="font-weight:bold; font-size:10px;"> Observaciones: </label> &nbsp; <label> <?php echo utf8_encode(ucfirst(strtolower($observaciones))); ?> </label> </td>
        			</tr>
        		</table>
        	</div><!-- FIN DIV DATOS MOLDE -->
        </div><!-- FIN DIV CONTENIDO GENERAL -->
        <div class="contenido_fechas">
                <div style="float:left; width:480px;">
	                <table width="518">
	                	<tr>
	                        <td width="50" id="alright"> <label class="textos">Recibio: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo ucwords(strtolower($recibio)); ?> </td>
	                        <td width="30" id="alright"> <label class="textos">Fecha: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo $fecha_recibio; ?> </td>
	                        <td width="30" id="alright"> <label class="textos">Hora: </label> </td>
	                        <td style="border-bottom:1px solid #000; font-size:12px; text-align:center;"> <?php echo $hora_recibio; ?> </td>
	                    </tr>
	                    <tr>
	                        <td width="50" id="alright"> <label class="textos">Fabrico: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                        <td width="30" id="alright"> <label class="textos">Fecha: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                        <td width="30" id="alright"> <label class="textos">Hora: </label> </td>
	                        <td style="border-bottom:1px solid #000;">  </td>
	                    </tr>
	                    <tr>
	                        <td width="50" id="alright"><label class="textos">Entrego: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                        <td width="30" id="alright"><label class="textos">Fecha: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                        <td width="30" id="alright"><label class="textos">Hora: </label></td>
	                        <td style="border-bottom:1px solid #000;"> </td>
	                    </tr>
	                </table>
                </div>
                <div style="height:57px; width:100px; float:right; margin-left:12px; margin-right:-40px; margin-top:10px; padding-top:4px;">
	                <table width="100%" border="1" style="border-collapse:collapse;">
	                    <tr>
	                       	<td align="center"> <label class="textos" style="font-size:14px;"> Total: </label> </td>
	                    </tr>
	                    <tr>
	                    	<td align="center">
	                        	$
	                        	<?php 
	                        		// SE VALIDA SI ES DE ADAPTACION O REPOSICION EL COSTO TOTAL ES 0 SINO SE COLOCA EL COSTO DEL MOLDE
	                        		if ( $adaptacion == "si" || $reposicion == "si" )
	                        		{
	                        			echo "0.00";
	                        		}
	                        		else
	                        		{
	                        			echo number_format($costo,2);
	                        		}
	                        	?>
	                        </td> 
	                    </tr>
	                </table>
                </div>
        </div><!-- FIN DIV CONTENIDO FECHAS -->
    	<div id="contenido">
            <div class="contenido_garantia">
	            <center>
	            	<label style="font-size:7px; font-family:Arial, Helvetica, sans-serif;">
		            	NOTAS IMPORTANTES:<br />
			            CONSERVE ESTE RECIBO YA QUE ES EL COMPROBANTE PARA RECLAMAR SU MOLDE, SI LO EXTRAVIA, 
			            NO NOS HACEMOS RESPONSABLES.<br />
						NO NOS HACEMOS RESPONSABLES POR TRABAJOS DESPUES DE 30 DIAS.
					</label>
	            </center>
            </div><!-- FIN DIV CONTENIDO GARANTIA -->
        </div><!-- FIN DIV CONTENIDO -->
    </div> <!-- FIN DIV CONTENIDO COPIA -->
    <p style="float:right;">
    	<input type="button" name="imprimir" id="imprimir" class="fondo_boton" value="Imprimir" title="Imprimir Nota de Molde" />
    </p>
</div> <!-- FIN DIV CONTENIDO FORMATO -->
</body>
</html>