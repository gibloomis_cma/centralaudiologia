<?php
	error_reporting(0);

	// SE INICIA SESION PARA EL USUARIO QUE ENTRO AL SISTEMA
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Entrada de Refacciones</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
<script type="text/javascript">
	$(function() {
    	//call the function onload, default to page 1
    	getdata( 1 );
	});
		
	function getdata( pageno ){                     
    	var targetURL = 'lista_refacciones_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_refacciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_refacciones').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow');
	}
	
	function getdata2( ){   
		var valor = $('#paginas option:selected').attr('value');
		var targetURL = 'lista_refacciones_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_refacciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_refacciones').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow'); 
	}     
</script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Refacciones
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <div class="contenido_proveedor">
                <center>
                    <div id="tabla_lista_refacciones">
						<img src="../img/ajax-loader.gif"/>
					</div>
                </center>
                <br />
                </div><!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>