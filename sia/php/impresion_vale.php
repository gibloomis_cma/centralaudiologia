<?php
		include('config.php');
		$vale = mysql_query("SELECT id_vale_bateria, uso_bateria, fecha, hora, nombre, paterno, materno 
				FROM vales_baterias, uso_baterias, empleados 
				WHERE uso_de_bateria = id_uso_bateria
				AND responsable = id_empleado 
				ORDER BY id_vale_bateria DESC") or die(mysql_error());
		$row_vale = mysql_fetch_array($vale);
		$id_vale = $row_vale['id_vale_bateria'];
		$fecha = $row_vale['fecha'];
		$fecha_separada = explode("-", $fecha);
		$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
		$responsable = $row_vale['nombre']." ".$row_vale['paterno']." ".$row_vale['materno'];
		$uso = $row_vale['uso_bateria'];	
	?>
<!-- ************************************************************************************************ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		body
		{
			font-size:14px;	
			font-family:Arial;
		}
		
		#cont
		{
			margin:0px auto;
			text-align:center;		
			padding-bottom:5px;
		}
		
		#impresion
		{
			text-align:center;
			width:215px;
			font-size:14px;	
			font-family:Arial;
		}
		
		#impresion table
		{
			margin:0px auto;
			font-family:Arial;
			font-size:12px;	
			font-size:10px;
		}
		
		#impresion table tr th
		{
			font-size:12px;	
			font-family:Arial;
		}
		
		#impresion table tr td
		{
			text-align:right;
			padding:5px 5px;
			font-size:12px;	
			font-family:Arial;
		}
		
		span
		{
			text-decoration:underline;
			font-size:14px;
			font-family:Arial;
		}
	</style>
</head>
<body onload="javascript: window.print();">
	<div id="cont">
		<div id="impresion">
			JORGE LEAL Y CIA. S.A. DE C.V. <br />
			Sebastian Lerdo de Tejada 186<br /> 
			Col. Centro     Morelia     Michoacan <br />
			CP. 58000 <br/> 
			RFC: JLE010622GK2 <br />
			Telefono: (443) 312-5994 <br />
			Vale de Baterias <br />
			Fecha: <?php echo $fecha; ?><br />
			N° de Folio: <?php echo $id_vale; ?> <br/>
			<table>	
			    <tr>
			    	<td style="font-family:Arial; text-align:center;"> Descripción </td>
			        <td style="font-family:Arial; text-align:center;"> Cantidad </td>
			    </tr>
			    <tr>
			    	<td colspan="2"> ------------------------------------------------- </td>
			    </tr>
			<?php
				$descripcion_vale=mysql_query('SELECT almacen, descripcion, cantidad 
											   FROM descripcion_vale_baterias, base_productos, almacenes
											   WHERE codigo = id_base_producto
											   AND origen = id_almacen
											   AND id_vale_bateria='.$id_vale) or die(mysql_error());
				$x=0;
				while($row_descripcion_vale=mysql_fetch_array($descripcion_vale))
				{
					$codigo = $row_descripcion_vale['descripcion'];
					$cantidad = $row_descripcion_vale['cantidad'];
					$origen = $row_descripcion_vale['almacen'];
					$x = $x + $cantidad;
			?>
				<tr> 
			    	<td style="font-family:Arial; text-align:center;"> <?php echo $codigo; ?> </td>
			        <td style="font-family:Arial;"> <?php echo $cantidad; ?> </td>
			    </tr>
			<?php
				}
			?>
				<tr>
			    	<td></td>
			        <td style="font-family:Arial;">Total: <?php echo $x; ?></td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:left;"> Uso: <span style="font-family:Arial; font-size:12px;"> <?php echo $uso; ?> </span> </td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:left;"> Origen: <span style="font-family:Arial; font-size:12px;"> <?php echo $origen; ?> </span> </td>
			    </tr>
			    <tr>
			    	<td colspan="2"> <br/> </td>
			    </tr>
			    <tr>
			    	<td colspan="2"> <br/> </td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:center;"> 
			    		-------------------------------------------------  
			    		<br/>
			    		Responsable
			    		<br/>
			    		<?php echo ucwords(strtolower($responsable)); ?>
			    	</td>
			    </tr>
			</table>
		</div>
			<p>&nbsp;&nbsp;</p>
	</div>
</body>
</html>
<!-- ************************************************************************************************ -->