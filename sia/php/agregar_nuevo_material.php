<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Nuevo Material </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
			<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Materiales
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO1 -->
							<div class="area_contenido1">
								<br/>
								<?php
									// IMPORTACION DEL ARCHIVO DE CONEXION DE LA BASE DE DATOS 
									include("config.php");
									
									// QUERY QUE OBTIENE LOS MATERIALES DE LA BASE DE DATOS
									$query_materiales = mysql_query("SELECT id_material, material, precio
																	 FROM materiales
																	 ORDER BY id_material ASC") or die (mysql_error());
								?>
								<center>
									<table>
										<tr>
											<th width="120"> N° de Material </th>
											<th> Nombre Material </th>
                                            <th width="80"> Precio </th>
                                            <th width="25"></th>
										</tr>
									<?php
										while( $row_material = mysql_fetch_array($query_materiales) )
										{
											$id_material = $row_material['id_material'];
											$nombre_material = $row_material['material'];
											$precio = $row_material['precio'];
									?>
										<tr>
											<td id="centrado"> <?php echo $id_material ?> </td>
											<td> <?php echo utf8_encode(ucwords(strtolower($nombre_material))); ?> </td>
                                            <td> <?php echo "$".number_format($precio,2); ?></td>
                                            <td> <a href="modificar_material.php?id_material=<?php echo $id_material; ?>">
                                            		<img src="../img/modify.png" />
                                                 </a>
                                            </td>
										</tr>
									<?php
										}
									?>
									</table><!-- FIN TABLA -->
								</center><!-- FIN CENTER -->
								<br/>
								<div class="titulos"> Agregar Nuevo Material </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
								<form name="form_nuevo_material" id="form_nuevo_material" method="post" action="procesa_agregar_nuevo_material.php" onsubmit="return validaAgregarNuevoMaterial()">
									<table>
										<tr>
											<td id="alright"> 
                                            	<label class="textos"> Nombre del Material: </label> 
                                            </td>
											<td id="alleft">
                                            	<input type="text" name="txt_nombre_material" id="txt_nombre_material"/>
                                            </td>
										</tr><tr>
                                        	<td id="alright">
                                            	<label class="textos"> Precio: </label>
                                           	</td>
                                            <td id="alleft">
                                            	$<input name="precio" type="text" size="10" maxlength="10" />
                                            </td>
                                        </tr><tr>
											<td id="alright">
                                            	<label class="textos"> Descripción: </label>
                                            </td>
											<td id="alleft">
                                            	<textarea name="txt_descripcion" id="txt_descripcion"></textarea>
                                           	</td>
										</tr><tr>
											<td colspan="2"> 
												<p align="right">
													<input type="reset" name="accion" id="accion" value="Cancelar" title="Cancelar" class="fondo_boton"/>&nbsp;&nbsp; <input type="submit" name="accion" id="accion" class="fondo_boton" value="Aceptar" title="Aceptar"/> 
												</p>
											</td>
										</tr>
									</table><!-- FIN TABLA -->
								</form><!-- FIN FORM -->
								</center><!-- FIN CENTER -->
							</div><!-- FIN DIV AREA CONTENIDO1 -->
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html>