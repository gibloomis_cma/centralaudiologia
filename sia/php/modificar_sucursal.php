<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Departamentos</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<input name="id_forma_contacto[]" type="hidden" /><label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label></td><td><input type="text" name="descripcion[]" /> <a href="#"><img src="../img/delete.png" /></a><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="wrapp">
	<div id="contenido_columna2">            
		<div class="contenido_pagina">    
			<div class="fondo_titulo1">       
            	<div class="categoria">
            		Sucursales
            	</div>
        	</div><!--Fin de fondo titulo-->       
 	<?php 
			include("config.php");
			$id_sucursal = $_GET["id_sucursal"];						
			$consulta_sucursal = mysql_query("SELECT * FROM sucursales 
															WHERE id_sucursal=".$id_sucursal) 
															or die(mysql_error());
			$row = mysql_fetch_array($consulta_sucursal);
			$sucursal = $row["nombre"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$colonia = $row["colonia"];
			$codigo_postal=$row['codigo_postal'];
			$id_estado=$row['id_estado'];
			$id_ciudad=$row['id_ciudad'];
	?>                
        	<div class="area_contenido1">
        	<br />
            	<div class="contenido_proveedor">
                    <form name="forma1" action="proceso_modificar_sucursal.php" method="post">
                    <table>
                        <tr>
                            <th colspan="4">
                                Modificar Sucursales
                                <input name="id_sucursal" type="hidden" value="<?php echo $id_sucursal; ?>"/>
                            </th>
                        </tr><tr>
                            <td id="alright">            
                    			<label class="textos">Nombre: </label>
                        	</td><td id="alleft" colspan="3">
                    			<input name="sucursal" type="text" value="<?php echo $sucursal; ?>" style="width:200px;"/>
							</td>
                        </tr><tr>
                            <td id="alright">
                				<label class="textos">Calle: </label>
                        	</td><td id="alleft">
                        		<input name="calle" type="text" value="<?php echo $calle; ?>" style="width:200px;" />
                        	</td><td id="alright">
                        		<label class="textos">Numero: </label>
                        	</td><td id="alleft">
                            	<label class="textos">Ext. </label>
                                <input name="num_exterior" type="text" size="4" maxlength="6" value="<?php echo $num_exterior; ?>"/>
                                <label class="textos"> Int. </label>
                                <input name="num_interior" type="text" size="4" maxlength="6" value="<?php echo $num_interior; ?>"/>
                        	</td>
                        </tr><tr>
                        	<td id="alright">
                        		<label class="textos">Colonia: </label>
                        	</td><td id="alleft">
                        		<input name="colonia" type="text" value="<?php echo $colonia; ?>" style="width:200px;" />
                        	</td><td id="alright">
                            	<label class="textos">Codigo Postal: </label>
                            </td><td id="alleft">
                             	<input name="codigo_postal" type="text" size="4" maxlength="6" value="<?php echo $codigo_postal; ?>"/>
                            </td>
                    	</tr><tr>
                        	<td id="alright">
                            	<label class="textos">Estado: </label>
                            </td><td id="alleft">
                            	<select name="id_estado" id="id_estado" style="width:200px;">
								<?php
									$estados=mysql_query('SELECT * from estados')or die(mysql_error());
									while($row_estados=mysql_fetch_array($estados)){
										$id_estado2=$row_estados['id_estado'];
										$estado=ucwords(strtolower($row_estados['estado']));
										if($id_estado2==$id_estado){
											$sel='selected="selected"';
										}else{
											$sel=" ";
										}
								?>
                                <option value="<?php echo $id_estado2; ?>" <?php echo $sel; ?> >
									<?php echo utf8_encode($estado); ?>
                                </option>
                                <?php
									}
								?>
                                </select>
                            </td><td id="alright">
                            	<label class="textos">Ciudad: </label>
                            </td><td id="alleft">
                            	<select name="id_municipio" id="id_municipio" style="width:200px;">
								<?php
									$ciudades=mysql_query('SELECT * from ciudades WHERE id_estado='.$id_estado)or die(mysql_error());
									while($row_ciudades=mysql_fetch_array($ciudades)){
										$id_ciudad2=$row_ciudades['id_ciudad'];
										$ciudad=ucwords(strtolower($row_ciudades['ciudad']));
										if($id_ciudad2==$id_ciudad){
											$sel='selected="selected"';
										}else{
											$sel=" ";
										}
								?>
                                <option value="<?php echo $id_ciudad; ?>" <?php echo $sel; ?> >
									<?php echo utf8_encode($ciudad); ?>
                                </option>
                                <?php
									}
								?>
                                </select>
                            </td>
                        </tr><tr> 
                        	<td id="alright" colspan="3"> 
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input name="municipio" type="text" style="width:200px;" />
                            </td>
                        </tr><tr> 
                    		<td style="text-align:center" colspan="4">
                            	<label class="textos">- Contacto -</label>
                        	</td>
                        </tr>
								<?php
                                    $consulta_forma_contacto=mysql_query("SELECT * FROM forma_contacto_sucursal
                                                                        WHERE id_sucursal=".$id_sucursal)
                                                                        or die(mysql_error());
                                    while($row5 = mysql_fetch_array($consulta_forma_contacto)){
                                    $id_forma_contacto_sucursal = $row5["id_forma_contacto_sucursal"];
                                    $tipo = $row5["tipo"];
                                    $descripcion = $row5["descripcion"];	            
    
                                ?>
                       	<tr>  
                    		<td style="text-align:center" colspan="4"> 
                                <input name="id_contacto_empleado[]" type="hidden" value="<?php echo $id_forma_contacto_sucursal; ?>" />                            
                                <label class="textos">Tipo:</label>
                                <select name="tipo_telefono[]">
                                <?php
										$formas_contacto[0] = "Telefono";
										$formas_contacto[1] = "Celular";
										$formas_contacto[2] = "Fax";
										$formas_contacto[3] = "Correo";						 
										for($i=0; $i<4; $i++){
											if($tipo == $formas_contacto[$i]){							
                                ?>                                                           
                                        <option value="<?php echo $tipo; ?>" selected="selected">
                                            <?php echo $tipo; ?>
                                        </option>
                                <?php 
                                        	}else{
                                ?>                            
                                    <option value="<?php echo $formas_contacto[$i]; ?>">
                                        <?php echo $formas_contacto[$i]; ?>
                                    </option>
                                <?php
											}
										}
                                ?>
                                </select>
                                <label class="textos">Descripción:</label>
                                <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>" />
                                <a href="eliminar_forma_contacto_sucursal.php?id_forma_contacto=<?php echo $id_forma_contacto_sucursal; ?>&id_sucursal=<?php echo $id_sucursal; ?>"><img src="../img/delete.png" title="Eliminar <?php echo $tipo; ?>" /></a>
                               <?php /* <input type="button" value="Eliminar" class="fondo_boton" 
                                title="Eliminar <?php echo $tipo; ?>"  
                                onclick="window.location.href='eliminar_forma_contacto_sucursal.php?id_forma_contacto=<?php echo $id_forma_contacto_sucursal; ?>&id_sucursal=<?php echo $id_sucursal; ?>'"/>*/?><br /><br />    
								
							</td>
                		</tr>		
								<?php 
                                    }
                                ?>
						<tr>                        
                        	<td style="text-align:center" colspan="4"><br />                         
                            	<div id="nuevo" style="margin-top:-10px"></div>
                        	</td>
                		</tr><tr>      
                    		<td colspan="4" style="text-align:center">                  
								<input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>
                       		</td>
                   		</tr><tr>                        
                        	<td id="alright" colspan="4">
                            	<input type="button" name="volver" value="Volver" class="fondo_boton"
                                onclick="window.location.href='lista_sucursales.php'" />
                            	<input name="accion" type="submit" value="Modificar" class="fondo_boton"/>
                            </td>
                        </tr>
                    </table>                    
                    </form>
             	</div><!-- Fin de contenido proveedor -->
        	</div><!-- Fin de area contenido -->
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>