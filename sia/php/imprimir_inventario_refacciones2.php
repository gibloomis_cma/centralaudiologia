<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');
    // SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DE LAS BATERIAS
    $query_refacciones = "SELECT id_base_producto,referencia_codigo,descripcion,precio_compra
                          FROM base_productos
                          WHERE id_categoria = 1
                          ORDER BY descripcion ASC";
    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_refacciones = mysql_query($query_refacciones) or die(mysql_error());
    // SE DECLARA VARIABLE PARA CALCULAR EL COSTO TOTAL        
    $contador = 0; $aGeneral = 0; $aMatriz = 0; $aOcolusen = 0; $costosTotales = 0;
    $entradaGeneral=0; $entradaMatriz=0; $entradaOcolusen=0;
    $salidaGeneral=0; $salidaMatriz=0; $salidaOcolusen=0;
    $totalGeneral=0; $totalMatriz=0; $totalOcolusen=0;
    $generalInicial=0;
    $fechaInicio1="";
    $fechaFinal1="";
    $fechaActual=date('Y-m-d');
    $inventarioInicial=0;
    $id_almacen[0]=1;$id_almacen[1]=2;$id_almacen[2]=4;    
    $inicialAlmacen[]="";
    if(isset($_GET['fechaI'])){
        $fechaInicio=$_GET['fechaI'];
        $fechaFinal=$fechaActual;             
        /*$separarInicio=explode('/',$_POST['from']);
        $fechaInicio=$separarInicio[2]."-".$separarInicio[1]."-".$separarInicio[0];*/
        //$separarFinal=explode('/',$_POST['to']);
        //$fechaFinal=$separarFinal[2]."/".$separarFinal[1]."/".$separarFinal[0];
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Inventario de Refacciones </title>
	 
    <script language="javascript">
    function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  h + ':' + min + ':' + sec;
            window.print();
        }
    </script>
    <style type="text/css">
        .tot{
            color:#ac1f1f !important; 
            font-size: 12px !important;  
            text-align: center !important; 
        }
    </style>
</head>
<body onload="hora()">
<center>
    <table>
        <tr>
            <td colspan="8" style="font-size: 22px; color: #000; font-weight: bold; text-align: center;"> INVENTARIO DE BATERIAS </td>
        </tr>
        <tr>
            <td id="hora" colspan="8" style="font-size: 18px; color: #000; font-weight: bold; text-align: center;"> Fecha y hora de impresi&oacute;n: &nbsp; <?php echo date('d/m/Y'); ?> &nbsp; </td>
        </tr>
    </table>
                    <table border="1">
                        <tr>
                            <th style="font-size: 14px;"> C&oacute;digo </th>
                            <th style="font-size: 14px;"> Concepto </th>                           
                            <th style="font-size: 14px;"> Inventario Inicial </th>                            
                            <th style="font-size: 14px;"> Almac&eacute;n General </th>
                            <th style="font-size: 14px;"> Almac&eacute;n Recepci&oacute;n Matriz </th>                            
                            <th style="font-size: 14px;"> Almac&eacute;n Recepci&oacute;n Ocolusen </th>
                            <th style="font-size: 14px;"> Costo Total </th>
                        </tr>
                        <?php
// SE REALIZA CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                            while( $row_refacciones = mysql_fetch_array($resultado_refacciones)){
                                $id_base_producto = $row_refacciones['id_base_producto'];
                                $codigo = $row_refacciones['referencia_codigo'];
                                $concepto = $row_refacciones['descripcion'];
                                $precio_compra = $row_refacciones['precio_compra'];
                                $entradas=0;$pases=0;$salidas=0;$iniciales=0; 
                                $entradaGeneral=0; $entradaMatriz=0; $entradaOcolusen=0;
                                $salidaGeneral=0; $salidaMatriz=0; $salidaOcolusen=0;                             
                                // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO TOTAL DE CADA REFACCION                                                                                    
//CONSULTA DE EXISTENCIAS FECHAS ATRAS----------------------------------------------------------------------------------------//                                    
                                    $inventariosInicial = mysql_query("SELECT SUM(num_serie_cantidad) AS enInventario
                                                                       FROM inventarios
                                                                       WHERE id_articulo = ".$id_base_producto."                                                                       
                                                                       AND fecha_entrada <= '".$fechaInicio."'")
                                                                       or die('Consultas Totales por Almacen');                                
                                    $rowInventariosInicial=mysql_fetch_array($inventariosInicial);                                    
                                    $iniciales=$rowInventariosInicial['enInventario'];                              
//ENTRADAS INICIAL------------------------------------------------------------------------------------------------------------//
                                    $inicialEntradas =  mysql_query("SELECT SUM(num_serie_cantidad) AS totalEntrada
                                                                      FROM entrada_almacen_general
                                                                      WHERE id_articulo = ".$id_base_producto."                                                                      
                                                                      AND fecha_entrada BETWEEN '".$fechaInicio."'
                                                                      AND '".$fechaActual."'") 
                                                                      or die('Entradas Totales');                                
                                    $rowInicialEntradas = mysql_fetch_array($inicialEntradas);                                    
                                    $entradas=$rowInicialEntradas['totalEntrada'];
//PASES INICIAL----------------------------------------------------------------------------------------------------------------//                                    
                                    $inicialPases =  mysql_query("SELECT SUM(cantidad) as totalPase
                                                                  FROM vales_refacciones,descripcion_vale_refacciones
                                                                  WHERE vales_refacciones.id_registro = descripcion_vale_refacciones.id_vale_refaccion                                                                
                                                                  AND codigo = ".$id_base_producto."
                                                                  AND uso IN (2,3,4,5)
                                                                  AND fecha BETWEEN '".$fechaInicio."' 
                                                                  AND '".$fechaActual."'") 
                                                                  or die('Pases Totales');                                
                                    $rowInicialPases = mysql_fetch_array($inicialPases);                                    
                                    $pases=$rowInicialPases['totalPase'];
//SALIDAS INICIAL--------------------------------------------------------------------------------------------------------------//                                    
                                    $inicialSalidas =  mysql_query("SELECT SUM(cantidad) as totalSalida
                                                                    FROM descripcion_venta,ventas, sucursales 
                                                                    WHERE ventas.folio_num_venta = descripcion_venta.folio_num_venta
                                                                    AND fecha BETWEEN '".$fechaInicio."' AND '".$fechaActual."'
                                                                    AND id_categoria=1
                                                                    AND descripcion_venta.id_sucursal=sucursales.id_sucursal
                                                                    AND ventas.id_sucursal=sucursales.id_sucursal
                                                                    AND descripcion_venta.id_sucursal=ventas.id_sucursal                                                                   
                                                                    AND descripcion='".$concepto."'") 
                                                                    or die(mysql_error());                                
                                    $rowInicialSalidas = mysql_fetch_array($inicialSalidas);                                    
                                    $salidas=$rowInicialSalidas['totalSalida'];                                    
//CONSULTA DE EXISTENCIAS POR ALMANCEN----------------------------------------------------------------------------------------//                                    
                                    $inventariosAlmacenes = mysql_query("SELECT SUM(num_serie_cantidad) AS enInventario
                                                                       FROM inventarios
                                                                       WHERE id_articulo = ".$id_base_producto."                                                                       
                                                                       AND fecha_entrada BETWEEN '".$fechaInicio."'
                                                                       AND '".$fechaActual."'")
                                                                        or die('Consultas Totales por Almacen');                                
                                    $rowInventariosAlmacenes=mysql_fetch_array($inventariosAlmacenes);                                    
                                    $totalesAlmacen=$rowInventariosAlmacenes['enInventario'];
//------------------------------------------------------------------------------------------------------------------------//   
//CONSULTA DE ENTRADAS/SALIDAS POR ALMACEN-------------------------------------------------------------------------------------------//                                   
                                $distribucionEntradasSalidas =  mysql_query("SELECT cantidad, origen, uso
                                                                        FROM vales_refacciones, descripcion_vale_refacciones                                                                     
                                                                        WHERE codigo = ".$id_base_producto."
                                                                        AND vales_refacciones.id_registro = descripcion_vale_refacciones.id_vale_refaccion
                                                                        AND fecha BETWEEN '".$fechaInicio."'
                                                                        AND '".$fechaActual."'
                                                                        ORDER BY origen, uso") 
                                                                        or die(mysql_error());                                
                                while($rowDistribuciones = mysql_fetch_array($distribucionEntradasSalidas)){
                                    $distribucionCantidad = $rowDistribuciones['cantidad']; 
                                    $distribucionOrigen = $rowDistribuciones['origen'];
                                    $distribucionUsoBateria = $rowDistribuciones['uso'];
                                    $orige=""; $dest="--";
                                    if($distribucionOrigen==1){$salidaGeneral+=$distribucionCantidad; $orige="General";}
                                    if($distribucionOrigen==2){$salidaMatriz+=$distribucionCantidad; $orige="Matriz";}
                                    if($distribucionOrigen==4){$salidaOcolusen+=$distribucionCantidad; $orige="Ocolusen";}
                                    if($distribucionUsoBateria==6){$entradaGeneral+=$distribucionCantidad; $dest="General";}
                                    if($distribucionUsoBateria==7){$entradaMatriz+=$distribucionCantidad; $dest="Matriz";}
                                    if($distribucionUsoBateria==8){$entradaOcolusen+=$distribucionCantidad; $dest="Ocolusen";}
                                    if($distribucionUsoBateria==2){$dest="Laboratorio";}
                                    if($distribucionUsoBateria==3){$dest="Consultorio";}
									if($distribucionUsoBateria==4){$dest="Reparaciones";} 
									if($distribucionUsoBateria==5){$dest="Jornada";}                                   
                                }
//CONSULTA DE EXISTENCIAS FECHAS ATRAS----------------------------------------------------------------------------------------//                                    
                                    for($i=0;$i<3;$i++){
                                        //CONSULTA DE EXISTENCIA POR ALMACEN
                                        $distribucionInicial = mysql_query("SELECT SUM(num_serie_cantidad) AS enInventario
                                                                           FROM inventarios
                                                                           WHERE id_articulo = ".$id_base_producto."                                                                       
                                                                           AND fecha_entrada <= '".$fechaActual."'
                                                                           AND id_almacen = ".$id_almacen[$i])
                                                                           or die('Consultas Totales por Almacen');                             
                                        $rowdistribucionInicial=mysql_fetch_array($distribucionInicial);
                                        $inicialAlmacen[$i]=$rowdistribucionInicial['enInventario'];                                        
                                        //CONSULTA DE VENTAS POR ALMACEN
                                        if($i>0)
										{
										
											if($i==2)
											{
											$sucu = 2;
											}
											else{
											$sucu = 1;
											}
											
												$ventasAlmacenes=mysql_query("SELECT SUM(cantidad) as totalSalida
																			 FROM ventas, descripcion_venta, almacenes, base_productos
																			 WHERE ventas.fecha BETWEEN '".$fechaInicio."' 
																			 AND '".$fechaActual."' 
																			 AND ventas.id_sucursal=".$sucu."            		           		                                                             AND almacenes.id_sucursal=ventas.id_sucursal 
																			 AND almacenes.id_sucursal = descripcion_venta.id_sucursal
																			 AND descripcion_venta.id_categoria=1 
																			 AND ventas.folio_num_venta=descripcion_venta.folio_num_venta 
																			 AND almacenes.id_almacen=".$id_almacen[$i]." 
																			 AND id_base_producto = ".$id_base_producto."
																			 AND descripcion_venta.descripcion = base_productos.descripcion"
																			) or die(mysql_error());
																			 
											//}
											//else{
										
                                            
											//}
                                            $rowVentasAlmacenes = mysql_fetch_array($ventasAlmacenes);
                                            $ventasAlmacen[$i] = $rowVentasAlmacenes['totalSalida'];


                                            //CONSULTA DE EXISTENCIA POR ALMACEN
                                            $distribucionInicial = mysql_query("SELECT SUM(num_serie_cantidad) AS enInventario
                                                                               FROM inventarios
                                                                               WHERE id_articulo = ".$id_base_producto."                                                                       
                                                                               AND fecha_entrada <= '".$fechaActual."'
                                                                               AND id_almacen = ".$id_almacen[$i])
                                                                               or die('Consultas Totales por Almacen');                             
                                            $rowdistribucionInicial=mysql_fetch_array($distribucionInicial);
                                            $inicialAlmacen[$i]=$rowdistribucionInicial['enInventario'];  
                                        }
                                    }                                                                                                          
                                //RESULTADO DEL INVENTARIO INICIAL
                                $inventarioInicial=$iniciales+$salidas+$pases-$entradas+$totalesAlmacen;
                                $aGeneral=$inicialAlmacen[0]+$salidaGeneral-($entradaGeneral+$entradas);
                                $aMatriz=$inicialAlmacen[1]+$ventasAlmacen[1]+$salidaMatriz-$entradaMatriz;
                                $aOcolusen=$inicialAlmacen[2]+$ventasAlmacen[2]+$salidaOcolusen-$entradaOcolusen;
                                $almacenCosto=$inventarioInicial*$precio_compra;
                                $totalGeneral+=$aGeneral;
                                $totalMatriz+=$aMatriz;
                                $totalOcolusen+=$aOcolusen;
                                //Variables totales de busqueda
                                $generalInicial+=$inventarioInicial;
                                $costosTotales+=$almacenCosto;
                               //Si Hay inventario Imprimir Resultados
                                if($inventarioInicial > 0){
                                  $contador++;                                                                    
                        ?>
                        <tr>
                            <td style="font-size: 10px; text-align: center;"> <?php echo $codigo; ?> </td>
                            <td style="font-size: 10px;"> <?php echo $concepto; ?> </td>
                            <td style="font-size: 10px; text-align: center;">
                                <?php 
                                    echo $inventarioInicial;
                                ?>
                            </td><td style='font-size: 10px; text-align: center;'>                                
                                <?php
                                    echo $aGeneral;                                    
                                ?>
                                <!--<table style="width:100px">
                                <?php
                                    echo /*"<tr><td style='text-align: right;'>PSalidas:</td><td style='text-align: right;'>".$salidaGeneral."</td></tr>
                                       
                                        <tr><td style='text-align: right;'>PEntradas:</td><td style='text-align: right;'>".$entradaGeneral."</td></tr>
                                        <tr><td style='text-align: right;'>Existencia:</td><td style='text-align: right;'>".$inicialAlmacen[0]."</td></tr>                                        
                                       	<tr><td style='text-align: center; color: #444343; font-size: 10px;'>".*/$aGeneral;//."</td></tr>";                                    
                                ?>                                
                                </table>   -->
                            </td><td style="font-size: 10px; text-align: center;">  
                            	<?php
                                    echo $aMatriz;                                    
                                ?>                              
                                 <!--<table style="width:100px">
                                <?php
                                    echo /*"<tr><td style='text-align: right;'>PSalidas:</td><td style='text-align: right;'>".$salidaMatriz."</td></tr>
                                        <tr><td style='text-align: right;'>Ventas:</td><td style='text-align: right;'>".$ventasAlmacen[1]."</td></tr>
                                        <tr><td style='text-align: right;'>PEntradas:</td><td style='text-align: right;'>".$entradaMatriz."</td></tr>
                                        <tr><td style='text-align: right;'>Existencia:</td><td style='text-align: right;'>".$inicialAlmacen[1]."</td></tr>                                        
                                        <tr><td style='text-align: center; color: #444343; font-size: 10px;'>".*/$aMatriz;//."</td></tr>";                                    
                                ?>                                
                                </table>  -->                                                              
                            </td><td style="font-size: 10px; text-align: center;">
                            
                                <?php
                                    echo $aOcolusen;                                    
                                ?> 
                                 <!--<table style="width:100px"> 
                                <?php
								
                                   echo /*"<tr><td style='text-align: right;'>PSalidas:</td><td style='text-align: right;'>".$salidaOcolusen."</td></tr> 
                                        <tr><td style='text-align: right;'>Ventas:</td><td style='text-align: right;'>".$ventasAlmacen[2]."</td></tr>
									    <tr><td style='text-align: right;'>PEntradas:</td><td style='text-align: right;'>".$entradaOcolusen."</td></tr>
                                        <tr><td style='text-align: right;'>Existencia:</td><td style='text-align: right;'>".$inicialAlmacen[2]."</td></tr>                                        
                                      	<tr><td style='text-align: center; color:#444343; font-size: 10px;'>".*/$aOcolusen;//."</td></tr>";                                    
                                ?>                                
                                </table>-->
                            </td><td style="font-size: 10px; width: 75px; text-align: center;"> 
                                <?php  echo "$ ".number_format($almacenCosto,2); ?> 
                            </td>
                        </tr><tr>
                            <td colspan="8"><hr style="background-color:#e6e6e6; height:2px; border:none;"></td>
                        </tr>
                        <?php
                                }                                
                            }
                            if($contador!=0){
                        ?>
                        <tr>
                            <td colspan="2"></td>
                            <td class="tot">Total:<br /><?php  echo number_format($generalInicial); ?></td>                            
                            <td class="tot">Total:<br /><?php  echo number_format($totalGeneral); ?></td>
                            <td class="tot">Total:<br /><?php  echo number_format($totalMatriz); ?></td>                            
                            <td class="tot">Total:<br /><?php  echo number_format($totalOcolusen); ?></td>
                            <td class="tot">Total en inventario:<br /><?php echo "$ ".number_format($costosTotales,2); ?></td>
                        </tr>
                        <?php 
                            }
                        ?>
                    </table>
                    <br /><br />
                    <?php
						/*}
                            if( $contador > 0 )
                            {
                            ?>
                                <div id="opciones" style="float:right;">
                                    <form name="form_inventario" style="float:right; margin-left:20px;" method="post" action="inventario_refacciones2_excel.php">
                                        <div id="spanreloj"></div>
                                        <input type="hidden" name="fechaI" value="<?php echo $fechaInicio; ?>" />
                                        <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                                    </form> &nbsp;&nbsp;
                                    <a href="#" title="Imprimir Reporte" style=" margin-left:15px;" onclick="window.open('imprimir_inventario_refacciones2.php?fechaI=<?php echo $fechaInicio; ?>','Reporte de Inventario de Bater&iacute;as','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1150, height=700');"> <img src="../img/print icon.png"/> </a>
                                </div> <!-- FIN DIV OPCIONES -->
                                <br /><br />
                            <?php
                            }*/
                        ?>
            	
</center>
</body>
</html>