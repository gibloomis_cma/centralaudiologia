<?php session_start(); if($_SESSION['id_usuario'] == 1 || $_SESSION['id_usuario'] == 2){ 
    
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$total_refaccion = 0;
	$total_refaccion_vales = 0;
	$contador = 0;
	$total_global_notas = 0;
	$total_global_vales = 0;
	$total_global = 0;
	$totales=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Moldes</title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/demos.css"/>
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ){
					var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					    instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width: 600px;">
                    Reporte de Accesos
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <div class="contenido_proveedor">
                    <center>
                        <form name="form" action="reporte_accesos.php" method="post">
                            <div class="demo">
                                <label for="from" class="textos"> Fecha Inicio: </label> &nbsp;
                                <input type="text" id="from" name="from"/> &nbsp;&nbsp;
                                <label for="to" class="textos"> Fecha Fin: </label> &nbsp;
                                <input type="text" id="to" name="to"/>
                                &nbsp;
                                 <input type="submit" name="accion" value="Buscar" title="Buscar" class="fondo_boton"/>
                            </div><!-- End demo -->
                        </form>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
                        <br />
                    </center>
                </div><!--Fin de contenido proveedor-->
                <center>
	            <?php
					// SE VALIDA SI EL USUARIO HA OPRIMIDO EL BOTON DE BUSCAR
					if( isset($_POST['accion']) && $_POST['accion'] == "Buscar"){
						// SE RECIBEN LAS VARIABLES DEL FORMULARIO
						$fecha_inicio = $_POST['from'];
						$fecha_inicio_separada = explode("/", $fecha_inicio);
						$fecha_inicio_mysql = $fecha_inicio_separada[0]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[2];
						$fecha_final = $_POST['to'];
						$fecha_final_separada = explode("/", $fecha_final);
						$fecha_final_mysql = $fecha_final_separada[0]."-".$fecha_final_separada[1]."-".$fecha_final_separada[2];
				?>
                    <table>
                        <tr>
                        	<td style="color:#000; font-size:22px; text-align:center;"> ACCESOS </td>
                        </tr>
                        <tr>
                           	<td style="color:#000; font-size:16px; text-align:center;"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
                        </tr>
                    </table>
                    <br /> 
                    <table>
                        <tr>
                            <th style="font-size:10px;"> FECHA </th>
                            <th style="font-size:10px;"> HORA </th>
                            <th style="font-size:10px;"> USUARIO </th>                                            
                        </tr>
						<?php
								// SE REALIZA QUERY QUE OBTIENE LOS USUARIOS en_sistema entre las fechas indicadas
								$query_accesos = "SELECT fecha_entrada, hora_entrada, nombre, paterno, materno 
														FROM en_sistema, usuarios, empleados 
														WHERE en_sistema.id_usuario = usuarios.id_usuario 
														AND empleados.id_empleado = usuarios.id_empleado 
														AND en_sistema.fecha_entrada BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'
														ORDER BY fecha_entrada DESC";
										
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$result_accesos = mysql_query($query_accesos) or die(mysql_error());
								
						// SE VALIDA SI EL CONTADOR ES MAYOR A CERO
						if( mysql_num_rows($result_accesos) > 0){
					?>
                        <div id="opciones" style="float:right; margin-right:70px;">
                            <form name="form_reporte" style="float:right; margin-left:20px;" method="post" action="reporte_accesos_excel.php">
                                <div id="spanreloj"></div>
                                <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                <input type="hidden" name="fecha_final" value="<?php echo $fecha_final_mysql; ?>" />
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <!--<a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_reporte_moldes_salen.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_final_mysql; ?>','Reporte de Refacciones que Salen','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1250, height=700');"> <img src="../img/print icon.png"/> </a>-->
                        </div> <!-- FIN DIV OPCIONES -->
					<?php
						}
								// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
								while( $row_accesos = mysql_fetch_array($result_accesos) ){
						?>
                        <tr>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $row_accesos['fecha_entrada']; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $row_accesos['hora_entrada']; ?>  </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo ucfirst($row_accesos['nombre'])." ".ucfirst($row_accesos['paterno'])." ".ucfirst($row_accesos['materno']); ?> </td>                            
                        </tr>
                        <tr>
                            <td colspan="7"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                        </tr>
						<?php
								}
                        ?>
					</table>
					<br /><br />
					<?php
                    }
            		?>
                <br /><br />
                </center>
                <br />
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>
<?php } ?>