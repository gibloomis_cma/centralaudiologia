<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Numero de Serie</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<?php 
		include("config.php");
		$id_registro_inventario = $_GET["id_registro_inventario"];
		$id_base_producto = $_GET["id_base_producto"];
?>
<body> 
<div id="wrapp">     
<div id="contenido_columna2">
	<div class="contenido_pagina">
		<div class="fondo_titulo1">
			<div class="categoria">
                Auxiliar
            </div>
		</div><!--Fin de fondo titulo-->  
		<div class="area_contenido1">
        <br />
            <div class="contenido_proveedor">           
            <form name="forma1" action="proceso_modificar_numero_serie.php" method="post">
            <input name="id_registro_inventario" type="hidden" value="<?php echo $id_registro_inventario; ?>" />
            <input name="id_base_producto" type="hidden" value="<?php echo $id_base_producto; ?>" />
  		<?php
                $consulta_datos_base_producto = mysql_query("SELECT id_categoria, id_articulo, id_subcategoria, descripcion,
                                                                     referencia_codigo 
                                                                FROM base_productos
                                                                WHERE id_base_producto=".$id_base_producto) 
                                                                or die(mysql_error());
                $row = mysql_fetch_array($consulta_datos_base_producto);
                $id_categoria = $row["id_categoria"];
                $articulo = $row["id_articulo"];
                $id_subcategoria = $row["id_subcategoria"];
                $referencia_codigo = $row["referencia_codigo"];
                $descripcion = $row["descripcion"];
                $consulta_subcategoria = mysql_query("SELECT * FROM subcategorias_productos
                                                                WHERE id_subcategoria=".$id_subcategoria)
                                                                 or die(mysql_error());
                $row3 = mysql_fetch_array($consulta_subcategoria);
                $subcategoria = $row3["subcategoria"];
        ?>
                <table>
                    <tr>
                        <th colspan="4">Datos del Productos</th>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Categoria: </label>
                        </td><td id="alleft">
                        <?php 
                            if($id_categoria == 1){
                                echo "Refacciones";
                            }elseif($id_categoria == 4){
                                echo "Baterias";
                            }else{
                                echo "Modelos";
                            }
                        ?>
                        </td>
                        <?php if($id_categoria == 4){ ?>
                        <td></td>
                        <td></td>
                        <?php }else{ ?>
                        <td id="alright">
                            <label class="textos">Subcategoria: </label> 
                        </td><td id="alleft">
                            <?php echo $subcategoria; ?>
                        </td>
                        <?php }?>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Codigo: </label> 
                        </td><td id="alleft">
                            <?php echo $referencia_codigo; ?>
                        </td><td id="alright">
                            <label class="textos">Producto: </label> 
                        </td><td id="alleft">
                            <?php echo $descripcion; ?>
                        </td>
                   </tr>
                </table><br />         	
            <table>
            	<tr>
                	<th colspan="4">
                    	Modificar Numero de Serie
                    </th>
                </tr><tr>
                    <td id="alright">
                        <label class="textos">Num. Serie: </label>
                    </td><td id="alleft">
        <?php
			$consulta_numero_serie=mysql_query("SELECT num_serie_cantidad FROM inventarios 
																		WHERE id_registro=".$id_registro_inventario)
																		or die(mysql_error());
			$row_numero_serie=mysql_fetch_array($consulta_numero_serie);
			$num_serie = $row_numero_serie["num_serie_cantidad"];
		?>
                        <input name="num_serie" type="text" style="width:200px;" value="<?php echo $num_serie; ?>" />
   					</td>
                </tr><tr>
                	<td id="alright" colspan="4">
                        <a href="agregar_producto.php?id_base_producto=<?php echo $id_base_producto; ?>">
                        	<input name="volver" value="Volver" class="fondo_boton" type="button" />
                        </a>
                        <input name="accion" type="submit" value="Modificar" class="fondo_boton" />
                	</td>
                </tr>
            </table>
            </form>
            </div><!--Fin de contenido proveedor-->
        </div><!-- Fin de area contenido --> 
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div>
</body>
</html>