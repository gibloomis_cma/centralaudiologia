<?php
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Servicios de Laboratorio</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width:405px;">
                    Servicios Laboratorio
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <div class="contenido_poroveedor">
            <br />
            <center>
                <table>
                	<tr>
                    	<th colspan="4">Servicios Laboratorio</th>
                    </tr>
                    <tr>
                    	<th width="150">Referencia</th>
                        <th width="320">Concepto</th>
                        <th width="100">Precio</th>
                        <th></th>
                    </tr>
   	<?php
			$consultame_servicios=mysql_query("SELECT * FROM servicios")or die(mysql_error());
			while($row_consultas_servicios=mysql_fetch_array($consultame_servicios)){
				$id_servicio=$row_consultas_servicios["id_servicio"];
				$descripcion=$row_consultas_servicios["descripcion"];
				$referencia=$row_consultas_servicios["referencia"];
				$precio=$row_consultas_servicios["precio"];
	?>          
					<tr>
                    	<td><?php echo $referencia; ?></td>
                        <td><?php echo $descripcion; ?></td>
                        <td><?php echo "$".number_format($precio,2); ?></td>
                        <td><a href="modificacion_servicios_laboratorio.php?id_servicio=<?php echo $id_servicio; ?>"><img src="../img/modify.png" /></a></td>
                    </tr>
	<?php
			}
	?>
            	</table>
            </center>
            </div><!--Fin de contenido proveedor -->
            <br />
            	<div class="titulos">Nuevo Servicio de Laboratorio</div>
           	<br />
                <div class="contenido_proveedor">
                    	<table>                    
                        <form name="form1" action="procesa_servicios_laboratorio.php" method="post">
                            <input name="categoria1" value="6" type="hidden" />
                            <tr>
                                <tr>
								<td id="alright">
                            		<label class="textos">Referencia: </label>
                            	</td><td id="alleft">
                            		<input name="codigo" type="text" maxlength="30" style="width:300px" />
                            	</td>
                            </tr><tr>
                                <td id="alright">
                            		<label class="textos">Concepto: </label>
                            	</td><td id="alleft">
                            		<input name="concepto" type="text" maxlength="69" style="width:300px" />
                            	</td>
                       		</tr><tr>
                            	<td id="alright">                        
                            		<label class="textos">Precio: </label>
                            	</td><td id="alleft">
                                	<label class="textos">$</label>
                            		<input name="precio_venta" type="text" size="10" maxlength="10" />
                            	</td>
                            </tr><tr>
                            	<td colspan="2" id="alright">
                            		<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                            	</td>
                            </tr>
                        </form>
                        </table> 
                </div><!--Fin de contenido proveedor-->
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>