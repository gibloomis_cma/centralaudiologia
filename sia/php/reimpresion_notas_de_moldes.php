<?php
	// SE INICIA SESION
	session_start();

	// SE DECLARA UNA VARIABLE CON EL ID DEL USUARIO QUE INICIO SESION
	$id_usuario_logueado = $_SESSION['id_empleado_usuario'];

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO AL QUE PERTENCE EL USUARIO EN EL SISTEMA
	$query_departamento = "SELECT id_departamento
						   FROM empleados
						   WHERE id_empleado = '$id_usuario_logueado'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_departamento = mysql_query($query_departamento) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_departamento = mysql_fetch_array($resultado_departamento);
	$id_departamento_empleado = $row_departamento['id_departamento'];

	// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO AL QUE PERTENECE EL EMPLEADO EN EL SISTEMA
	$query_sucursal = "SELECT id_sucursal
					   FROM areas_departamentos
					   WHERE id_departamento = '$id_departamento_empleado'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);
	$id_sucursal = $row_sucursal['id_sucursal'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS FOLIOS DE LAS VENTAS REALIZADAS
	$query_folio_molde = "SELECT folio_num_molde
						  FROM moldes
						  ORDER BY folio_num_molde DESC";

	// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
	$resultado_folio_molde = mysql_query($query_folio_molde) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reimpresi&oacute;n de Nota de Moldes </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
</head>
<body>
	<div id="wrapp">
		<div id="contenido_columna2">
			<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:700px;">
						Reimpresi&oacute;n Nota de Molde
					</div><!-- FIN DIV CATEGORIA -->
				</div><!-- FIN DIV FONDO TITULO 1 -->
				<div class="area_contenido1">
					<div class="contenido_proveedor">
						<br/>
						<div class="titulos"> Informaci&oacute;n de la Nota de Molde </div>
						<br/>
						<form name="form_datos_nota_molde" id="form_datos_nota_molde" method="post" action="funciones_reimpresiones_ticket.php">
							<table style="margin-left:20px;">
								<tr>
									<td style="text-align:center;"> 
										<label class="textos"> N° de Nota: </label> &nbsp;
										<select name="folio_nota_molde" id="folio_nota_molde">
											<option value="0"> --- Seleccione Folio --- </option>
									<?php
										// SE REALIZA CICLO QUE MUESTRA TODOS LOS FOLIOS DE LA TABLA DE VALES DE BATERIA
										while ( $row_folio = mysql_fetch_array($resultado_folio_molde) ) 
										{
											$num_molde = $row_folio['folio_num_molde'];
										?>
											<option value="<?php echo $num_molde; ?>"> N° de Folio: &nbsp; <?php echo $num_molde; ?> </option>
										<?php
										}
									?>
										</select>
									</td>
								</tr>
							</table><!-- FIN TABLA FOLIOS DE VALE DE BATERIAS -->
							<br/>
							<table style="margin-left:40px;">
								<tr>
									<td style="text-align:left;"> <label class="textos"> N° de Folio: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Fecha: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="num_folio" id="num_folio" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="fecha_folio" id="fecha_folio" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td style="text-align:left;"> <label class="textos"> Cliente: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Lado Oido: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="nombre_cliente" id="nombre_cliente" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="lado_oido" id="lado_oido" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td style="text-align:left;"> <label class="textos"> Adapataci&oacute;n o Reposici&oacute;n: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Precio: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="adaptacion_reposicion" id="adaptacion_reposicion" size="30"  readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="precio" id="precio" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:right"> <input type="submit" name="btn_reimprimir_molde" id="btn_reimprimir_molde" value="Reimprimir Molde" title="Reimprimir Nota de Molde" class="fondo_boton" style="display:none;" /> </td>
								</tr>
							</table><!-- FIN TABLA INFORMACION VALE DE BATERIAS -->
						</form>
					</div><!-- FIN DIV CONTENIDO PROVEEDOR -->
				</div><!-- FIN DIV AREA CONTENIDO 1 -->
			</div><!-- FIN DIV CONTENIDO PAGINA -->
		</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html> 