<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

    // SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION DE LAS REFACCIONES
    $query_refacciones = "SELECT id_base_producto,referencia_codigo,descripcion,precio_compra
                          FROM base_productos
                          WHERE id_categoria = 1
                          ORDER BY descripcion ASC";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_refacciones = mysql_query($query_refacciones) or die(mysql_error());

    // SE DECLARA VARIABLE PARA CALCULAR EL COSTO TOTAL
    $total_en_almacen = 0;
    $costo = 0;
    $general = 0;
    $matriz = 0;
    $laboratorio = 0;
    $ocolusen = 0;
    $general_total = 0;
    $contador = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Inventario de Refacciones </title>
    <script type="text/javascript" language="javascript">
        function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  h + ':' + min + ':' + sec;
            window.print();
        }
    </script>
</head>
<body onload="hora()">
    <center>
        <table>
            <tr>
                <td style="font-size: 18px; color: #000; font-weight: bold; text-align: center;"> INVENTARIO DE REFACCIONES </td>
            </tr>
            <tr>
                <td id="hora" style="font-size: 12px; color: #000; font-weight: bold; text-align: center;"> Fecha y hora de impresi&oacute;n: <?php echo date('d/m/Y'); ?> &nbsp;&nbsp; </td>
            </tr>
        </table>
        <br />
        <table border="1">
            <tr>
                <th style="font-size: 11px;"> C&oacute;digo </th>
                <th style="font-size: 11px;"> Concepto </th>
                <th style="font-size: 11px;"> Inventario Total </th>
                <th style="font-size: 11px;"> Almac&eacute;n General </th>
                <th style="font-size: 11px;"> Almac&eacute;n Recepci&oacute;n Matriz </th>
                <th style="font-size: 11px;"> Almac&eacute;n Laboratorio </th>
                <th style="font-size: 11px;"> Almac&eacute;n Recepci&oacute;n Ocolusen </th>
                <th style="font-size: 11px;"> Costo Total </th>
            </tr>
                    <?php
                        // SE REALIZA CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                        while( $row_refacciones = mysql_fetch_array($resultado_refacciones) )
                        {
                            $id_base_producto = $row_refacciones['id_base_producto'];
                            $codigo = $row_refacciones['referencia_codigo'];
                            $concepto = $row_refacciones['descripcion'];
                            $precio_compra = $row_refacciones['precio_compra'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO TOTAL DE CADA REFACCION
                            $query_inventario_total = "SELECT SUM(num_serie_cantidad) AS inventario_total
                                                       FROM inventarios
                                                       WHERE id_articulo = '$id_base_producto'";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_inventario_total = mysql_query($query_inventario_total) or die(mysql_error());
                            $row_inventario_total = mysql_fetch_array($resultado_inventario_total);
                            $inventario_total = $row_inventario_total['inventario_total'];
                            $costo = $inventario_total * $precio_compra;
                            $total_en_almacen = $total_en_almacen + $costo;

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION PERO DE EL ALMACEN GENERAL
                            $query_almacen_general = "SELECT SUM(num_serie_cantidad) AS inventario_almacen_general
                                                      FROM inventarios
                                                      WHERE id_articulo = '$id_base_producto'
                                                      AND id_almacen = 1";

                            // SE EJECUTA EL QUERY DE ALMACEN GENERAL Y SE OBTIENE EL RESULTADO
                            $resultado_almacen_general = mysql_query($query_almacen_general) or die(mysql_error());
                            $row_almacen_general = mysql_fetch_array($resultado_almacen_general);
                            $inventario_almacen_general = $row_almacen_general['inventario_almacen_general'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION PERO DE EL ALMACEN RECEPCION MATRIZ
                            $query_recepcion_matriz = "SELECT SUM(num_serie_cantidad) AS inventario_recepcion_matriz
                                                       FROM inventarios
                                                       WHERE id_articulo = '$id_base_producto'
                                                       AND id_almacen = 2";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_recepcion_matriz = mysql_query($query_recepcion_matriz) or die(mysql_error());
                            $row_recepcion_matriz = mysql_fetch_array($resultado_recepcion_matriz);
                            $inventario_recepcion_matriz = $row_recepcion_matriz['inventario_recepcion_matriz'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO EN EL ALMACEN LABORATORIO
                            $query_laboratorio = "SELECT SUM(num_serie_cantidad) AS inventario_laboratorio
                                                  FROM inventarios
                                                  WHERE id_articulo = '$id_base_producto'
                                                  AND id_almacen = 3";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_laboratorio = mysql_query($query_laboratorio) or die(mysql_error());
                            $row_laboratorio = mysql_fetch_array($resultado_laboratorio);
                            $inventario_laboratorio = $row_laboratorio['inventario_laboratorio'];

                            // SE REALIZA QUERY QUE OBTIENE EL INVENTARIO DE CADA REFACCION DEL ALMACEN RECEPCION OCOLUSEN
                            $query_inventario_ocolusen = "SELECT SUM(num_serie_cantidad) AS inventario_ocolusen
                                                          FROM inventarios
                                                          WHERE id_articulo = '$id_base_producto'
                                                          AND id_almacen = 4";

                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                            $resultado_inventario_ocolusen = mysql_query($query_inventario_ocolusen) or die(mysql_error());
                            $row_inventario_ocolusen = mysql_fetch_array($resultado_inventario_ocolusen);
                            $inventario_ocolusen = $row_inventario_ocolusen['inventario_ocolusen'];

                            $general_total = $general_total + $inventario_total;
                            $general = $general + $inventario_almacen_general;
                            $matriz = $matriz + $inventario_recepcion_matriz;
                            $laboratorio = $laboratorio + $inventario_laboratorio;
                            $ocolusen = $ocolusen + $inventario_ocolusen;

                            // SE VALIDA SI EL INVENTARIO TOTAL ES MAYOR A 0
                            if( $inventario_total > 0 )
                            {
                              $contador++;
                            ?>
                                <tr>
                                    <td style="font-size: 10px; text-align: center;"> <?php echo $codigo; ?> </td>
                                    <td style="font-size: 10px;"> <?php echo $concepto; ?> </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_total) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_total;
                                            }
                                         ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_almacen_general) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_almacen_general;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_recepcion_matriz) )
                                            {
                                                echo "0";
                                            }else
                                            {
                                                echo $inventario_recepcion_matriz;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_laboratorio) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_laboratorio;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; text-align: center;">
                                        <?php
                                            if( is_null($inventario_ocolusen) )
                                            {
                                                echo "0";
                                            }
                                            else
                                            {
                                                echo $inventario_ocolusen;
                                            }
                                        ?>
                                    </td>
                                    <td style="font-size: 10px; width: 75px; text-align: center;"> <?php echo "$ ".number_format($costo,2); ?> </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $general_total; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $general; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $matriz; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $laboratorio; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total: <br /> <?php echo $ocolusen; ?> </td>
                            <td style="color:#ac1f1f; font-size: 10px; text-align: center;"> Total en inventario: <br /> <?php echo "$ ".number_format($total_en_almacen,2); ?> </td>
                        </tr>
                        </table>
    </center>
</body>
</html>