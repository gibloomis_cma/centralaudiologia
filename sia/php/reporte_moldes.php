<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$total_refaccion = 0;
	$total_refaccion_vales = 0;
	$contador = 0;
	$total_global_notas = 0;
	$total_global_vales = 0;
	$total_global = 0;
	$totales=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Moldes</title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/demos.css"/>
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ){
					var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					    instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width: 600px;">
                    Reporte de Moldes
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <div class="contenido_proveedor">
                    <center>
                        <form name="form" action="reporte_moldes.php" method="post">
                            <div class="demo">
                                <label for="from" class="textos"> Fecha Inicio: </label> &nbsp;
                                <input type="text" id="from" name="from"/> &nbsp;&nbsp;
                                <label for="to" class="textos"> Fecha Fin: </label> &nbsp;
                                <input type="text" id="to" name="to"/>
                                &nbsp;
                                 <input type="submit" name="accion" value="Buscar" title="Buscar" class="fondo_boton"/>
                            </div><!-- End demo -->
                        </form>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
                        <br />
                    </center>
                </div><!--Fin de contenido proveedor-->
                <center>
	            <?php
					// SE VALIDA SI EL USUARIO HA OPRIMIDO EL BOTON DE BUSCAR
					if( isset($_POST['accion']) && $_POST['accion'] == "Buscar"){
						// SE RECIBEN LAS VARIABLES DEL FORMULARIO
						$fecha_inicio = $_POST['from'];
						$fecha_inicio_separada = explode("/", $fecha_inicio);
						$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
						$fecha_final = $_POST['to'];
						$fecha_final_separada = explode("/", $fecha_final);
						$fecha_final_mysql = $fecha_final_separada[2]."-".$fecha_final_separada[1]."-".$fecha_final_separada[0];
				?>
                    <table>
                        <tr>
                        	<td style="color:#000; font-size:22px; text-align:center;"> MOLDES </td>
                        </tr>
                        <tr>
                           	<td style="color:#000; font-size:16px; text-align:center;"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
                        </tr>
                    </table>
                    <br /> 
                    <table>
                        <tr>
                            <th style="font-size:10px;"> NOTA </th>
                            <th style="font-size:10px;"> FECHA DE ENTRADA </th>
                            <th style="font-size:10px;"> CLIENTE </th>
                            <th style="font-size:10px;"> FECHA DE SALIDA </th>
                            <th style="font-size:10px;"> COSTO </th>
                            <th style="font-size:10px;"> TIPO </th>
                            <th style="font-size:10px;"> OBSERVACIONES </th>                                                
                        </tr>
                		<?php
							// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
							$query_tecnicos = mysql_query("SELECT DISTINCT(id_empleado), alias
												FROM moldes, moldes_elaboracion, empleados
												WHERE empleados.id_empleado = moldes_elaboracion.elaboro
												AND moldes.folio_num_molde = moldes_elaboracion.folio_num_molde																																												
												AND moldes.id_estatus_moldes <> 1
												AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'")
												or die(mysql_error());
							while($row_query_tecnicos=mysql_fetch_array($query_tecnicos)){
								$id_empleado=$row_query_tecnicos['id_empleado'];
								$alias=$row_query_tecnicos['alias'];								
						?>
                        <tr>
                            <td><?php echo ucwords($alias); ?></td>
                        </tr>	
						<?php
								// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
								$query_moldes_notas = "SELECT moldes.folio_num_molde, fecha_entrada, moldes.nombre, moldes.paterno, 
														fecha_salida, costo, moldes_elaboracion.observaciones, estado_molde, lado_oido,
														adaptacion, reposicion, id_estilo, id_material, id_salida, moldes.materno
														FROM moldes,moldes_elaboracion, estatus_moldes
														WHERE moldes.folio_num_molde = moldes_elaboracion.folio_num_molde															
														AND elaboro=".$id_empleado."
														AND moldes.folio_num_molde = moldes_elaboracion.folio_num_molde															
														AND moldes.id_estatus_moldes=estatus_moldes.id_estatus_moldes															
														AND moldes.id_estatus_moldes <> 1
														AND fecha_salida BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'
														ORDER BY fecha_entrada DESC";
										
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$resultado_moldes_notas = mysql_query($query_moldes_notas) or die(mysql_error());
								$costos=0;$precio=0;$registros=0;
								// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
								while( $row_molde_nota = mysql_fetch_array($resultado_moldes_notas) ){
									$precio=0;
									$contador++;
									$registros++;
									$lado_oido=ucwords(strtolower($row_molde_nota['lado_oido']));
									$estado_molde=$row_molde_nota['estado_molde'];
									$fecha_entrada = $row_molde_nota['fecha_entrada'];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$folio_molde = $row_molde_nota['folio_num_molde'];										
									$cliente = ucwords(strtolower($row_molde_nota['nombre']." ".$row_molde_nota['paterno']." ".$row_molde_nota['materno']));	
									$fecha_salida = $row_molde_nota['fecha_salida'];										
									$fecha_salida_separada = explode("-", $fecha_salida);
									$fecha_salida_normal = $fecha_salida_separada[2]."/".$fecha_salida_separada[1]."/".$fecha_salida_separada[0];										
									$costo = $row_molde_nota['costo'];									
									$observaciones = "[".$lado_oido."] ".$row_molde_nota['observaciones']; 
									$reposicion = $row_molde_nota['reposicion'];
									$adaptacion = $row_molde_nota['adaptacion'];
									$id_estilo=$row_molde_nota['id_estilo'];
									$id_material=$row_molde_nota['id_material'];
									$id_salida=$row_molde_nota['id_salida'];
									if($adaptacion!=""){
										$tipo="[Adaptacion] ";
										//Precio del Estilo
										$precio_estilos=mysql_query('SELECT * FROM estilos')or die('Consulta Precio del Estilo');
										$row_estilos=mysql_fetch_array($precio_estilos);
										$precio+=$row_estilos['precio'];
										//Precio del Material
										$precio_materiales=mysql_query('SELECT * FROM materiales')or die('Consulta Precio del Material');
										$row_materiales=mysql_fetch_array($precio_materiales);
										$precio+=$row_materiales['precio'];
										//Precio de la Salida
										$precio_salidas=mysql_query('SELECT * FROM salidas')or die('Consulta Precio del Salidas');
										$row_salidas=mysql_fetch_array($precio_salidas);
										$precio+=$row_salidas['precio'];
									}else if($reposicion!=""){
										$tipo="[Reposicion] ";
										$costo=0;
									}else{
										$tipo="";										
									}									 
									if($costo<=0 and $adaptacion!=""){
										//$observaciones.=$id_estilo." ".$id_material." ".$id_salida;
										$costo=$precio;
									}
									if($lado_oido=="Bilateral"){
										$registros++;
										//$costo=$costo*2;
									}
									$costos+=$costo;
									$totales+=$costo;
									
										
						?>
                        <tr>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $folio_molde; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $fecha_entrada_normal; ?>  </td>
                            <td style="font-size:9px;"> <?php  echo $cliente; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $fecha_salida_normal; ?> </td>                                               
                            <td style="font-size:9px; text-align:right;"><?php echo "$".number_format($costo,2); ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $tipo; ?> </td>  
                            <td style="font-size:9px;"> <?php  echo $observaciones; ?> </td>                                                
                        </tr>
                        <tr>
                            <td colspan="7"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                        </tr>
						<?php
								}
                        ?>
                        <tr>
                            <td colspan="7" style="font-size:12px; font-weight:lighter"><?php echo $registros." registros de detalle"; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-left:50px; font-size:9px; padding-bottom:50px;">Suma</td>
                            <td style="font-size:9px; text-align:right;" valign="top"><?php echo "$".number_format($costos,2); ?></td>
                        </tr>
						<?php
							}
                        ?>
                        <tr>                                        		
                            <td colspan="6"></td>                            
                            <td style="text-align:center; color:#ac1f1f;"> Total: <br /> <?php echo "$".number_format($totales,2); ?> </td>
                        </tr>
					</table>
					<br /><br />
					<?php
						// SE VALIDA SI EL CONTADOR ES MAYOR A CERO
						if( $contador > 0){
					?>
                        <div id="opciones" style="float:right; margin-right:70px;">
                            <form name="form_reporte" style="float:right; margin-left:20px;" method="post" action="reporte_moldes_excel.php">
                                <div id="spanreloj"></div>
                                <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                <input type="hidden" name="fecha_final" value="<?php echo $fecha_final_mysql; ?>" />
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <!--<a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_reporte_moldes_salen.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_final_mysql; ?>','Reporte de Refacciones que Salen','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1250, height=700');"> <img src="../img/print icon.png"/> </a>-->
                        </div> <!-- FIN DIV OPCIONES -->
					<?php
						}
                    }
            		?>
                <br /><br />
                </center>
                <br />
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>