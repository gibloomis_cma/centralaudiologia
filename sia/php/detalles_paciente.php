<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="">Seleccione</option><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label><input type="text" name="descripcion[]" /><label class="textos"> Titular: </label><input name="titular[]" type="text" /><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Pacientes
            </div>
         </div><!--Fin de fondo titulo-->
        <div class="area_contenido1">
		<?php
            include("config.php");
            $id_cliente = $_GET["id_cliente"];
            $consulta_datos_cliente = mysql_query("SELECT * FROM ficha_identificacion 
                                                WHERE id_cliente=".$id_cliente) 
												or die(mysql_error());
            $row = mysql_fetch_array($consulta_datos_cliente);
            $nombre = $row["nombre"];
            $paterno = $row["paterno"];
            $materno = $row["materno"];
            $fecha_nacimiento = $row["fecha_nacimiento"];
            $genero = $row["genero"];
            $calle = $row["calle"];
            $num_exterior = $row["num_exterior"];
            $num_interior = $row["num_interior"];
            $codigo_postal = $row["codigo_postal"];
            $colonia = $row["colonia"];
            $id_estado = $row["id_estado"];
            $id_ciudad = $row["id_ciudad"];
            $edad = $row["edad"];
            $ocupacion = $row["ocupacion"];
            $rfc = $row["rfc"];
        ?>
         <div class="contenido_proveedor">
            <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                <table>
                    <tr>
                    	<th colspan="4">
                        	Ficha de Identificación
                        </th>
                    </tr><tr>
                        <td id="alright">
                        	<label class="textos">Nombre(s): </label>
                        </td><td>
                        	<?php echo $nombre; ?>
                        </td><td id="alright">
                        	<label class="textos">Edad: </label>
                        </td><td>
                        	<?php echo $edad." Años"; ?>
                        </td>
                    </tr>
                        <td id="alright">
                        	<label class="textos">Paterno: </label>
                        </td><td>
                        	<?php echo $paterno; ?>
                        </td><td id="alright">
                        	<label class="textos">Materno: </label>
                        </td><td>
                        	<?php echo $materno; ?>
                        </td>
                    </tr><tr>                    
                        <td id="alright">
                        	<label class="textos">Ocupacion: </label>
                        </td><td>
                        	<?php echo $ocupacion; ?>
                        </td><td id="alright">
                        	<label class="textos">Fecha de Nacimiento: </label>
                        </td><td>
                            <?php echo $fecha_nacimiento; ?>
                        </td>
                    </tr><tr>
                        <td id="alright">
                        	<label class="textos">Genero:</label>
                        </td><td>
							<?php echo $genero; ?>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">RFC: </label>
                        </td><td>
                         	<?php echo $rfc; ?>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td style="text-align:center" colspan="4">
                        	--Dirección--
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Calle: </label>
                        </td><td>
                        	<?php echo $calle; ?>
                        </td><td id="alright">
                        	<label class="textos">Numero: </label>
                        </td><td>                        	
                        	<?php 
								echo $num_exterior; 
								if($num_interior!=""){
									echo "-".$num_interior; 
								}
							?>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Colonia:</label>
                        </td><td>
                        	<?php echo $colonia; ?>
                        </td><td id="alright">
                        	<label class="textos">Codigo Postal: </label>
                        </td><td>
                        	<?php echo $codigo_postal; ?>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Estado: </label>
                        </td><td>                        	
							<?php
                                $consulta_estados = mysql_query("SELECT * FROM estados WHERE id_estado=".$id_estado)or die(mysql_error());
                                $row3 = mysql_fetch_array($consulta_estados);
								$id_estado_consultada = $row3["id_estado"];
								$estado = $row3["estado"];                                    
								echo utf8_encode($estado); 
							?>
                        </td><td id="alright">
                        	<label class="textos">Ciudad: </label>
                        </td><td>                        	
							<?php
                                $consulta_ciudad = mysql_query("SELECT ciudad, id_ciudad FROM ciudades WHERE id_ciudad=".$id_ciudad)or die(mysql_error());
                                $row4 = mysql_fetch_array($consulta_ciudad);
								$id_ciudad_consultada = $row4["id_ciudad"];
								$ciudad = ucwords(strtolower($row4["ciudad"]));
                                echo utf8_encode($ciudad); 
							?> 
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td style="text-align:center" colspan="4">
                        	--Forma(s) de Contacto--
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">  
                        	<table>
					<?php                    					
						$consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
															WHERE id_cliente='$id_cliente'") or die(mysql_error());
						while($row2 = mysql_fetch_array($consulta_forma_contacto)){
							$id_contactos_cliente = $row2["id_contactos_cliente"];
							$tipo = $row2["tipo"];
							$descripcion = $row2["descripcion"];
							$titular = $row2["titular"];
                	?>
                            	<tr>
                                	<td id="alright">                      	
                						<label class="textos">Tipo: </label>							
                                    </td><td>
                                    	<?php echo $tipo; ?>
                                    </td><td id="alright">							
                            			<label class="textos"> Descripción: </label>
                            		</td><td>
                            			<?php echo $descripcion; ?>
                            		</td><td id="alright">
                            			<label class="textos"> Titular: </label>
                            		</td><td>
                            			<?php echo $titular; ?>
                            		</td>
                        		</tr>
                    <?php
						}
					?>
                    		</table>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr>
					<?php
                        $en_institucion=mysql_query('SELECT institucion, razon_social 
                                                    FROM instituciones, clientes_instituciones 
                                                    WHERE clientes_instituciones.id_institucion=instituciones.id_institucion
                                                    AND id_cliente='.$id_cliente)
                                                    or die(mysql_error());
                        $row_en_institucion=mysql_fetch_array($en_institucion);
                        $institucion=$row_en_institucion['razon_social'];
                        if($institucion!=""){
                    ?>
					<tr>
                        <td colspan="4" style="text-align:center">
                        	
                        	
                            <label class="textos">
                            	-- Institucion a la que pertenece --
                            </label><br />
                            <?php echo $institucion; ?>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr>
                    <?php
						}
					?>
                    <tr>
                    	<td id="alright" colspan="4">
                        	<input name="volver" type="button" value="Volver" class="fondo_boton"
                            onclick="window.location.href='agregar_pacientes.php'" />
                        	<input name="modificar" type="button" value="Editar" class="fondo_boton"
                            onclick="window.location.href='modificar_pacientes.php?id_cliente=<?php echo $id_cliente; ?>'"/>
                        </td>
                    </tr>
                </table>
        	</form>      				
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>