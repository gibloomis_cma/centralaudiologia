<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Contactos de Proveedor</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css" />
<script type="text/javascript" src="../js/Validacion.js"></script>
<style type="text/css">
	table tr td{
		/*border:1px solid #F66;*/
	}
</style>
</head>
<body>
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Proveedores
            </div>
        </div><!--Fin de fondo titulo-->                            
		<?php
            // SE IMPORTA EL ARCHIVO DE LA CONEXION A LA BASE DE DATOS
            include("config.php");
            $id_proveedor = $_GET["id_proveedor"];
            
            $query_datos_proveedor = mysql_query("SELECT razon_social,calle,num_exterior,
												num_interior,colonia,codigo_postal,id_ciudad, id_pais,
												id_estado,rfc,credito,plazos_pago 
												FROM proveedores                                                   
											  	WHERE id_proveedor = ".$id_proveedor)
												or die(mysql_error());
            $row = mysql_fetch_array($query_datos_proveedor);            
			$nombre_dependencia = $row["razon_social"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$colonia = $row["colonia"];
			$codigo_postal = $row["codigo_postal"];
			$id_ciudad = $row["id_ciudad"];
			$id_estado = $row["id_estado"];
			$id_pais = $row["id_pais"];
			$rfc = $row["rfc"];
			$credito = $row["credito"];
			$plazos_pago = $row["plazos_pago"];
        ?>
         <center>
         <form name="form_agregar_contacto_proveedor" id="form_agregar_contacto_proveedor" 
         action="procesa_agregar_contacto_proveedor.php" method="post" onsubmit="return validarContactoProveedor()" >
        	 <input  name="id_proveedor" value="<?php echo $id_proveedor; ?>" type="hidden"/>
         <div class="area_contenido1">                          
            <br />
                <div class="contenido_proveedor">                                   
                    <table width="624px">
                    	<tr>
                        	<th colspan="4">Datos Generales</th>
                        </tr>
                    	<tr>
                        	<td style="text-align:right">               
			                    <label class="textos">Nombre de la Dependencia: </label>
                            </td><td style="text-align:left" colspan="3">
            			        <?php echo $nombre_dependencia; ?>
                            </td>
                        </tr><tr>                        
                            <td style="text-align:right">                           
                				<label class="textos">Dirección: </label>
                            </td>
							<td style="text-align:left" colspan="3">
							<?php 						
                                if($num_interior == ""){
                                    $interior = "";
                                }else{
                                    $interior = " Int. ".$num_interior;
                                }
                                echo $calle." #".$num_exterior.$interior." Col. ".$colonia." C.P. ".$codigo_postal;
                            ?>                            
                            </td>                            
                        </tr><tr>
                        	<td style="text-align:right"><label class="textos">Pais: </label></td>
                            <td style="text-align:left">
							<?php
                                $consulta_paises = mysql_query("SELECT pais FROM paises 
                                                                WHERE id_pais=".$id_pais);
                                $row15 = mysql_fetch_array($consulta_paises);
                                $pais_consultado = $row15["pais"];	
                                echo $pais_consultado; 
                            ?>
                        </tr><tr>
                        	<td style="text-align:right">
                                <label class="textos">Estado: </label>
                            </td><td style="text-align:left">
							<?php
                                $consulta_estados = mysql_query("SELECT estado FROM estados 
                                                                WHERE id_estado=".$id_estado);
                                $row3 = mysql_fetch_array($consulta_estados);
                                $estado_consultado = $row3["estado"];	
                                echo utf8_encode($estado_consultado); 
                            ?>
                            </td><td style="text-align:right">
                                <label class="textos">Ciudad: </label>                         	
							</td><td style="text-align:left">
							<?php
                                $consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades
                                                                WHERE id_ciudad=".$id_ciudad);
                                $row4 = mysql_fetch_array($consulta_ciudad);
                                $ciudad_consultado = ucwords(strtolower($row4["ciudad"]));	
                                echo $ciudad_consultado; 
                            ?>                                                       
                            </td>
                        </tr><tr>
                        	<td style="text-align:right">
                            	<label class="textos">RFC: </label>
                            </td><td style="text-align:left">
								<?php echo strtoupper($rfc); ?>
                            </td><td style="text-align:right">                                
                                <label class="textos">Credito: </label>
                            </td><td style="text-align:left">                                
                                <?php echo "$".number_format($credito,2); ?>                                
                    		</td>
                        </tr><tr>
                        	<td style="text-align:right">
                            	<label class="textos">Plazos de Pago: </label>                                
                            </td><td style="text-align:left" colspan="3">
								<?php echo $plazos_pago; ?>
                           		<label> dias </label>
                            </td>
                        </tr>                 
                     </table>
                    <br />                                  
                    <table>                     
                    	<tr>
                        	<th colspan="2">Contactos</th>
                        </tr>   
                    <?php
                        $contactos = mysql_query('SELECT id_contacto_proveedor, nombre, paterno, materno
                                                FROM contactos_proveedores
                                                WHERE id_proveedor ='.$id_proveedor)or die(mysql_error());
						$n_contactos = 0;
                        while($row_contactos = mysql_fetch_array($contactos)){
                            $id_contacto = $row_contactos['id_contacto_proveedor'];
                            $nombre = $row_contactos['nombre'];
                            $materno = $row_contactos['materno'];
                            $paterno = $row_contactos['paterno'];
							$n_contactos++;
                    ?>
                        <tr>
                            <td style="padding:10px 5px; border-bottom:1px solid #666; text-align:right; width:45%">
								<?php echo utf8_encode($nombre." ".$paterno." ".$materno); ?></td>
                            <td style="padding:10px 10px; border-bottom:1px solid #666;">
                    <?php 
                        $formas_contacto = mysql_query('SELECT id_forma_contacto, tipo, descripcion
                                                    FROM forma_contacto_proveedores
                                                    WHERE id_contacto_proveedor ='.$id_contacto)
                                                    or die(mysql_error());						
                        while($row_formas_contacto = mysql_fetch_array($formas_contacto)){
                            $id_forma_contacto = $row_formas_contacto['id_forma_contacto'];
                            $tipo = $row_formas_contacto['tipo'];
                            $descripcion = $row_formas_contacto['descripcion'];
                    ?>                   
                                <div id="cleft">
                                    <label class="textos"><?php echo $tipo; ?>:</label>
                                </div>
                                <div id="cright">
									<?php echo $descripcion; ?>
                                </div>
                    <?php	
                        }
                    ?>									         
                            </td>                                                      
                        </tr>                            
                    <?php																														
                        }						
						if($n_contactos==0){
                    ?>
                    	<tr>
                        	<td colspan="2" style="text-align:center">No hay Contactos registrados</td>
                        </tr>                
                    <?php
                    	}
                    ?>
                    </table>
                    <br />
                    <table style="text-align:right">
                    	<tr>
                            <th colspan="2">
                            	Agregar Contacto
                            </th>                    	
                        </tr><tr>
                        	<td style="text-align:right;" width="50%">
                    <label class="textos">Nombre: </label><br /><br />
                    <label class="textos">Paterno: </label><br /><br />
                    <label class="textos">Materno: </label>
                    		</td><td style="text-align:left;">
                    <input name="nombre" type="text" onchange="validaNombre(this.value)" /><br /><br />
                    <input name="paterno" type="text" onchange="validaPaterno(this.value)" /><br /><br />
                    <input name="materno" type="text" onchange="validaMaterno(this.value)" />
                        	</td>
                       	</tr>
                 	</table>  
                    <p style="text-align:right; margin-right:50px;"> 
                    <?php
						if($n_contactos!=0){
					?>                                      		
                        <input name="accion" type="button" value="Terminar" 
                        class="fondo_boton" title="Terminar" onclick="regresar()"/>                          
                    <?php
						}
					?>
                        <input name="accion" type="submit" value="Guardar" 
                        class="fondo_boton" title="Guardar"/> 
                    </p>
               	</fieldset>                
                </div><!--Fin de contenido proveedor-->
        </div><!--Fin de area contenido-->
        </form>
        </center>
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>