<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_moldes = "SELECT folio_num_molde, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_cliente, fecha_entrada, id_estatus_moldes, costo, adaptacion, reposicion, pago_parcial
                     FROM moldes
                     WHERE id_estatus_moldes <> 7
                     ORDER BY fecha_entrada DESC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_query_moldes = mysql_query($query_moldes) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Control de Moldes </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css" />
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora_reparaciones' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
	<div id="wrapp">       
		<div id="contenido_columna2">
    		<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:600px;">
                		Lista de Control de Moldes
           			</div>
        		</div><!--Fin de fondo titulo-->
        		<div class="area_contenido1">
            		<div class="contenido_proveedor">
            		<br />
               			<div class="titulos"> Lista de Control de Moldes </div>
            		<br />
            		<table style="width:700px;">
            			<tr>
            				<th style="font-size:14px;"> N° Molde </th>
            				<th style="font-size:14px;"> Nombre Completo </th>
            				<th style="font-size:14px;"> Costo </th>
            				<th style="font-size:14px;"> Estatus </th>
            			</tr>
            		<?php
            			// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            			while ( $row_molde = mysql_fetch_array($resultado_query_moldes) )
            			{
            				$folio_num_molde = $row_molde["folio_num_molde"];
                            $nombre_cliente = $row_molde['nombre_cliente'];
                            $estado_molde = $row_molde["id_estatus_moldes"];
                            $fecha_entrada = $row_molde["fecha_entrada"];
                            $costo_molde = $row_molde['costo'];
                            $pago_parcial = $row_molde['pago_parcial'];
                            $adaptacion = $row_molde['adaptacion'];
                            $reposicion = $row_molde['reposicion'];
            
                            $consulta_estatus_nota_molde = mysql_query("SELECT * FROM estatus_moldes
                                                                        WHERE id_estatus_moldes = ".$estado_molde) or die(mysql_error());   
                            
                            $row_estatus_nota_molde = mysql_fetch_array($consulta_estatus_nota_molde);
                            $estatus_molde = $row_estatus_nota_molde["estado_molde"];                                 
                    ?>
                            <tr>
                                <td style="font-size:12px; text-align:center;"> <?php echo  $folio_num_molde; ?> </td>
                                <td style="font-size:12px;"> <?php echo ucwords(strtolower($nombre_cliente)); ?> </td>
                                <td style="font-size:12px; text-align:center;"> 
                                    <?php 
                                        if($adaptacion == "si" && $reposicion == "")
                                        { 
                                            echo "$ 0.00"; 
                                        }
                                        elseif( $reposicion == "si" && $adaptacion == "" )
                                        { 
                                            echo "$ 0.00"; 
                                        }
                                        else
                                        { 
                                            echo "$ ".number_format($costo_molde - $pago_parcial,2); 
                                        } 
                                    ?> 
                                </td>
                                <td style="font-size:12px; text-align:center;"> <?php echo ucfirst(strtolower($estatus_molde)); ?> </td>
                            </tr>
                            <tr>
                                <td colspan="4"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                            </tr>
            		<?php
            			}
            		?>
            		</table>
            		<br/>
            		<center>
	            		<div id="opciones">
                            <form name="form_moldes" style="float:right; margin-left:15px;" method="post" action="lista_control_moldes_excel.php">
                                <div id="spanreloj"></div>
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <a href="#" title="Imprimir Reporte" style="float:right;" onclick="window.open('imprimir_lista_control_moldes.php','Lista de Control de Moldes','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1050, height=700');"> <img src="../img/print icon.png"/> </a>
                        </div> <!-- FIN DIV OPCIONES -->
            		</center>
            		<br/>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>