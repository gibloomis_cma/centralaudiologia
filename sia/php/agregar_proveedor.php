<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Proveedor</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css" />
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body> 
<div id="contenido_columna2">
	<div class="contenido_pagina">
    	<div class="fondo_titulo1">
        	<div class="categoria">
            	Proveedores
            </div><!-- Fin DIV categoria -->
        </div><!--Fin de fondo titulo-->
        <div class="buscar2">
                <?php
                    // SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS 
                    include("config.php");								
                    if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                        $filtro = $_POST['filtro'];
                        $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                            FROM proveedores
                                                            WHERE razon_social LIKE '%".$filtro."%' 
                                                            OR proveedor LIKE '%".$filtro."%' 
															ORDER BY proveedor");	
                        $row_busqueda = mysql_fetch_array($res_busqueda);
                        $res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
                    }else{
						$res2="";
					}
                ?>              
            <form name="busqueda" method="post" action="agregar_proveedor.php">
                <label><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
        </div><!-- Fin de la clase buscar2 -->        
        <div class="area_contenido2">
                <center>
                <table>
                    <tr>
                        <th>Nombre</th>
                        <th colspan="2">Razon social</th>
                    </tr>
            <?php
                // QUERY QUE OBTIENE LOS PROVEEDORES
                if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                    $filtro = $_POST['filtro'];
                    $consulta_proveedores = mysql_query("SELECT DISTINCT(id_proveedor), 
                                                        razon_social, proveedor 
                                                        FROM proveedores
                                                        WHERE razon_social LIKE '%".$filtro."%' 
                                                        OR proveedor LIKE '%".$filtro."%' 
														ORDER BY proveedor");										
                            
                }else{
                $consulta_proveedores = mysql_query("SELECT DISTINCT(id_proveedor), 
													razon_social, proveedor 
                                                    FROM proveedores 
													ORDER BY proveedor");
                }
				$n_proveedores=0;
                while( $row_proveedores = mysql_fetch_array($consulta_proveedores)){					
                    $id_proveedor = $row_proveedores["id_proveedor"];
                    $proveedor = $row_proveedores["proveedor"];
                    $nombre = $row_proveedores["razon_social"];
					$n_proveedores++;
            ?>
                    <tr>
                        <td>                            
                            <?php echo $proveedor; ?>                            
                        </td>
                        <td> <?php echo $nombre; ?> </td>
                        <td>
                        	<a href="detalles_proveedor.php?id_proveedor=<?php echo $id_proveedor; ?>">
                            	<img src="../img/info.png" />
                            </a>
                        </td>
                    </tr>
            <?php		
                }
				if($n_proveedores==0){
			?>
					<tr>
                        <td style="text-align:center;" colspan="3">
                        	<label class="textos">"No hay proveedores registrados"</label>
                        </td>
                    </tr>            
            <?php
				}
            ?>
                </table>
                <br />
            <div class="titulos">Nuevo Proveedor</div> 
            <div class="contenido_proveedor" style="padding-top:30px;"> 
            	<center>
                <form action="procesa_agregar_proveedor.php" method="post" name="form_agregar_proveedor" 
                id="form_agregar_proveedor" onsubmit="return validarAgregarProveedor()" >                   
                    <table>
                    <style type="text/css">
						table tr td{
							color:#6d6d6d;
							text-align:center;
							font-weight:bold;
							font-size:12px;
							font-family:Tahoma, Geneva, sans-serif;
						}
					</style>
                        <tr>
                            <td style="text-align:right">
                            	<label class="textos">Nombre: </label>
                            </td>
                            <td style="text-align:left" colspan="3">
                            	<input name="proveedor" id="proveedor" type="text" title="Proveedor" size="60"/>
                            </td>
                       	</tr><tr>
                            <td style="text-align:right">
                            	<label class="textos">Razon Social: </label>
                            </td>
                            <td style="text-align:left" colspan="3">
                            	<input name="nombre" id="nombre" type="text" size="90" title="Razón Social" />
                            </td>
                        </tr><tr>                   
                            <td style="text-align:right">
                            	<label class="textos">Calle: </label>
                            </td>
                            <td style="text-align:left">
                            	<input name="calle" id="calle" type="text" size="30" title="Calle" />
                            </td>
                            <td style="text-align:right">
                            	<label class="textos">Número: </label>
                            </td>
                            <td style="text-align:left">
                            	<input name="num_ext" id="num_ext" type="text" size="3" 
                                maxlength="5" title="Exterior" onkeypress="return _soloNumeros(event)" />
                            	<label class="textos"> - </label>                           
                            	<input name="num_int" id="num_int" type="text" size="3" title="Interior" maxlength="5" />
                            </td>
                       </tr><tr>
                            <td style="text-align:right">
                            	<label class="textos">Colonia:</label>
                            </td>
                            <td style="text-align:left">
                            	<input name="colonia" id="colonia" type="text" size="30" title="Colonia" />
                            </td>
                            <td style="text-align:right">
                            	<label class="textos">C.P.: </label>
                            </td>
                            <td style="text-align:left;">
                                <input name="codigo_postal" id="codigo_postal" type="text" size="5" 
                                maxlength="5" title="Código Postal" onchange="validarCP(this.value)" />
                            	<label class="textos">&nbsp;&nbsp;&nbsp;RFC: </label>
                                <input name="rfc" id="rfc" type="text" title="RFC" size="18"/>
                            </td>
                        </tr><tr>
                        	<td style="text-align:right"><label class="textos">País: </label>
                            </td>
                            <td><select name="pais" id="pais">
                            	<option value="0">Seleccione</option>
                           		<?php
                                // QUERY QUE OBTIENE LOS ESTADOS DE MEXICO
                                $consulta_pais = mysql_query("SELECT id_pais, pais 
                                                                FROM paises
                                                                ORDER BY pais")or die(mysql_error());
                                while( $row3 = mysql_fetch_array($consulta_pais))
                                { 
                                    $id_pais = $row3["id_pais"];
                                    $pais = $row3["pais"];	
									if($id_pais==1){
                                ?>
                                    <option value="<?php echo $id_pais; ?>" selected="selected">
                                    	<?php echo utf8_encode($pais); ?>
                                    </option>
                                <?php
									}else{
									?>
                                     <option value="<?php echo $id_pais; ?>">
                                    	<?php echo utf8_encode($pais); ?>
                                    </option>
                                    <?php	
									}
                                }
                                ?>
                            </select></td>
                            <td id="alright">
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input name="pais_nuevo" type="text" maxlength="50" size="40"/>
                            </td>
                        </tr>
                        <tr>                    
                            <td style="text-align:right">
                            	<label class="textos">Estado: </label>
                            </td>
                            <td style="text-align:left">
                                <select id="id_estado" name="id_estado" title="Estados">
                                    <option value="0" selected="selected"> --- Estado --- </option>
                                </select>
                            </td>
                        	<td id="alright">
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input name="estado_nuevo" type="text" maxlength="50" size="40"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right">
                            	<label class="textos">Ciudad: </label>
                            </td>
                            <td style="text-align:left">
                                <select name="id_municipio" id="id_municipio" disabled="disabled" title="Municipio">
                                    <option value="0"> --- Ciudad --- </option>
                                </select>
                            </td>
                        	<td id="alright">
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input name="municipio_nuevo" type="text" maxlength="50" size="40"/>
                            </td>
                        </tr><tr>                    
                            <td>
                            	<label class="textos">Cr&eacute;dito: </label>
                            </td>
                            <td style="text-align:left">
                                <input name="credito" id="credito" type="text" title="Crédito" 
                                onkeypress="return _soloNumeros(event)" size="6" />
                            </td>
                            <td style="text-align:right">
                            	<label class="textos">Plazo de Pago: </label>
                            </td>
                            <td style="text-align:left">
                                <input name="plazo_pago" id="plazo_pago" type="text" title="Plazo de Pago" size="3"  
                                onkeypress="return _soloNumeros(event)" maxlength="5" />
                                <label class="textos">días</label>
                            </td> 
                        </tr><tr> 
                        	
                            <td style="text-align:right" colspan="4">
                            	<input name="accion" type="reset" value="Cancelar" class="fondo_boton" title="Cancelar"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            	<input name="accion" type="submit" value="Guardar" class="fondo_boton" title="Guardar"/>
                            </td>
                        </tr>
                    </table> 
				</form>  
                </center>           
                </div><!--Fin de contenido proveedor-->
			</div><!--Fin de area contenido-->
	</div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>