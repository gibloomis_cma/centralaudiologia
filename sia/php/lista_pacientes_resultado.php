<!-- include style -->
 <link rel="stylesheet" type="text/css" href="../css/style_pagination.css" />

<?php
    //open database
    include 'config.php';
    
    //include our awesome pagination
    //class (library)
    include '../librerias/ps_pagination.php';

    $filtro = $_POST['filtro'];

    if ( $filtro != "" )
    {
        // query all data anyway you want
        $sql = "SELECT DISTINCT(id_cliente), nombre, paterno, materno
                FROM ficha_identificacion
                                                                WHERE nombre LIKE '%".$filtro."%' 
                                                                OR paterno LIKE '%".$filtro."%' 
                                                                OR materno LIKE '%".$filtro."%'";
    }
    else
    {
        //query all data anyway you want
        $sql = "SELECT id_cliente, nombre, paterno, materno 
                FROM ficha_identificacion
                ORDER BY id_cliente";
    }

    //execute query and get and
    $rs = mysql_query($sql) or die(mysql_error());

    //now, where gonna use our pagination class
    //this is a significant part of our pagination
    //i will explain the PS_Pagination parameters
    //$conn is a variable from our config_open_db.php
    //$sql is our sql statement above
    //3 is the number of records retrieved per page
    //4 is the number of page numbers rendered below
    //null - i used null since in dont have any other
    //parameters to pass (i.e. param1=valu1&param2=value2)
    //you can use this if you're gonna use this class for search
    //results since you will have to pass search keywords
    $pager = new PS_Pagination( $link, $sql, 10, 3, null );

    //our pagination class will render new
    //recordset (search results now are limited
    //for pagination)
    $rs = $pager->paginate(); 

    //get retrieved rows to check if
    //there are retrieved data
    $num = mysql_num_rows( $rs );

    if( $num >= 1 )
    {     
    ?>
        <table>
            <tr>
                <th class="titulo" width="45"> Nº Nota </th>
                <th class="titulo" width="1000"> Nombre Completo </th>
                <th class="titulo" width="10"> </th>
            </tr>
    <?php    
        //looping through the records retrieved class='data-tr' align='center'
        while( $row = mysql_fetch_array( $rs ) )
        {
            $id_cliente = $row["id_cliente"];
            $nombre = $row["nombre"];
            $paterno = $row["paterno"];
            $materno = $row["materno"];
                                             
    ?>
            <tr>
                <td class="tamano_letra"> <?php echo $id_cliente ?> </td>
                <td class="tamano_letra"> <?php echo ucwords($nombre." ".$paterno." ".$materno); ?> </td>
                <td> <a href="detalles_paciente.php?id_cliente=<?php echo $id_cliente; ?>"> <img src="../img/info.png" /> </a> </td>
            </tr>   
    <?php
        }       
    ?>
        </table>
    <?php
    }
    else
    {
        // if no records found
        echo "<h3> No hay pacientes registrados</h3>";
    }

    //page-nav class to control
    //the appearance of our page 
    //number navigation
    echo "<div class='page-nav'>";
         //display our page number navigation
         echo $pager->renderFullNav();
    echo "</div>";

?>