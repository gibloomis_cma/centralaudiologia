<?php 
	session_start();
	include("config.php");
	include("metodo_cambiar_fecha.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Ticket de Ventas Diaria</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Reportes
                </div>   
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
  	<?php
		$id_registro_corte = $_GET["id_registro_corte"];
		$consulta_datos_del_corte=mysql_query("SELECT * FROM corte_ventas_diario 
														WHERE id_registro=".$id_registro_corte)or die(mysql_error()); 
		$row_datos_del_corte=mysql_fetch_array($consulta_datos_del_corte);
		$fecha_corte=$row_datos_del_corte["fecha_corte"];
		$fecha_corte_normal=cambiaf_a_normal($fecha_corte);
		$hora=$row_datos_del_corte["hora"];
		$id_empleado_corte=$row_datos_del_corte["id_empleado_corte"];
		$total_vendido=$row_datos_del_corte["total_vendido"];
		$total_articulos=$row_datos_del_corte["total_articulos"];
		$id_sucursal_corte=$row_datos_del_corte["id_sucursal"];
		$folio_corte=$row_datos_del_corte["folio_corte"];
	?>
            	<div class="contenido_proveedor">
  				<table>
                    <tr>
                        <td style="text-align:center" colspan="4">
                            <label class="textos" style="font-size:16px;">JORGE LEAL Y CIA. S.A. DE C.V. </label>
                            <br />
                            <label class="textos" style="font-size:14px;">Sebastian Lerdo de Tejada 186 </label>
                            <br />
                                <label class="textos" style="font-size:14px;">Col. Centro &nbsp;&nbsp;&nbsp; 
                                Morelia &nbsp;&nbsp;&nbsp; Michoacan </label>
                            <br />
                                <label class="textos" style="font-size:14px;">CP. 58000 </label>
                            <br />
                                <label class="textos" style="font-size:14px;">RFC: JLE010622GK2 </label>
                            <br />
                                <label class="textos" style="font-size:14px;">Telefono: (443) 312-5994 </label>
                            <br /><br />
                        </td>
                    </tr>
                    <tr><td colspan="4" style="text-align:center;">
					<?php
						if($id_sucursal_corte == 1){
							echo "Sucursal Matriz";	
						}else{
							echo "Sucursal Ocolusen";	
						}
					 ?></td></tr>
                    <tr>
                        <td id="alright">
                            <label class="textos">N° Folio de Corte de Venta: </label>
                            <?php echo $folio_corte; ?>
                        </td><td>
                        </td><td id="alright">
                            <label class="textos">Fecha del Corte: </label>
                        </td><td>
                            <?php echo $fecha_corte_normal." ".$hora; ?>                            
                        </td>
                    </tr>
                </table>
            	<br />
                <table style="border-collapse:collapse;">
                    <tr>
                        <th width="150px">Concepto</th>
                        <th width="150px">Cantidad de Articulos</th>
                        <th width="100px">Total</th>
                    </tr>
					<?php 
						$consulta_ventas_diaria = mysql_query("SELECT folio_num_venta, descuento, total, vendedor 
																		FROM ventas 
																		WHERE fecha ='".$fecha_corte."' AND
																		 id_sucursal=".$id_sucursal_corte) 
																		or die(mysql_error()); 
						while($row = mysql_fetch_array($consulta_ventas_diaria)) {
							$vendedor = $row["vendedor"];
							$folio_num_venta = $row["folio_num_venta"];
							$descuento = $row["descuento"];
							$total = $row["total"];
							$totalTotal += $total; 
							$consulta_ventas_diaria2 = mysql_query("SELECT cantidad, costo_unitario, id_categoria
																	FROM descripcion_venta 
																	WHERE folio_num_venta ='".$folio_num_venta."' 
																	AND id_sucursal=".$id_sucursal_corte) 
																	or die(mysql_error());
							while($row2 = mysql_fetch_array($consulta_ventas_diaria2)){
								$categoria = $row2["id_categoria"];
								$id_registro = $row2["id_registro"];
								$costo_unitario = $row2["costo_unitario"];
								$cantidad = $row2["cantidad"];
								$sumaTotal = ($cantidad * $costo_unitario);
								$sumaTotalDescuento = (($sumaTotal * $descuento[$i])/100);
								$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
								if($categoria == 1){
									$sumaTotal1 += $sumaTotalNueva;
									$cantidad1 += $cantidad;
								}
								if($categoria == 2){
									$sumaTotal2 += $sumaTotalNueva;
									$cantidad2 += $cantidad;	
								}
								if($categoria == 4){
									$sumaTotal3 += $sumaTotalNueva;
									$cantidad3 += $cantidad;
								}
								if($categoria == 100){
									$sumaTotal4 += $sumaTotalNueva;
									$cantidad4 += $cantidad;	
								}
							}
						}
					?>
                    <tr>
                        <td>Refacciones</td>
                        <td style="text-align:center;"><?php echo $cantidad1; ?></td>
                        <td id="alright"><?php echo "$".number_format($sumaTotal1,2); ?></td>
                    </tr><tr>
                        <td>Baterias</td>
                        <td style="text-align:center;"><?php echo $cantidad3; ?></td>
                        <td id="alright"><?php echo "$".number_format($sumaTotal3,2); ?></td>
                    </tr><tr>
                        <td>Consultas</td>
                        <td style="text-align:center;"><?php echo $cantidad2; ?></td>
                        <td id="alright"><?php echo "$".number_format($sumaTotal2,2); ?></td>
                    </tr><tr>
                        <td>Reparaciones</td>
                        <td style="text-align:center;"><?php echo $cantidad4; ?></td>
                        <td id="alright"><?php echo "$".number_format($sumaTotal4,2); ?></td>
                    </tr><tr>
                    	<td colspan="3">
                        	<hr />
                        </td>
                    </tr>
                </table>
                    <center>
                      	<?php $cantidadTotal = $cantidad1+$cantidad2+$cantidad3+$cantidad4;?> 
                		<label class="textos">Cantidad Total de Articulos: <?php echo $cantidadTotal; ?>
                        <br /><br />
                        Total: <span style="color:#F00"><?php echo "$".number_format($totalTotal,2); ?></span>
                        </label>
                        <input name="total_venta" type="hidden" value="<?php echo $totalTotal; ?>" />
                        <br /><br />
           	<?php
				$consulta_datos_empleado = mysql_query("SELECT nombre, materno, paterno 
														FROM empleados WHERE id_empleado=".$id_empleado_corte)
														or die(mysql_error());
				$row_datos_empleado=mysql_fetch_array($consulta_datos_empleado);
				$nombre_empleado=$row_datos_empleado["nombre"];
				$paterno=$row_datos_empleado["paterno"];
				$materno=$row_datos_empleado["materno"];
			?>
                        <textarea cols="30" rows="2" name="firma_encargada" style="resize:none; text-align:center;" readonly="readonly"><?php echo $nombre_empleado." ".$paterno." ".$materno; ?></textarea>
                        <br />
                        <label class="textos">Firma de Encargado(a) del Corte</label>
                    </center>
                   	<p align="right">
                    <a href="reporte_ticket_venta_diaria.php">
                		<input name="accion" type="submit" value="Volver" class="fondo_boton" />
            		</a>
                    </p>
                    <br />
            	</div><!--Fin de contenido proveedor-->    
        	</div><!--Fin de area contenido-->
    	</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>