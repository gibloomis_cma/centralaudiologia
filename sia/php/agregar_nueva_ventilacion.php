<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Nueva Ventilacion | SIA </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Ventilaciones
                </div><!-- FIN DIV CLASS CATEGORIA -->
            </div><!-- FIN DIV CLASS FONDO TITULO1 -->
                <div class="area_contenido1">
                    <br/>
                    <br/>
                    <?php
                        // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
                        include("config.php");
                        // QUERY QUE OBTIENE LAS VENTILACIONES EXISTENTES EN LA BASE DE DATOS
                        $query_ventilaciones= mysql_query("SELECT id_ventilacion,nombre_ventilacion,calibre,precio
                                                           FROM ventilaciones
                                                           ORDER BY id_ventilacion ASC") or die (mysql_error());
                    ?>
                    <center>
                    <table>
                        <tr>
                            <th width="150"> N° de Ventilación </th>
                            <th> Nombre Ventilación </th>
                            <th> Calibre </th>
                            <th width="80"> Precio </th>
                            <th width="25"></th>
                        </tr>
                    <?php
                    while($row_ventilacion = mysql_fetch_array($query_ventilaciones)){
                        $id_ventilacion = $row_ventilacion['id_ventilacion'];
                        $nombre_ventilacion = $row_ventilacion['nombre_ventilacion'];
                        $calibre = $row_ventilacion['calibre'];
                        $precio = $row_ventilacion['precio'];
                    ?>
                        <tr>
                            <td id="centrado"> <?php echo $id_ventilacion; ?> </td>
                            <td> <?php echo utf8_encode(ucwords(strtolower($nombre_ventilacion))); ?> </td>
                            <td> <?php echo $calibre; ?> </td>
                            <td> $<?php echo number_format($precio,2); ?> </td>
                            <td> <a href="modificar_ventilacion.php?id_ventilacion=<?php echo $id_ventilacion; ?>">
                                    <img src="../img/modify.png" />
                                 </a>
                            </td>
                        </tr>
                    <?php	
                    } 
                    ?>
                    </table>
                    </center>
                    <br/>
                    <div class="titulos"> Agregar Nueva Ventilación </div>
                    <center>
                        <form name="form_agregar_nueva_ventilacion" id="form_agregar_nueva_ventilacion" method="post" action="procesa_agregar_nueva_ventilacion.php" enctype="multipart/form-data" onsubmit="return validaAgregarNuevaVentilacion()">
                            <br/>
                            <table>
                                <tr>
                                    <td id="alright">
                                    	<label class="textos"> Nombre de la Ventilación: </label> 
                                    </td>
                                    <td id="alleft"> 
                                    	<input type="text" name="txt_nombre_ventilacion" id="txt_nombre_ventilacion"/> 
                                    </td>
                                </tr><tr>
                                    <td id="alright">
                                    	<label class="textos"> Calibre: </label> 
                                    </td>
                                    <td id="alleft"> 
                                    	<input type="text" name="txt_calibre_ventilacion" id="txt_calibre_ventilacion"/>
                                        <label class="textos">&nbsp;mm</label> 
                                   	</td>
                                </tr><tr>
                                	<td id="alright">
                                    	<label class="textos"> Precio: </label>
                                    </td>
                                    <td id="alleft">
                                     	$<input name="precio" type="text" size="10" maxlength="10" />
                                    </td>
                                </tr><tr>
                                    <td id="alright">
                                    	<label class="textos"> Imagen Ventilación: </label> 
                                    </td>
                                    <td id="alleft"> 
                                    	<input type="file" name="imagen_vent" id="imagen_vent" accept="image" style="height:25px;"/> 
                                   	</td>
                                </tr><tr>
                                    <td colspan="2">
                                        <p align="right">
                                            <input type="reset" name="cancelar" id="cancelar" class="fondo_boton" value="Cancelar" title="Cancelar"/>
                                            <input type="submit" name="accion" id="accion" value="Aceptar" title="Aceptar" class="fondo_boton"/>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </center>
                    <br/>
        	</div><!-- FIN DIV AREA CONTENIDO 1 -->
        </div><!-- FIN DIV CONTENIDO PAGINA -->
	</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
</div><!-- FIN DIV WRAPP -->
</body>
</html>