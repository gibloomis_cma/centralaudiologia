<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
    $fecha_final_mysql = $_GET['fecha_final'];
    $fecha_final_separada = explode("-", $fecha_final_mysql);
    $fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
    $fecha_inicio_mysql = $_GET['fecha_inicio'];
    $fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$contador = 0;
	$total_baterias_vales = 0;
	$total_global_vales = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Bater&iacute;as que Salen </title>
    <script type="text/javascript" language="javascript">
        function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  h + ':' + min + ':' + sec;
			window.print() ;
        }
     </script>
</head>
<body onload="hora()">
	<center>
    	<table>
        	<tr>
             	<td colspan="9" style="color:#000;  font-size:28px; text-align:center; font-weight:bold;"> BATER&Iacute;AS QUE SALEN </td>
        	</tr>
            <tr>
            	<td colspan="9" style="color:#000; font-size:22px; text-align:center; font-weight:bold;"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
            </tr>
        </table>
		<table border="1" style="width:1100px;">
            <tr>
            	<th style="font-size:16px;"> Fecha </th>
            	<th style="font-size:16px;"> C&oacute;digo </th>
            	<th style="font-size:16px;"> Concepto </th>
            	<th style="font-size:16px;"> Cantidad </th>
            	<th style="font-size:16px;"> Origen </th>
            	<th style="font-size:16px;"> Destino </th>
            	<th style="font-size:16px;"> Responsable </th>
            	<th style="font-size:16px;"> Observaciones </th>
            	<th style="font-size:16px;"> Costo Total </th>
            </tr>
					<?php
                        // SE REALIZA QUERY QUE OBTIENE TODAS LAS REFACCIONES QUE SALEN DE VALES DE REFACCIONES
                        $query_baterias_vales = "SELECT fecha,uso_de_bateria,responsable,observaciones,origen,codigo,cantidad
                                                                        FROM vales_baterias,descripcion_vale_baterias
                                                                        WHERE vales_baterias.id_vale_bateria = descripcion_vale_baterias.id_vale_bateria
                                                                        AND uso_de_bateria IN (1,2)
                                                                        AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                                                        ORDER BY fecha DESC";
                                        
                        // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                        $resultado_baterias_vales = mysql_query($query_baterias_vales) or die(mysql_error());
                                        
                        // SE REALIZA UN CICLO PARA MOSTRAR TODAS LAS REFACCIONES OBTENIDAS
                        while( $row_baterias_vale = mysql_fetch_array($resultado_baterias_vales) )
                        {
                                $contador++;
                                $fecha = $row_baterias_vale['fecha'];
                                $fecha_separada = explode("-", $fecha);
                                $fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
                                $uso = $row_baterias_vale['uso_de_bateria'];
                                                
                                // SE REALIZA QUERY QUE OBTIENE EL USO DEL VALE DE REFACCION
                                $query_uso_vale = "SELECT uso_bateria
                                                                      FROM uso_baterias
                                                                      WHERE id_uso_bateria = '$uso'";
                                                                                        
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                $resultado_uso_vale = mysql_query($query_uso_vale) or die(mysql_error());
                                $row_uso_vale = mysql_fetch_array($resultado_uso_vale);
                                $destino = $row_uso_vale['uso_bateria'];
                                                
                                $responsable = $row_baterias_vale['responsable'];
                                $observaciones = $row_baterias_vale['observaciones'];
                                $origen = $row_baterias_vale['origen'];
                                                
                                // SE REALIZA QUERY QUE OBTIENE EL ORIGEN DEL VALE DE REFACCION
                                $query_uso_vale2 = "SELECT almacen 
                                                                        FROM almacenes 
                                                                        WHERE id_almacen = '$origen'";
                                                                                        
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                $resultado_uso_vale2 = mysql_query($query_uso_vale2) or die(mysql_error());
                                $row_uso_vale2 = mysql_fetch_array($resultado_uso_vale2);
                                $origen_descripcion = $row_uso_vale2['almacen'];
                                                
                                $codigo = $row_baterias_vale['codigo'];
                                $cantidad_bateria_vale = $row_baterias_vale['cantidad'];
                                                
                                // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE CADA UNA DE LAS REFACCIONES QUE SALEN
                                $query_datos_refacciones = "SELECT referencia_codigo,descripcion,precio_compra
                                                                                        FROM base_productos
                                                                                        WHERE id_base_producto = '$codigo'";
                                                
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                                $resultado_datos_refaccion = mysql_query($query_datos_refacciones) or die(mysql_error());
                                $row_datos_refaccion = mysql_fetch_array($resultado_datos_refaccion);
                                $referencia_codigo = $row_datos_refaccion['referencia_codigo'];
                                $descripcion = $row_datos_refaccion['descripcion'];
                                $precio_compra = $row_datos_refaccion['precio_compra'];
                                $total_baterias_vales = $precio_compra * $cantidad_bateria_vale;
                                $total_global_vales += $total_baterias_vales;
                                                
                                // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO RESPONSABLE
                                $query_nombre_empleado_responsable = "SELECT nombre
                                                                    FROM empleados
                                                                    WHERE id_empleado = '$responsable'";
                                                                                        
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                $resultado_nombre_empleado = mysql_query($query_nombre_empleado_responsable) or die(mysql_error());
                                $row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
                                $nombre_empleado_vale = $row_nombre_empleado['nombre'];
                        ?>
                         <tr>
                         		<td style="font-size:14px; text-align:center;"> <?php  echo $fecha_normal; ?> </td>
                         		<td style="font-size:14px; text-align:center;"> <?php  echo $referencia_codigo; ?>  </td>
                         		<td style="font-size:14px;"> <?php  echo $descripcion; ?> </td>
                         		<td style="font-size:14px; text-align:center;"> <?php  echo $cantidad_bateria_vale; ?> </td>
                         		<td style="font-size:14px;"> <?php  echo $origen_descripcion; ?> </td>
                         		<td style="font-size:14px; text-align:center;"> <?php  echo $destino; ?></td>
                         		<td style="font-size:14px; text-align:center;"> <?php  echo $nombre_empleado_vale; ?> </td>
                         		<td style="font-size:14px;"> <?php  echo $observaciones;?>  </td>
                         		<td style="font-size:14px; text-align:right;"> <?php  echo "$".number_format($total_baterias_vales,2); ?> </td>
                         </tr>
                        <?php
						}
                        // SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
                            $query_ventas_notas = "SELECT fecha,ventas.folio_num_venta AS folio, descripcion, 
                                                        cantidad,vendedor, sucursales.nombre as sucursal
                                                        FROM descripcion_venta,ventas, sucursales
                                                        WHERE ventas.folio_num_venta = descripcion_venta.folio_num_venta
                                                        AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                                        AND id_categoria=4
                                                        AND descripcion_venta.id_sucursal=sucursales.id_sucursal
                                                        AND ventas.id_sucursal=sucursales.id_sucursal
                                                        AND descripcion_venta.id_sucursal=ventas.id_sucursal
                                                        ORDER BY fecha DESC";
                            
                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                            $resultado_ventas_notas = mysql_query($query_ventas_notas) or die(mysql_error());
                            // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                            $total_global_notas=0;
                            while($row_venta_nota = mysql_fetch_array($resultado_ventas_notas)){
                                $contador++;
                                $fecha_entrada = $row_venta_nota['fecha'];
                                $fecha_entrada_separada = explode("-", $fecha_entrada);                             
                                $fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
                                $folio_venta = $row_venta_nota['folio'];
                                $codigo_venta = $row_venta_nota['descripcion'];
                                $cantidad = $row_venta_nota['cantidad'];
                                $id_empleado = $row_venta_nota['vendedor']; 
                                $sucursal=$row_venta_nota['sucursal'];                                                          
                                // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE CADA UNA DE LAS REFACCIONES QUE SALEN
                                $query_datos_ventas = "SELECT referencia_codigo,descripcion,precio_compra
                                                            FROM base_productos
                                                            WHERE descripcion = '".$codigo_venta."'";
                                
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
                                $resultado_datos_venta = mysql_query($query_datos_ventas) or die(mysql_error());
                                $row_datos_venta = mysql_fetch_array($resultado_datos_venta);
                                $referencia_codigo = $row_datos_venta['referencia_codigo'];
                                $descripcion = $row_datos_venta['descripcion'];
                                $precio_compra = $row_datos_venta['precio_compra'];                             
                                $total_venta = $precio_compra * $cantidad;
                                $total_global_notas += $total_venta;                                
                                // SE REALIZA QUERY QUE OBTIENE EL RESPONSABLE
                               
                                // SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO RESPONSABLE
                                $query_nombre_empleado_responsable = "SELECT nombre
                                                                    FROM empleados
                                                                    WHERE id_empleado = '".$id_empleado."'";
                                                                                                                    
                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                $resultado_nombre_empleado = mysql_query($query_nombre_empleado_responsable) or die(mysql_error());
                                $row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
                                $nombre_empleado_nota = $row_nombre_empleado['nombre'];
                                ?>
                        <tr>
                            <td style="font-size:14px; text-align:center;"> <?php  echo $fecha_entrada_normal; ?> </td>
                            <td style="font-size:14px; text-align:center;"> <?php  echo $referencia_codigo; ?>  </td>
                            <td style="font-size:14px;"> <?php  echo $codigo_venta; ?> </td>
                            <td style="font-size:14px; text-align:center;"> <?php  echo $cantidad; ?></td>
                            <td style="font-size:14px;"> <?php echo $sucursal; ?> </td>
                            <td style="font-size:14px;"> Venta N° <?php echo $folio_venta; ?> </td>
                            <td style="font-size:14px;"> <?php  echo $nombre_empleado_nota; ?> </td>
                            <td style="font-size:14px;">  </td>
                            <td style="font-size:14px; text-align:right"><?php  echo "$".number_format($total_venta,2); ?> </td>
                        </tr>                        
                        <?php
                            }
                            $total_global = $total_global_notas + $total_global_vales;
						?>
                        <tr>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td></td>
                    		<td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Total: <br /> <?php echo "$".number_format($total_global,2); ?> </td>
                        </tr>
                        <tr>
                    		<td id="hora" colspan="9" style="text-align:center; font-size:14px; font-weight:bold;"> Fecha y hora de Impresi&oacute;n: <br /> <?php echo date('d/m/Y')."  ";?> </td>
                        </tr>
    		</table>
	 </center>
</body>
</html>