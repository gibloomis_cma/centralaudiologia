<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orden de Compra</title>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script>
$(document).ready(function(){	
	$('#id_categoria').change(function(){
		
		$.post('productos.php',{variable:$(this).val()},function(data){
			$('#id_producto option').not(':first').remove();
			$('#id_producto').append(data);
			$('#id_producto').removeAttr('disabled'); 			
		})	
	})
})
</script>
</head>
<body>        
<div id="contenido_columna2">
    <div class="contenido_pagina">                
        <div class="fondo_titulo1">                    
            <div class="categoria">
                Proveedores
            </div><!-- Fin de categoria -->                        
        </div><!-- Fin de fondo titulo 1 -->                    
        <div class="area_contenido1">
            <br />
            <form name="forma1" action="procesa_orden_de_compra.php" method="post">                        
            <div class="titulos">Orden de Compra</div>
            <br />
            <div class="contenido_proveedor">
            	<table>
                	<tr>
                    	<td id="alright">
                        	<label class="textos">Proveedor:</label>
                        </td><td>
						<?php 	
                            if(isset($_POST['proveedor'])){
                                $id_proveedor = $_POST['proveedor'];
								$_SESSION['id_proveedor']=$_POST['proveedor'];
                            }else{
                                $id_proveedor = $_GET['id_proveedor'];
								$_SESSION['id_proveedor']=$_GET['id_proveedor'];
                            }
                            include('config.php');
                            $proveedor = mysql_query('SELECT razon_social 
                                        FROM proveedores
                                        WHERE id_proveedor = '.$id_proveedor)
                                        or die(mysql_error());
                            $row_proveedor = mysql_fetch_array($proveedor);
                            echo $row_proveedor['razon_social'];
                        ?>
                        </td><td id="alright">
						<?php
                            $fecha_entrega="";
                            if(isset($_GET['folio'])){
                                $folio = $_GET['folio'];
                                $c_fecha_entrega=mysql_query('SELECT fecha_entrega 
															FROM ordenes_de_compra 
															WHERE folio_orden_compra='.$folio)
															or die(mysql_error());
                                $row_c_fecha_entrega=mysql_fetch_array($c_fecha_entrega);
                                $fecha_entrega=$row_c_fecha_entrega['fecha_entrega'];
                            }else{
                                $max_folio = mysql_query('SELECT MAX(folio_orden_compra)+1 AS folio
                                                    FROM ordenes_de_compra');
                                $row_folio = mysql_fetch_array($max_folio);
                                $folio = $row_folio['folio'];
                                if($folio==""){
                                    $folio=1;
                                }
                            }
                        ?>
                			<label class="textos">Folio: </label>                            
                		</td><td>
                        	<input type="hidden" name="folio" value="<?php echo $folio; ?>" />
							<?php echo $folio; ?>
               		 	</td>
					</tr><tr>
                        <td id="alright">
                			<label class="textos">Fecha de Orden: </label>
                		</td><td>
                            <input name="fecha_orden" type="type" readonly="readonly" size="10"
                            value="<?php echo date('d/m/Y')?>" style="text-align:center" />
						<?php
                            if($fecha_entrega!=""){
                                $readonly = 'readonly="readonly"';
                            }else{
                                $readonly="";
								$fecha_entrega="";
                            }
                        ?>
                    	</td><td id="alright">
               				<label class="textos">Fecha de Entrega: </label>
                		</td><td>
                            <input name="fecha_entrega" type="type" <?php echo $readonly ?> size="10"
                            style="text-align:center" value="<?php echo $fecha_entrega; ?>" />
						</td>
               		</tr><tr>
                		<td id="alright">
            				<label class="textos">Categoria: </label>
                		</td><td>
                            <select name="id_categoria" id="id_categoria" style="width:250px;">
                                <option value="0">Seleccionar</option>
                                <option value="1">Refacciones</option>
                                <option value="4">Baterias</option>
                                <option value="5">Modelos</option>                    
                            </select>
                		</td>
               		</tr><tr>                        
                        <td id="alright">
                			<label class="textos">Producto: </label>
                		</td><td>
                            <select name="id_producto" id="id_producto" disabled="disabled" style="width:250px;">
                                <option value="0">Seleccionar</option>
                            </select>
                		</td><td id="alright">
               				<label class="textos">Cantidad: </label>
                		</td><td>
                			<input name="cantidad" type="text" size="4" />
                		</td>
                	</tr><tr>
                    	<td colspan="6" id="alright">
                            <input name="id_proveedor" type="hidden" value="<?php echo $id_proveedor; ?>" />                   
                   			<input name="accion" type="submit" value="Agregar a Orden" class="fondo_boton"/>
                        </td>
                    </tr>
                </table>                
                <center>
                <table>
                    <tr>
                    	<th>Categoria</th>
                        <th>Producto</th>
                        <th colspan="2">Cantidad</th>                        
                    </tr>
                    <?php
						$lista_productos = mysql_query('SELECT base_productos.id_categoria as id, categoria, descripcion,
													referencia_codigo, SUM(cantidad), ordenes_de_compra.id_registro as producto
													FROM categorias_productos, base_productos, ordenes_de_compra
													WHERE categorias_productos.id_categoria = base_productos.id_categoria 
													AND base_productos.id_base_producto = ordenes_de_compra.id_producto 
													AND folio_orden_compra = '.$folio.' 
													GROUP BY referencia_codigo')or die(mysql_error());
						$p=0;
						while($row_lista_productos = mysql_fetch_array($lista_productos)){
							$p++;
							$id_categoria=$row_lista_productos['id'];
							$id_producto=$row_lista_productos['producto'];
							if($id_categoria==1){
								$imprime=$row_lista_productos['referencia_codigo'];
							}else{
								$imprime=$row_lista_productos['descripcion'];
							}
					?>
                    <tr>
                        <td><?php echo $row_lista_productos['categoria']; ?></td>
                        <td><?php echo $imprime; ?></td>
                        <td style="text-align:center"><?php echo $row_lista_productos['SUM(cantidad)']; ?></td>
                        <td style="text-align:center">
                        	<a href="eliminar_producto_orden_compra.php<?php echo "?id_proveedor=".$id_proveedor."&id_producto=".$id_producto."&folio=".$folio; ?>">
                            	<img src="../img/delete.png" title="<?php echo $imprime; ?>" />
                            </a>
                        </td>
                    </tr>
                    <?php
						}
						if($p==0){						
					?>
                    <tr>
                    	<td style="text-align:center" colspan="4">
                        	<label class="textos">Orden de Compra vacia</label>
                        </td>
                    </tr>
                    <?php
						}
					?>
                </table>
                </center>
                <hr />
                <p align="center">
                	<input type="button" name="volver" value="Volver"
                    onclick="window.location.href='ordenes_de_compra.php?id_proveedor=<?php echo $id_proveedor ?>'" class="fondo_boton" />
                    
                    <?php
						if($p!=0){
					?>
                    <input type="button" name="Cancelar" value="Cancelar Orden" class="fondo_boton"
                    onclick="window.location.href='eliminar_producto_orden_compra.php?id_proveedor=<?php echo $id_proveedor."&folio=".$folio; ?>'" />
                    <input name="accion" type="submit" value="Generar Orden" class="fondo_boton"/>
                    <?php
						}
					?>
                </p>
            </div><!--Fin de contenido proveedor-->
        </form>
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>