<?php 
	include("config.php");
	include("metodo_cambiar_fecha.php");
	$accion = $_POST["accion"];
	$num_folio_reparacion = $_POST["folio"];
	$id_estatus_reparaciones = $_POST["id_estatus_reparaciones"];
	
	/* Variables de presupuesto */
	$presupuesto = $_POST["presupuesto"];
	$tiempo_estimado = $_POST["estimado"];
	$autorizo = $_POST["autorizo"];
	$quien = $_POST["quien_autorizo"];
	$fecha_autorizacion = $_POST["fecha_autorizacion"];	
	$fecha_autorizacion_mysql=cambiaf_a_mysql($fecha_autorizacion);
	
	$fecha_actual = $_POST["fecha_actual"];
	$fecha_actual_mysql=cambiaf_a_mysql($fecha_actual);
	$id_empleado = $_POST["id_empleado"];
	$descripcion = $_POST["descripcion"];
	$cantidad = $_POST["cantidad"];
	$subcategoria = $_POST["subcategoria"];
	$costo_Total = $_POST["costo_Total"];
	$observaciones = $_POST["observaciones"];
	$hora = $_POST["hora"];
	
	/* Variables de reparacion */
	$fecha_reparacion = $_POST["fecha_reparacion"];
	$fecha_reparacion_mysql=cambiaf_a_mysql($fecha_reparacion);
	$responsable_reparacion = $_POST["reparado"];
	$garantia_reparacion = $_POST['txt_reparacion'];
	$garantia_adaptacion = $_POST['txt_adaptacion'];
	$garantia_venta = $_POST['txt_venta'];
	$se_cobra = $_POST['check_se_cobra'];
	$no_reparado = $_POST['check_reparado'];
	
	/* Variables de entrega */
	$recibio = $_POST["recibio"];
	$cobro_total = $_POST["cobro_total"];
	$fecha_entrega = $_POST["fecha_entrega"];
	$fecha_entrega_mysql=cambiaf_a_mysql($fecha_entrega);

	if($accion == "Cambiar Estatus"){
		/* Si estatus es igual a 5.-Laboratorio entonces se guarda el movimiento y se actualiza el estatus del auxiliar */
		if($id_estatus_reparaciones == 5){
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
												 id_estatus_reparaciones, id_empleado)
										VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."',
												'".$id_estatus_reparaciones."','".$id_empleado."')") 
												or die(mysql_error());
												
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());
		
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
		/* Si estatus es igual a 15 o 16 entonces se guarda el movimiento y se actualiza el estatus del auxiliar */	
		if($id_estatus_reparaciones == 15 or $id_estatus_reparaciones == 16){
			/* Se registra el movimiento realizado */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
												 id_estatus_reparaciones, id_empleado)
										VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."',
												'".$id_estatus_reparaciones."','".$id_empleado."')") 
												or die(mysql_error());
			/* Se actualiza el estatus del auxiliar */
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());									
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
		/* Si estatus es igual a 2.-Recepcion Matriz, No reparado, entonces haces lo siguiente */
		if($id_estatus_reparaciones == 2){
			/* Se registra el movimiento realizado */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
												 id_estatus_reparaciones, id_empleado)
										VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."',
												'".$id_estatus_reparaciones."','".$id_empleado."')") 
												or die(mysql_error());
			/* Se actualiza el estatus del auxiliar */
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());

			/* SE ACTUALIZA EL PRECIO Y PRESUPUESTO SI NO HA SIDO REPARADO */
			mysql_query("UPDATE reparaciones_laboratorio SET presupuesto = '0', costo = '0'
						 WHERE folio_num_reparacion = '$num_folio_reparacion'") or die(mysql_error());
			
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
		/* Si estatus es igual a 13.-Recepcion Ocolusen, No reparado, entonces haces lo siguiente */
		if($id_estatus_reparaciones == 13)
		{
			/* Se registra el movimiento realizado */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."','".$id_estatus_reparaciones."','".$id_empleado."')") or die(mysql_error());
			
			/* Se actualiza el estatus del auxiliar */
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());

			/* SE ACTUALIZA EL PRECIO Y PRESUPUESTO SI NO HA SIDO REPARADO */
			mysql_query("UPDATE reparaciones_laboratorio SET presupuesto = '0', costo = '0'
						 WHERE folio_num_reparacion = '$num_folio_reparacion'") or die(mysql_error());

			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
		/* Si estatus es igual a 2.-Recepcion-Reparado entonces se actualiza el estatus del auxiliar y se actualiza la tabla reparaciones */
		if($id_estatus_reparaciones == 9){
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
														 id_estatus_reparaciones, id_empleado)
												VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."','9',
														'".$id_empleado."')") or die(mysql_error());			
			header('Location: lista_reparaciones.php');
		}elseif($id_estatus_reparaciones == 14){
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
														 id_estatus_reparaciones, id_empleado)
												VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."','14',
														'".$id_empleado."')") or die(mysql_error());			
			header('Location: lista_reparaciones.php');
		}
		/* Si estatus es igual a 12.-Laboratorio, No reparado, entonces se actualiza el estatus del auxiliar y se actualiza la tabla de reparaciones */
		if($id_estatus_reparaciones == 12){		
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones.", no_reparado='si'
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
														 id_estatus_reparaciones, id_empleado)
												VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','12',
														'".$id_empleado."')") or die(mysql_error());

			/* SE ACTUALIZA EL PRECIO Y PRESUPUESTO SI NO HA SIDO REPARADO */
			mysql_query("UPDATE reparaciones_laboratorio SET presupuesto = '0', costo = '0', observaciones = '$observaciones',
						reparado_por=".$id_empleado.", fecha_salida='".$fecha_actual_mysql."', hora_ultima_modificacion='".$hora."'
						WHERE folio_num_reparacion = '$num_folio_reparacion'") or die(mysql_error());

			header('Location: lista_reparaciones.php');
		}
		if($id_estatus_reparaciones == 11){
			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones =".$id_estatus_reparaciones."
											WHERE folio_num_reparacion = ".$num_folio_reparacion)
											or die(mysql_error());
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
														 id_estatus_reparaciones, id_empleado)
												VALUES('".$num_folio_reparacion."','2','".$fecha_actual_mysql."','".$hora."','11',
														'".$id_empleado."')") or die(mysql_error());
			header('Location: lista_reparaciones.php');
		}
	}
	/* Si accion es igual a Guardar presupusto, se guarda el presupuesto en la tabla reparaciones laboratorio y se actualiza el estatus del auxiliar */
	if($_POST["guardar_presupuesto"] == "Guardar Presupuesto")
	{
		if ( $no_reparado == "si" )
		{
			// SE REALIZA QUERY QUE OBTIENE EL TOTAL DE SERVICIOS QUE SE BRINDAN
			$consulta_servicios_laboratorio = mysql_query("SELECT COUNT(id_servicio) 
														   FROM servicios") or die(mysql_error());

			// SE ALMACENA EL RESULTADO EN UNA VARIABLE EN FORMA DE ARREGLO Y POSTERIORMENTE EN UNA VARIABLE
			$row_servicios_laboratorio = mysql_fetch_array($consulta_servicios_laboratorio);
			$count = $row_servicios_laboratorio['COUNT(id_servicio)'];

			if( $count > 0 )
			{
				$detalles_servicios = mysql_query('SELECT id_servicio 
												   FROM servicios 
												   ORDER BY id_servicio') or die(mysql_error());
				$i = 0;
				while( $row_detalles_servicios = mysql_fetch_array($detalles_servicios) )
				{
					$id_servicio[$i] = $row_detalles_servicios['id_servicio'];
					$i++;			
				}
				for( $j = 0; $j < $count; $j++ )
				{
					if( isset($_POST['descripcion_'.$id_servicio[$j]]) )
					{
						mysql_query("INSERT INTO servicios_reparaciones(folio_num_reparacion, id_servicio)
									 VALUES('".$num_folio_reparacion."','".$id_servicio[$j]."')") or die(mysql_error());
					}
				}
			}

			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones = 12, no_reparado = '$no_reparado'
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
			
			mysql_query("INSERT INTO reparaciones_laboratorio(folio_num_reparacion, presupuesto, tiempo_estimado_reparacion, observaciones, costo)
						 VALUES('".$num_folio_reparacion."','".$presupuesto."','".$tiempo_estimado."','".$observaciones."','".$presupuesto."')") or die(mysql_error());
			
			/* Se guarda el movimiento */				
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','12','".$id_empleado."')") or die(mysql_error());
			
			header('Location: lista_reparaciones.php');
		}
		elseif ( $garantia_venta == "si" || $se_cobra == "si" ) 
		{
			// SE REALIZA QUERY QUE OBTIENE EL TOTAL DE SERVICIOS QUE SE BRINDAN
			$consulta_servicios_laboratorio = mysql_query("SELECT COUNT(id_servicio) 
														   FROM servicios") or die(mysql_error());

			// SE ALMACENA EL RESULTADO EN UNA VARIABLE EN FORMA DE ARREGLO Y POSTERIORMENTE EN UNA VARIABLE
			$row_servicios_laboratorio = mysql_fetch_array($consulta_servicios_laboratorio);
			$count = $row_servicios_laboratorio['COUNT(id_servicio)'];

			if( $count > 0 )
			{
				$detalles_servicios = mysql_query('SELECT id_servicio 
												   FROM servicios 
												   ORDER BY id_servicio') or die(mysql_error());
				$i = 0;
				while( $row_detalles_servicios = mysql_fetch_array($detalles_servicios) )
				{
					$id_servicio[$i] = $row_detalles_servicios['id_servicio'];
					$i++;			
				}
				for( $j = 0; $j < $count; $j++ )
				{
					if( isset($_POST['descripcion_'.$id_servicio[$j]]) )
					{
						mysql_query("INSERT INTO servicios_reparaciones(folio_num_reparacion, id_servicio)
									 VALUES('".$num_folio_reparacion."','".$id_servicio[$j]."')") or die(mysql_error());
					}
				}
			}
			
			mysql_query("INSERT INTO reparaciones_laboratorio(folio_num_reparacion, presupuesto, tiempo_estimado_reparacion, observaciones, costo)
						 VALUES('".$num_folio_reparacion."','".$presupuesto."','".$tiempo_estimado."','".$observaciones."','".$presupuesto."')") or die(mysql_error());
			
			/* Se guarda el movimiento */				
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','4','".$id_empleado."')") or die(mysql_error());

			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones = 7, aplica_garantia = '$se_cobra'
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
				
			/* Se guarda el movimiento */				
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones,id_empleado)
						  VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','7','".$id_empleado."')") or die(mysql_error());
				
			mysql_query("UPDATE reparaciones_laboratorio SET autorizo_presupuesto='Si', quien_autorizo='Automatico',fecha_autorizacion='".$fecha_actual_mysql."'
			 		 	 WHERE folio_num_reparacion=".$num_folio_reparacion) or die(mysql_error());
			
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
		
		else
		{
			// SE REALIZA QUERY QUE OBTIENE EL TOTAL DE SERVICIOS QUE SE BRINDAN
			$consulta_servicios_laboratorio = mysql_query("SELECT COUNT(id_servicio) 
														   FROM servicios") or die(mysql_error());

			// SE ALMACENA EL RESULTADO EN UNA VARIABLE EN FORMA DE ARREGLO Y POSTERIORMENTE EN UNA VARIABLE
			$row_servicios_laboratorio = mysql_fetch_array($consulta_servicios_laboratorio);
			$count = $row_servicios_laboratorio['COUNT(id_servicio)'];

			if( $count > 0 )
			{
				$detalles_servicios = mysql_query('SELECT id_servicio 
												   FROM servicios 
												   ORDER BY id_servicio') or die(mysql_error());
				$i = 0;
				while( $row_detalles_servicios = mysql_fetch_array($detalles_servicios) )
				{
					$id_servicio[$i] = $row_detalles_servicios['id_servicio'];
					$i++;			
				}
				for( $j = 0; $j < $count; $j++ )
				{
					if( isset($_POST['descripcion_'.$id_servicio[$j]]) )
					{
						mysql_query("INSERT INTO servicios_reparaciones(folio_num_reparacion, id_servicio)
									 VALUES('".$num_folio_reparacion."','".$id_servicio[$j]."')") or die(mysql_error());
					}
				}
			}

			mysql_query("UPDATE reparaciones SET id_estatus_reparaciones = 4
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
			
			mysql_query("INSERT INTO reparaciones_laboratorio(folio_num_reparacion, presupuesto, tiempo_estimado_reparacion, observaciones, costo)
						 VALUES('".$num_folio_reparacion."','".$presupuesto."','".$tiempo_estimado."','".$observaciones."','".$presupuesto."')") or die(mysql_error());
			
			/* Se guarda el movimiento */				
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','4','".$id_empleado."')") or die(mysql_error());
			
			if( $presupuesto <= 500 )
			{
				mysql_query("UPDATE reparaciones SET id_estatus_reparaciones = 7
							 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
				
				/* Se guarda el movimiento */				
				mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones,id_empleado)
							 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','7','".$id_empleado."')") or die(mysql_error());
				
				mysql_query("UPDATE reparaciones_laboratorio SET autorizo_presupuesto='Si', quien_autorizo='Automatico',fecha_autorizacion='".$fecha_actual_mysql."'
						 	 WHERE folio_num_reparacion=".$num_folio_reparacion) or die(mysql_error());
			}
			
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
	}
	if($_POST["autorizacion"] == "Autorizacion")
	{
		if($autorizo == "Si")
		{
			mysql_query("UPDATE reparaciones 
						 SET id_estatus_reparaciones ='7'
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
			
			mysql_query("UPDATE reparaciones_laboratorio 
						 SET autorizo_presupuesto='".$autorizo."', quien_autorizo='".$quien."', fecha_autorizacion='".$fecha_autorizacion_mysql."', observaciones = '".$observaciones."'
						 WHERE folio_num_reparacion='".$num_folio_reparacion."'") or die(mysql_error());
			
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','7','".$id_empleado."')") or die(mysql_error());

			// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE NECESITA LA REPARACION
			$query_refacciones_reparacion = "SELECT codigo, cantidad, referencia_codigo, precio_compra, id_categoria,inventario_maximo
										  	 FROM vales_reparacion, base_productos
											 WHERE vales_reparacion.codigo = id_base_producto
											 AND folio_num_reparacion = '$num_folio_reparacion'";

			// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
			$resultado_refacciones_reparacion = mysql_query($query_refacciones_reparacion) or die(mysql_error());

			// SE REALIZA UN CICLO PARA HACER LAS OPERACIONES POSTERIORES
			while ( $row_refacciones_reparaciones = mysql_fetch_array($resultado_refacciones_reparacion) )
			{
				$cantidad = $row_refacciones_reparaciones['cantidad'];
				$codigo_refaccion_reparacion = $row_refacciones_reparaciones['codigo'];
				$referencia_codigo = $row_refacciones_reparaciones['referencia_codigo'];
				$precio_compra_refaccion_reparacion = $row_refacciones_reparaciones['precio_compra'];
				$id_categoria_refaccion_reparacion = $row_refacciones_reparaciones['id_categoria'];
				$inventario_maximo = $row_refacciones_reparaciones['inventario_maximo'];

				// SE REALIZA QUERY QUE OBTIENE EL NUMERO DE REFACCIONES EN EXISTENCIA
				$query_inventario_refaccion = "SELECT SUM(num_serie_cantidad) AS total
											   FROM inventarios
											   WHERE id_articulo = '$codigo_refaccion_reparacion'
											   AND id_almacen = 1";

				// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
				$resultado_inventario_refaccion = mysql_query($query_inventario_refaccion) or die(mysql_error());
				$row_inventario_refaccion = mysql_fetch_array($resultado_inventario_refaccion);
				$total_en_inventario = $row_inventario_refaccion['total'];

				if ( $total_en_inventario == 0 || $total_en_inventario == "" || is_null($total_en_inventario) )
				{
					// SE REALIZA QUERY QUE OBTIENE EL NUMERO DE FOLIO MAXIMO PARA REALIZAR LA ORDEN DE COMPRA AL PROVEEDOR
					$query_folio_orden_compra = "SELECT MAX(folio_orden_compra) + 1 AS folio_orden_compra
												 FROM ordenes_de_compra";

					// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
					$resultado_folio_orden_compra = mysql_query($query_folio_orden_compra) or die(mysql_error());
					$row_folio_orden_compra = mysql_fetch_array($resultado_folio_orden_compra);
					$folio_orden_compra = $row_folio_orden_compra['folio_orden_compra'];

					// SE REALIZA QUERY QUE OBTIENE EL ID DEL PROVEEDOR DE PENDIENDO DE LA REFACCION A PEDIR
					$query_id_proveedor = "SELECT id_proveedor
										   FROM refacciones
										   WHERE codigo = '$referencia_codigo'";

					// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
					$resultado_id_proveedor = mysql_query($query_id_proveedor) or die(mysql_error());
					$row_id_proveedor = mysql_fetch_array($resultado_id_proveedor);
					$id_proveedor = $row_id_proveedor['id_proveedor'];
					$fecha_hoy = date('d-m-Y');

					// SE REALIZA EL PEDIDO AL PROVEEDOR CUANDO NO EXISTEN REFACCIONES EN INVENTARIO
					$query_insert_pedido = "INSERT INTO ordenes_de_compra(folio_orden_compra,fecha_orden,id_proveedor,id_estatus,id_categoria_producto,id_producto,cantidad,precio_unitario)
											VALUES('$folio_orden_compra','$fecha_hoy','$id_proveedor','2','$id_categoria_refaccion_reparacion','$codigo_refaccion_reparacion','$inventario_maximo','$precio_compra_refaccion_reparacion')";

					// SE EJECUTA EL QUERY PARA REALIZAR EL PEDIDO
					mysql_query($query_insert_pedido) or die(mysql_error());
				}
			}
			
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);	
		}
		else
		{
			mysql_query("UPDATE reparaciones 
						 SET id_estatus_reparaciones ='12', no_reparado='".$no_reparado."'
						 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
			
			mysql_query("UPDATE reparaciones_laboratorio 
						 SET autorizo_presupuesto = 'No', observaciones = '".$observaciones."',quien_autorizo='".$quien."',fecha_autorizacion='".$fecha_autorizacion_mysql."',
						 fecha_salida='".$fecha_actual_mysql."', hora_ultima_modificacion='".$hora."', reparado_por=".$id_empleado."
						 WHERE folio_num_reparacion='".$num_folio_reparacion."'") or die(mysql_error());
			
			/* Se guarda el movimiento */
			mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,id_estatus_reparaciones, id_empleado)
						 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','12','".$id_empleado."')") or die(mysql_error());												
			
			header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
		}
	}
	if($_POST["aceptar"] == "Aceptar"){
		/* Se actualiza la tabla de reparaciones laboratorio con la nueva informacion */
		mysql_query("UPDATE reparaciones_laboratorio SET fecha_entrega='".$fecha_entrega_mysql."',
					 entrego='".$id_empleado."', recibio='".$recibio."'
					 WHERE folio_num_reparacion='".$num_folio_reparacion."'") or die(mysql_error());
		mysql_query("UPDATE reparaciones SET id_estatus_reparaciones ='3'
										WHERE folio_num_reparacion = ".$num_folio_reparacion)
										or die(mysql_error());
		/* Se guarda el movimiento */
		mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora,
													 id_estatus_reparaciones, id_empleado)
											VALUES('".$num_folio_reparacion."','3','".$fecha_actual_mysql."','".$hora."','3',
													'".$id_empleado."')") or die(mysql_error());			
		header('Location: lista_reparaciones.php');
	}

	/* Si accion es igual a Agregar Refaccion, entonces has lo siguiente */
	if( $_POST["agregar_refaccion"] == "Agregar Refaccion")
	{
		// SE REALIZA QUERY QUE OBTIENE EL PRECIO DE VENTA DE LA REFACCION AGREGADA
		$cosnulta_costo_unitario = mysql_query("SELECT precio_venta
												FROM base_productos 
												WHERE id_base_producto = ".$descripcion) or die(mysql_error());

		// SE OBTIENE EL RESULTADO, EL CUAL SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
		$row_costo_unitario = mysql_fetch_array($cosnulta_costo_unitario);
		$costo_unitario = $row_costo_unitario["precio_venta"];

		// SE REALIZA UN QUERY DE TIPO INSERT PARA ALMACENAR LA REFACCION AGREGADA
		mysql_query("INSERT INTO vales_reparacion(folio_num_reparacion, codigo, cantidad, costo_unitario)
					 VALUES('".$num_folio_reparacion."','".$descripcion."','".$cantidad."','".$costo_unitario."')") or die(mysql_error());
		
		// SE REDIRECCIONA A LA PAGINA DE DETALLES REPARACIONES PARA VER LA LISTA DE REFACCIONES QUE SE HAN IDO AGREGANDO
		header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
	}
	if($accion == "Guardar Reparacion")
	{
		// SE REALIZA QUERY TIPO UPDATE EN LA TABLA DE REPARACIONES LABORATORIO
		mysql_query("UPDATE reparaciones_laboratorio SET
					 fecha_salida='".$fecha_reparacion_mysql."',
					 reparado_por='".$responsable_reparacion."',
					 fecha_ultima_modificacion='".$fecha_reparacion_mysql."',
					 hora_ultima_modificacion='".$hora."',
					 observaciones='".$observaciones."'
					 WHERE folio_num_reparacion='".$num_folio_reparacion."'") or die(mysql_error());

		// SE REALIZA QUERY TIPO UPDATE PARA ACTUALIZAR EL ESTATUS DE LA REPARACION
		mysql_query("UPDATE reparaciones SET id_estatus_reparaciones = '6'
					 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());
		
		/* Se guarda el movimiento */				
		mysql_query("INSERT INTO movimientos(folio_num_reparacion, id_estado_movimiento, fecha, hora, id_estatus_reparaciones, id_empleado)
					 VALUES('".$num_folio_reparacion."','1','".$fecha_actual_mysql."','".$hora."','6', '".$id_empleado."')") or die(mysql_error());
		
		// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES A NECESITAR PARA LA REPARACION
		$query_refacciones = "SELECT codigo,cantidad
							  FROM vales_reparacion
							  WHERE folio_num_reparacion = '$num_folio_reparacion'";

		// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
		$resultado_refacciones = mysql_query($query_refacciones) or die(mysql_error());

		// SE REALIZA UN CICLO PARA REALIZAR LAS OPERACIONES NECESARIAS
		while ( $row_refaccion = mysql_fetch_array($resultado_refacciones) )
		{
			$codigo_refaccion = $row_refaccion['codigo'];
			$cantidad_refaccion = $row_refaccion['cantidad'];

			// SE REALIZA QUERY QUE OBTIENE EL NUMERO MAXIMO DE ID REGISTRO DE LA TABLA DE INVENTARIOS
			$query_max_registro = "SELECT MAX(id_registro) AS id_registro
								   FROM inventarios
								   WHERE id_articulo = '$codigo_refaccion'
								   AND id_almacen = '1'
								   AND num_serie_cantidad <> 0
								   AND num_serie_cantidad >= $cantidad_refaccion";

			// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO CORRESPONDIENTE
			$resultado_max_registro = mysql_query($query_max_registro) or die(mysql_error());
			$row_max_registro = mysql_fetch_array($resultado_max_registro);
			$max_registro = $row_max_registro['id_registro'];

			// SE REALIZA QUERY UPDATE PARA ACTUALIZAR EL INVENTARIO DE CADA UNA DE LAS REFACCIONES A UTILIZAR
			$query_update_inventario = "UPDATE inventarios
										SET num_serie_cantidad = num_serie_cantidad - $cantidad_refaccion
										WHERE id_registro = '$max_registro'";

			// SE EJECUTA EL QUERY PARA DESCONTAR DEL INVENTARIO LA REFACCION
			mysql_query($query_update_inventario) or die(mysql_error());
		}

		// SE REDIRECCIONA A LA PAGINA ANTERIOR PARA VER LOS CAMBIOS REALIZADOS
		header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
	}
	if ($_POST['observaciones_reparado'] == "Guardar") 
	{
		mysql_query("UPDATE reparaciones_laboratorio 
					 SET fecha_ultima_modificacion='".$fecha_reparacion_mysql."',
					 hora_ultima_modificacion='".$hora."',
					 observaciones='".$observaciones."'
					 WHERE folio_num_reparacion='".$num_folio_reparacion."'") or die(mysql_error());

		header('Location: detalles_reparaciones2.php?folio='.$num_folio_reparacion);
	}
?>