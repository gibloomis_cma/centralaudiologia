<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBEN LAS VARIABLES POR METODO GET
	$fecha_inicio_mysql = $_GET['fecha_inicio'];
	$fecha_final_mysql = $_GET['fecha_final'];
	$id_categoria = $_GET['id_categoria'];
	$fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio_normal = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	$fecha_final_separada = explode("-", $fecha_final_mysql);
	$fecha_final_normal = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Consulta de Entradas a Almacen General </title>
	<!--<link rel="stylesheet" href="../css/style3.css" type="text/css">-->
	<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
	<script src="../js/jquery-1.7.1.js"></script>
	<script src="../js/ui/jquery.ui.core.js"></script>
	<script src="../js/ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../css/themes/base/demos.css">
	<script type="text/javascript" language="javascript">
        function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  d + '/' + m + '/' + y + ' --- ' + h + ':' + min + ':' + sec;
            window.print();
        }
    </script>
</head>
<body onload="hora()">
	<table style="width:1000px;">
        <tr>
            <td colspan="6" style="text-align:center; color:#000; font-size:24px; font-weight:bold;"> 
            <?php 
                if ( $id_categoria == 1 )
                {
                ?>
                    <label> <?php echo "REPORTE DE REFACCIONES QUE ENTRAN"; ?> </label>
                <?php
                }
                elseif( $id_categoria == 4 )
                {
                ?>
                    <label> <?php echo "REPORTE DE BATERIAS QUE ENTRAN"; ?> </label>
                <?php
                }
                else
                {
                ?>
                    <label> <?php echo "REPORTE DE AUXILIARES QUE ENTRAN"; ?> </label>
                <?php
                }
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center; font-weight:bold; font-size:18px; color:#000;"> <label> DEL: <?php echo $fecha_inicio_normal; ?> &nbsp;&nbsp; AL: <?php echo $fecha_final_normal; ?> </label> </td>
        </tr>
    </table>
    <table style="width:1000px;" border="1">
        <tr>
         	<th style="font-size:14px;">Fecha de Entrada</th>
            <th style="font-size:14px;">C&oacute;digo</th>
            <th style="font-size:14px;">Concepto</th>
            <th style="font-size:14px;">Cantidad</th>
            <th style="font-size:14px;"> Observaciones </th>
            <th style="font-size:14px;"> Costo Total </th>
        </tr>
   						<?php 
							$buscar_subcategorias = mysql_query("SELECT id_subcategoria FROM subcategorias_productos 
													 			 WHERE id_categoria = ".$id_categoria) or die(mysql_error());
	
							$n_entradas = 0;
							$costo_total = 0;
							$total_entro = 0;
							$total_piezas = 0;
							while($row_subcategorias = mysql_fetch_array($buscar_subcategorias))
							{
								$id_subcategorias = $row_subcategorias["id_subcategoria"];	
								$consulta_folios_venta = mysql_query("SELECT entrada_almacen_general.id_registro AS id_registro, entrada_almacen_general.num_serie_cantidad AS num_serie_cantidad, entrada_almacen_general.fecha_entrada AS fecha_entrada, entrada_almacen_general.hora AS hora, entrada_almacen_general.id_articulo AS id_articulo, entrada_almacen_general.id_subcategoria AS id_subcategoria, observaciones, referencia_codigo,precio_compra
														  			  FROM entrada_almacen_general, inventarios,base_productos
														  			  WHERE entrada_almacen_general.fecha_entrada BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
														  			  AND entrada_almacen_general.id_almacen = '1'
														  			  AND entrada_almacen_general.id_subcategoria = '$id_subcategorias'
														  			  AND entrada_almacen_general.id_subcategoria = inventarios.id_subcategoria
														  			  AND entrada_almacen_general.id_almacen = inventarios.id_almacen
														  			  AND entrada_almacen_general.fecha_entrada = inventarios.fecha_entrada
														  			  AND entrada_almacen_general.id_articulo = inventarios.id_articulo
														  			  AND entrada_almacen_general.hora = inventarios.hora
														  			  AND entrada_almacen_general.id_articulo = id_base_producto
														  			  AND inventarios.id_articulo = id_base_producto
														  			  ORDER BY id_registro DESC") or die(mysql_error());

								while($row_folios_venta = mysql_fetch_array($consulta_folios_venta))
								{
									$num_serie_cantidad = $row_folios_venta["num_serie_cantidad"];
									$fecha_entrada = $row_folios_venta["fecha_entrada"];
									$fecha_entrada_separada = explode("-", $fecha_entrada);
									$hora = $row_folios_venta["hora"];
									$id_registro = $row_folios_venta["id_registro"];
									$id_articulo = $row_folios_venta["id_articulo"];
									$id_subcategoria = $row_folios_venta["id_subcategoria"];
									$observaciones = $row_folios_venta['observaciones'];
									$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
									$precio_compra = $row_folios_venta['precio_compra'];
									$codigo = $row_folios_venta['referencia_codigo'];
									$n_entradas++;
									$costo_total = $num_serie_cantidad * $precio_compra;
									$total_entro = $total_entro + $costo_total;
									$total_piezas = $total_piezas + $num_serie_cantidad;
									$consulta_base_producto = mysql_query("SELECT descripcion 
																		   FROM base_productos 
																		   WHERE id_base_producto=".$id_articulo) or die(mysql_error());
									$row_base_producto = mysql_fetch_array($consulta_base_producto);
									$descripcion_base_producto = $row_base_producto["descripcion"];
					
								?>
		    						<tr>
		                                <td style="font-size:12px; text-align:center;"><?php echo $fecha_entrada_normal." ".$hora; ?></td>
		                                <td style="font-size:12px; text-align:center;"> <?php echo $codigo; ?> </td>
		                                <td style="font-size:12px; text-align:center;"><?php echo $descripcion_base_producto; ?></td>
		                                <td style="font-size:12px; text-align:center;"><?php echo $num_serie_cantidad; ?></td>
		                                <td style="font-size:12px; text-align:center;"><?php echo $observaciones; ?></td>
		                                <td style="font-size:12px; text-align:center;"> <?php echo "$".number_format($costo_total,2); ?> </td>
		                            </tr>
  								<?php
							}
						}					
				if($n_entradas==0)
				{
				?>
                    <tr>
                        <td style="text-align:center;" colspan="4">
                            <label class="textos">"No hay Entradas registradas"</label>
                        </td>
                    </tr>        
				<?php		
				}
				else
				{
				?>
					<tr>
						<td colspan="6"> <br/> </td>
					</tr>
					<tr>
		                <td colspan="2" id="hora" style="text-align:center; font-weight:bold; font-size:12px; color:#b60b0b;"> Fecha y hora de impresi&oacute;n: &nbsp; </td>
		                <td style="text-align:right; font-weight:bold;"> <label class="textos" style="font-size:14px; color:#b60b0b;"> Total Piezas: </label> </td>
		                <td style="text-align:center; font-weight:bold;"> <label class="textos" style="font-size:14px; color:#b60b0b;"> <?php echo $total_piezas; ?> </label> </td>
		                <td style="text-align:right; font-weight:bold;"> <label class="textos" style="font-size:14px; color:#b60b0b;"> Total que Entro: </label> </td>
		                <td style="text-align:center; font-weight:bold;"> <label class="textos" style="font-size:14px; color:#b60b0b;"> <?php echo "$".number_format($total_entro,2); ?> </label> </td>
		            </tr>
					</table>
                        </center>
                <?php
                }
                ?>
</body>
</html>