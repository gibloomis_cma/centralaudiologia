<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
	
	// FUNCION QUE CONVIERTE EL COLOR DE HEXADECIMAL A RGB
	function html2rgb($color)
	{
    	if ($color[0] == '#')
        	$color = substr($color, 1);

    	if (strlen($color) == 6)
        	list($r, $g, $b) = array($color[0].$color[1],
                                 	 $color[2].$color[3],
                                 	 $color[4].$color[5]);
    	elseif (strlen($color) == 3)
        	list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    	else
        	return false;

    	$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
		return array($r, $g, $b);
    }

	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
	$nombre_color = $_POST['txt_nombre_color'];
	$color_en_hexadecimal = $_POST['color_picker'];
	$color_rgb = html2rgb($color_en_hexadecimal);
	
	$query_insercion = "INSERT INTO colores(color,r,g,b) VALUES('$nombre_color','$color_rgb[0]','$color_rgb[1]','$color_rgb[2]')";
	
	if( $_POST['accion'] == "Aceptar" )
	{
		if( mysql_query($query_insercion) or die (mysql_error()) )
		{
			mysql_close($link);
		?>
			<script type="text/javascript" language="javascript">
				<?php echo "alert('El color $nombre_color se registro exitosamente')"?>;
				window.location.href = 'agregar_nuevos_colores.php';
			</script>
		<?php
		}
	}
?>