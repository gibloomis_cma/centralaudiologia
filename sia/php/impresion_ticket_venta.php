<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBEN LAS VARIABLES A TRAVES DEL METODO GET ENVIADAS DESDE EL FORMULARIO
	$folio_num_venta = $_GET['folio_num_venta'];
	$sucursal = $_GET['sucursal'];
	$vendedor = $_GET['vendedor'];
	$fecha = $_GET['fecha'];
	$fecha_separada = explode("-", $fecha);
	$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];

	// QUERY QUE OBTIENE EL TOTAL DE LA VENTA DE LA TABLA DE VENTAS
	$query_total_venta = "SELECT total
						  FROM ventas
						  WHERE id_sucursal = '$sucursal'
						  AND folio_num_venta = '$folio_num_venta'
						  AND vendedor = '$vendedor'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE
	$resultado_total_venta = mysql_query($query_total_venta) or die(mysql_error());
	$row_total_venta = mysql_fetch_array($resultado_total_venta);
	$total_venta = $row_total_venta['total'];
			
	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO QUE HIZO LA VENTA
	$query_nombre_empleado = "SELECT CONCAT(nombre,' ',paterno) AS nombre_empleado
							  FROM empleados
							  WHERE id_empleado = '$vendedor'";

	// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
	$resultado_nombre_empleado = mysql_query($query_nombre_empleado) or die(mysql_error());

	$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
	$nombre_empleado = $row_nombre_empleado['nombre_empleado'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA SUCURSAL DE ACUERDO AL ID DE LA SUCURSAL OBTENIDO
	$query_sucursal = "SELECT nombre,calle,num_exterior,num_interior,colonia,codigo_postal,id_ciudad,id_estado
					   FROM sucursales
					   WHERE id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIBLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);

	$nombre_sucursal = $row_sucursal['nombre'];
	$calle_sucursal = $row_sucursal['calle'];
	$num_exterior_sucursal = $row_sucursal['num_exterior'];
	$num_interior_sucursal = $row_sucursal['num_interior'];
	$colonia_sucursal = $row_sucursal['colonia'];
	$codigo_postal_sucursal = $row_sucursal['codigo_postal'];
	$id_ciudad_sucursal = $row_sucursal['id_ciudad'];
	$id_estado_sucursal = $row_sucursal['id_estado'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL ESTADO DE ACUERDO AL OBTENIDO
	$query_estado_sucursal = "SELECT estado
							  FROM estados
							  WHERE id_estado = '$id_estado_sucursal'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
	$resultado_estado_sucursal = mysql_query($query_estado_sucursal) or die(mysql_error());
	$row_estado_sucursal = mysql_fetch_array($resultado_estado_sucursal);
	$nombre_estado_sucursal = $row_estado_sucursal['estado'];

	// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DE LA CIUDAD DE ACUERDO AL ID DEL ESTADO Y DE LA CIUDAD OBTENIDOS
	$query_ciudad_sucursal = "SELECT ciudad
							  FROM ciudades
							  WHERE id_ciudad = '$id_ciudad_sucursal'
							  AND id_estado = '$id_estado_sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_ciudad_sucursal = mysql_query($query_ciudad_sucursal) or die(mysql_error());
	$row_ciudad_sucursal = mysql_fetch_array($resultado_ciudad_sucursal);
	$nombre_ciudad_sucursal = $row_ciudad_sucursal['ciudad'];

	// QUERY QUE OBTIENE EL DESCUENTO QUE SE REALIZO EN LA VENTA
	$query_descuento = "SELECT descuento
						FROM ventas
						WHERE folio_num_venta = $folio_num_venta";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_query_descuento = mysql_query($query_descuento) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_descuento = mysql_fetch_array($resultado_query_descuento);

	// SE ALMACENA EN UNA VARIABLE EL RESULTADO FINAL
	$descuento = $row_descuento['descuento'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Ticket Venta </title>
	<style>
		body
		{
			font-size:14px;
			font-family:Arial;	
		}

		#cont
		{
			margin:0px auto;
			text-align:center;		
			padding-bottom:5px;
		}

		#impresion
		{
			text-align:center;
			width:220px;
			font-size:14px;	
			font-family:Arial;
		}

		#impresion table
		{
			margin:0px auto;
			font-family:Arial;
			font-size:12px;	
			font-size:10px;
		}

		#impresion table tr th
		{
			font-size:12px;	
			font-family:Arial;
		}

		#impresion table tr td
		{
			text-align:right;
			padding:5px 5px;
			font-size:12px;	
			font-family:Arial;
		}

		span
		{
			text-decoration:underline;
			font-size:14px;
			font-family:Arial;	
		}
	</style>
</head>
<body onload="javascript: window.print();">
	<div id="cont">
		<div id="impresion">
			JORGE LEAL Y CIA. S.A. DE C.V. <br />
			<?php echo ucwords(strtolower($calle_sucursal)); ?> <br /> 
			<?php echo "COL.".ucfirst(strtolower($colonia_sucursal)).", ".utf8_encode(ucfirst(strtolower($nombre_ciudad_sucursal)))." , ".utf8_encode(ucfirst(strtolower($nombre_estado_sucursal))); ?> <br />
			<?php echo "CP.".$codigo_postal_sucursal."  "?> TEL. (443) 312-5994 <br/> 
			RFC: JLE010622GK2 <br />
			Ticket de Venta<br />
			Folio: <?php echo $folio_num_venta; ?> <br/>
			Fecha: <?php echo $fecha_normal; ?> <br />
			Descuento: <?php echo $descuento."%" ?> <br/>
			Vendio: <?php echo $nombre_empleado; ?> <br />
			
			<table>	
				<tr>
					<td colspan="3"> ------------------------------------------------- </td>
				</tr>
		<?php
			// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA VENTA
			$venta = "SELECT id_sucursal,id_categoria,folio_num_venta,descripcion,cantidad,costo_unitario,pago_parcial
		  			  FROM descripcion_venta
		  			  WHERE folio_num_venta = '$folio_num_venta'
		  			  AND id_sucursal = '$sucursal'";

			// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
			$resultado_venta = mysql_query($venta) or die(mysql_error());

			// SE DECLARA UNA VARIABLE PARA IR HACIENDO EL TOTAL
			$x = 0;

			// SE REALIZA UN CICLO PARA MOSTRAR LA DESCRIPCION DE LA VENTA
			while( $row_descripcion_venta = mysql_fetch_array($resultado_venta) )
			{
				$descripcion = $row_descripcion_venta['descripcion'];
				$cantidad = $row_descripcion_venta['cantidad'];
				$id_categoria = $row_descripcion_venta['id_categoria'];
				$costo_unitario = $row_descripcion_venta['costo_unitario'];
				$pago_parcial = $row_descripcion_venta['pago_parcial'];
				if ( $id_categoria == 200 && $pago_parcial != "0" ) 
                {
                    $x = $x + $pago_parcial;
                } 
                else
                {
                    $x = $x + ($cantidad * $costo_unitario);
                }
		?>
				<tr>
					<?php
    					if ( $id_categoria == 200 && $pago_parcial != "0" && $costo_unitario - $pago_parcial > 0 ) 
                        {
          				?>
          					<td colspan="3" style="text-align:center; font-family:Arial;"> Pago Parcial </td>
          				<?php
                        } 
                        else
                        {
                        }
    				?> 
				</tr>
				<tr> 
    				<td style="text-align:center; font-family:Arial;"> <?php echo $cantidad ?> </td>
    				<td colspan="2" style="text-align:left; font-family:Arial;"> <?php echo ucfirst(strtolower($descripcion)); ?> </td>
    			</tr>
    			<tr>
    				<td>  </td>
    				<td style="font-family:Arial;"> <?php echo "$ ".number_format($costo_unitario,2); ?> </td>
    				<td style="font-family:Arial;"> 
    					<?php
    						if ( $id_categoria == 200 && $pago_parcial != "0" ) 
                            {
                                echo "$".number_format($pago_parcial,2); 
                            } 
                            else
                            {
                            	echo "$".number_format($cantidad * $costo_unitario,2); 
                            }
    					?> 
    				</td>
    			</tr>
		<?php
			}
		?>
				<tr>
					<td colspan="3"> ------------------------------------------------- </td>
				</tr>
				<tr>
        			<td colspan="3" style="text-align:right; font-family:Arial;"> TOTAL: <?php echo "$".number_format($x,2); ?>  </td>
    			</tr>
    			<tr>
    				<td colspan="3" style="text-align:right; font-family:Arial;"> TOTAL C. DESC: <?php echo "$".number_format($total_venta,2); ?> </td>
    			</tr>
			</table>
			<p style="font-family:Arial;"> contacto@centraldeaudiologia.com </p>
			<p style="font-family:Arial;"> 
				Las palabras pueden dañar ... <br/> 
				pero el silencio rompe el corazón.
			</p>
		</div>
			<p>&nbsp;&nbsp;</p>
	</div>
</body>
</html>