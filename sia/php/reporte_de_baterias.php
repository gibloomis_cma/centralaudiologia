<?php
	error_reporting(0);

    // SE INICIA SESION
	session_start();

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
	include("metodo_cambiar_fecha.php");
	//include("hacer_inventario_anterior.php");

	// SE ESTABLECE LA ZONA HORARIA
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Reporte de Bater&iacute;as </title>
    <link rel="stylesheet" href="../css/style3.css" type="text/css">
    <link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../css/themes/base/demos.css">
	<script>
	$(function(){
		var dates = $( "#fecha" ).datepicker({
			defaultDate: "",
			changeMonth: true,
			numberOfMonths: 1
		});
	});
	</script>
</head>
<body>
<div id="wrapp">
	<div id="contenido_columna2">
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria">
                	Baterias
            	</div>
			</div><!--Fin de fondo titulo-->
			<div class="area_contenido1">
            <br />
            <div class="titulos"> Reporte Total de Baterías </div>
            <br />
            <center>
	            <form name="form_reporte_baterias" id="form_reporte_baterias" method="post" action="reporte_de_baterias.php">
	            	<table>
	            		<tr>
	            			<td style="text-align:right;"> <label class="textos"> Seleccionar Fecha: </label> </td>
	            			<td> <input type="text" name="fecha" id="fecha" /> </td>
	            		</tr>
	            		<tr>
	            			<td colspan="2" style="text-align:center;"> <input type="submit" name="buscar" id="buscar" class="fondo_boton" value="Buscar" title="Buscar" /> </td>
	            		</tr>
	            	</table>
	            </form>
            </center>
            <?php
            	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
            	$fecha = $_POST['fecha'];
            	$fecha_separada = explode("/", $fecha);
            	$fecha_mysql = $fecha_separada[2]."-".$fecha_separada[1]."-".$fecha_separada[0];

            	// SE VALIDA SI SE HA BUSCADO POR ALGUNA FECHA EN ESPECIFICO
            	if ( isset( $_POST['buscar'] ) && $fecha != "" )
            	{
            ?>
		            <div class="contenido_proveedor">
		  				<?php
							/* Consulta el departamento del empleado que entro en el sistema */
							$consulta_departamento_empleado=mysql_query("SELECT id_departamento FROM empleados
																		 WHERE id_empleado=".$_SESSION["id_empleado_usuario"]) or die(mysql_error());
				
							$row_departamento_empleado = mysql_fetch_array($consulta_departamento_empleado);
							$id_departamento_empleado = $row_departamento_empleado["id_departamento"];
							
							/* Consulta la sucursal del departamento */
							$consulta_sucursal = mysql_query("SELECT id_sucursal FROM areas_departamentos
															  WHERE id_departamento = ".$id_departamento_empleado) or die(mysql_error());
							
							$row_sucursal=mysql_fetch_array($consulta_sucursal);
							$id_sucursal = $row_sucursal["id_sucursal"];
							
							// SE VALIDA LA SUCURSAL A LA QUE PERTENECE EL REPORTE
							if( $id_sucursal == 1 )
							{
								$id_almacen = 2;
								$almacen = "Almacen Recepcion Matriz";
							}else
							{
								$id_almacen = 4;
								$almacen = "Almacen Recepcion Ocolusen";
							}
						?>
		            	<center>
							<table>
		                    	<tr>
		                    		<th colspan="8"><?php echo $almacen; ?></th>
		                    	</tr>
		                    	<tr>
		                        	<th width="230"> Descripción </th>
		                        	<th width="90"> Inventario Anterior </th>
		                        	<th width="60"> Entradas </th>
		                            <th width="60"> Medico </th>
		                            <th width="65"> Laboratorio </th>
		                            <th width="50"> Ventas </th>
		                            <th> Almacen General </th>
		                            <th width="90"> Inventario Actual </th>
		                    	</tr>
							<?php
		 						$consulta_base_productos = mysql_query("SELECT descripcion, id_base_producto 
		 																FROM base_productos 
		 																WHERE id_subcategoria = 55") or die(mysql_error());
		      					
		      					while( $row = mysql_fetch_array($consulta_base_productos) )
		      					{
									$id_base_producto = $row["id_base_producto"];
									$descripcion_bateria = $row["descripcion"];
									$consulta_baterias_inventario_anterior = mysql_query("SELECT id_registro, id_articulo, SUM(cantidad) AS cantidad_total
		                                                                				  FROM inventario_baterias_anterior
		                                                                				  WHERE id_almacen =".$id_almacen." 
		                                                                				  AND id_articulo='".$id_base_producto."'
		                                                                				  AND fecha_inventario = '$fecha_mysql'") or die(mysql_error());
									
									$row_baterias_inventario_anterior = mysql_fetch_array($consulta_baterias_inventario_anterior);
									$id_registro_bateria_anterior = $row_baterias_inventario_anterior["id_registro"];
									$cantidad_inventario_anterior = $row_baterias_inventario_anterior["cantidad_total"];

									$consulta_baterias_inventario_actual = mysql_query("SELECT id_registro, id_articulo, SUM(num_serie_cantidad) AS cantidad_total2
		                                                                				FROM inventarios
		                                                                				WHERE id_almacen=".$id_almacen." 
		                                                                				AND id_articulo='".$id_base_producto."'
		                                                                				AND fecha_entrada <= '$fecha_mysql'") or die(mysql_error());
					
									$row_baterias_inventario_actual = mysql_fetch_array($consulta_baterias_inventario_actual);
									$id_registro = $row_baterias_inventario_actual["id_registro"];
									$cantidad_inventario_actual = $row_baterias_inventario_actual["cantidad_total2"];

									$consulta_temporal_inventario_baterias = mysql_query("SELECT codigo, cantidad, tipo
																						  FROM temporal_inventario_baterias
																						  WHERE id_almacen=".$id_almacen." 
																						  AND codigo='".$id_base_producto."'
																						  AND fecha = '$fecha_mysql'") or die(mysql_error());

									$query_entradas_almacen_general = mysql_query("SELECT SUM(num_serie_cantidad) AS total_general
																   				   FROM inventarios
																   				   WHERE id_subcategoria = 55 
																   				   AND id_articulo = '$id_base_producto' 
																   				   AND id_almacen = 1
																   				   AND fecha_entrada = '$fecha_mysql'") or die(mysql_error());
									
									$row_almacen_general = mysql_fetch_array($query_entradas_almacen_general);
									$total_general = $row_almacen_general['total_general'];

									while( $row_temporal_inventario_baterias = mysql_fetch_array($consulta_temporal_inventario_baterias) )
									{
										$codigo = $row_temporal_inventario_baterias["codigo"];
										$tipo = $row_temporal_inventario_baterias["tipo"];
										$cantidad_temporal_inventario_baterias = $row_temporal_inventario_baterias["cantidad"];
										if( $tipo == "Medico" )
										{
											$cantidad_medico = $cantidad_medico + $cantidad_temporal_inventario_baterias;
										}
										elseif( $tipo == "Laboratorio" )
										{
											$cantidad_laboratorio = $cantidad_laboratorio + $cantidad_temporal_inventario_baterias;
										}
										elseif( $tipo == "Matriz" )
										{
											$cantidad_entrada = $cantidad_entrada + $cantidad_temporal_inventario_baterias;
										}
										elseif( $tipo == "Ocolusen" )
										{
											$cantidad_entrada = $cantidad_entrada + $cantidad_temporal_inventario_baterias;
										}
										elseif( $tipo == "Ventas" )
										{
											$cantidad_salidas = $cantidad_salidas + $cantidad_temporal_inventario_baterias;
										}
									}
								?>
				                    	<tr>
				                            <td style="text-align:left; padding-top:4px;"> <?php echo $descripcion_bateria; ?> </td>
				                            <td style="text-align:center;"> <?php echo $cantidad_inventario_anterior; ?> </td>
				                            <td style="text-align:center;"> <?php echo $cantidad_entrada; ?> </td>
				                           	<td style="text-align:center;"> <?php echo $cantidad_medico; ?> </td>
				                            <td style="text-align:center;"> <?php echo $cantidad_laboratorio; ?> </td>
				                            <td style="text-align:center;"> <?php echo $cantidad_salidas; ?> </td>
				                            <td style="text-align:center;"> <?php if( $total_general != "" ){ echo $total_general; } else{ echo ""; }  ?> </td>
				                            <td style="text-align:center;"> <?php echo $cantidad_inventario_actual; ?> </td>
				                    	</tr>
								<?php
										$total_cantidad_salidas += $cantidad_salidas;
										$total_cantidad_entrada += $cantidad_entrada;
										$total_cantidad_laboratorio += $cantidad_laboratorio;
										$total_cantidad_medico += $cantidad_medico;
										$total_cantidad_inventario_anterior += $cantidad_inventario_anterior;
										$total_cantidad_inventario_actual += $cantidad_inventario_actual;
										$total_almacen_general = $total_almacen_general + $total_general;
										$cantidad_entrada = "";
										$cantidad_laboratorio = "";
										$cantidad_medico = "";
										$cantidad_salidas = "";
								}
								?>
				    	    		<tr>
				    					<td id="alright">Totales: </td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_inventario_anterior; ?></td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_entrada; ?></td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_medico; ?></td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_laboratorio; ?></td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_salidas; ?></td>
				                        <td style="text-align:center;"> <?php echo $total_almacen_general; ?> </td>
				                        <td style="text-align:center;"><?php echo $total_cantidad_inventario_actual; ?>
				    				</tr>
		                	</table>
		            		<br />
		            	</center>
		            </div><!--Fin de contenido proveedor-->
		        <?php
		    	}
		        ?>
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>