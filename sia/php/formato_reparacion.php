<?php
	// SE DECLARA LA SENTENCIA ERROR REPORTING PARA NO MOSTRAR LOS ERRORES DEL FORMULARIO
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Nota de Reparaci&oacute;n </title>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script>
	$(document).ready(function(){
		$('#imprimir').click(function(){
			$('#imprimir').hide();
			window.print(); 
			$('#imprimir').show();
		})
	});
</script>
<style>
	#contenido_formato{
		margin:0px auto;
		width:650px;
		/* height:790px; */
	}
	.contenido_copia{
		height:495px;
		width:650px;
		float:left;
	}
	.contenido_original{
		height:505px;
		width:650px;
		float:left;	
	}
	label{
		font-size:12px;
		font-family:Arial, Helvetica, sans-serif;
	}
	span{
		font-size:14px;
		font-family:Arial, Helvetica, sans-serif;
		font-weight:bold;
	}
	#header{
		height:121px;
		width:650px;
		float:left;
	}
	.datos_control{
		height:100px;
		width:95px;
		float:left;
	}
	.imagen{
		height:70px;
		float:left;
	}
	.textos_principal{
		float:left;
		text-align:center;
		width:400px;
		height:60px;
	}
	#contenido{
		height:250px;
		float:left;
		width:650px;
	}
	.textos{
		color:#6d6d6d;
		text-align:center;
		font-weight:bold;
		font-size:11px;
		font-family:Arial, Helvetica, sans-serif;
	}
	.recibimos{
		background-color:#ececec;
		border:1px solid;
		padding:3px;
	}
	.division1{
		height:65px;
		float:left;
		width:240px;
		margin-top:-30px;
		margin-left:60px;
	}
	.division2{
		height:65px;
		width:240px;
		float:left;
		margin-top:-30px;
	}
	.datos_auxiliar_datos_dejados{
		margin-left: 15px;
		margin-right: 15px;	
	}
	.datos_auxiliar_datos_dejados2{
		margin-left: 15px;
		margin-right: 15px;
	}
	#alleft{
		text-align:left !important;
	}
	#alright{
		text-align:right !important;
	}
	.contenido_problemas{
		border:solid 1px;	
	}
	.contenido_fechas{
		width:650px;
		float:left;
		height:60px;
	}
	.contenido_garantia{
		border:1px solid;
		background-color:#ececec;
		float:right;
		width:648px;	
	}
</style>
</head>
<body>
	<?php
		include("config.php");
		include("metodo_cambiar_fecha.php");
		$num_folio_reparacion=$_GET["folio_num_reparacion"];
		/* Consulta la Nota de Reparacion */
		$consulta_nota_reparacion=mysql_query("SELECT * FROM reparaciones 
												WHERE folio_num_reparacion=".$num_folio_reparacion)or die(mysql_error());
		$row_nota_reparacion=mysql_fetch_array($consulta_nota_reparacion);
		$id_cliente = $row_nota_reparacion["id_cliente"];
		$nombre = $row_nota_reparacion["nombre"];
		$paterno = $row_nota_reparacion["paterno"];
		$materno = $row_nota_reparacion["materno"];
		$telefono = $row_nota_reparacion["descripcion"];
		$calle = $row_nota_reparacion["calle"];
		$num_exterior = $row_nota_reparacion["num_exterior"];
		$num_interior = $row_nota_reparacion["num_interior"];
		$codigo_postal = $row_nota_reparacion["codigo_postal"];
		$colonia = $row_nota_reparacion["colonia"];
		$id_ciudad = $row_nota_reparacion["id_ciudad"];
		$id_estado = $row_nota_reparacion["id_estado"];
		$fecha_entrada = $row_nota_reparacion["fecha_entrada"];
		$fecha_entrada_separada = explode("-", $fecha_entrada);
		$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
		$id_modelo = $row_nota_reparacion["id_modelo"];
		$num_serie = $row_nota_reparacion["num_serie"];
		$id_estatus_reparaciones = $row_nota_reparacion["id_estatus_reparaciones"];
		$descripcion = $row_nota_reparacion["descripcion_problema"];
		$garantia_reparacion = $row_nota_reparacion['reparacion'];
		$garantia_adaptacion = $row_nota_reparacion['adaptacion'];
		$garantia_venta = $row_nota_reparacion['venta'];
		$aplica_garantia = $row_nota_reparacion['aplica_garantia'];

		/* Consulto el estado */	
		$consulta_estado = mysql_query("SELECT estado FROM estados
											WHERE id_estado=".$id_estado) or die(mysql_error());
		$row2 = mysql_fetch_array($consulta_estado);
		$estado = $row2["estado"];
		/* Consulto la ciudad */
		$consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades 
											WHERE id_ciudad=".$id_ciudad) or die(mysql_error());
		$row3 = mysql_fetch_array($consulta_ciudad);
		$ciudad = $row3["ciudad"];
		/* Consulto el modelo del auxiliar */	
		$consulta_modelo = mysql_query("SELECT descripcion, subcategoria, id_articulo, base_productos_2.id_subcategoria AS id_subcategoria
											FROM base_productos_2, subcategorias_productos_2
											WHERE id_base_producto2=".$id_modelo." AND 
											subcategorias_productos_2.id_subcategoria = base_productos_2.id_subcategoria") 
											or die(mysql_error());
		$row5 = mysql_fetch_array($consulta_modelo);
		$modelo = $row5["descripcion"];
		$marca = $row5["subcategoria"];
		$id_subcategoria_modelo = $row5["id_subcategoria"];
		$id_articulo_modelo = $row5["id_articulo"];
		
		/* Consulta tipo de Aparato */
		$consulta_tipo_aparato=mysql_query("SELECT modelo_aparato FROM tipos_modelos_aparatos, modelos_2
																WHERE id_modelo2='".$id_articulo_modelo."' AND 
																id_subcategoria='".$id_subcategoria_modelo."' AND
																modelos_2.id_tipo_modelo=tipos_modelos_aparatos.id_tipo_modelo")
																or die('Aqui el error');
		$row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato);
		$tipo_aparato=$row_tipo_aparato["modelo_aparato"];
		
		/* Consulta tabla de accesorios_problemas */
		$consulta_accesorios_problemas=mysql_query("SELECT * FROM accesorios_problemas 
															WHERE folio_num_reparacion=".$num_folio_reparacion)
															or die(mysql_error());
		$row_accesorios_problemas=mysql_fetch_array($consulta_accesorios_problemas);
		$estuche = $row_accesorios_problemas["estuche"];
		$molde = $row_accesorios_problemas["molde"];
		$pila = $row_accesorios_problemas["pila"];
		$no_se_oye = $row_accesorios_problemas["no_se_oye"];
		$portapila = $row_accesorios_problemas["portapila"];
		$circuito = $row_accesorios_problemas["circuito"];
		$se_oye_dis = $row_accesorios_problemas["se_oye_dis"];
		$microfono = $row_accesorios_problemas["microfono"];
		$tono = $row_accesorios_problemas["tono"];
		$falla = $row_accesorios_problemas["falla"];
		$receptor = $row_accesorios_problemas["receptor"];
		$caja = $row_accesorios_problemas["caja"];
		$gasta_pila = $row_accesorios_problemas["gasta_pila"];
		$control_volumen = $row_accesorios_problemas["control_volumen"];
		$contactos_pila = $row_accesorios_problemas["contactos_pila"];
		$cayo_piso = $row_accesorios_problemas["cayo_piso"];
		$switch = $row_accesorios_problemas["switch"];
		$cambio_tubo = $row_accesorios_problemas["cambio_tubo"];
		$diagnostico = $row_accesorios_problemas["diagnostico"];
		$bobina = $row_accesorios_problemas["bobina"];
		$cableado = $row_accesorios_problemas["cableado"];
		$limpieza = $row_accesorios_problemas["limpieza"];
	?>
<div id="contenido_formato">
    <div class="contenido_copia">
    	<div id="header">
        	<div class="imagen">
        		<img src="../img/logo_reparacion.PNG" style="float:left;" width="140" />
            </div>
            <div class="textos_principal">
            	<center>
        			<span>Central de Audiología</span><br />
                    <label style="font-size:9px; font-family:Arial, Helvetica, sans-serif;">Distribuidor de los audifonos mas modernos de Europa y los E.U.A.<br />
                    Mayoreo y Menudeo.</label> <br/> <b style="font-size:14px;"> REPARACI&Oacute;N </b>
                    <table width="100%">
                    	<tr>
                        	<td align="center"><label style="font-size:10px; font-weight:bold; margin-top:-5px; float:left; margin-left:30px; font-family:Arial, Helvetica, sans-serif;">Matriz</label></td>
                            <td align="center"><label style="font-size:10px; font-weight:bold; margin-top:-5px; float:right; margin-right:90px; font-family:Arial, Helvetica, sans-serif;">Sucursal</label></td>
                    </table>
           		</center>
                </div>            
                <div class="datos_control">
           		<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px;">
               		<label style="font-size:16px; font-weight:bold;">Control:</label>
              	</div>
                <center>
                    <input name="folio" type="text" value="<?php echo $num_folio_reparacion; ?>" size="9" maxlength="9" readonly="readonly" style="text-align:center;" />
               	</center>
              	<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top: 3px;">
                	<label style="font-size:16px; font-weight:bold;">Fecha:</label>
                </div>
                <center>
            		<input name="fecha_entrada" type="text" readonly="readonly" value="<?php echo $fecha_entrada_normal; ?>" size="9" maxlength="9" style="text-align:center;" />
               	</center>
    		</div>
                <div class="division1">
                <center>
                	<label style="font-size:10px; font-family:Arial, Helvetica, sans-serif;">
                    	Seb. Lerdo de Tejada 186, Centro, Morelia Mich.<br />
                        Tel. 01 (443) 312-5984, 312-5994.<br />
                        Lada sin costo 01 800 509 1499 <br />
                        www.centraldeaudiologia.com                     
                	</label>
                </center>
                </div>
                <div class="division2">
                <center>
                	<label style="font-size:10px; font-family:Arial;">
                    	Fray Antonio de San Miguel 1400, <br />
                        Col Floresta Ocolusen, Morelia Mich.<br />
                       	Tel. 01 (443) 147-2012<br />
                        E-mail: contacto@centraldeaudiologia.com
                	</label>
               	</center>
                </div>
        </div>
    	<div id="contenido">
        	<div class="recibimos">
				<label class="textos" style="margin-left:15px;"> Recibimos de: </label><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label class="textos" style="margin-left:125px;"> Tel. </label>
<?php 
	if($id_cliente == 0){
		echo "<label style='font-size:11px; font-family:Arial, Helvetica, sans-serif;'>".$telefono."</label>";	
	}else{
		/* Si el cliente esta registrado, consulta el telefono de la tabla contactos clientes */
		$consulta_telefono = mysql_query("SELECT descripcion FROM contactos_clientes 
																WHERE id_cliente =".$id_cliente)or die(mysql_error());
		$row4 = mysql_fetch_array($consulta_telefono);
		$telefono_cliente = $row4["descripcion"];
		echo "<label style='font-size:11px; font-family:Arial, Helvetica, sans-serif;'>".$telefono_cliente."</label>";										
	}
?>
			<br />
				<label class="textos" style="margin-left:15px;"> Direcci&oacute;n: </label><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo ucwords(strtolower($calle))." #".$num_exterior." ".$num_interior." Col. ".$colonia." Cp.".$codigo_postal;?></label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<label class="textos" style="margin-left:80px;"> Localidad: </label><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo utf8_encode(ucwords(strtolower($ciudad)).", ".ucwords(strtolower($estado))); ?></label>
         	</div>
            <div class="datos_auxiliar_datos_dejados">
            <table width="100%">
            	<tr>
            		<td width="50" id="alright"><label class="textos">Estilo: </label></td>
            		<td> <label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $tipo_aparato; ?></label></td>
                    <td id="alright" width="70"><label class="textos">Marca: </label></td>
                    <td> <label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $marca; ?></label></td>
                    <td id="alright"><label class="textos">Modelo: </label></td>
                    <td><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $modelo; ?></label></td>
                    <td id="alright"> <label class="textos">Num. Serie: </label></td>
                    <td><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $num_serie; ?></label></td>
                </tr>
                <tr>
                	<td width="50"><label class="textos">Accesorios: </label></td>
                    <td id="alright" width="70">
               	<?php 
					if($estuche==1){
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <input name='estuche' type='checkbox' class='checkbox' checked='checked' disabled='disabled' /></td>";
					}else{
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <input name='estuche' type='checkbox' class='checkbox' disabled='disabled' /></td>";
	
					}
				?> 
                	<td id="alright">
                <?php 
					if($molde==1){
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <img width='14' height='14' src='../img/accept.png' /> </td>";
					}else{
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <input name='molde' type='checkbox' class='checkbox' disabled='disabled' /></td>";
					}
				?>
					<td id="alright">
      	<?php 
				if($pila==1){
					echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
				}else{
					echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
					//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <input name='pila' type='checkbox' class='checkbox' disabled='disabled' /></td>";
				}
		?>  	
        		</tr>
          	</table> 
            </div>
            <div class="contenido_problemas">
            <table width="100%">
				<tr>
                    <td id="alright" width="145"><label class="textos">No se oye nada</label></td>
                    <td id="alleft" width="20">
                    <?php if($no_se_oye == 1){	?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="no_se_oye" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="no_se_oye" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright" width="145"><label class="textos">Portapila</label></td>
                    <td id="alleft" width="20">
                    <?php if($portapila == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="portapila" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="portapila" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright" width="145"><label class="textos">Circuito</label></td>
                    <td id="alleft">
                    <?php if($circuito == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!-- <input name="circuito" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                   	<?php }else{ ?>
                   		<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="circuito" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Se oye distorcionado</label></td>
                    <td id="alleft">
                    <?php if($se_oye_dis == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="se_oye_dis" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="se_oye_dis" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Micrófono</label></td>
                    <td id="alleft">
                    <?php if($microfono ==1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="microfono" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                   		<!--<input name="microfono" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>      
                    </td>
                    <td id="alright"><label class="textos">Tono</label></td>
                    <td id="alleft">
                    <?php if($tono == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="tono" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="tono" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?> 
                    </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Falla de vez en cuando</label></td>
                    <td id="alleft">
                    <?php if($falla == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="falla" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="falla" class="checkbox" disabled="disabled"/>-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Receptor</label></td>
                    <td id="alleft">
                    <?php if($receptor == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="receptor" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="receptor" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Caja</label></td>
                    <td id="alleft">
                    <?php if($caja == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="caja" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                     <?php }else{ ?>
                     	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                     	<!--<input name="caja" type="checkbox" class="checkbox" disabled="disabled" />-->
                     <?php } ?>
                     </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Gasta mucha pila</label></td>
                    <td id="alleft">
                    <?php if($gasta_pila == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                   		<!--<input type="checkbox" name="gasta_pila" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="gasta_pila" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Control de Volumen</label></td>
                    <td id="alleft">
                    <?php if($control_volumen == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="control_volumen" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="control_volumen" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Contactos de pila</label></td>
                    <td id="alleft">
                    <?php if($contactos_pila == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="contactos_pila" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="contactos_pila" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                	</td>
                </tr><tr>
                    <td id="alright"><label class="textos">Cayo al piso</label></td>
                    <td id="alleft">
                    <?php if($cayo_piso == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="cayo_piso" class="checkbox" checked="checked" disabled="disabled" /></td>-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="cayo_piso" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    <td id="alright"><label class="textos">Switch</label></td>
                    <td id="alleft">
                    <?php if($switch ==1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="switch" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="switch" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php }?>
                    </td>
                    <td id="alright"><label class="textos">Cambio de tubo al molde</label></td>
                    <td id="alleft">
                    <?php if($cambio_tubo == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="cambio_tubo" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="cambio_tubo" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td id="alright"><label class="textos">Diagnostico</label></td>
                    <td id="alleft">
                    <?php if($diagnostico == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="diagnostico" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="diagnostico" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Bobina Telefonica</label></td>
                    <td id="alleft">
                    <?php if($bobina == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="bobina" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="bobina" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Cableado</label></td>
                    <td id="alleft">
                    <?php if($cableado == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="cableado" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="cableado" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td id="alright"><label class="textos">Limpieza</label></td>
                    <td id="alleft">
                    <?php if($limpieza == 1){ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="limpieza" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img style="margin-left:5px;" width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="limpieza" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Observaciones: </label> </td>
                    <td colspan="3"><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $descripcion; ?></label></td>
                </tr>
            </table>
            </div>
            <div class="contenido_fechas">
                <div style="float:left; width:480px; height:57px;">
                <table width="100%">
                    <tr>
                        <td width="90" id="alright"><label class="textos">Presupuesto: </label></td>
                        <td style="border-bottom:1px solid #000;">  </td>
                        <td width="50" id="alright"><label class="textos">Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;">  </td>
                        <td width="50" id="alright"><label class="textos">Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                    <tr>
                        <td width="90" id="alright"><label class="textos">Reparo: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos">Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos">Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                    <tr>
                        <td width="90" id="alright"><label class="textos">Entrego: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos">Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;">  </td>
                        <td width="50" id="alright"><label class="textos">Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                </table>
                </div>
                <div style="height:57px; width:100px; float:left; margin-left:12px; margin-right:8px; padding-top:4px;">
                <table width="100%" border="1" style="border-collapse:collapse;">
                    <tr>
                        <td align="center"><label class="textos" style="font-size:14px;">Total: </label></td>
                    </tr>
                    <tr>
                        <td align="center" style="height:30px;">
                        	<?php
                        		if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                        		{
                        			echo "$ 0.00";
                        		}
                        		else
                        		{
                        			echo $cobro_total;
                        		}
                        	?>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div class="contenido_garantia">
            <center><label style="font-size:8px; font-family:Arial, Helvetica, sans-serif;">NOTAS IMPORTANTES:<br />
            CONSERVE ESTE RECIBO YA QUE ES EL COMPROBANTE PARA RECLAMAR SU APARATO, SI LO EXTRAVIA, 
            NO NOS HACEMOS RESPONSABLES.<br />
			LA GARANTIA AMPARA UNICAMENTE PIEZAS CAMBIADAS O SERVICIOS REALIZADOS DURANTE 30 DIAS.<br />
			NO NOS HACEMOS RESPONSABLES POR TRABAJOS DESPUES DE 30 DIAS.</label>
            </center>
            </div>
        </div>
    </div><!-- Fin de contenido copia -->
    <div class="contenido_original">
    	<div style="margin-top:-30px; width:100%; height:5px; border-bottom:3px dashed #000;"></div>
    	<div id="header">
        	<div class="imagen">
        		<img src="../img/logo_reparacion.PNG" style="float:left;" width="140" />
            </div>
           <div class="textos_principal">
            	<center>
        			<span>Central de Audiología</span><br />
                    <label style="font-size:9px; font-family:Arial, Helvetica, sans-serif;">Distribuidor de los audifonos mas modernos de Europa y los E.U.A.<br />
                    Mayoreo y Menudeo.</label> <br/> <b style="font-size:14px;"> REPARACI&Oacute;N </b>
                    <table width="100%">
                    	<tr>
                        	<td align="center"><label style="font-size:10px; font-weight:bold; margin-top:-5px; float:left; margin-left:30px; font-family:Arial, Helvetica, sans-serif;">Matriz</label></td>
                            <td align="center"><label style="font-size:10px; font-weight:bold; margin-top:-5px; float:right; margin-right:90px; font-family:Arial, Helvetica, sans-serif;">Sucursal</label></td>
                    </table>
           		</center>
                </div>            
                <div class="datos_control">
           		<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px;">
               		<label style="font-size:16px; font-weight:bold;">Control:</label>
              	</div>
                <center>
                    <input name="folio" type="text" value="<?php echo $num_folio_reparacion; ?>" size="9" maxlength="9" readonly="readonly" style="text-align:center;" />
               	</center>
              	<div style="border:1px solid; width:auto; text-align:center; background-color:#ececec; margin-bottom:3px; margin-top: 3px;">
                	<label style="font-size:16px; font-weight:bold;">Fecha:</label>
                </div>
                <center>
            		<input name="fecha_entrada" type="text" readonly="readonly" value="<?php echo $fecha_entrada_normal; ?>" size="9" maxlength="9" style="text-align:center;" />
               	</center>
    		</div>
                <div class="division1">
                <center>
                	<label style="font-size:10px; font-family:Arial, Helvetica, sans-serif;">
                    	Seb. Lerdo de Tejada 186, Centro, Morelia Mich.<br />
                        Tel. 01 (443) 312-5984, 312-5994.<br />
                        Lada sin costo 01 800 509 1499 <br />
                        www.centraldeaudiologia.com                     
                	</label>
                </center>
                </div>
                <div class="division2">
                <center>
                	<label style="font-size:10px; font-family:Arial;">
                    	Fray Antonio de San Miguel 1400, <br />
                        Col Floresta Ocolusen, Morelia Mich.<br />
                       	Tel. 01 (443) 147-2012<br />
                        E-mail: contacto@centraldeaudiologia.com
                	</label>
               	</center>
                </div>
        </div>
    	<div id="contenido">
        	<div class="recibimos">
				<label class="textos" style="margin-left:15px;"> Recibimos de: </label>
                <label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo ucwords(strtolower($nombre." ".$paterno." ".$materno)); ?> </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label class="textos" style="margin-left:125px;"> Tel. </label>
<?php 
	if($id_cliente == 0){
		echo "<label style='font-size:11px; font-family:Arial, Helvetica, sans-serif;'>".$telefono."</label>";	
	}else{
		/* Si el cliente esta registrado, consulta el telefono de la tabla contactos clientes */
		$consulta_telefono = mysql_query("SELECT descripcion FROM contactos_clientes 
																WHERE id_cliente =".$id_cliente)or die(mysql_error());
		$row4 = mysql_fetch_array($consulta_telefono);
		$telefono_cliente = $row4["descripcion"];
		echo "<label style='font-size:11px; font-family:Arial, Helvetica, sans-serif;'>".$telefono_cliente."</label>";										
	}
?>
			<br />
				<label class="textos" style="margin-left:15px;"> Direcci&oacute;n: </label><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo ucwords(strtolower($calle))." #".$num_exterior." ".$num_interior." Col. ".$colonia." Cp.".$codigo_postal;?></label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<label class="textos" style="margin-left:80px;"> Localidad: </label><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo utf8_encode(ucwords(strtolower($ciudad)).", ".ucwords(strtolower($estado))); ?></label>
         	</div>
            <div class="datos_auxiliar_datos_dejados2">
            	 <table width="100%">
            	<tr>
            		<td width="50" id="alright"><label class="textos">Estilo: </label></td>
            		<td> <label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $tipo_aparato; ?></label></td>
                    <td id="alright" width="70"><label class="textos">Marca: </label></td>
                    <td> <label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $marca; ?></label></td>
                    <td id="alright"><label class="textos">Modelo: </label></td>
                    <td><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $modelo; ?></label></td>
                    <td id="alright"> <label class="textos">Num. Serie: </label></td>
                    <td><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $num_serie; ?></label></td>
                </tr>
                <tr>
                	<td width="50"><label class="textos">Accesorios: </label></td>
                    <td id="alright" width="70">
               	<?php 
					if($estuche==1){
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <input name='estuche' type='checkbox' class='checkbox' checked='checked' disabled='disabled' /></td>";
					}else{
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label></td><td> <input name='estuche' type='checkbox' class='checkbox' disabled='disabled' /></td>";
	
					}
				?> 
                	<td id="alright">
                <?php 
					if($molde==1){
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <input name='molde' type='checkbox' class='checkbox' checked='checked' disabled='disabled' /></td>";
					}else{
						echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
						//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label></td><td> <input name='molde' type='checkbox' class='checkbox' disabled='disabled' /></td>";
					}
				?>
					<td id="alright">
      	<?php 
				if($pila==1){
					echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' /> </td>";
					//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <input name='pila' type='checkbox' class='checkbox' checked='checked' disabled='disabled' /></td>";
				}else{
					echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' /> </td>";
					//echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label></td><td> <input name='pila' type='checkbox' class='checkbox' disabled='disabled' /></td>";
				}
		?>  	
        		</tr>
          	</table>
            </div>
            <div class="contenido_problemas">
            <table width="100%">
				<tr>
                    <td id="alright" width="145"><label class="textos">No se oye nada</label></td>
                    <td id="alleft" width="20">
                    <?php if($no_se_oye == 1){	?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="no_se_oye" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="no_se_oye" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright" width="145"><label class="textos">Portapila</label></td>
                    <td id="alleft" width="20">
                    <?php if($portapila == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="portapila" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="portapila" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright" width="145"><label class="textos">Circuito</label></td>
                    <td id="alleft">
                    <?php if($circuito == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="circuito" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                   	<?php }else{ ?>
                   		<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="circuito" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Se oye distorcionado</label></td>
                    <td id="alleft">
                    <?php if($se_oye_dis == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="se_oye_dis" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="se_oye_dis" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Micrófono</label></td>
                    <td id="alleft">
                    <?php if($microfono ==1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="microfono" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                   		<!--<input name="microfono" type="checkbox" class="checkbox" disabled="disabled" />  -->
                    <?php } ?>      
                    </td>
                    <td id="alright"><label class="textos">Tono</label></td>
                    <td id="alleft">
                    <?php if($tono == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="tono" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="tono" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?> 
                    </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Falla de vez en cuando</label></td>
                    <td id="alleft">
                    <?php if($falla == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="falla" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="falla" class="checkbox" disabled="disabled"/>-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Receptor</label></td>
                    <td id="alleft">
                    <?php if($receptor == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="receptor" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="receptor" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Caja</label></td>
                    <td id="alleft">
                    <?php if($caja == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="caja" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                     <?php }else{ ?>
                     	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                     	<!--<input name="caja" type="checkbox" class="checkbox" disabled="disabled" />-->
                     <?php } ?>
                     </td>
                </tr><tr>
                    <td id="alright"><label class="textos">Gasta mucha pila</label></td>
                    <td id="alleft">
                    <?php if($gasta_pila == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                   		<!--<input type="checkbox" name="gasta_pila" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="gasta_pila" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Control de Volumen</label></td>
                    <td id="alleft">
                    <?php if($control_volumen == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="control_volumen" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="control_volumen" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Contactos de pila</label></td>
                    <td id="alleft">
                    <?php if($contactos_pila == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="contactos_pila" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="contactos_pila" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                	</td>
                </tr><tr>
                    <td id="alright"><label class="textos">Cayo al piso</label></td>
                    <td id="alleft">
                    <?php if($cayo_piso == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input type="checkbox" name="cayo_piso" class="checkbox" checked="checked" disabled="disabled" /></td>-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input type="checkbox" name="cayo_piso" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    <td id="alright"><label class="textos">Switch</label></td>
                    <td id="alleft">
                    <?php if($switch ==1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="switch" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="switch" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php }?>
                    </td>
                    <td id="alright"><label class="textos">Cambio de tubo al molde</label></td>
                    <td id="alleft">
                    <?php if($cambio_tubo == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="cambio_tubo" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="cambio_tubo" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td id="alright"><label class="textos">Diagnostico</label></td>
                    <td id="alleft">
                    <?php if($diagnostico == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="diagnostico" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="diagnostico" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Bobina Telefonica</label></td>
                    <td id="alleft">
                    <?php if($bobina == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="bobina" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="bobina" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Cableado</label></td>
                    <td id="alleft">
                    <?php if($cableado == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="cableado" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                	<?php }else{ ?>
                		<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="cableado" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td id="alright"><label class="textos">Limpieza</label></td>
                    <td id="alleft">
                    <?php if($limpieza == 1){ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
                    	<!--<input name="limpieza" type="checkbox" class="checkbox" checked="checked" disabled="disabled" />-->
                    <?php }else{ ?>
                    	<img width='14' height='14' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
                    	<!--<input name="limpieza" type="checkbox" class="checkbox" disabled="disabled" />-->
                    <?php } ?>
                    </td>
                    <td id="alright"><label class="textos">Observaciones: </label> </td>
                    <td colspan="3"><label style="font-size:11px; font-family:Arial, Helvetica, sans-serif;"><?php echo $descripcion; ?></label></td>
                </tr>
            </table>
            </div>
            <div class="contenido_fechas">
                <div style="float:left; width:480px; height:57px;">
                <table width="100%">
                    <tr>
                        <td width="90" id="alright"><label class="textos"> Presupuesto: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                    <tr>
                        <td width="90" id="alright"><label class="textos"> Reparo: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                    <tr>
                        <td width="90" id="alright"><label class="textos"> Entrego: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Fecha: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                        <td width="50" id="alright"><label class="textos"> Hora: </label></td>
                        <td style="border-bottom:1px solid #000;"> </td>
                    </tr>
                </table>
                </div>
                <div style="height:57px; width:100px; float:left; margin-left:12px; margin-right:8px; padding-top:3px;">
                <table width="100%" border="1" style="border-collapse:collapse;">
                    <tr>
                        <td align="center"><label class="textos" style="font-size:14px;">Total: </label></td>
                    </tr>
                    <tr>
                        <td align="center" style="height:30px;">
                        	<?php
                        		if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                        		{
                        			echo "$ 0.00";
                        		}
                        		else
                        		{
                        			echo $cobro_total;
                        		}
                        	?>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div class="contenido_garantia">
            <center><label style="font-size:8px; font-family:Arial, Helvetica, sans-serif;">NOTAS IMPORTANTES:<br />
            CONSERVE ESTE RECIBO YA QUE ES EL COMPROBANTE PARA RECLAMAR SU APARATO, SI LO EXTRAVIA, 
            NO NOS HACEMOS RESPONSABLES.<br />
			LA GARANTIA AMPARA UNICAMENTE PIEZAS CAMBIADAS O SERVICIOS REALIZADOS DURANTE 30 DIAS.<br />
			NO NOS HACEMOS RESPONSABLES POR TRABAJOS DESPUES DE 30 DIAS.</label>
            </center>
            </div>
       	<p align="right">
           	<input name="imprimir" id="imprimir" type="button" value="Imprimir" />
        </p>
        </div>           
    	</div>
    </div><!--Fin de contenido original-->
</div><!--Fin de formato contenido-->
</body>
</html>