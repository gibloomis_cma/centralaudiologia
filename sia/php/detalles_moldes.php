<?php
	session_start();
	include("config.php");
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha_actual = date("d/m/Y");
	$hora = date("h:i:s A");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalles de Moldes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">	
  	<?php 
		//include("metodo_cambiar_fecha.php");
		/* Obtengo las variables que mande del otro formulario */
		$num_folio_molde = $_GET["folio"];/* Folio de N° de Reparacion */
		/* Consulto los datos de la tabla reparaciones segun el numero de folio */
		$consulta_datos_de_num_molde = mysql_query("SELECT * FROM moldes 
																	WHERE folio_num_molde =".$num_folio_molde) 
																	or die(mysql_error());													
		$row = mysql_fetch_array($consulta_datos_de_num_molde);
		$id_cliente = $row["id_cliente"];
		$nombre = $row["nombre"];
		$paterno = $row["paterno"];
		$materno = $row["materno"];
		$telefono = $row["descripcion"];
		$calle = $row["calle"];
		$num_exterior = $row["num_exterior"];
		$num_interior = $row["num_interior"];
		$codigo_postal = $row["codigo_postal"];
		$colonia = $row["colonia"];
		$id_ciudad = $row["id_ciudad"];
		$id_estado = $row["id_estado"];
		$fecha_entrada = $row["fecha_entrada"];
		$fecha_entrada_separada = explode("-", $fecha_entrada);
		$lado_oido = $row["lado_oido"];
		$id_estilo = $row["id_estilo"];
		$id_material = $row["id_material"];
		$id_color = $row["id_color"];
		$id_ventilacion = $row["id_ventilacion"];
		$id_salida = $row["id_salida"];
		$id_estatus_molde = $row["id_estatus_moldes"];
		$costo = $row["costo"];
		$pago_parcial = $row['pago_parcial'];
		$adeudo = $costo - $pago_parcial;
		$observaciones = $row["observaciones"];
		$adaptacion = $row['adaptacion'];
		$reposicion = $row['reposicion'];
		/* Consulto el estado */	
		$consulta_estado = mysql_query("SELECT estado FROM estados 
											WHERE id_estado=".$id_estado) or die(mysql_error());
		$row2 = mysql_fetch_array($consulta_estado);
		$estado = $row2["estado"];
		/* Consulto la ciudad */
		$consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades 
										WHERE id_ciudad=".$id_ciudad) or die(mysql_error());
		$row3 = mysql_fetch_array($consulta_ciudad);
		$ciudad = $row3["ciudad"];
		
		$consulta_id_estatus_molde=mysql_query("SELECT estado_molde FROM estatus_moldes 
												WHERE id_estatus_moldes=".$id_estatus_molde)or die(mysql_error());
		$row_estatus_molde = mysql_fetch_array($consulta_id_estatus_molde);
		$estado_molde=$row_estatus_molde["estado_molde"];
		
		$consulta_salida=mysql_query("SELECT salida FROM salidas WHERE id_salida=".$id_salida)or die(mysql_error());
		$row_salida=mysql_fetch_array($consulta_salida);
		$nombre_salida=$row_salida["salida"];
		
		$consulta_ventilacion=mysql_query("SELECT nombre_ventilacion, calibre FROM ventilaciones 
											WHERE id_ventilacion=".$id_ventilacion)or die(mysql_error());
		$row_ventilacion=mysql_fetch_array($consulta_ventilacion);
		$nombre_ventilacion=$row_ventilacion["nombre_ventilacion"];
		$calibre = $row_ventilacion["calibre"];
		
		$consulta_material_color=mysql_query("SELECT material, color FROM colores, materiales
												WHERE id_material='".$id_material."' AND id_color ='".$id_color."'")
												or die(mysql_error());
		$row_material_color = mysql_fetch_array($consulta_material_color);
		$nombre_material=$row_material_color["material"];
		$color=$row_material_color["color"];
		
		$consulta_estilo = mysql_query("SELECT estilo 
										FROM estilos,catalogo
										WHERE estilos.id_estilo = catalogo.id_estilo
										AND id_catalogo =".$id_estilo)or die(mysql_error());
		$row_estilo=mysql_fetch_array($consulta_estilo);
		$nombre_estilo=$row_estilo["estilo"];		
		
		$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
	?>
    		<br />
            	<div class="contenido_proveedor">
                <form name="forma1" id="form_detalles_moldes" method="post" action="cambiar_estatus_moldes.php">
                    <fieldset>
                    <legend>Datos Generales</legend>
                    <label class="textos">N° Nota: </label><?php echo $num_folio_molde; ?>
                    <input name="folio_molde" value="<?php echo $num_folio_molde; ?>" type="hidden" />
                    <input name="id_empleado_sistema" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de Entrada: </label><?php echo $fecha_entrada_normal; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha Actual: </label><input name="fecha_actual" type="text" value="<?php echo $fecha_actual; ?>" readonly="readonly" />
                    <input name="hora_real" type="hidden" value="<?php echo $hora; ?>" />
                <br />
                    <label class="textos">Recibimos de: </label><?php echo $nombre." ".$paterno." ".$materno; ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tel. </label>
 	<?php 
		if($id_cliente == 0){
			echo $telefono;	
		}else{
			/* Si el cliente esta registrado, consulta el telefono de la tabla contactos clientes */
			$consulta_telefono = mysql_query("SELECT descripcion FROM contactos_clientes 
																	WHERE id_cliente =".$id_cliente)or die(mysql_error());
			$row4 = mysql_fetch_array($consulta_telefono);
			$telefono_cliente = $row4["descripcion"];
			echo $telefono_cliente;										
		}
	?>
                <br /><br />
                    <label class="textos">Direccion: </label><?php echo $calle." #".$num_exterior." ".$num_interior." Col. ".$colonia." Cp.".$codigo_postal;?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Localidad: </label><?php echo utf8_encode(ucfirst(strtolower($ciudad.", ".$estado))); ?>
                    </fieldset>
                <br />
                    <fieldset>
                    <legend>Datos del Molde a Elaborar</legend>
                    <label class="textos">Estilo: </label>
                    <?php echo ucwords(strtolower($nombre_estilo)); ?>
                <br /><br />
                    <label class="textos">Material: </label>
                    <?php echo ucwords(strtolower($nombre_material)); ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Color: </label>
                    <?php echo ucwords(strtolower($color)); ?>
                <br /><br />
                    <label class="textos">Ventilación: </label>
                    <?php echo ucwords(strtolower($nombre_ventilacion)); ?>
             	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Calibre: </label>
                    <?php echo $calibre; ?>
                <br /><br />
                    <label class="textos">Salida: </label>
                    <?php echo $nombre_salida; ?>
                <br /><br />
                	<label class="textos">Lado del Oido: </label>
                    <?php echo ucwords(strtolower($lado_oido)); ?>
               	<br /><br />
                	<label class="textos">Costo Total: </label>
                	$<?php if( $adaptacion == "si" && $reposicion == "" ){ echo " 0.00"; }elseif( $reposicion == "si" && $adaptacion == "" ){ echo " 0.00"; }else{ echo number_format($costo,2); } ?>
                <br /><br />
                	<label class="textos">Pago Parcial: </label>
                	$<?php  echo number_format($pago_parcial,2);  ?>
                <br /><br />
                	<?php
                		if ( $adaptacion == "si" )
                		{
                		?>
                			<label class="textos"> Adaptaci&oacute;n: </label>
                			<?php echo ucfirst(strtolower($adaptacion)); ?>
                			<br/><br/>
                		<?php
                		}
                		if ( $reposicion == "si" ) 
                		{
                		?>
                			<label class="textos"> Reposici&oacute;n: </label>
                			<?php echo ucfirst(strtolower($reposicion)); ?>
                			<br/><br/>
                		<?php
                		}
                	?>
                    <label class="textos">Observaciones: </label>
                    <?php echo $observaciones; ?>
                <br /><br />
                    <label class="textos">Estatus del Molde: </label>
      	<?php 
				/* Si id_estatus_molde es igual a 1.-Recepcion Matriz */
				if($id_estatus_molde == 1){
		?>
                    <select name="id_estatus_moldes">
   						<option value="1" selected="selected" disabled="disabled">Recepcion Matriz</option>
                        <option value="2">Laboratorio, En fabricacion</option>
                	</select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
     	<?php 	
				}
				/* Si id_estatus_molde es igual a 4.-Recepcion Ocolusen */
				if($id_estatus_molde == 4){
		?>
                    <select name="id_estatus_moldes">
   						<option value="4" selected="selected" disabled="disabled">Recepcion Ocolusen</option>
                        <option value="2">Laboratorio, En fabricacion</option>
                	</select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
     	<?php 	
				}
				/* Si id_estatus_molde es igual a 2.-Laboratorio, En fabricacion */
				if($id_estatus_molde == 2){
		?>
                    <select name="id_estatus_moldes">
                        <option value="2" selected="selected" disabled="disabled">Laboratorio, En fabricacion</option>
                	</select>
                    </fieldset>
     	<?php 	
				}
				/* Si id_estatus_molde es igual a 8.-Laboratorio, Fabricado */
				if($id_estatus_molde == 8){
		?>
                    <select name="id_estatus_moldes">
                        <option value="8" selected="selected" disabled="disabled">Laboratorio, Fabricado</option>
       	<?php
					$consulta_movimientos_folio=mysql_query("SELECT id_registro, id_estatus_moldes 
															FROM movimientos_moldes
															WHERE folio_num_molde=".$num_folio_molde." AND
															 (id_estatus_moldes='1' OR id_estatus_moldes='4')
															  ORDER BY id_registro DESC")
															 or die(mysql_error());
					$row_movimientos_folio=mysql_fetch_array($consulta_movimientos_folio);
					$id_estatus_moldes_origen = $row_movimientos_folio["id_estatus_moldes"];
					if($id_estatus_moldes_origen == 1){
		?>
        				<option value="5">Recepcion Matriz, Elaborado</option>
        <?php
					}else{
		?>
        				<option value="6">Recepcion Ocolusen, Elaborado</option>
        <?php 		
					}
		?>
                	</select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
     	<?php 	
				}
				/* Si id_estatus_molde es igual a 5.-Recepcion Matriz, Elaborado */
				if($id_estatus_molde == 5){
		?>	
        			<select name="id_estatus_moldes">
                        <option value="5" selected="selected" disabled="disabled">Recepcion Matriz, Elaborado</option>
                        <option value="6">Recepcion Ocolusen, Elaborado</option>
                        <option value="9">Almacen</option>
                    </select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset> 
		<?php			
				}
				/* Si id_estatus_molde es igual a 6.-Recepcion Ocolusen, Elaborado */
				if($id_estatus_molde == 6){
		?>	
        			<select name="id_estatus_moldes">
                        <option value="6" selected="selected" disabled="disabled">Recepcion Ocolusen, Elaborado</option>
                        <option value="5">Recepcion Matriz, Elaborado</option>
                        <option value="9">Almacen</option>
                    </select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset> 
		<?php			
				}
				/* Si id_estatus_molde es igual a 9.-Almacen */
				if($id_estatus_molde == 9){
		?>	
        			<select name="id_estatus_moldes">
                        <option value="9" selected="selected" disabled="disabled">Almacen</option>
                        <option value="6" >Recepcion Ocolusen, Elaborado</option>
                        <option value="5">Recepcion Matriz, Elaborado</option>
                    </select>
                    <input name="cambiar_estatus" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset> 
		<?php			
				}
				/* Si id_estatus_molde es igual a 7.-Entregado */
				if($id_estatus_molde == 7){
		?>	
        			<select name="id_estatus_moldes">
                        <option value="7" selected="selected" disabled="disabled">Entregado</option>
                    </select>
                    </fieldset> 
		<?php			
				}
		?>
        
        <?php
				/* Si id_estatus_molde es igual a 2.-Laboratorio, En fabricacion */		
				if($id_estatus_molde == 2){
		?>
        			<br />
        			<fieldset>
                    	<legend>Elaboración</legend>
                        <label class="textos">Fabrico: </label>
                  	<?php			
						$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
															WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
															or die(mysql_error());
						$row14 = mysql_fetch_array($consulta_empleados);
						$nombre = $row14["nombre"];
						$paterno = $row14["paterno"];
						$materno = $row14["materno"];                 
						echo $nombre." ".$paterno." ".$materno; ?>
                  	&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="quien_elaboro" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                    	<label class="textos">Fecha de Fabricación: </label>
                        <?php echo $fecha_actual; ?>
                        <input name="fecha_fabricacion" type="hidden" value="<?php echo $fecha_actual; ?>" />
                    <br /><br />
                    	<label class="textos">Observaciones: </label><br />
                        <textarea name="observaciones" rows="2" cols="60"></textarea>
                  	<p align="right">
                    	<input name="guardar_fabricacion" type="submit" value="Guardar Fabricacion" class="fondo_boton" />
                    </p>
                    </fieldset>	
		<?php
				}
				/* Si id_estatus_molde es igual a 8.-Laboratorio, Fabricado
												  9.-Almacen
												  5.-Recepcion Matriz, Elaborado
												  6.-Recepcion Ocolusen, Elaborado
				 */
				if($id_estatus_molde == 8 or $id_estatus_molde == 9){
					$consulta_datos_molde=mysql_query("SELECT * FROM moldes_elaboracion 
														WHERE folio_num_molde=".$num_folio_molde)or die(mysql_error());
					$row_datos_molde=mysql_fetch_array($consulta_datos_molde);
					$fecha_elaboracion_molde=$row_datos_molde["fecha_salida"];
					$fecha_molde_separada = explode("-", $fecha_elaboracion_molde);
					$fecha_elaboracion_molde_normal = $fecha_molde_separada[2]."/".$fecha_molde_separada[1]."/".$fecha_molde_separada[0];
					$observaciones_elaboracion=$row_datos_molde["observaciones"];
					$id_empleado_elaboro= $row_datos_molde["elaboro"];
		?>
        			<br />
        			<fieldset>
                    	<legend>Elaboración</legend>
                        <label class="textos">Fabrico: </label>
                  	<?php			
						$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
															WHERE id_empleado=".$id_empleado_elaboro)
															or die(mysql_error());
						$row14 = mysql_fetch_array($consulta_empleados);
						$nombre = $row14["nombre"];
						$paterno = $row14["paterno"];
						$materno = $row14["materno"];                 
						echo $nombre." ".$paterno." ".$materno; ?>
                  	&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    	<label class="textos">Fecha de Fabricación: </label>
                        <?php echo $fecha_elaboracion_molde_normal; ?>
                    <br /><br />
                    	<label class="textos">Observaciones: </label>
                        <?php echo $observaciones_elaboracion; ?>
                    </fieldset>	
        <?php 	
				}
				if( $id_estatus_molde == 5 or $id_estatus_molde == 6 ) 
				{
					$consulta_datos_molde=mysql_query("SELECT * FROM moldes_elaboracion 
														WHERE folio_num_molde=".$num_folio_molde)or die(mysql_error());
					$row_datos_molde=mysql_fetch_array($consulta_datos_molde);
					$fecha_elaboracion_molde=$row_datos_molde["fecha_salida"];
					$fecha_molde_separada = explode("-", $fecha_elaboracion_molde);
					$fecha_elaboracion_molde_normal = $fecha_molde_separada[2]."/".$fecha_molde_separada[1]."/".$fecha_molde_separada[0];
					$observaciones_elaboracion=$row_datos_molde["observaciones"];
					$id_empleado_elaboro= $row_datos_molde["elaboro"];
		?>
        			<br />
        			<fieldset>
                    	<legend>Elaboración</legend>
                        <label class="textos">Fabrico: </label>
                  	<?php			
						$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
															WHERE id_empleado=".$id_empleado_elaboro)
															or die(mysql_error());
						$row14 = mysql_fetch_array($consulta_empleados);
						$nombre = $row14["nombre"];
						$paterno = $row14["paterno"];
						$materno = $row14["materno"];                 
						echo $nombre." ".$paterno." ".$materno; ?>
                  	&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    	<label class="textos">Fecha de Fabricación: </label>
                        <?php echo $fecha_elaboracion_molde_normal; ?>
                    <br /><br />
                    	<label class="textos">Observaciones: </label>
                        <?php echo $observaciones_elaboracion; ?>
                    </fieldset>	
                    <br/>
                    <?php
                    	if ( $costo == 0 || $adaptacion == "si" || $reposicion == "si" || $adeudo == 0 )
                    	{
                    	?>
                    		<fieldset>
		                    <legend>Entrega</legend>
		                    <label class="textos">Fecha de Entrega: </label>
		                    <input name="fecha_entrega" type="text" value="<?php echo $fecha_actual; ?>" readonly="readonly"/>
		                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                    <label class="textos">Entrego: </label>
				  			<?php
				            $consulta_empleado_sistema = mysql_query("SELECT nombre, paterno, materno 
																		FROM empleados 
																		WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
																		or die(mysql_error());
							$row14 = mysql_fetch_array($consulta_empleado_sistema);
							$nombre = $row14["nombre"];
							$paterno = $row14["paterno"];
							$materno = $row14["materno"];
							?>
		                    <?php echo $nombre." ".$paterno." ".$materno; ?>
		                    <input name="hora" type="hidden" value="<?php echo $hora; ?>" />
		                    <input name="quien_entrego" type="hidden" value="<?php echo $_SESSION['id_empleado_usuario']; ?>" />
		               	<br /><br />
		                    <label class="textos">Recibio: </label>
		                    <input name="recibio" id="recibio" type="text" size="40" maxlength="40" />
		             	<p align="right">
		                	<input name="aceptar" value="Aceptar" type="submit" class="fondo_boton" />
		                </p>
		                    </fieldset>
                    	<?php
                    	}
                    ?>
        <?php 
				}
				/* Si id_estatus_molde es igual a 7.-Entregado */
				if($id_estatus_molde == 7){
					$consulta_datos_molde=mysql_query("SELECT * FROM moldes_elaboracion 
														WHERE folio_num_molde=".$num_folio_molde)or die(mysql_error());
					$row_datos_molde=mysql_fetch_array($consulta_datos_molde);
					$fecha_elaboracion_molde=$row_datos_molde["fecha_salida"];
					$fecha_elaboracion_separada = explode("-", $fecha_elaboracion_molde);
					$fecha_elaboracion_molde_normal = $fecha_elaboracion_separada[2]."/".$fecha_elaboracion_separada[1]."/".$fecha_elaboracion_separada[0];
					$observaciones_elaboracion=$row_datos_molde["observaciones"];
					$id_empleado_elaboro= $row_datos_molde["elaboro"];
					$recibio = $row_datos_molde["recibio"];
					$quien_entrego = $row_datos_molde["quien_entrego"];
					$fecha_entrega = $row_datos_molde["fecha_entrega"];
					$fecha_entrega_separada = explode("-", $fecha_entrega);
					$fecha_entrega_normal = $fecha_entrega_separada[2]."/".$fecha_entrega_separada[1]."/".$fecha_entrega_separada[0];
					$pago = $row_datos_molde["cobro"];					
		?>
        			<br />
        			<fieldset>
                    	<legend>Elaboración</legend>
                        <label class="textos">Fabrico: </label>
                  	<?php			
						$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
															WHERE id_empleado=".$id_empleado_elaboro)
															or die(mysql_error());
						$row14 = mysql_fetch_array($consulta_empleados);
						$nombre = $row14["nombre"];
						$paterno = $row14["paterno"];
						$materno = $row14["materno"];                 
						echo $nombre." ".$paterno." ".$materno; ?>
                  	&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    	<label class="textos">Fecha de Fabricación: </label>
                        <?php echo $fecha_elaboracion_molde_normal; ?>
                    <br /><br />
                    	<label class="textos">Observaciones: </label>
                        <?php echo $observaciones_elaboracion; ?>
                    </fieldset>
                    <br />
                    <fieldset>
                    	<legend>Entregado</legend>
                   		<label class="textos">Recibio: </label>
                        <?php echo $recibio; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    	<label class="textos">Fecha de Entrega: </label> 
                        <?php echo $fecha_entrega_normal; ?> 
                   	<br /><br />
                    	<label class="textos">Pago: </label>
                    	$<?php if( $adaptacion == "si" && $reposicion == "" ){ echo " 0.00"; }elseif( $reposicion == "si" && $adaptacion == "" ){ echo " 0.00"; }else{ echo number_format($pago,2); }  ?>
                   	<br /><br />
                    	<label class="textos">Entrego: </label>
      	<?php			
						$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
															WHERE id_empleado=".$quien_entrego)
															or die(mysql_error());
						$row14 = mysql_fetch_array($consulta_empleados);
						$nombre = $row14["nombre"];
						$paterno = $row14["paterno"];
						$materno = $row14["materno"];                 
						echo $nombre." ".$paterno." ".$materno; 
		?>
                    </fieldset>
        <?php 	
				}
		?>
                </form>
                <p align="right">
                	<?php
                		if ( $id_estatus_molde != 7 )
                		{
                		?>
                			<a href="lista_moldes.php">
                        		<input name="volver" type="button" value="Volver" class="fondo_boton" />
                    		</a>
                		<?php
                		}
                		else
                		{
                		?>
                			<a href="lista_moldes_entregados.php">
                        		<input name="volver" type="button" value="Volver" class="fondo_boton" />
                    		</a>
                		<?php
                		}
                	?>
                </p>
                <!-- Para mostrar el movimiento del auxiliar del paciente -->
                    <a href="#" onClick="muestra_oculta('contenido_a_mostrar')" title="">
                        <label class="textos" style="font-size:13px;">Mostrar Movimientos del Molde</label>
                    </a>
                    <div id="contenido_a_mostrar">
                    <table style="border-collapse:collapse;">
                        <tr>
                            <th width="60">N° nota</th>
                            <th width="80">Fecha</th>
                            <th width="100">Hora</th>
                            <th width="130">Movimiento</th>
                            <th width="100">Responsable</th>
                            <th width="100">Estatus</th>
                        </tr>
 	<?php
        /* Consulta el movimiento del molde de la tabla movimientos_moldes */
		$consulta_movimientos_molde = mysql_query("SELECT * FROM movimientos_moldes
															WHERE folio_num_molde=".$num_folio_molde) 
															or die(mysql_error());
		while($row8 = mysql_fetch_array($consulta_movimientos_molde)){
			$num_folio = $row8["folio_num_molde"];
			$id_movimiento = $row8["id_estado_movimiento"];
			$fecha_movimiento = $row8["fecha"];
			$fecha_movimiento_separada = explode("-", $fecha_movimiento);
			$fecha_movimiento_normal = $fecha_movimiento_separada[2]."/".$fecha_movimiento_separada[1]."/".$fecha_movimiento_separada[0];
			$hora_movimiento = $row8["hora"];
			$estatus_molde = $row8["id_estatus_moldes"];
			$id_responsable = $row8["id_empleado"];
			/* Consulta el estado del movimiento */				
			$consulta_descripcion_estado = mysql_query("SELECT estado_movimiento
															FROM estados_movimientos
															WHERE id_estado_movimiento=
															".$id_movimiento) 
															or die(mysql_error());
															
			$row9 = mysql_fetch_array($consulta_descripcion_estado);
			$estado_movimiento = $row9["estado_movimiento"];
			/* Consulta el responsable del movimiento */		
			$consulta_responsable = mysql_query("SELECT alias FROM empleados
																WHERE id_empleado=".$id_responsable) 
																or die(mysql_error());
													
			$row10 = mysql_fetch_array($consulta_responsable);
			$alias_responsable = $row10["alias"];
			/* Consulta el estatus de la reparacion */		
			$consulta_estatus_molde = mysql_query("SELECT estado_molde
															FROM estatus_moldes
															WHERE id_estatus_moldes
															=".$estatus_molde) 
															or die(mysql_error());
			$row11 = mysql_fetch_array($consulta_estatus_molde);
			$descripcion_estatus_molde = $row11["estado_molde"];		
	?>
                       	<tr>
                            <td><?php echo $num_folio; ?></td>
                            <td><?php echo $fecha_movimiento_normal; ?></td>
                            <td><?php echo $hora_movimiento; ?></td>
                            <td><?php echo $estado_movimiento; ?></td>
                            <td><?php echo $alias_responsable; ?></td>
                            <td><?php echo $descripcion_estatus_molde; ?></td>
                        </tr>
	<?php	
		}
	?>
                    </table>
                    </div><!--Fin de contenido a mostrar-->
                <br /><br />
                </div><!--Fin contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>