<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Anamnesis</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<link rel="stylesheet" href="../css/jquery.alerts.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript">
function muestra_oculta(id){
	if (document.getElementById){ //se obtiene el id
		var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
		el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
	}
}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
	muestra_oculta('contenido_a_mostrar');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar2');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar3');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar4');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar5');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar6');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar7');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar8');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar9');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar10');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar11');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar12');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar13');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar14');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar15');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
	muestra_oculta('contenido_a_mostrar16');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
}
				
// Fin del script -->
</script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script language="JavaScript">

$(document).ready(function()
{
	$('#lugar_residencia').change(function(){
		if($('#lugar_residencia option:selected').val() == "Agregar")
		{	
			jPrompt("Agregar nuevo lugar de Residencia","","Agregar Residencia",function(r) {
				if(r)
				{
					$.post('pagina.php',{residencia:r},function(data){
						$('#lugar_residencia').find('option').not(':first').remove();
						$.post('seleccion_residencias.php', {},function(data2){
							$('#lugar_residencia').append(data2);
							})
					});	
				}
				});
		}
		});	
});
function agregar() {
	$.post('agregar.php',{}, function(data){
		$("#nuevo").append(data);
		});									
}
function agregar_actividad() {
	$.post('agregar_actividad.php',{}, function(data){
		$("#nuevo1").append(data);
		});									
}
function agregar_auxiliar() {
	$.post('agregar_auxiliar.php',{}, function(data){
		$("#nuevo2").append(data);
		});									
}
function agregar_padecimiento() {
	$.post('agregar_padecimiento.php',{}, function(data){
		$("#nuevo3").append(data);
		});									
}
</script>
<script type="text/javascript" src="../js/jquery.alerts.js"></script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Anamnesis
                </div>                
            </div><!--Fin de fondo titulo-->           
            <div class="area_contenido1">
	<?php
                include("config.php");
                date_default_timezone_set('America/Monterrey');
                $script_tz = date_default_timezone_get();
                $fechaSistema = date("d/m/Y");              
                $id_cliente = $_GET["id_paciente"];
                $consulta_datos_paciente = mysql_query("SELECT nombre, paterno, materno 
                                                                FROM ficha_identificacion
                                                                WHERE id_cliente=".$id_cliente) 
                                                                or die(mysql_error());
                $row = mysql_fetch_array($consulta_datos_paciente);
                $nombre = $row["nombre"];
                $paterno = $row["paterno"];
                $materno = $row["materno"];
  	?>
                <div class="contenido_proveedor">
                <br />
                <form action="proceso_guardar_anamnesis.php" method="post" name="forma1"> 
                    <label class="textos" style="font-size:15px;">Nombre del Cliente: 
                    <?php echo $nombre." ".$paterno." ".$materno; ?></label>
                    <input name="id_paciente" type="hidden" value="<?php echo $id_cliente; ?>" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label class="textos" style="font-size:15px;">Fecha: <?php echo $fechaSistema; ?></label>
                <input name="fecha" type="hidden" value="<?php echo $fechaSistema; ?>" />                  
                <br /><br />
                    <label class="textos" style="font-size:15px;">Elaboro: </label>
                    <select name="elaboro">
                        <option value="0">Seleccione</option>
   	<?php
                        $consulta_nombre_doctor = mysql_query("SELECT nombre, paterno, materno, id_empleado
                                                        FROM empleados") 
                                                        or die(mysql_error());
                        while($row3 = mysql_fetch_array($consulta_nombre_doctor)){
                            $id_empleado = $row3["id_empleado"];
                            $nombre = $row3["nombre"];
                            $paterno = $row3["paterno"];
                            $materno = $row3["materno"];
                    
   	?>
                        <option value="<?php echo $id_empleado?>">
                            <?php echo $nombre." ".$paterno." ".$materno;?>
                        </option>
  	<?php
                        }
    ?>
                    </select>
                <br /><br />
                <fieldset>
                <legend>
                    <label class="textos" style="font-size:15px;">Cuestionario de Anamnesis</label>
                </legend>
                    <label class="textos">1-. ANTECEDENTES DE FAMILIARES CONSANGUINEOS </label>
                
                <ul><!--Antecendentes de familiares consaguineos-->
                    <li>Sordera.</li>
                        <input type="radio" value="Si" name="sordera" />Si
                        <input type="radio" value="No" name="sordera" checked="checked" />No
                    <li>Hipertencion.</li>
                        <input type="radio" value="Si" name="hipertencion" />Si
                        <input type="radio" value="No" name="hipertencion" checked="checked" />No
                    <li>Diabeticos.</li>
                        <input type="radio" value="Si" name="diabeticos" />Si
                        <input type="radio" value="No" name="diabeticos" checked="checked" />No
                    <li>Es un paciente con sordera infantil?</li>
                        <input type="radio" value="Si" name="con_sordera_infantil" onClick="muestra_oculta('contenido_a_mostrar')"/>Si
                        <input type="radio" value="No" name="con_sordera_infantil" checked="checked" onClick="muestra_oculta('contenido_a_mostrar')"/>No
                        
                        <div id="contenido_a_mostrar"><!--Inicio de contenido mostrar-->
                            <ul>
                                <li>Fue prematuro.</li>
                                    <input type="radio" value="Si" name="fue_prematuro" />Si
                                    <input type="radio" value="No" name="fue_prematuro" checked="checked" />No
                                <li>Tubo bilirrubinas altas?</li>
                                    <input type="radio" value="Si" name="tubo_bilirrubinas_altas" />Si
                                    <input type="radio" value="No" name="tubo_bilirrubinas_altas" checked="checked" />No
                                <li>Estubo en incubadora?</li>
                                    <input type="radio" value="Si" name="estubo_en_incubadora" />Si
                                    <input type="radio" value="No" name="estubo_en_incubadora" checked="checked" />No
                                <li>Otros problemas durante el embarazo.</li>
                                    <input name="otros_problemas_durante_embarazo" type="text" size="100" maxlength="100" />
                            </ul>
                        </div><!--Finde contenido mostrar-->
                        
                    <li>Otros.</li>
                        <input name="otros" type="text" size="100" maxlength="100" />
                </ul><!--Fin de Antecendentes de familiares consaguineos-->
                
                    <label class="textos">2-. ANTECEDENTES PERSONALES NO PATOLOGICOS. </label> 
                    
                <ul><!--ANTECEDENTES PERSONALES NO PATOLOGICOS-->
                    <li>Lugar de residencia</li>
                        <select name="lugar_residencia" id="lugar_residencia">
                            <option value="0">Seleccione</option>                           
  	<?php
                        $consulta_residencias = mysql_query("SELECT * FROM residencias") or die(mysql_error());
                        while($row2 = mysql_fetch_array($consulta_residencias)){
                            $id_residencia = $row2["id_residencia"];
                            $descripcion_residencia = $row2["descripcion_residencia"];
  	?>
                            <option value="<?php echo $id_residencia; ?>">
                                <?php echo $descripcion_residencia; ?>
                            </option>
    <?php
                        }
    ?>
                            <option value="Agregar">Agregar</option>
                        </select>
                </ul><!--Fin de ANTECEDENTES PERSONALES NO PATOLOGICOS-->
                
                    <label class="textos">Laborales Actuales </label>
                    
                <ul><!--Inicio de Laborales Actuales-->
                    <li>Ocupacion Actual: </li>
                        <input name="ocupacion_actual" type="text" size="100" maxlength="100" />
                    <li>Desde que año: </li>
                        <input name="desde_el_año" type="text" />
                    <li>Expuesto a ruido?</li>
                        <input type="radio" value="Si" name="expuesto_a_ruido" onClick="muestra_oculta('contenido_a_mostrar2')" />Si
                        <input type="radio" value="No" name="expuesto_a_ruido" checked="checked" onClick="muestra_oculta('contenido_a_mostrar2')" />No
                        
                        <div id="contenido_a_mostrar2"><!--Inicio de contenido mostrar 2-->
                            <ul>
                                <li>Fuente del Ruido:</li>
                                    <input name="fuente_del_ruido" type="text" size="100" maxlength="100" />
                                <li>Tiempo de exposicion diario:</li>
                                    <input name="tiempo_exposicion_diario" type="text" />hrs.
                            </ul>
                        </div><!--Finde contenido mostrar 2-->
                        
                    <li>Utiliza solventes o metales pesados?</li>
                        <input type="radio" value="Si" name="utiliza_solventes_o_metales_actual" onClick="muestra_oculta('contenido_a_mostrar3')" />Si
                        <input type="radio" value="No" name="utiliza_solventes_o_metales_actual" checked="checked" onClick="muestra_oculta('contenido_a_mostrar3')" />No
                        
                        <div id="contenido_a_mostrar3"><!--Inicio de mostrar 3-->
                            <ul>
                                <li>Producto:</li>
                                    <input name="producto" type="text" size="100" maxlength="100" />
                            </ul>
                        </div><!--Fin de contenido mostrar 3-->
                        
                    <li>Otros factores de riesgo auditivo:</li>
                        <input name="otros_factores_riesgo_auditivo" type="text" size="100" maxlength="100" />
                </ul><!--Fin de Laborales Actuales-->
                
                    <label class="textos">Laborales Anteriores </label>
                    
                <ul><!--Inicio de Laborales Anteriores-->
                    <li>Estubo en algun otro trabajo expuesto a factores de riesgo auditivo:</li>
                        <input type="radio" value="Si" name="otros_trabajos_expuesto_factores_riesgo" onClick="muestra_oculta('contenido_a_mostrar4')" />Si
                        <input type="radio" value="No" name="otros_trabajos_expuesto_factores_riesgo" checked="checked" onClick="muestra_oculta('contenido_a_mostrar4')" />No
                        
                        <div id="contenido_a_mostrar4"><!--Inicio de contenido mostrar 4-->
                            <ul>
                                <li>Ocupacion: </li>
                                    <input name="ocupacion_anterior[]" type="text" size="100" maxlength="100" />
                                <li>Desde que año:</li>
                                    <input name="desde_que_año[]" type="text" />
                                <li>Hasta que año:</li>
                                    <input name="hasta_el_año[]" type="text" />
                                <li>Expuesto a ruido?</li>
                                    <input type="radio" value="Si" name="expuesto_a_ruido_anterior[]" onClick="muestra_oculta('contenido_a_mostrar5')" />Si
                                    <input type="radio" value="No" name="expuesto_a_ruido_anterior[]" checked="checked" onClick="muestra_oculta('contenido_a_mostrar5')" />No
                                    
                                <div id="contenido_a_mostrar5"><!--Inicio de contenido mostrar 5-->
                                    <ul>
                                        <li>Fuente del Ruido:</li>
                                            <input name="fuente_del_ruido_anterior[]" type="text" size="90" maxlength="90" />
                                        <li>Tiempo de exposicion diario:</li>
                                            <input name="tiempo_exposicion_diario_anterior[]" type="text" />hrs.
                                    </ul>
                                </div><!--Fin de contenido mostrar 5-->
                                
                                <li>Utilizaba solventes o metales pesados?</li>
                                    <input type="radio" value="Si" name="utiliza_solventes_o_metales_anterior[]" onClick="muestra_oculta('contenido_a_mostrar6')" />Si
                                    <input type="radio" value="No" name="utiliza_solventes_o_metales_anterior[]" checked="checked" onClick="muestra_oculta('contenido_a_mostrar6')" />No
                        
                                <div id="contenido_a_mostrar6"><!--Inicio de contenido mostrar 6-->
                                    <ul>
                                        <li>Producto:</li>
                                            <input name="producto_anterior[]" type="text" size="90" maxlength="90" />
                                    </ul>
                                </div><!--Fin de contenido mostrar 6-->
                                
                                <li>Otros factores de riesgo auditivo:</li>
                                    <input name="otros_factores_riesgo_auditivo_anterior[]" type="text" size="90" maxlength="90" />
                            </ul>
                        </div><!--Fin de contenido mostrar 4-->
                    <div id="nuevo"></div>
                                <li>Desea agregar otro trabajo con exposicion a factores de riesgo auditivo</li>                                       <span style="font-size:18px;"><a href="#" onclick="agregar();">Agregar</a></span>    
                </ul><!--Fin de Laborales Anteriores-->        
                    <label class="textos">Extra Laborales </label>                    
                <ul><!--Inicio de Extra laborales-->
                    <li>Practica alguna actividad donde este expuesto a factores de riesgo auditivo:</li>
                        <input type="radio" value="Si" name="otras_actividades_expuesto_factores_riesgo" onClick="muestra_oculta('contenido_a_mostrar7')" />Si
                        <input type="radio" value="No" name="otras_actividades_expuesto_factores_riesgo" checked="checked" onClick="muestra_oculta('contenido_a_mostrar7')" />No                       
                    <div id="contenido_a_mostrar7"><!--Inicio de contenido mostrar 7-->
                      <ul>
                        <li>Actividad:</li>
                              <input name="actividad[]" type="text" size="90" maxlength="90"  />
                        <li>Desde que año:</li>
                              <input name="desde_que_año_actividad[]" type="text" />
                        <li>Expuesto a ruido?</li>
                            <input type="radio" value="Si" name="expuesto_a_ruido_actividad[]" onClick="muestra_oculta('contenido_a_mostrar8')" />Si
                            <input type="radio" value="No" name="expuesto_a_ruido_actividad[]" checked="checked" onClick="muestra_oculta('contenido_a_mostrar8')" />No                           
                        <div id="contenido_a_mostrar8"><!--Inicio de contenido mostrar 8-->
                            <ul>
                                <li>Fuente del Ruido:</li>
                                    <input name="fuente_del_ruido_actividad[]" type="text" size="90" maxlength="90" />
                                <li>Tiempo de exposicion mensual:</li>
                                    <input name="tiempo_exposicion_diario_actividad[]" type="text" />hrs.
                            </ul>
                        </div><!--Fin de contenido mostrar 8-->                        
                        <li>Utilizaba solventes o metales pesados?</li>
                            <input type="radio" value="Si" name="utiliza_solventes_o_metales_actividad[]" onClick="muestra_oculta('contenido_a_mostrar9')" />Si
                            <input type="radio" value="No" name="utiliza_solventes_o_metales_actividad[]" checked="checked" onClick="muestra_oculta('contenido_a_mostrar9')" />No                           
                        <div id="contenido_a_mostrar9"><!--Inicio de contenido mostrar 9-->
                            <ul>
                                <li>Producto:</li>
                                    <input name="producto_actividad[]" type="text" size="90" maxlength="90" />
                            </ul>
                        </div><!--Fin de contenido mostrar 9-->
                        <li>Otros factores de riesgo auditivo:</li>
                            <input name="otros_factores_riesgo_auditivo_actividad[]" type="text" size="90" maxlength="90" />
                        </ul>
                    </div><!--Fin de contenido mostrar 7-->
                    <div id="nuevo1"></div>
                                <li>Desea agregar otra actividad con exposicion a factores de riesgo auditivo</li>                                       <span style="font-size:18px;"><a href="#" onclick="agregar_actividad();">Agregar</a></span>
                </ul><!--Fin de Extra laborales-->                
                <label class="textos">3-. ANTECEDENTES PERSONALES PATOLOGICOS. </label>                 
                <ul><!--Inicio de ANTECEDENTES PERSONALES PATOLOGICOS.-->
                    <li>Presenta sordera:</li>
                        <input type="radio" value="Si" name="presenta_sordera" onClick="muestra_oculta('contenido_a_mostrar10')" />Si
                        <input type="radio" value="No" name="presenta_sordera" checked="checked" onClick="muestra_oculta('contenido_a_mostrar10')" />No
                        
                        <div id="contenido_a_mostrar10"><!--Inicio de contenido mostrar 10-->
                            <ul>
                                <li>Desde que año:</li>
                                    <input name="desde_que_año_sordo" type="text" />
                                <li>Ha usado auxiliar auditivo:</li>
                                    <input type="radio" value="Si" name="ha_usado_auxiliar" onClick="muestra_oculta('contenido_a_mostrar11')" />Si
                                    <input type="radio" value="No" name="ha_usado_auxiliar" checked="checked" onClick="muestra_oculta('contenido_a_mostrar11')" />No
                                <div id="contenido_a_mostrar11"><!--Inicio de contenido mostrar 11-->
                                  <ul>
                                        <li>Desde que año:</li>
                                            <input name="desde_que_año_auxiliar[]" type="text" />
                                        <li>Modelo</li>
                                            <input name="modelo[]" type="text" size="60" maxlength="60"  />
                                        <li>Que resultados obtuvo</li>
                                          <input name="que_resultado_obtuvo[]" type="text" size="80" maxlength="80" />
                                    </ul>
                                     <div id="nuevo2"></div>
                                <li>Desea agregar otro auxiliar auditivo que ha usado?</li>                                       <span style="font-size:18px;"><a href="#" onclick="agregar_auxiliar();">Agregar</a></span>                                    
                                </div><!--Fin de contenido mostrar 11-->       
                            </ul>
                        </div><!--Fin de contenido mostrar 10-->
                    <li>Hipertencion:</li>
                        <input type="radio" value="Si" name="hipertencion_personales" />Si
                        <input type="radio" value="No" name="hipertencion_personales" checked="checked" />No
                    <li>Diabeticos:</li>
                        <input type="radio" value="Si" name="diabeticos_personales" />Si
                        <input type="radio" value="No" name="diabeticos_personales" checked="checked" />No
                    <li>Infecciones de garganta recurrentes:</li>
                        <input type="radio" value="Si" name="infecciones_garganta" />Si
                        <input type="radio" value="No" name="infecciones_garganta" checked="checked" />No 
                    <li>Ha presentado algun padecimiento, cirugia o traumatismo que afecte sus oidos:</li>
                        <input type="radio" value="Si" name="cirugia_traumatismo" onClick="muestra_oculta('contenido_a_mostrar12')" />Si
                        <input type="radio" value="No" name="cirugia_traumatismo" checked="checked" onClick="muestra_oculta('contenido_a_mostrar12')" />No
                        <div id="contenido_a_mostrar12"><!--Inicio de contenido mostrar 12-->
                            <ul>
                                <li>Descripcion:</li>
                                    <input name="descripcion[]" type="text" size="80" maxlength="80" />
                                <li>Desde que año:</li>
                                    <input name="desde_que_año_cirugia_traumatismo[]" type="text" />
                                <li>Tratamiento:</li>
                                    <input name="tratamiento[]" type="text" size="80" maxlength="80" />
                                <li>Resultados:</li>
                                    <input name="resultados[]" type="text" size="80" maxlength="80" />
                               <div id="nuevo3"></div>
                                <li>Desea agregar otro padecimiento</li><span style="font-size:18px;"><a href="#" onclick="agregar_padecimiento();">Agregar</a></span>                          
                            </ul>                            
                        </div><!--Fin de contenido mostrar 12-->
                
                </ul><!--Fin de ANTECEDENTES PERSONALES PATOLOGICOS.-->
                <p align="right">
                    <input name="accion" type="submit" value="Guardar" class="fondo_boton" />
                </p>                
                </fieldset>
                <br />
                </form>
                </div><!-- Fin de contenido proveedor -->           
            </div><!-- Fin de area contenido -->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>