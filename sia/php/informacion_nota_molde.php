<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL ID DE LA NOTA DE MOLDE A TRAVES DEL METODO POST
	$folio_nota_molde = $_POST['folio_nota_molde'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS CORRESPONDIENTES A MOSTRAR DE ACUERDO AL ID RECIBIDO
	$query_nota_molde = "SELECT folio_num_molde,fecha_entrada,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo,lado_oido,adaptacion,reposicion,costo
						 FROM moldes
						 WHERE folio_num_molde = '$folio_nota_molde'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
	$resultado_nota_molde = mysql_query($query_nota_molde) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_nota_molde = mysql_fetch_array($resultado_nota_molde);

	// SE DECLARAN VARIABLES CON EL RESULTADO DEL QUERY
	$folio_num_molde = $row_nota_molde['folio_num_molde'];
	$fecha_entrada = $row_nota_molde['fecha_entrada'];
	$fecha_entrada_separada = explode("-", $fecha_entrada);
	$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
	$nombre_cliente = $row_nota_molde['nombre_completo'];
	$lado_oido = $row_nota_molde['lado_oido'];
	$adaptacion = $row_nota_molde['adaptacion'];
	$reposicion = $row_nota_molde['reposicion'];
	$precio = $row_nota_molde['costo'];

	// SE IMPRIMEN LAS VARIABLES PARA MOSTRARLAS EN PANTALLA
	echo $folio_num_molde;
	echo "°";
	echo $fecha_entrada_normal;
	echo "°";
	echo $nombre_cliente;
	echo "°";
	echo ucfirst($lado_oido);
	echo "°";
	if ( $adaptacion == "si" && $reposicion == "" ) 
	{
		echo "Adaptación";
	}
	elseif ( $reposicion == "si" && $adaptacion == "" ) 
	{
		echo "Reposición";
	}
	echo "°";
	if ( ($adaptacion == "si" && $reposicion == "" ) || ($reposicion == "si" && $adaptacion == "" )  ) 
	{
		echo "$ 0.00";
	}
	else
	{
		echo "$".number_format($precio,2);
	}
?>