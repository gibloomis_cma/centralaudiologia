<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Instituciones</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<style type="text/css">
	#table tr td{
		border:1px solid #939;
	}
</style>
</head>
<body>
<div id="contenido_columna2">
	<div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Instituciones
            </div>
        </div><!--Fin de fondo titulo-->
        <div class="area_contenido1">
    	<?php
			include("config.php");       
			$id_institucion = $_GET["id_institucion"];
			$consulta_datos_institucion = mysql_query("SELECT * FROM instituciones 
												WHERE id_institucion = ".$id_institucion)
												or die(mysql_error());
			$row = mysql_fetch_array($consulta_datos_institucion);
			$institucion = $row["institucion"];
			$razon_social = $row["razon_social"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$codigo_postal = $row["codigo_postal"];
			$colonia = $row["colonia"];
			$id_estado = $row["id_estado"];
			$id_ciudad = $row["id_ciudad"];
			$descuento = $row["descuento"];
			$poliza = $row["poliza"];
			$costo_aparato = $row["costo_aparato"];
			$rfc = $row["rfc"];
    	?>
            <div class="contenido_proveedor"  id="kls">
                <br />                                           
                <form action="procesa_modificar_institucion.php" method="post" name="form_agregar_instituciones" 
                onsubmit="return validarAgregarInstituciones()" class="textos2"> 
                <table>                    
                    <tr>
                        <th colspan="4">Datos Generales</th>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <label class="textos">Nombre: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="id_institucion" type="hidden" value="<?php echo $id_institucion; ?>" />
                            <input name="institucion" type="text" value="<?php echo $institucion; ?>" size="30" />
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Razon Social: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="razon_social" type="text" size="30" 
                        value="<?php echo $razon_social; ?>" maxlength="50" />
                        </td>
                    </tr><tr>                   
                        <td style="text-align:right">
                            <label class="textos">Calle: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="calle" id="calle" type="text" size="30" 
                            value="<?php echo $calle; ?>" title="Calle">
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Número: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="num_ext" id="num_ext" type="text" size="3" maxlength="5" 
                            title="Exterior" value="<?php echo $num_exterior; ?>"
                            onchange="validarSiNumero(this.value)">
                            <label class="textos"> - </label>                           
                            <input name="num_int" id="num_int" type="text" size="3" 
                            title="Interior" value="<?php echo $num_interior; ?>">
                        </td>
                   </tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Colonia:</label>
                        </td>
                        <td style="text-align:left">
                            <input name="colonia" id="colonia" type="text" size="30" 
                            value="<?php echo $colonia; ?>" title="Colonia">
                        </td>
                        <td style="text-align:right">
                            <label class="textos">C.P.: </label>
                        </td>
                        <td style="text-align:left;">
                            <input name="codigo_postal" id="codigo_postal" type="text" size="4" maxlength="5" 
                            title="Código Postal" onchange="validarCP(this.value)" value="<?php echo $codigo_postal; ?>">
                        </td>
                    </tr><tr>                    
                        <td style="text-align:right">
                            <label class="textos">Estado: </label>
                        </td>
                        <td style="text-align:left">
                            <select id="id_estado" name="id_estado" title="Estados">
                                <option value="0" selected="selected"> --- Estado --- </option>
                            <?php
                                $consulta_estados = mysql_query("SELECT * FROM estados");
                                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                    $id_estado_consultada = $row3["id_estado"];
                                    $estado = $row3["estado"];
                                    if($id_estado<>"" && $id_estado == $id_estado_consultada){
                            ?>
                                <option value="<?php echo $id_estado; ?>" selected="selected">
                                   <?php echo utf8_encode($estado); ?> 
                                </option>
                            <?php
                                    }else{
                            ?>       
                                <option value="<?php echo $id_estado_consultada; ?>">
                                    <?php echo utf8_encode($estado);?>
                                </option>
                            <?php 
                                    } 
                                }
                            ?>
                            </select>
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Ciudad: </label>
                        </td>
                        <td style="text-align:left">
                            <select name="id_municipio" id="id_municipio" title="Municipio">
                                <option value="0" selected="selected"> --- Municipio --- </option>
                            <?php
                                $consulta_ciudad = mysql_query("SELECT ciudad, id_ciudad 
                                                                FROM ciudades 
                                                                WHERE id_estado=".$id_estado);
                                while( $row4 = mysql_fetch_array($consulta_ciudad)){ 
                                    $id_ciudad_consultada = $row4["id_ciudad"];
                                    $ciudad =ucwords(strtolower($row4["ciudad"]));
                                    if($id_ciudad<>"" && $id_ciudad == $id_ciudad_consultada){
                            ?>
                                <option value="<?php echo $id_ciudad; ?>" selected="selected">
                                    <?php echo utf8_encode($ciudad); ?> 
                                </option>
                            <?php
                                    }else{
                            ?>       
                                <option value="<?php echo $id_ciudad_consultada; ?>">
                                    <?php echo utf8_encode($ciudad); ?>
                                </option>
                            <?php
                                    } 
                                }
                            ?>
                            </select>
                        </td>
                    </tr><tr>                    
                        <td>
                            <label class="textos">Descuento: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="descuento" id="descuetno" type="text" title="Descuento" 
                            onchange="validarSiNumero(this.value)" size="3" value="<?php echo $descuento; ?>">
                        	<label class="textos"> %</label>
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Costo por aparato: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="costo_aparato" id="costo_aparato" type="text" 
                            value="<?php echo $costo_aparato; ?>" title="Costo por aparato" size="11">                            
                        </td> 
                    </tr><tr>
                    	<td style="text-align:right">
                         	<label class="textos">RFC: </label>
                        </td><td style="text-align:left">
                            <input name="rfc" type="text" value="<?php echo strtoupper($rfc); ?>" size="20" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Poliza</label> 
                        </td><td style="text-align:left" colspan="4">
                        	<textarea name="poliza" rows="2" cols="65"><?php echo $poliza; ?></textarea>
                        </td>
                    </tr><tr>                                                   
                        <td style="text-align:right" colspan="4">                        
                            <input name="accion" type="submit" value="Modificar" class="fondo_boton" 
                            title="Modificar Datos Generales"/>
                        </td>
                    </tr>
                </table>  
                </form>
            </div><!--Fin de contenido proveedor-->    <br />   
	        <div class="contenido_proveedor">
                <table>                	
                    <tr>
                        <th colspan="4">Contacto(s)</th>
                    </tr>
					<?php    
                        $consulta_contacto = mysql_query("SELECT * FROM contactos_instituciones 
                                                        WHERE id_institucion=".$id_institucion) 
                                                        or die(mysql_error());            
                        $contador=0;
						while($row2 = mysql_fetch_array($consulta_contacto)){
                            $id_contacto_institucion = $row2["id_contacto_institucion"];
                            $nombre = $row2["nombre"];
                            $paterno = $row2["paterno"];
                            $materno = $row2["materno"];
							$puesto = $row2["puesto"];
							$departamento = $row2["departamento"];
                            $contador += 1;
                    ?>
                    
                    
                    
                    <!--INICIA FORMULARIO-->
                    
                    
                    
                    
	                <form name="forma2" action="procesa_modificar_contactos_institucion.php" method="post" >                   	
                    <tr>
                        <td colspan="2">
                        	<table style="width:300px !important">
                            	<tr>
                                <input name="id_contacto" type="hidden" 
                                value="<?php echo $id_contacto_institucion; ?>" />
                                <input name="id_institucion" type="hidden" 
                                value="<?php echo $id_institucion; ?>" />
                                    <td style="text-align:right">
                                        <label class="textos">Nombre:</label>
                                    </td><td>
                            			<input name="nombre" type="text" value="<?php echo $nombre; ?>" />
                            		</td>
                            	</tr><tr>
                            		<td style="text-align:right">
                                    	<label class="textos">Paterno:</label>
                                    </td><td>
                                    	<input name="paterno" type="text" value="<?php echo $paterno; ?>" />
                                    </td>
                            	</tr><tr>
                            		<td style="text-align:right">
                                    	<label class="textos">Materno:</label>
                                    </td><td>
                                    	<input name="materno" type="text" value="<?php echo $materno; ?>" />
                                    </td>
                            	</tr>
                            </table>
                        </td><td colspan="2">
                             <table style="width:300px !important">
                                    <tr>
                         				<td style="text-align:right">
                                        	<label class="textos">Puesto:</label>
                                        </td><td>
				                            <input name="puesto" type="text" value="<?php echo $puesto; ?>" />
                            			</td>
                                	</tr><tr>
                                    	<td style="text-align:right">
                                        	<label class="textos"> Departamento:</label>
                                        </td><td>
                            				<input name="departamento" type="text" value="<?php echo $departamento; ?>" />
                         			</td>
                           		</tr>
                            </table>
                    </tr><tr>
                        <td colspan="4" style="text-align:center">
                            <label class="textos">Formas de contacto</label>
                        </td>
                    </tr>                             
					<?php							
                        $consulta_forma_contacto = mysql_query("SELECT * FROM forma_contacto_instituciones
                                                        WHERE id_contacto_institucion=".$id_contacto_institucion)
                                                        or die(mysql_error());                
                        while($row5 = mysql_fetch_array($consulta_forma_contacto)){
                            $id_forma_contacto = $row5["id_forma_contacto"];
                            $tipo = $row5["tipo"];
                            $descripcion = $row5["descripcion"];	            
                    ?>
                    <tr>
                        <td style="text-align:center;" colspan="4">	
                            <input name="id_forma_contacto[]" type="hidden" value="<?php echo $id_forma_contacto; ?>" />
                            <label class="textos">Tipo:</label> 
                            <select name="tipo_telefono[]"/>
                        <?php
                            $formas_contacto[0] = "Telefono";
                            $formas_contacto[1] = "Celular";
                            $formas_contacto[2] = "Fax";
                            $formas_contacto[3] = "Correo";						 
                            for($i=0; $i<4; $i++){
                                if($tipo == $formas_contacto[$i]){							
                        ?>                                                           
                                    <option value="<?php echo $tipo; ?>" selected="selected">
                                        <?php echo $tipo; ?>
                                    </option>
                        <?php 
                                }else{
                        ?>                            
                                <option value="<?php echo $formas_contacto[$i]; ?>">
                                    <?php echo $formas_contacto[$i]; ?>
                                </option>
                        <?php
                                }
                            }
                        ?>
                            </select>
                            <label class="textos">Descripción:</label>
                            <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>" />
                         	&nbsp;&nbsp;
                            <input type="button" value="Eliminar" class="fondo_boton" 
                            title="Eliminar <?php echo $tipo; ?>" 
                            onclick="location='proceso_eliminar_forma_contacto_institucion.php?id_forma_contacto=<?php echo $id_forma_contacto; ?>&id_institucion=<?php echo $id_institucion; ?>'">                                 
                        </td>
                    </tr>         
		            <?php
                		}
            		?>  
                    <tr>                  
                        <td style="text-align:center; padding-top:10px" colspan="4">
                            <input type="button" value="Nueva Forma de Contacto" class="fondo_boton"
                            title="Agregar Nueva Forma de Contacto"
                            onclick="location='agregar_forma_contacto_institucion2.php?id_institucion=<?php echo $id_institucion; ?>&id_contacto=<?php echo $id_contacto_institucion; ?>'"/>                                
                        	&nbsp;&nbsp;
                            <input name="accion" type="submit" value="Guardar Cambios" class="fondo_boton" 
                            title="Guardar Cambios de <?php echo $nombre." ".$paterno; ?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="4"><hr /></td>
                    </tr>
				</form>
	            
		        <!--TERMINA FORMULARIO-->
        
        	<?php
            	}
        	?>  
            		<tr>
                    	<td colspan="4" style="text-align:center"> 
                        <input type="button" value="Volver" class="fondo_boton"
		               onclick="location='detalles_institucion.php?id_institucion=<?php echo $id_institucion; ?>'"/>
                    	<input type="button" value="Nuevo Contacto" class="fondo_boton"
		               onclick="location='agregar_contacto_institucion2.php?id_institucion=<?php echo $id_institucion; ?>'"/>
                       	</td>
                	</tr> 
            	</table>
            </div><!--Fin contenido proveedor-->
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>