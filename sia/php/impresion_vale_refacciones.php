<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA QUERY QUE OBTIENE LA INFORMACION DEL VALE DE REFACCIONES
	$query_vale_refaccion = "SELECT id_registro, vales_refacciones.uso AS uso, fecha, hora, id_departamento, CONCAT( nombre, ' ', paterno, ' ', materno ) AS nombre_completo_empleado
							 FROM vales_refacciones, empleados, uso_refacciones
							 WHERE vales_refacciones.uso = id_uso_refaccion
							 AND responsable = id_empleado
							 ORDER BY id_registro DESC";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO QUE SE ALMACENA EN UNA VARIABLE
	$resultado_vale_refaccion = mysql_query($query_vale_refaccion) or die(mysql_error());

	// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO EN UNA VARIABLE
	$row_vale_de_refaccion = mysql_fetch_array($resultado_vale_refaccion);

	// SE ALMACENAN EN VARIABLES EL RESULTADO OBTENIDO DEL QUERYS
	$id_vale_de_refaccion = $row_vale_de_refaccion['id_registro'];
	$uso_vale_refaccion = $row_vale_de_refaccion['uso'];
	$fecha_vale_refaccion = $row_vale_de_refaccion['fecha'];
	$fecha_vale_refaccion_separada = explode("-", $fecha_vale_refaccion);
	$fecha_vale_refaccion_normal = $fecha_vale_refaccion_separada[2]."/".$fecha_vale_refaccion_separada[1]."/".$fecha_vale_refaccion_separada[0];
	$hora_vale_refaccion = $row_vale_de_refaccion['hora'];
	$departamento_vale_refaccion = $row_vale_de_refaccion['id_departamento'];
	$nombre_empleado = $row_vale_de_refaccion['nombre_completo_empleado'];

	// SE REALIZA QUERY QUE OBTIENE EL ID DE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO DEL EMPLEADO
	$query_id_sucursal = "SELECT id_sucursal
						  FROM areas_departamentos
						  WHERE id_departamento = '$departamento_vale_refaccion'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_id_sucursal = mysql_query($query_id_sucursal) or die(mysql_error());
	$row_id_sucursal = mysql_fetch_array($resultado_id_sucursal);
	$id_sucursal_vale = $row_id_sucursal['id_sucursal']; 

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL A LA QUE PERTENECE EL EMPLEADO
	$nombre_sucursal = "SELECT nombre
						FROM sucursales
						WHERE id_sucursal = '$id_sucursal_vale'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_sucursal = mysql_query($nombre_sucursal) or die(mysql_error());
	$row_nombre_sucursal = mysql_fetch_array($resultado_nombre_sucursal);
	$nombre_sucursal = $row_nombre_sucursal['nombre'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL USO DE LA REFACCION
	$query_nombre_uso = "SELECT uso
						 FROM uso_refacciones
						 WHERE id_uso_refaccion = '$uso_vale_refaccion'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO QUE SE ALMACENA EN UNA VARIABLE
	$resultado_nombre_uso = mysql_query($query_nombre_uso) or die(mysql_error());
	$row_nombre_uso = mysql_fetch_array($resultado_nombre_uso);
	$nombre_uso = $row_nombre_uso['uso'];
?>
<!-- ************************************************************************************************ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		body
		{
			font-size:14px;	
			font-family:Arial;
		}

		#cont
		{
			margin:0px auto;
			text-align:center;		
			padding-bottom:5px;
		}

		#impresion
		{
			text-align:center;
			width:215px;
			font-size:14px;	
			font-family:Arial;
		}

		#impresion table
		{
			margin:0px auto;
			font-family:Arial;
			font-size:12px;	
			font-size:10px;
		}

		#impresion table tr th
		{
			font-size:12px;	
			font-family:Arial;
		}

		#impresion table tr td
		{
			text-align:right;
			padding:5px 5px;
			font-size:12px;	
			font-family:Arial;
		}

		span
		{
			text-decoration:underline;
			font-size:14px;
			font-family:Arial;
		}
	</style>
</head>
<body onload="javascript: window.print();">
	<div id="cont">
		<div id="impresion">
			Vale de Refacciones <br />
			Sucursal <?php echo $nombre_sucursal; ?> <br/>
			Fecha: <?php echo $fecha_vale_refaccion_normal; ?><br />
			Hora: <?php echo $hora_vale_refaccion; ?> <br/>
			N° Vale de Refacción: <?php echo $id_vale_de_refaccion; ?>
			<br/><br/>
	
			<table>	
				<tr>
				   	<td style="font-family:Arial; text-align:center;"> Descripción </td>
				    <td style="font-family:Arial; text-align:center;"> Cantidad </td>
				</tr>
				<tr>
				   	<td colspan="2"> ------------------------------------------------- </td>
				</tr>
			<?php
				$descripcion_vale = mysql_query('SELECT almacen, descripcion, cantidad 
											   	 FROM descripcion_vale_refacciones, base_productos, almacenes
											   	 WHERE codigo = id_base_producto
											   	 AND origen = id_almacen
											   	 AND id_vale_refaccion = '.$id_vale_de_refaccion) or die(mysql_error());
				$x=0;
				while( $row_descripcion_vale = mysql_fetch_array($descripcion_vale))
				{
					$codigo = $row_descripcion_vale['descripcion'];
					$cantidad = $row_descripcion_vale['cantidad'];
					$origen = $row_descripcion_vale['almacen'];
					$x = $x + $cantidad;
			?>
				<tr> 
			    	<td style="font-family:Arial; text-align:left;"> <?php echo ucfirst(strtolower($codigo)); ?> </td>
			        <td style="font-family:Arial;"> <?php echo $cantidad; ?> </td>
			    </tr>
			<?php
				}
			?>
				<tr>
			    	<td></td>
			        <td style="font-family:Arial;">Total: <?php echo $x; ?></td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:left;"> Uso: <span style="font-family:Arial; font-size:12px;"> <?php echo ucfirst(strtolower($nombre_uso)); ?> </span> </td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:left;"> Origen: <span style="font-family:Arial; font-size:12px;"> <?php echo $origen; ?> </span> </td>
			    </tr>
			    <tr>
			    	<td colspan="2"> <br/> </td>
			    </tr>
			    <tr>
			    	<td colspan="2"> <br/> </td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family:Arial; text-align:center;"> 
			    		-------------------------------------------------  
				   		<br/>
				   		Responsable
				   		<br/>
				   		<?php echo ucwords(strtolower($nombre_empleado)); ?>
				   	</td>
				</tr>
			</table>
		</div>
			<p>&nbsp;&nbsp;</p>
	</div>
</body>
</html>
<!-- ************************************************************************************************ -->