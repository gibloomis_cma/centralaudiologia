<?php
//Exportar datos de php a Excel
	header("Content-Type: application/vnd.ms-excel");
	header("Expires: 0");
	header("Cache-ConTRol: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=Reporte de Refacciones - ".date("d-m-y").".xls");
?>
<HTML LANG="es">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>::. Exportacion de Datos .::</TITLE>
<style>
	#titulo{
		 background-color:#06C;
		 color:#fff;
		 font:20px normal Arial, Helvetica, sans-serif; 
		 font-weight:bold;
		 padding:5px 10px;
	}
	#subtitulo{
		background-color:#069; 
		color:#FFF;
		font:16px normal Arial, Helvetica, sans-serif; 
		font-weight:bold;
		padding:5px 10px;
	}
</style>
</head>
<body>
<?php
	include('config.php');
	$refacciones=mysql_query('SELECT subcategoria, proveedor, codigo, concepto, precio_venta, precio_compra,
								inventario_minimo, inventario_maximo, refacciones.descripcion as descripcion
								FROM subcategorias_productos, proveedores, base_productos, refacciones									 
								WHERE subcategorias_productos.id_subcategoria = refacciones.id_subcategoria
								AND subcategorias_productos.id_subcategoria = base_productos.id_subcategoria
								AND refacciones.id_proveedor = proveedores.id_proveedor
								AND refacciones.id_refaccion = base_productos.id_articulo
								AND base_productos.id_categoria = 1 
								ORDER BY subcategorias_productos.id_subcategoria')
								or die(mysql_error());
?>
<TABLE BORDER=1 align="center" CELLPADDING=1 CELLSPACING=1>
    <TR>
        <TH colspan="9" align="center" id="titulo">
        	Reporte de Refacciones
   		</TH>
    </TR><TR>
        <TH id="subtitulo">Subcategoria</TH>
        <TH id="subtitulo">Proveedor</TH>
        <TH id="subtitulo">Codigo</TH>
        <TH id="subtitulo">Concepto</TH>
        <TH id="subtitulo">Precio Venta</TH>
        <TH id="subtitulo">Precio Compra</TH>
        <TH id="subtitulo">Minimo</TH>
        <TH id="subtitulo">Maximo</TH>
        <TH id="subtitulo">Descripcion</TH>
    </TR>
<?php
	while($row = mysql_fetch_array($refacciones)) {
		$subcategoria=$row['subcategoria'];
		$proveedor=$row["proveedor"];
		$codigo=$row["codigo"];
		$concepto=$row["concepto"];		
		$precio_venta=$row['precio_venta'];
		$precio_compra=$row['precio_compra'];
		$inventario_minimo=$row['inventario_minimo'];
		$inventario_maximo=$row['inventario_maximo'];
		$descripcion=$row['descripcion'];
		printf("<TR>
					<TD>".$subcategoria."</TD>
					<TD>".$proveedor."</TD>
					<TD style='text-align:center'>".$codigo."</TD>
					<TD>".$concepto."</TD>
					<TD style='text-align:right'>$".number_format($precio_venta,2)."</TD>
					<TD style='text-align:right'>$".number_format($precio_compra,2)."</TD>
					<TD style='text-align:right'>".$inventario_minimo."</TD>
					<TD style='text-align:right'>".$inventario_maximo."</TD>
					<TD style='text-align:justify'>".$descripcion."</TD>
				</TR>");
	}
	mysql_free_result($refacciones);
	/*mysql_close($IdConexion);  //Cierras la Conexión*/
?>
</table>
</body>
</html>
