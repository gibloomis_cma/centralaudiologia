<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Materiales </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Materiales
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO 1 -->
							<div class="area_contenido1">
								<br/><br/>
								<?php 
									include("config.php");
									$id_catalogo = $_GET['id_catalogo'];
									$query_estilo_consultado = mysql_query("SELECT estilo,foto
																			FROM estilos,catalogo
																			WHERE estilos.id_estilo = catalogo.id_estilo
																			AND id_catalogo = '$id_catalogo'") or die (mysql_error());
									$row_estilo = mysql_fetch_array($query_estilo_consultado);
									$estilo = $row_estilo['estilo'];
									$imagen = $row_estilo['foto'];
									
									$query_materiales = mysql_query("SELECT id_material,material
																	 FROM materiales
																	 ORDER BY id_material ASC") or die (mysql_error());
								?>
								<div class="titulos"> Estilo </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td> <label class="textos"> Nombre del Estilo: </label> </td>
											<td>  &nbsp;<label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $estilo; ?> </label> </td>
											<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
											<td> <img width="100" height="100" src="../moldes/estilos/<?php echo $imagen; ?>"/> </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Agregar Material </div><!-- FIN DIV TITULOS -->
								<center>
								<form name="form_agregar_material_estilo" id="form_agregar_material_estilo" method="post" action="procesa_agregar_materiales_estilo.php">
									<table id="tabla_informacion">
										<tr>
											<td> <input type="hidden" name="txt_id_catalogo" id="txt_id_catalogo" value="<?php echo $id_catalogo; ?>"/> </td>
										</tr>
										<tr>
											<td id="alright"> <label class="textos"> Nombre del Material: </label> </td>
											<td id="alleft">
												<select name="material_estilo">
													<option value="0"> --- Seleccione Material --- </option>
												<?php
													while( $row_materiales = mysql_fetch_array($query_materiales) )
													{
														$id_material = $row_materiales['id_material'];
														$material = $row_materiales['material'];
												?>
													<option value="<?php echo $id_material; ?>"> <?php echo ucwords(strtolower($material)); ?> </option>
												<?php
													}
												?>
												</select>
											</td>
										</tr>
										<br/>
									</table>
									<div id="boton_Guardar">
										<p align="right">
											<input type="submit" name="accion" value="Guardar" title="Guardar" class="fondo_boton" />
										</p>
									</div>
									<br/><br/><br/>
								</form>
								</center>
							</div><!-- FIN DIV AREA CONTENIDO -->
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>

</html>