<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Institucion</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
</head>
<body> 
<style>
table tr td {
color: #6D6D6D;
text-align: center;
font-weight: bold;
font-size: 12px;
font-family: Tahoma, Geneva, sans-serif;
}
</style>
<div id="contenido_columna2">
	<div class="contenido_pagina">
    	<div class="fondo_titulo1">
        	<div class="categoria">
            	Instituciones
            </div><!-- Fin DIV categoria -->
        </div><!--Fin de fondo titulo-->
        <div class="buscar2">
		<?php
            // SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS 
            include("config.php");								
            if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                $filtro = $_POST['filtro'];
                $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                    FROM instituciones
                                                    WHERE razon_social LIKE '%".$filtro."%' 
                                                    OR institucion LIKE '%".$filtro."%' 
													ORDER BY institucion");														
                $row_busqueda = mysql_fetch_array($res_busqueda);
                $res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
            }else{
				$res2="";
			}
        ?>              
            <form name="busqueda" method="post" action="agregar_instituciones.php">
                <label><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
        </div><!-- Fin de la clase buscar2 --> 
        <br /> 
		<div class="area_contenido2">
                <center>
                <table>                	
                    <tr>
                        <th>Nombre</th>
                        <th colspan="2">Razon social</th>
                    </tr>
            <?php
                // QUERY QUE OBTIENE LOS PROVEEDORES
                if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                    $filtro = $_POST['filtro'];
                    $consulta_instituciones = mysql_query("SELECT DISTINCT(id_institucion), 
                                                        razon_social, institucion 
                                                        FROM instituciones
                                                        WHERE razon_social LIKE '%".$filtro."%' 
                                                        OR institucion LIKE '%".$filtro."%' 
														ORDER BY institucion");														
                            
                }else{
					$consulta_instituciones = mysql_query("SELECT DISTINCT(id_institucion), 
														razon_social, institucion 
														FROM instituciones 
														ORDER BY institucion");
                }
				$n_instituciones=0;
                while($row_instituciones = mysql_fetch_array($consulta_instituciones)){					
                    $id_institucion = $row_instituciones["id_institucion"];
                    $institucion = $row_instituciones["institucion"];
                    $nombre = $row_instituciones["razon_social"];
					$n_instituciones++;
            ?>
                    <tr>
                        <td>
                            <?php echo $institucion; ?>
                        </td>
                        <td> <?php echo $nombre; ?> </td>
                        <td id="alright">
                        	<a href="detalles_institucion.php?id_institucion=<?php echo $id_institucion; ?>">
                            	<img src="../img/modify.png" title="<?php echo $nombre; ?>" />
                            </a>
                        </td>
                    </tr>
            <?php		
                }
				if($n_instituciones==0){
			?>
					<tr>
                        <td style="text-align:center;" colspan="3">
                        	<label class="textos">"No hay instituciones registradas"</label>
                        </td>
                    </tr>            
            <?php
				}
            ?>
                </table>
			<form action="procesa_agregar_instituciones.php" method="post" 
            name="form_agregar_instituciones" onsubmit="return validarAgregarInstituciones()">
            <table>
            	<tr>
                	<th colspan="4">Nueva Institucion</th>
                </tr><tr>  
  					<td style="text-align:right"><label class="textos">Nombre: </label></td>
			        <td><input name="institucion" type="text" size="30" /></td>
                    <td style="text-align:right"><label class="textos">Razon Social: </label></td>
                    <td><input name="razon_social" type="text" size="30" /></td>
        		</tr><tr>
                	<td style="text-align:right"><label class="textos">Calle: </label></td>
                    <td><input name="calle" type="text" size="30" /></td>
                    <td style="text-align:right">
                        <label class="textos">Numero</label>
                    </td><td>
                    	<label class="textos">Ext.:</label>
                        <input name="num_exterior" type="text" size="3" onkeypress="return _soloNumeros(event);" maxlength="4"/>
                        <label class="textos">Int.: </label>
                        <input name="num_interior" type="text" size="3" maxlength="4"/>
                    </td>
                </tr><tr>
                	<td style="text-align:right"><label class="textos">Colonia:</label></td>
                    <td><input name="colonia" type="text" size="30" /></td>
                    <td style="text-align:right"><label class="textos">Codigo Postal: </label></td>
                    <td><input name="codigo_postal" type="text" size="4" onkeypress="return _soloNumeros(event);" maxlength="5"/></td>
                </tr><tr>
                	<td style="text-align:right"><label class="textos">Estado: </label></td>
                    <td>
                    	<select id="id_estado" name="id_estado">
                        	<option value="0" selected="selected"> -- Estado -- </option>
							<?php
                                include("php/config.php");
                                $consulta_estados = mysql_query("SELECT id_estado,estado FROM estados");
                                while( $row3 = mysql_fetch_array($consulta_estados))
                                { 
                                    $id_estado = $row3["id_estado"];
                                    $estado = $row3["estado"];	
                            ?>
                                    <option value="<?php echo $id_estado; ?>"> <?php echo utf8_encode($estado); ?> </option>
                            <?php
                                }
                            ?>
				       	</select>
                    </td>
                    <td style="text-align:right"><label class="textos">Ciudad: </label></td>                    
                    <td>
                        <select name="id_municipio2" id="id_municipio"/>
                            <option value="0" selected="selected">--Municipio--</option>
                        </select>
                    </td>
                </tr><tr>
                	<td style="text-align:right"><label class="textos">Descuento: </label></td>
                    <td><input name="descuento" type="text" size="2" onkeypress="return _soloNumeros(event);" maxlength="2"/><label class="textos"> %</label></td>
                    <td style="text-align:right"><label class="textos">Costo por aparato: </label></td>
                    <td><input name="costo_aparato" type="text" size="11" onkeypress="return _soloNumeros(event);"/></td>
                </tr><tr>
                	<td style="text-align:right"><label class="textos">RFC: </label></td>
                    <td><input name="rfc" type="text" maxlength="13"/></td>
                </tr><tr>
                	<td style="text-align:right"><label class="textos">Poliza: </label></td>
                    <td colspan="2"><textarea name="poliza" rows="5" cols="30"></textarea></td>
                    <td style="text-align:right"><input name="guarda" type="submit" value="Guardar" class="fondo_boton" /></td>
                </tr>
			</table>             
			</form>
            </center>
		</div>
	</div>
</div>
</body>
</html>