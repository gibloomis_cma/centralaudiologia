<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nuevo Estudio</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/style_estudio_nuevo.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<?php 
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha = date("d/m/Y");
?>
<!------------------ Script para Tabs ------------------------>
<script src="../js/jquery.min.js" type="text/javascript"></script>
<script>
$(document).ready(function()
{
 
	$("ul.tabs li").click(function()     //cada vez que se hace click en un li
       {
		$("ul.tabs li").removeClass("active"); //removemos clase active de todos
		$(this).addClass("active"); //añadimos a la actual la clase active
		$(".tab_content").hide(); //escondemos todo el contenido
 
		var content = $(this).find("a").attr("href"); //obtenemos atributo href del link
		$(content).fadeIn(); // mostramos el contenido
		return false; //devolvemos false para el evento click
	});
});
</script>
<!------------------ Fin de Script Tabs--------------------------->
<script>
	$(document).ready(function(){
		$(".exterminados").click(function(){
			$(this).children().show();
		});
		
		
	});
</script>
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="../js/lightbox.js"></script>
<script language="javascript">
<!-- Se abre el comentario para ocultar el script de navegadores antiguos

function muestraReloj()
{
// Compruebo si se puede ejecutar el script en el navegador del usuario
if (!document.layers && !document.all && !document.getElementById) return;
// Obtengo la hora actual y la divido en sus partes
var fechacompleta = new Date();
var horas = fechacompleta.getHours();
var minutos = fechacompleta.getMinutes();
var segundos = fechacompleta.getSeconds();
var mt = "AM";
// Pongo el formato 12 horas
if (horas >= 12) {
mt = "PM";
horas = horas - 12;
}
if (horas == 0) horas = 12;
// Pongo minutos y segundos con dos dígitos
if (minutos <= 9) minutos = "0" + minutos;
if (segundos <= 9) segundos = "0" + segundos;
// En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
 cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
 // Escribo el reloj de una manera u otra, según el navegador del usuario


if (document.layers) {
document.layers.spanreloj.document.write(cadenareloj);
document.layers.spanreloj.document.close();
}
else if (document.all) spanreloj.innerHTML = cadenareloj;
else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
// Ejecuto la función con un intervalo de un segundo
setTimeout("muestraReloj()", 1000);
}

function muestra_oculta(id){
	if (document.getElementById){ //se obtiene el id
		var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
		el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
	}
}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
	muestra_oculta('contenido_a_mostrar');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
}	
// Fin del script -->
</script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">
			<div id="contenido_columna2">
				<div class="contenido_pagina">
					<div class="fondo_titulo1">
        				<div class="categoria">
        					Estudios
        				</div>
                   	</div><!--Fin de fondo titulo-->
                    <div class="area_contenido1">
	<?php
				include("config.php");
				include("metodo_cambiar_fecha.php");		
				$id_cliente = $_GET["id_paciente"];
				$consulta_datos_paciente = mysql_query("SELECT * FROM ficha_identificacion
																	WHERE id_cliente=".$id_cliente) 
																	or die(mysql_error());
				$row = mysql_fetch_array($consulta_datos_paciente);
				$nombre = $row["nombre"];
				$paterno = $row["paterno"];
				$materno = $row["materno"];
				if($_SESSION['folio']==""){
					$folio_num_estudio_max = mysql_query('SELECT MAX(folio_num_estudio+1) 
																	FROM folios_num_estudios')or die(mysql_error());
					$row_folio_num_estudio_max = mysql_fetch_array($folio_num_estudio_max);
					$_SESSION['folio'] = $row_folio_num_estudio_max['MAX(folio_num_estudio+1)'];
					if($_SESSION["folio"] == NULL){
						$_SESSION["folio"] = 1;
					}
				}
	?>
            		   <div class="contenido_proveedor" style="margin-bottom:4px; margin-top:4px;">
                    	<form name="forma1" action="guardar_historial_clinico.php" method="post">
                        <fieldset>
                        <legend><label class="textos">
                        	<a href="#" onClick="muestra_oculta('contenido_a_mostrar')" title="">Datos del Estudio</a>
                        </label></legend>
                        <div id="contenido_a_mostrar">      	
            				<label class="textos">Folio del Estudio: </label><?php echo $_SESSION["folio"]; ?>
                            <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"]; ?>" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="textos">Nombre del paciente: </label><?php echo $nombre." ".$paterno." ".$materno; ?>
                            <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="textos">Fecha: </label><?php echo $fecha; ?>
                            <input name="fecha_historial_clinico" type="hidden" value="<?php echo $fecha; ?>" />
                       	<br /><br />
                            <label class="textos">Elaboro: </label>
                            <select name="elaboro">
                                <option value="0">Seleccione</option>
	<?php
              	$consulta_nombre_doctor = mysql_query("SELECT nombre, paterno, materno, id_empleado
                                                    		FROM empleados") 
                                                    		or die(mysql_error());
              	while($row3 = mysql_fetch_array($consulta_nombre_doctor)){
					$id_empleado = $row3["id_empleado"];
					$nombre = $row3["nombre"];
					$paterno = $row3["paterno"];
					$materno = $row3["materno"];
  	?>
                                <option value="<?php echo $id_empleado?>">
                                    <?php echo $nombre." ".$paterno." ".$materno;?>
                                </option>
    <?php
              	}
    ?>
                            </select>
                      	<br /><br />
                            <label class="textos">Notas: </label><br />
                            <textarea name="notas" cols="60" rows="2"></textarea>
                       	<p align="right">
                            <input name="accion" type="submit" value="Guardar Estudio" class="fondo_boton" />
                        </p>
                        </div><!--Fin de contenido a mostrar-->
                        </fieldset>
                        </form>
</div><!--Fin de contenido proveedor-->
                        <div class="contenido_proveedor" style="margin-right:18px; margin-left:18px;">
                        	<ul class="tabs">
                                <li><a href="#tab1">Otoscopia</a></li>
                                <li><a href="#tab2">Logo Audiometria</a></li>
                                <li><a href="#tab3">Timpanometría</a></li>
                                <li><a href="#tab4">Reflejos Acústicos</a></li>
                                <li><a href="#tab5">Emisiones Otoacusticas</a></li>
                                <li><a href="#tab6">Audiometria</a></li>
                            </ul>
         
               				<div class="tab_container">
    <!-------------------Otoscopia----------------------------->
    <?php 
				$consulta_videotoscopia = mysql_query("SELECT COUNT(id_cliente) FROM videotoscopia 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_videotoscopia = mysql_fetch_array($consulta_videotoscopia);
				$resultado_videotoscopia = $row_videotoscopia["COUNT(id_cliente)"];
				if($resultado_videotoscopia == 0){
	?>
                    			<div id="tab1" class="tab_content">
                    			<form name="forma1" method="post" action="guardar_videotoscopia.php" enctype="multipart/form-data">
                    			<center>
                                    <label style="font-size:16px; font-weight:bold;">OTOSCOPIA</label>
                                    <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                                    <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                                    <table  border= "1" style="border-collapse:collapse; border-style:double;">
                                    <tr>
                                        <th colspan="2" width="350px">Oido Izquierdo</th>
                                        <th colspan="2" width="350px">Oido Derecho</th>
                                    </tr>
                                    <tr>
                                        <td align="right">Canal auditivo: </td>
                                        <td><textarea cols="28" rows="2" name="canal_izquierdo"></textarea></td>
                                        <td align="right">Canal auditivo: </td>
                                        <td><textarea cols="28" rows="2" name="canal_derecho"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="170px">Membrana timpanica: </td>
                                        <td><textarea cols="28" rows="2" name="membrana_izquierda"></textarea></td>
                                        <td align="right" width="170px">Membrana timpanica: </td>
                                        <td><textarea cols="28" rows="2" name="membrana_derecha"></textarea></td>
                                    </tr>	
                                    </table>
                            	</center>
                            	<br />
                                    <label class="textos">Observaciones: </label><br />
                                    <textarea name="observaciones" cols="70" rows="2"></textarea>
                                <p align="right">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="100000">
                                    <input name="accion" type="submit" class="fondo_boton" value="Guardar Otoscopia" />
                                </p>
                                    <label class="textos">Cargar archivo de Otoscopia: </label>
                                    <input name="userfile" type="file" id="userfile" style="height:23px"/>
                                    <input name="accion" type="submit" value="Guardar" class="fondo_boton"/>   
                            	</form>
                            	</div>
 	<?php
				}else{
					$consultame_todo_videotoscopia = mysql_query("SELECT * FROM videotoscopia
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_videotoscopia = mysql_fetch_array($consultame_todo_videotoscopia);
					$canal_izquierdo = $row_consulta_todo_videotoscopia["canal_izquierdo"];
					$canal_derecho = $row_consulta_todo_videotoscopia["canal_derecho"];
					$membrana_derecha = $row_consulta_todo_videotoscopia["membrana_derecha"];
					$membrana_izquierda = $row_consulta_todo_videotoscopia["membrana_izquierda"];
					$observaciones_videotoscopia = $row_consulta_todo_videotoscopia["observaciones"];
					
					$accion = "Editar Otoscopia";
	?>
                                <div id="tab1" class="tab_content">
                                <form name="forma1" method="post" action="guardar_videotoscopia.php" enctype="multipart/form-data">
                                <center>
                                    <label style="font-size:16px; font-weight:bold;">VIDEOTOSCOPIA</label>
                                    <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                                    <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                                    <table  border= "1" style="border-collapse:collapse; border-style:double;">
                                    <tr>
                                        <th colspan="2" width="350px">Oido Izquierdo</th>
                                        <th colspan="2" width="350px">Oido Derecho</th>
                                    </tr>
                                    <tr>
                                        <td align="right">Canal auditivo: </td>
                                        <td>
                                        	<textarea cols="28" rows="2" name="canal_izquierdo"><?php echo $canal_izquierdo; ?></textarea></td>
                                        <td align="right">Canal auditivo: </td>
                                        <td><textarea cols="28" rows="2" name="canal_derecho"><?php echo $canal_derecho; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="170px">Membrana timpanica: </td>
                                        <td><textarea cols="28" rows="2" name="membrana_izquierda"><?php echo $membrana_izquierda; ?></textarea></td>
                                        <td align="right" width="170px">Membrana timpanica: </td>
                                        <td><textarea cols="28" rows="2" name="membrana_derecha"><?php echo $membrana_derecha; ?></textarea></td>
                                    </tr>	
                                    </table>
                                </center>
                                <br />
                                	<label class="textos">Observaciones: </label><br />
                                	<textarea name="observaciones" cols="70" rows="2"><?php echo $observaciones_videotoscopia; ?></textarea>
                                <p align="right">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="100000">
                                    <input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                                </p>
                                    <label class="textos">Cargar archivo de Otoscopia: </label>
                                    <input name="userfile" type="file" id="userfile" style="height:23px" />
                                    <input name="accion" type="submit" value="Guardar" class="fondo_boton"/>   
                                <br />
                                <div class="set">
                                <ul>
  	<?php
					$consulta_imagenes = mysql_query("SELECT * FROM imagenes_videotoscopia
																WHERE id_estudio=".$_SESSION["folio"])
																or die(mysql_error());
					while($row_imagenes = mysql_fetch_array($consulta_imagenes)){
						$id_identificador = $row_imagenes["id_identificador"];
						$id_estudio = $row_imagenes["id_estudio"];
						$tipo = $row_imagenes["tipo"];
	?>
                   				<div class="single">
                      				<li><a href="videotoscopia/<?php echo $id_estudio."_".$id_identificador.".".$tipo; ?>" rel="lightbox[plants]">
											<?php echo $id_estudio."_".$id_identificador.".".$tipo; ?>
                             			</a>
                      				</li>
                    			</div><!--Fin de class single-->
	<?php
					}
	?>
                                </ul>
                                </div><!--Fin de class set-->
                                </form>
                                </div>

	<?php
                }
	?>
    <!---------------------------Logo Audiometria--------------------------------------->
    <?php 
				$consulta_logo_audiometria = mysql_query("SELECT COUNT(id_cliente) FROM logo_audiometria 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_logo_audiometria = mysql_fetch_array($consulta_logo_audiometria);
				$resultado_logo_audiometria = $row_logo_audiometria["COUNT(id_cliente)"];
				if($resultado_logo_audiometria == 0){
	?>
                    <div id="tab2" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_logo_audiometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">LOGO AUDIOMETRIA</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />  
                    </center>
                    <table>
                    	<tr>
                    		<th></th>
                            <th>IZQ</th>
                            <th>DER</th>
                    	</tr>
                    	<tr>
                        	<td style="text-align:right;">SRT Umbral de percepción acústica:</td>
                            <td><input name="umbral_percepcion_izq" type="text" />dB</td>
                            <td><input name="umbral_percepcion_der" type="text" />dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">MCL Nivel de máxima comodidad:</td>
                            <td><input name="nivel_maxima_izq" type="text" />dB</td>
                            <td><input name="nivel_maxima_der" type="text" />dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">UCL Nivel de incomodidad:</td>
                            <td><input name="nivel_incomodidad_izq" type="text" />dB</td>
                            <td><input name="nivel_incomodidad_der" type="text" />dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Método:</td>
                            <td colspan="2"><input name="metodo" type="text" size="50" maxlength="50" /></td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Cooperación:</td>
                            <td colspan="2"><input name="cooperacion" type="text" size="50" maxlength="50" /></td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Enmascarador:</td>
                            <td colspan="2"><input name="enmascarador" type="text" size="48" maxlength="48" />dB</td>
                        </tr>
                        <tr>
                        	<th colspan="3">DISCRIMINACION EN SILENCIO</th>
                        </tr>
                        <tr>
                    		<th></th>
                            <th>IZQ</th>
                            <th>DER</th>
                    	</tr>
                        <tr>
                        	<td style="text-align:right;">Nivel de habla:</td>
                            <td><input name="nivel_habla_izq" type="text" />dB</td>
                            <td><input name="nivel_habla_der" type="text" />dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Porcentaje:</td>
                            <td><input name="porcentaje_izq" type="text" />%</td>
                            <td><input name="porcentaje_der" type="text" />%</td>
                        </tr>
                    </table>
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones_logo" cols="70" rows="2"></textarea>
                    <p align="right">
                        <input name="accion" type="submit" class="fondo_boton" value="Guardar Logo Audiometria" />
                    </p>  
                    </form>           
                    </div>
	<?php
				}else{
					$consultame_todo_logo_audiometria = mysql_query("SELECT * FROM logo_audiometria
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_logo_audiometria = mysql_fetch_array($consultame_todo_logo_audiometria);
					$umbral_percepcion_izq = $row_consulta_todo_logo_audiometria["umbral_percepcion_izq"];
					$umbral_percepcion_der = $row_consulta_todo_logo_audiometria["umbral_percepcion_der"];
					$nivel_maxima_izq = $row_consulta_todo_logo_audiometria["nivel_maxima_comodidad_izq"];
					$nivel_maxima_der = $row_consulta_todo_logo_audiometria["nivel_maxima_comodidad_der"];
					$nivel_incomodidad_izq = $row_consulta_todo_logo_audiometria["nivel_incomodidad_izq"];
					$nivel_incomodidad_der = $row_consulta_todo_logo_audiometria["nivel_incomodidad_der"];
					$metodo = $row_consulta_todo_logo_audiometria["metodo"];
					$cooperacion = $row_consulta_todo_logo_audiometria["cooperacion"];
					$enmascarador = $row_consulta_todo_logo_audiometria["enmascarador"];
					$nivel_habla_izq = $row_consulta_todo_logo_audiometria["nivel_habla_izq"];
					$nivel_habla_der = $row_consulta_todo_logo_audiometria["nivel_habla_der"];
					$porcentaje_izq = $row_consulta_todo_logo_audiometria["porcentaje_izq"];
					$porcentaje_der = $row_consulta_todo_logo_audiometria["porcentaje_der"];
					$observaciones_logo_audiometria = $row_consulta_todo_logo_audiometria["observaciones"];
					$accion = "Editar Logo Audiometria";
    ?>
    				<div id="tab2" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_logo_audiometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">LOGO AUDIOMETRIA</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />  
                    </center>
                    <table>
                    	<tr>
                    		<th></th>
                            <th>IZQ</th>
                            <th>DER</th>
                    	</tr>
                    	<tr>
                        	<td style="text-align:right;">SRT Umbral de percepción acústica:</td>
                            <td><input name="umbral_percepcion_izq" type="text" value="<?php echo $umbral_percepcion_izq; ?>"/>dB</td>
                            <td><input name="umbral_percepcion_der" type="text" value="<?php echo $umbral_percepcion_der; ?>"/>dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">MCL Nivel de máxima comodidad:</td>
                            <td><input name="nivel_maxima_izq" type="text" value="<?php echo $nivel_maxima_izq; ?>"/>dB</td>
                            <td><input name="nivel_maxima_der" type="text" value="<?php echo $nivel_maxima_der; ?>"/>dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">UCL Nivel de incomodidad:</td>
                            <td><input name="nivel_incomodidad_izq" type="text" value="<?php echo $nivel_incomodidad_izq; ?>"/>dB</td>
                            <td><input name="nivel_incomodidad_der" type="text" value="<?php echo $nivel_incomodidad_der; ?>"/>dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Método:</td>
                            <td colspan="2"><input name="metodo" type="text" size="50" maxlength="50" value="<?php echo $metodo; ?>"/></td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Cooperación:</td>
                            <td colspan="2"><input name="cooperacion" type="text" size="50" maxlength="50" value="<?php echo $cooperacion; ?>"/></td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Enmascarador:</td>
                            <td colspan="2"><input name="enmascarador" type="text" size="48" maxlength="48" value="<?php echo $enmascarador; ?>"/>dB</td>
                        </tr>
                        <tr>
                        	<th colspan="3">DISCRIMINACION EN SILENCIO</th>
                        </tr>
                        <tr>
                    		<th></th>
                            <th>IZQ</th>
                            <th>DER</th>
                    	</tr>
                        <tr>
                        	<td style="text-align:right;">Nivel de habla:</td>
                            <td><input name="nivel_habla_izq" type="text" value="<?php echo $nivel_habla_izq; ?>"/>dB</td>
                            <td><input name="nivel_habla_der" type="text" value="<?php echo $nivel_habla_der; ?>"/>dB</td>
                        </tr>
                        <tr>
                        	<td style="text-align:right;">Porcentaje:</td>
                            <td><input name="porcentaje_izq" type="text" value="<?php echo $porcentaje_izq; ?>"/>%</td>
                            <td><input name="porcentaje_der" type="text" value="<?php echo $porcentaje_der; ?>"/>%</td>
                        </tr>
                    </table>
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones_logo" cols="70" rows="2"><?php echo $observaciones_logo_audiometria; ?></textarea>
                    <p align="right">
                        <input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                    </p>  
                    </form>           
                    </div>
    <?php
				}
	?>
   	<!----------------------------Timpanometria------------------------------------>
          	<?php 
				$consulta_timpanometria = mysql_query("SELECT COUNT(id_cliente) FROM timpanometria 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_timpanometria = mysql_fetch_array($consulta_timpanometria);
				$resultado_timpanometria = $row_timpanometria["COUNT(id_cliente)"];
				if($resultado_timpanometria == 0)
				{
			?>
                    <div id="tab3" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_timpanometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">TIMPANOMETRIA</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    </center>
                    	<label class="textos">Frecuencia Tonal de Sonda(Hz)</label>
                   		<select name="frecuencia">
                            <option value="">Seleccione</option>
                            <option value="220">220Hz</option>
                            <option value="226">226Hz</option>
                            <option value="678">678Hz</option>
                            <option value="1000">1000Hz</option>
                    	</select>
                    <br /><br />
                   
                    	<table border="0" style="border-collapse:collapse; border-style:double;">
                    		<tr>
                            	<th></th>
                                <th  width="80px">Oido Derecho</th>
                                <th  width="80px">Oido Izquierdo</th>
                                <th></th>
                            </tr>
                    		<tr>
                            	<td align="right">Tipo</td>
                                <td>
                                	<select name="tipo_derecho">
    									<option value="">Seleccione</option>
                                        <option value="A">A</option>   
                                        <option value="Ad">Ad</option> 
                                        <option value="As">As</option> 
                                        <option value="B">B</option> 
                                        <option value="C">C</option>                          
	                               	</select>
                                </td>
                                <td align="right">
                                	<select name="tipo_izquierdo">
                                        <option value="">Seleccione</option>
                                        <option value="A">A</option>   
                                        <option value="Ad">Ad</option> 
                                        <option value="As">As</option> 
                                        <option value="B">B</option> 
                                        <option value="C">C</option> 
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                           	  <td align="right">Admitancia Estática(ml.)</td>
                              <td><input name="admitancia_derecha" type="text" size="5" maxlength="5" /></td>
                              <td align="right"><input name="admitancia_izquierda" type="text" size="5" maxlength="5" /></td>
                              <td>0 - 3</td>
                            </tr>
                            <tr>
                           	  <td align="right">Presión Máxima(daPa)</td>
                              <td><input name="presion_maxima_derecha" type="text" size="4" maxlength="4" /></td>
                              <td  align="right"><input name="presion_maxima_izquierda" type="text" size="4" maxlength="4" /></td>
                              <td>-300 - +300</td>
                            </tr>
                            <tr>
                           	  <td align="right">Gradiente Timpanométrico(ml.)</td>
                              <td><input name="gradiente_timpanometrico_derecha" type="text" size="5" maxlength="5" /></td>
                              <td  align="right"><input name="gradiente_timpanometrico_izquierda" type="text" size="5" maxlength="5" /></td>
                              <td>0 - 4</td>
                            </tr>
                             <tr>
                           	  <td align="right">Volumen Equivalente(ml.)</td>
                              <td><input name="volumen_derecha" type="text" size="5" maxlength="5" /></td>
                              <td align="right"><input name="volumen_izquierda" type="text" size="5" maxlength="5" /></td>
                              <td>0 - 4</td>
                            </tr>
                    	</table>
                   	
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones_timpanometria" cols="70" rows="2"></textarea>
                   	<p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="Guardar Timpanometria" />
                    </p>
                    </form>	                
                    </div>
  	<?php 
				}else{
					$consultame_todo_timpanometria = mysql_query("SELECT * FROM timpanometria
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_timpanometria = mysql_fetch_array($consultame_todo_timpanometria);
					$frecuencia = $row_consulta_todo_timpanometria["frecuencia"];
					$tipo_derecho = $row_consulta_todo_timpanometria["tipo_derecho"];
					$tipo_izquierdo = $row_consulta_todo_timpanometria["tipo_izquierdo"];
					$admitancia_derecha = $row_consulta_todo_timpanometria["admitancia_derecha"];
					$admitancia_izquierda = $row_consulta_todo_timpanometria["admitancia_izquierda"];
					$presion_maxima_derecha = $row_consulta_todo_timpanometria["presion_maxima_derecha"];
					$presion_maxima_izquierda = $row_consulta_todo_timpanometria["presion_maxima_izquierda"];
					$gradiente_timpanometrico_derecha = $row_consulta_todo_timpanometria["gradiente_timpanometrico_derecha"];
					$gradiente_timpanometrico_izquierda = $row_consulta_todo_timpanometria["gradiente_timpanometrico_izquierda"];
					$volumen_derecha = $row_consulta_todo_timpanometria["volumen_derecha"];
					$volumen_izquierda = $row_consulta_todo_timpanometria["volumen_izquierda"];
					$observaciones_timpanometria = $row_consulta_todo_timpanometria["observaciones"];
					$accion = "Editar Timpanometria";
	?>
    				<div id="tab3" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_timpanometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">TIMPANOMETRIA</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    </center>
                    	<label class="textos">Frecuencia Tonal de Sonda(Hz)</label>
                   		<select name="frecuencia">
   	<?php
						$frecuencias[0] = "";
						$frecuencias[1] = "220";
						$frecuencias[2] = "226";
						$frecuencias[3] = "678";
						$frecuencias[4] = "1000";						 
						for($i=0; $i<5; $i++){
							if($frecuencia == $frecuencias[$i]){							
   	?>                                                           
                			<option value="<?php echo $frecuencia; ?>" selected="selected">
                            	<?php echo $frecuencia; ?>
                          	</option>
   	<?php 
                            }else{
    ?>                            
                            <option value="<?php echo $frecuencias[$i]; ?>">
                                <?php echo $frecuencias[$i]; ?>
                            </option>
  	<?php
                            }
                    	}
	?>
                    	</select>
                    <br /><br />
                   
                    	<table border= "0" style="border-collapse:collapse; border-style:double;">
                    		<tr>
                            	<th></th>
                                <th  width="80px">Oido Derecho</th>
                                <th  width="80px">Oido Izquierdo</th>
                                <th></th>
                            </tr>
                    		<tr>
                            	<td align="right">Tipo</td>
                                <td>
                                	<select name="tipo_derecho">
   	<?php
									$tipos[0] = "";
									$tipos[1] = "A";
									$tipos[2] = "Ad";
									$tipos[3] = "As";
									$tipos[4] = "B";
									$tipos[5] = "C";						 
									for($i=0; $i<6; $i++){
										if($tipo_derecho == $tipos[$i]){							
   	?>                                                           
                                        <option value="<?php echo $tipo_derecho; ?>" selected="selected">
                                            <?php echo $tipo_derecho; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $tipos[$i]; ?>">
                                            <?php echo $tipos[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                    				</select>                          

                                </td>
                                <td align="right">
                                	<select name="tipo_izquierdo">
   	<?php
									$tipos[0] = "";
									$tipos[1] = "A";
									$tipos[2] = "Ad";
									$tipos[3] = "As";
									$tipos[4] = "B";
									$tipos[5] = "C";						 
									for($i=0; $i<6; $i++){
										if($tipo_izquierdo == $tipos[$i]){							
   	?>                                                           
                                        <option value="<?php echo $tipo_izquierdo; ?>" selected="selected">
                                            <?php echo $tipo_izquierdo; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $tipos[$i]; ?>">
                                            <?php echo $tipos[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                    				</select> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                           	  <td align="right">Admitancia Estática(ml.)</td>
                              <td><input name="admitancia_derecha" type="text" size="5" maxlength="5" value="<?php echo $admitancia_derecha?>"/></td>
                              <td align="right"><input name="admitancia_izquierda" type="text" size="5" maxlength="5" value="<?php echo $admitancia_izquierda; ?>"/></td>
                              <td>0 - 3</td>
                            </tr>
                            <tr>
                           	  <td align="right">Presión Máxima(daPa)</td>
                              <td><input name="presion_maxima_derecha" type="text" size="4" maxlength="4" value="<?php echo $presion_maxima_derecha; ?>"/></td>
                              <td  align="right"><input name="presion_maxima_izquierda" type="text" size="4" maxlength="4" value="<?php echo $presion_maxima_izquierda; ?>"/></td>
                              <td>-300 - +300</td>
                            </tr>
                            <tr>
                           	  <td align="right">Gradiente Timpanométrico(ml.)</td>
                              <td><input name="gradiente_timpanometrico_derecha" type="text" size="5" maxlength="5" value="<?php echo $gradiente_timpanometrico_derecha; ?>"/></td>
                              <td  align="right"><input name="gradiente_timpanometrico_izquierda" type="text" size="5" maxlength="5" value="<?php echo $gradiente_timpanometrico_izquierda; ?>"/></td>
                              <td>0 - 4</td>
                            </tr>
                             <tr>
                           	  <td align="right">Volumen Equivalente(ml.)</td>
                              <td><input name="volumen_derecha" type="text" size="5" maxlength="5" value="<?php echo $volumen_derecha; ?>"/></td>
                              <td align="right"><input name="volumen_izquierda" type="text" size="5" maxlength="5" value="<?php echo $volumen_izquierda; ?>"/></td>
                              <td>0 - 4</td>
                            </tr>
                    	</table>
                   	
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones_timpanometria" cols="70" rows="2"><?php echo $observaciones_timpanometria; ?></textarea>
                   	<p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                    </p>
                    </form>	                
                    </div>
    
    <?php
				
				}
	?>
     <!------------------------------Reflejos Acusticos------------------------------------------>
  	<?php 
				$consulta_reflejos_acusticos = mysql_query("SELECT COUNT(id_cliente) FROM reflejos_acusticos 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_reflejos_acusticos = mysql_fetch_array($consulta_reflejos_acusticos);
				$resultado_reflejos_acusticos = $row_reflejos_acusticos["COUNT(id_cliente)"];
				if($resultado_reflejos_acusticos == 0)
				{
	?>
                    <div id="tab4" class="tab_content"  style="display:none;">
                   	<form name="forma1" method="post" action="guardar_reflejos_acusticos.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">PRUEBA DE REFLEJOS ACUSTICOS</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    <br /><br />
                    	<table border= "1" style="border-collapse:collapse; border-style:double;">
                    		<tr>
                            	<th colspan="3" width="250px">Tipo de prueba</th>
                                <th colspan="4" width="280px">URA(dB HL)</th>
                                <th colspan="2" width="200px">Caida</th>
                            </tr>
                            <tr>
                            	<td></td>
                            	<td>Estim.</td>
                                <td>Medicion</td>
                                <td>500Hz</td>
                                <td>1Khz</td>
                                <td>2Khz</td>
                                <td>4Khz</td>
                                <td>500Hz</td>
                                <td>1Khz</td>
                            </tr>
                            <tr >
                            	<td>CONTRA</td>
                                <td>OD</td>
                                <td>OI</td>
                                <td class="exterminados"><select class="desaparecidos" name="contra_500">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="contra_1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                               	<td class="exterminados"><select name="contra_2" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="contra_4" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>CONTRA</td>
                                <td>OI</td>
                                <td>OD</td>
                                <td class="exterminados"><select name="contra2_500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="contra2_1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                               	<td class="exterminados"><select name="contra2_2" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="contra2_4" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra2500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra21" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>IPSI</td>
                                <td>OD</td>
                                <td>OD</td>
                                <td class="exterminados"><select name="ipsi_500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="ipsi_1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                               	<td class="exterminados"><select name="ipsi_2" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="ipsi_4" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>IPSI</td>
                                <td>OI</td>
                                <td>OI</td>
                                <td class="exterminados"><select name="ipsi2_500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="ipsi2_1" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                               	<td class="exterminados"><select name="ipsi2_2" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="ipsi2_4" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                    <option value="80">80</option>
                                    <option value="85">85</option>
                                    <option value="90">90</option>
                                    <option value="95">95</option>
                                    <option value="100">100</option>
                                    <option value="105">105</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi2500" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi21" class="desaparecidos">
                                	<option value=""></option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Negativo">Negativo</option>
                                    <option value="NR">NR</option>
                                </select></td>
                            </tr>
                    	</table>
                   	</center>
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones" cols="70" rows="2"></textarea>
                   	<p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="Guardar Reflejos Acusticos" />
                    </p>
                    </form>
                    </div>
  	<?php
				}else{
					$consultame_todo_reflejos_acusticos = mysql_query("SELECT * FROM reflejos_acusticos
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_reflejos_acusticos = mysql_fetch_array($consultame_todo_reflejos_acusticos);
					$contra_500 = $row_consulta_todo_reflejos_acusticos["contra_ura_500"];
					$contra_1 = $row_consulta_todo_reflejos_acusticos["contra_ura_1"];
					$contra_2 = $row_consulta_todo_reflejos_acusticos["contra_ura_2"];
					$contra_4 = $row_consulta_todo_reflejos_acusticos["contra_ura_4"];
					$caida_contra500 = $row_consulta_todo_reflejos_acusticos["contra_caida_500"];
					$caida_contra1 = $row_consulta_todo_reflejos_acusticos["contra_caida_1"];
					$contra2_500 = $row_consulta_todo_reflejos_acusticos["contra2_ura_500"];
					$contra2_1 = $row_consulta_todo_reflejos_acusticos["contra2_ura_1"];
					$contra2_2 = $row_consulta_todo_reflejos_acusticos["contra2_ura_2"];
					$contra2_4 = $row_consulta_todo_reflejos_acusticos["contra2_ura_4"];
					$caida_contra2500 = $row_consulta_todo_reflejos_acusticos["contra2_caida_500"];
					$caida_contra21 = $row_consulta_todo_reflejos_acusticos["contra2_caida_1"];
					$ipsi_500 = $row_consulta_todo_reflejos_acusticos["ipsi_ura_500"];
					$ipsi_1 = $row_consulta_todo_reflejos_acusticos["ipsi_ura_1"];
					$ipsi_2 = $row_consulta_todo_reflejos_acusticos["ipsi_ura_2"];
					$ipsi_4 = $row_consulta_todo_reflejos_acusticos["ipsi_ura_4"];
					$caida_ipsi500 = $row_consulta_todo_reflejos_acusticos["ipsi_caida_500"];
					$caida_ipsi1 = $row_consulta_todo_reflejos_acusticos["ipsi_caida_1"];
					$ipsi2_500 = $row_consulta_todo_reflejos_acusticos["ipsi2_ura_500"];
					$ipsi2_1 = $row_consulta_todo_reflejos_acusticos["ipsi2_ura_1"];
					$ipsi2_2 = $row_consulta_todo_reflejos_acusticos["ipsi2_ura_2"];
					$ipsi2_4 = $row_consulta_todo_reflejos_acusticos["ipsi2_ura_4"];
					$caida_ipsi2500 = $row_consulta_todo_reflejos_acusticos["ipsi2_caida_500"];
					$caida_ipsi21 = $row_consulta_todo_reflejos_acusticos["ipsi2_caida_1"];
					$observaciones_reflejos_acusticos = $row_consulta_todo_reflejos_acusticos["observaciones"];
					$accion = "Editar Reflejos Acusticos";
	?>
    				<div id="tab4" class="tab_content"  style="display:none;">
                   	<form name="forma1" method="post" action="guardar_reflejos_acusticos.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">PRUEBA DE REFLEJOS ACUSTICOS</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    <br /><br />
                    	<table border= "1" style="border-collapse:collapse; border-style:double;">
                    		<tr>
                            	<th colspan="3" width="250px">Tipo de prueba</th>
                                <th colspan="4" width="280px">URA(dB HL)</th>
                                <th colspan="2" width="200px">Caida</th>
                            </tr>
                            <tr>
                            	<td></td>
                            	<td>Estim.</td>
                                <td>Medicion</td>
                                <td>500Hz</td>
                                <td>1Khz</td>
                                <td>2Khz</td>
                                <td>4Khz</td>
                                <td>500Hz</td>
                                <td>1Khz</td>
                            </tr>
                            <tr >
                            	<td>CONTRA</td>
                                <td>OD</td>
                                <td>OI</td>
                                <td class="exterminados"><select name="contra_500">
  	<?php
									$contra500[0] = "";
									$contra500[1] = "70";
									$contra500[2] = "75";
									$contra500[3] = "80";
									$contra500[4] = "85";
									$contra500[5] = "90";
									$contra500[6] = "95";
									$contra500[7] = "100";
									$contra500[8] = "105";
									$contra500[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra_500 == $contra500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra_500; ?>" selected="selected">
                                            <?php echo $contra_500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra500[$i]; ?>">
                                            <?php echo $contra500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="contra_1">
  	<?php
									$contra1[0] = "";
									$contra1[1] = "70";
									$contra1[2] = "75";
									$contra1[3] = "80";
									$contra1[4] = "85";
									$contra1[5] = "90";
									$contra1[6] = "95";
									$contra1[7] = "100";
									$contra1[8] = "105";
									$contra1[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra_1 == $contra1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra_1; ?>" selected="selected">
                                            <?php echo $contra_1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra1[$i]; ?>">
                                            <?php echo $contra1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                               	<td class="exterminados"><select name="contra_2">
  	<?php
									$contra2[0] = "";
									$contra2[1] = "70";
									$contra2[2] = "75";
									$contra2[3] = "80";
									$contra2[4] = "85";
									$contra2[5] = "90";
									$contra2[6] = "95";
									$contra2[7] = "100";
									$contra2[8] = "105";
									$contra2[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra_2 == $contra2[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra_2; ?>" selected="selected">
                                            <?php echo $contra_2; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra2[$i]; ?>">
                                            <?php echo $contra2[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="contra_4">
  	<?php
									$contra4[0] = "";
									$contra4[1] = "70";
									$contra4[2] = "75";
									$contra4[3] = "80";
									$contra4[4] = "85";
									$contra4[5] = "90";
									$contra4[6] = "95";
									$contra4[7] = "100";
									$contra4[8] = "105";
									$contra4[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra_4 == $contra4[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra_4; ?>" selected="selected">
                                            <?php echo $contra_4; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra4[$i]; ?>">
                                            <?php echo $contra4[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra500">
   	<?php
									$caida500[0] = "";
									$caida500[1] = "Positivo";
									$caida500[2] = "Negativo";
									$caida500[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_contra500 == $caida500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_contra500; ?>" selected="selected">
                                            <?php echo $caida_contra500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida500[$i]; ?>">
                                            <?php echo $caida500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra1">
   	<?php
									$caida1[0] = "";
									$caida1[1] = "Positivo";
									$caida1[2] = "Negativo";
									$caida1[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_contra1 == $caida1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_contra1; ?>" selected="selected">
                                            <?php echo $caida_contra1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida1[$i]; ?>">
                                            <?php echo $caida1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>CONTRA</td>
                                <td>OI</td>
                                <td>OD</td>
                                <td class="exterminados"><select name="contra2_500">
  	<?php
									$contra500[0] = "";
									$contra500[1] = "70";
									$contra500[2] = "75";
									$contra500[3] = "80";
									$contra500[4] = "85";
									$contra500[5] = "90";
									$contra500[6] = "95";
									$contra500[7] = "100";
									$contra500[8] = "105";
									$contra500[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra2_500 == $contra500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra2_500; ?>" selected="selected">
                                            <?php echo $contra2_500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra500[$i]; ?>">
                                            <?php echo $contra500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="contra2_1">
	<?php
									$contra1[0] = "";
									$contra1[1] = "70";
									$contra1[2] = "75";
									$contra1[3] = "80";
									$contra1[4] = "85";
									$contra1[5] = "90";
									$contra1[6] = "95";
									$contra1[7] = "100";
									$contra1[8] = "105";
									$contra1[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra2_1 == $contra1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra2_1; ?>" selected="selected">
                                            <?php echo $contra2_1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra1[$i]; ?>">
                                            <?php echo $contra1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                               	<td class="exterminados"><select name="contra2_2">
  	<?php
									$contra2[0] = "";
									$contra2[1] = "70";
									$contra2[2] = "75";
									$contra2[3] = "80";
									$contra2[4] = "85";
									$contra2[5] = "90";
									$contra2[6] = "95";
									$contra2[7] = "100";
									$contra2[8] = "105";
									$contra2[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra2_2 == $contra2[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra2_2; ?>" selected="selected">
                                            <?php echo $contra2_2; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra2[$i]; ?>">
                                            <?php echo $contra2[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="contra2_4">
	<?php
									$contra4[0] = "";
									$contra4[1] = "70";
									$contra4[2] = "75";
									$contra4[3] = "80";
									$contra4[4] = "85";
									$contra4[5] = "90";
									$contra4[6] = "95";
									$contra4[7] = "100";
									$contra4[8] = "105";
									$contra4[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($contra2_4 == $contra4[$i]){							
   	?>                                                           
                                        <option value="<?php echo $contra2_4; ?>" selected="selected">
                                            <?php echo $contra2_4; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $contra4[$i]; ?>">
                                            <?php echo $contra4[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra2500">
   	<?php
									$caida500[0] = "";
									$caida500[1] = "Positivo";
									$caida500[2] = "Negativo";
									$caida500[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_contra2500 == $caida500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_contra2500; ?>" selected="selected">
                                            <?php echo $caida_contra2500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida500[$i]; ?>">
                                            <?php echo $caida500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_contra21">
   	<?php
									$caida1[0] = "";
									$caida1[1] = "Positivo";
									$caida1[2] = "Negativo";
									$caida1[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_contra21 == $caida1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_contra21; ?>" selected="selected">
                                            <?php echo $caida_contra21; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida1[$i]; ?>">
                                            <?php echo $caida1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>IPSI</td>
                                <td>OD</td>
                                <td>OD</td>
                                <td class="exterminados"><select name="ipsi_500">
	<?php
									$ipsi500[0] = "";
									$ipsi500[1] = "70";
									$ipsi500[2] = "75";
									$ipsi500[3] = "80";
									$ipsi500[4] = "85";
									$ipsi500[5] = "90";
									$ipsi500[6] = "95";
									$ipsi500[7] = "100";
									$ipsi500[8] = "105";
									$ipsi500[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi_500 == $ipsi500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi_500; ?>" selected="selected">
                                            <?php echo $ipsi_500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi500[$i]; ?>">
                                            <?php echo $ipsi500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="ipsi_1">
	<?php
									$ipsi1[0] = "";
									$ipsi1[1] = "70";
									$ipsi1[2] = "75";
									$ipsi1[3] = "80";
									$ipsi1[4] = "85";
									$ipsi1[5] = "90";
									$ipsi1[6] = "95";
									$ipsi1[7] = "100";
									$ipsi1[8] = "105";
									$ipsi1[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi_1 == $ipsi1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi_1; ?>" selected="selected">
                                            <?php echo $ipsi_1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi1[$i]; ?>">
                                            <?php echo $ipsi1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                               	<td class="exterminados"><select name="ipsi_2">
	<?php
									$ipsi2[0] = "";
									$ipsi2[1] = "70";
									$ipsi2[2] = "75";
									$ipsi2[3] = "80";
									$ipsi2[4] = "85";
									$ipsi2[5] = "90";
									$ipsi2[6] = "95";
									$ipsi2[7] = "100";
									$ipsi2[8] = "105";
									$ipsi2[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi_2 == $ipsi2[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi_2; ?>" selected="selected">
                                            <?php echo $ipsi_2; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi2[$i]; ?>">
                                            <?php echo $ipsi2[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="ipsi_4">
	<?php
									$ipsi4[0] = "";
									$ipsi4[1] = "70";
									$ipsi4[2] = "75";
									$ipsi4[3] = "80";
									$ipsi4[4] = "85";
									$ipsi4[5] = "90";
									$ipsi4[6] = "95";
									$ipsi4[7] = "100";
									$ipsi4[8] = "105";
									$ipsi4[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi_4 == $ipsi4[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi_4; ?>" selected="selected">
                                            <?php echo $ipsi_4; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi4[$i]; ?>">
                                            <?php echo $ipsi4[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>n>
                                    <option value="NR">NR</option>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi500">
   	<?php
									$caida500[0] = "";
									$caida500[1] = "Positivo";
									$caida500[2] = "Negativo";
									$caida500[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_ipsi500 == $caida500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_ipsi500; ?>" selected="selected">
                                            <?php echo $caida_ipsi500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida500[$i]; ?>">
                                            <?php echo $caida500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi1">
   	<?php
									$caida1[0] = "";
									$caida1[1] = "Positivo";
									$caida1[2] = "Negativo";
									$caida1[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_ipsi1 == $caida1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_ipsi1; ?>" selected="selected">
                                            <?php echo $caida_ipsi1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida1[$i]; ?>">
                                            <?php echo $caida1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td>IPSI</td>
                                <td>OI</td>
                                <td>OI</td>
                                <td class="exterminados"><select name="ipsi2_500">
	<?php
									$ipsi500[0] = "";
									$ipsi500[1] = "70";
									$ipsi500[2] = "75";
									$ipsi500[3] = "80";
									$ipsi500[4] = "85";
									$ipsi500[5] = "90";
									$ipsi500[6] = "95";
									$ipsi500[7] = "100";
									$ipsi500[8] = "105";
									$ipsi500[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi2_500 == $ipsi500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi2_500; ?>" selected="selected">
                                            <?php echo $ipsi2_500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi500[$i]; ?>">
                                            <?php echo $ipsi500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="ipsi2_1">
	<?php
									$ipsi1[0] = "";
									$ipsi1[1] = "70";
									$ipsi1[2] = "75";
									$ipsi1[3] = "80";
									$ipsi1[4] = "85";
									$ipsi1[5] = "90";
									$ipsi1[6] = "95";
									$ipsi1[7] = "100";
									$ipsi1[8] = "105";
									$ipsi1[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi2_1 == $ipsi1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi2_1; ?>" selected="selected">
                                            <?php echo $ipsi2_1; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi1[$i]; ?>">
                                            <?php echo $ipsi1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                               	<td class="exterminados"><select name="ipsi2_2">
	<?php
									$ipsi2[0] = "";
									$ipsi2[1] = "70";
									$ipsi2[2] = "75";
									$ipsi2[3] = "80";
									$ipsi2[4] = "85";
									$ipsi2[5] = "90";
									$ipsi2[6] = "95";
									$ipsi2[7] = "100";
									$ipsi2[8] = "105";
									$ipsi2[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi2_2 == $ipsi2[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi2_2; ?>" selected="selected">
                                            <?php echo $ipsi2_2; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi2[$i]; ?>">
                                            <?php echo $ipsi2[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
           
                                </select></td>
                                <td class="exterminados"><select name="ipsi2_4">
	<?php
									$ipsi4[0] = "";
									$ipsi4[1] = "70";
									$ipsi4[2] = "75";
									$ipsi4[3] = "80";
									$ipsi4[4] = "85";
									$ipsi4[5] = "90";
									$ipsi4[6] = "95";
									$ipsi4[7] = "100";
									$ipsi4[8] = "105";
									$ipsi4[9] = "NR";					 
									for($i=0; $i<10; $i++){
										if($ipsi2_4 == $ipsi4[$i]){							
   	?>                                                           
                                        <option value="<?php echo $ipsi2_4; ?>" selected="selected">
                                            <?php echo $ipsi2_4; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $ipsi4[$i]; ?>">
                                            <?php echo $ipsi4[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>n>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi2500">
   	<?php
									$caida500[0] = "";
									$caida500[1] = "Positivo";
									$caida500[2] = "Negativo";
									$caida500[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_ipsi2500 == $caida500[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_ipsi2500; ?>" selected="selected">
                                            <?php echo $caida_ipsi2500; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida500[$i]; ?>">
                                            <?php echo $caida500[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                                <td class="exterminados"><select name="caida_ipsi21">
   	<?php
									$caida1[0] = "";
									$caida1[1] = "Positivo";
									$caida1[2] = "Negativo";
									$caida1[3] = "NR";					 
									for($i=0; $i<4; $i++){
										if($caida_ipsi21 == $caida1[$i]){							
   	?>                                                           
                                        <option value="<?php echo $caida_ipsi21; ?>" selected="selected">
                                            <?php echo $caida_ipsi21; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $caida1[$i]; ?>">
                                            <?php echo $caida1[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                    	</table>
                   	</center>
                    <br />
                    	<label class="textos">Observaciones: </label><br />
                    	<textarea name="observaciones" cols="70" rows="2"><?php echo $observaciones_reflejos_acusticos; ?></textarea>
                   	<p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                    </p>
                    </form>
                    </div>
    <?php
				}  
	?>
     <!----------------------------Emisiones Otoacusticas-------------------------------------->
   	<?php
				$consulta_emisiones_otoacusticas = mysql_query("SELECT COUNT(id_cliente) FROM emisiones_otoacusticas 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_emisiones_otoacusticas = mysql_fetch_array($consulta_emisiones_otoacusticas);
				$resultado_emisiones_otoacusticas = $row_emisiones_otoacusticas["COUNT(id_cliente)"];
				if($resultado_emisiones_otoacusticas == 0){			
	
	?>          
                    <div id="tab5" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_emisiones_otoacusticas.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">EMISIONES OTOACUSTICAS</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    </center>
                    <br />
                    	<table border= "1" style="border-collapse:collapse; border-style:double;">
                        	<tr>
                            	<th colspan="2">Lista de Campos</th>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Resultado:</td>
                                <td style="text-align:left;"><select name="resultado">
                                	<option value="">Seleccione</option>
                                    <option value="Pasa">Pasa</option>
                                    <option value="Referir">Referir</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Veces que se ha realizado el estudio:</td>
                                <td style="text-align:left;"><select name="veces_estudio">
                                	<option value="">Seleccione</option>
                                    <option value="1ra">1ra</option>
                                    <option value="2da">2da</option>
                                    <option value="3ra">3ra</option>
                                    <option value="4ta">4ta</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Protocolo de pase:</td>
                                <td style="text-align:left;"><select name="protocolo_de_pase" id="protocolo_de_pase">
                                	<option value="">Seleccione</option>
                                    <option value="2-3">2 DE 3</option>
                                    <option value="2-4">2 DE 4</option>
                                    <option value="3-4">3 DE 4</option>
                                    <option value="3-3">3 DE 3</option>
                                    <option value="4-4">4 DE 4</option>
                                    <option value="3-5">3 DE 5</option>
                                   	<option value="4-5">4 DE 5</option>
                                    <option value="4-6">4 DE 6</option>
                                    <option value="5-6">5 DE 6</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right; padding-bottom:4px; padding-top:4px;">Fecha de la prueba:</td>
                                <td style="text-align:left; padding-bottom:4px; padding-top:4px;">
                                <input name="fecha" value="<?php echo $fecha; ?>" type="text" readonly="readonly" /><div id="spanreloj"></div></td>
                            </tr>
                        	<tr>
                            	<td style="text-align:right;">Tipo de Emisiones:</td>
                                <td style="text-align:left;"><select name="tipo_emisiones">
                                	<option value="">Seleccione</option>
                                    <option value="DPOAE">DPOAE</option>
                                    <option value="TEOAE">TEOAE</option>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right">Diagnostico:</td>
                                <td><textarea name="diagnostico" cols="55" rows="2"></textarea></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right">Observaciones:</td>
                                <td><textarea name="observaciones" cols="55" rows="2"></textarea></td>
                            </tr>
                        </table> 
                    <br />
                    <div id="tabla_frecuencias">
                    
                    </div>
                    <p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="Guardar Emisiones Otoacusticas" />
                    </p>
                    </form>                  
                    </div>
    <?php
				}else{
					$consultame_todo_emisiones_otoacusticas = mysql_query("SELECT * FROM emisiones_otoacusticas
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_emisiones_otoacusticas = mysql_fetch_array($consultame_todo_emisiones_otoacusticas);
					$resultado = $row_consulta_todo_emisiones_otoacusticas["resultado"];
					$veces_estudio = $row_consulta_todo_emisiones_otoacusticas["veces_que_realizo_estudio"];
					$protocolo_de_pase = $row_consulta_todo_emisiones_otoacusticas["protocolo_pase"];
					$fecha_prueba = $row_consulta_todo_emisiones_otoacusticas["fecha_prueba"];
					$hora_prueba = $row_consulta_todo_emisiones_otoacusticas["hora"];
					$tipo_emisiones = $row_consulta_todo_emisiones_otoacusticas["tipo_emisiones"];
					$diagnostico = $row_consulta_todo_emisiones_otoacusticas["diagnostico"];
					$observaciones_emisiones = $row_consulta_todo_emisiones_otoacusticas["observaciones"];
					$accion = "Editar Emisiones Otoacusticas";
					$fecha_normal = cambiaf_a_normal($fecha_prueba);
	?>
    				<div id="tab5" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_emisiones_otoacusticas.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">EMISIONES OTOACUSTICAS</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" id="folio"/>
                    </center>
                    <br />
                    	<table border= "1" style="border-collapse:collapse; border-style:double;">
                        	<tr>
                            	<th colspan="2">Lista de Campos</th>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Resultado:</td>
                                <td style="text-align:left;"><select name="resultado">
   	<?php
									$resultado_original[0] = "";
									$resultado_original[1] = "Pasa";
									$resultado_original[2] = "Referir";				 
									for($i=0; $i<3; $i++){
										if($resultado == $resultado_original[$i]){							
   	?>                                                           
                                        <option value="<?php echo $resultado; ?>" selected="selected">
                                            <?php echo $resultado; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $resultado_original[$i]; ?>">
                                            <?php echo $resultado_original[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Veces que se ha realizado el estudio:</td>
                                <td style="text-align:left;"><select name="veces_estudio">
   	<?php
									$veces_estudios[0] = "";
									$veces_estudios[1] = "1ra";
									$veces_estudios[2] = "2da";
									$veces_estudios[3] = "3ra";
									$veces_estudios[4] = "4ta";			 
									for($i=0; $i<5; $i++){
										if($veces_estudio == $veces_estudios[$i]){							
   	?>                                                           
                                        <option value="<?php echo $veces_estudio; ?>" selected="selected">
                                            <?php echo $veces_estudio; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $veces_estudios[$i]; ?>">
                                            <?php echo $veces_estudios[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right;">Protocolo de pase:</td>
                                <td style="text-align:left;"><select name="protocolo_de_pase" id="protocolo_de_pase2">
    <?php
						$protocolos[0] = "";
						$protocolos[1] = "2-3";
						$protocolos[2] = "2-4";	
						$protocolos[3] = "3-4";	
						$protocolos[4] = "3-3";	
						$protocolos[5] = "4-4";	
						$protocolos[6] = "3-5";	
						$protocolos[7] = "4-5";	
						$protocolos[8] = "4-6";
						$protocolos[9] = "5-6";
						$protocolos_valor[0]="Seleccione";
						$protocolos_valor[1]="2 DE 3";
						$protocolos_valor[2]="2 DE 4";
						$protocolos_valor[3]="3 DE 4";
						$protocolos_valor[4]="3 DE 3";
						$protocolos_valor[5]="4 DE 4";
						$protocolos_valor[6]="3 DE 5";
						$protocolos_valor[7]="4 DE 5";
						$protocolos_valor[8]="4 DE 6";
						$protocolos_valor[9]="5 DE 6";
						
						for($i=0; $i<10; $i++){
							if($protocolo_de_pase == $protocolos[$i]){							
   	?>                                                           
                                    <option value="<?php echo $protocolo_de_pase; ?>" selected="selected">
                                        <?php echo $protocolo_de_pase; ?>
                                    </option>
	<?php 
                          	}else{
	?>                            
                                    <option value="<?php echo $protocolos[$i]; ?>">
                                        <?php echo $protocolos_valor[$i]; ?>
                                    </option>
  	<?php
                            }
                    	}
	?>
                                </select></td>                        
                            </tr>
                            <tr>
                            	<td style="text-align:right; padding-bottom:4px; padding-top:4px;">Fecha de la prueba:</td>
                                <td style="text-align:left; padding-bottom:4px; padding-top:4px;">
                                <input name="fecha" value="<?php echo $fecha_normal; ?>" type="text" readonly="readonly" /><div id="spanreloj"></div></td>
                            </tr>
                        	<tr>
                            	<td style="text-align:right;">Tipo de Emisiones:</td>
                                <td style="text-align:left;"><select name="tipo_emisiones">
    <?php
									$tipo_emision[0] = "";
									$tipo_emision[1] = "DPOAE";
									$tipo_emision[2] = "TEOAE";		 
									for($i=0; $i<3; $i++){
										if($tipo_emisiones == $tipo_emision[$i]){							
   	?>                                                           
                                        <option value="<?php echo $tipo_emisiones; ?>" selected="selected">
                                            <?php echo $tipo_emisiones; ?>
                                        </option>
   	<?php 
                            			}else{
    ?>                            
                                        <option value="<?php echo $tipo_emision[$i]; ?>">
                                            <?php echo $tipo_emision[$i]; ?>
                                        </option>
  	<?php
                            			}
                    				}
	?>
                                </select></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right">Diagnostico:</td>
                                <td><textarea name="diagnostico" cols="55" rows="2"><?php echo $diagnostico; ?></textarea></td>
                            </tr>
                            <tr>
                            	<td style="text-align:right">Observaciones:</td>
                                <td><textarea name="observaciones" cols="55" rows="2"><?php echo $observaciones_emisiones; ?></textarea></td>
                            </tr>
                        </table> 
                    <br />
                    <div id="tabla_frecuencias">
                       				<table border= "1" style="border-collapse:collapse; border-style:double;">
                    	<tr>
                            <th></th>
                            <th width="100">Frecuencia</th>
                            <th width="150">SNR</th>
                            <th width="120">Pasa</th>
               			</tr>
    <?php
				$consultame_todas_las_frecuencias = mysql_query("SELECT * FROM frecuencias
																			WHERE id_nota_clinica=".$_SESSION["folio"]." 
																			ORDER BY id_registro")
																			or die(mysql_error());
				while($row_consulta_frecuencias = mysql_fetch_array($consultame_todas_las_frecuencias)){
				$identificador = $row_consulta_frecuencias["identificador"];
				$frecuencia = $row_consulta_frecuencias["frecuencia"];
				$snr = $row_consulta_frecuencias["snr"];
				$pasa = $row_consulta_frecuencias["pasa"];
	?>
    					<tr>
                        	<td>F<?php echo $identificador; ?></td>
                            <td><?php echo $frecuencia; ?></td>
                            <td><?php echo $snr; ?></td>
                            <td><?php echo $pasa; ?></td>
                        </tr>
    <?php
				}
	?>
    				</table>
                    </div>

 
                    <p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                    </p>
                    </form>                  
                    </div>
    <?php
				}
	?>
    <!----------------------------Estudio Audiometrico-------------------------------------->
   	<?php
				$consulta_estudio_audiometrico = mysql_query("SELECT COUNT(id_cliente) FROM estudio_audiometrico 
																				WHERE id_nota_clinica=".$_SESSION["folio"]." 
																				AND id_cliente=".$id_cliente) 
																				or die(mysql_error());
				$row_estudio_audiometrico = mysql_fetch_array($consulta_estudio_audiometrico);
				$resultado_estudio_audiometrico = $row_estudio_audiometrico["COUNT(id_cliente)"];
				if($resultado_estudio_audiometrico == 0){			
	
	?>
                    <div id="tab6" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_audiometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">ESTUDIO AUDIOMETRICO</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    </center>
                    <ul>
                    	<li>¿Cómo oye una conversación normal de persona a persona?</li>
                        R=<input name="respuesta1" type="text" size="80" maxlength="80" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye una conversación estando entre un grupo?</li>
                        R=<input name="respuesta2" type="text" size="80" maxlength="80" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye en auditorios, teatros, iglesias?</li>
                        R=<input name="respuesta3" type="text" size="80" maxlength="80" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye la televisión o el radio?</li>
                        R=<input name="respuesta4" type="text" size="80" maxlength="80" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye el teléfono?</li>
                        R=<input name="respuesta5" type="text" size="80" maxlength="80" />
                    </ul>
                    <label class="textos">Observaciones:</label><br />
                    <textarea name="observaciones" rows="2" cols="70"></textarea>
                    <p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="Guardar Estudio Audiometrico" />
                    </p>
                    </form>
                    </div>
 	<?php
				}else{
					$consultame_todo_estudio_audiometrico = mysql_query("SELECT * FROM estudio_audiometrico
																			WHERE id_cliente='".$id_cliente."' 
																			AND id_nota_clinica=".$_SESSION["folio"])
																			or die(mysql_error());
					$row_consulta_todo_estudio_audiometrico = mysql_fetch_array($consultame_todo_estudio_audiometrico);
					$respuesta1 = $row_consulta_todo_estudio_audiometrico["respuesta1"];
					$respuesta2 = $row_consulta_todo_estudio_audiometrico["respuesta2"];
					$respuesta3 = $row_consulta_todo_estudio_audiometrico["respuesta3"];
					$respuesta4 = $row_consulta_todo_estudio_audiometrico["respuesta4"];
					$respuesta5 = $row_consulta_todo_estudio_audiometrico["respuesta5"];
					$observaciones_estudio_audiometrico = $row_consulta_todo_estudio_audiometrico["observaciones"];
					$accion="Editar Estudio Audiometrico";
	?>
    				<div id="tab6" class="tab_content"  style="display:none;">
                    <form name="forma1" method="post" action="guardar_audiometria.php" enctype="multipart/form-data">
                    <center>
                        <label style="font-size:16px; font-weight:bold;">ESTUDIO AUDIOMETRICO</label>
                        <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        <input name="folio_estudio" type="hidden" value="<?php echo $_SESSION["folio"];?>" />
                    </center>
                    <ul>
                    	<li>¿Cómo oye una conversación normal de persona a persona?</li>
                        R=<input name="respuesta1" type="text" size="80" maxlength="80" value="<?php echo $respuesta1; ?>" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye una conversación estando entre un grupo?</li>
                        R=<input name="respuesta2" type="text" size="80" maxlength="80"value="<?php echo $respuesta2; ?>" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye en auditorios, teatros, iglesias?</li>
                        R=<input name="respuesta3" type="text" size="80" maxlength="80" value="<?php echo $respuesta3; ?>" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye la televisión o el radio?</li>
                        R=<input name="respuesta4" type="text" size="80" maxlength="80" value="<?php echo $respuesta4; ?>" />
                        <li style="list-style-type:none;"><br /></li>
                        <li>¿Cómo oye el teléfono?</li>
                        R=<input name="respuesta5" type="text" size="80" maxlength="80" value="<?php echo $respuesta5; ?>" />
                    </ul>
                    <label class="textos">Observaciones:</label><br />
                    <textarea name="observaciones" rows="2" cols="70"><?php echo $observaciones_estudio_audiometrico; ?></textarea>
                    <p align="right">
                    	<input name="accion" type="submit" class="fondo_boton" value="<?php echo $accion; ?>" />
                    </p>
                    </form>
                    </div>
    <?php
				}
	?>
            				</div><!--Fin de tab_container-->
                    	</div><!--Fin de contenido proveedor-->
                	</div><!-- Fin de area contenido -->
                </div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido clolumna 2-->
</div><!--Fin del Wrapp-->
</body>
</html>