<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Salidas </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
            <div class="categoria">
                Salidas
            </div><!-- FIN DIV CATEGORIAS -->
            </div><!-- FIN DIV FONDO TITULO1 -->
                <div class="area_contenido1">
                    <br/>
                    <?php
                        // SE IMPORTA EL ARCHIVO DE LA CONEXION A LA BASE DE DATOS
                        include("config.php");
                        // SE REALIZA QUERY QUE OBTIENE LAS SALIDAS QUE EXISTEN EN LA BASE DE DATOS
                        $query_salidas = mysql_query("SELECT id_salida,salida, precio
                                                      FROM salidas
                                                      ORDER BY id_salida ASC") or die (mysql_error());
                    ?>
                    <center>
                        <table>
                            <tr>
                                <th width="130"> N° de Salida </th>
                                <th> Nombre Salida </th>
                                <th width="80"> Precio </th>
                                <th width="25"> </th>
                            </tr>
                            <?php
                            while( $row_salida = mysql_fetch_array($query_salidas) )
                            {
                                $id_salida = $row_salida['id_salida'];
                                $nombre_salida = $row_salida['salida'];
                                $precio = $row_salida['precio'];
                            ?>
                                <tr>
                                    <td id="centrado"> <?php echo $id_salida; ?> </td>
                                    <td> <?php echo $nombre_salida; ?> </td>
                                    <td> $<?php echo number_format($precio,2); ?></td>
                                    <td> <a href="modificar_salidas.php?id_salida=<?php echo $id_salida; ?>">
                                            <img src="../img/modify.png" />
                                         </a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </table><!-- FIN TABLA -->
                    </center><!-- FIN CENTER -->
                    <br/>
                    <div class="titulos"> Agregar Nueva Salida </div><!-- FIN DIV TITULOS -->
                    <br/>
                    <center>
                        <form name="form_agregar_nueva_salida" id="form_agregar_nueva_salida" 
                        method="post" action="procesa_agregar_nuevas_salidas.php" onsubmit="return validaAgregarSalida()" 
                        enctype="multipart/form-data" >
                            <table>
                                <tr>
                                    <td id="alright"> 
                                    	<label class="textos"> Nombre de Salida: </label> 
                                    </td><td id="alleft"> 
                                    	<input type="text" name="txt_nombre_salida" id="txt_nombre_salida"/> 
                                    </td>
                                </tr>
                                <tr>
                                	<td id="alright">
                                    	<label class="textos"> Precio: </label>
                                    </td><td id="alleft">
                                     	$<input name="precio" type="text" size="10" maxlength="10" />
                                    </td>
                                </tr><tr>
                                    <td id="alright">
                                    	<label class="textos"> Imagen Salida: </label> 
                                    </td><td id="alleft"> 
                                    	<input type="file" name="file_imagen" id="file_imagen" accept="image" style="height:25px;"/> 
                                   	</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <p align="right">
                                        <input type="reset" name="cancelar"  value="Cancelar" title="Cancelar" class="fondo_boton"/>
                                        <input type="submit" name="accion" value="Aceptar" title="Aceptar" class="fondo_boton"/>
                                   	</p>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </center>
         	</div><!-- FIN DIV AREA CONTENIDO1 -->
        </div><!-- FIN DIV CONTENIDO PAGINA -->
    </div><!-- FIN DIV CONTENIDO COLUMNA2 -->
</div><!-- FIN DIV WRAPP -->
</body>

</html>
