<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Personal</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<input name="id_contacto_empleado[]" type="hidden" /><label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label></td><td><input type="text" name="descripcion[]" /> <input type="button" value="Eliminar" class="fondo_boton" title="Eliminar"/><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="contenido_columna2">
	<div class="contenido_pagina">
    	<div class="fondo_titulo1">
        	<div class="categoria">
            	Empleados
            </div><!-- Fin DIV categoria -->
        </div><!--Fin de fondo titulo-->
        <br /><br />
        <div class="area_contenido2">
        		<?php
					include("config.php");			
					$id_empleado = $_GET["id_empleado"];
                    $query_usuario_empleado = mysql_query("SELECT user,pass
                                                            FROM usuarios
                                                            WHERE id_empleado = '$id_empleado'") or die(mysql_error());
                    $row_usuario_empleado = mysql_fetch_array($query_usuario_empleado);
                    $user = $row_usuario_empleado['user'];
                    $password = $row_usuario_empleado['pass'];

					$consulta_datos_empleado = mysql_query("SELECT * FROM empleados 
															WHERE id_empleado=".$id_empleado) 
															or die(mysql_error());
					$row = mysql_fetch_array($consulta_datos_empleado);
					$nombre = $row["nombre"];
					$paterno = $row["paterno"];
					$materno = $row["materno"];
					$fecha_nacimiento = $row["fecha_nacimiento"];
					$genero = $row["genero"];
					$id_departamento = $row["id_departamento"];
					$estado_civil = $row["estado_civil"];
					$calle = $row["calle"];
					$num_exterior = $row["num_exterior"];
					$num_interior = $row["num_interior"];
					$codigo_postal = $row["codigo_postal"];
					$colonia = $row["colonia"];
					$id_estado = $row["id_estado"];
					$id_ciudad = $row["id_ciudad"];
					$ocupacion = $row["ocupacion"];
					$alias = $row["alias"];				
				?>
            <center>
            <form  action="procesa_modificar_personal.php" method="post" name="form_agregar_personal" onsubmit="return validarAgregarPersonal()" >
                <table>
                	<tr>
                    	<th colspan="4">Modificar Empleado</th>
                    </tr>
                	<tr>
                    	<td style="text-align:right">
                    		<input name="id_empleado" type="hidden" value="<?php echo $id_empleado; ?>" />
                    		<label class="textos">Nombre(s): </label>
                        </td><td style="text-align:left">
                   			<input name="nombre" type="text" size="25" maxlength="25" 
                            title="Nombre(s)" value="<?php echo $nombre; ?>" />
                        </td><td style="text-align:right">
                            <label class="textos">Alias: </label>
                        </td><td style="text-align:left">
                            <input name="alias" type="text" title="Alias" maxlength="15" 
                            size="25" value="<?php echo $alias; ?>"/>
                        </td>
					</tr><tr>
  						<td style="text-align:right">
                        	<label class="textos">Paterno: </label>
                        </td><td style="text-align:left">
                          	<input name="paterno" type="text" size="25" maxlength="20" 
                            title="Apellido Paterno" value="<?php echo $paterno; ?>" />
                        </td><td style="text-align:right">
                        	<label class="textos">Materno: </label>
                        </td><td style="text-align:left">
                        	<input name="materno" type="text" size="25" maxlength="20" 
                            title="Apellido Materno" value="<?php echo $materno; ?>" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Fecha de Nacimiento: </label>
                        </td><td style="text-align:left">
                            <input name="fecha_nacimiento" type="text" size="10" maxlength="10"
                             title="Fecha de Nacimiento" value="<?php echo $fecha_nacimiento; ?>"
                             onkeyup="Validar(this,'/',patron,true)" />
                        </td><td style="text-align:right">
                        	<label class="textos">Ocupacion: </label>
                        </td><td style="text-align:left">
                        	<input name="ocupacion" type="text" title="Ocupación" 
                            value="<?php echo $ocupacion; ?>" />
                        </td>
                   	</tr><tr>
                        <td style="text-align:right">
                        	<label class="textos">Departamento: </label>
                        </td><td style="text-align:left">
                        	<select name="id_departamento" id="id_departamento">                            	
        					<?php 
								$consulta_departamentos = mysql_query("SELECT * FROM areas_departamentos") or die(mysql_error());			
								while($row = mysql_fetch_array($consulta_departamentos)){			
									$departamentos = $row["departamentos"];
									$id_departamento2 = $row["id_departamento"];	
									if($id_departamento == $id_departamento2){
							?>
                            	<option value="<?php echo $id_departamento2; ?>" selected="selected">
									<?php	echo $departamentos; ?>
                                </option>
                            <?php		
									}else{
							?>
        						<option value="<?php echo $id_departamento2; ?>" ><?php	echo $departamentos; ?></option>
        					<?php
									}
        						}
							?>	
       						</select>
						</td>
                   	</tr>
                    <tr>
                        <td style="text-align: right;"> <label class="textos"> Usuario: </label> </td>
                        <td> <input type="text" name="user_name" id="user_name" value="<?php echo $user; ?>" /> </td>
                        <td style="text-align: right;"> <label class="textos"> Password: </label> </td>
                        <td> <input type="text" name="user_pass" id="user_pass" value="<?php echo $password; ?>" /> </td>
                    </tr>
                    <tr>
                    	<td colspan="4" style="text-align:center"><label class="textos">- Direccion -</label></td>
                	</tr><tr>                    
                        <td style="text-align:right">
                        	<label class="textos">Calle: </label>
                        </td><td style="text-align:left">
                        	<input name="calle" type="text" title="Calle" size="30" 
                            value="<?php echo $calle; ?> "maxlength="50"/>
                        </td><td style="text-align:right">
                        	<label class="textos">N&uacute;mero </label>
                        </td><td style="text-align:left">
                            <label class="textos">ext.: </label>                        
                            <input name="num_exterior" type="text" size="1" maxlength="5"
                            value="<?php echo $num_exterior; ?>" title="Número Exterior" />                        
                            <label class="textos"> int.: </label>
                            <input name="num_interior" type="text" size="1" maxlength="6" 
                            value="<?php echo $num_interior; ?>" title="Número Interior" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Colonia:</label>
                        </td><td style="text-align:left">
                        	<input name="colonia" type="text" size="30" maxlength="50" 
                            value="<?php echo $colonia; ?>" title="Colonia"/>
                        </td><td style="text-align:right">
                        	<label class="textos">Codigo Postal: </label>
                        </td><td style="text-align:left">
                        	<input name="codigo_postal" type="text" size="5" maxlength="5"
                            value="<?php echo $codigo_postal; ?>" title="Código Postal" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Estado: </label>
                        </td><td style="text-align:left">
                            <select id="id_estado" name="id_estado" title="Estados">                                
                            <?php
                                $consulta_estados = mysql_query("SELECT id_estado,estado FROM estados");
                                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                    $id_estado2 = $row3["id_estado"];
                                    $estado = $row3["estado"];
									if($id_estado==$id_estado2){
							?>
                            	<option value="<?php echo $id_estado2; ?>" selected="selected"> 
                                    <?php echo utf8_encode($estado); ?> 
                                </option>
                            <?php
									}else{
                            ?>
                                <option value="<?php echo $id_estado2; ?>"> 
                                    <?php echo utf8_encode($estado); ?> 
                                </option>
                            <?php
									}
                                }
                            ?>
                            </select>
                    	</td><td style="text-align:right">
                        	<label class="textos">Ciudad: </label>
                        </td><td style="text-align:left">
                            <select name="id_municipio" id="id_municipio" title="Municipios" >
                            <?php
                                $consulta_estados = mysql_query("SELECT id_ciudad,ciudad 
																FROM ciudades 
																WHERE id_estado=".$id_estado);
                                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                    $id_ciudad2 = $row3["id_ciudad"];
                                    $ciudad = ucwords(strtolower($row3["ciudad"]));
									if($id_ciudad2==$id_ciudad){
							?>
                            	<option value="<?php echo $id_ciudad2; ?>" selected="selected"> 
                                    <?php echo utf8_encode($ciudad); ?> 
                                </option>
                            <?php
									}else{
                            ?>
                                <option value="<?php echo $id_ciudad2; ?>"> 
                                    <?php echo utf8_encode($ciudad); ?> 
                                </option>
                            <?php
									}
                                }
                            ?>
                            </select>
                    	</td>
                   	</td><tr>
                   		<td style="text-align:right">
                        	<label class="textos">Genero: </label>
                        </td><td style="text-align:left">                        	
                            <select name="sexo" id="sexo">
                            	<?php 
									if($genero=="Femenino"){
								?>                                
                                <option value="Femenino" selected="selected">Femenino</option>
                                <option value="Masculino" >Masculino</option>
                                <?php
									}else{
								?>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino" selected="selected" >Masculino</option>                                
                                <?php
									}
								?>
                            </select>
                        </td><td style="text-align:right">
                        	<label class="textos">Estado Civil:</label>
                        </td><td style="text-align:left">
                            <select name="estado_civil" id="tamaño">
                            	<?php 
									if($estado_civil=="Casado(a)"){
								?>                                                                                            
                                <option value="Casado(a)" selected="selected">Casado(a)</option>
                                <option value="Soltero(a)">Soltero(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                                <option value="Separado(a)">Separado(a)</option>
                                <?php
									}
									if($estado_civil=="Soltero(a)"){
								?>                                                                                            
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Soltero(a)" selected="selected">Soltero(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                                <option value="Separado(a)">Separado(a)</option>
                                <?php
									}
									if($estado_civil=="Divorciado(a)"){
								?>                                                                                            
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Soltero(a)">Soltero(a)</option>
                                <option value="Divorciado(a)" selected="selected">Divorciado(a)</option>
                                <option value="Separado(a)">Separado(a)</option>
                                <?php
									}
									if($estado_civil=="Separado(a)"){
								?>                                                                                            
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Soltero(a)">Soltero(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                                <option value="Separado(a)" selected="selected">Separado(a)</option>
                                <?php
									}
								?>
                            </select>
                        </td>
                	</tr><tr>                        
						<td colspan="4" style="text-align:center"><label class="textos">- Contacto -</label></td>
                	</tr><tr>  
                    	<td style="text-align:center" colspan="4">
                        	<?php
								$consulta_forma_contacto=mysql_query("SELECT * FROM contacto_empleados
																	WHERE id_empleado=".$id_empleado)
																	or die(mysql_error());
								while($row5 = mysql_fetch_array($consulta_forma_contacto)){
								$id_contacto_empleado = $row5["id_contacto_empleado"];
								$tipo = $row5["tipo_telefono"];
								$descripcion = $row5["descripcion"];	            

							?> 
                            <input name="id_contacto_empleado[]" type="hidden" value="<?php echo $id_contacto_empleado; ?>" />                            
                            <label class="textos">Tipo:</label>
                            <select name="tipo_telefono[]">
                        <?php
                            $formas_contacto[0] = "Telefono";
                            $formas_contacto[1] = "Celular";
                            $formas_contacto[2] = "Fax";
                            $formas_contacto[3] = "Correo";						 
                            for($i=0; $i<4; $i++){
                                if($tipo == $formas_contacto[$i]){							
                        ?>                                                           
                                    <option value="<?php echo $tipo; ?>" selected="selected">
                                        <?php echo $tipo; ?>
                                    </option>
                        <?php 
                                }else{
                        ?>                            
                                <option value="<?php echo $formas_contacto[$i]; ?>">
                                    <?php echo $formas_contacto[$i]; ?>
                                </option>
                        <?php
                                }
                            }
                        ?>
                            </select>
                            <label class="textos">Descripción:</label>
                            <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>" />
                            <input type="button" value="Eliminar" class="fondo_boton" 
                            title="Eliminar <?php echo $tipo; ?>" onclick="location='proceso_eliminar_forma_contacto_proveedor.php?id_forma_contacto=<?php echo $id_forma_contacto; ?>&id_proveedor=<?php echo $id_proveedor; ?>'"><br /><br />    
                        <?php
							}
						?>
						</td>
                	</tr><tr>                        
                        <td style="text-align:center" colspan="4">                            
                            <div id="nuevo" style="margin-top:-10px"></div>
                        </td>
                	</tr><tr>      
                    	<td colspan="4" style="text-align:center">                  
							<input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>
                       	</td>
                   	</tr><tr>
                    	<td colspan="4" style="text-align:right">
                        	<input type="button" name="volver" value="Volver" class="fondo_boton"
                            onclick="window.location.href='agregar_personal.php'" />
                            <input name="modificar" type="submit" value="Modificar" class="fondo_boton"/>
                    	</td>
					</tr>
				</table>                        
                </form>                
        </div>
    </div><!--Fin de cuerpo-->   
</div><!--Fin de wrapp-->
</body>
</html>