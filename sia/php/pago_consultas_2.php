    <?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Pago de Consultas </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/jquery.ui.all.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" language="javascript" src="../js/ui/jquery.ui.datepicker.js"></script>
	<link type="text/css" rel="stylesheet" href="../css/themes/base/demos.css"/>
	<script>
		$(function(){
			var dates = $( "#from, #to" ).datepicker({
				defaultDate: "",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ){
					var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					    instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
		});
	</script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }       
        // Fin del script -->
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">     
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria" style="width: 550px;">
                    Reporte de Pago de Consultas
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />
                <!--<div class="titulos"> Pago de Consultas </div>-->
                <div class="contenido_proveedor">
                    <center>
                        <form name="form" action="pago_consultas_2.php" method="post">
                            <div class="demo">
                                <label class="textos"> Doctor: </label> &nbsp;
                                <select name="empleado">
                                    <option value="0"> Seleccione Doctor </option>
           	                <?php
                                // SE REALIZA QUERY QUE OBTIENE A LOS DOCTORES
        				        $consulta_doctores = mysql_query("SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo, ocupacion, id_empleado
        						                                  FROM empleados WHERE ocupacion
        														  LIKE '%Dr%'") or die(mysql_error());

                                // SE REALIZA CICLO PARA MOSTRAR EL RESULTADO OBTENIDO EN EL QUERY ANTERIOR
                                while( $row2 = mysql_fetch_array($consulta_doctores) )
                                {
        					        $nombre = $row2['nombre_completo'];
        					        $ocupacion = $row2["ocupacion"];
        					        $id_empleado = $row2["id_empleado"];
        	                ?>
        							<option value="<?php echo $id_empleado; ?>"> <?php echo $ocupacion." ".$nombre; ?> </option>
        	                <?php
        				        }
        	                ?>
                            	</select>
                                <br /><br />
                                <label for="from" class="textos"> Fecha Inicio: </label> &nbsp;
                                <input type="text" id="from" name="from"/> &nbsp;&nbsp;
                                <label for="to" class="textos"> Fecha Fin: </label> &nbsp;
                                <input type="text" id="to" name="to"/>
                            </div><!-- End demo -->
                            <p align="center">
                                <input type="submit" name="accion" value="Aceptar" title="Aceptar" class="fondo_boton"/>
                            </p>
                        </form>
                        <hr style="background-color:#e6e6e6; height:3px; border:none;">
                        <br />
                    </center>
                </div><!--Fin de contenido proveedor-->
	            <?php
                    //  SE RECIBE LAs VARIABLEs DEL FORMULARIO QUE CONTIENE EL ID DEL EMPLEADO Y EL RANGO DE FECHA
                    $id_empleado = $_POST['empleado'];
                    $fecha_inicio = $_POST['from'];
                    $fecha_inicio_separada = explode("/", $fecha_inicio);
                    $fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
                    $fecha_final = $_POST['to'];
                    $fecha_final_separada = explode("/", $fecha_final);
                    $fecha_final_mysql = $fecha_final_separada[2]."-".$fecha_final_separada[1]."-".$fecha_final_separada[0];

                    // SE VALIDA SI SE HA OPRIMIDO EL BOTON ACEPTAR Y SI SE HA SELECCIONADO ALGUN DOCTOR
                    if ( isset($_POST['accion']) && $id_empleado != 0 && $fecha_inicio != "" && $fecha_final != "" )
                    {
                        // SE REALIZA QUERY QUE OBTIENE EL NOMBRE COMPLETO DEL DOCTOR SELECCIONADO
                        $query_nombre_doctor = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_doctor,ocupacion
                                                FROM empleados
                                                WHERE id_empleado = '$id_empleado'";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
                        $resultado_nombre_doctor = mysql_query($query_nombre_doctor) or die(mysql_error());
                        $row_nombre_doctor = mysql_fetch_array($resultado_nombre_doctor);
                        $nombre_completo_doctor = $row_nombre_doctor['nombre_completo_doctor'];
                        $ocupacion = $row_nombre_doctor['ocupacion'];

                        // SE DECLARA UN CONTADOR PARA SABER SI EXISTEN CONSULTAS PARA EL DOCTOR SELECCIONADO
                        $contador = 0;

                        // SE DECLARA UNA VARIABLE PARA IR SUMANDA EL TOTAL DE LAS CONSULTAS REALIZADAS
                        $total_consultas = 0;

                        // SE DECLARA UNA VARIABLE PARA SUMAR LA CANTIDAD EN DINERO A PAGARLE AL DOCTOR
                        $total_pago = 0;

                        // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LAS CONSULTAS REALIZADAS POR EL DOCTOR SELECCIONADO
                        $query_lista_consultas = "SELECT id_registro,fecha,pago,cantidad,descripcion, pago_consultas.id_subcategoria as id_subcategoria
                                                  FROM consultas,pago_consultas
                                                  WHERE consultas.id_subcategoria = pago_consultas.id_subcategoria
                                                  AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                                  AND estado = 'No pagado'
                                                  AND id_empleado = '$id_empleado'
                                                  ORDER BY fecha ASC";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
                        $resultado_lista_consultas = mysql_query($query_lista_consultas) or die(mysql_error());
                    ?>
                        <center>
                            <form name="form_pago_consulta" method="post" action="guardar_pago_consultas.php">
                                <table>
                                    <tr>
                                        <td colspan="5" style="text-align:center;"> <label style="font-size:24px;"> Pago de Consultas </label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" style="text-align:center;"> <label> Del: &nbsp; <?php echo $fecha_inicio; ?> Al: &nbsp; <?php echo $fecha_final; ?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" style="text-align:center;"> <label> <?php echo $ocupacion." ".$nombre_completo_doctor; ?> </label> </td>
                                    </tr>
                                    <tr>
                                        <th style="font-size:12px;"> N° de Registro </th>
                                        <th style="font-size:12px;"> Fecha </th>
                                        <th style="font-size:12px;"> Cantidad </th>
                                        <th style="font-size:12px;"> Pago </th>
                                        <th style="font-size:12px;"> Descripci&oacute;n </th>
                                    </tr>
                            <?php
                                // SE REALIZA UN CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                                while ( $row_lista_consultas = mysql_fetch_array($resultado_lista_consultas) )
                                {
                                    $id_registro = $row_lista_consultas['id_registro'];
                                    $id_subcategoria = $row_lista_consultas['id_subcategoria'];
                                    $fecha = $row_lista_consultas['fecha'];
                                    $fecha_separada = explode("-", $fecha);
                                    $fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
                                    $cantidad = $row_lista_consultas['cantidad'];
                                    $descripcion = $row_lista_consultas['descripcion'];
                                    $pago = $row_lista_consultas['pago'];                                                                                                            
                                    $total_consultas = $total_consultas + $cantidad;
                                    $total_pago = $total_pago + $pago;
                                    $contador++;

                                ?>
                                    <tr>
                                        <td> <input type="hidden" name="registro[]" value="<?php echo $id_registro; ?>" /> </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;"> <?php echo $id_registro; ?> </td>
                                        <td style="text-align:center;"> <?php echo $fecha_normal; ?> </td>
                                        <td style="text-align:center;"> <?php echo $cantidad; ?> </td>
                                        <td style="text-align:center;"> <?php echo "$ ".number_format($pago,2); ?> </td>
                                        <td style="text-align:left;"> <?php echo $descripcion; ?> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                                    </tr>
                                <?php
                                }

                                // SE VALIDA SI EL CONTADOR ES MAYOR O ES IGUAL A CERO
                                if ( $contador == 0 )
                                {
                                ?>
                                    <tr>
                                        <td colspan="5" style="text-align:center;"> <h3> No existen consultas registradas para el Dr(a) <?php echo $nombre_completo_doctor; ?> </h3> </td>
                                    </tr>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <tr>
                                        <td>  </td>
                                        <td>  </td>
                                        <td style="text-align:center; color:#ac1f1f; font-size:10px;"> Total de Consultas <br/> <?php echo $total_consultas; ?> </td>
                                        <td style="text-align:center; color:#ac1f1f; font-size:10px;"> Total de Pago <br/> <?php echo "$ ".number_format($total_pago,2); ?> </td>
                                    </tr>
                                </table>
                                <br/>
                                <?php
                                    // SE REALIZA QUERY QUE OBTIENE EN RESUMEN EL TOTAL DE CADA TIPO DE CONSULTA REALIZADA POR EL DOCTOR SELECCIONADO
                                    $query_resumen_categoria = "SELECT SUM(cantidad) As total_consultas,descripcion,SUM(pago) AS pago_total, pago_consultas.id_subcategoria as id 
                                                                FROM consultas,pago_consultas
                                                                WHERE consultas.id_subcategoria = pago_consultas.id_subcategoria
                                                                AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                                                AND estado = 'No pagado'
                                                                AND id_empleado = '$id_empleado'
                                                                GROUP BY descripcion
                                                                ORDER BY fecha ASC";

                                    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO OBTENIDO
                                    $resultado_resumen_categoria = mysql_query($query_resumen_categoria) or die(mysql_error());
                                ?>
                                    <table>
                                        <tr>
                                            <th> Total </th>
                                            <th> Descripci&oacute;n </th>
                                            <th>Comision</th>
                                            <th> Total a Pagar </th>
                                        </tr>
                                <?php
                                    $total_a_pagar = 0;
                                    $comision=0;
                                    $original=0;
                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                                    while ( $row_resumen = mysql_fetch_array($resultado_resumen_categoria) )
                                    {
                                        $total_consultas = $row_resumen['total_consultas'];
                                        $descripcion = $row_resumen['descripcion'];                                        
                                        $pago_total = $row_resumen['pago_total'];
                                        $id_sub=$row_resumen['id'];
                                        //Quitar la Comision
                                        $operaciones_comision=mysql_query('SELECT comision
                                                                           FROM comisiones_consultas
                                                                           WHERE id_subcategoria='.$id_sub.'
                                                                           AND id_empleado='.$id_empleado)
                                                                           or die('Operaciones para sacar la comision del empleado');
                                        $row_operaciones=mysql_fetch_array($operaciones_comision);
                                        $comision=$row_operaciones['comision'];
                                        $original=100-$comision;
                                        $pago_total=($pago_total*$comision)/$original;
                                        //Continuar con las operaciones
                                        $total_a_pagar = $total_a_pagar + $pago_total;
                                    ?>
                                        <tr>
                                            <td style="text-align:center;"> <?php echo $total_consultas; ?> </td>
                                            <td> <?php echo $descripcion; ?> </td>
                                            <td style="text-align:center;"><?php echo $comision."%"; ?></td>
                                            <td style="text-align:right;"> <?php echo "$".number_format($pago_total,2); ?> </td>                                        
                                        </tr>
                                        <tr>
                                            <td colspan="4"> <hr style="background-color:#e6e6e6; height:3px; border:none;"> </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                        <tr>
                                            <td> </td>
                                            <td> </td>
                                            <td style="text-align:center; color:#ac1f1f;"> Total a Pagar <br/> <?php echo "$ ".number_format($total_a_pagar,2); ?> </td>
                                        </tr>
                                    </table>
                                    <br/>
                                <?php
                                }
                            ?>
                            </form>
                        </center>
                        <div id="opciones" style="float:right; margin-right:70px;">
                            <form name="form_moldes" style="float:right; margin-left:20px;" method="post" action="pago_consultas_excel.php">
                                <div id="spanreloj"></div>
                                <input type="hidden" name="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                <input type="hidden" name="fecha_final" value="<?php echo $fecha_final_mysql; ?>" />
                                <input type="hidden" name="id_empleado" value="<?php echo $id_empleado; ?>" />
                                <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                            </form> &nbsp;&nbsp;
                            <a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_pago_consultas.php?fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_final_mysql; ?>&id_empleado=<?php echo $id_empleado; ?>','Reporte de Pago de Consultas','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1050, height=700');"> <img src="../img/print icon.png"/> </a>
                        </div> <!-- FIN DIV OPCIONES -->
                        <br/><br/>
                    <?php
                    }
                ?>
                <br/><br/>
            </div><!--Fin de area contenido -->
        </div><!--Fin de contenido pagina -->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>