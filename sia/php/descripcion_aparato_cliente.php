<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="wrapp">
			<div id="contenido_columna2">
				<div class="contenido_pagina">
					<div class="fondo_titulo1">
            			<div class="categoria">
            				Pacientes
            			</div>
                    </div><!--Fin de fondo titulo-->                        
                    <div class="area_contenido1">
                        <div class="contenido_proveedor">
   	<?php
					include("config.php");
					$id_cliente = $_GET["id_paciente"];
					$consulta_datos_cliente=mysql_query("SELECT nombre, paterno, materno FROM ficha_identificacion
																							WHERE id_cliente=".$id_cliente)
																							or die(mysql_error());
					$row_datos_cliente = mysql_fetch_array($consulta_datos_cliente);
					$nombre = $row_datos_cliente["nombre"];
					$paterno = $row_datos_cliente["paterno"];
					$materno = $row_datos_cliente["materno"];
	?>
                        <br />
                       		<label class="textos">Nombre del paciente: </label>
                        	<?php echo $nombre." ".$paterno." ".$materno; ?>
                        <br /><br />
                        <center>
                        	<table>
                            	<tr style="width:650px;">
                                	<th colspan="4">Relación de Aparatos-Paciente</th>
                            	</tr>
                            	<tr>
                                    <th width="100">Nº Registro</th>
                                    <th width="300">Modelo / Num serie</th>
                                    <th width="150">Estatus</th>
                                    <th></th>
                            	</tr>
 	<?php
					$consulta_relacion_aparatos = mysql_query("SELECT * FROM relacion_aparatos 
                                                            			WHERE id_cliente=".$id_cliente)
																		or die(mysql_error());
					while($row = mysql_fetch_array($consulta_relacion_aparatos)){
						$id_registro = $row["id_registro"];
						$id_modelo = $row["id_modelo"];
						$num_serie_cantidad = $row["num_serie_cantidad"];
						$id_estatus_relacion = $row["id_estatus_relacion"];
						
						$consulta_modelos=mysql_query("SELECT modelo FROM modelos 
																		WHERE id_modelo=".$id_modelo)
																		or die(mysql_error());
						$row_modelos=mysql_fetch_array($consulta_modelos);
						$modelo = $row_modelos["modelo"];
						
						$consulta_estatus_relacion = mysql_query("SELECT * FROM estatus_relaciones_aparatos
																			WHERE id_estatus_relacion=".$id_estatus_relacion)
																			or die(mysql_error());
						$row_estatus_relacion=mysql_fetch_array($consulta_estatus_relacion);
						$estatus_relacion = $row_estatus_relacion["estatus_relacion"];
	?>
                           		<tr>
                                    <td><?php echo $id_registro; ?></td>
                                    <td style="text-align:left;"><?php echo $modelo."  &nbsp; / &nbsp;  ".$num_serie_cantidad; ?></td>
                                    <td><?php echo $estatus_relacion; ?></td>
                                    <td><a href="descripcion_aparato.php?id_registro=<?php echo $id_registro; ?>">Ver</a></td>
  	<?php
					}
	?>
							</table>
                        </center>
                        	<p align="right">
                            	<a href="lista_pacientes_relacion_aparatos.php">
                            		<input name="accion" type="button" value="Volver" class="fondo_boton" />
                                </a>
                            </p>
                    	</div><!--Fin de contenido proveedor-->
                	</div><!--Fin area contenido-->
				</div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>