<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Agregar Ventilaciones Material </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Ventilaciones
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO1 -->
						<div class="area_contenido1">
							<br/>
							<?php
								include("config.php");
								$id_catalogo = $_GET['catalogo'];
								$id_cat_nivel1 = $_GET['cat_nivel1'];
								$query_estilo = mysql_query("SELECT estilo,foto
															 FROM estilos,catalogo
															 WHERE estilos.id_estilo = catalogo.id_estilo 
															 AND id_catalogo ='$id_catalogo'") or die (mysql_error());
								$row_estilo = mysql_fetch_array($query_estilo);
								$estilo = $row_estilo['estilo'];
								$imagen = $row_estilo['foto'];
								
								$query_material = mysql_query("SELECT material
															   FROM cat_nivel1,materiales
															   WHERE cat_nivel1.id_material = materiales.id_material
															   AND id_catalogo = '$id_catalogo'
															   AND id_cat_nivel1 = '$id_cat_nivel1'") or die (mysql_error());
								$row_material = mysql_fetch_array($query_material);
								$material = $row_material['material'];
								
								$query_ventilaciones = mysql_query("SELECT id_ventilacion,nombre_ventilacion,calibre
																	FROM ventilaciones ORDER BY id_ventilacion ASC") or die (mysql_error());
							?>
							<div class="titulos"> Estilo </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td> <label class="textos"> Nombre del Estilo: </label> </td>
											<td>  &nbsp;<label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $estilo; ?> </label> </td>
											<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
											<td> <img width="100" height="100" src="../moldes/estilos/<?php echo $imagen; ?>"/> </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Material </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td colspan="2"> 
                                            	<label class="textos"> Nombre del Material: </label><?php echo $material; ?> 
                                            </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Ventilaciones </div><!-- FIN DIV TITULOS -->
								<center>
								<form name="form_ventilacion_material" method="post" action="procesa_ventilaciones_material.php" class="valida_ventilacion">
									<table id="tabla_ventilaciones">
										<tr>
											<td> <input type="hidden" name="txt_catalogo" id="txt_catalogo" value="<?php echo $id_catalogo; ?>"/> </td>
										</tr>
										<tr>
											<td> <input type="hidden" name="txt_cat_nivel1" id="txt_cat_nivel1" value="<?php echo $id_cat_nivel1; ?>"/> </td>
										</tr>
										<tr>
											<td> <label class="textos"> Nombre de la Ventilación: </label> </td>
											<td>
												<select name="ventilacion_material">
													<option value="0"> --- Seleccione Ventilacion --- </option>
												<?php
												while( $row_ventilacion = mysql_fetch_array($query_ventilaciones) )
												{
													$id_ventilacion = $row_ventilacion['id_ventilacion'];
													$nombre_ventilacion = $row_ventilacion['nombre_ventilacion'];
													$calibre = $row_ventilacion['calibre'];
												?>
													<option value="<?php echo $id_ventilacion ?>"> <?php echo utf8_encode(ucwords(strtolower($nombre_ventilacion))); ?> | <?php echo $calibre ?> </option>
												<?php
												}
												?>
												</select>
											</td>
										</tr>
									</table>
									<div id="boton_ventilacion">
										<p align="right">
											<input type="submit" name="accion" id="accion" class="fondo_boton" value="Guardar" title="Guardar"/>
										</p>
									</div>
									<br/><br/>
								</form>
								</center>
						</div><!-- FIN DIV AREA CONTENIDO1 -->					
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html>