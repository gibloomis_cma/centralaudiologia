<?php
    // SE INICIA SESION PARA EL USUARIO QUE INGRESO AL SISTEMA
	session_start();

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include('config.php');

    //include("metodo_cambiar_fecha.php");

    // SE ESTABLECE LA FECHA DE ACUERDO A LA ZONA HORARIA DEL PAIS
    date_default_timezone_set('America/Monterrey');
    $script_tz = date_default_timezone_get();

    // SE DECLARA UNA VARIABLE CON LA FECHA ACTUAL DEL SISTEMA
    $fecha = date('d/m/Y');

    // SE RECIBE POR METODO GET EL FOLIO DEL VALE DE BATERIAS
    $vale_bateria = $_GET['folio_vale'];

    // SE DECLARA UNA VARIABLE CON EL ID DEL EMPLEADO QUE INICIO SESION EN EL SISTEMA
    $id_empleado_usuario_sistema = $_SESSION['id_empleado_usuario'];

    // SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO DEL EMPLEADO QUE SE ENCUENTRA EN EL SISTEMA
    $consultame_departamento_empleado = "SELECT nombre, id_empleado, id_departamento 
                                         FROM empleados 
                                         WHERE id_empleado = '$id_empleado_usuario_sistema'";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
    $resultado_consultame_departamento_empleado = mysql_query($consultame_departamento_empleado) or die(mysql_error());

    // SE ALMACENA EL RESULTADO DEL QUERY EN UNA VARIABLE EN FORMA DE ARREGLO
    $row_departamento_empleado = mysql_fetch_array($resultado_consultame_departamento_empleado);
    $id_departamento_empleado = $row_departamento_empleado['id_departamento'];
    $nombre_empleado = $row_departamento_empleado['nombre'];
    $id_empleado_logueado = $row_departamento_empleado['id_empleado'];

    // SE REALIZA QUERY QUE OBTIENE LOS NOMBRES DE LOS EMPLEADOS
    $query_nombre_empleados = "SELECT id_empleado,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_empleado
                               FROM empleados
                               ORDER BY nombre_completo_empleado ASC";

    // SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
    $resultado_nombre_empleados = mysql_query($query_nombre_empleados) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Vale de Baterías </title>
    <link rel="stylesheet" href="../css/style3.css" type="text/css"/>
    <script language="javascript" type="text/javascript" src="../js/jquery-1.7.1.js"></script>
    <script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
    <script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
    <script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            
            // Pongo el formato 12 horas
            if ( horas >= 12 ) 
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
            
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) 
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }		
        // Fin del script -->
    </script>
</head>
<body onLoad="muestraReloj()">
<div id="wrapp">
	<div id="contenido_columna2">
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria">
            		Bater&iacute;as
            	</div>
            </div><!--Fin de fondo titulo-->
        	<div class="area_contenido1"><!-- Desde aqui en adelante es el espacio que se trabaja en las paginas-->
        	<br />
				<div class="titulos"> Vale de Bater&iacute;as </div>
            	<br />
                <div class="contenido_proveedor">
                	<form name="forma1" id="form_vale_baterias" method="post" action="procesa_vale_bateria.php">
                        <input type="hidden" name="id_departamento_empleado" value="<?php echo $id_departamento_empleado; ?>" id="id_departamento_empleado" />
                <?php
				    if( $vale_bateria == "" ) 
                    {	
				?>
                   	    <input type="hidden" name="folio_vale" value="" />
						<div id="spanreloj"></div>
                        <table style="margin-left:-20px;">
                            <tr>
                                <td> <input type="hidden" name="responsable_sistema" value="<?php echo $id_empleado_logueado; ?>" /> </td>
                            </tr>
                        	<tr>
                            	<td id="alright"> <label class="textos"> Fecha: </label> </td>
                                <td id="alleft"> <input type="text" name="fecha" value="<?php echo $fecha; ?>" readonly="readonly" style="border:none; background-color:#7f7f7f; text-align:center; font-weight:bold; color:#FFF;" /> </td>
                                <td id="alright"> <label class="textos"> Responsable: </label> </td>
                                <td>
                                    <select name="responsable" id="responsable">
                                        <option value="0"> Seleccione Responsable </option>
                                <?php
                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
                                    while ( $row_empleados = mysql_fetch_array($resultado_nombre_empleados) ) 
                                    {
                                        $id_empleado_consultado = $row_empleados['id_empleado'];
                                        $nombre_empleado_consultado = $row_empleados['nombre_completo_empleado'];

                                        // SE BUSCA EL USUARIO LOGUEADO Y SE SELECCIONA
                                        if ( $id_empleado_usuario_sistema != "" && $id_empleado_usuario_sistema == $id_empleado_consultado ) 
                                        {
                                        ?>
                                            <option value="<?php echo $id_empleado_consultado; ?>" selected="selected"> <?php echo ucwords(strtolower($nombre_empleado_consultado)); ?> </option>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <option value="<?php echo $id_empleado_consultado; ?>"> <?php echo ucwords(strtolower($nombre_empleado_consultado)); ?> </option>
                                        <?php
                                        }   
                                    }
                                ?>
                                    </select>
                                </td>
							</tr>
                            <tr>
                                <td colspan="5"> <br/> </td>
                            </tr>
                            <tr>
                            	<td id="alright"> <label class="textos"> Uso: </label> </td>
                                <td id="alleft">
                                    <select name="uso" id="uso">
                                        <option value="0"> Seleccione Uso </option>
                                <?php
                                    // SE REALIZA QUERY QUE OBTIENE LOS DIFERENTES USOS DE LAS BATERIAS
                                    $consultar_uso_bateria = "SELECT id_uso_bateria, uso_bateria 
                                                              FROM uso_baterias"; 

                                    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
                                    $resultado_consultar_uso_bateria = mysql_query($consultar_uso_bateria) or die(mysql_error());

                                    // SE REALIZA UN CICLO PARA MOSTRAR LAS DIFERENTES OPCIONES
                                    while( $row1 = mysql_fetch_array($resultado_consultar_uso_bateria) )
                                    {
                                        $nombre_uso = $row1['uso_bateria'];
                                        $id_uso_bateria = $row1['id_uso_bateria'];
                                ?>
                                        <option value="<?php echo $id_uso_bateria; ?>"> <?php echo ucfirst(strtolower($nombre_uso)); ?> </option>
                                <?php
                                    }
                                ?>
                                    </select>
		                        </td>
                                <td id="alright"> <label class="textos"> Origen: </label> </td>
                                <td>
                                    <input type="hidden" name="id_origen" class="id_origen_bateria" />
                                    <select name="origen" id="origen">
                                        <option value="0"> Seleccione Origen </option>
                                <?php
                                    // SE REALIZA QUERY QUE OBTIENE LOS ALAMACENES
                                    $query_origen = "SELECT id_almacen,almacen
                                                     FROM almacenes";

                                    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
                                    $resultado_origen = mysql_query($query_origen) or die(mysql_error());

                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
                                    while ( $row_origen = mysql_fetch_array($resultado_origen) ) 
                                    {
                                        $id_almacen = $row_origen['id_almacen'];
                                        $almacen = $row_origen['almacen'];
                                    ?>
                                        <option value="<?php echo $id_almacen; ?>"> <?php echo ucfirst(strtolower($almacen)); ?> </option>
                                    <?php
                                    }                                    
                                ?>
                                    </select>
                                </td>
        	                </tr>
                            <tr>
                                <td colspan="5"> <br/> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Descripci&oacute;n: </label> </td>
                                <td> 
                                    <select name="desc1" id="desc1" class="baterias_vale">
                                        <option value="0"> Seleccione Bateria </option>
                                <?php
                                    // SE REALIZA QUERY QUE OBTIENE LA INFORMACION DE LAS BATERIAS
                                    $resultado2 = "SELECT descripcion, id_base_producto 
                                                   FROM base_productos
                                                   WHERE id_categoria = 4 
                                                   AND activo = 'Si'";

                                    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                    $resultado_resultado2 = mysql_query($resultado2) or die(mysql_error());

                                    // SE REALIZA CICLO QUE MUESTRA LOS RESULTADOS OBTENIDOS DEL QUERTY
                                    while( $row3 = mysql_fetch_array($resultado_resultado2) )
                                    {
                                        $descripcion = $row3["descripcion"];
                                        $id_base_producto = $row3["id_base_producto"];  
                                ?>          
                                        <option value="<?php echo $id_base_producto; ?>"> <?php echo $descripcion; ?> </option>   
                                <?php 
                                    }
                            ?>
                                    </select>
                                </td>
                                <td id="alright"> <label class="textos"> Cantidad: </label> </td>   
                                <td>
                                    <select name="cantidad2" id="cantidad2">
                                        <option value="0"> Seleccione Cantidad </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"> <br/> </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top;"> <label class="textos"> Observaciones: </label> </td>
                                <td colspan="4">
                                    <textarea name="observaciones" id="observaciones" cols="40" rows="4" style="resize:none;">-Ninguna-</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"> <br/> </td>
                            </tr>
                            <tr>
                            	<td id="alright" colspan="4">
		                            <input type="submit" name="accion" value="Agregar" title="Agregar" class="fondo_boton btn_agregar"/>        
                        		</td>
                        	</tr>
   						</table>
					<?php 
					}
                    else
                    {
							$consulta_datos_vale = mysql_query("SELECT uso_de_bateria, responsable, fecha,observaciones 
																	FROM vales_baterias 
																	WHERE id_vale_bateria=".$vale_bateria) or die(mysql_error());									
							$row5 = mysql_fetch_array($consulta_datos_vale);
							$uso = $row5["uso_de_bateria"];
							$responsable = $row5["responsable"];
							$fecha_vale = $row5["fecha"];	
                            $observaciones_vale_bateria = $row5['observaciones'];		
							$consulta_responsable = mysql_query("SELECT nombre, paterno, materno 
																	FROM empleados 
																	WHERE id_empleado=".$responsable) or die(mysql_error());
							$row7 = mysql_fetch_array($consulta_responsable);
							$nombre = $row7["nombre"];
							$paterno = $row7["paterno"];
							$materno = $row7["materno"];
							$consulta_uso_vale = mysql_query("SELECT uso_bateria 
																	FROM uso_baterias
																	WHERE id_uso_bateria=".$uso) or die(mysql_error());
							$row8 = mysql_fetch_array($consulta_uso_vale);
							$uso_vale = $row8["uso_bateria"];
				            $fecha_vale_separada = explode("-", $fecha_vale);
							$fecha_normal = $fecha_vale_separada[2]."/".$fecha_vale_separada[1]."/".$fecha_vale_separada[0];
					?>
                    <input type="hidden" name="uso" value="<?php echo $uso; ?>" />
                    	<table style="width:700px;">
                        	<tr>
                            	<td id="alright">
                    				<label class="textos">N° vale: </label>
                        		</td><td id="alleft">
                        			<input name="folio_vale" type="text" value="<?php echo $vale_bateria; ?>" size="4" maxlength="4" readonly="readonly" style="background-color:#7f7f7f; border:none; text-align:center; font-weight:bold; color:#FFF;" />
                        		</td><td id="alright">
   									<label class="textos">Fecha: </label>							
                        		</td><td id="alleft">
									<?php echo $fecha_normal; ?>
                                </td>
                       		</tr><tr>
                            	<td id="alright">
                       				<label class="textos">Responsable: </label>
								</td><td id="alleft">
									<?php echo $nombre." ".$paterno." ".$materno; ?>
                        		</td><td id="alright">
    								<label class="textos">Uso: </label>
                                </td><td id="alleft">
									<?php echo $uso_vale; ?>
                                    <input type="hidden" name="uso" id="uso1" value="<?php echo $uso; ?>"/>
                        		</td>
                    		</tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Observaciones: </label> </td>
                                <td> <label><?php echo ucfirst(strtolower($observaciones_vale_bateria)); ?></label> </td>
                                <td id="alright"> <label class="textos"> Origen: </label> </td>
                                <td>
                                    <input type="hidden" name="id_origen" class="id_origen_bateria" />
                                    <select name="origen" id="origen2">
                                        <option value="0"> Seleccione Origen </option>
                                <?php
                                    // SE REALIZA QUERY QUE OBTIENE LOS ALAMACENES
                                    $query_origen = "SELECT id_almacen,almacen
                                                     FROM almacenes";

                                    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
                                    $resultado_origen = mysql_query($query_origen) or die(mysql_error());

                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
                                    while ( $row_origen = mysql_fetch_array($resultado_origen) ) 
                                    {
                                        $id_almacen = $row_origen['id_almacen'];
                                        $almacen = $row_origen['almacen'];
                                    ?>
                                        <option value="<?php echo $id_almacen; ?>"> <?php echo ucfirst(strtolower($almacen)); ?> </option>
                                    <?php
                                    }                                    
                                ?>
                                    </select>
                                </td>
                            </tr>
									<input name="id_origen" class="id_origen_bateria2" type="hidden" />
                            <tr>
                            	<td id="alright">
        							<label class="textos">Descripción: </label>
                        		</td><td id="alleft">
                                    <select name="desc1" id="desc1" class="baterias_vale2">
                                        <option value="0" selected="selected">Seleccione</option>
                                        <?php
                                            $resultado2 = mysql_query("SELECT descripcion, id_base_producto FROM base_productos 
                                                                                                            WHERE id_categoria=4 AND activo='Si'") 
                                                                                                            or die(mysql_error());
                                            while($row3 = mysql_fetch_array($resultado2)){
                                                $descripcion = $row3["descripcion"];
                                                $id_base_producto = $row3["id_base_producto"];	
                                        ?>			
                                        <option value="<?php echo $id_base_producto; ?>"> <?php echo $descripcion; ?> </option>	
                                        <?php 
                                            }
                                        ?>
                                    </select>
                        		</td><td id="alright">
									<label class="textos">Cantidad: </label>
                        		</td><td id="alleft">
                                    <select name="cantidad2" id="cantidad3">
                                        <option value="0">Seleccione</option>
                                    </select>
                        		</td>
							</tr><tr>
                            	<td colspan="4" id="alright">
		                        <input name="accion" type="submit" value="Agregar" class="fondo_boton btn_agregar"/>
        						</td>  
                            </tr>
                        </table>
                        <table align="center">
                            <tr>
                            	<th width="100">Sucursal</th>
                            	<th width="150">Origen</th>
                                <th width="250">Descripcion</th>
                                <th width="100">Cantidad</th>
                                <th></th>
                            </tr>
  							<?php 
								/* Consulta la descripcion del vale de bateria */
								$consulta_descripcion_vale = mysql_query("SELECT codigo, cantidad, origen, id_registro
																				FROM descripcion_vale_baterias
																				WHERE id_vale_bateria=".$vale_bateria) 
																				or die(mysql_error());
								while($row9 = mysql_fetch_array($consulta_descripcion_vale)){
									$codigo = $row9["codigo"];
									$id_registro_vale_bateria = $row9["id_registro"];
									$cantidad = $row9["cantidad"];
									$origen = $row9["origen"];
									/* Consulta la descripcion de la bateria */
									$consulta_descripcion_refaccion = mysql_query("SELECT descripcion 
																						FROM base_productos
																						WHERE id_base_producto=".$codigo) 
																						or die(mysql_error());
									$row6 = mysql_fetch_array($consulta_descripcion_refaccion);
									$descripcion = $row6["descripcion"];
									/* Consulta el almacen de origen de la bateria */
									$consulta_origen_bateria = mysql_query("SELECT almacen, id_sucursal FROM almacenes 
																							WHERE id_almacen=".$origen)
																							or die(mysql_error());
									$row_origen_almacen = mysql_fetch_array($consulta_origen_bateria);
									$origen_almacen = $row_origen_almacen["almacen"];
									$id_sucursal = $row_origen_almacen["id_sucursal"];
									/* Consulta la sucursal de origen de la bateria */
									$consulta_origen_sucursal = mysql_query("SELECT nombre FROM sucursales
																							WHERE id_sucursal=".$id_sucursal)
																							or die(mysql_error());
									$row_origen_sucursal=mysql_fetch_array($consulta_origen_sucursal);
									$sucursal_origen = $row_origen_sucursal["nombre"];
							?>
                            <tr>
                            	<td><?php echo $sucursal_origen; ?></td>
                            	<td><?php echo $origen_almacen; ?></td>
                                <td><?php echo $descripcion; ?></td>
                                <td><?php echo $cantidad; ?></td>
                                <td>
                                	<a href="eliminar_producto_bateria.php?id_registro_descripcion_vale_bateria=<?php echo $id_registro_vale_bateria; ?>">
                                		<img src="../img/delete.png">
                                	</a></td>
                            </tr>
   							<?php 
								}
							?>
                   		</table>
                    <p align="right">
              			<input name="accion" type="submit" value="Cancelar" class="fondo_boton"/>
              		&nbsp;&nbsp;&nbsp;
              			<input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
        			</p>
    				<?php		
						}
					?>
                    <input name="fecha" value="<?php echo $fecha; ?>" type="hidden" style="width:300px;" />
                    <div id="spanreloj"></div>
                    
					
					</form> 
               </div><!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>