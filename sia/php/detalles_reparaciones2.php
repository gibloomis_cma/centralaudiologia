<?php
	if(!isset($_GET["folio"])){header("Location: http://sia.syfors.com");}
	session_start();
	include("config.php");
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha_actual = date("d/m/Y");
	$hora = date("h:i:s A");
	$mov=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Reparaciones</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script type="text/javascript">
	//Declaro variable para saber si esta habilitado el check_reparado
	var bloqueo = 0;
	var servicios = 0;
	//Funcion para bloquear los el check_reparado
	function bloquear_reparado(check){			
		$('#check_reparado').attr('disabled', 'disabled');
		bloqueo++;
		$('input[name="descripcion_'+check+'"]').attr('onClick', 'desbloquear_reparado('+check+')');
	}
	function desbloquear_reparado(check){
		bloqueo--;
		if(bloqueo==0){
			$('#check_reparado').removeAttr('disabled');
		}
		$('input[name="descripcion_'+check+'"]').attr('onClick', 'bloquear_reparado('+check+')');
	}
	function bloquear_servicios(){
		if(servicios==0){
			servicios=1;			
			<?php
				$consultarServicios=mysql_query('SELECT * FROM servicios')
												or die('Consultar Servicios Para Validacion');
				while($row_consultaServicios=mysql_fetch_array($consultarServicios)){
					$idServicio=$row_consultaServicios['id_servicio'];
			?>
			$('input[name="descripcion_'+<?php echo $idServicio; ?>+'"]').attr('disabled','disabled');
			<?php
				}
			?>
			$('input[name="estimado"]').attr('value','0');
			//Poner valor para evitar validacion
			$('#precio_total_servicios_laboratorio').removeAttr('readonly');
			$('#precio_total_servicios_laboratorio').attr('value','300');
			$('#precio_total_servicios_laboratorio').attr('readonly', 'readonly');
		}else{
			servicios=0;			
			<?php
				$consultarServicios=mysql_query('SELECT * FROM servicios')
												or die('Consultar Servicios Para Validacion');
				while($row_consultaServicios=mysql_fetch_array($consultarServicios)){
					$idServicio=$row_consultaServicios['id_servicio'];
			?>
			$('input[name="descripcion_'+<?php echo $idServicio; ?>+'"]').removeAttr('disabled');
					
			<?php
				}
			?>			
			//Regresa el 0 a el total por servicios
			$('#precio_total_servicios_laboratorio').removeAttr('readonly');
			$('#precio_total_servicios_laboratorio').attr('value','0');
			$('#precio_total_servicios_laboratorio').attr('readonly', 'readonly');
		}
	}
</script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Reparaciones
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">	
  	<?php 
		//include("metodo_cambiar_fecha.php");
		/* Obtengo las variables que mande del otro formulario */
		$num_folio_reparacion = $_GET["folio"];/* Folio de N° de Reparacion */
		if(isset($_GET['folio_vale'])){
			$vale_refaccion = $_GET["folio_vale"]; /*Numero de folio del vale de refaccion */
		}else{
			$vale_refaccion="";
		}
		/* Consulto los datos de la tabla reparaciones segun el numero de folio */
		$consulta_datos_de_num_reparacion = mysql_query("SELECT * FROM reparaciones 
														 WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());													
		
		$row = mysql_fetch_array($consulta_datos_de_num_reparacion);
		$id_cliente = $row["id_cliente"];
		$nombre = $row["nombre"];
		$paterno = $row["paterno"];
		$materno = $row["materno"];
		$telefono = $row["descripcion"];
		$calle = $row["calle"];
		$num_exterior = $row["num_exterior"];
		$num_interior = $row["num_interior"];
		$codigo_postal = $row["codigo_postal"];
		$colonia = $row["colonia"];
		$id_ciudad = $row["id_ciudad"];
		$id_estado = $row["id_estado"];
		$fecha_entrada = $row["fecha_entrada"];
		$fecha_entrada_separada = explode("-", $fecha_entrada);
		$id_modelo = $row["id_modelo"];
		$num_serie = $row["num_serie"];
		$id_estatus_reparaciones = $row["id_estatus_reparaciones"];
		$descripcion = $row["descripcion_problema"];
		$garantia_reparacion = $row['reparacion'];
		$garantia_adaptacion = $row['adaptacion'];
		$garantia_venta = $row['venta'];
		$aplica_garantia = $row['aplica_garantia'];
		
		/* Consulto el estado */	
		$consulta_estado = mysql_query("SELECT estado FROM estados 
										WHERE id_estado=".$id_estado) or die(mysql_error());
		$row2 = mysql_fetch_array($consulta_estado);
		$estado = $row2["estado"];
		
		/* Consulto la ciudad */
		$consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades 
											WHERE id_ciudad=".$id_ciudad) or die(mysql_error());
		$row3 = mysql_fetch_array($consulta_ciudad);
		$ciudad = $row3["ciudad"];
		
		/* Consulto el modelo del auxiliar */	
		$consulta_modelo = mysql_query("SELECT descripcion, subcategoria, id_articulo, base_productos_2.id_subcategoria AS id_subcategoria 
										FROM base_productos_2, subcategorias_productos_2
										WHERE id_base_producto2 =".$id_modelo." AND 
										subcategorias_productos_2.id_subcategoria = base_productos_2.id_subcategoria") or die(mysql_error());
		$row5 = mysql_fetch_array($consulta_modelo);
		$modelo = $row5["descripcion"];
		$marca = $row5["subcategoria"];
		$id_subcategoria_modelo = $row5["id_subcategoria"];
		$id_articulo_modelo = $row5["id_articulo"];
		
		/* Consulta tipo de Aparato */
		$consulta_tipo_aparato=mysql_query("SELECT modelo_aparato FROM tipos_modelos_aparatos, modelos_2
											WHERE id_modelo2='".$id_articulo_modelo."' AND 
											id_subcategoria='".$id_subcategoria_modelo."' AND
											modelos_2.id_tipo_modelo=tipos_modelos_aparatos.id_tipo_modelo") or die(mysql_error());
		$row_tipo_aparato=mysql_fetch_array($consulta_tipo_aparato);
		$tipo_aparato=$row_tipo_aparato["modelo_aparato"];
		$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
		/* Consulta tabla de accesorios_problemas */
		$consulta_accesorios_problemas=mysql_query("SELECT * FROM accesorios_problemas 
															WHERE folio_num_reparacion=".$num_folio_reparacion)
															or die(mysql_error());
		$row_accesorios_problemas=mysql_fetch_array($consulta_accesorios_problemas);
		$estuche = $row_accesorios_problemas["estuche"];
		$molde = $row_accesorios_problemas["molde"];
		$pila = $row_accesorios_problemas["pila"];
		$no_se_oye = $row_accesorios_problemas["no_se_oye"];
		$portapila = $row_accesorios_problemas["portapila"];
		$circuito = $row_accesorios_problemas["circuito"];
		$se_oye_dis = $row_accesorios_problemas["se_oye_dis"];
		$microfono = $row_accesorios_problemas["microfono"];
		$tono = $row_accesorios_problemas["tono"];
		$falla = $row_accesorios_problemas["falla"];
		$receptor = $row_accesorios_problemas["receptor"];
		$caja = $row_accesorios_problemas["caja"];
		$gasta_pila = $row_accesorios_problemas["gasta_pila"];
		$control_volumen = $row_accesorios_problemas["control_volumen"];
		$contactos_pila = $row_accesorios_problemas["contactos_pila"];
		$cayo_piso = $row_accesorios_problemas["cayo_piso"];
		$switch = $row_accesorios_problemas["switch"];
		$cambio_tubo = $row_accesorios_problemas["cambio_tubo"];
		$diagnostico = $row_accesorios_problemas["diagnostico"];
		$bobina = $row_accesorios_problemas["bobina"];
		$cableado = $row_accesorios_problemas["cableado"];
		$limpieza = $row_accesorios_problemas["limpieza"];
	?>
    		<br />
            	<div class="contenido_proveedor">
                <form name="forma1" id="form_detalles_reparaciones" action="cambiar_estatus_reparaciones.php" method="post" class="validacion_laboratorio">
                    <input type="hidden" name="txt_reparacion" value="<?php echo $garantia_reparacion; ?>" />
                    <input type="hidden" name="txt_adaptacion" value="<?php echo $garantia_adaptacion; ?>" />
                    <input type="hidden" name="txt_venta" value="<?php echo $garantia_venta; ?>" />
                    <fieldset>
                    <legend> Datos Generales </legend>
                    <label class="textos"> N° Nota: </label><?php echo $num_folio_reparacion; ?>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <input name="id_empleado" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos"> Fecha de Entrada: </label><?php echo $fecha_entrada_normal; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos"> Fecha Actual: </label><input name="fecha_actual" type="text" value="<?php echo $fecha_actual; ?>" readonly="readonly" />
                    <input name="hora" type="hidden" value="<?php echo $hora; ?>" />
                <br />
                    <label class="textos">Recibimos de: </label><?php echo $nombre." ".$paterno." ".$materno; ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tel. </label>
 	<?php 
		if($id_cliente == 0){
			echo $telefono;	
		}else{
			/* Si el cliente esta registrado, consulta el telefono de la tabla contactos clientes */
			$consulta_telefono = mysql_query("SELECT descripcion FROM contactos_clientes 
											  WHERE id_cliente =".$id_cliente)or die(mysql_error());
			$row4 = mysql_fetch_array($consulta_telefono);
			$telefono_cliente = $row4["descripcion"];
			echo $telefono_cliente;										
		}
	?>
                <br /><br />
                    <label class="textos">Direccion: </label><?php echo $calle." #".$num_exterior." ".$num_interior." Col. ".$colonia." Cp.".$codigo_postal;?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Localidad: </label><?php echo utf8_encode($ciudad.", ".$estado); ?>
                    </fieldset>
                <br />
                    <fieldset>
                    <legend>Datos del Aparato</legend>
                    <label class="textos">Marca: </label><?php echo $marca; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<label class="textos">Estilo de Aparato: </label><?php echo $tipo_aparato; ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      
                    <label class="textos">Modelo: </label><?php echo $modelo; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Num. Serie: </label><?php echo $num_serie; ?>
              	<br /><br />
                	<label class="textos">Accesorios dejados por el paciente: </label><br /><br />
    	<?php 
				if($estuche==1){echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Estuche:</label> Si";}
				if($molde==1){echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Molde:</label> Si";}
				if($pila==1){echo "&nbsp;&nbsp;&nbsp;&nbsp; <label class='textos'>Pila:</label> Si";}
		?>
        		<br /><br />
        			<label class="textos">Posibles problemas: </label><br /><br />
        <?php
				if($tono==1){echo "&nbsp;&nbsp; <label class='textos'>Tono:</label> Si ";}
				if($caja==1){echo "&nbsp;&nbsp; <label class='textos'>Caja:</label> Si ";}
				if($portapila==1){echo "&nbsp;&nbsp; <label class='textos'>Portapila:</label> Si ";}
				if($circuito==1){echo "&nbsp;&nbsp; <label class='textos'>Circuito:</label> Si <br /><br />";}		
				if($gasta_pila==1){echo "&nbsp;&nbsp; <label class='textos'>Gasta pila:</label> Si ";}
				if($switch==1){echo "&nbsp;&nbsp; <label class='textos'>Switch:</label> Si ";}
				if($cableado==1){echo "&nbsp;&nbsp; <label class='textos'>Cableado:</label> Si ";}
				if($limpieza==1){echo "&nbsp;&nbsp; <label class='textos'>Limpieza:</label> Si <br /><br />";}
				if($microfono==1){echo "&nbsp;&nbsp; <label class='textos'>Microfono:</label> Si ";}
				if($receptor==1){echo "&nbsp;&nbsp; <label class='textos'>Receptor:</label> Si ";}
				if($cambio_tubo==1){echo "&nbsp;&nbsp; <label class='textos'>Cambio de tubo al molde:</label> Si <br /><br />";}
				if($cayo_piso==1){echo "&nbsp;&nbsp; <label class='textos'>Cayo al piso:</label> Si ";}
				if($diagnostico==1){echo "&nbsp;&nbsp; <label class='textos'>Diagnostico:</label> Si ";}
				if($bobina==1){echo "&nbsp;&nbsp; <label class='textos'>Bobina Telefonica:</label> Si ";}
				if($no_se_oye==1){echo "&nbsp;&nbsp; <label class='textos'>No se oye nada:</label> Si <br /><br />";}
				if($control_volumen==1){echo "&nbsp;&nbsp; <label class='textos'>Control de Volumen:</label> Si ";}	
				if($contactos_pila==1){echo "&nbsp;&nbsp; <label class='textos'>Contactos de Pila:</label> Si ";}
				if($se_oye_dis==1){echo "&nbsp;&nbsp; <label class='textos'>Se oye distorcionado:</label> Si ";}
				if($falla==1){echo "&nbsp;&nbsp; <label class='textos'>Falla de vez en cuando:</label> Si";}
		?>
                <br /><br />
                	<label class="textos"> Tipo de Garant&iacute;a: </label>
                	<br/>&nbsp;
                	<label> Garant&iacute;a de Adaptaci&oacute;n </label>
	                	<?php 
	                		if ( $garantia_adaptacion == "si" )
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
	                		<?php
	                		}
	                		else
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
	                		<?php
	                		}
	                	?>
                	&nbsp;
                	<label> Garant&iacute;a de Reparaci&oacute;n </label>
	                	<?php
	                		if ( $garantia_reparacion == "si" )
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
	                		<?php
	                		}
	                		else
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
	                		<?php
	                		}
	                	?>
	                &nbsp;
                	<label> Auxiliar para Venta </label>
                		<?php
	                		if ( $garantia_venta == "si" )
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
	                		<?php
	                		}
	                		else
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
	                		<?php
	                		}
	                	?>
	                &nbsp;
                	<label> Ninguna </label>
                		<?php
	                		if ( $garantia_venta == "" && $garantia_reparacion == "" && $garantia_adaptacion == "" )
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Checked2.jpg' />
	                		<?php
	                		}
	                		else
	                		{
	                		?>
	                			<img width='18' height='18' src='../librerias/ScrewDefaultButtons/images/checkbox_Unchecked2.jpg' />
	                		<?php
	                		}
	                	?>
                <br/><br/>
                    <label class="textos">Descripción del Problema: </label>
                    <?php echo ucfirst(strtolower($descripcion)); ?>
                <br /><br />
                    <label class="textos">Estatus del aparato: </label>
                    <select name="id_estatus_reparaciones">
   	<?php
			/* Si estatus es igual a 15.-Recepcion Matriz, entonces muestra la opcion siguiente */
			if($id_estatus_reparaciones == 15){
  	?>
    					<option value="15" disabled="disabled" selected="selected">Recepcion Matriz</option>
                        <option value="5"> Laboratorio, en Reparación</option>
                    </select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php
			}
			/* Si estatus es igual a 16.-Recepcion Ocolusen, entonces muestra la opcion siguiente */
			elseif($id_estatus_reparaciones == 16){
	?>
    					<option value="16" disabled="disabled" selected="selected">Recepcion Ocolusen</option>
                        <option value="5"> Laboratorio, en Reparación</option>
                    </select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php	
				
			}
			/* Si estatus es igual a 5.-Laboratorio, en Reparacion, entonces muestra la opcion siguiente */
			if($id_estatus_reparaciones == 5)
			{ 
	?>
					<option value="5" selected="selected">Laboratorio, en Reparación</option>
    				<option value="15">Recepcion Matriz</option>
                	<option value="16">Recepcion Ocolusen</option>
                </select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
				</fieldset>
	<?php
			}
	?> 
    	<?php
			/* Si estatus es igual a 4.-Presupuesto, entonces muestra el estatus actual */
			if($id_estatus_reparaciones == 4)
			{
	?>
    					<option value="4" selected="selected" disabled="disabled">Presupuesto</option>
                 	</select>
    				</fieldset>
    			<br />
    <?php
			}			
			/* Si estatus es igual a 7.-Laboratorio, Autorizado, entonces solo muestra la opcion que pueda escoger */
			if($id_estatus_reparaciones == 7){
	?>
    					<option value="7" selected="selected" disabled="disabled">Laboratorio, Autorizado</option>
                        <option value="12">Laboratorio, No reparado</option>
                 	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
   	<?php		
			}
			/* Si estatus es igual a 12.-Laboratorio, No reparado, entonces solo muestra la opcion que pueda escoger */
			if($id_estatus_reparaciones == 12){
	?>
						<option value="12" selected="selected" disabled="disabled">Laboratorio, No reparado</option>
   	<?php
				$consulta_movimientos_folio=mysql_query("SELECT id_registro, id_estatus_reparaciones 
														FROM movimientos
														WHERE folio_num_reparacion=".$num_folio_reparacion." AND
														 (id_estatus_reparaciones='15' OR id_estatus_reparaciones='16') 
														 ORDER BY id_registro DESC")
														 or die(mysql_error());
				$row_movimientos_folio=mysql_fetch_array($consulta_movimientos_folio);
				$id_estatus_reparaciones_origen = $row_movimientos_folio["id_estatus_reparaciones"];
				if($id_estatus_reparaciones_origen == 15){
	?>
    					<option value="2">Recepcion Matriz, No reparado</option>
    <?php	
				}else{
	?>	
    					<option value="13">Recepcion Ocolusen, No reparado</option>
    <?php	
				}
	?>
                 	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
	<?php 	
        	}
			/* Si estatus es igual a 8.-Laboratorio, No autorizado, entonces solo muestra la opcion que pueda escoger */
			if($id_estatus_reparaciones == 8){
	?>
    					<option value="8" selected="selected" disabled="disabled">Laboratorio, No autorizado</option>
   	<?php
				$consulta_movimientos_folio=mysql_query("SELECT id_registro, id_estatus_reparaciones 
														FROM movimientos
														WHERE folio_num_reparacion=".$num_folio_reparacion." AND
														 (id_estatus_reparaciones='15' OR id_estatus_reparaciones='16')
														  ORDER BY id_registro DESC")
														 or die(mysql_error());
				$row_movimientos_folio=mysql_fetch_array($consulta_movimientos_folio);
				$id_estatus_reparaciones_origen = $row_movimientos_folio["id_estatus_reparaciones"];
				if($id_estatus_reparaciones_origen == 15){
	?>
    					<option value="2">Recepcion Matriz, No reparado</option>
    <?php	
				}else{
	?>	
    					<option value="13">Recepcion Ocolusen, No reparado</option>
    <?php	
				}
	?>
                	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php			
			}
			/* Si estatus es igual a 2.-Recepcion Matriz, No reparado, muestra las siguientes opciones */
			if($id_estatus_reparaciones == 2){
	?>			
    					<option value="2" selected="selected" disabled="disabled">Recepcion Matriz, No reparado</option>
                        <option value="13">Recepcion Ocolusen, No reparado</option>
                        <option value="11">Almacen</option>
                  	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>	
	<?php			
			}
			/* Si estatus es igual a 13.-Recepcion Ocolusen, No reparado, muestra las siguientes opciones */
			if($id_estatus_reparaciones == 13){
	?>			
    					<option value="13" selected="selected" disabled="disabled">Recepcion Ocolusen, No reparado</option>
                        <option value="2">Recepcion Matriz, No reparado</option>
                        <option value="11">Almacen</option>
                  	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>	
	<?php	
			}	
			/* Si estatus es igual a 3.-Entregado, muestra solo la opcion escogida */
			if($id_estatus_reparaciones == 3){
	?>
    					 <option value="3" selected="selected" disabled="disabled">Entregado</option>
                 	</select>
                    </fieldset>
    <?php							
			}
			/* Si estatus es igual a 6.-Laboratorio, Reparado, entonces muestra la opcion que se puede escoger */
			if($id_estatus_reparaciones == 6){
	?>
    					 <option value="6" selected="selected" disabled="disabled">Laboratorio, Reparado</option>
   	<?php
				$consulta_movimientos_folio=mysql_query("SELECT id_registro, id_estatus_reparaciones 
														FROM movimientos
														WHERE folio_num_reparacion=".$num_folio_reparacion." AND
														 (id_estatus_reparaciones='15' OR id_estatus_reparaciones='16')
														  ORDER BY id_registro DESC")
														 or die(mysql_error());
				$row_movimientos_folio=mysql_fetch_array($consulta_movimientos_folio);
				$id_estatus_reparaciones_origen = $row_movimientos_folio["id_estatus_reparaciones"];
				if($id_estatus_reparaciones_origen == 15){
	?>
    					<option value="9">Recepcion Matriz, Reparado</option>
    <?php	
				}elseif($id_estatus_reparaciones_origen == 16){
	?>	
    					<option value="14">Recepcion Ocolusen, Reparado</option>
    <?php	
				}
	?>
                	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php
			}
			/* Si estatus es igual a 9.-Recepcion Matriz, Reparado, muestra la siguientes opciones */
			if($id_estatus_reparaciones == 9){
	?>
    					 <option value="9" selected="selected" disabled="disabled">Recepcion Matriz, Reparado</option>
                         <option value="14">Recepcion Ocolusen, Reparado</option>
                         <option value="11">Almacen</option>
                 	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php	
			}
			/* Si estatus es igual a 14.- Recepcion Ocolusen, Reparado, muestra las siguientes opciones */
			elseif($id_estatus_reparaciones == 14){
	?>
    					 <option value="14" selected="selected" disabled="disabled">Recepcion Ocolusen, Reparado</option>
                        <option value="9">Recepcion Matriz, Reparado</option>
                         <option value="11">Almacen</option>
                 	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php		
			}
			/* Si el estatus es igual a 11.-Almacen, entonces muestra la siguiente informacion */
			if($id_estatus_reparaciones == 11){
	?>
    					 <option value="11" selected="selected" disabled="disabled">Almacen</option>
                         <option value="9">Recepcion Matriz, Reparado</option>
                         <option value="14">Recepcion Ocolusen, Reparado</option>
                   	</select>
                    <input name="accion" type="submit" value="Cambiar Estatus" class="fondo_boton" />
                    </fieldset>
    <?php
				
			}
			/* Si el estatus es igual a 5.-Laboratorio, en Reparacion entonces muestra la siguiente informacion */
			if($id_estatus_reparaciones == "5"){
	?> 
    			<br />
                    <fieldset>
                    <legend>Presupuesto</legend>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos">Tipo de Refacción: </label>
                    <select name="subcategoria" id="tipo_subcategoria">
                        <option value="0" >Seleccione</option>
 				<?php 
					$consulta_refacciones = mysql_query("SELECT * FROM subcategorias_productos 
														 WHERE id_categoria= '1'") or die(mysql_error());
					while( $row = mysql_fetch_array($consulta_refacciones) )
					{
						$nombre_subcategoria = $row["subcategoria"];
						$id_subcategoria = $row["id_subcategoria"];
   				?>
                        <option value="<?php echo $id_subcategoria; ?>"> <?php	echo $nombre_subcategoria; ?> </option>
    			<?php
          			}
    			?> 
                    </select>
             	<br /><br />
                    <label class="textos">Descripción: </label>
                    <select name="descripcion" id="id_articulo">
                        <option value="0" >Seleccione</option>
                    </select>
                    <label class="textos">Cantidad: </label>
					<input name="cantidad" type="text" value="1" size="5" />
                <p align="right">
                    <input name="agregar_refaccion" type="submit" class="fondo_boton" value="Agregar Refaccion" />
                </p>
                	  <table align="center">
                        <tr>
                            <th width="300"> Descripci&oacute;n </th>
                            <th width="65"> Cantidad </th>
                            <th> Costo Unitario </th>
                            <th> Existencia </th>
                            <th width="15"> </th>
                        </tr>
     	<?php
			$consulta_vales_reparaciones=mysql_query("SELECT * FROM vales_reparacion 
																WHERE folio_num_reparacion=".$num_folio_reparacion)
																or die(mysql_error());
			$cantidad_total_refacciones = 0;
			while($row_vales_reparacion=mysql_fetch_array($consulta_vales_reparaciones))
			{
				$id_registro_reparacion = $row_vales_reparacion["id_registro"];
				$codigo = $row_vales_reparacion["codigo"];
				$cantidad = $row_vales_reparacion["cantidad"];
				$costo_unitario = $row_vales_reparacion["costo_unitario"];
				$cosnulta_costo_unitario=mysql_query("SELECT descripcion, num_serie_cantidad FROM base_productos, inventarios 
																	WHERE base_productos.id_base_producto = inventarios.id_articulo AND base_productos.id_base_producto =".$codigo)or die(mysql_error());
				$row_costo_unitario=mysql_fetch_array($cosnulta_costo_unitario);
				$descripcion_codigo=$row_costo_unitario["descripcion"];
				$existencia=$row_costo_unitario["num_serie_cantidad"];
		?>
        				<tr>
                        	<td> <?php echo ucfirst(strtolower($descripcion_codigo)); ?> </td>
                            <td style="text-align:center;"> <?php echo $cantidad; ?> </td>
                            <td style="text-align:center;"> $<?php echo number_format($costo_unitario,2); ?> </td>
                            <td style="text-align:center;"><?php echo $existencia; ?></td>
                            <td> <a href="eliminar_vale_reparacion.php?id_registro=<?php echo $id_registro_reparacion; ?>&folio_reparacion=<?php echo $num_folio_reparacion; ?>"> <img src="../img/modify.png" /> </a> </td>
                        </tr>
        <?php
				$cantidad_total_refacciones +=($cantidad * $costo_unitario);
			}
		?>
                    </table>
                    <input name="costo_Total" id="costo_Total" type="hidden" value="<?php echo $cantidad_total_refacciones; ?>" readonly="readonly"/>					
               	<br />
                	<table id="modulo">
                    <tr align="center">
                    	<td colspan="2">Servicios de Laboratorio</td>
                    </tr>
                	<tr>
   	<?php
		$consulta_servicios_laboratorio=mysql_query("SELECT * FROM servicios ORDER BY id_servicio")or die(mysql_error());
		while($row_servicios_laboratorio=mysql_fetch_array($consulta_servicios_laboratorio)){
			$id_servicio = $row_servicios_laboratorio["id_servicio"];
			$descripcion = $row_servicios_laboratorio["descripcion"];
			$precio = $row_servicios_laboratorio["precio"];
			
	?>

                    	<td id="alright" width="300"><label class="textos"><?php echo $descripcion; ?>:</label></td> 
                    	<td id="alleft">
                    		<input name="descripcion_<?php echo $id_servicio; ?>" type="checkbox" 
                    		value="1" id="<?php echo $precio; ?>" class="servicio_<?php echo $id_servicio; ?>" onClick="bloquear_reparado(<?php echo $id_servicio; ?>)" />
                    	</td>
                    </tr>
	<?php
		}
	?>
    			</table>
                <p align="right">
                	<input name="generar_presupuesto" type="button" value="Generar presupuesto" class="fondo_boton" />
    			</p>
                	<label class="textos">Presupuesto Total: </label>
                    <input name="presupuesto_servicios" type="hidden" id="presupuesto_servicios" />
                    $<input name="presupuesto" type="text" id="precio_total_servicios_laboratorio" readonly="readonly" />
              	<br /><br />
                    <label class="textos">Tiempo estimado de reparación: </label>
                    <input name="estimado" type="text" size="5" maxlength="5" value="0" />días
               	<br /><br />
               		<input type="checkbox" name="check_se_cobra" id="check_se_cobra" value="si" />
               		<label class="textos"> ¿Aplica Garant&iacute;a? </label>
               		&nbsp;&nbsp;&nbsp;
               		<label class="textos"> No Reparado </label>
               		<input type="checkbox" name="check_reparado" id="check_reparado" value="si" onClick="bloquear_servicios()" />
               	<br/><br/>
                 	<label class="textos">Observaciones:</label><br />
                    <textarea name="observaciones" id="observaciones_presupuesto_garantia" cols="60" rows="2"></textarea>
                <p align="right">
                    <input name="guardar_presupuesto" id="guardar_presupuesto" type="submit" value="Guardar Presupuesto" class="fondo_boton" />
                </p>
                	</fieldset>
  			<?php
			}
			/*Si el estatus es igual a 4.-Presupuesto entonces muestra la siguiente informacion */
			if($id_estatus_reparaciones == 4)
			{
				// SE REALIZA QUERY QUE OBTIENE LA INFORMACION SOBRE EL PRESUPUESTO PARA LA REPARACION
				$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion,observaciones 
														   FROM reparaciones_laboratorio
														   WHERE folio_num_reparacion = ".$num_folio_reparacion) or die(mysql_error());

				// SE ALMACENA EN UNA VARIABLE EL RESULTADO OBTENIDO DEL QUERY
				$row12 = mysql_fetch_array($consulta_datos_presupuesto);
				$presupuesto_hecho = $row12["presupuesto"];
				$tiempo_estimado = $row12["tiempo_estimado_reparacion"];
				$observaciones = $row12['observaciones'];

				// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE NECESITA LA REPARACION
				$query_refacciones_reparacion = "SELECT descripcion,cantidad
												 FROM vales_reparacion,base_productos
												 WHERE folio_num_reparacion = '$num_folio_reparacion'
												 AND codigo = id_base_producto";

				// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
				$resultado_refacciones_reparacion = mysql_query($query_refacciones_reparacion) or die(mysql_error());

				// SE REALIZA QUERY QUE OBTIENE LOS SERVICIOS QUE SE LE VAN A REALIZAR A LA REPARACION
				$query_servicios_reparacion = "SELECT descripcion
											   FROM servicios, servicios_reparaciones
											   WHERE servicios_reparaciones.id_servicio = servicios.id_servicio
											   AND folio_num_reparacion = '$num_folio_reparacion'";

				// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
				$resultado_servicios_reparacion = mysql_query($query_servicios_reparacion) or die(mysql_error());
			?>		      	
				<fieldset>
	                <legend> Presupuesto </legend> <br/>
	                	<input type="hidden" name="folio" value="<?php echo $num_folio_reparacion; ?>"/>
	                	<label class="textos"> Presupuesto: </label>
	                		<?php
	                			if ( $garantia_venta == "si" || $aplica_garantia == "si" )
	                			{
	                			 	echo "$ 0.00";
	                			}
	                			else
	                			{
	                				echo "$".number_format($presupuesto_hecho,2); 
	                			} 
	                		?>
	                		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                	<label class="textos"> Tiempo estimado de reparaci&oacute;n: </label>
	                		<?php echo $tiempo_estimado; ?> día(s)
	                		<br /><br />
	                	<label class="textos"> Autoriza presupuesto </label>
	                		<input type="checkbox" name="autorizo" value="Si"/>
	                		&nbsp;&nbsp;
	                	<label class="textos"> Quien autoriz&oacute;: </label>
	                		<input type="text" name="quien_autorizo" id="txt_quien_autorizo"/>
	                		&nbsp;&nbsp;
	                	<label class="textos"> Fecha de autorizaci&oacute;n: </label>
	                		<input name="fecha_autorizacion" type="text" size="10" maxlength="10" value="<?php echo $fecha_actual; ?>" readonly="readonly" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;"/>
	                		&nbsp;&nbsp;
	                		<br/>
	             		<label class="textos" style="margin-top:0px; float:left;"> Observaciones: </label> &nbsp;
	             			<textarea name="observaciones" id="observaciones_presupuesto" style="resize:none; width:350px; height:50px;"><?php echo ucfirst(strtolower($observaciones)); ?></textarea>
	             		<hr style="background-color:#e6e6e6; height:3px; border:none;" />
	             		<table>
	             			<tr>
	             				<td> <label class="textos"> Refacciones </label> </td>
	             			</tr>
	             			<tr>
	             				<td style="text-align:center;"> <label class="textos"> Refacci&oacute;n </label> </td>
	             				<td style="text-align:center;"> <label class="textos"> Cantidad </label> </td>
	             			</tr>
	             	<?php
	             		// SE REALIZA UN CICLO PARA MOSTRAR LAS REFACCIONES OBTENIDAS
	             		while ( $row_refacciones_reparacion = mysql_fetch_array($resultado_refacciones_reparacion) )
	             		{
	             			$cantidad_refaccion = $row_refacciones_reparacion['cantidad'];
	             			$descripcion_refaccion = $row_refacciones_reparacion['descripcion'];
	             		?>
	             			<tr>
	             				<td style="text-align:justify;"> <?php echo ucfirst(strtolower($descripcion_refaccion)); ?> </td>
	             				<td style="text-align:center;"> <?php echo $cantidad_refaccion; ?> </td>
	             			</tr>
	             		<?php
	             		}
	             	?>
	             		</table>
	             		<hr style="background-color:#e6e6e6; height:3px; border:none;" />
	             		<table>
	             			<tr>
	             				<td> <label class="textos"> Servicios </label> </td>
	             			</tr>
	             	<?php
	             		// SE REALIZA UN CICLO PARA MOSTRAR LOS SERVICIOS OBTENIDOS
	             		while ( $row_servicio_reparacion = mysql_fetch_array($resultado_servicios_reparacion) )
	             		{
	             			$servicio_reparacion = $row_servicio_reparacion['descripcion'];
	             		?>
	             			<tr>
	             				<td> <?php echo ucfirst(strtolower($servicio_reparacion)); ?> </td>
	             			</tr>
	             		<?php
	             		}
	             	?>
	             		</table>
	                	<p align="right">
	             			<input name="autorizacion" class="fondo_boton" value="Autorizacion" type="submit" />
	                	</p>   
                </fieldset>
   			<?php
			}
		/* Si el estatus es igual a 7.-Laboratorio, Autorizado entonces muestra la siguiente informacion */
		if($id_estatus_reparaciones == 7)
		{		
		?>
 			<br />
    	<?php
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion, quien_autorizo, autorizo_presupuesto, fecha_autorizacion, observaciones
													   FROM reparaciones_laboratorio
													   WHERE folio_num_reparacion=".$num_folio_reparacion) or die(mysql_error());
			
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$presupuesto_hecho = $row13["presupuesto"];
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada2 = explode("-", $fecha_autorizacion);
			$observaciones = $row13["observaciones"];
			$fecha_autorizacion_normal = $fecha_autorizacion_separada2[2]."/".$fecha_autorizacion_separada2[1]."/".$fecha_autorizacion_separada2[0];
		?>
            <fieldset>
                <legend> Presupuesto </legend>
                <input type="hidden" name="hora" value="<?php echo $hora; ?>"/>
                <input type="hidden" name="folio" value="<?php echo $num_folio_reparacion; ?>"/>
                <label class="textos"> Presupuesto: </label>
                	<?php
                		if ( $garantia_venta == "si" || $aplica_garantia ==  "si" )
                		{
                		 	echo "$ 0.00";
                		}
                		else
                		{
                			echo "$".number_format($presupuesto_hecho,2); 
                		}
                	?>
                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label class="textos"> Tiempo estimado de reparaci&oacute;n: </label>
                	<?php echo $tiempo_estimado; ?> días
                	<br /><br />
                <label class="textos"> Autoriza presupuesto </label>
                	<?php echo $autorizo; ?>
                	&nbsp;&nbsp;&nbsp;
                <label class="textos"> Quien autoriz&oacute;: </label>
                	<?php echo $quien_autorizo; ?>
                	&nbsp;&nbsp;&nbsp;
                <label class="textos"> Fecha de autorizaci&oacute;n: </label>
                	<?php echo $fecha_autorizacion_normal; ?>
                	&nbsp;&nbsp;&nbsp; <br/><br/>
                <label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
            </fieldset>
            <br />
            <fieldset>
                <legend> Reparaci&oacute;n </legend>
                <input type="hidden" name="folio" value="<?php echo $num_folio_reparacion; ?>"/>
                <label class="textos"> Fecha de Reparaci&oacute;n: </label>
                <input type="text" name="fecha_reparacion" value="<?php echo $fecha_actual; ?>" readonly="readonly" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;"/>
                <label class="textos"> Reparado por: </label>
  		<?php
			$consulta_empleados = mysql_query("SELECT nombre, paterno, materno 
											   FROM empleados 
											   WHERE id_empleado = ".$_SESSION["id_empleado_usuario"]) or die(mysql_error());
			
			$row14 = mysql_fetch_array($consulta_empleados);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"];                 
			echo $nombre." ".$paterno." ".$materno; 
		?>
                <input type="hidden" name="reparado" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>"/>
                <br /><br />
               	<label class="textos"> Servicios requeridos: </label>
                <br /><br />
      	<?php 
			$consultar_servicios_requeridos = mysql_query("SELECT descripcion, precio 
														   FROM servicios, servicios_reparaciones
														   WHERE folio_num_reparacion = '$num_folio_reparacion' 
														   AND servicios.id_servicio=servicios_reparaciones.id_servicio") or die(mysql_error());
			while( $row_servicios_requeridos = mysql_fetch_array($consultar_servicios_requeridos) )
			{
				echo $descripcion_servicio_requerido = $row_servicios_requeridos["descripcion"];
				echo "&nbsp; $";
				echo $precio_servicio_requerido = $row_servicios_requeridos["precio"];
				echo "<br /><br/>";
			}
			?>
                <label class="textos"> Piezas reemplazadas: </label>
              	<br />
               <table align="center">
                    <tr>
                        <th width="350"> Descripci&oacute;n </th>
                        <th width="65"> Cantidad </th>
                        <th width="100"> Costo Unitario </th>
                        <th> En inventario </th>
                    </tr>
	     	<?php
	     		$ii = 0;
	     		$cantidad_total_refacciones = 0;
				$consulta_vales_reparaciones = mysql_query("SELECT * 
															FROM vales_reparacion 
															WHERE folio_num_reparacion =".$num_folio_reparacion) or die(mysql_error());
				while( $row_vales_reparacion = mysql_fetch_array($consulta_vales_reparaciones) )
				{
					$id_registro_reparacion = $row_vales_reparacion["id_registro"];
					$codigo = $row_vales_reparacion["codigo"];
					$cantidad = $row_vales_reparacion["cantidad"];
					$costo_unitario = $row_vales_reparacion["costo_unitario"];
					$cosnulta_costo_unitario=mysql_query("SELECT descripcion FROM base_productos
														  WHERE id_base_producto = ".$codigo) or die(mysql_error());
					$row_costo_unitario = mysql_fetch_array($cosnulta_costo_unitario);
					$descripcion_codigo = $row_costo_unitario["descripcion"];

					// SE REALIZA QUERY QUE OBTIENE LA CANTIDAD DE PIEZAS EN ALMACEN GENERAL
					$query_inventario_refaccion = "SELECT SUM(num_serie_cantidad) AS total
												   FROM inventarios
												   WHERE id_articulo = '$codigo'
												   AND id_almacen = 1";

					// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO CORRESPONDIENTE
					$resultado_inventario_refaccion = mysql_query($query_inventario_refaccion) or die(mysql_error());
					$row_inventario_refaccion = mysql_fetch_array($resultado_inventario_refaccion);
					$total_piezas = $row_inventario_refaccion['total'];
			?>
        			<tr>
                        <td style="text-align:justify;"><?php echo ucfirst(strtolower($descripcion_codigo)); ?></td>
                        <td style="text-align:center;"><?php echo $cantidad; ?></td>
                        <td style="text-align:center;">$<?php echo number_format($costo_unitario,2); ?></td>
                        <td style="text-align:center;"> 
                        	<?php
                        		if ( $total_piezas == "" )
                        		{
                        		 	echo "0";
                        		}
                        		else
                        		{
                        			echo $total_piezas; 	
                        		}
                        	?> 
                        </td>
                    </tr>
        	<?php
					$cantidad_total_refacciones +=($cantidad * $costo_unitario);
					if ( $total_piezas == 0 OR $total_piezas == NULL OR $total_piezas < $cantidad )
					{
						$ii++;
					}
				}
			?>
                    </table>     
                <br /><br />          	     
                    <label class="textos">Observaciones:</label><br />
                    <textarea name="observaciones" cols="60" rows="2" style="resize:none;"><?php echo $observaciones; ?></textarea>
               <p align="right">
               		<?php
               			// SE VALIDA SI EXISTE UN CERO EN LOS INVENTARIOS NO SE PERMITE GUARDAR LA REPARACION
               			if ( $ii == 0 )
               			{
               			?>
               				<input name="accion" type="submit" value="Guardar Reparacion" class="fondo_boton"/>
               			<?php
               			}
               			else
               			{
               			?>
               			<?php
               			}
               		?>
               </p>
               		</fieldset>
  	<?php
		}
		/* SI EL ID ESTATUS REPARACION ES IGUAL A 12 .- LABORATORIO NO REPARADO */
		if ( $id_estatus_reparaciones == 12 ) 
		{
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion, quien_autorizo,
															autorizo_presupuesto, fecha_autorizacion, observaciones,
															costo, reparado_por, fecha_ultima_modificacion,
															hora_ultima_modificacion, fecha_salida
														FROM reparaciones_laboratorio
														WHERE folio_num_reparacion=".$num_folio_reparacion) 
														or die(mysql_error());
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$presupuesto_hecho = $row13["presupuesto"];
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada4 = explode("-", $fecha_autorizacion);
			$fecha_autorizacion_normal = $fecha_autorizacion_separada4[2]."/".$fecha_autorizacion_separada4[1]."/".$fecha_autorizacion_separada4[0];
			$fecha_reparacion = $row13["fecha_salida"];
			$fecha_reparacion_separada3 = explode("-", $fecha_reparacion);
			$fecha_reparacion_normal = $fecha_reparacion_separada3[2]."/".$fecha_reparacion_separada3[1]."/".$fecha_reparacion_separada3[0];
			$observaciones = $row13["observaciones"];
			$costoTotal = $row13["costo"];
			$reparado_por = $row13["reparado_por"];
			$fecha_ultima_modificacion = $row13["fecha_ultima_modificacion"];
			$fecha_ultima_modificacion_separada3 = explode("-", $fecha_ultima_modificacion);
			$fecha_ultima_modificacion_normal = $fecha_ultima_modificacion_separada3[2]."/".$fecha_ultima_modificacion_separada3[1]."/".$fecha_ultima_modificacion_separada3[0];
			$hora_ultima_modificacion = $row13["hora_ultima_modificacion"];	
			/* Consulta datos del empleado que realizo la reparacion */
			$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
																				WHERE id_empleado=".$reparado_por)
																				or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleados);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"];
		?>
			<fieldset>
                    <legend>Presupuesto</legend>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos"> Presupuesto: </label>
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	}
                    	else
                    	{
                    		echo "$".number_format($presupuesto_hecho,2); 
                    	} 
                    ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tiempo estimado de reparacion: </label>
                    <?php echo $tiempo_estimado; ?> días
                <br /><br />
                    <label class="textos">Autoriza presupuesto</label>
                    <?php echo $autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Quien autorizo:</label>
                    <?php echo $quien_autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de autorizacion:</label>
                    <?php echo $fecha_autorizacion_normal; ?>
                &nbsp;&nbsp;&nbsp;<br/><br/>
                	<label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
                   	</fieldset>
              	<br />
              	<fieldset>
                    <legend>Reparado</legend>
                    <label class="textos">Fecha de Reparaci&oacute;n: </label>
                    <?php echo $fecha_reparacion_normal; ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Reparado por: </label>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
              	<br /><br />
                	<label class="textos">Costo Total: </label>$
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	}
                    	else
                    	{
                    		echo number_format($costoTotal,2);
                    	}
                    ?>
               	<br /><br />
                	<label class="textos">Servicios requeridos: </label>
                <br /><br />
      		<?php 
				$consultar_servicios_requeridos=mysql_query("SELECT descripcion, precio FROM servicios, servicios_reparaciones
															WHERE folio_num_reparacion=".$num_folio_reparacion." AND
															servicios.id_servicio=servicios_reparaciones.id_servicio")
															or die(mysql_error());
				while($row_servicios_requeridos=mysql_fetch_array($consultar_servicios_requeridos)){
					echo $descripcion_servicio_requerido=$row_servicios_requeridos["descripcion"];
					echo "&nbsp; $";
					echo $precio_servicio_requerido=$row_servicios_requeridos["precio"];
					echo "<br /><br />";
				}
			?>
                  	<label class="textos">Piezas reemplazadas: </label>
              	<br /><br />
               <table align="center">
                        <tr>
                            <th width="400">Descripcion</th>
                            <th width="65">Cantidad</th>
                            <th width="100">Costo Unitario</th>
                        </tr>
     	<?php
			$consulta_vales_reparaciones=mysql_query("SELECT * FROM vales_reparacion 
																WHERE folio_num_reparacion=".$num_folio_reparacion)
																or die(mysql_error());
			while($row_vales_reparacion=mysql_fetch_array($consulta_vales_reparaciones)){
				$id_registro_reparacion = $row_vales_reparacion["id_registro"];
				$codigo = $row_vales_reparacion["codigo"];
				$cantidad = $row_vales_reparacion["cantidad"];
				$costo_unitario = $row_vales_reparacion["costo_unitario"];
				$cosnulta_costo_unitario=mysql_query("SELECT descripcion FROM base_productos_2 
																	WHERE id_base_producto2 =".$codigo)or die(mysql_error());
				$row_costo_unitario=mysql_fetch_array($cosnulta_costo_unitario);
				$descripcion_codigo=$row_costo_unitario["descripcion"];
		?>
        				<tr>
                        	<td><?php echo $descripcion_codigo; ?></td>
                            <td><?php echo $cantidad; ?></td>
                            <td>$<?php echo number_format($costo_unitario,2); ?></td>
                        </tr>
        <?php
				$cantidad_total_refacciones +=($cantidad * $costo_unitario);
			}
		?>
                    </table>     
                <br /><br />
                	<label class="textos" style="margin-top:0px; float:left;"> Observaciones: </label> &nbsp;
                    <textarea name="observaciones" style="resize:none; width:250px; height:60px;"><?php echo $observaciones; ?></textarea>
               	<br /><br />
                    <label class="textos" style="vertical-align:top;">Fecha ultima modificación: </label>
                    <?php echo $fecha_ultima_modificacion_normal." ".$hora_ultima_modificacion; ?> <br/>
                    <p align="right">
                    <input type="hidden" name="fecha_reparacion" value="<?php echo $fecha_actual; ?>" />
                	<input name="observaciones_reparado" value="Guardar" type="submit" class="fondo_boton" />
                </p>
                    </fieldset>
		<?php
		}
		/* Si id_estatus_reparaciones es igual a 2.-Recepcion Matriz, No reparado o a 13.-Recepcion Ocolusen, No reparado */
		if($id_estatus_reparaciones == 2 or $id_estatus_reparaciones == 13){
	?>
    			<br />
    			<?php
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT tiempo_estimado_reparacion, quien_autorizo,
															autorizo_presupuesto, fecha_autorizacion, observaciones
														FROM reparaciones_laboratorio
														WHERE folio_num_reparacion=".$num_folio_reparacion) 
														or die(mysql_error());
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada2 = explode("-", $fecha_autorizacion);
			$observaciones = $row13["observaciones"];
			$fecha_autorizacion_normal = $fecha_autorizacion_separada2[2]."/".$fecha_autorizacion_separada2[1]."/".$fecha_autorizacion_separada2[0];
	?>
                    <fieldset>
                    <legend>Presupuesto</legend>
                    <input name="hora" type="hidden" value="<?php echo $hora; ?>" />
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos">Tiempo estimado de reparacion: </label>
                    <?php echo $tiempo_estimado; ?> días
                <br /><br />
                    <label class="textos">Autoriza presupuesto</label>
                    <?php echo $autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Quien autorizo:</label>
                    <?php echo $quien_autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de autorizacion:</label>
                    <?php echo $fecha_autorizacion_normal; ?>
                &nbsp;&nbsp;&nbsp; <br/><br/>
                	<label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
                   	</fieldset>
                   	<br/>
        			<fieldset>
                    <legend>Entrega</legend>
                    <label class="textos">Fecha de Entrega: </label>
                    <input name="fecha_entrega" type="text" value="<?php echo $fecha_actual; ?>" readonly="readonly"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Entrego: </label>
  	<?php
            $consulta_empleado_sistema = mysql_query("SELECT nombre, paterno, materno 
														FROM empleados 
														WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
														or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleado_sistema);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"];
	?>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
                    <input name="hora" type="hidden" value="<?php echo $hora; ?>" />
               	<br /><br />
                    <label class="textos">Recibio: </label>
                    <input name="recibio" type="text" size="40" maxlength="40" />
             	<p align="right">
                	<input name="aceptar" value="Aceptar" type="submit" class="fondo_boton" />
                </p>
                    </fieldset>
  	<?php
		}
		/* si id_Estatus_reparaciones es igual a 3.-Entregado */
		if($id_estatus_reparaciones == 3){
	?>
    			<br />
    <?php
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion, quien_autorizo,
															autorizo_presupuesto, fecha_autorizacion, observaciones, 
															costo, reparado_por, fecha_ultima_modificacion,
															hora_ultima_modificacion, fecha_salida, fecha_entrega, entrego,
															cobro, recibio
														FROM reparaciones_laboratorio
														WHERE folio_num_reparacion=".$num_folio_reparacion) 
														or die(mysql_error());
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$presupuesto_hecho = $row13["presupuesto"];
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada3 = explode("-", $fecha_autorizacion);
			$fecha_autorizacion_normal = $fecha_autorizacion_separada3[2]."/".$fecha_autorizacion_separada3[1]."/".$fecha_autorizacion_separada3[0];
			$fecha_reparacion = $row13["fecha_salida"];
			$fecha_reparacion_separada2 = explode("-", $fecha_reparacion);
			$fecha_reparacion_normal = $fecha_reparacion_separada2[2]."/".$fecha_reparacion_separada2[1]."/".$fecha_reparacion_separada2[0];
			$observaciones = $row13["observaciones"];
			$costoTotal = $row13["costo"];
			$reparado_por = $row13["reparado_por"];
			$recibio = $row13["recibio"];
			$fecha_entrega = $row13["fecha_entrega"];
			$fecha_entrega_separada = explode("-", $fecha_entrega);
			$fecha_entrega_normal = $fecha_entrega_separada[2]."/".$fecha_entrega_separada[1]."/".$fecha_entrega_separada[0];
			$quien_entrego = $row13["entrego"];
			$cobro_final = $row13["cobro"];
			$fecha_ultima_modificacion = $row13["fecha_ultima_modificacion"];
			$fecha_ultima_modificacion_separada2 = explode("-", $fecha_ultima_modificacion);
			$fecha_ultima_modificacion_normal = $fecha_ultima_modificacion_separada2[2]."/".$fecha_ultima_modificacion_separada2[1]."/".$fecha_ultima_modificacion_separada2[0];
			$hora_ultima_modificacion = $row13["hora_ultima_modificacion"];	
			/* Consulta datos del empleado que realizo la reparacion */
			$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
																				WHERE id_empleado=".$reparado_por)
																				or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleados);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"]; 
			if($autorizo == "Si"){
	?>	
     				<fieldset>
                    <legend>Presupuesto</legend>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos">Presupuesto: </label>
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	}
                    	else
                    	{
                    		echo "$".number_format($presupuesto_hecho,2);
                    	}
                    ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tiempo estimado de reparacion: </label>
                    <?php echo $tiempo_estimado; ?> días
                <br /><br />
                    <label class="textos">Autoriza presupuesto</label>
                    <?php echo $autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Quien autorizo:</label>
                    <?php echo $quien_autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de autorizacion:</label>
                    <?php echo $fecha_autorizacion_normal; ?>
                &nbsp;&nbsp;&nbsp;
                	<label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
                   	</fieldset>
              	<br />
    				<fieldset>
                    <legend>Reparado</legend>
                    <label class="textos">Fecha de Reparacion: </label>
                    <?php echo $fecha_reparacion_normal; ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Reparado por: </label>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
              	<br /><br />
                	<label class="textos">Costo Total: </label>$
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	} 
                    	else
                    	{
                    		echo number_format($costoTotal,2);
                    	}
                    ?>
               	<br /><br />
                	<label class="textos">Servicios requeridos: </label>
                <br /><br />
      		<?php 
				$consultar_servicios_requeridos=mysql_query("SELECT descripcion, precio FROM servicios, servicios_reparaciones
															WHERE folio_num_reparacion=".$num_folio_reparacion." AND
															servicios.id_servicio=servicios_reparaciones.id_servicio")
															or die(mysql_error());
				while($row_servicios_requeridos=mysql_fetch_array($consultar_servicios_requeridos)){
					echo $descripcion_servicio_requerido=$row_servicios_requeridos["descripcion"];
					echo "&nbsp; $";
					echo $precio_servicio_requerido=$row_servicios_requeridos["precio"];
					echo "<br /><br />";
				}
			?>
                	<label class="textos">Piezas reemplazadas: </label>
              	<br /><br />
               <table align="center">
                        <tr>
                            <th width="400">Descripcion</th>
                            <th width="65">Cantidad</th>
                            <th width="100">Costo Unitario</th>
                        </tr>
     	<?php
			$consulta_vales_reparaciones=mysql_query("SELECT * FROM vales_reparacion 
																WHERE folio_num_reparacion=".$num_folio_reparacion)
																or die(mysql_error());
			while($row_vales_reparacion=mysql_fetch_array($consulta_vales_reparaciones)){
				$id_registro_reparacion = $row_vales_reparacion["id_registro"];
				$codigo = $row_vales_reparacion["codigo"];
				$cantidad = $row_vales_reparacion["cantidad"];
				$costo_unitario = $row_vales_reparacion["costo_unitario"];
				$cosnulta_costo_unitario=mysql_query("SELECT descripcion FROM base_productos_2 
																	WHERE id_base_producto2 =".$codigo)or die(mysql_error());
				$row_costo_unitario=mysql_fetch_array($cosnulta_costo_unitario);
				$descripcion_codigo=$row_costo_unitario["descripcion"];
		?>
        				<tr>
                        	<td><?php echo $descripcion_codigo; ?></td>
                            <td><?php echo $cantidad; ?></td>
                            <td>$<?php echo number_format($costo_unitario,2); ?></td>
                        </tr>
        <?php
				$cantidad_total_refacciones +=($cantidad * $costo_unitario);
			}
		?>
                    </table>     
                <br /><br />
                	<label class="textos">Observaciones: </label>
                    <?php echo $observaciones; ?>
               	<br /><br />
                    <label class="textos">Fecha ultima modificación: </label>
                    <?php echo $fecha_ultima_modificacion_normal." ".$hora_ultima_modificacion; ?> 
                    </fieldset>
              	<br />
  	<?php
			}
	?>
               	  <fieldset>
                    <legend>Entrega</legend>
                    <label class="textos">Fecha de Entrega: </label>
                    <?php echo $fecha_entrega_normal; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Entrego: </label>
  	<?php
            $consulta_empleado_sistema = mysql_query("SELECT nombre, paterno, materno 
														FROM empleados 
														WHERE id_empleado=".$quien_entrego)
														or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleado_sistema);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"];
	?>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
               	<br /><br />
                    <label class="textos">Recibio: </label>
                    <?php echo $recibio; ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  	<?php
			if($autorizo == "Si"){
	?>
                    <label class="textos">Total Pagado: </label>
                    <?php echo "$".number_format($cobro_final,2); ?>
   	<?php 
			}
	?>
                    </fieldset>	
    <?php
		}
		/* Si estatus es igual a 6.-Laboratorio, Reparado */
		if($id_estatus_reparaciones == 6){
	?>
    			<br />
    <?php
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion, quien_autorizo,
															autorizo_presupuesto, fecha_autorizacion, observaciones,
															costo, reparado_por, fecha_ultima_modificacion,
															hora_ultima_modificacion, fecha_salida
														FROM reparaciones_laboratorio
														WHERE folio_num_reparacion=".$num_folio_reparacion) 
														or die(mysql_error());
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$presupuesto_hecho = $row13["presupuesto"];
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada4 = explode("-", $fecha_autorizacion);
			$fecha_autorizacion_normal = $fecha_autorizacion_separada4[2]."/".$fecha_autorizacion_separada4[1]."/".$fecha_autorizacion_separada4[0];
			$fecha_reparacion = $row13["fecha_salida"];
			$fecha_reparacion_separada3 = explode("-", $fecha_reparacion);
			$fecha_reparacion_normal = $fecha_reparacion_separada3[2]."/".$fecha_reparacion_separada3[1]."/".$fecha_reparacion_separada3[0];
			$observaciones = $row13["observaciones"];
			$costoTotal = $row13["costo"];
			$reparado_por = $row13["reparado_por"];
			$fecha_ultima_modificacion = $row13["fecha_ultima_modificacion"];
			$fecha_ultima_modificacion_separada3 = explode("-", $fecha_ultima_modificacion);
			$fecha_ultima_modificacion_normal = $fecha_ultima_modificacion_separada3[2]."/".$fecha_ultima_modificacion_separada3[1]."/".$fecha_ultima_modificacion_separada3[0];
			$hora_ultima_modificacion = $row13["hora_ultima_modificacion"];	
			/* Consulta datos del empleado que realizo la reparacion */
			$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
																				WHERE id_empleado=".$reparado_por)
																				or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleados);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"]; 
	?>	
     				<fieldset>
                    <legend>Presupuesto</legend>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos">Presupuesto: </label>
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	}
                    	else
                    	{
                    		echo "$".number_format($presupuesto_hecho,2); 
                    	} 
                    ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tiempo estimado de reparacion: </label>
                    <?php echo $tiempo_estimado; ?> días
                <br /><br />
                    <label class="textos">Autoriza presupuesto</label>
                    <?php echo $autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Quien autorizo:</label>
                    <?php echo $quien_autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de autorizacion:</label>
                    <?php echo $fecha_autorizacion_normal; ?>
                &nbsp;&nbsp;&nbsp;<br/><br/>
                	<label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
                   	</fieldset>
              	<br />
    				<fieldset>
                    <legend>Reparado</legend>
                    <label class="textos">Fecha de Reparacion: </label>
                    <?php echo $fecha_reparacion_normal; ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Reparado por: </label>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
              	<br /><br />
                	<label class="textos">Costo Total: </label>$
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	} 
                    	else
                    	{
                    		echo number_format($costoTotal,2);
                    	}
                    ?>
               	<br /><br />
                	<label class="textos">Servicios requeridos: </label>
                <br /><br />
      		<?php 
				$consultar_servicios_requeridos=mysql_query("SELECT descripcion, precio FROM servicios, servicios_reparaciones
															WHERE folio_num_reparacion=".$num_folio_reparacion." AND
															servicios.id_servicio=servicios_reparaciones.id_servicio")
															or die(mysql_error());
				while($row_servicios_requeridos=mysql_fetch_array($consultar_servicios_requeridos)){
					echo $descripcion_servicio_requerido=$row_servicios_requeridos["descripcion"];
					echo "&nbsp; $";
					echo $precio_servicio_requerido=$row_servicios_requeridos["precio"];
					echo "<br /><br />";
				}
			?>
                  	<label class="textos">Piezas reemplazadas: </label>
              	<br /><br />
               <table align="center">
                        <tr>
                            <th width="400">Descripcion</th>
                            <th width="65">Cantidad</th>
                            <th width="100">Costo Unitario</th>
                        </tr>
     	<?php
			$consulta_vales_reparaciones=mysql_query("SELECT * FROM vales_reparacion 
																WHERE folio_num_reparacion=".$num_folio_reparacion)
																or die(mysql_error());
			while($row_vales_reparacion=mysql_fetch_array($consulta_vales_reparaciones)){
				$id_registro_reparacion = $row_vales_reparacion["id_registro"];
				$codigo = $row_vales_reparacion["codigo"];
				$cantidad = $row_vales_reparacion["cantidad"];
				$costo_unitario = $row_vales_reparacion["costo_unitario"];
				$cosnulta_costo_unitario=mysql_query("SELECT descripcion FROM base_productos_2 
																	WHERE id_base_producto2 =".$codigo)or die(mysql_error());
				$row_costo_unitario=mysql_fetch_array($cosnulta_costo_unitario);
				$descripcion_codigo=$row_costo_unitario["descripcion"];
		?>
        				<tr>
                        	<td><?php echo $descripcion_codigo; ?></td>
                            <td><?php echo $cantidad; ?></td>
                            <td>$<?php echo number_format($costo_unitario,2); ?></td>
                        </tr>
        <?php
				$cantidad_total_refacciones +=($cantidad * $costo_unitario);
			}
		?>
                    </table>     
                <br /><br />
                	<label class="textos" style="margin-top:0px; float:left;"> Observaciones: </label> &nbsp;
                    <textarea name="observaciones" style="resize:none; width:250px; height:60px;"><?php echo $observaciones; ?></textarea>
               	<br /><br />
                    <label class="textos" style="vertical-align:top;">Fecha ultima modificación: </label>
                    <?php echo $fecha_ultima_modificacion_normal." ".$hora_ultima_modificacion; ?> <br/>
                    <p align="right">
                    <input type="hidden" name="fecha_reparacion" value="<?php echo $fecha_actual; ?>" />
                	<input name="observaciones_reparado" value="Guardar" type="submit" class="fondo_boton" />
                </p>
                    </fieldset>
    <?php
		}
		/* Si id_estatus_reparaciones es igual a 9.-Recepcion-Reparado */
		if($id_estatus_reparaciones == 9 or $id_estatus_reparaciones == 14 or $id_estatus_reparaciones == 11){
	?>
    			<br />
    <?php
			/* Consulta los datos del presupuesto */
			$consulta_datos_presupuesto = mysql_query("SELECT presupuesto, tiempo_estimado_reparacion, quien_autorizo,
															autorizo_presupuesto, fecha_autorizacion, observaciones, 
															costo, reparado_por, fecha_ultima_modificacion,
															hora_ultima_modificacion, fecha_salida
														FROM reparaciones_laboratorio
														WHERE folio_num_reparacion=".$num_folio_reparacion) 
														or die(mysql_error());
			$row13 = mysql_fetch_array($consulta_datos_presupuesto);
			$presupuesto_hecho = $row13["presupuesto"];
			$tiempo_estimado = $row13["tiempo_estimado_reparacion"];
			$autorizo = $row13["autorizo_presupuesto"];
			$quien_autorizo = $row13["quien_autorizo"];
			$fecha_autorizacion = $row13["fecha_autorizacion"];
			$fecha_autorizacion_separada = explode("-", $fecha_autorizacion);
			$fecha_autorizacion_normal = $fecha_autorizacion_separada[2]."/".$fecha_autorizacion_separada[1]."/".$fecha_autorizacion_separada[0];
			$fecha_reparacion = $row13["fecha_salida"];
			$fecha_reparacion_separada = explode("-", $fecha_reparacion);
			$fecha_reparacion_normal = $fecha_reparacion_separada[2]."/".$fecha_reparacion_separada[1]."/".$fecha_reparacion_separada[0];
			$observaciones = $row13["observaciones"];
			$costoTotal = $row13["costo"];
			$reparado_por = $row13["reparado_por"];
			$fecha_ultima_modificacion = $row13["fecha_ultima_modificacion"];
			$fecha_ultima_modificacion_separada = explode("-", $fecha_ultima_modificacion);
			$fecha_ultima_modificacion_normal = $fecha_ultima_modificacion_separada[2]."/".$fecha_ultima_modificacion_separada[1]."/".$fecha_ultima_modificacion_separada[0];
			$hora_ultima_modificacion = $row13["hora_ultima_modificacion"];	
			/* Consulta datos del empleado que realizo la reparacion */
			$consulta_empleados = mysql_query("SELECT nombre, paterno, materno FROM empleados 
																				WHERE id_empleado=".$reparado_por)
																				or die(mysql_error());
			$row14 = mysql_fetch_array($consulta_empleados);
			$nombre = $row14["nombre"];
			$paterno = $row14["paterno"];
			$materno = $row14["materno"]; 
	?>	
     				<fieldset>
                    <legend>Presupuesto</legend>
                    <input name="folio" value="<?php echo $num_folio_reparacion; ?>" type="hidden" />
                    <label class="textos">Presupuesto: </label>
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	} 
                    	else
                    	{
                    		echo "$".number_format($presupuesto_hecho,2);
                    	}
                    ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Tiempo estimado de reparacion: </label>
                    <?php echo $tiempo_estimado; ?> días
                <br /><br />
                    <label class="textos">Autoriza presupuesto</label>
                    <?php echo $autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Quien autorizo:</label>
                    <?php echo $quien_autorizo; ?>
                &nbsp;&nbsp;&nbsp;
                    <label class="textos">Fecha de autorizacion:</label>
                    <?php echo $fecha_autorizacion_normal; ?>
                &nbsp;&nbsp;&nbsp; <br/><br/>
                	<label class="textos"> Observaciones: </label>
                	<?php echo ucfirst(strtolower($observaciones)); ?>
                   	</fieldset>
              	<br />
    				<fieldset>
                    <legend>Reparado</legend>
                    <label class="textos">Fecha de Reparacion: </label>
                    <?php echo $fecha_reparacion_normal; ?>
              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="textos">Reparado por: </label>
                    <?php echo $nombre." ".$paterno." ".$materno; ?>
              	<br /><br />
                	<label class="textos">Costo Total: </label>$
                    <?php
                    	if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	 	echo "$ 0.00";
                    	} 
                    	else
                    	{
                    		echo number_format($costoTotal,2);
                    	}
                    ?>
               	<br /><br />
                	<label class="textos">Servicios requeridos: </label>
                <br /><br />
      		<?php 
				$consultar_servicios_requeridos=mysql_query("SELECT descripcion, precio FROM servicios, servicios_reparaciones
															WHERE folio_num_reparacion=".$num_folio_reparacion." AND
															servicios.id_servicio=servicios_reparaciones.id_servicio")
															or die(mysql_error());
				while($row_servicios_requeridos=mysql_fetch_array($consultar_servicios_requeridos)){
					echo $descripcion_servicio_requerido=$row_servicios_requeridos["descripcion"];
					echo "&nbsp; $";
					echo $precio_servicio_requerido=$row_servicios_requeridos["precio"];
					echo "<br /><br />";
				}
			?>
                  	<label class="textos">Piezas reemplazadas: </label>
              	<br /><br />
               <table align="center">
                        <tr>
                            <th width="400">Descripcion</th>
                            <th width="65">Cantidad</th>
                            <th width="100">Costo Unitario</th>
                        </tr>
     	<?php
			$consulta_vales_reparaciones=mysql_query("SELECT * FROM vales_reparacion 
																WHERE folio_num_reparacion=".$num_folio_reparacion)
																or die(mysql_error());
			while($row_vales_reparacion=mysql_fetch_array($consulta_vales_reparaciones)){
				$id_registro_reparacion = $row_vales_reparacion["id_registro"];
				$codigo = $row_vales_reparacion["codigo"];
				$cantidad = $row_vales_reparacion["cantidad"];
				$costo_unitario = $row_vales_reparacion["costo_unitario"];
				$cosnulta_costo_unitario=mysql_query("SELECT descripcion FROM base_productos_2 
																	WHERE id_base_producto2 =".$codigo)or die(mysql_error());
				$row_costo_unitario=mysql_fetch_array($cosnulta_costo_unitario);
				$descripcion_codigo=$row_costo_unitario["descripcion"];
		?>
        				<tr>
                        	<td><?php echo $descripcion_codigo; ?></td>
                            <td><?php echo $cantidad; ?></td>
                            <td>$<?php echo number_format($costo_unitario,2); ?></td>
                        </tr>
        <?php
				$cantidad_total_refacciones +=($cantidad * $costo_unitario);
			}
		?>
                    </table>     
                <br /><br />
                	<label class="textos">Observaciones: </label>
                    <?php echo $observaciones; ?>
               	<br /><br />
                    <label class="textos">Fecha ultima modificación: </label>
                    <?php echo $fecha_ultima_modificacion_normal." ".$hora_ultima_modificacion; ?> 
                    <br/><br/>
                    </fieldset>
                    <?php
                    	if ( $garantia_reparacion == "si" || $garantia_adaptacion == "si" || $garantia_venta == "si" || $aplica_garantia == "si" )
                    	{
                    	?>
                    		<br/>
                    		<fieldset>
		                    <legend>Entrega</legend>
		                    <label class="textos">Fecha de Entrega: </label>
		                    <input name="fecha_entrega" type="text" value="<?php echo $fecha_actual; ?>" readonly="readonly"/>
		                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                    <label class="textos">Entrego: </label>
		  	<?php
		            $consulta_empleado_sistema = mysql_query("SELECT nombre, paterno, materno 
																FROM empleados 
																WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
																or die(mysql_error());
					$row14 = mysql_fetch_array($consulta_empleado_sistema);
					$nombre = $row14["nombre"];
					$paterno = $row14["paterno"];
					$materno = $row14["materno"];
			?>
		                    <?php echo $nombre." ".$paterno." ".$materno; ?>
		                    <input name="hora" type="hidden" value="<?php echo $hora; ?>" />
		               	<br /><br />
		                    <label class="textos">Recibio: </label>
		                    <input name="recibio" type="text" size="40" maxlength="40" />
		             	<p align="right">
		                	<input name="aceptar" value="Aceptar" type="submit" class="fondo_boton" />
		                </p>
		                    </fieldset>
                    	<?php
                    	}
                    ?>
              	<br />
    <?php
		}
?>
                </form>
                <p align="right">
                	<?php
                		if ( $id_estatus_reparaciones != 3 )
                		{
                		?>
                			<a href="lista_reparaciones.php">
                        		<input name="volver" type="button" value="Volver" class="fondo_boton" />
                    		</a>
                		<?php
                		}
                		else
                		{
                		?>
                			<a href="lista_reparaciones_entregadas.php">
                        		<input name="volver" type="button" value="Volver" class="fondo_boton" />
                    		</a>
                		<?php
                		}
                	?>
                    
                </p>
                <!-- Para mostrar el movimiento del auxiliar del paciente -->
                    <a href="#" onClick="muestra_oculta('contenido_a_mostrar')" title="">
                        <label class="textos" style="font-size:13px;">Mostrar Movimientos del Auxiliar</label>
                    </a>
                    <div id="contenido_a_mostrar">
                    <table style="border-collapse:collapse;">
                        <tr>
                            <th width="60">N° nota</th>
                            <th width="80">Fecha</th>
                            <th width="100">Hora</th>
                            <th width="130">Movimiento</th>
                            <th width="100">Responsable</th>
                            <th width="100">Estatus</th>
                            <th width="100">Eliminar</th>
                        </tr>
	<?php
		/* Consulta el movimiento del auxiliar de la tabla movimientos */
		$consulta_movimientos_aparato = mysql_query("SELECT * FROM movimientos
															WHERE folio_num_reparacion=".$num_folio_reparacion) 
															or die(mysql_error());
		while($row8 = mysql_fetch_array($consulta_movimientos_aparato)){
			$id_registro = $row8["id_registro"];
			$num_folio = $row8["folio_num_reparacion"];
			$id_movimiento = $row8["id_estado_movimiento"];
			$fecha_movimiento = $row8["fecha"];
			$fecha_movimiento_separada = explode("-", $fecha_movimiento);
			$fecha_movimiento_normal = $fecha_movimiento_separada[2]."/".$fecha_movimiento_separada[1]."/".$fecha_movimiento_separada[0];
			$hora_movimiento = $row8["hora"];
			$estatus_reparacion = $row8["id_estatus_reparaciones"];
			$id_responsable = $row8["id_empleado"];
			$mov++;
			/* Consulta el estado del movimiento */				
			$consulta_descripcion_estado = mysql_query("SELECT estado_movimiento
															FROM estados_movimientos
															WHERE id_estado_movimiento=
															".$id_movimiento) 
															or die(mysql_error());
															
			$row9 = mysql_fetch_array($consulta_descripcion_estado);			
			$estado_movimiento = $row9["estado_movimiento"];
			/* Consulta el responsable del movimiento */		
			$consulta_responsable = mysql_query("SELECT alias FROM empleados
																WHERE id_empleado=".$id_responsable) 
																or die(mysql_error());
													
			$row10 = mysql_fetch_array($consulta_responsable);
			$alias_responsable = $row10["alias"];
			/* Consulta el estatus de la reparacion */		
			$consulta_estatus_reparacion = mysql_query("SELECT estado_reparacion
															FROM estatus_reparaciones
															WHERE id_estatus_reparacion
															=".$estatus_reparacion) 
															or die(mysql_error());
			$row11 = mysql_fetch_array($consulta_estatus_reparacion);
			$estatus_aparato = $row11["estado_reparacion"];		
	?>
                        <tr>                            
                            <td><?php echo $num_folio; ?></td>
                            <td><?php echo $fecha_movimiento_normal; ?></td>
                            <td><?php echo $hora_movimiento; ?></td>
                            <td><?php echo $estado_movimiento; ?></td>
                            <td><?php echo $alias_responsable; ?></td>
                            <td><?php echo $estatus_aparato; ?></td>
                            <td style="text-align:center">
                            	<?php
                            		if($mov>1){                            		
                            	?>
                            	<a href="detalles_reparaciones2.php<?php echo "?d=".$id_registro."&folio=".$num_folio_reparacion; ?>">
                            		<img src="../img/delete.png">
                            	</a>
                            	<?php
                            		}
                            	?>
                            </td>
                        </tr>
	<?php	
		}
	?>
                    </table>
                    </div><!--Fin de contenido a mostrar-->
                <br /><br />
                </div><!--Fin contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>