<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include('config.php');
    
    // SE REALIZA EL QUERY QUE OBTIENE LAS CATEGORIAS
    $query_categorias = "SELECT id_categoria, categoria
                         FROM categorias_productos
                         WHERE id_categoria = 1 OR id_categoria = 4 OR id_categoria = 5";
    
    // SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
    $resultado_query_categorias = mysql_query($query_categorias) or die(mysql_error());
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Lista Inventario </title>
        <link type="text/css" rel="stylesheet" href="../css/style3.css"/>
        <link type="text/css" rel="stylesheet" href="../css/themes/base2/jquery.ui.all.css"/>
        <script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
        <script type="text/javascript" language="javascript" src="../js/jquery.ui.core.js"></script>
        <script type="text/javascript" language="javascript" src="../js/jquery.ui.widget.js"></script>
        <script type="text/javascript" language="javascript" src="../js/jquery.ui.tabs.js"></script>
        <script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
        <link type="text/css" rel="stylesheet" href="../css/demos.css"/>
        <script type="text/javascript" language="javascript">
            $(function() {
		$("#tabs").tabs();
            });
        </script>
    </head>
    <body>
        <div id="wrapp">
            <div id="contenido_columna2">
                <div class="contenido_pagina">
                    <div class="fondo_titulo1">
                        <div class="categoria">
                            Inventario
                        </div><!-- FIN DIV CATEGORIA -->
                    </div><!-- FIN DIV FONDO TITULO 1 -->
                    <div class="buscar2">
                        <form name="Buscar" action="lista_inventario.php" method="post">
                            <select name="categoria" id="categoria">
                                <option value="0"> Categor&iacute;a </option>
                        <?php
                            // SE REALIZA UN CICLO PARA MOSTRAR LAS CATEGORIAS
                            while( $row_categoria = mysql_fetch_array($resultado_query_categorias) )
                            {
                                $id_categoria = $row_categoria['id_categoria'];
                                $categoria = $row_categoria['categoria'];
                            ?>
                                <option value="<?php echo $id_categoria; ?>"> <?php echo ucfirst(strtolower($categoria)); ?> </option>
                            <?php
                            }
                        ?>
                            </select>
                            <select name="subcategoria" id="subcategoria">
                                <option value="0"> Subcategor&iacute;a </option>
                            </select>
                            <input type="text" name="filtro" size="15" maxlength="15"/>
                            <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height: 25px;"/>
                        </form><!-- FIN DIV FORM BUSCAR -->
                    </div><!-- FIN DIV BUSCAR 2 -->
                    <div class="area_contenido2">
                        <?php
                            // SE VALIDA SI SE HA OPRIMIDO EL BOTON BUSCAR
                            if( isset( $_POST['buscar'] ) )
                            {
                                // SE VALIDA SI EL CAMPO DE TEXTO TIENE ALGO ESCRITO
                                if( $_POST['filtro'] != "" )
                                {
                                    $filtro_busc = " AND referencia_codigo LIKE '%".$_POST['filtro']."%' 
                                                     OR descripcion LIKE '%".$_POST['filtro']."%' ";
                                }
                                else
                                {
                                    $filtro_busc = " ";
                                }
                                // SE VALIDA SI SE HA SELECCIONADO ALGUNA CATEGORIA
                                if( $_POST['categoria'] !=0 )
                                {
                                    // SE VALIDA SI EL CAMPO DE TEXTO TIENE ALGO ESCRITO PARA BUSCAR
                                    if( $_POST['filtro'] != "" )
                                    {
                                        $filtro_cate = " AND id_categoria=".$_POST['categoria']." ";
                                    }
                                    else
                                    {
                                        $filtro_cate = " AND id_categoria=".$_POST['categoria']." ";
                                    }
                                }
                                else
                                {
                                    $filtro_cate = " ";
                                }
                                // SE VALIDA SI SE HA SELECCIONADO ALGUNA SUBCATEGORIA
                                if( $_POST['subcategoria'] != 0 )
                                {
                                    $filtro_subc = " AND id_subcategoria=".$_POST['subcategoria']." ";
                                }
                                else
                                {
                                    $filtro_subc=" ";
                                }
                            }
                            else
                            {
                                $filtro_busc=" ";
                                $filtro_subc=" ";
                                $filtro_cate=" ";
                            }
                        ?>
                        <br/>
                        <div class="contenido_proveedor" style="margin-bottom: -15px;">
                            <form name="forma3" action="procesa_base_productos_activos.php" method="post">
                                <div class="demo" style="width: 700px; margin-left: -30px;">
                                    <div id="tabs">
                                        <ul>
                                            <li> <a href="#tabs-1"> Refacciones </a> </li>
                                            <li> <a href="#tabs-2"> Baterias </a> </li>
                                            <li> <a href="#tabs-3"> Auxiliares </a> </li>
                                        </ul>
                                        <div id="tabs-1">
                                            <div style="height: auto">
                                                <table>
                                                    <tr>
                                                        <th width="120"> Referencia </th>
                                                        <th width="260"> Descripci&oacute;n </th>
                                                        <th width="40"> Activo </th>
                                                        <th width="48"> Stock </th>
                                                        <th style="width: 55px !important; color:rgb(127,127,127) !important" colspan="2"> ---------------- </th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="height:280px; overflow:auto; overflow-x:hidden;">
                                                <table>
                                                    <?php
                                                        $consulta_todo_base_productos = mysql_query("SELECT referencia_codigo,descripcion,activo,id_articulo,id_subcategoria,id_base_producto,id_categoria
                                                                                                     FROM base_productos 
                                                                                                     WHERE id_categoria = 1".$filtro_busc.$filtro_cate.$filtro_subc."
                                                                                                     ORDER BY activo DESC, referencia_codigo") or die(mysql_error());
                                                        $resultado = 0;
                                                        while( $row2 = mysql_fetch_array($consulta_todo_base_productos) )
                                                        {
                                                            $resultado++;
                                                            $referencia_codigo = $row2["referencia_codigo"];
                                                            $descripcion = $row2["descripcion"];
                                                            $activo = $row2["activo"];
                                                            $id_articulo = $row2["id_articulo"];
                                                            $id_categoria_consultada = $row2["id_categoria"];  
                                                            $id_base_producto = $row2["id_base_producto"];
                                                            $id_subcategoria = $row2["id_subcategoria"];
                                                            $consulta_inventario = mysql_query("SELECT SUM(num_serie_cantidad), COUNT(num_serie_cantidad) 
                                                                                                FROM inventarios
                                                                                                WHERE id_articulo='".$id_base_producto."'");
                                                            //  AND id_subcategoria='".$id_subcategoria."'
                                                            while( $row4 = mysql_fetch_array($consulta_inventario) )
                                                            {
                                                                $cantidad_inventario = $row4["SUM(num_serie_cantidad)"];
                                                                $cantidad_num_series = $row4["COUNT(num_serie_cantidad)"];	
                                                        ?>
                                                                <tr>
                                                                    <td width="120px">
                                                                        <label class="textos"><?php echo $referencia_codigo; ?></label>
                                                                    </td>
                                                                    <td width="320px">                                    
                                                                        <label class="textos"><?php echo ucfirst(strtolower($descripcion)); ?></label>                                    
                                                                    </td>
                                                                    <td width="40px" style="text-align:center">
                                                                    <?php 
                                                                        if($activo == "No"){                                        
                                                                    ?>
                                                                            <input type="checkbox" value="Si" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                            <input type="checkbox" value="Si" checked="checked" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                    </td>
                                                                    <td width="48px" style="text-align:center">
                                                                        <label class="textos">
                                                                        <?php 
                                                                            if( $id_categoria_consultada <>5 )
                                                                            {
                                                                                if($cantidad_inventario == "")
                                                                                {
                                                                                    echo "0";
                                                                                }else
                                                                                {
                                                                                    echo $cantidad_inventario;
                                                                                }
                                                                            }else
                                                                            {
                                                                                echo $cantidad_num_series;	
                                                                            }
                                                                        ?>
                                                                        </label>
                                                                    </td><td id="alright" width="27px">
                                                                        <a href="agregar_producto.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/inventary.png" title="Añadir a Stock [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td><td id="alright"  width="27px">
                                                                        <a href="modificar_inventario.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/modify.png" title="Modificar [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        if($resultado==0){
                                                        ?>
                                                            <tr>
                                                                <td colspan="6" style="text-align:center">
                                                                    <label class="textos"> No se encontraron registros </label>
                                                                    <input type="checkbox" name="no_change" checked="checked" value="0" style="visibility:hidden" />
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>          
                                                </table>
                                            </div>
                                        </div><!-- FIN DIV TABS 1 -->
                                        <div id="tabs-2">
                                               <div style="height: auto">
                                                <table>
                                                    <tr>
                                                        <th width="120"> Referencia </th>
                                                        <th width="260"> Descripci&oacute;n </th>
                                                        <th width="40"> Activo </th>
                                                        <th width="48"> Stock </th>
                                                        <th style="width: 55px !important; color:rgb(127,127,127) !important" colspan="2"> ---------------- </th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="height:280px; overflow:auto; overflow-x:hidden;">
                                                <table>
                                                    <?php
                                                        $consulta_todo_base_productos = mysql_query("SELECT referencia_codigo,descripcion,activo,id_articulo,id_subcategoria,id_base_producto,id_categoria
                                                                                                     FROM base_productos 
                                                                                                     WHERE id_categoria = 4".$filtro_busc.$filtro_cate.$filtro_subc."
                                                                                                     ORDER BY activo DESC, referencia_codigo") or die(mysql_error());
                                                        $resultado = 0;
                                                        while( $row2 = mysql_fetch_array($consulta_todo_base_productos) )
                                                        {
                                                            $resultado++;
                                                            $referencia_codigo = $row2["referencia_codigo"];
                                                            $descripcion = $row2["descripcion"];
                                                            $activo = $row2["activo"];
                                                            $id_articulo = $row2["id_articulo"];
                                                            $id_categoria_consultada = $row2["id_categoria"];  
                                                            $id_base_producto = $row2["id_base_producto"];
                                                            $id_subcategoria = $row2["id_subcategoria"];
                                                            $consulta_inventario = mysql_query("SELECT SUM(num_serie_cantidad), COUNT(num_serie_cantidad) 
                                                                                                FROM inventarios
                                                                                                WHERE id_articulo='".$id_base_producto."' 
                                                                                                AND id_subcategoria='".$id_subcategoria."'");
                                                            while( $row4 = mysql_fetch_array($consulta_inventario) )
                                                            {
                                                                $cantidad_inventario = $row4["SUM(num_serie_cantidad)"];
                                                                $cantidad_num_series = $row4["COUNT(num_serie_cantidad)"];	
                                                        ?>
                                                                <tr>
                                                                    <td width="120px">
                                                                        <label class="textos"><?php echo $referencia_codigo; ?></label>
                                                                    </td>
                                                                    <td width="320px">                                    
                                                                        <label class="textos"><?php echo $descripcion; ?></label>                                    
                                                                    </td>
                                                                    <td width="40px" style="text-align:center">
                                                                    <?php 
                                                                        if($activo == "No"){                                        
                                                                    ?>
                                                                            <input type="checkbox" value="Si" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                            <input type="checkbox" value="Si" checked="checked" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                    </td>
                                                                    <td width="48px" style="text-align:center">
                                                                        <label class="textos">
                                                                        <?php 
                                                                            if( $id_categoria_consultada <>5 )
                                                                            {
                                                                                if($cantidad_inventario == "")
                                                                                {
                                                                                    echo "0";
                                                                                }else
                                                                                {
                                                                                    echo $cantidad_inventario;
                                                                                }
                                                                            }else
                                                                            {
                                                                                echo $cantidad_num_series;	
                                                                            }
                                                                        ?>
                                                                        </label>
                                                                    </td><td id="alright" width="27px">
                                                                        <a href="agregar_producto.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/inventary.png" title="Añadir a Stock [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td><td id="alright"  width="27px">
                                                                        <a href="modificar_inventario.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/modify.png" title="Modificar [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        if($resultado==0){
                                                        ?>
                                                            <tr>
                                                                <td colspan="6" style="text-align:center">
                                                                    <label class="textos"> No se encontraron registros </label>
                                                                    <input type="checkbox" name="no_change" checked="checked" value="0" style="visibility:hidden" />
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>          
                                                </table>
                                            </div> 
                                        </div><!-- FIN DIV TABS 2 -->
                                        <div id="tabs-3">
                                            <div style="height: auto">
                                                <table>
                                                    <tr>
                                                        <th width="120"> Referencia </th>
                                                        <th width="260"> Descripci&oacute;n </th>
                                                        <th width="40"> Activo </th>
                                                        <th width="48"> Stock </th>
                                                        <th style="width: 55px !important; color:rgb(127,127,127) !important" colspan="2"> ---------------- </th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="height:280px; overflow:auto; overflow-x:hidden;">
                                                <table>
                                                    <?php
                                                        $consulta_todo_base_productos = mysql_query("SELECT referencia_codigo,descripcion,activo,id_articulo,id_subcategoria,id_base_producto,id_categoria
                                                                                                     FROM base_productos 
                                                                                                     WHERE id_categoria = 5".$filtro_busc.$filtro_cate.$filtro_subc."
                                                                                                     ORDER BY activo DESC, referencia_codigo") or die(mysql_error());
                                                        $resultado = 0;
                                                        while( $row2 = mysql_fetch_array($consulta_todo_base_productos) )
                                                        {
                                                            $resultado++;
                                                            $referencia_codigo = $row2["referencia_codigo"];
                                                            $descripcion = $row2["descripcion"];
                                                            $activo = $row2["activo"];
                                                            $id_articulo = $row2["id_articulo"];
                                                            $id_categoria_consultada = $row2["id_categoria"];  
                                                            $id_base_producto = $row2["id_base_producto"];
                                                            $id_subcategoria = $row2["id_subcategoria"];
                                                            $consulta_inventario = mysql_query("SELECT SUM(num_serie_cantidad), COUNT(num_serie_cantidad) 
                                                                                                FROM inventarios
                                                                                                WHERE id_articulo='".$id_base_producto."' 
                                                                                                AND id_subcategoria='".$id_subcategoria."'");
                                                            while( $row4 = mysql_fetch_array($consulta_inventario) )
                                                            {
                                                                $cantidad_inventario = $row4["SUM(num_serie_cantidad)"];
                                                                $cantidad_num_series = $row4["COUNT(num_serie_cantidad)"];	
                                                        ?>
                                                                <tr>
                                                                    <td width="120px">
                                                                        <label class="textos"><?php echo $referencia_codigo; ?></label>
                                                                    </td>
                                                                    <td width="320px">                                    
                                                                        <label class="textos"><?php echo $descripcion; ?></label>                                    
                                                                    </td>
                                                                    <td width="40px" style="text-align:center">
                                                                    <?php 
                                                                        if($activo == "No"){                                        
                                                                    ?>
                                                                            <input type="checkbox" value="Si" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                            <input type="checkbox" value="Si" checked="checked" name="activo_<?php echo $id_base_producto ?>" />
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                    </td>
                                                                    <td width="48px" style="text-align:center">
                                                                        <label class="textos">
                                                                        <?php 
                                                                            if( $id_categoria_consultada <>5 )
                                                                            {
                                                                                if($cantidad_inventario == "")
                                                                                {
                                                                                    echo "0";
                                                                                }else
                                                                                {
                                                                                    echo $cantidad_inventario;
                                                                                }
                                                                            }else
                                                                            {
                                                                                echo $cantidad_num_series;	
                                                                            }
                                                                        ?>
                                                                        </label>
                                                                    </td><td id="alright" width="27px">
                                                                        <a href="agregar_producto.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/inventary.png" title="Añadir a Stock [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td><td id="alright"  width="27px">
                                                                        <a href="modificar_inventario.php?id_base_producto=<?php echo $id_base_producto; ?>">
                                                                            <img src="../img/modify.png" title="Modificar [<?php echo $referencia_codigo; ?>]" />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        if($resultado==0){
                                                        ?>
                                                            <tr>
                                                                <td colspan="6" style="text-align:center">
                                                                    <label class="textos"> No se encontraron registros </label>
                                                                    <input type="checkbox" name="no_change" checked="checked" value="0" style="visibility:hidden" />
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>          
                                                </table>
                                            </div>
                                        </div><!-- FIN DIV TABS 3 -->
                                    </div><!-- FIN DIV TABS -->
                                </div><!-- FIN DIV DEMO -->
                                <p align="right">
                                <?php
                                    if( !isset( $_POST['buscar'] ) )
                                    {
                                ?>
                                        <input name="accion" value="Guardar Cambios" type="submit" class="fondo_boton"/>        							
                                <?php
                                    }
                                ?>
                                </p>
                            </form>
                            <br/>
                        </div><!-- FIN DIV CONTENIDO PROVEEDOR -->
                    </div><!-- FIN DIV AREA CONTENIDO 2 -->
                </div><!-- FIN DIV CONTENIDO PAGINA -->
            </div><!-- FIN DIV CONTENIDO COLUMNA2 -->
        </div><!-- FIN DIV WRAPP -->
    </body>
</html>
