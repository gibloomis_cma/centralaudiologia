<?php
session_start(); if($_SESSION['id_usuario'] == 1 || $_SESSION['id_usuario'] == 2){ 
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
	
	// SE RECIBEN LAS VARIABLES DEL FORMULARIO
	$totales=0;
	$hora = $_POST['hora'];
    $fecha_final_mysql = $_POST['fecha_final'];
    $fecha_final_separada = explode("-", $fecha_final_mysql);
    $fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
    $fecha_inicio_mysql = $_POST['fecha_inicio'];
    $fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
	$fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];
	
	// SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=Reporte de Accesos.xls");
	
	// SE DECLARAN VARIABLES PARA LLEVAR EL CONTROL DE LOS TOTALES
	$contador = 0;
	$total_baterias_vales = 0;
	$total_global_vales = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Bater&iacute;as que Salen </title>
</head>
<body>
	<style>
		table tr td{
			border:1px solid #CCC
		}
	</style>
	<center>
        <table> 
            <tr>
                <td style="color:#000; font-size:22px; text-align:center;" colspan="3"> ACCESOS </td>
            </tr>
            <tr>
                <td style="color:#000; font-size:16px; text-align:center;" colspan="3"> Del: <?php echo $fecha_inicio; ?> Al: <?php echo $fecha_final; ?> </td>
            </tr>
            <tr>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> FECHA </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> HORA </th>
                            <th style="font-size:14px; background-color:#039; color:#FFF"> USUARIO </th>                                              
                        </tr>
						<?php
								// SE REALIZA QUERY QUE OBTIENE LAS REFACCIONES QUE SE UTILIZAN EN LAS NOTAS DE REPARACIONES
								// SE REALIZA QUERY QUE OBTIENE LOS USUARIOS en_sistema entre las fechas indicadas
								$query_accesos = "SELECT fecha_entrada, hora_entrada, nombre, paterno, materno 
														FROM en_sistema, usuarios, empleados 
														WHERE en_sistema.id_usuario = usuarios.id_usuario 
														AND empleados.id_empleado = usuarios.id_empleado 
														AND en_sistema.fecha_entrada BETWEEN '".$fecha_inicio_mysql."' AND '".$fecha_final_mysql."'
														ORDER BY fecha_entrada DESC";
										
								// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO 
								$result_accesos = mysql_query($query_accesos) or die(mysql_error());
								// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
								while( $row_accesos = mysql_fetch_array($result_accesos) ){
						?>
                        <tr>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $row_accesos['fecha_entrada']; ?> </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo $row_accesos['hora_entrada']; ?>  </td>
                            <td style="font-size:9px; text-align:center;"> <?php  echo ucfirst($row_accesos['nombre'])." ".ucfirst($row_accesos['paterno'])." ".ucfirst($row_accesos['materno']); ?> </td>                            
                        </tr>                       
						<?php
								}
                        ?>
					</table>
	 </center>
</body>
</html>
<?php } ?>