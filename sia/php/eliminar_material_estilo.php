<?php
	include("config.php");
	$id_cat_nivel1 = $_GET["id_cat_nivel1"];
	$id_estilo = $_GET["id_estilo"];
	/* Consulta el Catalogo */
	$consulta_catalogo=mysql_query("SELECT id_catalogo FROM catalogo WHERE id_estilo=".$id_estilo)or die(mysql_error());
	$row_catalogo=mysql_fetch_array($consulta_catalogo);
	$id_catalogo=$row_catalogo["id_catalogo"];
	/* Consulta cuantos materiales del Estilo hay */
	$consulta_materiales=mysql_query("SELECT COUNT(id_cat_nivel1) AS contador_materiales FROM cat_nivel1 
										WHERE id_catalogo=".$id_catalogo)or die(mysql_error());
	$row_materiales=mysql_fetch_array($consulta_materiales);
	$numero_materiales=$row_materiales["contador_materiales"];
	if($numero_materiales <= 1){
?>
	<script type="text/javascript" language="javascript">
		alert("No se puede eliminar, porque solo hay un material cargado");
    	window.location.href = 'modificar_catalogo_moldes.php?id_estilo='+<?php echo $id_estilo; ?>
	</script>
<?php
	}else{
		/* Elimina el material */
		mysql_query("DELETE FROM cat_nivel1 WHERE id_cat_nivel1=".$id_cat_nivel1)or die(mysql_error());
		/* Consulta ventilaciones del Material */
		$consulta_ventilaciones=mysql_query("SELECT id_cat_nivel2  FROM cat_nivel2 
											WHERE id_cat_nivel1=".$id_cat_nivel1)or die(mysql_error());
		while($row_ventilaciones=mysql_fetch_array($consulta_ventilaciones)){
			$id_cat_nivel2 = $row_ventilaciones["id_cat_nivel2"];
			/* Elimina la ventilacion */
			mysql_query("DELETE FROM cat_nivel2 WHERE id_cat_nivel2=".$id_cat_nivel2)or die(mysql_error());
			/* Consulta salidas de la Ventilacion */
			$consulta_salidas=mysql_query("SELECT id_cat_nivel3 FROM cat_nivel3
											WHERE id_cat_nivel2=".$id_cat_nivel2)or die(mysql_error());
			while($row_salidas=mysql_fetch_array($consulta_salidas)){
				$id_cat_nivel3 = $row_salidas["id_cat_nivel3"];
				/* Elimina la salida */
				mysql_query("DELETE FROM cat_nivel3 WHERE id_cat_nivel3=".$id_cat_nivel3)or die(mysql_error());
			}
		}
?>
		<script type="text/javascript" language="javascript">
			alert("Se elimino exitosamente el Material");
            window.location.href = 'modificar_catalogo_moldes.php?id_estilo='+<?php echo $id_estilo; ?>
        </script>
<?php
	}
?>