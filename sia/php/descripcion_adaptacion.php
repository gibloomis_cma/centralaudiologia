<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Adaptaciones Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">      
			<div id="contenido_columna2">
            	<div class="contenido_pagina">   
					<div class="fondo_titulo1">       
            			<div class="categoria">
            				Pacientes
            			</div>
        			</div><!--Fin de fondo titulo-->  
                    <div class="area_contenido1">
                        <div class="contenido_proveedor">
  	<?php
				include("config.php");
				$id_cliente = $_GET["id_paciente"];
				$consulta_datos_cliente=mysql_query("SELECT nombre, paterno, materno FROM ficha_identificacion
																						WHERE id_cliente=".$id_cliente)
																						or die(mysql_error());
				$row_datos_cliente = mysql_fetch_array($consulta_datos_cliente);
				$nombre = $row_datos_cliente["nombre"];
				$paterno = $row_datos_cliente["paterno"];
				$materno = $row_datos_cliente["materno"];
	?>
    					<br />
                           	<label class="textos">Nombre del paciente: </label>
                        	<?php echo $nombre." ".$paterno." ".$materno; ?>
                        <br /><br />
                        <center>
                        	<table>
                            	<tr style="width:650px;">
                                	<th colspan="5">Auxiliares del Paciente</th>
                            	</tr>
                            	<tr>
                                    <th width="90">Nº Registro</th>
                                    <th width="270">Modelo / Num serie</th>
                                    <th width="150">Estado</th>
                                    <th width="100">Saldo</th>
                                    <th></th>
                            	</tr>
                         	<?php
				$consulta_relacion_aparatos = mysql_query("SELECT * FROM relacion_aparatos 
                                                            		WHERE id_cliente=".$id_cliente)
																	or die(mysql_error());
				while($row = mysql_fetch_array($consulta_relacion_aparatos)){
					$id_registro_relacion = $row["id_registro"];
					$id_modelo = $row["id_modelo"];
					$num_serie_cantidad = $row["num_serie_cantidad"];
					$id_estatus_relacion = $row["id_estatus_relacion"];
					
					$consulta_modelos=mysql_query("SELECT modelo FROM modelos 
																	WHERE id_modelo=".$id_modelo)
																	or die(mysql_error());
					$row_modelos=mysql_fetch_array($consulta_modelos);
					$modelo = $row_modelos["modelo"];
					
					$consulta_adaptaciones_pacientes=mysql_query("SELECT costo_total, id_registro_adaptaciones, estatus_pago
																	FROM adaptaciones_pacientes
																	WHERE id_registro_relacion_aparato=".$id_registro_relacion)
																	or die(mysql_error());
					$row_adaptaciones_pacientes = mysql_fetch_array($consulta_adaptaciones_pacientes);
					$costo_total = $row_adaptaciones_pacientes["costo_total"];
					$id_registro_adaptaciones = $row_adaptaciones_pacientes["id_registro_adaptaciones"];
					$estatus_pago = $row_adaptaciones_pacientes["estatus_pago"];				
	?>
                           		<tr>
                                    <td><?php echo $id_registro_adaptaciones; ?></td>
                                    <td style="text-align:left;"><?php echo $modelo."  &nbsp; / &nbsp;  ".$num_serie_cantidad; ?></td>
                                    <td><?php echo $estatus_pago; ?>
                                	</td>
                                    <td>
	<?php 
					$consulta_pagos_adaptaciones = mysql_query("SELECT cantidad 
																	FROM pagos_adaptaciones_pacientes	
																	WHERE id_registro_adaptaciones=".$id_registro_adaptaciones)
																	or die(mysql_error());
					while($row_pagos_adaptaciones = mysql_fetch_array($consulta_pagos_adaptaciones)){
						$cantidad = $row_pagos_adaptaciones["cantidad"];
						$cantidadTotal += $cantidad;				 
					}	
					$saldo =  $costo_total - $cantidadTotal;
	?>
    									$<?php echo number_format($saldo,2);?>
    								</td>
   	<?php
					if($saldo == 0){											
	?>
									<td><a href="descripcion_aparato_ya_pagado.php?id_registro_relacion=<?php echo $id_registro_relacion; ?>&id_paciente=<?php echo $id_cliente; ?>">Ver</a></td>
  	<?php
					}else{
	?>
    								<td><a href="descripcion_aparato_adaptado.php?id_registro_relacion=<?php echo $id_registro_relacion; ?>&id_paciente=<?php echo $id_cliente; ?>">Ver</a></td>
    <?php
					}
					$cantidadTotal=0;
				}
	?>
							</table>
                        </center>
                        <p align="right">
                            <a href="lista_pacientes_adaptaciones.php">
                            	<input name="accion" type="button" value="Volver" class="fondo_boton" />
                            </a>
                        </p>
                        </div><!--Fin de contenido proveedor-->
                   	</div><!--Fin de area contenido-->                   
				</div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>