<?php
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Moldes | Catálogo </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
<div id="wrapp">
	<div id="contenido_columna2">
			<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria">
						Moldes
					</div> <!-- FIN DEL DIV CATEGORIA -->
					</div> <!-- FIN DEL DIV FONDO TITULO1 -->
						<div class="area_contenido1">
							<br/>
							<div class="titulos"> Catálogo </div>
							<br/>
							<?php
							// SE IMPORTA EL ARCHIVO PARA REALIZAR LA CONEXION A LA BASE DE DATOS
								include("config.php");
							// SE REALIZA QUERY PARA OBTENER LOS TIPOS DE APARATOS QUE EXISTEN
								$query_catalogo = mysql_query("SELECT id_catalogo,estilo 
															   FROM catalogo,estilos 
															   WHERE estilos.id_estilo = catalogo.id_estilo ORDER BY id_catalogo ASC") or die (mysql_error());
							?>
							<table id="moldes">
								<tr>
									<td> Estilo: </td>
									<td>
										<select id="estilos" name="estilos" style="width:235px;">
											<option value="0"> --- Seleccione Estilo --- </option>
										<?php
										while( $row_estilos = mysql_fetch_array($query_catalogo) )
										{
											$id_catalogo = $row_estilos['id_catalogo'];
											$estilo = $row_estilos['estilo'];
										?>
											<option value="<?php echo $id_catalogo; ?>"> <?php echo $estilo; ?> </option>
										<?php
										}
										?>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
								<tr>
									<td> Material: </td>
									<td>
										<select id="materiales" name="materiales" disabled="disabled" style="width:235px;">
											<option value="0"> --- Seleccione Material --- </option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
								<tr>
									<td> Color: </td>
									<td>
										<select id="colores" name="colores" disabled="disabled" style="width:235px; float:left;">
											<option value="0"> --- Seleccione Color --- </option>
										</select>
										<div class="campo_color" style="width:50px; height:20px; float:left; margin-left:8px; margin-top:-2px;">  </div>
									</td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
								<tr>
									<td> Ventilación: </td>
									<td>
										<select id="ventilaciones" name="ventilaciones" disabled="disabled" style="width:235px;">
											<option value="0"> --- Seleccione Ventilacion --- </option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
								<tr>
									<td> Calibre: </td>
									<td> <input type="text" name="calibre" id="calibre" readonly="readonly"/> </td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
								<tr>
									<td> Salida: </td>
									<td>
										<select id="salidas" name="salidas" disabled="disabled" style="width:235px;">
											<option value="0"> --- Seleccione Salida --- </option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2"> <br/> </td>
								</tr>
							</table>
							<div id="imagenes">
							
								<div id="estilos_moldes" style="display:none">
								
								</div>
								<div id="imagen_ventilacion" style="display:none">
								
								</div>
								<div id="imagen_salida" style="display:none">
								
								</div>
							</div>
							<br/>
						</div> <!-- FIN DEL DIV AREA CONTENIDO1 -->					
				</div> <!-- FIN DEL DIV CONTENIDO PAGINA -->
			</div> <!-- FIN DEL DIV CONTENIDO COLUMNA2 -->
</div> <!-- FIN DEL DIV WRAPP -->
</body>
</html>