<?php
    error_reporting(0);
	// SE INICIA LA SESION EN EL SCRIPT 
	session_start();

	// SE COLOCA LA VARIABLE DE SESION FOLIO EN BLANCO
	$_SESSION["folio"] = "";

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");

	// SE IMPORTA LA LIBRERIA DE PAGINACION
	//require_once '../librerias/PHPPaging.lib.php';

	// SE ESTABLECEN LAS CARACTERISTICAS DE LA PAGINACION
	//$paging = new PHPPaging();
    //$paging->verPost(true);
    //$paging->porPagina(10);
    //$paging->linkSeparador('&nbsp;&nbsp;');
    //$paging->linkClase('paginas');
    //$paging->mostrarPrimera('Primera');
    //$paging->mostrarUltima('Ultima');

    // SE REALIZA EL QUERY QUE OBTIENE LOS NOMBRES DE LOS CLIENTES REGISTRADOS
    $query_clientes_registrados = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre
				 				   FROM ficha_identificacion";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_clientes_registrados = mysql_query($query_clientes_registrados) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Moldes </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript">
	    $(function(){
	        <?php
	            while($row = mysql_fetch_array($resultado_clientes_registrados))
	            {
	                $nombre = $row['nombre'];
	                $elementos[] = '"'.$nombre.'"';
	            }
	            $arreglo = implode(", ", $elementos);
	        ?>	
	        var availableTags = new Array(<?php echo $arreglo; ?>);
	        $("#nombre_paciente").autocomplete({
	            source: availableTags
	        });
	    });

        $(function() {
            //call the function onload, default to page 1
            getdata( 1 );
        });

        function getdata( pageno ){                     
            var targetURL = 'lista_moldes_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons

            $('#tabla_lista_moldes').html('<p><img src="../img/ajax-loader.gif" /></p>');       
            $('#tabla_lista_moldes').load( targetURL,{
                filtro: "<?php echo $_POST['filtro']; ?>"
            } ).hide().fadeIn('slow');
        } 
		function getdata2( ){   
			var valor = $('#paginas option:selected').attr('value');
			var targetURL = 'lista_reparaciones_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
			$('#tabla_lista_reparaciones').html('<p><img src="../img/ajax-loader.gif" /></p>');       
			$('#tabla_lista_reparaciones').load( targetURL,{
				filtro: "<?php echo $_POST['filtro']; ?>"
			} ).hide().fadeIn('slow');                  
			
		} 
	</script>
	<style type="text/css">
		div.contenedor 
		{
			position: relative;
			width: 140px;
			left: 107px;
		}
		
		#input 
		{
			font-family: Arial;
			color: #000;
			font-size: 10pt;
			width: 140px;
		}

		div.fill 
		{
			font-family: Arial;
			font-size: 10pt;
			display: none;
			width: 197px;
			color: #CCC;
			background-color: #CCC;
			border: 1px solid #999;
			overflow: auto;
			height: auto;
			top: -1px;
		}

		tr.fill 
		{
			font-family: Arial;
			font-size: 8pt;
			color: #FFF;
			background-color: #a2a2a3;
			border: 1px solid #a2a2a3;
		}

		#tr 
		{
			font-family: Arial;
			font-size: 8pt;
			background-color: #d9dcdf;
			color: #000;
			border: 1px solid #d9dcdf;
		}
	</style>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div><!-- FIN DIV CATEGORIA -->
            </div><!--Fin de fondo titulo-->
			<?php 
        		date_default_timezone_set('America/Monterrey');
        		$script_tz = date_default_timezone_get();
        		$fecha = date("d/m/Y");
        		$hora = date("h:i:s A");

        		// SE VALIDA SI SE HA REALIZADO UNA BUSQUEDA
        		if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
        		{
            		$filtro = $_REQUEST['filtro'];
                	$res_busqueda = mysql_query("SELECT count(DISTINCT(folio_num_molde)) AS contador
                                                 FROM moldes
                                                 WHERE (nombre LIKE '%".$filtro."%' 
                                                 OR paterno LIKE '%".$filtro."%'
                                                 OR materno LIKE '%".$filtro."%'
                                                 OR folio_num_molde LIKE '%".$filtro."%')
                                                 AND id_estatus_moldes <> 7") or die(mysql_error());
                	
                	$row_busqueda = mysql_fetch_array($res_busqueda);
                	$busqueda = 0;
                	$busqueda += $row_busqueda["contador"];
            		if( $busqueda == 0 )
            		{
                		$busqueda = 0;	
            		}
            		$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
        		}
        		else
        		{
            		$res2="";
        		}
			?> 
            <div class="buscar2">
            	<form name="busqueda" method="post" action="lista_moldes.php">
                	<label class="textos"><?php echo $res2; ?></label>
                	<input name="filtro" type="text" size="15" maxlength="15" />
                	<input name="buscar" type="submit" value="Buscar" class="fondo_boton" style="height:25px;" />
            	</form>
            </div><!-- FIN DIV BUSCAR 2 -->
            <div class="area_contenido2">
            	<div class="contenido_proveedor">
                <center>
                    <table>
                        <tr>
                            <th colspan="5"> MOLDES </th>
                        </tr>
                    </table>
                    <div id="tabla_lista_moldes">
                        <img src="../img/ajax-loader.gif" />
                    </div>
                </center>
                <br />
                </div><!-- FIN DIV CONTENIDO PROVEEDOR -->
                <div class="titulos"> Agregar Nuevo Molde </div>
                <div class="contenido_proveedor">
                    <?php
                        // SE REALIZA QUERY QUE OBTIENE LAS NOTAS YA ENTREGADAS
                        $query_notas_entregadas = "SELECT folio_num_molde,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_paciente
                                                   FROM moldes
                                                   WHERE id_estatus_moldes = '7'
                                                   ORDER BY folio_num_molde DESC";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
                        $resultado_notas_entregadas = mysql_query($query_notas_entregadas) or die(mysql_error());
                    ?>
                	<form name="aceptar" action="lista_moldes.php" method="post">
                		<br/>
                		<table>
                			<tr>
                				<td width="100"><label class="textos"> Buscar Cliente: </label> </td>
                				<td> <input type="text" name="paciente" id="nombre_paciente" class="activar_nombre" size="35" maxlength="35" /> </td>
                			</tr>
                			<tr>
                				<td colspan="2"> <input type="checkbox" name="boletin" id="checkbox_no_registrado" value="ON"/> <label class="textos"> No, se encuentra registrado </label> </td>
                			</tr>
                			<tr>
                				<td colspan="2"> <input type="checkbox" name="adaptacion" id="checkbox_adaptacion" value="si"/> <label class="textos"> Adaptaci&oacute;n </label> </td>
                			</tr>
                			<tr>
                				<td> <input type="checkbox" name="reposicion" id="checkbox_reposicion" value="si"/> <label class="textos"> Reposici&oacute;n </label> </td>
                                <td>
                                    <select name="nota_reposicion" id="nota_reposicion" disabled="disabled">
                                        <option value="0"> Seleccione N° de Nota </option>
                                <?php
                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY OBTENIDO
                                    while ( $row_notas_entregadas = mysql_fetch_array($resultado_notas_entregadas) )
                                    {
                                        $folio_nota_entregada = $row_notas_entregadas['folio_num_molde'];
                                        $nombre_completo_paciente = $row_notas_entregadas['nombre_completo_paciente'];
                                    ?>
                                        <option value="<?php echo $folio_nota_entregada; ?>"> <?php echo " N° ". $folio_nota_entregada." - ".$nombre_completo_paciente; ?> </option>
                                    <?php
                                    }
                                ?>
                                    </select>
                                </td>
                			</tr>
                			<tr>
                				<td colspan="2"> <input type="submit" name="accion" value="Aceptar" title="Aceptar" class="fondo_boton" /> </td>
                			</tr>
                		</table>
                	</form>
                <br />
				<?php 
					$paciente = $_REQUEST["paciente"];
					$accion = $_REQUEST["accion"];
					$adaptacion = $_REQUEST['adaptacion'];
					$reposicion = $_REQUEST['reposicion'];
                    $nota_entregada = $_REQUEST['nota_reposicion'];
                    $boletin = $_REQUEST['boletin'];
				
					// SE VALIDA SI EL BOTON ACEPTAR FUE OPRIMIDO
					if( $accion == "Aceptar" )
					{
						// SE VALIDA QUE SE HAYA ESCRITO EL NOMBRE DEL PACIENTE
						if( $paciente != "" )
						{
							// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DEL PACIENTE
							$consulta_datos_paciente = "SELECT id_cliente, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_paciente, calle, num_exterior, num_interior, codigo_postal, colonia, id_ciudad, id_estado, edad, ocupacion
														FROM ficha_identificacion";

							// SE OBTIENE EL RESULTADO DEL QUERY Y SE ALMACENA EN UNA VARIABLE
							$resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());
						
							// SE REALIZA UN CICLO PARA MOSTRAR LOS DATOS DEL PACIENTE REGISTRADO
							while( $row5 = mysql_fetch_array($resultado_datos_paciente) )
							{
								$id_cliente = $row5["id_cliente"];
								$calle = $row5["calle"];
								$num_exterior = $row5["num_exterior"];
								$num_interior = $row5["num_interior"];
								$codigo_postal = $row5["codigo_postal"];
								$colonia = $row5["colonia"];
								$id_estado = $row5["id_estado"];
								$id_ciudad = $row5["id_ciudad"];
								$edad = $row5["edad"];
								$ocupacion = $row5["ocupacion"];
								$nombre_completo = $row5['nombre_paciente'];
								
								// SE VALIDA SI EL PACIENTE YA SE ENCUENTRA REGISTRADO Y SE HACE EL SIGUIENTE PROCEDIMIENTO
								if( $paciente == $nombre_completo )
								{
								?>
            						<hr>
   								<?php
   									// SE VALIDA SI EL FOLIO ES IGUAL A NADA
									if( $_SESSION['folio'] == "" )
									{
										// SE REALIZA EL QUERY QUE OBTIENE EL NUMERO MAXIMO DE FOLIO
										$folio_max = "SELECT MAX(folio_num_molde + 1)
													  FROM moldes";

										// SE OBTIENE EL RESULTADO DEL QUERY Y SE ALMACENA EN UNA VARIABLE
										$resultado_folio_max = mysql_query($folio_max) or die(mysql_error());
										
										// EL FOLIO SE ALMACENA EN UNA VARIABLE									
										$row_folio_max = mysql_fetch_array($resultado_folio_max);
										$_SESSION['folio'] = $row_folio_max['MAX(folio_num_molde + 1)'];

										// SE VALIDA SI EL RESULTADO DEL QUERY MAX FOLIO REGRESO NULL O CERO
										if( $_SESSION['folio'] == 0 )
										{
											$_SESSION['folio'] = 1;
										}
									}		
								?>
              						<form name="guardar_nota_molde" action="proceso_guardar_nota_molde.php" method="post" class="valida_nota_molde">
              							<label class="textos"> Nº Nota: </label> <?php echo $_SESSION['folio']; ?>
                    					<input name="hora_real" type="hidden" value="<?php echo $hora; ?>" />
                    					<input name="variable" type="hidden" value="1" />
                    					<input name="id_empleado" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                						<input type="hidden" name="adaptacion" value="<?php echo $adaptacion; ?>" />
                						<input type="hidden" name="reposicion" value="<?php echo $reposicion; ?>" />
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    					<label class="textos">Fecha: </label><?php echo $fecha; ?>
               							<div id="spanreloj"></div>
                						<div id="ficha_identificacion"><!--Inicio de div ficha identificacion-->
	                						<br />
                    						<input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                    						<input name="variable_sin_editar" type="hidden" value="1" />
                    						<label class="textos">Nombre Completo:  </label>&nbsp;
                    						<?php echo $nombre_completo; ?>
                							<br /><br />
                    						<label class="textos">Dirección: </label>
                    						<?php echo $calle." #".$num_exterior." ".$num_interior." Col.". $colonia." ".$codigo_postal; ?>
                							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                						<?php
              								// SE REALIZA LA CONSULTA DE LA CIUDAD CORRESPONDIENTE AL PACIENTE
              								$consulta_ciudad = "SELECT ciudad 
              													FROM ciudades 
																WHERE id_estado = '$id_estado' AND id_ciudad = '$id_ciudad'";

											// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
											$resultado_ciudad = mysql_query($consulta_ciudad) or die(mysql_error());
													
											// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
											$row4 = mysql_fetch_array($resultado_ciudad);

											// EL NOMBRE DE LA CIUDAD ES ALMACENADO EN UNA VARIABLE PARA DESPUES MOSTRARLA EN PANTALLA
											$ciudad = utf8_encode($row4['ciudad']);

											// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL ESTADO DEL PACIENTE
											$consulta_estados = "SELECT estado 
																 FROM estados 
																 WHERE id_estado = '$id_estado'";

											// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
											$resultado_estados = mysql_query($consulta_estados) or die(mysql_error());
													
											// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
											$row3 = mysql_fetch_array($resultado_estados);

											// EL NOMBRE DEL ESTADO ES ALMACENADO EN UNA VARIABLE PARA DESPUES MOSTRARLO EN PANTALLA 
											$estado = utf8_encode($row3['estado']);
										?>
                						<br /><br />
                    					<label class="textos">Edad: </label><?php echo $edad; ?>
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    					<label class="textos">Ocupacion: </label><?php echo $ocupacion; ?>
                						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		
                						<br /><br />
                    					<label class="textos">Forma de Contacto:</label>
                    					<table>
   										<?php
											$consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
																					WHERE id_cliente=".$id_cliente) or die(mysql_error());
			
											while($row2 = mysql_fetch_array($consulta_forma_contacto))
											{
												$id_contactos_cliente = $row2["id_contactos_cliente"];
												$tipo = $row2["tipo"];
												$descripcion = $row2["descripcion"];
												$titular = $row2["titular"];
										?>
                        						<tr>
                            						<td><label class="textos">Tipo: </label></td>
                            						<td width="50"><?php echo $tipo; ?></td>
                            						<td>&nbsp;</td>
                            						<td><label class="textos">Descripcion: </label></td>
                            						<td width="80"><?php echo $descripcion; ?></td>
                            						<td>&nbsp;</td>
                            						<td><label class="textos">Titular: </label></td>
                            						<td><?php echo $titular; ?></td>
                        						</tr>
   										<?php 
											}
										?>
                    					</table>
                						<p align="right">
                    						<input name="editar" type="button" value="Editar Datos Paciente" class="fondo_boton" id="editar" />
                						</p>
                						</div><!--Fin de div ficha identificacion-->
                    <input  name="folio_molde" type="hidden" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                    <input name="fecha" type="hidden" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
      			<br />
		    <?php
				// SE REALIZA QUERY PARA OBTENER LOS TIPOS DE APARATOS QUE EXISTEN
					$query_catalogo = mysql_query("SELECT id_catalogo,estilo 
												   FROM catalogo,estilos 
												   WHERE estilos.id_estilo = catalogo.id_estilo ORDER BY id_catalogo ASC") or die (mysql_error());
				?>
                    <table>
                        <tr>
                            <td id="alright" width="70"> Estilo: </td>
                            <td>
                    <select id="estilos" name="estilos" style="width:235px;">
                        <option value="0"> --- Seleccione Estilo --- </option>
					<?php
                        while( $row_estilos = mysql_fetch_array($query_catalogo) ){
                            $id_catalogo = $row_estilos['id_catalogo'];
                            $estilo = $row_estilos['estilo'];
                    ?>
                        <option value="<?php echo $id_catalogo; ?>"> <?php echo $estilo; ?> </option>
                    <?php
                        }
                    ?>
                    </select>
                    <label class="textos">Precio:</label>
                    $<input name="precio_estilo" id="precio_estilo" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Material: </td>
                            <td>
                                <select id="materiales" name="materiales" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Material --- </option>
                                </select>
                                <label class="textos">Precio:</label>
                                $<input name="precio_material" id="precio_material" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Color: </td>
                            <td>
                                <select id="colores" name="colores" disabled="disabled" style="width:235px; float:left;">
                                    <option value="0"> --- Seleccione Color --- </option>
                                </select>
                                <div class="campo_color" style="width:50px; height:20px; float:left; margin-left:20px;">  </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Ventilación: </td>
                            <td>
                                <select id="ventilaciones" name="ventilaciones" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Ventilacion --- </option>
                                </select>
                                <label class="textos">Precio:</label>
                                $<input name="precio_ventilacion" id="precio_ventilacion" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Calibre: </td>
                            <td> <input type="text" name="calibre" id="calibre" readonly="readonly"/> </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Salida: </td>
                            <td>
                                <select id="salidas" name="salidas" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Salida --- </option>
                                </select>
                              	<label class="textos">Precio: </label>
                                $<input name="precio_salida" type="text" id="precio_salida" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                    </table>
                   	<div id="imagenes2">
                        <div id="estilos_moldes" style="display:none"></div>
                        <div id="imagen_ventilacion" style="display:none"></div>
                        <div id="imagen_salida" style="display:none"></div>
                    </div>
              	<br />
               	  <label class="textos">Costo Total del Molde: </label>
                    $<input name="costo_total_molde" type="text" id="costo_total_molde" size="8" maxlength="8" readonly="readonly" /> 
              	<br /><br />
                    <label class="textos">Lado del Oido: </label>
                    <select name="lado_oido" id="lado_oido">
                    	<option value="0">-- Seleccione Lado --</option>
                        <option value="derecho">Derecho</option>
                        <option value="izquierdo">Izquierdo</option>
                        <option value="bilateral">Bilateral</option>
                    </select>
                <br /><br />
                    <label class="textos">Observaciones: </label><br />
                    <textarea name="observaciones" rows="2" cols="60"></textarea>
               	<p align="right">
                    <input name="guardar_molde" class="fondo_boton" type="submit" value="Guardar Molde"/>
                </p>
              	</form>
 				<?php
					}
				}
			}// TERMINA EL IF SI SE ESCRIBIO EL NOMBRE DEL PACIENTE
            if ( $reposicion == "si" && $nota_entregada != 0 ) 
            {
                    // SE REALIZA QUERY QUE OBTIENE EL ID DEL CLIENTE DE ACUERDO A LA NOTA RECIBIDA
                    $query_id_cliente = "SELECT id_cliente
                                         FROM moldes
                                         WHERE folio_num_molde = '$nota_entregada'";

                    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO CORRESPONDIENTE
                    $resultado_id_cliente = mysql_query($query_id_cliente) or die(mysql_error());
                    $row_id_cliente = mysql_fetch_array($resultado_id_cliente);
                    $id_cliente_consultado = $row_id_cliente['id_cliente'];

                    // SE VALIDA SI EL PACIENTE SE ENCUENTRA REGISTRADO EN LA TABLA FICHA DE IDENTIFICACION
                    if ( $id_cliente_consultado != 0 )
                    {
                        // SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DEL PACIENTE
                        $consulta_datos_paciente = "SELECT id_cliente, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_paciente, calle, num_exterior, num_interior, codigo_postal, colonia, id_ciudad, id_estado, edad, ocupacion
                                                    FROM ficha_identificacion
                                                    WHERE id_cliente = '$id_cliente_consultado'";

                        // SE OBTIENE EL RESULTADO DEL QUERY Y SE ALMACENA EN UNA VARIABLE
                        $resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());
                    }
                    else
                    {
                        // SE REALIZA QUERY QUE OBTIENE LOS DATOS DEL PACIENTE DE LA TABLA DE MOLDES
                        $consulta_datos_paciente = "SELECT id_cliente, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_paciente, calle, num_exterior, num_interior, codigo_postal, colonia, id_ciudad, id_estado, ciudad, fecha_nacimiento
                                                    FROM moldes
                                                    WHERE folio_num_molde = '$nota_entregada'";

                        // SE OBTIENE EL RESULTADO DEL QUERY Y SE ALMACENA EN UNA VARIABLE
                        $resultado_datos_paciente = mysql_query($consulta_datos_paciente) or die(mysql_error());
                    }

                    
                        
                            // SE REALIZA UN CICLO PARA MOSTRAR LOS DATOS DEL PACIENTE REGISTRADO
                            while( $row5 = mysql_fetch_array($resultado_datos_paciente) )
                            {
                                $id_cliente = $row5["id_cliente"];
                                $calle = $row5["calle"];
                                $num_exterior = $row5["num_exterior"];
                                $num_interior = $row5["num_interior"];
                                $codigo_postal = $row5["codigo_postal"];
                                $colonia = $row5["colonia"];
                                $id_estado = $row5["id_estado"];
                                $id_ciudad = $row5["id_ciudad"];
                                $ciudad_consultada = $row5['ciudad'];
                                $fecha_nacimiento_consultada = $row5['fecha_nacimiento'];
                                $edad = $row5["edad"];
                                $ocupacion = $row5["ocupacion"];
                                $nombre_completo = $row5['nombre_paciente'];
                                ?>
                                    <hr style="background-color:#e6e6e6; height:3px; border:none;">
                                <?php
                                    // SE VALIDA SI EL FOLIO ES IGUAL A NADA
                                    if( $_SESSION['folio'] == "" )
                                    {
                                        // SE REALIZA EL QUERY QUE OBTIENE EL NUMERO MAXIMO DE FOLIO
                                        $folio_max = "SELECT MAX(folio_num_molde + 1)
                                                      FROM moldes";

                                        // SE OBTIENE EL RESULTADO DEL QUERY Y SE ALMACENA EN UNA VARIABLE
                                        $resultado_folio_max = mysql_query($folio_max) or die(mysql_error());
                                        
                                        // EL FOLIO SE ALMACENA EN UNA VARIABLE                                 
                                        $row_folio_max = mysql_fetch_array($resultado_folio_max);
                                        $_SESSION['folio'] = $row_folio_max['MAX(folio_num_molde + 1)'];

                                        // SE VALIDA SI EL RESULTADO DEL QUERY MAX FOLIO REGRESO NULL O CERO
                                        if( $_SESSION['folio'] == 0 )
                                        {
                                            $_SESSION['folio'] = 1;
                                        }
                                    }       
                                ?>
                                    <form name="guardar_nota_molde" action="proceso_guardar_nota_molde.php" method="post" class="valida_nota_molde">
                                        <label class="textos"> Nº Nota: </label> <?php echo $_SESSION['folio']; ?>
                                        <input name="hora_real" type="hidden" value="<?php echo $hora; ?>" />
                                        <input name="variable" type="hidden" value="1" />
                                        <input name="id_empleado" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                                        <input type="hidden" name="adaptacion" value="<?php echo $adaptacion; ?>" />
                                        <input type="hidden" name="reposicion" value="<?php echo $reposicion; ?>" />
                                        <input type="hidden" name="nota_anterior" value="<?php echo $nota_entregada; ?>" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label class="textos">Fecha: </label><?php echo $fecha; ?>
                                        <div id="spanreloj"></div>
                                        <div id="ficha_identificacion"><!--Inicio de div ficha identificacion-->
                                            <br />
                                            <input name="id_cliente" type="hidden" value="<?php echo $id_cliente; ?>" id="paciente_reparacion" />
                                            <input name="variable_sin_editar" type="hidden" value="1" />
                                            <label class="textos">Nombre Completo:  </label>&nbsp;
                                            <?php echo $nombre_completo; ?>
                                            <br /><br />
                                            <label class="textos">Dirección: </label>
                                            <?php echo $calle." #".$num_exterior." ".$num_interior." Col.". $colonia." ".$codigo_postal; ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php
                                            // SE REALIZA LA CONSULTA DE LA CIUDAD CORRESPONDIENTE AL PACIENTE
                                            $consulta_ciudad = "SELECT ciudad 
                                                                FROM ciudades 
                                                                WHERE id_estado = '$id_estado' AND id_ciudad = '$id_ciudad'";

                                            // SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
                                            $resultado_ciudad = mysql_query($consulta_ciudad) or die(mysql_error());
                                                    
                                            // EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
                                            $row4 = mysql_fetch_array($resultado_ciudad);

                                            // EL NOMBRE DE LA CIUDAD ES ALMACENADO EN UNA VARIABLE PARA DESPUES MOSTRARLA EN PANTALLA
                                            $ciudad = utf8_encode($row4['ciudad']);

                                            // SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL ESTADO DEL PACIENTE
                                            $consulta_estados = "SELECT estado 
                                                                 FROM estados 
                                                                 WHERE id_estado = '$id_estado'";

                                            // SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
                                            $resultado_estados = mysql_query($consulta_estados) or die(mysql_error());
                                                    
                                            // EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
                                            $row3 = mysql_fetch_array($resultado_estados);

                                            // EL NOMBRE DEL ESTADO ES ALMACENADO EN UNA VARIABLE PARA DESPUES MOSTRARLO EN PANTALLA 
                                            $estado = utf8_encode($row3['estado']);
                                        ?>
                                        <br /><br />
                                        <label class="textos">Edad: </label>
                                        <?php 
                                            if ( $edad != "" )
                                            {
                                                echo $edad;   
                                            }
                                            else
                                            {
                                                $fecha_nacimiento_separada = explode("-", $fecha_nacimiento_consultada);
                                                $year = $fecha_nacimiento_separada[0];
                                                $current_year = date('Y');
                                                echo $current_year - $year." años";
                                            } 
                                        ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label class="textos">Ocupacion: </label><?php echo $ocupacion; ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      
                                        <br /><br />
                                        <label class="textos">Forma de Contacto:</label>
                                        <table>
                                        <?php
                                            if ( $id_cliente_consultado != 0 )
                                            {
                                                $consulta_forma_contacto = mysql_query("SELECT * FROM contactos_clientes 
                                                                                        WHERE id_cliente=".$id_cliente) or die(mysql_error());

                                                while($row2 = mysql_fetch_array($consulta_forma_contacto))
                                                {
                                                    $id_contactos_cliente = $row2["id_contactos_cliente"];
                                                    $tipo = $row2["tipo"];
                                                    $descripcion = $row2["descripcion"];
                                                    $titular = $row2["titular"];
                                                ?>
                                                    <tr>
                                                        <td><label class="textos">Tipo: </label></td>
                                                        <td width="50"><?php echo $tipo; ?></td>
                                                        <td>&nbsp;</td>
                                                        <td><label class="textos">Descripcion: </label></td>
                                                        <td width="80"><?php echo $descripcion; ?></td>
                                                        <td>&nbsp;</td>
                                                        <td><label class="textos">Titular: </label></td>
                                                        <td><?php echo $titular; ?></td>
                                                    </tr>
                                            <?php 
                                                }
                                            }
                                            else
                                            {
                                                // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LA FORMA DE CONTACTO DEL PACIENTE
                                                $query_forma_contacto = "SELECT tipo,descripcion,titular
                                                                         FROM moldes
                                                                         WHERE folio_num_molde = '$nota_entregada'";

                                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                                $resultado_forma_contacto = mysql_query($query_forma_contacto) or die(mysql_error());
                                                $row_forma_contacto = mysql_fetch_array($resultado_forma_contacto);
                                                $tipo = $row_forma_contacto['tipo'];
                                                $descripcion = $row_forma_contacto['descripcion'];
                                                $titular = $row_forma_contacto['titular'];
                                            ?>
                                                <tr>
                                                    <td> <label class="textos"> Tipo: </label> </td>
                                                    <td width="50"> <?php echo $tipo; ?> </td>
                                                    <td>&nbsp; </td>
                                                    <td> <label class="textos"> Descripci&oacute;n: </label> </td>
                                                    <td width="80"> <?php echo $descripcion; ?> </td>
                                                    <td>&nbsp;  </td>
                                                    <td> <label class="textos"> Titular: </label> </td>
                                                    <td> <?php echo $titular; ?> </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                        <p align="right">
                                        <?php
                                            if ( $id_cliente_consultado != 0 )
                                            {
                                            ?>
                                                <input name="editar" type="button" value="Editar Datos Paciente" class="fondo_boton" id="editar" />
                                            <?php
                                            }
                                            else
                                            {

                                            }
                                        ?>
                                        </p>
                                        </div><!--Fin de div ficha identificacion-->
                    <input  name="folio_molde" type="hidden" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" />
                    <input name="fecha" type="hidden" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" />
                <br />
            <?php
                // SE REALIZA QUERY PARA OBTENER LOS TIPOS DE APARATOS QUE EXISTEN
                    $query_catalogo = mysql_query("SELECT id_catalogo,estilo 
                                                   FROM catalogo,estilos 
                                                   WHERE estilos.id_estilo = catalogo.id_estilo ORDER BY id_catalogo ASC") or die (mysql_error());
                ?>
                    <table>
                        <tr>
                            <td id="alright" width="70"> Estilo: </td>
                            <td>
                    <select id="estilos" name="estilos" style="width:235px;">
                        <option value="0"> --- Seleccione Estilo --- </option>
                    <?php
                        while( $row_estilos = mysql_fetch_array($query_catalogo) ){
                            $id_catalogo = $row_estilos['id_catalogo'];
                            $estilo = $row_estilos['estilo'];
                    ?>
                        <option value="<?php echo $id_catalogo; ?>"> <?php echo $estilo; ?> </option>
                    <?php
                        }
                    ?>
                    </select>
                    <label class="textos">Precio:</label>
                    $<input name="precio_estilo" id="precio_estilo" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Material: </td>
                            <td>
                                <select id="materiales" name="materiales" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Material --- </option>
                                </select>
                                <label class="textos">Precio:</label>
                                $<input name="precio_material" id="precio_material" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Color: </td>
                            <td>
                                <select id="colores" name="colores" disabled="disabled" style="width:235px; float:left;">
                                    <option value="0"> --- Seleccione Color --- </option>
                                </select>
                                <div class="campo_color" style="width:50px; height:20px; float:left; margin-left:20px;">  </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Ventilación: </td>
                            <td>
                                <select id="ventilaciones" name="ventilaciones" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Ventilacion --- </option>
                                </select>
                                <label class="textos">Precio:</label>
                                $<input name="precio_ventilacion" id="precio_ventilacion" type="text" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Calibre: </td>
                            <td> <input type="text" name="calibre" id="calibre" readonly="readonly"/> </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td id="alright" width="70"> Salida: </td>
                            <td>
                                <select id="salidas" name="salidas" disabled="disabled" style="width:235px;">
                                    <option value="0"> --- Seleccione Salida --- </option>
                                </select>
                                <label class="textos">Precio: </label>
                                $<input name="precio_salida" type="text" id="precio_salida" size="6" maxlength="6" readonly="readonly" />
                            </td>
                        </tr>
                    </table>
                    <div id="imagenes2">
                        <div id="estilos_moldes" style="display:none"></div>
                        <div id="imagen_ventilacion" style="display:none"></div>
                        <div id="imagen_salida" style="display:none"></div>
                    </div>
                <br />
                  <label class="textos">Costo Total del Molde: </label>
                    $<input name="costo_total_molde" type="text" id="costo_total_molde" size="8" maxlength="8" readonly="readonly" /> 
                <br /><br />
                    <label class="textos">Lado del Oido: </label>
                    <select name="lado_oido" id="lado_oido">
                        <option value="0">-- Seleccione Lado --</option>
                        <option value="derecho">Derecho</option>
                        <option value="izquierdo">Izquierdo</option>
                        <option value="bilateral">Bilateral</option>
                    </select>
                <br /><br />
                    <label class="textos">Observaciones: </label><br />
                    <textarea name="observaciones" rows="2" cols="60"></textarea>
                <p align="right">
                    <input name="guardar_molde" class="fondo_boton" type="submit" value="Guardar Molde"/>
                </p>
                </form>
                <?php
                }
            }
			elseif ($boletin == "ON") 
			{
				// SI EL PACIENTE NO SE ENCUENTRA REGISTRADO SE HACE ESTE PROCEDIMIENTO
			?>
    			<hr>
    		<?php
				if( $_SESSION['folio'] == "" )
				{
					// SE REALIZA EL QUERY QUE OBTIENE EL NUMERO DE FOLIO MAS ALTO
					$folio_max = "SELECT MAX(folio_num_molde + 1) 
								  FROM moldes";

					// SE EJECUTA EL QUERY QUE OBTIENE EL NUMERO DE FOLIO Y SE ALMACENA EN UNA VARIABLE
					$resultado_folio_max = mysql_query($folio_max) or die(mysql_error());

					// EL RESULTADO DEL QUERY SE ALMACENA EN UNA VARIABLE EN FORMA DE ARREGLO
					$row_folio_max = mysql_fetch_array($resultado_folio_max);

					$_SESSION['folio'] = $row_folio_max['MAX(folio_num_molde + 1)'];

					// SE VALIDA SI EL NUM DE FOLIO OBTENIDO NO ES IGUAL A 0 O A NULO
					if( $_SESSION['folio'] == 0 )
					{
						$_SESSION['folio'] = 1;
					}
				}
				?>
                <form name="guardar_nota_molde" action="proceso_guardar_nota_molde.php" method="post" class="valida_nota_molde2">
                    <input name="id_cliente" type="hidden" value="0" />
                    <input name="id_empleado" type="hidden" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" />
                    <input name="variable" value="2" type="hidden" />
                    <input name="hora_real" type="hidden" value="<?php echo $hora; ?>" />
                    <table>
                    	<tr>
                    		<td> <label class="textos"> Nº de Nota: </label> </td>
                    		<td> <input type="text" name="folio_reparacion" size="5" maxlength="5" readonly="readonly" value="<?php echo $_SESSION['folio']; ?>" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                    		<td style="text-align:right;"> <label class="textos"> Fecha: </label>	</td>
                    		<td> <input type="text" name="fecha" size="10" maxlength="10" readonly="readonly" value="<?php echo $fecha; ?>" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                    	</tr>
                    	<tr>
                    		<td> <label class="textos"> Nombre(s): </label> <input type="text" name="nombre" id="nombre" /> </td>
                    		<td> <label class="textos"> Apellido Paterno: </label> <input type="text" name="paterno" /> </td>
                    		<td> <label class="textos"> Apellido Materno: </label> <input type="text" name="materno" /> </td>
                    	    <td> <label class="textos"> Fecha de Nacimiento: </label> <input type="text" name="fecha_nacimiento" onkeyup="Validar(this,'/',patron,true)" /> </td>
                        </tr>
                    	<tr>
                    		<td> <label class="textos"> Calle: </label> <input type="text" name="calle" /> </td>
                    		<td> <label class="textos"> N° Exterior: </label> <input type="text" name="num_exterior" /> </td>
                    		<td> <label class="textos"> N° Interior: </label> <input type="text" name="num_interior" /> </td>
                    		<td> <label class="textos"> Colonia: </label> <input type="text" name="colonia" /> </td>
                    	</tr>
                    	<tr>
                    		<td>
                    			<label class="textos"> Estado: </label>
                    			<select id="id_estado" name="id_estado">
                    				<option value="0" selected="selected"> --- Estado --- </option>
                    			<?php
                    				// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DE LOS ESTADOS DE LA REPUBLICA MEXICANA
                    				$consulta_estados = "SELECT id_estado, estado 
                    									 FROM estados";

                    				// SE EJECUTA EL QUERY Y EL RESULTADO OBTENIDO SE ALMACENA EN UNA VARIABLE
                    				$resultado_estados = mysql_query($consulta_estados) or die(mysql_error());

                    				// SE REALIZA UN WHILE PARA MOSTRAR EL RESULTADO OBTENIDO
                    				while ( $row3 = mysql_fetch_array($resultado_estados) ) 
                    				{
                    					$id_estado = $row3['id_estado'];
                    					$estado = $row3['estado'];
                    				?>
                    					<option value="<?php echo $id_estado; ?>"> <?php echo utf8_encode(ucfirst(strtolower($estado))); ?> </option>
                    				<?php
                    				}

                    			?>
                    			</select>
                    		</td>
                    		<td colspan="2">
                    			<label class="textos"> Ciudad: </label> <br/>
                    			<select name="id_municipio" id="id_municipio"/>
                        			<option value="0" selected="selected"> --- Municipio --- </option>
                    			</select>
                    		</td>
                    		<td> <label class="textos"> Ciudad: </label> <input type="text" name="ciudad" id="ciudad"/> </td>
                    	</tr>
                    	<tr>
                    		<td> 
                    			<label class="textos"> Forma de Contacto: </label>  
                    			<select name="tipo_telefono"/>
                        			<option value="" selected="selected"> --- Seleccione --- </option>
                        			<option value="Telefono">Telefono</option>
                        			<option value="Celular">Celular</option>
                        			<option value="Correo">Correo</option>
                        			<option value="Fax">Fax</option>
                    			</select>
                    		</td>
                    		<td> <label class="textos"> Descripci&oacute;n: </label> <input type="text" name="descripcion" /> </td>
                    		<td> <label class="textos"> Titular: </label> <input type="text" name="titular" /> </td>
                    	</tr>
                    	<tr>
                    		<td> <input type="hidden" name="folio_molde" value="<?php echo $_SESSION['folio']; ?>" size="5" maxlength="5" readonly="readonly" /> </td>
                    		<td> <input type="hidden" name="fecha" value="<?php echo $fecha; ?>" size="10" maxlength="10" readonly="readonly" /> </td>
                    	</tr>   	
                    </table>
                    <?php
                    	// SE REALIZA QUERY PARA OBTENER LOS TIPOS DE APARATOS QUE EXISTEN
                    	$query_catalogo = "SELECT id_catalogo,estilo 
                                      	   FROM catalogo,estilos 
                                       	   WHERE estilos.id_estilo = catalogo.id_estilo 
                                       	   ORDER BY id_catalogo ASC";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
            			$resultado_query_catalogo = mysql_query($query_catalogo) or die (mysql_error());
                    ?>
            		<table style="width:450px;">
                		<tr>
                    		<td> 
                    			<label class="textos"> Estilo: </label> <br/> 
                    			<select id="estilos" name="estilos" style="width:235px;">
                					<option value="0"> --- Seleccione Estilo --- </option>
            					<?php
            					// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
                				while( $row_estilos = mysql_fetch_array($resultado_query_catalogo) )
                				{
                    				$id_catalogo = $row_estilos['id_catalogo'];
                    				$estilo = $row_estilos['estilo'];
            					?>
                					<option value="<?php echo $id_catalogo; ?>"> <?php echo $estilo; ?> </option>
            					<?php
                				}
            					?>
            					</select>
                    		</td>
                    		<td> <label class="textos"> Precio: </label> <br/> <label class="textos"> $ </label> <input type="text" name="precio_estilo" id="precio_estilo" size="6" maxlength="6" readonly="readonly" style="border:none; text-align:center; font-weight:bold;" /> </td>
                		</tr>
                		<tr>
                    		<td> 
                    			<label class="textos"> Material: </label> <br/>
                    			<select id="materiales" name="materiales" disabled="disabled" style="width:235px;">
                            		<option value="0"> --- Seleccione Material --- </option>
                        		</select>
                    		</td>
                    		<td> <label class="textos"> Precio: </label> <br/> <label class="textos"> $ </label> <input type="text" name="precio_material" id="precio_material" size="6" maxlength="6" readonly="readonly" style="border:none; text-align:center; font-weight:bold;" /> </td>
                		</tr>
                		<tr>
                    		<td> 
                    			<label class="textos"> Color: </label> <br/>
                    			<select id="colores" name="colores" disabled="disabled" style="width:235px; float:left;">
                            		<option value="0"> --- Seleccione Color --- </option>
                        		</select>
                    		</td>
                    		<td>
                    			<div class="campo_color" style="width:50px; height:20px; float:left; margin-left:8px;">  </div>
                    		</td>
                		</tr>
                		<tr>
                    		<td> 
                    			<label class="textos"> Ventilaci&oacute;n: </label> <br/>
                    			<select id="ventilaciones" name="ventilaciones" disabled="disabled" style="width:235px;">
                            		<option value="0"> --- Seleccione Ventilacion --- </option>
                        		</select>
                    		</td>
                    		<td> <label class="textos"> Precio: </label> <br/> <label class="textos"> $ </label> <input type="text" name="precio_ventilacion" id="precio_ventilacion" size="6" maxlength="6" readonly="readonly" style="border:none; text-align:center; font-weight:bold;" /> </td>
                		</tr>
                		<tr>
                    		<td> <label class="textos"> Calibre: </label> <br/> <input type="text" name="calibre" id="calibre" readonly="readonly"/> </td>
                		</tr>
                		<tr>
                    		<td> 
                    			<label class="textos"> Salida: </label> <br/>
                        		<select id="salidas" name="salidas" disabled="disabled" style="width:235px;">
                            		<option value="0"> --- Seleccione Salida --- </option>
                        		</select>
                        	</td>
                        	<td> <label class="textos"> Precio: </label> <br/> <label class="textos"> $ </label> <input name="precio_salida" type="text" id="precio_salida" size="6" maxlength="6" readonly="readonly" style="border:none; text-align:center; font-weight:bold;" /> </td>
                		</tr>
                		<tr>
                			<td> <label class="textos"> Costo Total del Molde: </label> <br/> <label class="textos"> $ </label> <input type="text" name="costo_total_molde" id="costo_total_molde" size="8" maxlength="8" readonly="readonly" style="text-align:center; border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" />  </td>
                		</tr>
                		<tr>
                			<td>
                				<label class="textos"> Lado del O&iacute;do: </label> <br/>
                    				<select name="lado_oido" id="lado_oido">
                    					<option value="0"> --- Seleccione Lado --- </option>
                        				<option value="derecho"> Derecho </option>
                        				<option value="izquierdo"> Izquierdo </option>
                        				<option value="bilateral"> Bilateral </option>
                    				</select>
                			</td>
                		</tr>
                		<tr>
                			<td>
                				<label class="textos"> Observaciones: </label> <br />
                    			<textarea name="observaciones" rows="4" cols="35" style="resize:none;"></textarea>
                			</td>
                		</tr>
            		</table>
            		<div id="imagenes2">
                		<div id="estilos_moldes" style="display:none"></div>
                		<div id="imagen_ventilacion" style="display:none"></div>
                		<div id="imagen_salida" style="display:none"></div>
            		</div>
                	<p align="right">
                    	<input type="submit" name="accion" class="fondo_boton" value="Guardar Reparacion" title="Guardar Reparación" id="guardar_reparacion"/>
                	</p>
                </form>
   				<?php
			}
		}
				?>
            	</div><!--Fin de contenido proveedor-->
        	</div><!-- Fin de area contenido -->
    	</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>