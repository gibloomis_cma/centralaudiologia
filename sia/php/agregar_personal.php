<?php session_start(); if($_SESSION['id_usuario'] == 1 || $_SESSION['id_usuario'] == 2){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Personal</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="">Seleccione</option><option value="Telefono">Telefono</option><option value="Celular">Celular</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label></td><td><input type="text" name="descripcion[]" /><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="contenido_columna2">
	<div class="contenido_pagina">
    	<div class="fondo_titulo1">
        	<div class="categoria">
            	Empleados
            </div><!-- Fin DIV categoria -->
        </div><!--Fin de fondo titulo-->
        <div class="buscar2">
		<?php
            // SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS 
            include("config.php");	
			$res2="";							
            if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                $filtro = $_POST['filtro'];				
                $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                    FROM empleados
                                                    WHERE nombre LIKE '%".$filtro."%' 
                                                    OR paterno LIKE '%".$filtro."%' 
													OR materno LIKE '%".$filtro."%'")
													or die(mysql_error());	
                
				$row_busqueda = mysql_fetch_array($res_busqueda);
                $res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
            }
        ?>              
            <form name="busqueda" method="post" action="agregar_personal.php">
                <label><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
        </div><!-- Fin de la clase buscar2 -->                
        <div class="area_contenido2">
            <center>
            <form  action="procesa_agregar_personal.php" method="post"
            name="form_agregar_personal" onsubmit="return validarAgregarPersonal()" >       
            <table>
                <tr>
                    <th>Nº Empleado</th>
                    <th>Nombre Completo</th>
                    <th>Alias</th>
                    <th colspan="2">Ocupacion</th>
                </tr>
        <?php 
            if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                $consulta_personal = mysql_query("SELECT DISTINCT(id_empleado), 
                                                    nombre, paterno, materno, alias, ocupacion
                                                    FROM empleados
                                                    WHERE nombre LIKE '%".$filtro."%' 
                                                    OR paterno LIKE '%".$filtro."%' 
                                                    OR materno LIKE '%".$filtro."%'")
                                                    or die(mysql_error());
            }else{
                $consulta_personal = mysql_query("SELECT nombre, paterno, materno, id_empleado, alias, ocupacion 
                                            FROM empleados") or die(mysql_error());
            }
			$n_empleados=0;
            while($row = mysql_fetch_array($consulta_personal)){
                $id_empleado = $row["id_empleado"];
                $nombre = ucwords($row["nombre"]);
                $paterno = ucwords($row["paterno"]);
                $materno = ucwords($row["materno"]);
                $ocupacion = ucwords($row["ocupacion"]);
                $alias = ucwords($row["alias"]);
				$n_empleados++;
        ?>                 
				<tr>
                    <td style="text-align:center"><label class="textos"><?php echo $id_empleado; ?></label></td>
                    <td>
                    	<label class="textos">
	                        <?php echo $nombre." ".$paterno." ".$materno; ?>
                        </label>
                    </td>
                    <td><label class="textos"><?php echo $alias; ?></label></td>
                    <td><label class="textos"><?php echo $ocupacion; ?></label></td> 
                    <td id="alright">
                    	 <a href="modificar_personal.php?id_empleado=<?php echo $id_empleado; ?>">
                         	<img src="../img/modify.png" title="<?php echo $alias; ?>" />
                         </a>
                    </td>                   
                </tr>
        <?php
            }
			if($n_empleados==0){
        ?>
        		<tr>
                    <td style="text-align:center" colspan="5">
                    	<label class="textos">No hay Empleados registrados</label>
                    </td>                    
                </tr>
        <?php
			}
		?>
                </table>
                <br />
                <?php
					//$empleado_max = mysql_query('')or die(mysql_error());
                    //if($_SESSION['contacto']==""){
					$empleado_max = mysql_query('SELECT MAX(id_empleado+1) FROM contador_empleados')or die(mysql_error());
					$row_empleado_max = mysql_fetch_array($empleado_max);
					$empleado = $row_empleado_max['MAX(id_empleado+1)'];
					if($empleado == "" or $empleado==0){
						$empleado = 1;
					}
				?>
                <table>
                	<tr>
                    	<th colspan="4">Nuevo Empleado</th>
                    </tr>
                	<tr>
                    	<td style="text-align:right">
                    		<input name="contacto" type="hidden" value="<?php echo $empleado; ?>" />
                    		<label class="textos">Nombre(s): </label>
                        </td><td style="text-align:left">
                   			<input name="nombre" type="text" size="25" maxlength="25" title="Nombre(s)" />
                        </td><td style="text-align:right">
                            <label class="textos">Alias: </label>
                        </td><td style="text-align:left">
                            <input name="alias" type="text" title="Alias" maxlength="15" size="25"/>
                        </td>
					</tr><tr>
  						<td style="text-align:right">
                        	<label class="textos">Paterno: </label>
                        </td><td style="text-align:left">
                          	<input name="paterno" type="text" size="25" maxlength="20" title="Apellido Paterno" />
                        </td><td style="text-align:right">
                        	<label class="textos">Materno: </label>
                        </td><td style="text-align:left">
                        	<input name="materno" type="text" size="25" maxlength="20" title="Apellido Materno" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Fecha de Nacimiento: </label>
                        </td><td style="text-align:left">
                            <input name="fecha_nacimiento" type="text" size="10" maxlength="10"
                             title="Fecha de Nacimiento" onkeyup="Validar(this,'/',patron,true)" />
                        </td><td style="text-align:right">
                        	<label class="textos">Ocupacion: </label>
                        </td><td style="text-align:left">
                        	<input name="ocupacion" type="text" title="Ocupación" />
                        </td>
                   	</tr><tr>                        
                        <td style="text-align:right">
                        	<label class="textos">Departamento: </label>
                        </td><td style="text-align:left">
                        	<select name="id_departamento" id="id_departamento">
                            	<option value="0" title="Departamento">--- Seleccione ---</option>
        					<?php 
								$consulta_departamentos = mysql_query("SELECT * FROM areas_departamentos") or die(mysql_error());			
								while($row = mysql_fetch_array($consulta_departamentos)){			
									$departamentos = $row["departamentos"];
									$id_departamento = $row["id_departamento"];			
							?>
        						<option value="<?php echo $id_departamento; ?>" ><?php	echo $departamentos; ?></option>
        					<?php
        						}
							?>	
       						</select>
						</td>
                   	</tr><tr>
                    	<td colspan="4" style="text-align:center"><label class="textos">- Direccion -</label></td>
                	</tr><tr>                    
                        <td style="text-align:right">
                        	<label class="textos">Calle: </label>
                        </td><td style="text-align:left">
                        	<input name="calle" type="text" title="Calle" size="30" maxlength="50"/>
                        </td><td style="text-align:right">
                        	<label class="textos">Numero </label>
                        </td><td style="text-align:left">
                            <label class="textos">ext.: </label>                        
                            <input name="num_ext" type="text" size="1" maxlength="5" title="Número Exterior" />                        
                            <label class="textos"> int.: </label>
                            <input name="num_int" type="text" size="1" maxlength="6" title="Número Interior" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Colonia:</label>
                        </td><td style="text-align:left">
                        	<input name="colonia" type="text" size="30" maxlength="50" title="Colonia"/>
                        </td><td style="text-align:right">
                        	<label class="textos">Codigo Postal: </label>
                        </td><td style="text-align:left">
                        	<input name="codigo_postal" type="text" size="5" maxlength="5" title="Código Postal" />
                        </td>
                    </tr><tr>
                    	<td style="text-align:right">
                        	<label class="textos">Estado: </label>
                        </td><td style="text-align:left">
                            <select id="id_estado" name="id_estado" title="Estados">
                                <option value="0" selected="selected"> --- Estado --- </option>
                            <?php
                                $consulta_estados = mysql_query("SELECT id_estado,estado FROM estados");
                                while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                    $id_estado = $row3["id_estado"];
                                    $estado = $row3["estado"];	
                            ?>
                                <option value="<?php echo $id_estado; ?>"> 
                                    <?php echo utf8_encode($estado); ?> 
                                </option>
                            <?php
                                }
                            ?>
                            </select>
                    	</td><td style="text-align:right">
                        	<label class="textos">Ciudad: </label>
                        </td><td style="text-align:left">
                            <select name="id_municipio" id="id_municipio" title="Municipios" disabled="disabled">
                                <option value="0">--- Municipio ---</option>
                            </select>
                    	</td>
                   	</td><tr>
                   		<td style="text-align:right">
                        	<label class="textos">Genero: </label>
                        </td><td style="text-align:left">                        	
                            <select name="sexo" id="sexo">
                                <option value="0" selected="selected">--- Seleccione ---</option>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino" >Masculino</option>
                            </select>
                        </td><td style="text-align:right">
                        	<label class="textos">Estado Civil:</label>
                        </td><td style="text-align:left">
                            <select name="estado_civil" id="tamaño">
                                <option value="0">--- Seleccione ---</option>
                                <option value="Casado(a)">Casado(a)</option>
                                <option value="Soltero(a)">Soltero(a)</option>
                                <option value="Divorciado(a)">Divorciado(a)</option>
                                <option value="Separado(a)">Separado(a)</option>
                            </select>
                        </td>
                	</tr><tr>                        
						<td colspan="4" style="text-align:center"><label class="textos">- Contacto -</label></td>
                	</tr><tr>  
                    	<td style="text-align:center" colspan="4">                        	
                            <label class="textos">Tipo:</label>
                            <select name="tipo_telefono[]">
                                <option value="">Seleccione</option>
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Correo">Correo</option>
                                <option value="Fax">Fax</option>
                            </select>
                            <label class="textos">Descripción:</label>
                            <input name="descripcion[]" type="text" />                         	
						</td>
                	</tr><tr>                        
                        <td style="text-align:center" colspan="4">                            
                            <div id="nuevo" style="margin-top:10px"></div>
                        </td>
                	</tr><tr>      
                    	<td colspan="4" style="text-align:center">                  
							<input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>
                       	</td>
                   	</tr><tr>
                    	<td colspan="4" style="text-align:right">                                                        
                            <input name="guardar" type="submit" value="Guardar" class="fondo_boton" title="Guardar"/>
                    	</td>
					</tr>
				</table>                        
                </form>                
        </div>
    </div><!--Fin de cuerpo-->   
</div><!--Fin de wrapp-->
</body>
</html>
<?php } ?>