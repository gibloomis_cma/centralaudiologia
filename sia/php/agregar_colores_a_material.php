<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Colores </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
						<div class="contenido_pagina">
							<div class="fondo_titulo1">
								<div class="categoria">
									Colores
								</div><!-- FIN DIV CATEGORIA -->
							</div><!-- FIN DIV FONDO TITULO1 -->
								<div class="area_contenido1">
									<br/>
									<?php
										// SE RECIBE EL ID DEL MATERIAL REGISTRADO
										$id_material = $_GET['id_material'];
										// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
										include("config.php");
										// QUERY QUE OBTIENE EL NOMBRE DEL MATERIAL REGISTRADO ANTERIORMENTE
										$query_material_consultado = mysql_query("SELECT material
																				  FROM materiales
																				  WHERE id_material = '$id_material'") or die (mysql_error());
										$row_nombre_material = mysql_fetch_array($query_material_consultado);
										$nombre_material = $row_nombre_material['material'];
										// QUERY QUE OBTIENE TODOS LOS COLORES
										$query_colores = mysql_query("SELECT id_color,color
																	  FROM colores
																	  ORDER BY id_color ASC") or die (mysql_error());
									?>
									<div class="titulos"> Material </div><!-- FIN DIV TITULOS -->
									<br/>
									<center>
										<table>
											<tr>
												<td> <label class="textos"> Nombre del Material: </label> &nbsp;&nbsp; </td>
												<td> <label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $nombre_material; ?> </label> </td>
											</tr>
										</table><!-- FIN TABLA -->
									</center><!-- FIN DE CENTER -->
									<br/>
									<div class="titulos"> Colores </div><!-- FIN DIV TITULOS -->
									<br/>
									<?php 
										// SE REALIZA EL QUERY QUE OBTIENE LOS COLORES QUE SE ACABAN DE AGREGAR AL MATERIAL
										$query_colores_material = mysql_query("SELECT color,r,g,b
																			   FROM colores,materiales_nivel1
																			   WHERE materiales_nivel1.id_color = colores.id_color AND
																			   id_material = '$id_material'") or die (mysql_error());
									?>
									<center>
										<table>
										<?php
											// SE REALIZA UN CICLO QUE MUESTRA LOS COLORES DEL MATERIAL
											while( $row_colores_material = mysql_fetch_array($query_colores_material) )
											{
												$color = $row_colores_material['color'];
												$r = $row_colores_material['r'];
												$g = $row_colores_material['g'];
												$b = $row_colores_material['b'];
											?>
												<tr>
													<td> <label class="textos"> <?php echo $color; ?> </label> </td>
													<td> <input type="text" name="color" id="color" size="5" readonly="readonly" style=" border:none; background:rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b ?>)"/> </td>
												</tr>
											<?php
											}
										?>
										</table>
									</center>
									<br/>
									<div class="titulos"> Agregar Colores </div><!-- FIN DIV TITULOS -->
									<br/>
									<center>
										<form name="form_agregar_colores" id="form_agregar_colores" method="post" action="procesa_agregar_colores_a_material.php">
											<table id="agregar_nuevo_color">
												<tr>
													<td> <input type="hidden" name="txt_id_material" id="txt_id_material" value="<?php echo $id_material; ?>"/> </td>
												</tr>
												<tr>
													<td> <label class="textos"> Nombre Color: </label> </td>
													<td>
														<select name="colores[]">
															<option value="0"> --- Seleccione Color --- </option>
														<?php 
														while( $row_color = mysql_fetch_array($query_colores) )
														{
															$id_color = $row_color['id_color'];
															$nombre_color = $row_color['color'];
														?>
															<option value="<?php echo $id_color; ?>"> <?php echo ucwords(strtolower($nombre_color)); ?> </option>
														<?php
														}
														?>
														</select>
													</td>
													<td> <input type="button" name="agregar_color" id="agregar_color" value="Agregar Nuevo Color" title="Agregar Nuevo Color" class="fondo_boton"/> </td>
												</tr>
											</table><!-- FIN TABLA -->
											<br/>
											<div id="boton_guardar_colores">
												<p align="right">
													<input type="button" name="terminar_agregar_color" id="terminar_agregar_color" value="Terminar" title="Terminar" class="fondo_boton"/>
													<input type="submit" name="accion" class="fondo_boton" value="Guardar" title="Guardar"/>
												</p><!-- FIN PARRAFO -->
											</div><!-- FIN DIV BOTON GUARDAR COLORES -->
											<br/><br/>
										</form><!-- FIN FORM -->
									</center><!-- FIN CENTER -->
								</div><!-- FIN DIV AREA CONTENIDO 1 -->
						</div><!-- FIN DIV CONTENIDO PAGINA -->
					</div><!-- FIN DIV CONTENDIO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>

</html>
