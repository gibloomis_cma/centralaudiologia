<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include 'config.php';

	// SE RECIBE LA VARIABLE ID ESTADO POR METODO POST
	$id_estado = $_POST['id_estado'];

	// SE REALIZA QUERY QUE OBTIENE LAS CIUDADES DE ACUERDO AL ESTADO OBTENIDO
	$query_estado_ciudades = "SELECT id_ciudad,ciudad
							  FROM ciudades
							  WHERE id_estado = '$id_estado'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
	$resultado_estado_ciudades = mysql_query($query_estado_ciudades) or die(mysql_error());

	// SE REALIZA CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
	while ( $row_ciudad = mysql_fetch_array($resultado_estado_ciudades) )
	{
		$id_ciudad = $row_ciudad['id_ciudad'];
		$ciudad = $row_ciudad['ciudad'];
	?>
		<option value="<?php echo $id_ciudad; ?>"> <?php echo utf8_encode(ucwords(strtolower($ciudad))); ?> </option>
	<?php
	}	
?>