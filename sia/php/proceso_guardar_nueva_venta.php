<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');
	
	//include("metodo_cambiar_fecha.php");
	
	$accion = $_POST["accion"];
	$descripcion_producto = $_POST["desc"];
	$descripcion_producto1 = $_POST["desc1"];
	$id_subcategoria = $_POST["desc3"];
	$folio_num_reparacion = $_POST["desc4"];
	$cantidad = $_POST["cantidad"];
	$costo = $_POST["costo"];
	$costo1 = $_POST["costo1"];
	$costo4 = $_POST["costo4"];
	$costo5 = $_POST["costo5"];
	$costo6 = $_POST["costo6"];
	$folio_num_molde = $_POST["nota_molde"];
	$folio_num_venta = $_POST["venta"];
	$categoria = $_POST["categoria"];
	$sucursal = $_POST["sucursal"];
	$hora = $_POST['hora'];
	$fecha = $_POST["fecha"];
	$fecha_separada = explode("/", $fecha);
	$fecha_mysql = $fecha_separada[2]."-".$fecha_separada[1]."-".$fecha_separada[0];
	$descuento = $_POST["descuento"];
	$id_empleado = $_POST["id_empleado"]; 
	$id_doctor = $_POST["id_doctor"];
	$recibio = $_POST["recibio"];
	$entrego = $_POST["entrego"];
	$pago_parcial = $_POST['pago_parcial'];

	if( $accion == "Aceptar" )
	{
		header('Location: formato_venta.php?folio='.$folio_num_venta.'&descuento='.$descuento.'&fecha='.$fecha_mysql.'&id_empleado='.$id_empleado.'&sucursal='.$sucursal.'&hora='.$hora);
		exit;
	}

	if( $cantidad == 0 )
	{
		echo "Vacio";
		header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal);
		exit;
	}

	if( $accion == "Agregar" )
	{
		if( $categoria == 1 )
		{
			/* QUERY PARA GUARDAR LA REFACCION EN LA TABLA DESCRIPCION DE VENTA */
			mysql_query("INSERT INTO descripcion_venta(id_sucursal, id_categoria, folio_num_venta, descripcion, cantidad,
														 costo_unitario)
												VALUES('".$sucursal."','1','".$folio_num_venta."','".$descripcion_producto."',
														'".$cantidad."','".$costo."')") or die(mysql_error());
														
			$id_registro_descripcion_venta = mysql_insert_id(); //Guardo el id de la descripcion de la venta
											
			/* QUERY PARA BUSCAR EL ID_BASE_PRODUCTO Y ID_SUBCATEGORIA DE LA TABLA BASE PRODUCTOS */
			$consulta_datos_base_productos = mysql_query("SELECT id_subcategoria, id_base_producto
														  FROM base_productos
														  WHERE descripcion='".$descripcion_producto."'") or die(mysql_error());

			$row = mysql_fetch_array($consulta_datos_base_productos);
			$id_subcategoria = $row["id_subcategoria"];	
			$id_base_producto = $row["id_base_producto"];			
			
			/* SE OBTIENE EL IDENTIFICADOR PARA CONSULTAR PARA QUE ALMACEN VAN */
			if( $sucursal == 1 )
			{
				$id_almacen = 2;	
			}
			else
			{
				$id_almacen = 4;	
			}

			/* QUERY PARA BUSCAR LOS PRODUCTOS DEL INVENTARIO */
			$consulta_datos_inventario = mysql_query("SELECT id_registro, num_serie_cantidad, id_almacen
													  FROM inventarios
													  WHERE id_subcategoria='".$id_subcategoria."' 
														    AND id_articulo='".$id_base_producto."' 
														    AND id_almacen='".$id_almacen."'
														    ORDER BY id_registro ASC") or die(mysql_error());

			while( $row2 = mysql_fetch_array($consulta_datos_inventario) )
			{
				$id_registro[] = $row2["id_registro"];
				$num_serie_cantidad[] = $row2["num_serie_cantidad"];
				$id_almacen_inventario[] = $row2["id_almacen"];
			}

			for($i = 0; $i < count($num_serie_cantidad); $i++)
			{
				$id_almacen_inventario[$i];	
				$id_registro[$i];
				$num_serie_cantidad[$i];
				$cantidad_original_num_serie = $num_serie_cantidad[$i];
				if( $cantidad != 0 )
				{
					$cantidad_nueva = $cantidad - $num_serie_cantidad[$i];
					$nueva_cantidad = $cantidad;
					$cantidad = $cantidad_nueva;
					if( $cantidad_nueva <= 0 )
					{
						$cantidad = 0;
						$num_serie_cantidad[$i] = -($cantidad_nueva);
						mysql_query("INSERT INTO venta_temporal(id_registro_inventario, cantidad,id_registro_descripcion_venta)
									 VALUES('".$id_registro[$i]."','".$nueva_cantidad."','".$id_registro_descripcion_venta."')") or die(mysql_error());

						mysql_query("UPDATE inventarios SET num_serie_cantidad='".$num_serie_cantidad[$i]."'
									 WHERE id_registro='".$id_registro[$i]."' AND id_almacen=".$id_almacen_inventario[$i]) or die(mysql_error());
					}
					else
					{
						mysql_query("INSERT INTO venta_temporal(id_registro_inventario, cantidad, id_registro_descripcion_venta)
									 VALUES('".$id_registro[$i]."','".$cantidad_original_num_serie."','".$id_registro_descripcion_venta."')") or die(mysql_error());						
						
						mysql_query("UPDATE inventarios SET num_serie_cantidad=0
      								 WHERE id_registro='".$id_registro[$i]."' AND id_almacen=".$id_almacen_inventario[$i]) or die(mysql_error());							
					}	
				}
			}
			header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal.'&descuento='.$descuento);
		}

		if( $categoria == 4 )
		{	
			/* QUERY PARA GUARDAR LA REFACCION EN LA TABLA DESCRIPCION DE VENTA */
			mysql_query("INSERT INTO descripcion_venta(id_sucursal,id_categoria, folio_num_venta, descripcion, cantidad, costo_unitario)
						 VALUES('".$sucursal."','4','".$folio_num_venta."','".$descripcion_producto."','".$cantidad."','".$costo."')") or die(mysql_error());
														
			/* SE GUARDA EL ID DE LA DESCRIPCION DE LA VENTA */
			$id_registro_descripcion_venta = mysql_insert_id();

			/* QUERY PARA BUSCAR EL ID_BASE_PRODUCTO Y ID_SUBCATEGORIA DE LA TABLA BASE PRODUCTOS */											
			$consulta_datos_base_productos = mysql_query("SELECT id_subcategoria, id_base_producto
														  FROM base_productos
														  WHERE descripcion = '".$descripcion_producto."'") or die(mysql_error());

			$row = mysql_fetch_array($consulta_datos_base_productos);
			$id_subcategoria = $row["id_subcategoria"];
			$id_base_producto = $row["id_base_producto"];
			
			/* SE OBTIENE EL IDENTIFICADOR PARA CONSULTAR PARA QUE ALMACEN VAN */
			if( $sucursal == 1 )
			{
				$id_almacen2 = 2;	
			}
			else
			{
				$id_almacen2 = 4;	
			}

			/* QUERY PARA BUSCAR LOS PRODUCTOS DEL INVENTARIO */
			$consulta_datos_inventario = mysql_query("SELECT id_registro, num_serie_cantidad, id_almacen
													  FROM inventarios
													  WHERE id_subcategoria = '".$id_subcategoria."' AND
															id_articulo = '".$id_base_producto."' AND
															id_almacen = '".$id_almacen2."' 
															ORDER BY id_registro ASC") or die(mysql_error());

			while( $row2 = mysql_fetch_array($consulta_datos_inventario) )
			{
				$id_registro[] = $row2["id_registro"];
				$num_serie_cantidad[] = $row2["num_serie_cantidad"];
				$id_almacen_inventario[] = $row2["id_almacen"];
			}	
			for( $i = 0; $i < count($num_serie_cantidad); $i++ )
			{
				$id_registro[$i];
				$num_serie_cantidad[$i];
				$id_almacen_inventario[$i];
				$cantidad_original_num_serie = $num_serie_cantidad[$i];

				if( $cantidad != 0 )
				{
					$cantidad_nueva = $cantidad - $num_serie_cantidad[$i];
					$nueva_cantidad = $cantidad;
					$cantidad = $cantidad_nueva;
					if( $cantidad_nueva <= 0 )
					{
						$cantidad = 0;
						$num_serie_cantidad[$i] = -($cantidad_nueva);			
						mysql_query("INSERT INTO venta_temporal(id_registro_inventario, cantidad,id_registro_descripcion_venta)
									 VALUES('".$id_registro[$i]."','".$nueva_cantidad."','".$id_registro_descripcion_venta."')") or die(mysql_error());

						mysql_query("UPDATE inventarios SET num_serie_cantidad='".$num_serie_cantidad[$i]."'
									 WHERE id_registro='".$id_registro[$i]."' AND id_almacen=".$id_almacen_inventario[$i]) or die(mysql_error());
					}
					else
					{
						mysql_query("INSERT INTO venta_temporal(id_registro_inventario, cantidad,id_registro_descripcion_venta)
									 VALUES('".$id_registro[$i]."','".$cantidad_original_num_serie."','".$id_registro_descripcion_venta."')") or die(mysql_error());						
							
						mysql_query("UPDATE inventarios SET num_serie_cantidad = 0
									 WHERE id_registro='".$id_registro[$i]."' AND id_almacen=".$id_almacen_inventario[$i]) or die(mysql_error());			
					}
				}			
			}
			header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal.'&descuento='.$descuento);
		}

		/* SI LA CATEGORIA ES IGUAL A 2, SE GUARDARA UNA CONSULTA */
		if( $categoria == 2 )
		{
			$consulta_descripcion_servicio_medico = mysql_query("SELECT subcategoria 
																 FROM subcategorias_productos 
																 WHERE id_subcategoria=".$id_subcategoria) or die(mysql_error());

			$row_descripcion_servicios_medico = mysql_fetch_array($consulta_descripcion_servicio_medico);
			$subcategoria_servicio_medico = $row_descripcion_servicios_medico["subcategoria"];

			mysql_query("INSERT INTO descripcion_venta(id_sucursal, id_categoria, folio_num_venta, descripcion, cantidad,costo_unitario)
						 VALUES('".$sucursal."','2','".$folio_num_venta."','".$subcategoria_servicio_medico."','".$cantidad."','".$costo4."')") or die(mysql_error());

			$id_descripcion_venta_consulta = mysql_insert_id();

			mysql_query("INSERT INTO pago_consultas(folio_num_venta, id_registro_descripcion_venta, id_subcategoria, id_empleado,fecha, cantidad, estado)
						 VALUES('".$folio_num_venta."','".$id_descripcion_venta_consulta."','".$id_subcategoria."','".$id_doctor."','".$fecha_mysql."','".$cantidad."','No pagado')") or die(mysql_error());
			
			header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal.'&descuento='.$descuento);
		}
					
		/* SI LA CATEGORIA ES IGUAL A 100, SE GUARDA LA REPARACION */
		if( $categoria == 100 )
		{
			mysql_query("INSERT INTO descripcion_venta(id_sucursal, id_categoria, folio_num_venta, descripcion, cantidad,costo_unitario)
						 VALUES('".$sucursal."','100','".$folio_num_venta."','Folio de reparacion N° ".$folio_num_reparacion."','".$cantidad."','".$costo5."')") or die(mysql_error());
														
			mysql_query("UPDATE reparaciones_laboratorio SET fecha_entrega = '".$fecha_mysql."',cobro = '".$costo5."',entrego = '".$entrego."',recibio = '".$recibio."'
						 WHERE folio_num_reparacion = '".$folio_num_reparacion."'") or die(mysql_error());
			
			header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal.'&descuento='.$descuento);
		}

		if( $categoria == 200 )
		{
			// SE REALIZA QUERY QUE OBTIENE SI SE HA DADO UN PAGO ANTERIOR
			$query_pago_anterior = "SELECT pago_parcial
									FROM moldes
									WHERE folio_num_molde = '$folio_num_molde'";

			// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
			$resultado_pago_anterior = mysql_query($query_pago_anterior) or die(mysql_error());
			$row_pago_anterior = mysql_fetch_array($resultado_pago_anterior);
			$pago_anterior = $row_pago_anterior['pago_parcial'];

			if ( $pago_anterior == 0 )
			{
				mysql_query("INSERT INTO descripcion_venta(id_sucursal, id_categoria, folio_num_venta, descripcion, cantidad,costo_unitario,pago_parcial)
						 	 VALUES('".$sucursal."','200','".$folio_num_venta."','Folio de molde N° ".$folio_num_molde."','".$cantidad."','".$costo6."','".$pago_parcial."')") or die(mysql_error());

				if ( $pago_parcial == "" )
				{
					mysql_query("UPDATE moldes
							 SET pago_parcial = '$costo6'
							 WHERE folio_num_molde = '$folio_num_molde'") or die(mysql_error());
				}
				else
				{
					mysql_query("UPDATE moldes
							 SET pago_parcial = '$pago_parcial'
							 WHERE folio_num_molde = '$folio_num_molde'") or die(mysql_error());
				}

				mysql_query("UPDATE moldes_elaboracion 
						     SET fecha_entrega = '$fecha_mysql', cobro = '$costo6', quien_entrego = '$entrego', recibio = '$recibio'
							 WHERE folio_num_molde = '".$folio_num_molde."'") or die(mysql_error());
			}
			else
			{
				mysql_query("INSERT INTO descripcion_venta(id_sucursal, id_categoria, folio_num_venta, descripcion, cantidad,costo_unitario,pago_parcial_2)
						 	 VALUES('".$sucursal."','200','".$folio_num_venta."','Folio de molde N° ".$folio_num_molde."','".$cantidad."','".$costo6."','".$costo6."')") or die(mysql_error());

				mysql_query("UPDATE moldes
						 	 SET pago_parcial_2 = '$costo6'
						 	 WHERE folio_num_molde = '$folio_num_molde'") or die(mysql_error());

				mysql_query("UPDATE moldes_elaboracion 
					     	 SET fecha_entrega = '$fecha_mysql', cobro = '$costo6', quien_entrego = '$entrego', recibio = '$recibio'
						 	 WHERE folio_num_molde = '".$folio_num_molde."'") or die(mysql_error());
			}
				

			header('Location: nueva_venta2.php?folio='.$folio_num_venta.'&id_sucursal='.$sucursal.'&descuento='.$descuento);
		}
	}	
?>