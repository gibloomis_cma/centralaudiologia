<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE EL ID DEL VALE DE REFACCION A TRAVES DEL METODO POST
	$id_vale_refaccion = $_POST['id_vale_refaccion'];

	// SE REALIZA QUERY QUE OBTIENE LA INFORMACION DEL VALE DE REFACCION DE ACUERDO AL ID RECIBIDO ANTERIORMENTE
	$query_vale_refaccion = "SELECT uso_refacciones.uso AS uso,fecha,id_sucursal,origen,CONCAT(nombre,' ', paterno) AS responsable 
						   	 FROM vales_refacciones, uso_refacciones, empleados,areas_departamentos, descripcion_vale_refacciones
						   	 WHERE vales_refacciones.uso = uso_refacciones.id_uso_refaccion
						   	 AND responsable = id_empleado
						   	 AND vales_refacciones.id_registro = descripcion_vale_refacciones.id_vale_refaccion
 						   	 AND areas_departamentos.id_departamento = empleados.id_departamento
						   	 AND vales_refacciones.id_registro = '$id_vale_refaccion'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_vale_refaccion = mysql_query($query_vale_refaccion) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_vale_refaccion = mysql_fetch_array($resultado_vale_refaccion);

	// SE DECLARAN LAS VARIABLES CON EL RESULTADO FINAL DEL QUERY
	$folio_vale_refaccion = $id_vale_refaccion;
	$fecha = $row_vale_refaccion['fecha'];
	$fecha_separada = explode("-", $fecha);
	$fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
	$responsable = $row_vale_refaccion['responsable'];
	$uso_refaccion = $row_vale_refaccion['uso'];
	$sucursal = $row_vale_refaccion['id_sucursal'];
	$origen = $row_vale_refaccion['origen'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DE LA SUCURSAL
	$query_nombre_sucursal = "SELECT nombre
							  FROM sucursales
							  WHERE id_sucursal = '$sucursal'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_sucursal = mysql_query($query_nombre_sucursal) or die(mysql_error());
	$row_nombre_sucursal = mysql_fetch_array($resultado_nombre_sucursal);

	// SE DECLARA UNA VARIABLE CON EL RESULTADO FINAL DEL QUERY
	$nombre_sucursal = $row_nombre_sucursal['nombre'];

	// SE REALIZA QUERY QUE OBTIENE EL NOMBRE DEL ALMACEN
	$query_nombre_almacen = "SELECT almacen
							 FROM almacenes
							 WHERE id_almacen = '$origen'";

	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_almacen = mysql_query($query_nombre_almacen) or die(mysql_error());

	// SE ALMACENA EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
	$row_nombre_almacen = mysql_fetch_array($resultado_nombre_almacen);

	// SE DECLARA UNA VARIABLE CON EL RESULTADO FINAL
	$nombre_almacen = $row_nombre_almacen['almacen'];

	// SE IMPRIMEN LAS VARIABLES CORRESPONDIENTES PARA MOSTRAR LA INFORMACION EN PANTALLA
	echo $folio_vale_refaccion;
	echo "°";
	echo $fecha_normal;
	echo "°";
	echo $nombre_sucursal;
	echo "°";
	echo $responsable;
	echo "°";
	echo $nombre_almacen;
	echo "°";
	echo $uso_refaccion;
?>