<?php
	// SE INICIA SESION
	session_start();

	// SE DECLARA UNA VARIABLE CON EL ID DEL USUARIO QUE INICIO SESION
	$id_usuario_logueado = $_SESSION['id_empleado_usuario'];

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO AL QUE PERTENCE EL USUARIO EN EL SISTEMA
	$query_departamento = "SELECT id_departamento
						   FROM empleados
						   WHERE id_empleado = '$id_usuario_logueado'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_departamento = mysql_query($query_departamento) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_departamento = mysql_fetch_array($resultado_departamento);
	$id_departamento_empleado = $row_departamento['id_departamento'];

	// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO AL QUE PERTENECE EL EMPLEADO EN EL SISTEMA
	$query_sucursal = "SELECT id_sucursal
					   FROM areas_departamentos
					   WHERE id_departamento = '$id_departamento_empleado'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
	$row_sucursal = mysql_fetch_array($resultado_sucursal);
	$id_sucursal = $row_sucursal['id_sucursal'];

	// SE REALIZA EL QUERY QUE OBTIENE LOS FOLIOS DE LAS VENTAS REALIZADAS
	$query_folio_reparacion = "SELECT folio_num_reparacion
							   FROM reparaciones
							   ORDER BY folio_num_reparacion DESC";

	// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
	$resultado_folio_reparacion = mysql_query($query_folio_reparacion) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reimpresi&oacute;n de Nota de Reparaci&oacute;n </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
</head>
<body>
	<div id="wrapp">
		<div id="contenido_columna2">
			<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:700px;">
						Reimpresi&oacute;n Nota de Reparaci&oacute;n
					</div><!-- FIN DIV CATEGORIA -->
				</div><!-- FIN DIV FONDO TITULO 1 -->
				<div class="area_contenido1">
					<div class="contenido_proveedor">
						<br/>
						<div class="titulos"> Informaci&oacute;n de la Nota de Reparaci&oacute;n </div>
						<br/>
						<form name="form_datos_nota_reparacion" id="form_datos_nota_reparacion" method="post" action="funciones_reimpresiones_ticket.php">
							<table style="margin-left:20px;">
								<tr>
									<td style="text-align:center;"> 
										<label class="textos"> N° de Nota: </label> &nbsp;
										<select name="folio_nota_reparacion" id="folio_nota_reparacion">
											<option value="0"> --- Seleccione Folio --- </option>
									<?php
										// SE REALIZA CICLO QUE MUESTRA TODOS LOS FOLIOS DE LA TABLA DE VALES DE BATERIA
										while ( $row_folio = mysql_fetch_array($resultado_folio_reparacion) ) 
										{
											$num_reparacion = $row_folio['folio_num_reparacion'];
										?>
											<option value="<?php echo $num_reparacion; ?>"> N° de Folio: &nbsp; <?php echo $num_reparacion; ?> </option>
										<?php
										}
									?>
										</select>
									</td>
								</tr>
							</table><!-- FIN TABLA FOLIOS DE VALE DE BATERIAS -->
							<br/>
							<table style="margin-left:40px;">
								<tr>
									<td style="text-align:left;"> <label class="textos"> N° de Folio: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Fecha: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="num_folio" id="num_folio" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="fecha_folio" id="fecha_folio" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td style="text-align:left;"> <label class="textos"> Cliente: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Modelo: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="nombre_cliente" id="nombre_cliente" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="modelo" id="modelo" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td style="text-align:left;"> <label class="textos"> N° de Serie: </label> </td>
									<td width="50">  </td>
									<td style="text-align:left;"> <label class="textos"> Estatus de Reparaci&oacute;n: </label> </td>
								</tr>
								<tr>
									<td> <input type="text" name="num_serie" id="num_serie" size="30"  readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
									<td width="50">  </td>
									<td> <input type="text" name="estatus_reparacion" id="estatus_reparacion" size="30" readonly="readonly" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
								</tr>
								<tr>
									<td colspan="4"> <br/> </td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:right"> <input type="submit" name="btn_reimprimir_reparacion" id="btn_reimprimir_reparacion" value="Reimprimir Reparacion" title="Reimprimir Nota de Reparaci&oacute;n" class="fondo_boton" style="display:none;" /> </td>
								</tr>
							</table><!-- FIN TABLA INFORMACION VALE DE BATERIAS -->
						</form>
					</div><!-- FIN DIV CONTENIDO PROVEEDOR -->
				</div><!-- FIN DIV AREA CONTENIDO 1 -->
			</div><!-- FIN DIV CONTENIDO PAGINA -->
		</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>
</html> 