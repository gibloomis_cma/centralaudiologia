<!-- include style -->
 <link rel="stylesheet" type="text/css" href="../css/style_pagination.css" />

<?php
    //open database
    include 'config.php';
    
    //include our awesome pagination
    //class (library)
    include '../librerias/ps_pagination.php';

    $filtro = $_POST['filtro'];

    if ( $filtro != "" )
    {
        // query all data anyway you want
        $sql = "SELECT DISTINCT(id_cliente), nombre, paterno, materno
                FROM ficha_identificacion
                WHERE nombre LIKE '%".$filtro."%' 
                OR paterno LIKE '%".$filtro."%'
                OR materno LIKE '%".$filtro."%'";
    }
    else
    {
        //query all data anyway you want
        $sql = "SELECT id_cliente, nombre, paterno, materno 
                FROM ficha_identificacion
                ORDER BY id_cliente";
    }

    //execute query and get and
    $rs = mysql_query($sql) or die(mysql_error());

    //now, where gonna use our pagination class
    //this is a significant part of our pagination
    //i will explain the PS_Pagination parameters
    //$conn is a variable from our config_open_db.php
    //$sql is our sql statement above
    //3 is the number of records retrieved per page
    //4 is the number of page numbers rendered below
    //null - i used null since in dont have any other
    //parameters to pass (i.e. param1=valu1&param2=value2)
    //you can use this if you're gonna use this class for search
    //results since you will have to pass search keywords
    $pager = new PS_Pagination( $link, $sql, 20, 3, null );

    //our pagination class will render new
    //recordset (search results now are limited
    //for pagination)
    $rs = $pager->paginate(); 

    //get retrieved rows to check if
    //there are retrieved data
    $num = mysql_num_rows( $rs );

    if( $num >= 1 )
    {     
    ?>
        <table>
            <tr>
                <th width="80px">Nº</th>
                <th width="250px">Nombre Completo</th>
                <th width="150px">Historial Clinico</th>
                <th width="150px">Nuevo Examen</th>
            </tr>
			<?php    
                //looping through the records retrieved class='data-tr' align='center'
				$n_cliente=0;
                while( $row = mysql_fetch_array( $rs ) )
                {
                    $id_cliente = $row["id_cliente"];
                    $paterno = $row["paterno"];
                    $materno = $row["materno"];
                    $nombre = $row["nombre"];
                    $consulta_datos_anamnesis = mysql_query("SELECT COUNT(id_anamnesis) FROM anamnesis 
                                                             WHERE id_cliente=".$id_cliente)
                                                             or die(mysql_error());
                    $row_consulta_datos_anamnesis = mysql_fetch_array($consulta_datos_anamnesis);
                    $contador_anamnesis = $row_consulta_datos_anamnesis["COUNT(id_anamnesis)"];
                    $n_cliente++;
                                                     
            ?>
            <tr>
            	<td>
					<label class="textos"><?php echo $id_cliente; ?></label>
                </td>
                <td>
					<label class="textos"><?php echo $nombre." ".$paterno." ".$materno; ?></label>
                            </td>
                            <td style="text-align:center">
                                <a href="lista_de_estudios.php?id_paciente=<?php echo $id_cliente; ?>">
                                    <img src="../img/inventary.png" />
                                </a>
                            </td>
                            <td style="text-align:center">
                            <?php
                                if($contador_anamnesis == 0){
                            ?>
                            
                                <a href="ventana_emergente.php?id_paciente=<?php echo $id_cliente; ?>">
                                    <img src="../img/new.png" />
                                </a>
                            <?php
                                }else{
                            ?>
                                <a href="nuevo_estudio.php?id_paciente=<?php echo $id_cliente; ?>">
                                    <img src="../img/copy.png" />
                                </a>
                            <?php
                                }
                            ?>
                            </td>
                        </tr>
                        <?php
                            }
                            
    ?>
        </table>
    <?php
    }
    else
    {
        // if no records found
        echo "<h3> No hay pacientes registrados</h3>";
    }

    //page-nav class to control
    //the appearance of our page 
    //number navigation
    echo "<div class='page-nav'>";
         //display our page number navigation
         echo $pager->renderFullNav();
    echo "</div>";

?>