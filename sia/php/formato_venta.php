<?php
    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include('config.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Nueva Venta </title>
    <link rel="stylesheet" href="../css/style3.css" type="text/css"/>
    <script language="javascript" type="text/javascript" src="../js/jquery-1.7.1.js"></script>
    <script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body onload="javascript:document.forma1.cantidad.focus();">
<div id="wrapp">
	<div id="contenido_columna2">
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
    			<div class="categoria">
        			Ventas
            	</div> <!-- FIN DIV CATEGORIA -->
       		</div><!-- FIN DIV FONDO TITULO 1 -->
        	<div class="area_contenido1">
                <br />
				<?php		
                    $folio_num_venta = $_GET["folio"];
                    $descuento = $_GET["descuento"];
					$sucursal = $_GET["sucursal"];
                    $hora = $_GET['hora'];			
					
                    /* SE VALIDA SI EL DESCUENTO ES DIFERENTE DE 0 */
                    if( $descuento == "" )
                    {
                        $descuento = 0;
					}

                    $fecha = $_GET["fecha"];
                    $id_empleado = $_GET["id_empleado"];
                    
                    /* SE REALIZA UN QUERY QUE OBTIENE EL NOMBRE DEL VENDEDOR */
                    $consulta_vendedor = mysql_query("SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo
                                                      FROM empleados
                                                      WHERE id_empleado = ".$id_empleado) or die(mysql_error());

                    $row = mysql_fetch_array($consulta_vendedor);
                    $nombre_empleado = $row['nombre_completo'];
                ?>
                <div class="contenido_proveedor">
                    <center>
                    <form name="forma1" id="forma1" method="post" action="proceso_guardar_orden_venta.php" onsubmit="return confirm('Efectivo ingresado = ' + document.forma1.cantidad.value + '\n Su cambio es = ' + document.forma1.cambio.value);">
                        <input name="sucursal" type="hidden" value="<?php echo $sucursal; ?>" />
                        <input type="hidden" name="hora_venta" value="<?php echo $hora; ?>" />
                        <table>
                            <tr>
                                <td style="text-align:center" colspan="4">
                            	    <label class="textos" style="font-size:16px;">JORGE LEAL Y CIA. S.A. DE C.V. </label>
                        		    <br />
                            		<label class="textos" style="font-size:14px;">Sebastian Lerdo de Tejada 186 </label>
                                    <br />
                                    <label class="textos" style="font-size:14px;">Col. Centro &nbsp;&nbsp;&nbsp; 
                                    Morelia &nbsp;&nbsp;&nbsp; Michoacan </label>
                                    <br />
                                    <label class="textos" style="font-size:14px;">CP. 58000 </label>
                                    <br />
                                    <label class="textos" style="font-size:14px;">RFC: JLE010622GK2 </label>
                                    <br />
                                    <label class="textos" style="font-size:14px;">Telefono: (443) 312-5994 </label>
                                    <br /><br />
                        		</td>
                        	</tr>
                            <tr>
                                <td colspan="4" style="text-align:center;">
								    <?php
                                       /* SE VALIDA DE QUE SUCURSAL ES LA VENTA */ 
									   if( $sucursal == 1 )
                                       {
                                          echo "Sucursal Matriz";	
									   }
                                       else
                                       {
										  echo "Sucursal Ocolusen";	
									   }
									?>
                                </td>
                            </tr>
                            <tr>
                                <td id="alright">
                                    <label class="textos">N° Folio: </label>
                                </td>
                                <td>
                                    <input name="folio" type="hidden" value="<?php echo $folio_num_venta; ?>" />
                                    <?php echo $folio_num_venta; ?>
                                </td>
                                <td id="alright">
                                    <label class="textos">Fecha: </label>
                                </td>
                                <td>
                                    <input name="fecha" type="hidden" value="<?php echo $fecha; ?>" />
									<?php include('fecha.php'); ?>                            
                                </td>
                                </tr>
                                <tr>
                                    <td id="alright">
                                        <label class="textos">Descuento: </label>
                                    </td>
                                    <td>
                                    	<input name="descuento" type="hidden" value="<?php echo $descuento; ?>" />
                                    	<?php echo $descuento."%"; ?>                                        
                                    </td>
                                    <td id="alright">
                                    	<input name="vendedor" type="hidden" value="<?php echo $id_empleado; ?>" />
                                        <label class="textos">Vendedor: </label>
                                    </td>
                                    <td>
                                    	<?php echo ucwords(strtolower($nombre_empleado)); ?>
                                    </td>
                                </tr>
                        	</table>
                        	<br /><br />                                        
                            <table style="border-collapse:collapse;">
                                <tr>
                                    <th width="50px"> Cantidad </th>
                                    <th width="265px"> Descripcion </th>
                                    <th width="100px"> Costo unitario </th>
                                    <th width="100px"> Pago Parcial </th>
                                </tr>
								<?php
                                    /* SE REALIZA UN QUERY QUE OBTIENE LOS DATOS DE LA VENTA DE LA TABLA DE DESCRIPCION VENTA DE ACUERDO AL FOLIO DE VENTA RECIBIDO POR METODO GET ASI COMO LA SUCURSAL */
									$consulta_datos_venta = mysql_query("SELECT id_categoria,descripcion, costo_unitario, pago_parcial, SUM(cantidad) as cantidad
																		 FROM descripcion_venta 
																		 WHERE folio_num_venta=".$folio_num_venta." AND id_sucursal=".$sucursal."
																		 GROUP BY descripcion") or die(mysql_error());

                                    $total_venta = 0;
									while( $row3 = mysql_fetch_array($consulta_datos_venta) )
                                    {								
										$cantidad = $row3["cantidad"];
										$costo_unitario = $row3["costo_unitario"];
										$descripcion = $row3["descripcion"];
                                        $pago_parcial = $row3['pago_parcial'];
                                        $id_categoria = $row3['id_categoria'];

										if( $cantidad <= 3)
                                        {
											$costo_unitario = $costo_unitario;	
										}
                                        elseif( $cantidad >= 4 && $cantidad <= 39 )
                                        {
											$rebaja = ($costo_unitario * 10)/100;
											$costo_unitario = $costo_unitario - $rebaja;
										}
                                        else
                                        {
											$rebaja = ($costo_unitario * 20)/100;
											$costo_unitario = $costo_unitario - $rebaja;	
										}
								    ?>
                                        <tr>
                                            <td style="text-align:center"> <?php echo $cantidad; ?> </td>
                                            <td> <?php echo $descripcion; ?> </td>
                                            <td id="alright"> $ <?php echo number_format($costo_unitario,2); ?> </td>
                                            <?php 
                                                if ( $id_categoria == 200 && $pago_parcial != "0" ) 
                                                {
                                                ?>
                                                    <td id="alright"> $ <?php echo number_format($pago_parcial,2); ?> </td>
                                                <?php
                                                }
                                            ?>
                                        </tr>
								    <?php
										$total_venta += $costo_unitario * $cantidad;	
                                	}

									$total_descuento = ($total_venta * $descuento)/100;
									$totalTotal = ($total_venta - $total_descuento);
								    ?>
                                <tr>
                                	<td colspan="4"> <hr /> </td>
                        		</tr>
                                <tr>
                                	<td id="alright" colspan="3">                                    
                            			<label class="textos"> Subtotal: </label>
                                    </td>
                                    <td id="alright">
										<?php
                                            if ( $id_categoria == 200 && $pago_parcial != "0" ) 
                                            {
                                                echo "$".number_format($total_venta - ($costo_unitario - $pago_parcial),2);
                                            }
                                            else
                                            {
                                                echo "$".number_format($total_venta,2);     
                                            }
                                        ?>
                            		</td>
                        		</tr>
                                <tr>
                                	<td id="alright" colspan="3">                                    	                                 	
                                        <label class="textos"> Descuento: </label>
                                    </td>
                                    <td id="alright">
										<?php echo $descuento."%"; ?>                                        
                            		</td>
                        		</tr>
                                <tr>
                                	<td id="alright" colspan="3">                                    	
                                        <label class="textos"> Total: </label>
                                    </td>
                                    <td id="alright">
                                        <?php
                                            if ( $id_categoria == 200 && $pago_parcial != "0" ) 
                                            {
                                                $totalTotal = $totalTotal - ($costo_unitario - $pago_parcial);
                                            ?>
                                                <span style="color:#F00"> <?php echo "$".number_format($totalTotal,2); ?> </span>
                                                <input name="total" id="total_venta" type="hidden" value="<?php echo $totalTotal; ?>" />
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <span style="color:#F00"> <?php echo "$".number_format($totalTotal,2); ?> </span>
                                                <input name="total" id="total_venta" type="hidden" value="<?php echo $totalTotal; ?>" />
                                            <?php
                                            }
                                        ?>
                            		</td>
                        		</tr>
                                <tr>
                                    <td id="alright" colspan="3"> <label class="textos"> Efectivo: </label> </td>
                                    <td id="alright"> <input type="text" name="cantidad" id="cantidad" size="10" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                                </tr>
                                <tr>
                                    <td id="alright" colspan="3"> <label class="textos"> Cambio: </label> </td>
                                    <td id="alright"> <input type="text" name="cambio" id="cambio" readonly="readonly" size="10" style="border:none; background-color:#7f7f7f; color:#FFF; font-weight:bold;" /> </td>
                                </tr>
                                <tr>
                                	<td style="text-align:center" colspan="4">
                                        <input name="accion" type="submit" value="Aceptar" id="btn_aceptar" title="Aceptar" class="fondo_boton" />                        
                                        <input name="accion" onclick="javascript:window.location.href='nueva_venta2.php?descuento='+<?php echo $descuento; ?>" type="button" value="Regresar" title="Regresar" class="fondo_boton" />
                            		</td>
                                </tr>
                        	</table>
                    </form>
                    </center>
                </div><!--Fin de contenido proveedor-->	
			</div><!--Fin de area contenido-->
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>