<?php
	include("config.php");
	$id_cat_nivel2 = $_GET["id_cat_nivel2"];
	$id_estilo = $_GET["id_estilo"];
	/* Consulta el Catalogo */
	$consulta_catalogo=mysql_query("SELECT id_catalogo FROM catalogo WHERE id_estilo=".$id_estilo)or die(mysql_error());
	$row_catalogo=mysql_fetch_array($consulta_catalogo);
	$id_catalogo=$row_catalogo["id_catalogo"];
	/* Consulta materiales del Estilo */
	$consulta_materiales=mysql_query("SELECT id_cat_nivel1 FROM cat_nivel2 
										WHERE id_cat_nivel2=".$id_cat_nivel2)or die(mysql_error());
	$row_materiales=mysql_fetch_array($consulta_materiales);
	$id_cat_nivel1=$row_materiales["id_cat_nivel1"];
	/* Consulta cuantas Ventilaciones del Material hay */
	$consulta_ventilaciones=mysql_query("SELECT COUNT(id_cat_nivel2) AS contador_ventilaciones FROM cat_nivel2 
										WHERE id_cat_nivel1=".$id_cat_nivel1)or die(mysql_error());
	$row_ventilaciones=mysql_fetch_array($consulta_ventilaciones);
	$numero_ventilaciones=$row_ventilaciones["contador_ventilaciones"];
	if($numero_ventilaciones <= 1){
?>
	<script type="text/javascript" language="javascript">
		alert("No se puede eliminar, porque solo hay una ventilacion cargada para el Material");
    	window.location.href = 'modificar_catalogo_moldes.php?id_estilo='+<?php echo $id_estilo; ?>
	</script>
<?php
	}else{
		/* Elimina la ventilacion */
		mysql_query("DELETE FROM cat_nivel2 WHERE id_cat_nivel2=".$id_cat_nivel2)or die(mysql_error());
		/* Consulta salidas de la Ventilacion */
		$consulta_salidas=mysql_query("SELECT id_cat_nivel3 FROM cat_nivel3
										WHERE id_cat_nivel2=".$id_cat_nivel2)or die(mysql_error());
		while($row_salidas=mysql_fetch_array($consulta_salidas)){
			$id_cat_nivel3 = $row_salidas["id_cat_nivel3"];
			/* Elimina la salida */
			mysql_query("DELETE FROM cat_nivel3 WHERE id_cat_nivel3=".$id_cat_nivel3)or die(mysql_error());
		}
?>
		<script type="text/javascript" language="javascript">
			alert("Se elimino exitosamente la Ventilacion");
            window.location.href = 'modificar_catalogo_moldes.php?id_estilo='+<?php echo $id_estilo; ?>
        </script>
<?php
	}
?>