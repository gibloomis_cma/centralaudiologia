<?php
	session_start();
	include("config.php");
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha = date("d/m/Y");
	$fecha_separada = explode("/", $fecha);
	$fecha_mysql = $fecha_separada[2]."-".$fecha_separada[1]."-".$fecha_separada[0];
	$consulta_inventario_anterior = mysql_query("SELECT COUNT(*) AS fecha
                                                 FROM inventario_baterias_anterior
												 WHERE fecha_inventario = '".$fecha_mysql."'") or die(mysql_error());
	$row_inventario_anterior = mysql_fetch_array($consulta_inventario_anterior);
	$id_registro = $row_inventario_anterior["fecha"];

	if($id_registro == 0)
	{
		/* Borra todo lo que haya en la tabla inventario baterias anterior */
		mysql_query("TRUNCATE table inventario_baterias_anterior") or die(mysql_error());
		/* Consulta la tabla inventarios */
		$consulta_inventario_actual = mysql_query("SELECT id_articulo, num_serie_cantidad, id_almacen
		                                           FROM inventarios
                                                   WHERE id_subcategoria = '55'") or die(mysql_error());

		while( $row_inventario_actual = mysql_fetch_array($consulta_inventario_actual) )
		{
			$id_articulo = $row_inventario_actual["id_articulo"];
			$cantidad = $row_inventario_actual["num_serie_cantidad"];
			$id_almacen = $row_inventario_actual["id_almacen"];

            mysql_query("INSERT INTO inventario_baterias_anterior(fecha_inventario, id_almacen, id_articulo, cantidad)
						 VALUES('".$fecha_mysql."','".$id_almacen."','".$id_articulo."','".$cantidad."')") or die(mysql_error());

            mysql_query("INSERT INTO inventario_dias_anteriores(fecha_inventario, id_almacen, id_articulo, cantidad)
						 VALUES('".$fecha_mysql."','".$id_almacen."','".$id_articulo."','".$cantidad."')") or die(mysql_error());
		}
	}

	/* Borra todo lo que haya en la tabla  temporal_inventario_baterias */
	mysql_query("TRUNCATE table temporal_inventario_baterias") or die(mysql_error());

	/* Consulta los vales que se hicieron el dia de hoy */
	$consulta_vale_baterias = mysql_query("SELECT id_vale_bateria, fecha, uso_de_bateria
										   FROM vales_baterias
										   WHERE fecha = '".$fecha_mysql."'") or die(mysql_error());

	while( $row_vale_baterias = mysql_fetch_array($consulta_vale_baterias) )
    {
		$id_vale_bateria = $row_vale_baterias["id_vale_bateria"];
		$uso_de_bateria=$row_vale_baterias["uso_de_bateria"];
		if( $uso_de_bateria == 1 )
        {
			$consulta_descripcion_vale_baterias = mysql_query("SELECT * FROM descripcion_vale_baterias
			                                                   WHERE id_vale_bateria=".$id_vale_bateria) or die(mysql_error());

            while($row_descripcion_vale_baterias = mysql_fetch_array($consulta_descripcion_vale_baterias))
            {
				$origen = $row_descripcion_vale_baterias["origen"];
				$cantidad = $row_descripcion_vale_baterias["cantidad"];
				$codigo = $row_descripcion_vale_baterias["codigo"];

				if($origen == 2)
                {
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
			                     VALUES('".$codigo."','".$cantidad."','Medico','2','".$fecha_mysql."')") or die(mysql_error());
				}
                elseif($origen == 4)
                {
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
				                 VALUES('".$codigo."','".$cantidad."','Medico','4','".$fecha_mysql."')") or die(mysql_error());
				}
			}
		}
        elseif( $uso_de_bateria == 2 )
        {
			$consulta_descripcion_vale_baterias=mysql_query("SELECT * FROM descripcion_vale_baterias
																		WHERE id_vale_bateria=".$id_vale_bateria)
																		or die(mysql_error());
			while($row_descripcion_vale_baterias=mysql_fetch_array($consulta_descripcion_vale_baterias))
            {
				$origen=$row_descripcion_vale_baterias["origen"];
				$cantidad=$row_descripcion_vale_baterias["cantidad"];
				$codigo=$row_descripcion_vale_baterias["codigo"];
				if($origen == 2){
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Laboratorio','2','".$fecha_mysql."')") or die(mysql_error());
				}elseif($origen == 4){
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Laboratorio','4','".$fecha_mysql."')") or die(mysql_error());
				}
			}
		}elseif($uso_de_bateria == 3){
			$consulta_descripcion_vale_baterias=mysql_query("SELECT * FROM descripcion_vale_baterias 
																		WHERE id_vale_bateria=".$id_vale_bateria)
																		or die(mysql_error());
			while($row_descripcion_vale_baterias=mysql_fetch_array($consulta_descripcion_vale_baterias)){
				$origen=$row_descripcion_vale_baterias["origen"];
				$cantidad=$row_descripcion_vale_baterias["cantidad"];
				$codigo=$row_descripcion_vale_baterias["codigo"];
				if($origen == 1){
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Matriz','2','".$fecha_mysql."')") or die(mysql_error());
				}
			}
		}elseif($uso_de_bateria == 4){
			$consulta_descripcion_vale_baterias=mysql_query("SELECT * FROM descripcion_vale_baterias 
																		WHERE id_vale_bateria=".$id_vale_bateria)
																		or die(mysql_error());
			while($row_descripcion_vale_baterias=mysql_fetch_array($consulta_descripcion_vale_baterias)){
				$origen=$row_descripcion_vale_baterias["origen"];
				$cantidad=$row_descripcion_vale_baterias["cantidad"];
				$codigo=$row_descripcion_vale_baterias["codigo"];
				if($origen == 1){
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Ocolusen','4','".$fecha_mysql."')") or die(mysql_error());
				}
			}
		}
	}		
	/* Consulta el departamento del empleado que entro en el sistema */
	$consulta_departamento_empleado=mysql_query("SELECT id_departamento FROM empleados 
																		WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
																		or die(mysql_error());
	$row_departamento_empleado=mysql_fetch_array($consulta_departamento_empleado);
	$id_departamento_empleado=$row_departamento_empleado["id_departamento"];
	/* Consulta la sucursal del departamento */
	$consulta_sucursal=mysql_query("SELECT id_sucursal FROM areas_departamentos 
														WHERE id_departamento=".$id_departamento_empleado)
														or die(mysql_error());
	$row_sucursal=mysql_fetch_array($consulta_sucursal);
	$id_sucursal=$row_sucursal["id_sucursal"];
	
	/* Consulta las ventas que se hicieron el dia de hoy */
	$consulta_folios_ventas=mysql_query("SELECT folio_num_venta, fecha, vendedor
											FROM ventas
											WHERE fecha='".$fecha_mysql."' AND id_sucursal=".$id_sucursal)
											or die(mysql_error());

	while($row_folios_ventas=mysql_fetch_array($consulta_folios_ventas)){
		$folio_num_venta = $row_folios_ventas["folio_num_venta"];
		$responsable = $row_folios_ventas["vendedor"];
		if($id_sucursal==1)
        {
			$venta_del_vendedor = 2;
			/* Consulta la tabla de descripcion de ventas del dia de hoy */
			$consulta_descripcion_ventas = mysql_query("SELECT * FROM descripcion_venta
																WHERE folio_num_venta=".$folio_num_venta." AND
																 id_sucursal=".$id_sucursal)or die(mysql_error());
			while($row_descripcion_venta=mysql_fetch_array($consulta_descripcion_ventas)){
				$id_categoria = $row_descripcion_venta["id_categoria"];
				$cantidad = $row_descripcion_venta["cantidad"];
				$descripcion = $row_descripcion_venta["descripcion"];
				if($id_categoria == 4)
                {
					$consulta_codigo_bateria=mysql_query("SELECT id_base_producto FROM base_productos
																					WHERE descripcion='".$descripcion."' AND
																					id_categoria=".$id_categoria)
																					or die(mysql_error());
					$row_codigo_bateria=mysql_fetch_array($consulta_codigo_bateria);
					$codigo = $row_codigo_bateria["id_base_producto"];

					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Ventas','2','".$fecha_mysql."')") or die(mysql_error());
				}
			}
		}
        else
        {
			$venta_del_vendedor = 4;
			/* Consulta la tabla de descripcion de ventas del dia de hoy */
			$consulta_descripcion_ventas = mysql_query("SELECT * FROM descripcion_venta
																WHERE folio_num_venta=".$folio_num_venta." AND 
																id_sucursal=".$id_sucursal)or die(mysql_error());
			while($row_descripcion_venta=mysql_fetch_array($consulta_descripcion_ventas))
            {
				$id_categoria = $row_descripcion_venta["id_categoria"];
				$cantidad = $row_descripcion_venta["cantidad"];
				$descripcion = $row_descripcion_venta["descripcion"];

				if( $id_categoria == 4 )
                {
					$consulta_codigo_bateria = mysql_query("SELECT id_base_producto FROM base_productos
																					WHERE descripcion='".$descripcion."' AND
																					id_categoria=".$id_categoria)
																					or die(mysql_error());
					$row_codigo_bateria=mysql_fetch_array($consulta_codigo_bateria);
					$codigo = $row_codigo_bateria["id_base_producto"];
					
					mysql_query("INSERT INTO temporal_inventario_baterias(codigo, cantidad, tipo, id_almacen, fecha)
					             VALUES('".$codigo."','".$cantidad."','Ventas','4','".$fecha_mysql."')") or die(mysql_error());

				}
			}	
		}
	}
?>