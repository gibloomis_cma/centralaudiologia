<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Salida </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Ventilaciones
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO1 -->
						<div class="area_contenido1">
							<br/>
							<?php
								include("config.php");
								$id_catalogo = $_GET['id_catalogo'];
								$id_cat_nivel1 = $_GET['id_cat_nivel1'];
								$id_cat_nivel2 = $_GET['id_cat_nivel2'];
								
								$query_estilo = mysql_query("SELECT estilo,foto
															 FROM estilos,catalogo
															 WHERE estilos.id_estilo = catalogo.id_estilo 
															 AND id_catalogo ='$id_catalogo'") or die (mysql_error());
								$row_estilo = mysql_fetch_array($query_estilo);
								$estilo = $row_estilo['estilo'];
								$imagen = $row_estilo['foto'];
								
								$query_material = mysql_query("SELECT material
															   FROM cat_nivel1,materiales
															   WHERE cat_nivel1.id_material = materiales.id_material
															   AND id_catalogo = '$id_catalogo'
															   AND id_cat_nivel1 = '$id_cat_nivel1'") or die (mysql_error());
								$row_material = mysql_fetch_array($query_material);
								$material = $row_material['material'];
								
								$query_ventilaciones = mysql_query("SELECT nombre_ventilacion,calibre
																	FROM ventilaciones,cat_nivel2
																	WHERE cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion
																	AND cat_nivel2.id_cat_nivel1 = '$id_cat_nivel1'
																	AND id_cat_nivel2 = '$id_cat_nivel2'") or die (mysql_error());
								$row_ventilacion = mysql_fetch_array($query_ventilaciones);
								$nombre_ventilacion = $row_ventilacion['nombre_ventilacion'];
								$calibre = $row_ventilacion['calibre'];
								
								$query_salidas = mysql_query("SELECT id_salida,salida
															  FROM salidas
															  ORDER BY id_salida ASC") or die (mysql_error());
							?>
							<div class="titulos"> Estilo </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td> <label class="textos"> Nombre del Estilo: </label> </td>
											<td>  &nbsp;<label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $estilo; ?> </label> </td>
											<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
											<td> <img width="100" height="100" src="../moldes/estilos/<?php echo $imagen; ?>"/> </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Material </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td colspan="2"> 
                                            	<label class="textos"> Nombre del Material: </label><?php echo $material; ?>
                                            </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Ventilación </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
                              		<table>
										<tr>
											<td colspan="2">
                                                <label class="textos"> Nombre de la Ventilación: </label><?php echo $nombre_ventilacion; ?>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label class="textos"> Calibre: </label><?php echo $calibre; ?>
											</td>
                                        </tr>
									</table>
                                </center>
								<br/>
								<div class="titulos"> Salidas </div><!-- FIN DIV TITULOS -->
								<center>
								<form name="form_salidas" id="form_salidas" method="post" action="procesa_salida_ventilacion.php" class="validar_salidas">
									<table id="tabla_salida">
										<tr>
											<td> <input type="hidden" name="txt_id_catalogo" id="txt_id_catalogo" value="<?php echo $id_catalogo; ?>"/> </td>
										</tr>
										<tr>
											<td> <input type="hidden" name="txt_id_cat_nivel1" id="txt_id_cat_nivel1" value="<?php echo $id_cat_nivel1; ?>"/> </td>
										</tr>
										<tr>
											<td> <input type="hidden" name="txt_id_cat_nivel2" id="txt_id_cat_nivel2" value="<?php echo $id_cat_nivel2; ?>"/> </td>
										</tr>
										<tr>
											<td> <label class="textos"> Nombre de Salida: </label> </td>
											<td>
												<select name="salidas_ventilacion">
													<option value="0"> --- Seleccione Salida --- </option>
													<?php
													while( $row_salida = mysql_fetch_array($query_salidas) )
													{
														$id_salida = $row_salida['id_salida'];
														$salida = $row_salida['salida'];
													?>
														<option value="<?php echo $id_salida; ?>"> <?php echo $salida; ?> </option>
													<?php
													}
													?>
												</select>
											</td>
										</tr>
									</table>
									<div id="boton_salida">
										<p align="right">
											<input type="submit" name="accion" id="accion" class="fondo_boton" value="Guardar" title="Guardar"/>											
										</p>
									</div>
									<br/><br/>
								</form>
								</center>
						</div><!-- FIN DIV AREA CONTENIDO1 -->					
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>

</html>
