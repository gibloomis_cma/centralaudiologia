<?php
	session_start();
	include("config.php");
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();
	$fecha = date("d/m/Y");
    $id_empleado_usuario_sistema = $_SESSION['id_empleado_usuario'];
	$consulta_empleado = mysql_query("SELECT nombre, id_empleado, id_departamento FROM empleados 
																WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
																or die(mysql_error());
	$row3 = mysql_fetch_array($consulta_empleado);
	$id_empleado= $row3["id_empleado"];
	$nombre = $row3["nombre"];
	$id_departamento_empleado = $row3["id_departamento"];

    // SE REALIZA QUERY QUE OBTIENE LOS EMPLEADOS
    $query_datos_empleados = "SELECT id_empleado,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_empleado
                              FROM empleados
                              ORDER BY nombre_completo_empleado ASC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
    $resultado_datos_empleados = mysql_query($query_datos_empleados) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vale de Refacciones</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script language="javascript">
<!-- Se abre el comentario para ocultar el script de navegadores antiguos

function muestraReloj()
{
// Compruebo si se puede ejecutar el script en el navegador del usuario
if (!document.layers && !document.all && !document.getElementById) return;
// Obtengo la hora actual y la divido en sus partes
var fechacompleta = new Date();
var horas = fechacompleta.getHours();
var minutos = fechacompleta.getMinutes();
var segundos = fechacompleta.getSeconds();
var mt = "AM";
// Pongo el formato 12 horas
if (horas >= 12) {
mt = "PM";
horas = horas - 12;
}
if (horas == 0) horas = 12;
// Pongo minutos y segundos con dos dígitos
if (minutos <= 9) minutos = "0" + minutos;
if (segundos <= 9) segundos = "0" + segundos;
// En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
 cadenareloj = "<input name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";
 // Escribo el reloj de una manera u otra, según el navegador del usuario


if (document.layers) {
document.layers.spanreloj.document.write(cadenareloj);
document.layers.spanreloj.document.close();
}
else if (document.all) spanreloj.innerHTML = cadenareloj;
else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
// Ejecuto la función con un intervalo de un segundo
setTimeout("muestraReloj()", 1000);
}			
// Fin del script -->
</script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
</head>
<body onLoad="muestraReloj()">
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Refacciones
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
            <br />	
                <div class="titulos">Vale de Refacciones</div>
            <br />					
                <div class="contenido_proveedor">
					<form name="forma1" id="form_vale_refacciones" method="post" action="procesa_vale_refaccion.php">                     
                        <input name="id_departamento_empleado" value="<?php echo $id_departamento_empleado; ?>" type="hidden" />
			<?php
                $vale_refaccion = $_GET["folio_vale"];
                if( $vale_refaccion == "" )
                {
            ?>
                        <input name="folio_vale" type="hidden" value="" />
                        <div id="spanreloj"></div>
                        <table style="margin-left:-30px;">
                            <tr>
                                <td id="alright"> <label class="textos"> Fecha: </label> </td>
                                <td id="alleft"> <input type="text" name="fecha" value="<?php echo $fecha; ?>" readonly="readonly" style="border:none; background-color:#7f7f7f; text-align:center; font-weight:bold; color:#FFF;" /> </td>
                                <td id="alright"> <label class="textos"> Responsable: </label> </td>
                                <td id="alleft">
                                    <select name="responsable" id="responsable">
                                        <option value="0"> Seleccione Responsable </option>
                                <?php 
                                    // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
                                    while ( $row_empleados = mysql_fetch_array($resultado_datos_empleados) ) 
                                    {
                                        $id_empleado_consultado = $row_empleados['id_empleado'];
                                        $nombre_empleado_consultado = $row_empleados['nombre_completo_empleado'];

                                        // SE VALIDA PARA SELECCIONAR EL NOMBRE DEL EMPLEADO LOGUEADO
                                        if ( $id_empleado_usuario_sistema != "" && $id_empleado_usuario_sistema == $id_empleado_consultado ) 
                                        {
                                        ?>
                                            <option value="<?php echo $id_empleado_consultado; ?>" selected="selected"> <?php echo ucwords(strtolower($nombre_empleado_consultado)); ?> </option>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <option value="<?php echo $id_empleado_consultado; ?>"> <?php echo ucwords(strtolower($nombre_empleado_consultado)); ?> </option>
                                        <?php
                                        }
                                    }
                                ?>
                                    </select>
                                    <input type="hidden" name="responsable_logueado" value="<?php echo $id_empleado; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"> <br/> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Uso: </label> </td>
                                <td id="alleft">
                                    <select name="id_uso" id="id_uso" style="width:200px;">
                                        <option value="0"> Seleccione Uso </option>
                              	<?php 
										$consulta_usos = mysql_query("SELECT * FROM uso_refacciones 
																				WHERE id_uso_refaccion <>4 
																				ORDER BY id_uso_refaccion")or die(mysql_error());
										while($row2 = mysql_fetch_array($consulta_usos)){
											$uso = $row2["uso"];
											$id_uso_refaccion = $row2["id_uso_refaccion"];
                                ?>
                                        <option value="<?php echo $id_uso_refaccion; ?>"><?php echo $uso; ?></option>
                           		<?php
										}
                                ?> 
                                    </select>
                                </td>
                                <td id="alright"> <label class="textos"> Origen: </label> </td>
                                <td id="alleft">
                                    <select name="id_origen" id="id_origen" style="width:200px;">
                                        <option value="0" >Seleccione Origen </option>
                              	<?php 
										$consulta_origen2 = mysql_query("SELECT * FROM almacenes")
																					or die(mysql_error());
										while($row3 = mysql_fetch_array($consulta_origen2)){
											$id_almacen = $row3["id_almacen"];
											$almacen = $row3["almacen"];
                               	?>
                                        <option value="<?php echo $id_almacen; ?>">
                                            <?php echo $almacen; ?>
                                        </option>
                               	<?php
                                        }
                                ?> 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"> <br/> </td>
                            </tr>
                            <tr>
                                <td id="alright" style="vertical-align:top;"> <label class="textos"> Observaciones: </label> </td>
                                <td id="alleft" colspan="3"> <textarea name="observaciones" id="observaciones" cols="40" rows="3" style="resize:none;">-Ninguna-</textarea> </td>
                            </tr>
                            <tr>
                                <td id="alright"> <label class="textos"> Refacción: </label> </td>
                                <td id="alleft" colspan="2">
                                    <select name="subcategoria" id="tipo_subcategoria" style="width:200px;">
                                        <option value="0" >Seleccione</option>
                           		<?php 
										$consulta_refacciones = mysql_query("SELECT * FROM subcategorias_productos 
																						WHERE id_categoria= '1'")
																						or die(mysql_error());
										while($row = mysql_fetch_array($consulta_refacciones)){
											$nombre_subcategoria = $row["subcategoria"];
											$id_subcategoria = $row["id_subcategoria"];
                               	?>
                                        <option value="<?php echo $id_subcategoria; ?>">
											<?php echo $nombre_subcategoria; ?>
                                        </option>
                               	<?php
                                      	}
                               	?> 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td id="alright">
                                    <label class="textos">Descripción: </label>
                                </td><td id="alleft" colspan="2">                                    
                                    <select name="descripcion" id="id_articulo" style="width:200px;" disabled="disabled">
                                        <option value="0" >Seleccione</option>
                                    </select>
                                </td>
                            </tr><tr>
                                <td id="alright">
                                    <label class="textos">Cantidad: </label>
                                </td><td id="alleft" colspan="2">
                                    <select name="cantidad" id="cantidad" style="width:200px;" disabled="disabled">
                                        <option value="0">Seleccione</option>
                                    </select>
                                </td>
                           </tr><tr>                                   
                                <td id="alright" colspan="4">
                                    <input name="accion" value="Agregar" type="submit" class="fondo_boton btn_agregar" />
                                </td>
                           </tr>
                        </table>
            <?php 
                }else{	 /* Cuando se haya guadado un producto se mostrara la siguiente informacion */
                    
                    $consulta_datos_vale = mysql_query("SELECT observaciones, uso, responsable, fecha
                                                        FROM vales_refacciones 
                                                        WHERE id_registro = ".$vale_refaccion) or die(mysql_error());
                    $row4 = mysql_fetch_array($consulta_datos_vale);
                    $observaciones = $row4["observaciones"];
                    $uso = $row4["uso"];
                    $responsable = $row4["responsable"];
                    $fecha_vale = $row4["fecha"];
                    $fecha_vale_separada = explode("-", $fecha_vale);
                    $fecha_normal = $fecha_vale_separada[2]."/".$fecha_vale_separada[1]."/".$fecha_vale_separada[0];
                    
                    $consulta_responsable = mysql_query("SELECT nombre, paterno, materno 
                                                         FROM empleados 
                                                         WHERE id_empleado = ".$responsable) or die(mysql_error());
                    $row7 = mysql_fetch_array($consulta_responsable);
                    $nombre = $row7["nombre"];
                    $paterno = $row7["paterno"];
                    $materno = $row7["materno"];
                    
                    $consulta_uso_vale = mysql_query("SELECT uso FROM uso_refacciones
                                                        WHERE id_uso_refaccion=".$uso)or die(mysql_error());
                    $row8 = mysql_fetch_array($consulta_uso_vale);
                    $uso_vale = $row8["uso"];							
            ?>
                    <table id="tabla_datos_vale">
                        <tr>
                            <td width="80" style="text-align:right;"> <label class="textos"> N° de Vale: </label> </td>
                            <td> <input type="text" name="folio_vale" value="<?php echo $vale_refaccion; ?>" size="4" maxlength="4" readonly="readonly" style="border:none; font-weight:bold; text-align:center; background-color:#7f7f7f; color:#FFF;" /> </td>
                            <td style="text-align:right;"> <label class="textos"> Fecha: </label> </td>
                            <td> <input type="text" name="fecha_normal" value="<?php echo $fecha_normal; ?>" size="12" maxlength="12" readonly="readonly" style="border:none; font-weight:bold; text-align:center; background-color:#7f7f7f; color:#FFF;" /> </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> <label class="textos"> Responsable: </label> </td>
                            <td> <input type="text" name="responsable" value="<?php echo $nombre." ".$paterno; ?>" readonly="readonly" style="border:none; font-weight:bold; text-align:center; background-color:#7f7f7f; color:#FFF;" /> </td>
                            <td style="text-align:right;"> <label class="textos"> Uso: </label> </td>
                            <td> <input type="text" name="uso" value="<?php echo $uso_vale; ?>" readonly="readonly" style="border:none; font-weight:bold; text-align:center; background-color:#7f7f7f; color:#FFF;" /> </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> <label class="textos"> Observaciones: </label> </td>
                            <td colspan="3"> <?php echo $observaciones; ?> </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> <label class="textos"> Origen: </label> </td>
                            <td>
                                <select name="id_origen" id="id_origen">
                                    <option value="0" > Seleccione Origen </option>
                                <?php 
                                    $consulta_origen2 = mysql_query("SELECT * FROM almacenes");
                                    while($row3 = mysql_fetch_array($consulta_origen2))
                                    {
                                        $id_almacen = $row3["id_almacen"];
                                        $almacen = $row3["almacen"];
                                ?>
                                        <option value="<?php echo $id_almacen; ?>"> <?php echo $almacen; ?> </option>
                                <?php
                                    }
                                ?> 
                                </select>
                            </td>
                            <td style="text-align:right;"> <label class="textos"> Refacción: </label> </td>
                            <td>
                                <select name="subcategoria" id="tipo_subcategoria">
                                    <option value="0" > Seleccione Refacción </option>
                            <?php 
                                $consulta_refacciones = mysql_query("SELECT * FROM subcategorias_productos 
                                                                    WHERE id_categoria= '1'") 
                                                                    or die(mysql_error());
                                while($row = mysql_fetch_array($consulta_refacciones))
                                {
                                    $nombre_subcategoria = $row["subcategoria"];
                                    $id_subcategoria = $row["id_subcategoria"];
                            ?>
                                    <option value="<?php echo $id_subcategoria; ?>"> <?php  echo $nombre_subcategoria; ?> </option>
                            <?php
                                }
                            ?> 
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> <label class="textos"> Descripción: </label> </td>
                            <td colspan="3">
                                <select name="descripcion" id="id_articulo">
                                    <option value="0" > Seleccione Descripción</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> <label class="textos"> Cantidad: </label> </td>
                            <td colspan="3">
                                <select name="cantidad" id="cantidad">
                                    <option value="0"> Seleccione Cantidad </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"> <input type="submit" name="accion" value="Agregar" title="Agregar a Vale de Refacciones" class="fondo_boton btn_agregar" /> </td>
                        </tr>
                    </table>
                    <input type="hidden" name="id_uso" value="<?php echo $uso; ?>" />

                    <table align="center"> 
                        <tr>
                            <th width="100">Sucursal</th>
                            <th width="150">Origen</th>
                            <th width="300">Descripcion</th>
                            <th width="80">Cantidad</th>
                            <th></th>
                        </tr>
<?php 
                    $consulta_descripcion_vale = mysql_query("SELECT codigo, cantidad, origen, id_registro
                                                                    FROM descripcion_vale_refacciones
                                                                    WHERE id_vale_refaccion="
                                                                    .$vale_refaccion) or die(mysql_error());
                    while($row5 = mysql_fetch_array($consulta_descripcion_vale)){
                        $id_registro_vale_refaccion = $row5["id_registro"];
                        $codigo = $row5["codigo"];
                        $cantidad = $row5["cantidad"];
                        $origen = $row5["origen"];
                        $consulta_descripcion_refaccion = mysql_query("SELECT descripcion 
                                                                        FROM base_productos
                                                                        WHERE id_base_producto="
                                                                        .$codigo) or die(mysql_error());
                        $row6 = mysql_fetch_array($consulta_descripcion_refaccion);
                        $descripcion = $row6["descripcion"];
                        
                        $consulta_nombre_almacen = mysql_query("SELECT almacen, id_sucursal
                                                                FROM almacenes 
                                                                WHERE id_almacen=".$origen) 
                                                                or die(mysql_error());
                        $row10 = mysql_fetch_array($consulta_nombre_almacen);
                        $descripcion_almacen = $row10["almacen"];
                        $id_sucursal = $row10["id_sucursal"];
                        /* Consulta la sucursal de origen de la bateria */
                        $consulta_origen_sucursal = mysql_query("SELECT nombre FROM sucursales
                                                                                    WHERE id_sucursal=".$id_sucursal)
                                                                                    or die(mysql_error());
                        $row_origen_sucursal=mysql_fetch_array($consulta_origen_sucursal);
                        $sucursal_origen = $row_origen_sucursal["nombre"];
?>
                        <tr>
                            <td><?php echo $sucursal_origen; ?></td>
                            <td><?php echo $descripcion_almacen; ?></td>
                            <td><?php echo $descripcion; ?></td>
                            <td><?php echo $cantidad; ?></td>
                            <td><a href="eliminar_producto_refaccion.php?id_registro_descripcion_vale_refaccion=<?php echo $id_registro_vale_refaccion; ?>">
                                    <img src="../img/delete.png">
                                </a></td>
                        </tr>
<?php 
                    }
?>
                    </table>
                    <p align="right">
                        <input name="accion" type="submit" value="Cancelar" class="fondo_boton"/>
                    &nbsp;&nbsp;&nbsp;
                        <input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                    </p> 
<?php		
                }
?> 
                <input name="fecha" value="<?php echo $fecha; ?>" type="hidden" style="width:300px;" />
                    <div id="spanreloj"></div>
                    <!--<p align="right">
                        <input name="accion" type="submit" value="Cancelar" class="fondo_boton"/>
                    &nbsp;&nbsp;&nbsp;
                        <input name="accion" type="submit" value="Guardar" class="fondo_boton"/>
                    </p>-->
                  </form> 
                </div><!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>