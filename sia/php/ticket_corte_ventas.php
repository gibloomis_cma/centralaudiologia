<?php
	error_reporting(0);
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	$folio_corte = $_GET["folio_corte_venta"];
	$fecha_corte = $_GET["fecha_corte"];
	$fecha_corte_separada = explode("/", $fecha_corte);
	$fecha_corte_mysql= $fecha_corte_separada[2]."-".$fecha_corte_separada[1]."-".$fecha_corte_separada[0];
	$empleado_corte = $_GET["empleado_corte"];
	$sucursal = $_GET["sucursal"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title> Corte de Ventas </title>
				<style>
					body
					{
						font-size:14px;	
						font:"Courier New";
					}

					#cont
					{
						margin:0px auto;
						text-align:center;		
						padding-bottom:5px;
					}

					#impresion
					{
						text-align:center;
						width:220px;
						font-size:14px;	
						font:"Courier New";
					}

					#impresion table
					{
						margin:0px auto;
						font:"Courier New";
						font-size:12px;	
						font-size:10px;
					}

					#impresion table tr th
					{
						font-size:12px;	
						font:"Courier New";
					}

					#impresion table tr td
					{
						text-align:right;
						padding:5px 5px;
						font-size:12px;	
						font:"Courier New";
					}

					span
					{
						text-decoration:underline;
						font-size:14px;
						font:"Courier New";	
					}
				</style>
			</head>
			<body>
				<?php
					// SE REALIZA EL QUERY QUE OBTIENE LOS DATOS DE LA SUCURSAL DE ACUERDO AL ID DE LA SUCURSAL OBTENIDO
					$query_sucursal = "SELECT nombre
									   FROM sucursales
									   WHERE id_sucursal = '$sucursal'";

					// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIBLE
					$resultado_sucursal = mysql_query($query_sucursal) or die(mysql_error());

					// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO
					$row_sucursal = mysql_fetch_array($resultado_sucursal);

					$nombre_sucursal = $row_sucursal['nombre'];

					// SE REALIZA EL QUERY QUE OBTIENE EL NOMBRE DEL EMPLEADO QUE HIZO LA VENTA
					$query_nombre_empleado = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
											  FROM empleados
											  WHERE id_empleado = '$empleado_corte'";

					// SE EJECUTA EL QUERY Y EL RESULTADO SE ALMACENA EN UNA VARIABLE
					$resultado_nombre_empleado = mysql_query($query_nombre_empleado) or die(mysql_error());

					$row_nombre_empleado = mysql_fetch_array($resultado_nombre_empleado);
					$nombre_empleado = $row_nombre_empleado['nombre_empleado'];
				?>
			<div id="cont">
				<div id="impresion">
					CORTE DIARIO DE VENTAS <br />
					Sucursal <?php echo ucwords(strtolower($nombre_sucursal)); ?> <br/>
					N° Folio de Corte: <?php echo $folio_corte; ?> <br/>
					<?php include('fecha.php'); ?> <br /><br/> 
					<table>
						<tr>
							<td style="text-align:center;"> Cantidad de Articulos </td>
							<td style="text-align:center;"> Total </td>
						</tr>
						<?php
							// SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LAS VENTAS REALIZADAS
							$consulta_ventas_diaria = "SELECT folio_num_venta, descuento, total, vendedor 
													   FROM ventas 
													   WHERE fecha = '$fecha_corte_mysql' 
													   AND id_sucursal = '$sucursal'";

							// SE EJECUTA EL QUERY Y SE ALMACENA EN UNA VARIABLE EL RESULTADO
							$resultado_ventas_diaria = mysql_query($consulta_ventas_diaria) or die(mysql_error()); 

							// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
							while( $row = mysql_fetch_array($resultado_ventas_diaria) ) 
							{
								$vendedor = $row["vendedor"];

								// SE REALIZA QUERY QUE OBTIENE EL DEPARTAMENTO DEL VENDEDOR
								$consulta_departamento_vendedor = "SELECT id_departamento 
																   FROM empleados 
																   WHERE id_empleado = '$vendedor'";

								// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
								$resultado_departamento_vendedor = mysql_query($consulta_departamento_vendedor) or die(mysql_error());

								// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
								$row_departamento_vendedor = mysql_fetch_array($resultado_departamento_vendedor);

								// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
								$id_departamento_vendedor = $row_departamento_vendedor["id_departamento"];
								
								// SE REALIZA QUERY QUE OBTIENE LA SUCURSAL DE ACUERDO AL DEPARTAMENTO
								$consulta_sucursal = "SELECT id_sucursal 
													  FROM areas_departamentos
													  WHERE id_departamento = '$id_departamento_vendedor'";

								// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
								$resultado_sucursal = mysql_query($consulta_sucursal) or die(mysql_error());

								// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
								$row_sucursal = mysql_fetch_array($resultado_sucursal);

								// EL RESULTADO FINAL SE ALMACENA EN UNA VARIABLE
								$id_sucursal_venta = $row_sucursal["id_sucursal"];

								// SE VALIDA SI EL ID DE LA SUCURSAL ES IGUAL A 1
								if( $id_sucursal_venta == 1 )
								{
									$folio_num_venta = $row["folio_num_venta"];
									$descuento = $row["descuento"];
									$total = $row["total"];
									$totalTotal += $total;

									$consulta_ventas_diaria2 = "SELECT id_registro, cantidad, costo_unitario, id_categoria
																FROM descripcion_venta 
																WHERE folio_num_venta = '$folio_num_venta' AND id_sucursal = '$sucursal'";

									$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

									while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
									{
										$categoria = $row2["id_categoria"];
										$id_registro = $row2["id_registro"];
										$costo_unitario = $row2["costo_unitario"];
										$cantidad = $row2["cantidad"];
										$sumaTotal = ($cantidad * $costo_unitario);
										$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
										$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;

										// SE VALIDA A QUE CATEGORIA PERTENECE EL PRODUCTO
										if($categoria == 1)
										{
											$sumaTotal1 += $sumaTotalNueva;
											$cantidad1 += $cantidad;
										}
										if($categoria == 2)
										{
											$sumaTotal2 += $sumaTotalNueva;
											$cantidad2 += $cantidad;	
										}
										if($categoria == 4)
										{
											$sumaTotal3 += $sumaTotalNueva;
											$cantidad3 += $cantidad;
										}
										if($categoria == 100)
										{
											$sumaTotal4 += $sumaTotalNueva;
											$cantidad4 += $cantidad;	
										}
										if($categoria == 200)
										{
											$sumaTotal5 += $sumaTotalNueva;
											$cantidad5 += $cantidad;	
										}
									}
								}
								// SE VALIDA SI EL ID SUCURSAL ES DIFERENTE A 1
								else
								{
									$folio_num_venta = $row["folio_num_venta"];
									$descuento = $row["descuento"];
									$total = $row["total"];
									$totalTotal += $total;
									$consulta_ventas_diaria2 = "SELECT cantidad, costo_unitario, id_categoria
																FROM descripcion_venta 
																WHERE folio_num_venta = '$folio_num_venta' 
																AND id_sucursal = '$sucursal'";

									$resultado_ventas_diaria2 = mysql_query($consulta_ventas_diaria2) or die(mysql_error());

									while( $row2 = mysql_fetch_array($resultado_ventas_diaria2) )
									{
										$categoria = $row2["id_categoria"];
										$id_registro = $row2["id_registro"];
										$costo_unitario = $row2["costo_unitario"];
										$cantidad = $row2["cantidad"];
										$sumaTotal = ($cantidad * $costo_unitario);
										$sumaTotalDescuento = (($sumaTotal * $descuento)/100);
										$sumaTotalNueva = $sumaTotal - $sumaTotalDescuento;
										if( $categoria == 1 )
										{
											$sumaTotal1 += $sumaTotalNueva;
											$cantidad1 += $cantidad;
										}
										if( $categoria == 2 )
										{
											$sumaTotal2 += $sumaTotalNueva;
											$cantidad2 += $cantidad;	
										}
										if( $categoria == 4 )
										{
											$sumaTotal3 += $sumaTotalNueva;
											$cantidad3 += $cantidad;
										}
										if( $categoria == 100 )
										{
											$sumaTotal4 += $sumaTotalNueva;
											$cantidad4 += $cantidad;	
										}
										if( $categoria == 200 )
										{
											$sumaTotal5 += $sumaTotalNueva;
											$cantidad5 += $cantidad;	
										}
									} 
								}
							}
							?>
                    	<tr>
                    		<td colspan="2" style="text-align:left;"> Refacciones </td>
                    	</tr>
						<tr> 
    						<td style="text-align:center;"> <?php echo $cantidad1; ?> </td>
    						<td style="text-align:center;"> <?php echo "$ ".number_format($sumaTotal1,2); ?> </td>
    					</tr>
    					<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
    					<tr>
    						<td colspan="2" style="text-align:left;"> Baterias </td>
    					</tr>
    					<tr>
    						<td style="text-align:center;"> <?php echo $cantidad3; ?> </td>
    						<td style="text-align:center;"> <?php echo "$ ".number_format($sumaTotal3,2); ?> </td>
    					</tr>
    					<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left;"> Consultas </td>
						</tr>
						<tr>
							<td style="text-align:center;"> <?php echo $cantidad2; ?> </td>
							<td style="text-align:center;"> <?php echo "$ ".number_format($sumaTotal2,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left;"> Reparaciones </td>
						</tr>
						<tr>
							<td style="text-align:center;"> <?php echo $cantidad4; ?> </td>
							<td style="text-align:center;"> <?php echo "$ ".number_format($sumaTotal4,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:left;"> Moldes </td>
						</tr>
						<tr>
							<td style="text-align:center;"> <?php echo $cantidad5; ?> </td>
							<td style="text-align:center;"> <?php echo "$ ".number_format($sumaTotal5,2); ?> </td>
						</tr>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<?php
								$total_de_articulos = 0;
								$total_de_articulos = $cantidad1 + $cantidad3 + $cantidad2 + $cantidad4 + $cantidad5;
								$total_dinero = $sumaTotal1 + $sumaTotal3 + $sumaTotal2 + $sumaTotal4 + $sumaTotal5;
							?>
							<td style="text-align:left;"> Total de Articulos: <?php echo $total_de_articulos; ?> </td>
        					<td style="text-align:left;"> Total: <?php echo "$ ".number_format($total_dinero,2); ?>  </td>
    					</tr>
					</table>
					<br/><br/><br/>
					<table>
						<tr>
							<td colspan="2"> ------------------------------------------------- </td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center;"> Firma de encargado(a) </td>
						</tr>
					</table>
				</div>
				<p>&nbsp;&nbsp;</p>
			</div>
		</body>
	</html>