<?php
	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE RECIBE LA VARIABLE DEL FORMULARIO DE ACUERDO A LO SELECCIONADO EN EL SELECT DE LA DESCRIPCION
	$folio_num_molde = $_POST['num_molde'];

	// SE REALIZA EL QUERY PARA OBTENER EL NOMBRE DEL CLIENTE DE ACUEROD AL FOLIO DEL MOLDE
	$query_nombre_cliente = "SELECT CONCAT(nombre,' ',paterno,' ',materno,' ') AS nombre_completo
							 FROM moldes
							 WHERE folio_num_molde = '$folio_num_molde'";

	// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO EN UNA VARIABLE
	$resultado_nombre_cliente = mysql_query($query_nombre_cliente) or die(mysql_error());

	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
	$row_nombre_cliente = mysql_fetch_array($resultado_nombre_cliente);

	// SE ALMACENA EL NOMBRE DEL CLIENTE EN UNA VARIABLE
	$nombre_cliente = $row_nombre_cliente['nombre_completo'];

	// SE IMPRIME EL NOMBRE DEL CLIENTE
	echo ucwords(strtolower($nombre_cliente));
?>