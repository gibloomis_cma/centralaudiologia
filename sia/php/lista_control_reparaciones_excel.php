<?php
    // SE RECIBE LA HORA POR METODO POST
    $hora = $_POST['hora_reparaciones'];

    // SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=lista_control_reparaciones.xls");

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_reparaciones = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo,fecha_entrada,id_estatus_reparaciones,folio_num_reparacion,num_serie,id_modelo, reparacion, adaptacion, venta, aplica_garantia
                		   FROM reparaciones
                		   WHERE id_estatus_reparaciones <> 3
                		   ORDER BY fecha_entrada DESC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_query_reparaciones = mysql_query($query_reparaciones) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> </title>
	<!--<link type="text/css" rel="stylesheet" href="../css/style3.css" />-->
	<!--<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>-->
</head>
<body>
	<div id="wrapp">       
		<div id="contenido_columna2">
    		<div class="contenido_pagina">
				<div class="fondo_titulo1">
        		</div><!--Fin de fondo titulo-->
        		<div class="area_contenido1">
            		<div class="contenido_proveedor">
               			<!--<div class="titulos"> Lista de Control de Reparaciones </div>-->
            		<table>
                        <tr>
                            <td colspan="7" style="font-size:14px; text-align:center; background-color:#7f7f7f; color:#FFF; font-weight:bold; height:20px;"> Lista de Control de Reparaciones <?php echo date('d/m/Y')."  ".$hora; ?> </td>
                        </tr>
                    </table>
                    <table border="1">
            			<tr>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> N° Nota </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> Nombre Completo </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> Marca </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> Modelo </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> N° de Serie </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> Precio de Reparaci&oacute;n </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:50px;"> Estatus </th>
            			</tr>
            		<?php
            			// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            			while ( $row_reparacion = mysql_fetch_array($resultado_query_reparaciones) )
            			{
            				$nombre_completo = $row_reparacion['nombre_completo'];
            				$num_serie = $row_reparacion["num_serie"];
            				$id_modelo = $row_reparacion["id_modelo"];
            				$estado_reparacion = $row_reparacion["id_estatus_reparaciones"];
            				$garantia_reparacion = $row_reparacion['reparacion'];
            				$garantia_adaptacion = $row_reparacion['adaptacion'];
            				$garantia_venta = $row_reparacion['venta'];
            				$aplica_garantia = $row_reparacion['aplica_garantia'];
            				$fecha_entrada = $row_reparacion["fecha_entrada"];
            				$fecha_entrada_separada = explode("-", $fecha_entrada);
            				$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
            				$folio_num_reparacion = $row_reparacion["folio_num_reparacion"];

            				// SE REALIZA QUERY QUE OBTIENE EL ESTATUS DE LA REPARACION
            				$consulta_estatus_nota_reparacion = mysql_query("SELECT * 
            																 FROM estatus_reparaciones
                                                             				 WHERE id_estatus_reparacion = '$estado_reparacion'") or die(mysql_error());    
                    
                    		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
            				$row_estatus_nota_reparacion = mysql_fetch_array($consulta_estatus_nota_reparacion);
            				$estatus_reparacion = $row_estatus_nota_reparacion["estado_reparacion"];

            				// SE REALIZA QUERY QUE OBTIENE LOS DATOS DEL APARATO
            				$consulta_descripcion_modelo = mysql_query("SELECT descripcion, subcategoria 
                                                        				FROM base_productos_2,subcategorias_productos_2
                                                        				WHERE id_base_producto2 = '$id_modelo' 
                                                        				AND base_productos_2.id_subcategoria = subcategorias_productos_2.id_subcategoria") or die(mysql_error());
                    		
                    		// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
            				$row_descripcion_modelo = mysql_fetch_array($consulta_descripcion_modelo);
            				$subcategoria_consultada = $row_descripcion_modelo["subcategoria"];
            				$descripcion_modelo = $row_descripcion_modelo["descripcion"];
                    
                    		// SE REALIZA QUERY QUE OBTIENE EL PRESUPUESTO DE LA REPARACION
            				$consulta_presupuesto = mysql_query("SELECT presupuesto 
                                                 				 FROM reparaciones_laboratorio 
                                                 				 WHERE folio_num_reparacion = '$folio_num_reparacion'") or die(mysql_error());
                    
            				$row_presupuesto = mysql_fetch_array($consulta_presupuesto);
            				$presupuesto = $row_presupuesto["presupuesto"];                                     
    				?>
            			<tr>
                			<td style="text-align:center; font-size:12px;"> <?php echo $folio_num_reparacion; ?> </td>
                			<td style="text-align:center; font-size:12px;"> <?php echo $nombre_completo; ?> </td>
                			<td style="text-align:center; font-size:12px;"> <?php echo $subcategoria_consultada; ?> </td>
                			<td style="text-align:center; font-size:12px;"> <?php echo $descripcion_modelo; ?> </td>
                			<td style="text-align:center; font-size:12px;"> <?php echo $num_serie; ?> </td>
                			<td style="text-align:center; font-size:12px;">
                    		<?php
                        		if ( $garantia_venta == "si" || $aplica_garantia == "si" )
                        		{
                            		echo "$ 0.00";
                        		}
                        		else
                        		{
                            		echo "$ ".number_format($presupuesto,2);
                        		}
                    		?>
                			</td>
                			<td style="text-align:center; font-size:12px;"> <?php echo $estatus_reparacion; ?> </td>
            			</tr>
            		<?php
            			}
            		?>
            		</table>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>