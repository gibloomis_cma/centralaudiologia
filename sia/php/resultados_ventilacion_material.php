<?php
	include('config.php');
	
	$query_ventilaciones = mysql_query("SELECT id_ventilacion,nombre_ventilacion,calibre
										FROM ventilaciones ORDER BY id_ventilacion ASC") or die (mysql_error());
?>
	<tr>
		<td> <label class="textos"> Nombre de la Ventilación: </label> </td>
		<td>
			<select name="ventilacion_material[]">
				<option value="0"> --- Seleccione Ventilacion --- </option>
				<?php
				while( $row_ventilacion = mysql_fetch_array($query_ventilaciones) )
				{
					$id_ventilacion = $row_ventilacion['id_ventilacion'];
					$nombre_ventilacion = $row_ventilacion['nombre_ventilacion'];
					$calibre = $row_ventilacion['calibre'];
				?>
					<option value="<?php echo $id_ventilacion ?>"> <?php echo utf8_encode(ucwords(strtolower($nombre_ventilacion))); ?> | <?php echo $calibre ?> </option>
				<?php
				}
				?>
			</select>
		</td>
	</tr>