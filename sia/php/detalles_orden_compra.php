<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8/iso-8859-1" />
	<title>Detalles de la Orden de Compra</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<!-- orden_de_compra.php -->
	<form name="forma1" id="forma1" method="post" action="orden_de_compra.php">
		<div id="contenido_columna2">
			<div class="contenido_pagina">    
				<div class="fondo_titulo1">        
					<div class="categoria">
						Proveedores
					</div><!-- Fin DIV categoria -->           
				</div><!-- Fin de fondo titulo1 -->
					<div class="area_contenido1">
						<br />
							<div class="contenido">								
							<?php
                            // SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS
                                include("config.php");
                            // SE GUARDA EN UNA VARIABLE EL ID DEL PROVEEDOR 
                                $id_proveedor = $_GET['id_proveedor'];
                            // SE REALIZA EL QUERY QUE SELECCIONA EL NOMBRE DEL PROVEEDOR DE ACUERDO AL ID SELECCIONADO
                                $query_nombre_proveedor = mysql_query("SELECT razon_social 
                                                                        FROM proveedores 
                                                                        WHERE id_proveedor = ".$id_proveedor) 
                                                                        or die (mysql_error());
                                $row = mysql_fetch_array($query_nombre_proveedor);
                                $nombre_proveedor =$row['razon_social'];                            
                            ?>
							</div> <!-- FIN DIV CLASS CONTENIDO -->
							<center>	
                            	<style type="text/css">
									table tr td label{
										font-weight:bold !important;
										color:#003 !important;
									}
									table tr td{
										color:#666 !important;
									}
									table{
										/*border-bottom:1px solid #666;*/
										margin-bottom:10px;
									}
                                </style>							
                                <table>
                                    <tr>
                                        <th colspan="6">
                                            Detalles de la Orden de Compra
                                        </th>
                                    </tr><tr>
                                        <td style="text-align:right">
                                        	<label>Folio: </label>
										</td><td style="text-align:left">
											<?php echo $_GET['folio'] ?>
                                        </td>
                                        <?php
											$a=0;
											$des_orden=mysql_query('SELECT * FROM ordenes_de_compra, estatus_ordenes 
																			WHERE ordenes_de_compra.id_estatus = estatus_ordenes.id_estatus_orden
																			AND folio_orden_compra='.$_GET['folio'])or die(mysql_error());
											$row_des_orden=mysql_fetch_array($des_orden);
												$fecha_orden = $row_des_orden['fecha_orden'];
												$estatus = $row_des_orden['estatus_orden'];
												$articulos[$a]=$row_des_orden['id_producto'];
												$a++;
										?>
                                        <td style="text-align:right">
                                        	<label>Fecha de Orden: </label>
										</td><td style="text-align:left">
											<?php echo $row_des_orden['fecha_orden']; ?>
                                        </td><td style="text-align:right">                                     
	                                        <label>Fecha de Entrega: </label>
                                        </td><td style="text-align:left">
											<?php echo $row_des_orden['fecha_entrega']; ?>
                                        </td>
                                    </tr><tr>                                     	
                                        <td style="text-align:right">                              
                                        	<label>Proveedor:</label>
                                        </td><td style="text-align:left" colspan="3">
                                        	<?php echo $nombre_proveedor; ?>
                                        </td><td style="text-align:right">
                                        	<label>Estatus: </label>
										</td><td style="text-align:left">
											<?php echo $row_des_orden['estatus_orden']; ?>
                                        </td> 
                                    </tr><tr>
                                        <th colspan="6">
                                            Articulos
                                        </th>
                                    </tr><tr>
                                        <td>
                                            <label>Categoria</label>
                                        </td><td colspan="2">
                                            <label>Articulo</label>
                                        </td><td style="text-align:center">
                                            <label>Cantidad</label>
                                        </td><td style="text-align:right">
                                            <label>Precio Unitario</label>
                                        </td><td style="text-align:right">
                                            <label>Subtotal</label>
                                        </td>
                                    </tr>                                    
                                    <?php			
										//CONSULTA DE SUBTOTALES ORDEN DE COMPRA						
										$ordenes_compra = mysql_query("SELECT base_productos.id_categoria, cantidad,
																		precio_unitario,referencia_codigo, descripcion, categoria																		
																		FROM ordenes_de_compra,categorias_productos,base_productos
																		WHERE base_productos.id_base_producto=ordenes_de_compra.id_producto 
																		AND base_productos.id_categoria=ordenes_de_compra.id_categoria_producto 
																		AND base_productos.id_categoria=categorias_productos.id_categoria 
																		AND categorias_productos.id_categoria=ordenes_de_compra.id_categoria_producto
																		AND folio_orden_compra=".$_GET['folio']) 
																		or die (mysql_error());
										while($row_ordenes_de_compra=mysql_fetch_array($ordenes_compra)){
											$cantidad=$row_ordenes_de_compra['cantidad'];
											$precio_unitario=$row_ordenes_de_compra['precio_unitario'];											
											$id_categoria=$row_ordenes_de_compra['id_categoria'];											
											$categoria=$row_ordenes_de_compra['categoria'];
											$referencia_codigo=$row_ordenes_de_compra['referencia_codigo'];
											$descripcion=$row_ordenes_de_compra['descripcion'];
											if($id_categoria==1){
												$imprime=$referencia_codigo;
											}else{
												$imprime=$descripcion;
											}
											
											
									?>
                                    <tr>
                                        <td>
                                        	<?php echo $categoria; ?>
                                        </td><td colspan="2">
                                        	<?php echo $imprime; ?>                                          
                                        </td><td style="text-align:center;">
                                            <?php echo $cantidad; ?>
                                        </td><td style="text-align:right">
                                            <?php echo "$".number_format($precio_unitario,2); ?>
                                        </td><td style="text-align:right">
                                            <?php echo "$".number_format($precio_unitario*$cantidad,2); ?>
                                        </td>
                                    </tr>
                                    <?php 
										}
									?>                                    
                                </table>									
                                <p align="center">
                                    <input type="hidden" name="proveedor" value="<?php echo $id_proveedor; ?>" />
                                    <input type="button" name="volver" value="Volver" class="fondo_boton"
                                    onclick="window.location.href='ordenes_de_compra.php?id_proveedor=<?php echo $id_proveedor; ?>'"  />
                                    <?php
										if($row_des_orden['estatus_orden']=="No Enviada"){
									?>
                                    <input type="button" name="editar" value="Editar" class="fondo_boton" 
                                    onclick="window.location.href='orden_de_compra.php?id_proveedor=<?php echo $id_proveedor."&folio=".$_GET['folio']; ?>'" />
                                    <?php
										}
									?>
                                </p>
							</center>
					</div><!--Fin de area contenido-->
			</div><!--Fin de contenido pagina-->
		</div><!--Fin de contenido columna2-->
	</form>
</body>
</html>