<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ver Anamnesis</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<style>
li{
	font-size:14px;
	color:#6d6d6d; 
	font-weight:bold;
	list-style:none;	
}
</style>
</head>
<body>
<div id="contenido_columna2">    
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Anamnesis
            </div>               
        </div><!--Fin de fondo titulo-->           
        <div class="area_contenido1">
		<?php
            include("config.php");
            include("metodo_cambiar_fecha.php"); 
            $id_cliente = $_GET["id_paciente"];
            $consulta_datos_paciente = mysql_query("SELECT * FROM ficha_identificacion
                                                            WHERE id_cliente=".$id_cliente) 
                                                            or die(mysql_error());
            $row = mysql_fetch_array($consulta_datos_paciente);
            $nombre = $row["nombre"];
            $paterno = $row["paterno"];
            $materno = $row["materno"];
            $calle = $row["calle"];
            $num_exterior = $row["num_exterior"];			
            $num_interior = $row["num_interior"];
			if($num_interior!=""){
				$num_interior="-".$num_interior;
			}
            $codigo_postal = $row["codigo_postal"];
            $colonia = $row["colonia"];
            $edad = $row["edad"];
            $ocupacion = $row["ocupacion"];
            $id_estado = $row["id_estado"];
            $id_ciudad = $row["id_ciudad"];
            $consulta_ciudad = mysql_query("SELECT ciudad FROM ciudades 
											WHERE id_ciudad=".$id_ciudad) 
											or die(mysql_error());
            $row_ciudad = mysql_fetch_array($consulta_ciudad);
            $descripcion_ciudad = ucwords(strtolower($row_ciudad["ciudad"])); 
            $consulta_estado = mysql_query("SELECT estado FROM estados
											WHERE id_estado=".$id_estado) 
											or die(mysql_error());
            $row_estado = mysql_fetch_array($consulta_estado);
            $descripcion_estado = utf8_encode($row_estado["estado"]);
        ?>
            <div class="contenido_proveedor">
                <br />
                <table>
                	<tr>
                    	<th colspan="4">
                        	Ficha de Identificación
                        </th>
                    </tr><tr>
                    	<td id="alright">
                        	 <label class="textos">Nombre: </label>
                        </td><td>
                        	<?php echo $nombre." ".$paterno." ".$materno; ?>
                        </td><td id="alright">
                        	<label class="textos">Edad: </label>
                        </td><td>
                        	<?php echo $edad; ?>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	 <label class="textos">Ocupacion: </label>
                        </td><td>
                        	<?php echo $ocupacion; ?>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Dirección: </label>
                        </td><td>
                        	<?php echo "C. ".$calle." #".$num_exterior." ".$num_interior."<br/> Col. ".$colonia." C.P. ".$codigo_postal; ?>
                        </td><td id="alright">
                        	<label class="textos">Ubicación: </label>
                        </td><td>
                        	<?php echo $descripcion_ciudad.", ".$descripcion_estado; ?>
                        </td>
                    </tr><tr>
                    	<th colspan="4">
                        	Elaboró
                        </th>
                    </tr>
					<?php 
            			$consulta_datos_anamnesis = mysql_query("SELECT * FROM anamnesis 
                                                                WHERE id_cliente=".$id_cliente) 
                                                                or die(mysql_error());
						$row2 = mysql_fetch_array($consulta_datos_anamnesis);
						$id_empleado = $row2["elaboro"];
						$fecha = $row2["fecha_elaboracion"];
						$id_anamnesis = $row2["id_anamnesis"];
						$fecha_normal = cambiaf_a_normal($fecha);
						$consulta_nombre_doctor = mysql_query("SELECT nombre, paterno, materno, id_empleado
                                                        	FROM empleados WHERE id_empleado=".$id_empleado) 
                                                        or die(mysql_error());
						$row3 = mysql_fetch_array($consulta_nombre_doctor);
						$nombre = $row3["nombre"];
						$paterno = $row3["paterno"];
						$materno = $row3["materno"];
						$consulta_antecedentes_familiares_consaguineos = mysql_query("SELECT * 
																					FROM antecedentes_familiares_consaguineos
																					WHERE id_cliente = ".$id_cliente) 
																					or die(mysql_error());
						$row4 = mysql_fetch_array($consulta_antecedentes_familiares_consaguineos);
						$sordera = $row4["sordera"];
						$hipertencion = $row4["hipertencion"];
						$diabeticos = $row4["diabeticos"];
						$con_sordera_infantil = $row4["con_sordera_infantil"];
						$otros = $row4["otros"];
						$consulta_sordera_infantil = mysql_query("SELECT * FROM paciente_con_sordera_infantil
																WHERE id_cliente=".$id_cliente) 
																or die(mysql_error());
						$row5 = mysql_fetch_array($consulta_sordera_infantil);
						$fue_prematuro = $row5["fue_prematuro"];
						$tubo_bilirrubinas_altas = $row5["tubo_bilirrubinas_altas"];
						$estubo_en_incubadora = $row5["estubo_en_incubadora"];
						$otros_problemas_durante_embarazo = $row5["otros_problemas_durante_embarazo"];
						$consulta_antecedentes_personales_no_patologicos = mysql_query("SELECT *
																	FROM antecedentes_personales_no_patologicos
																	WHERE id_cliente=".$id_cliente) 
																	or die(mysql_error());
						$row6 = mysql_fetch_array($consulta_antecedentes_personales_no_patologicos);
						$lugar_residencia = $row6["lugar_residencia"];
						$ocupacion_actual = $row6["ocupacion_actual"];
						$desde_el_año_actual = $row6["desde_el_ano"]; 
						$expuesto_a_ruido_actual = $row6["expuesto_a_ruido"];
						$utiliza_solventes_o_metales_actual = $row6["utiliza_solventes_o_metales_actual"];
						$otros_factores_riesgo_auditivo = $row6["otros_factores_riesgo_auditivo"];
						$otros_trabajos_expuesto_riesgos = $row6["otros_trabajos_expuesto_riesgos"];
						$practica_alguna_actividad_expuesto_riesgos = $row6["practica_alguna_actividad_expuesto_riesgos"];
						$consulta_residencias = mysql_query("SELECT descripcion_residencia 
																FROM residencias
																WHERE id_residencia=".$lugar_residencia) 
																or die(mysql_error());
						$row7 = mysql_fetch_array($consulta_residencias);
						$descripcion_residencia = $row7["descripcion_residencia"];							 
					?>
                    <tr>
                    	<td id="alright">
                        	<label class="textos">Nombre: </label>
                        </td><td>
                        	<?php echo $nombre." ".$paterno." ".$materno;?>
                        </td><td id="alright">
                        	<label class="textos">Fecha: </label>
                        </td><td>
                        	<?php echo $fecha_normal; ?>
                        </td>
                    </tr><tr>
                    	<th colspan="4">
                        	Cuestionario
                        </th>
                    </tr><tr>
                    	<td colspan="4">
                        	<label class="textos" style="font-size:16px;">1-. Antecedentes de familiares consaguineos </label>
                            <ul><!--Antecendentes de familiares consaguineos-->
                                <li>
                                    <label class="textos">Sordera: </label>
                                    <?php echo $sordera; ?>
                                </li><li>
                                	<label class="textos">Hipertencion: </label>
									<?php echo $hipertencion; ?>
                                </li><li>
                                	<label class="textos">Diabeticos: </label>
									<?php echo $diabeticos; ?>
                                </li><li>
                                	<label class="textos">¿Es un paciente con sordera infantil? </label>
                                    <?php echo $con_sordera_infantil; ?>                                
                    				<ul>
                        				<li>
                                        	<label class="textos">Fue prematuro: </label>
                                            <?php echo $fue_prematuro; ?>
                                        </li><li>
                                        	<label class="textos">Tubo bilirrubinas altas? </label>
                                             <?php echo $tubo_bilirrubinas_altas; ?>
                                        </li><li>
                                        	<label class="textos">¿Estubo en incubadora? </label>
                                            <?php echo $estubo_en_incubadora; ?>
                                        </li><li>
                                        	<label class="textos">Otros problemas durante el embarazo: </label>
                                            <?php echo $otros_problemas_durante_embarazo; ?>
                                        </li>
                    				</ul>
                    			</li><li>
                                	<label class="textos">Otros: </label>
                                    <?php echo $otros; ?>
                                </li>
                 			</ul>
                 			<label class="textos" style="font-size:16px;">2-. Antecedentes personales no patologicos </label> 
                            <ul><!--ANTECEDENTES PERSONALES NO PATOLOGICOS-->
                                <li>
                                	<label class="textos">Lugar de residencia: </label>
									<?php echo $descripcion_residencia; ?>
                                </li>
                            </ul>
                			<label class="textos" style="font-size:14px;">Laborales Actuales </label>
                            <ul><!--Inicio de Laborales Actuales-->
                                <li>
                                	<label class="textos">Ocupacion Actual: </label>
                                    <?php echo $ocupacion_actual; ?>
                                </li><li>
                                	<label class="textos">Desde que año: </label>
                                    <?php echo $desde_el_año_actual; ?>
                                </li><li>
                                	<label class="textos">¿Expuesto a ruido? </label>
                                    <?php echo $expuesto_a_ruido_actual; ?>
                                </li>
								<?php 
									if($expuesto_a_ruido_actual == "Si"){
										$consulta_si_es_ruidoso = mysql_query("SELECT * FROM si_es_ruidoso 
																				WHERE id_cliente=".$id_cliente." AND
																				id_registro_identificador=1") 
																				or die(mysql_error());
										$row8 = mysql_fetch_array($consulta_si_es_ruidoso);
										$fuente_del_ruido_actual = $row8["fuente_ruido"];
										$tiempo_exposicion_diario_actual = $row8["tiempo_exposicion_diaria"];
								?>
                                    <ul>
                                        <li>
                                        	<label class="textos">Fuente del Ruido: </label>
                                            <?php echo $fuente_del_ruido_actual; ?>
                                        </li><li>
                                        	<label class="textos">Tiempo de exposicion diario: </label>
                                            <?php echo $tiempo_exposicion_diario_actual; ?>hrs.
                                        </li>
                                    </ul>
								<?php 	
                        			}
								?>
                    			<li>
                                	<label class="textos">¿Utiliza solventes o metales pesados? </label>
                                    <?php 
										echo $utiliza_solventes_o_metales_actual; 
										if($utiliza_solventes_o_metales_actual == "Si"){
											$consulta_producto = mysql_query("SELECT * FROM si_utiliza 
																				WHERE id_cliente=".$id_cliente." AND 
																				id_registro_identificador=1 ")
																				or die(mysql_error());
											$row9 = mysql_fetch_array($consulta_producto);
											$producto_actual = $row9["producto"];
									?>	
                                    <ul>
                                        <li>
                                        	<label class="textos">Producto: </label>
                                            <?php echo $producto_actual; ?>
                                        </li>
                                    </ul>
									<?php
                                        }
                                    ?>
                                </li><li>
                                	<label class="textos">Otros factores de riesgo auditivo: </label>
                                     <?php echo $otros_factores_riesgo_auditivo; ?>
                                </li>
               				</ul>
               				<label class="textos" style="font-size:14px;"> Laborales Anteriores </label>
                			<ul><!--Inicio de Laborales Actuales-->
                    			<li>
                                	<label class="textos">Estubo en algun otro trabajo expuesto a factores de riesgo auditivo: </label>
                                    <?php 
										echo $otros_trabajos_expuesto_riesgos;
										if($otros_trabajos_expuesto_riesgos == "Si"){
											$consulta_otros_trabajos = mysql_query("SELECT * FROM estubo_otros_trabajos
																							WHERE id_cliente=".$id_cliente)
																							 or die(mysql_error());
											while($row10 = mysql_fetch_array($consulta_otros_trabajos)){
												$id_registro_otros_trabajos = $row10["id_registro_otros_trabajos"];
												$ocupacion_anterior = $row10["ocupacion_anterior"];
												$desde_que_año = $row10["desde_que_ano"];
												$hasta_que_año = $row10["hasta_que_ano"];
												$expuesto_a_ruido_anterior = $row10["expuesto_a_ruido"];
												$utiliza_solventes_o_metales_pesados_anterior = $row10["utiliza_solventes_o_metales_pesados"];
												$otros_factores_riesgo_auditivo_anterior = $row10["otros_factores_riesgo_auditivo"];
                                
									?> 
                                    <ul>
                                        <li>
                                        	<label class="textos">Ocupacion: </label>
                                            <?php echo $ocupacion_anterior; ?>
                                        </li><li>
                                        	<label class="textos">Desde que año: </label>
                                            <?php echo $desde_que_año; ?>
                                        </li><li>
                                        	<label class="textos">Hasta que año: </label>
                                            <?php echo $hasta_que_año; ?>
                                        </li><li>
                                        	<label class="textos">¿Expuesto a ruido? </label>
                                            <?php 
												echo $expuesto_a_ruido_anterior; 
												if($expuesto_a_ruido_anterior == "Si"){   
													$consulta_si_es_ruidoso = mysql_query("SELECT * FROM si_es_ruidoso 
																						WHERE id_cliente=".$id_cliente."
																						 AND id_registro_identificador=2 
																						 AND id_registro_tabla=
																						 ".$id_registro_otros_trabajos) 
																						or die(mysql_error());
													$row11 = mysql_fetch_array($consulta_si_es_ruidoso);
													$fuente_del_ruido_anterior = $row11["fuente_ruido"];
													$tiempo_exposicion_diario_anterior = $row11["tiempo_exposicion_diaria"];												
											?>
                                            <ul>
                                                <li>
                                                	<label class="textos">Fuente del Ruido: </label>
                                                    <?php echo $fuente_del_ruido_anterior; ?>
                                                </li><li>
                                                	<label class="textos">Tiempo de exposicion diario: </label>
                                                    <?php echo $tiempo_exposicion_diario_anterior; ?>hrs.
                                                </li>
                                            </ul>
											<?php 
                                				} 
											?>
                                        </li><li>
                                        	<label class="textos">¿Utilizaba solventes o metales pesados? </label>
                                            <?php
												echo $utiliza_solventes_o_metales_pesados_anterior; 
												if($utiliza_solventes_o_metales_pesados_anterior == "Si"){
													$consulta_producto = mysql_query("SELECT * FROM si_utiliza 
																						WHERE id_cliente=".$id_cliente." 
																						AND id_registro_identificador=2 AND
																						id_registro_tabla=
																						".$id_registro_otros_trabajos)
																						or die(mysql_error());
													$row12 = mysql_fetch_array($consulta_producto);
													$producto_anterior = $row12["producto"];
											?>
                                            <ul>
                                                <li>
                                                	<label class="textos">Producto:</label>
                                                	<?php echo $producto_anterior; ?>
                                                </li>
                                            </ul>
											<?php
                                				}
											?>
                            			<li>
                                        	<label class="textos">Otros factores de riesgo auditivo: </label>
                                            <?php echo $otros_factores_riesgo_auditivo_anterior; ?>
                                        </li><li style="list-style:none;">&nbsp;</li>
                        			</ul>
									<?php		
                            				}
                        				}
									?>
                      			</li>
                            </ul>
                            <label class="textos" style="font-size:14px;"> Extra Laborales </label>
                            <ul>
                            	<li>
                                	<label class="textos">Practica alguna actividad donde este expuesto a factores de riesgo auditivo: </label>
									<?php 
										echo $practica_alguna_actividad_expuesto_riesgos;
										if($practica_alguna_actividad_expuesto_riesgos == "Si"){
											$consulta_actividades_extras = mysql_query("SELECT * 
																						FROM actividades_extra_laborales
																						WHERE id_cliente=".$id_cliente)
																						or die(mysql_error());
											while($row13 = mysql_fetch_array($consulta_actividades_extras)){
												$id_registro_actividad = $row13["id_registro_actividad"];
												$actividad = $row13["actividad"];
												$desde_que_año_actividad = $row13["desde_que_ano"];
												$expuesto_a_ruido_actividad = $row13["expuesto_a_ruido"];
												$utiliza_solventes_o_metales_actividad = $row13["utiliza_solventes_o_metales"];
												$otros_factores_riesgo_auditivo_actividad = $row13["otros_factores_riesgo_auditivo"];
									?>
                                    <ul>
                                        <li>
                                        	<label class="textos">Actividad: </label>
                                            <?php echo $actividad; ?>
                                        </li><li>
                                        	<label class="textos">Desde que año: </label>
                                            <?php echo $desde_que_año_actividad; ?>
                                        </li><li>
                                        	<label class="textos">Expuesto a ruido?</label>
											<?php 
												echo $expuesto_a_ruido_actividad;                                         
												if($expuesto_a_ruido_actividad == "Si"){   
													$consulta_si_es_ruidoso = mysql_query("SELECT * FROM si_es_ruidoso 
																						WHERE id_cliente=".$id_cliente."
																						 AND id_registro_identificador=3 
																						 AND id_registro_tabla=
																						 ".$id_registro_actividad) 
																						or die(mysql_error());
													$row14 = mysql_fetch_array($consulta_si_es_ruidoso);
													$fuente_del_ruido_actividad = $row14["fuente_ruido"];
													$tiempo_exposicion_diario_actividad = $row14["tiempo_exposicion_diaria"];												
											?>
                                            <ul>
                                                <li>
                                                	<label class="textos">Fuente del Ruido: </label>
	                                                <?php echo $fuente_del_ruido_actividad; ?>
                                                </li>
                                                <li>
                                                	<label class="textos">Tiempo de exposicion mensual: </label>
                                                	<?php echo $tiempo_exposicion_diario_actividad; ?>hrs.
                                                </li>
                                            </ul>
										<?php 
                                            } 
                                        ?>
                                        </li><li>
                                        	<label class="textos">¿Utilizaba solventes o metales pesados? </label>
											<?php 
												echo $utiliza_solventes_o_metales_actividad; 
												if($utiliza_solventes_o_metales_actividad == "Si"){
													$consulta_producto = mysql_query("SELECT * FROM si_utiliza 
																						WHERE id_cliente=".$id_cliente." 
																						AND id_registro_identificador=3 AND
																						id_registro_tabla=
																						".$id_registro_actividad)
																						or die(mysql_error());
													$row15 = mysql_fetch_array($consulta_producto);
													$producto_actividad = $row15["producto"];
											?>
                                            <ul>
                                                <li>Producto:</li>
                                                <?php echo $producto_actividad; ?>
                                            </ul>
											<?php
                                                }
                                            ?>
                                        </li><li>
                                        	<label class="textos">Otros factores de riesgo auditivo: </label>
                                            <?php echo $otros_factores_riesgo_auditivo_actividad; ?>
                                        </li><li style="list-style:none;">
                                        	&nbsp;
                                        </li>
                        			</ul>
									<?php	
                            				}
                        				}
									?>
                                </li>
							</ul>
                			<label class="textos" style="font-size:16px;">3-. Antecedentes personales patalogicos </label> 
                			<ul>
							<?php 
								$consulta_antecedentes_personales_patologicos = mysql_query("SELECT *
																		FROM antecedentes_personales_patologicos
																		WHERE id_cliente=".$id_cliente) 
																		or die(mysql_error());
								$row16 = mysql_fetch_array($consulta_antecedentes_personales_patologicos);
								$presenta_sordera = $row16["presenta_sordera"]; 
								$hipertencion_personales = $row16["hipertencion_personales"];
								$diabeticos_personales = $row16["diabeticos_personales"];
								$infecciones_garganta = $row16["infecciones_garganta"];
								$cirugia_traumatismo = $row16["cirugia_traumatismo"];                   
                            ?>
                    			<li>
                                	<label class="textos">Presenta sordera: </label>
                                    <?php echo $presenta_sordera; ?>
                                </li>
								<?php 
									if($presenta_sordera == "Si"){
										$consulta_presenta_sordera = mysql_query("SELECT * FROM si_presenta_sordera
																					WHERE id_cliente=".$id_cliente) 
																					or die(mysql_error());
										$row17 = mysql_fetch_array($consulta_presenta_sordera);
										$desde_que_año_sordera = $row17["desde_que_ano"];
										$ha_usado_auxiliar_auditivo = $row17["ha_usado_auxiliar_auditivo"];	
                                ?>
                    			<ul>
                        			<li>
                                    	<label class="textos">Desde que año: </label>
                                        <?php echo $desde_que_año_sordera; ?>
                                    </li><li>
                                    	<label class="textos">Ha usado auxiliar auditivo: </label>
                                        <?php echo $ha_usado_auxiliar_auditivo; ?>
                                    </li>
									<?php
										if($ha_usado_auxiliar_auditivo == "Si"){
											$consulta_auxiliar_auditivo = mysql_query("SELECT * 
																					FROM si_ha_usado_auxiliar_auditivo
																					WHERE id_cliente=".$id_cliente) 
																					or die(mysql_error());	
											while($row18 = mysql_fetch_array($consulta_auxiliar_auditivo)){
												$desde_que_año_auxiliar = $row18["desde_que_ano"];
												$modelo = $row18["modelo"];
												$que_resultado_obtuvo = $row18["que_resultado_obtuvo"];
									?>
                                        <ul>
                                            <li>
                                            	<label class="textos">Desde que año: </label>
                                                <?php echo $desde_que_año_auxiliar; ?>
                                            </li><li>
                                            	<label class="textos">Modelo: </label>
                                                <?php echo $modelo; ?>
                                            </li><li>
                                            	<label class="textos">Que resultados obtuvo </label>
                                                <?php echo $que_resultado_obtuvo; ?>
                                            </li><li style="list-style:none;">
                                            	&nbsp;
                                            </li>
                                        </ul>
									<?php	
                                			}
                            			}
									?>
                    			</ul>
								<?php
                        			}
								?>
                    			<li>
                                	<label class="textos">Hipertencion: </label>
                                    <?php echo $hipertencion_personales; ?>
                                </li><li>
                                	<label class="textos">Diabeticos: </label>
                                    <?php echo $diabeticos_personales; ?>
                                </li><li>
                                	<label class="textos">Infecciones de garganta recurrentes: </label>
                                    <?php echo $infecciones_garganta; ?>
                                </li><li>
                                	<label class="textos">Ha presentado algun padecimiento, cirugia o traumatismo que afecte sus oidos: </label>
									<?php 
                        				echo $cirugia_traumatismo; 
										if($cirugia_traumatismo == "Si"){
											$consulta_traumatismo_o_cirugia = mysql_query("SELECT * 
																					FROM si_presenta_algun_padecimiento 
																					WHERE id_cliente=".$id_cliente) 
																				or die(mysql_error());
											while($row19 = mysql_fetch_array($consulta_traumatismo_o_cirugia)){
												$descripcion = $row19["descripcion"];
												$desde_que_ano_cirugia = $row19["desde_que_ano"];
												$tratamiento = $row19["tratamiento"];
												$resultados = $row19["resultados"];
									?>
                                    <ul>
                                        <li>
                                        	<label class="textos">Descripcion: </label>
                                            <?php echo $descripcion; ?>
                                        </li><li>.
                                        	<label class="textos">Desde que año: </label>
                                            <?php echo $desde_que_ano_cirugia; ?>
                                        </li><li>
                                        	<label class="textos">Tratamiento: </label>
                                            <?php echo $tratamiento; ?>
                                        </li><li>
                                        	<label class="textos">Resultados: </label>
                                            <?php echo $resultados; ?>
                                        </li><li style="list-style:none;">
                                        	&nbsp;
                                        </li>
                                    </ul>
									<?php	
                            				}
                        				}
									?>
								</li>
                			</ul>
                        </td>
                    </tr><tr>
                    	<td colspan="4" id="alright">
                        <form name="accion" method="post" action="editar_anamnesis.php">
                        <input name="id_paciente" type="hidden" value="<?php echo $id_cliente; ?>" />
                        	<input name="accion" type="submit" value="Editar Anamnesis" class="fondo_boton" />
                        </form>
                        </td>
                    </tr>
                </table>
                <span style="font-size:19px; margin-left:220px; font-weight:bold;"></span>
            </div><!-- Fin de contenido proveedor -->
        </div><!-- Fin de area contenido -->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido clolumna 2-->
</body>
</html>