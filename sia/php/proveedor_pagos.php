<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pagos Proveedor</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script language="javascript" type="text/javascript" src="../js/Validacion.js"></script>
</head>
<body>
	<div id="contenido_columna2">
		<div class="contenido_pagina">    
			<div class="fondo_titulo1">
				<div class="categoria">
					Proveedores
				</div><!-- Fin DIV categoria -->
			</div><!-- Fin de fondo titulo1 -->
					<div class="area_contenido1">
						<br />
						<?php
						// SE IMPORTA EL ARCHIVO QUE INCLUYE LA CONEXION A LA BASE DE DATOS
							include("config.php");
						// SE OBTIENE EL ID DEL PROVEEDOR Y EL FOLIO POR MEDIO DEL METODO GET
							$id_proveedor = $_GET['id_proveedor'];
							$folio = $_GET['folio'];
						/*SE REALIZA QUERY PARA OBTENER LOS DATOS DE LAS CUENTA 
						POR PAGAR DE ACUERDO AL PROVEEDOR SELECCIONADO ANTERIORMENTE*/
							$datos_cuentas_por_pagar = mysql_query("SELECT fecha, estatus_cuenta 
															FROM cuentas_por_pagar,estatus_cuentas 
															WHERE folio_orden_compra = ".$folio." 
															AND estatus_cuentas.id_estatus_cuentas = cuentas_por_pagar.id_estatus")
															or die (mysql_error());
							$row2 = mysql_fetch_array( $datos_cuentas_por_pagar );
						// SE REALIZA EL QUERY PARA OBTENER EL NOMBRE DEL PROVEEDOR DE ACUERDO AL ID OBTENIDO ANTERIORMENTE
							$query_nombre_proveedor = mysql_query("SELECT razon_social 
																	FROM proveedores 
																	WHERE id_proveedor = $id_proveedor") 
																	or die (mysql_error());
						// SE ALMACENAN EN EL ARREGLO LOS DATOS DEL PROVEEEDOR
							$row = mysql_fetch_array( $query_nombre_proveedor );
							$nombre_proveedor = $row['razon_social'];
						?>
						<div class="contenido_proveedor">
                        	<table>
                            	<tr>
									<td style="text-align:right">
                                    	<label class="textos">Folio: </label>
									</td><td>
                                    	<?php echo $folio; ?>
                            			<input name="folio" type="hidden" value="<?php echo $folio; ?>" 
			                            maxlength="3" disabled="disabled" style="text-align:center" size="4 "/>
									</td><td style="text-align:right">
                                    	<label class="textos">Proveedor: </label>
                                    </td><td>
                                    	<?php echo $nombre_proveedor; ?>
										<input name="proveedor" type="hidden" disabled="disabled" 
                                        style="text-align:center; width:250px;" value="<?php echo $nombre_proveedor; ?>"/>
                                    </td>
                               </tr><tr>
                               		<td style="text-align:right">
                                     	<label class="textos">Fecha de Recepción: </label>
                                    </td><td>
                                    	<?php echo $row2['fecha']; ?>
										<input name="fecha_orden" type="hidden" disabled="disabled" 
                            			maxlength="10" value="<?php echo $row2['fecha']; ?>" style="text-align:center"/>							
									</td><td style="text-align:right">
                            			<label class="textos">Estatus: </label>
									</td><td> 
                                    	<?php echo ucwords($row2['estatus_cuenta']); ?>                                   	
                                    	<input name="estatus" type="hidden" disabled="disabled" style="text-align:center; width:250px;" 
                            			value="<?php echo ucwords($row2['estatus_cuenta']); ?>"  />
                                    </td>
                               </tr>
                            </table>
                            <br />
							<table style="text-align:center">                            	
								<tr>
                                	<th colspan="5">Lista de articulos</th>
                                </tr><tr>
									<th colspan="2">Producto</th>
									<th>Cantidad</th>
									<th>Precio Unitario</th>
									<th>Subtotal</th>
								</tr>   
                                <?php
									$datos_cuentas = mysql_query('SELECT categoria, base_productos.id_categoria,cantidad_codigo, costo, descripcion,referencia_codigo
																FROM cuentas_articulos,base_productos,cuentas_por_pagar,categorias_productos
																WHERE folio_orden_compra = '.$folio.'
																AND categorias_productos.id_categoria=base_productos.id_categoria
																AND id_cuenta_por_pagar=cuentas_por_pagar.id_registro 
																AND base_productos.id_base_producto=cuentas_articulos.id_articulo')
																or die(mysql_error());
									$total=0;
									while($c_datos = mysql_fetch_array($datos_cuentas)){
										$id_categoria = $c_datos['id_categoria'];
										$categoria = $c_datos['categoria'];
										$cantidad = $c_datos['cantidad_codigo'];									
										$precio_unitario = $c_datos['costo'];
										$descripcion = $c_datos['descripcion'];
										$referencia_codigo = $c_datos['referencia_codigo'];
										if($id_categoria==1){
											$imprime=$referencia_codigo;
										}else{
											$imprime=$descripcion;
										}
										$total=$total+$precio_unitario*$cantidad;
									/*$c_productos = mysql_query('SELECT categoria, referencia_codigo 
														FROM categorias_productos, base_productos
														WHERE base_productos.id_categoria = categorias_productos.id_categoria
														AND id_base_producto = '.$id_base_producto
														)or die(mysql_error());
									$c_i = 0;
									$row_productos = mysql_fetch_array($c_productos);
									$categoria = $row_productos['categoria'];
									$producto = $row_productos['referencia_codigo'];*/
								?>                          
								<tr>
									<td><?php echo $categoria; ?></td>
                                    <td><?php echo "[".$imprime."]"; ?></td>
									<td style="text-align:center"><?php echo $cantidad; ?></td>
									<td id="alright"><?php echo "$".number_format($precio_unitario,2); ?></td>
									<td id="alright"><?php echo "$".number_format($precio_unitario*$cantidad,2); ?></td>
								</tr>
                                <?php
									//$deuda = $deuda + ($precio_unitario*$cantidad);
									}
								?>
                                <tr>
                                	<td colspan="5" id="alright" style="border-top:1px solid #666">
                                    	<label class="textos"> Total a Pagar: </label><?php echo "$".number_format($total,2) ?>
                                    </td>
                                </tr>
							</table>						
					</div><!--Fin de contenido proveedor-->
						<div class="contenido_proveedor">
							<table>
                            	<tr>
                                	<th colspan="4">Pagos</th>
                                </tr>
								<tr>
									<th>Fecha de Deposito</th>
									<th>Cantidad</th>
									<th>Forma de Pago</th>
									<th>Observaciones</th>
								</tr>
							   <?php
									$pagos_proveedor = mysql_query('SELECT fecha_deposito,cheque_transferencia,
																	cantidad, referencia, observaciones
																	FROM pagos_proveedores
																	WHERE id_proveedor ='.$id_proveedor." 
																	AND folio_orden_compra =".$folio)or die(mysql_error());
									$saldo=0;
									while($row_pagos = mysql_fetch_array($pagos_proveedor)){
										
								?>                                
								<tr>
									<td><?php echo $row_pagos['fecha_deposito']; ?></td>
									<td id="alright"><?php echo "$".number_format($row_pagos['cantidad'],2); ?></td>
									<td>
										<label class="textos"><?php echo $row_pagos['cheque_transferencia'].": "; ?></label><br />
										<?php echo $row_pagos['referencia']; ?>
                                    </td>
									<td><?php echo $row_pagos['observaciones']; ?></td>
								</tr>
                                <?php
									$saldo = $saldo+$row_pagos['cantidad']; 
									}
								?>
                                <tr>
                                	<td colspan="4" id="alright" style="border-top:1px solid #666">
                                    <label class="textos"> Saldo: </label>	
									<?php  
										echo "$".number_format($total-$saldo,2); 
										if(($total-$saldo)<=0 and (!isset($_GET['x']))){
											mysql_query('UPDATE cuentas_por_pagar SET id_estatus=3 WHERE folio_orden_compra='.$folio)or die(mysql_error());
										
										
									?>
                                    <script>
										window.location.href="proveedor_pagos.php?id_proveedor=<?php echo $id_proveedor."&folio=".$folio."&x=0" ?>";
									</script>
                                    <?php
										}
									?>                               
                                    </td>
                                </tr>
							</table>
                            <?php
								if(!isset($_GET['x'])){
							?>														
							<form name="form_nuevo_pago" id="form_nuevo_pago" method="post" 
                            action="procesa_proveedor_pago.php" onsubmit="return validarPagosProveedor()" >
							<br /><table width="624px">                            
                            	<tr>
                                	<th colspan="4">Nuevo Pago</th>
                                </tr><tr>                                
                                    <td style="text-align:right">
                                        <label class="textos"> Fecha de Deposito: </label>
                                    </td><td>
                                        <input name="fecha_deposito" id="fecha_deposito" type="text" 
                                        maxlength="10" onkeyup="Validar(this,'/',patron,true)" />
                                    </td><td style="text-align:right">
                                        <label class="textos">Cantidad: </label>
                                    </td><td>
                                        <input name="cantidad" type="text" id="cantidad" onchange="validarSiNumero(this.value)" />
									</td>
								</tr><tr>
                                	<td style="text-align:right">
                                    	<label class="textos"> Forma de Pago: </label>
									</td><td>
                                        <select name="cheque_transferencia">
                                            <option value="0" selected="selected"> --- Forma de Pago --- </option>
                                            <option value="Efectivo">Efectivo</option>
                                            <option value="Cheque">Cheque</option>
                                            <option value="Tarjeta de Débito">Tarjeta de Débito</option>
                                            <option value="Tarjeta de Crédito">Tarjeta de Crédito</option>
                                            <option value="Transferencia Bancaria">Transferencia Bancaria</option>
                                        </select>
                                    </td><td style="text-align:right">
										<label class="textos"> Referencia:</label>
									</td><td>
                                		<input name="referencia" type="text" />
									</td>
                                </tr><tr>
									<td style="text-align:right">
                                    	<label class="textos"> Observaciones: </label><br />
									</td><td colspan="3">
                                		<textarea name="observaciones" cols="52" rows="5"></textarea>
                                    </td>
                                </tr>
								<input type="hidden" name="folio" value="<?php echo $folio; ?>"/>
								<input type="hidden" name="proveedor" value="<?php echo $id_proveedor; ?>"/>
								<tr>
									<td colspan="4" style="text-align:right">
                                    	<input name="volver" type="button" value="Volver" class="fondo_boton" 
                                        onclick="window.location.href='cuentas_por_pagar.php?id_proveedor=<?php echo $id_proveedor; ?>'" />
                                        <input name="guardar" type="submit" value="Guardar"
                                         class="fondo_boton" title="Guardar"/>
									</td>
								</tr>
                            </table>
                            </form>
                            <?php
								}else{
							?>
                            <div style="text-align:center">
                                <input type="button" name="volver" value="Volver" class="fondo_boton"
                                onclick="window.location.href='cuentas_por_pagar.php?id_proveedor=<?php echo $id_proveedor ?>'" />
                            </div>
                            <?php
								}
							?>
                            <br />
							</div><!--Fin de contenido proveedor-->
				</div><!--Fin de area contenido-->
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</body>
</html>