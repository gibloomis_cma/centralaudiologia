<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Lista de Proveedores</title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
	<script type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
	<div id="contenido_columna2">           
		<div class="contenido_pagina">
			<div class="fondo_titulo1">
   				<div class="categoria">
            		Proveedores
            	</div><!-- Fin DIV categoria -->
			</div><!--Fin de fondo titulo-->
					<div class="contenido_atras">
        				<div class="buscar2">
                                <?php
									// SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS 
									include("config.php");
									$res2="";								
									if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
										$filtro = $_POST['filtro'];
										$res_busqueda = mysql_query("SELECT COUNT(*) 
																			FROM proveedores
																			WHERE razon_social LIKE '%".$filtro."%' 
																			OR proveedor LIKE '%".$filtro."%'");	
										$row_busqueda = mysql_fetch_array($res_busqueda);										
										$res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
									}
								?>              
                       	 	<form name="busqueda" method="post" action="lista_proveedores_cuentas.php">
                            	<label><?php echo $res2; ?></label>
                                <input name="filtro" type="text" size="15" maxlength="15" />
                                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" />
                            </form>
        				</div><!-- Fin de la clase buscar2 -->
                        <br />
        					<div class="area_contenido2">
    							<center>
									<table>
                                    	<tr>
                                        	<th colspan="3">
                                            	Cuentas por Pagar
                                            </th>
										</tr><tr>
        									<th>Proveedor</th>
        									<th colspan="2">Razon Social</th>
    									</tr>
                       			<?php                       			
								// SE REALIZA EL QUERY QUE OBTIENE LA INFORMACION RELACIONADA CON LOS PROVEEDORES								
									if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
										$filtro = $_POST['filtro'];
										$consulta_proveedores = mysql_query("SELECT DISTINCT(id_proveedor), 
																			razon_social, proveedor 
																			FROM proveedores
																			WHERE razon_social LIKE '%".$filtro."%' 
																			OR proveedor LIKE '%".$filtro."%'");										
												
									}else{
									$consulta_proveedores = mysql_query("SELECT DISTINCT(id_proveedor), razon_social, proveedor 
																		FROM proveedores");
									}
								// SE REALIZA UN CICLO EL CUAL SIRVE PARA IMPRIMIR Y OBTENER LA INFORMACION DE LOS DIFERENTES PROVEEDORES
								$res = 0;
									while($row = mysql_fetch_array($consulta_proveedores)){
										$res++;
										$id_proveedor = $row["id_proveedor"];
										$proveedor = $row["proveedor"];
										$nombre = $row["razon_social"];
									?>
										<tr>
											<td>
                                                <label class="textos">
													<?php echo $proveedor; ?> 
                                                </label>
                                           	</td><td> 
                                            	<label class="textos">
													<?php echo $nombre; ?> 
                                                </label>
                                            </td><td id="alright">
                                            	<a href="cuentas_por_pagar.php?id_proveedor=<?php echo $id_proveedor; ?>" target="_self">
                                                	<img src="../img/info.png" title="<?php echo $proveedor; ?>" />
                                                </a>
                                            </td>
   										</tr> 
                       		 	<?php		
									}
									if($res==0){
								?>
										<tr>
											<td style="text-align:center;" colspan="3">
												<label class="textos">"No hay proveedores registrados"</label>
											</td>
										</tr>            
								<?php
									}
								?>
									</table>                                    
							</center>
        					<br />
        					</div><!-- Fin de area contenido2 -->
        		</div><!-- Fin de contenido atras -->
			</div><!--Fin de contenido pagina-->
		</div><!--Fin de contenido clolumna 2-->
</body>
</html>