<?php
    error_reporting(0);
	// SE INICIA LA SESION EN EL SCRIPT 
	session_start();

	// SE COLOCA LA VARIABLE DE SESION FOLIO EN BLANCO
	$_SESSION["folio"] = "";

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Lista de Moldes Entregados </title>
	<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
	<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
	<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
	<script type="text/javascript" language="javascript">
        $(function() {
            //call the function onload, default to page 1
            getdata( 1 );
        });

        function getdata( pageno ){                     
            var targetURL = 'lista_moldes_entregados_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons

            $('#tabla_lista_moldes').html('<p><img src="../img/ajax-loader.gif" /></p>');       
            $('#tabla_lista_moldes').load( targetURL,{
                filtro: "<?php echo $_POST['filtro']; ?>"
            } ).hide().fadeIn('slow');
        } 
	</script>
	<style type="text/css">
		div.contenedor 
		{
			position: relative;
			width: 140px;
			left: 107px;
		}
		
		#input 
		{
			font-family: Arial;
			color: #000;
			font-size: 10pt;
			width: 140px;
		}

		div.fill 
		{
			font-family: Arial;
			font-size: 10pt;
			display: none;
			width: 197px;
			color: #CCC;
			background-color: #CCC;
			border: 1px solid #999;
			overflow: auto;
			height: auto;
			top: -1px;
		}

		tr.fill 
		{
			font-family: Arial;
			font-size: 8pt;
			color: #FFF;
			background-color: #a2a2a3;
			border: 1px solid #a2a2a3;
		}

		#tr 
		{
			font-family: Arial;
			font-size: 8pt;
			background-color: #d9dcdf;
			color: #000;
			border: 1px solid #d9dcdf;
		}
	</style>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div><!-- FIN DIV CATEGORIA -->
            </div><!--Fin de fondo titulo-->
			<?php 
        		date_default_timezone_set('America/Monterrey');
        		$script_tz = date_default_timezone_get();
        		$fecha = date("d/m/Y");
        		$hora = date("h:i:s A");

        		// SE VALIDA SI SE HA REALIZADO UNA BUSQUEDA
        		if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
        		{
            		$filtro = $_REQUEST['filtro'];
                	$res_busqueda = mysql_query("SELECT count(DISTINCT(folio_num_molde)) AS contador
                                                 FROM moldes
                                                 WHERE (nombre LIKE '%".$filtro."%' 
                                                 OR paterno LIKE '%".$filtro."%'
                                                 OR materno LIKE '%".$filtro."%'
                                                 OR folio_num_molde LIKE '%".$filtro."%')
                                                 AND id_estatus_moldes = 7") or die(mysql_error());
                	
                	$row_busqueda = mysql_fetch_array($res_busqueda);
                	$busqueda = 0;
                	$busqueda += $row_busqueda["contador"];
            		if( $busqueda == 0 )
            		{
                		$busqueda = 0;	
            		}
            		$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
        		}
        		else
        		{
            		$res2="";
        		}
			?> 
            <div class="buscar2">
            	<form name="busqueda" method="post" action="lista_moldes_entregados.php">
                	<label class="textos"><?php echo $res2; ?></label>
                	<input name="filtro" type="text" size="15" maxlength="15" />
                	<input name="buscar" type="submit" value="Buscar" class="fondo_boton" style="height:25px;" />
            	</form>
            </div><!-- FIN DIV BUSCAR 2 -->
            <div class="area_contenido2">
            	<div class="contenido_proveedor">
                    <center>
                        <table>
                            <tr>
                                <th colspan="5"> MOLDES ENTREGADOS </th>
                            </tr>
                        </table>
                        <div id="tabla_lista_moldes">
                            <img src="../img/ajax-loader.gif" />
                        </div>
                    </center>
                    <br />
                </div><!-- FIN DIV CONTENIDO PROVEEDOR -->
        	</div><!-- Fin de area contenido -->
    	</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>