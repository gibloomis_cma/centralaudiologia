<?php
	// SE INICIA SESION
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
	
	// SE ESTABLECE LA ZONA HORARIA
	date_default_timezone_set('America/Monterrey');
	$script_tz = date_default_timezone_get();

	// SE DECLARAN VARIABLES QUE CONTIENEN LA FECHA Y HORA DEL DIA
	$fecha_actual = date("d/m/Y");
	$hora = date("h:i:s A");

	// SE OBTIENE EL NUMERO DE MOLDE ATRAVES DE METODO GET
	$num_folio_molde = $_GET["folio"];

	// SE REALIZA QUERY QUE OBTIENE TODOS LOS DATOS DE LA TABLA REPARACIONES DE ACUERDO AL NUMERO DE FOLIO
	$consulta_datos_de_num_molde = mysql_query("SELECT *
												FROM moldes 
											 	WHERE folio_num_molde = '$num_folio_molde'") or die(mysql_error());
	
	// SE ALMACENA EL RESULTADO EN FORMA DE ARREGLO EN UNA VARIABLE
	$row = mysql_fetch_array($consulta_datos_de_num_molde);

	// SE DECLARAN VARIABLES CON EL RESULTADO CORRESPONDIENTE
	$id_cliente = $row["id_cliente"];
	$nombre = $row["nombre"];
	$paterno = $row["paterno"];
	$materno = $row["materno"];
	$telefono = $row["descripcion"];
	$calle = $row["calle"];
	$num_exterior = $row["num_exterior"];
	$num_interior = $row["num_interior"];
	$codigo_postal = $row["codigo_postal"];
	$colonia = $row["colonia"];
	$id_ciudad = $row["id_ciudad"];
	$id_estado = $row["id_estado"];
	$fecha_entrada = $row["fecha_entrada"];
	$fecha_entrada_separada = explode("-", $fecha_entrada);
	$fecha_entrada_normal = $fecha_entrada_separada[2]."/".$fecha_entrada_separada[1]."/".$fecha_entrada_separada[0];
	$lado_oido = $row["lado_oido"];
	$id_estilo = $row["id_estilo"];
	$id_material = $row["id_material"];
	$id_color = $row["id_color"];
	$id_ventilacion = $row["id_ventilacion"];
	$id_salida = $row["id_salida"];
	$id_estatus_molde = $row["id_estatus_moldes"];
	$costo = $row["costo"];
	$pago_parcial = $row['pago_parcial'];
	$adeudo = $costo - $pago_parcial;
	$observaciones = $row["observaciones"];
	$adaptacion = $row['adaptacion'];
	$reposicion = $row['reposicion'];
		
	// SE CONSULTA EL ESTATUS EN EL QUE SE ENCUENTRA EL MOLDE
	$consulta_id_estatus_molde = mysql_query("SELECT estado_molde 
											  FROM estatus_moldes 
											  WHERE id_estatus_moldes = '$id_estatus_molde'") or die(mysql_error());
	
	// SE DECLARA UNA VARIABLE EN FORMA DE ARREGLO CON EL RESULTADO OBTENIDO DEL QUERY
	$row_estatus_molde = mysql_fetch_array($consulta_id_estatus_molde);
	$estado_molde = $row_estatus_molde["estado_molde"];

	// SE REALIZA QUERY QUE OBTIENE EL TIPO DE VENTILACION QUE ES EL MOLDE
	$consulta_ventilacion = mysql_query("SELECT calibre
										 FROM ventilaciones
										 WHERE id_ventilacion = '$id_ventilacion'") or die(mysql_error());
	
	// SE DECLARA VARIABLE QUE ALMACENA EL RESULTADO OBTENIDO DEL QUERY EN FORMA DE ARREGLO
	$row_ventilacion = mysql_fetch_array($consulta_ventilacion);
	$calibre = $row_ventilacion["calibre"];

	// SE VALIDA SI EL PACIENTE BUSCADO ES CLIENTE O NO
	if( $id_cliente == 0 || ($id_cliente != 0 && $telefono != "" ) )
	{
		$telefono_cliente = $telefono;
	}
	else
	{
		$consulta_telefono = mysql_query("SELECT descripcion
										  FROM contactos_clientes
										  WHERE id_cliente = '$id_cliente'
										  AND tipo = 'Telefono'") or die(mysql_error());
		$row4 = mysql_fetch_array($consulta_telefono);
		$telefono_cliente = $row4["descripcion"];
	}

    // SE REALIZA QUERY QUE OBTIENE EL EMPLEADO QUE REALIZO LA ORDEN DEL MOLDE
    $query_empleado_recibio = "SELECT id_empleado
                               FROM movimientos_moldes
                               WHERE folio_num_molde = '$num_folio_molde'
                               AND id_estado_movimiento = '2'
                               AND id_estatus_moldes = '2'
                               ORDER BY id_registro DESC";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_empleado_recibio = mysql_query($query_empleado_recibio) or die(mysql_error());
    $row_empleado_recibio = mysql_fetch_array($resultado_empleado_recibio);
    $id_empleado_consultado = $row_empleado_recibio['id_empleado'];

    // SE REALIZA QUERY QUE OBTIENE EL ID DEL EMPLEADO QUE REALIZO EL MOLDE
    $query_empleado_fabrico = "SELECT elaboro,fecha_salida,observaciones
                               FROM moldes_elaboracion
                               WHERE folio_num_molde = '$num_folio_molde'";

    // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
    $resultado_empleado_fabrico = mysql_query($query_empleado_fabrico) or die(mysql_error());
    $row_empleado_fabrico = mysql_fetch_array($resultado_empleado_fabrico);
    $id_empleado_fabrico = $row_empleado_fabrico['elaboro'];
    $fecha_fabricacion = $row_empleado_fabrico['fecha_salida'];
    $fecha_fabricacion_separada = explode("-", $fecha_fabricacion);
    $fecha_fabricacion_normal = $fecha_fabricacion_separada[2]."/".$fecha_fabricacion_separada[1]."/".$fecha_fabricacion_separada[0];
    $observaciones_fabricacion = $row_empleado_fabrico['observaciones'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Detalles de Moldes </title>
	<link rel="stylesheet" href="../css/style3.css" type="text/css">
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
	<script type="text/javascript" language="javascript" src="../js/funcion.js"></script>
	<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div>
            </div><!--Fin de fondo titulo-->
            <div class="area_contenido1">
    		<br />
            <div class="contenido_proveedor">
                <form name="form_datos_generales" id="form_detalles_moldes" method="post" action="cambiar_estatus_moldes_modificacion.php">
                	<fieldset>
                    	<legend> Datos Generales </legend>
                    	<table>
                    		<tr>
                    			<td> <input type="hidden" name="id_empleado_sistema" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" /> </td>
                    			<td> <input type="hidden" name="hora_real" value="<?php echo $hora; ?>" /> </td>
                    		</tr>
                    		<tr>
                    			<td> <label class="textos"> N° de Nota: </label>  <input type="text" name="folio_molde" id="folio_molde" readonly="readonly" value="<?php echo $num_folio_molde; ?>" size="5" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                    			<td> <label class="textos"> Fecha de entrada: </label>  <label> <?php echo $fecha_entrada_normal; ?> </label> </td>
                    			<td> <label class="textos"> Fecha actual: </label> <input type="text" name="fecha_actual" readonly="readonly" value="<?php echo $fecha_actual; ?>" size="10" style="background-color:#7f7f7f; color:#FFF; text-align:center; font-weight:bold; border:none;" /> </td>
                    		</tr>
                    		<tr>
                    			<td>
                    				<label class="textos"> Nombre(s): </label>
                    				<br/>
                    				<input type="text" name="nombre_paciente" id="nombre_paciente" value="<?php echo $nombre; ?>" />
                    			</td>
                    			<td>
                    				<label class="textos"> Apellido Paterno: </label> <br/>
                    				<input type="text" name="paterno_paciente" id="paterno_paciente" value="<?php echo $paterno; ?>" />
                    			</td>
                    			<td>
                    				<label class="textos">  Apellido Materno: </label> <br/>
                    				<input type="text" name="materno_paciente" id="materno_paciente" value="<?php echo $materno; ?>" />
                    			</td>
                    		</tr>
                    		<tr>
                    			<td>
                    				<label class="textos"> Calle: </label> <br/>
                    				<input type="text" name="calle" id="calle" value="<?php echo $calle; ?>" />
                    			</td>
                    			<td>
                    				<label class="textos"> # Exterior: </label> <br/>
                    				<input type="text" name="num_exterior" id="num_exterior" value="<?php echo $num_exterior; ?>" />
                    			</td>
                    			<td>
                    				<label class="textos"> # Interior: </label> <br/>
                    				<input type="text" name="num_interior" id="num_interior" value="<?php echo $num_interior; ?>" />
                    			</td>
							</tr>
							<tr>
								<td>
									<label class="textos"> Colonia: </label> <br/>
									<input type="text" name="colonia" id="colonia" value="<?php echo $colonia; ?>" />
								</td>
								<td>
									<label class="textos"> C&oacute;digo Postal: </label> <br/>
									<input type="text" name="codigo_postal" id="codigo_postal" value="<?php echo $codigo_postal; ?>" />
								</td>
								<td>
									<label class="textos"> Tel&eacute;fono: </label> <br/>
									<input type="text" name="telefono" id="telefono" value="<?php echo $telefono_cliente; ?>" />
								</td>
							</tr>
							<tr>
								<td>
									<label class="textos"> Estado: </label> <br/>
									<select name="estado" id="estado">
										<option value="0"> Estados </option>
								<?php
									// SE REALIZA QUERY QUE CONSULTA LOS ESTADOS REGISTRADOS EN LA BASE DE DATOS
									$query_estados = "SELECT id_estado,estado
													  FROM estados";

									// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
									$resultado_estados = mysql_query($query_estados) or die(mysql_error());
									
									// SE REALIZA WHILE PARA MOSTRAR EL RESULTADO
									while ( $row_estado = mysql_fetch_array($resultado_estados) )
									{
										$id_estado_nuevo = $row_estado['id_estado'];
										$estado_nuevo = $row_estado['estado'];

										// SE VALIDA SI EL ESTADO OBTENIDO ES IGUAL AL DEL PACIENTE
										if ( $id_estado != "" && $id_estado == $id_estado_nuevo )
										{
									?>
											<option value="<?php echo $id_estado_nuevo; ?>" selected="selected"> <?php echo utf8_encode(ucwords(strtolower($estado_nuevo))); ?> </option>
									<?php
										}
										else
										{
										?>
											<option value="<?php echo $id_estado_nuevo; ?>"> <?php echo utf8_encode(ucwords(strtolower($estado_nuevo))); ?> </option>
										<?php
										}
									}
								?>
									</select>
								</td>
								<td colspan="2">
									<label class="textos"> Ciudad: </label> <br/>
									<select name="ciudad" id="ciudad">
										<option value="0"> Ciudades </option>
								<?php
									// SE REALIZA QUERY QUE CONSULTA LAS CIUDADES DE ACUERDO AL ESTADO OBTIENIDO
									$query_ciudades = "SELECT id_ciudad,ciudad
													   FROM ciudades
													   WHERE id_estado = '$id_estado'";

									// SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
									$resultado_ciudades = mysql_query($query_ciudades) or die(mysql_error());

									// SE REALIZA CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
									while ( $row_ciudad = mysql_fetch_array($resultado_ciudades) )
									{
										$id_ciudad_nuevo = $row_ciudad['id_ciudad'];
										$ciudad_nueva = $row_ciudad['ciudad'];

										// SE VALIDA SI LA CIUDAD DEL PACIENTE ES IGUAL A LA CIUDAD CONSULTADA
										if ( $id_ciudad != "" && $id_ciudad == $id_ciudad_nuevo )
										{
										?>
											<option value="<?php echo $id_ciudad_nuevo; ?>" selected="selected"> <?php echo utf8_encode(ucwords(strtolower($ciudad_nueva))); ?> </option>
										<?php
										}
										else
										{
										?>
											<option value="<?php echo $id_ciudad_nuevo; ?>"> <?php echo utf8_encode(ucwords(strtolower($ciudad_nueva))); ?> </option>
										<?php
										}
									}
								?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align:right;"> <input type="submit" name="btn_datos_personales" id="btn_datos_personales" title="Guardar Cambios" value="Guardar Cambios" class="fondo_boton" /> </td>
							</tr>
                    	</table>
                    </fieldset>
                </form>
                    <br />
                <form name="form_datos_molde" id="form_detalles_moldes" method="post" action="cambiar_estatus_moldes_modificacion.php">
                    <fieldset>
                    	<legend> Datos del Molde a Elaborar </legend>
                    	<table>
                            <tr>
                                <td> <input type="hidden" name="id_empleado_sistema" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" /> </td>
                                <td> <input type="hidden" name="hora_real" value="<?php echo $hora; ?>" /> </td>
                                <td> <input type="hidden" name="folio_molde" id="folio_molde" readonly="readonly" value="<?php echo $num_folio_molde; ?>" size="5" /> </td>
                            </tr>
                            <tr>
                                <td> <input type="hidden" name="fecha_actual" readonly="readonly" value="<?php echo $fecha_actual; ?>" size="10" /> </td>
                                <td> <input type="hidden" name="nombre_paciente" id="nombre_paciente" value="<?php echo $nombre; ?>" /> </td>
                            </tr>
                    		<tr>
                    			<td>
                    				<label class="textos"> Estilo: </label> <br/>
                    				<select name="estilo" id="estilos">
                    					<option value="0"> Estilos </option>
                    			<?php
                    				// SE REALIZA QUERY QUE OBTIENE LOS ESTILOS DISPONIBLES
                    				$query_estilos = "SELECT id_catalogo,estilo
													  FROM estilos,catalogo
													  WHERE estilos.id_estilo = catalogo.id_estilo";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
									$resultado_estilos = mysql_query($query_estilos) or die(mysql_error());

									// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO
									while ( $row_estilos = mysql_fetch_array($resultado_estilos) )
									{
										$id_catalogo_consultado = $row_estilos['id_catalogo'];
										$estilo_consultado = $row_estilos['estilo'];

										// SE VALIDA SI EL ESTILO DEL PACIENTE ES IGUAL AL CONSULTADO
										if ( $id_estilo != "" && $id_estilo == $id_catalogo_consultado )
										{
										?>
											<option value="<?php echo $id_catalogo_consultado; ?>" selected="selected"> <?php echo $estilo_consultado; ?> </option>
										<?php
										}
										else
										{
										?>
											<option value="<?php echo $id_catalogo_consultado; ?>"> <?php echo $estilo_consultado; ?> </option>
										<?php
										}
									}
                    			?>
                    				</select>
                                    <input type="hidden" name="precio_estilo" id="precio_estilo" />
                    			</td>
                    			<td>
                    				<label class="textos"> Material: </label> <br/>
                    				<select name="material" id="materiales">
                    					<option value="0"> Materiales </option>
                    			<?php
                    				// SE REALIZA QUERY QUE OBTIENE LOS MATERIALES
                    				$query_materiales = "SELECT cat_nivel1.id_material AS id_material,material
									 					 FROM cat_nivel1,catalogo,materiales
									 					 WHERE cat_nivel1.id_catalogo = catalogo.id_catalogo AND
									 	   				 cat_nivel1.id_material = materiales.id_material AND
									 	   				 cat_nivel1.id_catalogo = '$id_estilo'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
									$resultado_materiales = mysql_query($query_materiales) or die(mysql_error());

									// SE REALIZA CICLO PARA MOSTRAR EL RESULTADO OBTENIDO
									while ( $row_materiales = mysql_fetch_array($resultado_materiales) )
									{
										$id_material_consultado = $row_materiales['id_material'];
										$material_consultado = $row_materiales['material'];

										// SE VALIDA SI EL ID DEL MATERIAL DEL PACIENTE ES IGUAL AL CONSULTADO
										if ( $id_material != "" && $id_material == $id_material_consultado )
										{
										?>
											<option value="<?php echo $id_material_consultado; ?>" selected="selected"> <?php echo $material_consultado; ?> </option>
										<?php
										}
										else
										{
										?>
											<option value="<?php echo $id_material_consultado; ?>"> <?php echo $material_consultado; ?> </option>
										<?php
										}
									}
                    			?>
                    				</select>
                                    <input type="hidden" name="precio_material" id="precio_material" />
                    			</td>
                    			<td>
                    				<label class="textos"> Color: </label> <br/>
                    				<select name="color" id="colores">
                    					<option value="0"> Colores </option>
                    			<?php
                    				// SE REALIZA QUERY QUE OBTIENE LOS COLORES CORRESPONDIENTES AL MATERIAL
                    				$query_colores = "SELECT colores.id_color AS id_color,color
								  					  FROM colores,materiales_nivel1,materiales
								  					  WHERE materiales_nivel1.id_color = colores.id_color AND
								  					  materiales_nivel1.id_material = materiales.id_material AND
								  					  materiales_nivel1.id_material = '$id_material'";

								  	// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
								  	$resultado_colores = mysql_query($query_colores) or die(mysql_error());

								  	// SE REALIZA CICLO PARA MOSTRAR EL RESULTADO DEL QUERY
								  	while ( $row_colores = mysql_fetch_array($resultado_colores) )
								  	{
								  		$id_color_consultado = $row_colores['id_color'];
								  		$color_consultado = $row_colores['color'];

								  		// SE VALIDA PARA SABER SI EL COLOR DEL PACIENTE ES IGUAL AL CONSULTADO
								  		if ( $id_color != "" && $id_color == $id_color_consultado )
								  		{
								  		?>
								  			<option value="<?php echo $id_color_consultado; ?>" selected="selected"> <?php echo $color_consultado; ?> </option>
								  		<?php
								  		}
								  		else
								  		{
								  		?>
								  			<option value="<?php echo $id_color_consultado; ?>"> <?php echo $color_consultado; ?> </option>
								  		<?php
								  		}
								  	}
                    			?>
                    				</select>
                    			</td>
                    		</tr>
                    		<tr>
                    			<td>
                    				<label class="textos"> Ventilaci&oacute;n: </label> <br/>
                    				<select name="ventilacion" id="ventilaciones">
                    					<option value="0"> Ventilaciones </option>
                    			<?php
                    				// SE REALIZA QUERY QUE OBTIENE EL CAT_NIVEL_1
                    				$query_cat_nivel_1 = "SELECT id_cat_nivel1
									 					  FROM cat_nivel1,catalogo,materiales
									 					  WHERE cat_nivel1.id_catalogo = catalogo.id_catalogo AND
									 					  cat_nivel1.id_material = materiales.id_material AND
									 					  cat_nivel1.id_catalogo = '$id_estilo' AND
									 					  cat_nivel1.id_material = '$id_material'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
									$resultado_cat_nivel_1 = mysql_query($query_cat_nivel_1) or die(mysql_error());
									$row_cat_nivel_1 = mysql_fetch_array($resultado_cat_nivel_1);
									$id_cat_nivel1 = $row_cat_nivel_1['id_cat_nivel1'];

									// SE EJECUTA QUERY QUE OBTIENE LAS VENTILACIONES
									$query_ventilaciones = "SELECT ventilaciones.id_ventilacion AS id_ventilacion,nombre_ventilacion
															FROM cat_nivel2,cat_nivel1,ventilaciones,materiales
															WHERE cat_nivel1.id_material = materiales.id_material AND
											  				cat_nivel2.id_cat_nivel1 = cat_nivel1.id_cat_nivel1 AND
											  				cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion AND
											  				cat_nivel2.id_cat_nivel1 = '$id_cat_nivel1'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
									$resultado_ventilaciones = mysql_query($query_ventilaciones) or die(mysql_error());

									// SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
									while ( $row_ventilaciones = mysql_fetch_array($resultado_ventilaciones) )
									{
										$id_ventilacion_consultado = $row_ventilaciones['id_ventilacion'];
										$ventilacion_consultada = $row_ventilaciones['nombre_ventilacion'];

										// SE VALIDA SI LA VENTILACION DEL PACIENTE ES IGUAL A LA CONSULTADA
										if ( $id_ventilacion != "" && $id_ventilacion == $id_ventilacion_consultado )
										{
										?>
											<option value="<?php echo $id_ventilacion_consultado; ?>" selected="selected"> <?php echo $ventilacion_consultada; ?> </option>
										<?php
										}
										else
										{
										?>
											<option value="<?php echo $id_ventilacion_consultado; ?>"> <?php echo utf8_encode($ventilacion_consultada); ?> </option>
										<?php
										}
									}
                    			?>
                    				</select>
                                    <input type="hidden" name="precio_ventilacion" id="precio_ventilacion" />
                    			</td>
                    			<td>
                    				<label class="textos"> Calibre: </label> <br/>
                    				<input type="text" name="calibre" id="calibre" value="<?php echo $calibre; ?>" readonly="readonly"  />
                    			</td>
                    			<td>
                    				<label class="textos"> Salida: </label> <br/>
                    				<select name="salida" id="salidas">
                    					<option value="0"> Salidas </option>
                    			<?php
                    				// SE REALIZA QUERY QUE OBTIENE EL CAT NIVEL 1
                    				$query_cat_nivel_1_2 = "SELECT id_cat_nivel1
									 						FROM cat_nivel1,catalogo,materiales
									 						WHERE cat_nivel1.id_catalogo = catalogo.id_catalogo AND
										   					cat_nivel1.id_material = materiales.id_material AND
										   					cat_nivel1.id_catalogo = '$id_estilo' AND
										   					cat_nivel1.id_material = '$id_material'";

									// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
									$resultado_query_cat_nivel_1_2 = mysql_query($query_cat_nivel_1_2) or die(mysql_error());
									$row_cat_nivel_1_2 = mysql_fetch_array($resultado_query_cat_nivel_1_2);
									$id_cat_nivel1_2 = $row_cat_nivel_1_2['id_cat_nivel1'];

									// SE REALIZA QUERY QUE OBTIENE EL CAT NIVEL 2
									$query_cat_nivel_2 = "SELECT id_cat_nivel2
								  						  FROM cat_nivel2,cat_nivel1,ventilaciones
								  						  WHERE cat_nivel2.id_cat_nivel1 = cat_nivel1.id_cat_nivel1 AND
                                  	    				  cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion AND
                                        				  cat_nivel2.id_ventilacion = '$id_ventilacion' AND
      							        				  cat_nivel2.id_cat_nivel1 = '$id_cat_nivel1'";

      							    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
      							    $resultado_cat_nivel_2 = mysql_query($query_cat_nivel_2) or die(mysql_error());
      							    $row_cat_nivel_2 = mysql_fetch_array($resultado_cat_nivel_2);
      							    $id_cat_nivel2 = $row_cat_nivel_2['id_cat_nivel2'];



      							    // SE REALIZA QUERY QUE OBTIENE LAS SALIDAS DE ACUERDO A LA VENTILACION
      							    $query_salidas = "SELECT salidas.id_salida AS id_salida,salida
								  					  FROM salidas,cat_nivel3,cat_nivel2
								  					  WHERE cat_nivel3.id_cat_nivel2 = cat_nivel2.id_cat_nivel2 AND
      												  cat_nivel3.id_salida = salidas.id_salida AND
		 								  			  cat_nivel3.id_cat_nivel2 = '$id_cat_nivel2'";

		 							// SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO OBTENIDO
		 							$resultado_salidas = mysql_query($query_salidas) or die(mysql_error());

		 							// SE REALIZA UN CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS POR EL QUERY
		 							while ( $row_salidas = mysql_fetch_array($resultado_salidas) )
		 							{
		 								$id_salida_consultado = $row_salidas['id_salida'];
		 								$salida_consultada = $row_salidas['salida'];

		 								// SE VALIDA SI LA SALIDA DEL PACIENTE ES IGUAL A LA OBTENIDO ANTERIORMENTE
		 								if ( $id_salida != "" && $id_salida == $id_salida_consultado )
		 								{
		 								?>
		 									<option value="<?php echo $id_salida_consultado; ?>" selected="selected"> <?php echo $salida_consultada; ?> </option>
		 								<?php
		 								}
		 								else
		 								{
		 								?>
		 									<option value="<?php echo $id_salida_consultado; ?>"> <?php echo $salida_consultada; ?> </option>
		 								<?php
		 								}
		 							}
                    			?>
                    				</select>
                                    <input type="hidden" name="precio_salida" id="precio_salida" />
                    			</td>
                    		</tr>
                    		<tr>
                    			<td>
                    				<label class="textos"> Lado Oido: </label> <br/>
                    				<select name="lado_oido" id="lado_oido">
                    					<option value="0"> Lado del Oido </option>
                    			<?php
                    				// SE VALIDA QUE LADO DEL OIDO ES EL QUE TIENE EL PACIENTE
                    				if ( $lado_oido == "izquierdo" )
                    				{
                    				?>
                    					<option value="izquierdo" selected="selected"> Izquierdo </option>
                                        <option value="derecho"> Derecho </option>
                                        <option value="bilateral"> Bilateral </option>
                    				<?php
                    				}
                                    elseif ( $lado_oido == "derecho" )
                                    {
                                    ?>
                                        <option value="izquierdo"> Izquierdo </option>
                                        <option value="derecho" selected="selected"> Derecho </option>
                                        <option value="bilateral"> Bilateral </option>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                       <option value="izquierdo"> Izquierdo </option>
                    				   <option value="derecho"> Derecho </option>
                                       <option value="bilateral" selected="selected"> Bilateral </option>
                                    <?php
                                    }
                    			?>
                    				</select>
                    			</td>
                    			<td>
                    				<label class="textos"> Costo Total: </label> <br/>
                                    <?php
                                        if( $adaptacion == "si" && $reposicion == "" )
                                        {
                                        ?>
                                            <input type="text" name="costo_total" id="costo_total_molde" value="0.00" readonly="readonly" />
                                        <?php
                                        }
                                        elseif( $reposicion == "si" && $adaptacion == "" )
                                        {
                                        ?>
                                            <input type="text" name="costo_total" id="costo_total_molde" value="0.00" readonly="readonly" />
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <input type="text" name="costo_total" id="costo_total_molde" value="<?php echo $costo; ?>" readonly="readonly" />
                                        <?php
                                        }
                                    ?>
                                    <input type="hidden" name="costo_total_molde" value="<?php echo $costo; ?>" id="costo_total_molde" />
                    			</td>
                    			<td>
                                    <label class="textos"> Pago Parcial: </label> <br/>
                                    <input type="text" name="pago_parcial" id="pago_parcial" value="<?php echo $pago_parcial; ?>" readonly="readonly" />
                    			</td>
                    		</tr>
                            <tr>
                                <td>
                                    <label class="textos"> Adaptaci&oacute;n: </label> <br/>
                                    <?php
                                    if ( $adaptacion == "si" && $reposicion == "" ){
										$adapt = 1;
									}else{
										$adapt = 0;
									}
                		            ?>
                                    <select name="adaptacion" id="adaptacion">
                                      	<option <?php if ( $adapt == 1 ){ echo 'selected="selected"';} ?> value="si">Si</option>
                                        <option <?php if ( $adapt == 0 ){ echo 'selected="selected"';} ?> value="no">No</option>
                                    </select>
                                		
                                </td>
                                <td>
                                     <label class="textos"> Reposici&oacute;n: </label> <br />
                                     <?php
                                        if ( $reposicion == "si" && $adaptacion == "" ){
											$repo = 1;
										}
										else{
											$repo = 0;
										}
												
                                		?>
                                		<select name="reposicion" id="reposicion">
                                        	<option <?php if ( $repo == 1 ){ echo 'selected="selected"'; } ?> value="si">Si</option>
                                            <option <?php if ( $repo == 0 ){ echo 'selected="selected"'; } ?> value="no">No</option>
                                        </select>
                                </td>
                                <td>
                                    <label class="textos"> Estatus del Molde: </label> <br/>
                                    <?php
                                        if( $id_estatus_molde == 1 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="1" selected="selected"> Recepci&oacute;n Matriz </option>
                                            </select>
                                        <?php
                                        }
                                        elseif( $id_estatus_molde == 2 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="1"> Recepci&oacute;n Matriz </option>
                                                <option value="2" selected="selected"> Laboratorio, En Fabricaci&oacute;n </option>
                                                <option value="4"> Recepci&oacute;n Oculusen </option>
                                            </select>
                                        <?php
                                        }
                                        elseif ( $id_estatus_molde == 4 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="4" selected="selected"> Recepci&oacute;n Ocolusen </option>
                                            </select>
                                        <?php
                                        }
                                        elseif ( $id_estatus_molde == 5 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="1"> Recepci&oacute;n Matriz </option>
                                                <option value="2"> Laboratorio, En Fabricaci&oacute;n </option>
                                                <option value="8"> Laboratorio Fabricado </option>
                                                <option value="5" selected="selected"> Recepci&oacute;n Matriz Elaborado </option>
                                            </select>
                                        <?php
                                        }
                                        elseif( $id_estatus_molde == 6 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="4"> Recepci&oacute;n Ocolusen </option>
                                                <option value="2"> Laboratorio, En Fabricaci&oacute;n </option>
                                                <option value="8"> Laboratorio Fabricado </option>
                                                <option value="6" selected="selected"> Recepci&oacute;n Ocolusen Elaborado </option>
                                            </select>
                                        <?php
                                        }
                                        elseif ( $id_estatus_molde == 8 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                            <?php
                                                // SE REALIZA QUERY QUE OBTIENE DE QUE RECEPCION FUE ACEPTADO
                                                $query_recepcion = "SELECT id_registro, id_estatus_moldes
															        FROM movimientos_moldes
															        WHERE folio_num_molde = '$num_folio_molde'
                                                                    AND (id_estatus_moldes = '1' OR id_estatus_moldes = '4')
															        ORDER BY id_registro DESC";

                                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                                $resultado_recepcion = mysql_query($query_recepcion) or die(mysql_error());
                                                $row_recepcion = mysql_fetch_array($resultado_recepcion);
                                                $recepcion = $row_recepcion['id_estatus_molde'];

                                                // SE VALIDA QUE CUAL RECEPCION ES EL MOLDE
                                                if( $recepcion == 1 )
                                                {
                                                ?>
                                                    <option value="1"> Recepci&oacute;n Matriz </option>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="4"> Recepci&oacute;n Ocolusen </option>
                                                <?php
                                                }
                                            ?>
                                                <option value="2"> Laboratorio, En Fabricaci&oacute;n </option>
                                                <option value="8" selected="selected"> Laboratorio, Fabricado </option>
                                            </select>
                                        <?php
                                        }
                                        elseif( $id_estatus_molde == 9 )
                                        {
                                        ?>
                                            <select name="estatus_moldes" id="estatus_moldes">
                                                <option value="9" selected="selected">Almacen</option>
                                                <option value="2"> Laboratorio, En Fabricaci&oacute;n </option>
                                                <option value="8"> Laboratorio, Fabricado </option>
                                                <option value="6" >Recepci&oacute;n Ocolusen, Elaborado</option>
                                                <option value="5">Recepci&oacute;n Matriz, Elaborado</option>
                                            </select>
                                        <?php
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <label class="textos"> Observaciones: </label> <br/>
                                    <textarea name="observaciones" id="observaciones" style="resize: none; height: 60px; width: 300px;"> <?php echo $observaciones; ?> </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;"> <input type="submit" name="btn_datos_moldes" id="btn_datos_moldes" class="fondo_boton" value="Guardar Cambios" title="Guardar Cambios" /> </td>
                            </tr>
                    	</table>
                    </fieldset>
                </form>
                <br/>
                <form name="form_recibio" id="form_detalles_moldes" action="cambiar_estatus_moldes_modificacion.php" method="post" >
                    <?php
                        if( $id_estatus_molde == 2 )
                        {
                        ?>
                            <fieldset>
                    	        <legend> Recibio </legend>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="textos"> Recibio: </label> <br/>
                                                <select name="empleado" id="empleado">
                                            <?php
                                                // SE REALIZA QUERY QUE OBTIENE LOS EMPLEADOS REGISTRADOS
                                                $query_empleados = "SELECT id_empleado,CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
                                                                    FROM empleados";

                                                // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                                $resultado_empleados = mysql_query($query_empleados) or die(mysql_error());

                                                // SE REALIZA CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                                                while( $row_empleado = mysql_fetch_array($resultado_empleados) )
                                                {
                                                    $id_empleado = $row_empleado['id_empleado'];
                                                    $nombre_empleado = $row_empleado['nombre_empleado'];

                                                    // SE VALIDA PARA SELECCIONAR EL EMPLEADO QUE RECIBIO EL MOLDE
                                                    if( $id_empleado == $id_empleado_consultado )
                                                    {
                                                    ?>
                                                        <option value="<?php echo $id_empleado; ?>" selected="selected"> <?php echo $nombre_empleado; ?> </option>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                        <option value="<?php echo $id_empleado; ?>"> <?php echo $nombre_empleado; ?> </option>
                                                    <?php
                                                    }
                                                }
                                            ?>
                                                </select>
                                            </td>
                                            <td>
                                                <label class="textos"> Fecha de Entrada: </label> <br/>
                                                <label> <?php echo $fecha_entrada_normal; ?> </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> <input type="hidden" name="id_empleado_sistema" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" /> </td>
                                            <td> <input type="hidden" name="hora_real" value="<?php echo $hora; ?>" /> </td>
                                            <td> <input type="hidden" name="folio_molde" id="folio_molde" readonly="readonly" value="<?php echo $num_folio_molde; ?>" size="5" /> </td>
                                        </tr>
                                        <tr>
                                            <td> <input type="hidden" name="fecha_actual" readonly="readonly" value="<?php echo $fecha_actual; ?>" size="10" /> </td>
                                            <td> <input type="hidden" name="nombre_paciente" id="nombre_paciente" value="<?php echo $nombre; ?>" /> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: right;"> <input type="submit" name="btn_datos_recibio" id="btn_datos_recibio" class="fondo_boton" value="Guardar Cambios" title="Guardar Cambios" /> </td>
                                        </tr>
                                    </table>
                            </fieldset>
                        <?php
                        }
                    ?>
                </form>
                <form name="form_datos_fabricado" id="form_detalles_moldes" method="post" action="cambiar_estatus_moldes_modificacion.php">
                    <?php
                        if( $id_estatus_molde == 8 || $id_estatus_molde == 9 )
                        {
                        ?>
                            <fieldset>
                    	        <legend> Elaboraci&oacute;n </legend>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="textos"> Fabric&oacute;: </label> <br/>
                                            <select name="empleado" id="empleado">
                                        <?php
                                            // SE REALIZA QUERY QUE OBTIENE LOS EMPLEADOS REGISTRADOS EN EL SISTEMA
                                            $query_empleados_sistema = "SELECT id_empleado, CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
                                                                        FROM empleados";

                                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                            $resultado_empleados = mysql_query($query_empleados_sistema) or die(mysql_error());

                                            // SE REALIZA UN CICLO QUE MUESTRA TODOS LOS RESULTADOS OBTENIDOS
                                            while( $row_empleado = mysql_fetch_array($resultado_empleados) )
                                            {
                                                $id_empleado = $row_empleado['id_empleado'];
                                                $nombre_empleado = $row_empleado['nombre_empleado'];

                                                // SE VALIDA PARA SELECCIONAR EL EMPLEADO QUE FABRICO EL MOLDE
                                                if( $id_empleado == $id_empleado_fabrico )
                                                {
                                                ?>
                                                    <option value="<?php echo $id_empleado; ?>" selected="selected"> <?php echo $nombre_empleado; ?> </option>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="<?php echo $id_empleado; ?>"> <?php echo $nombre_empleado; ?> </option>
                                                <?php
                                                }
                                            }
                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <label class="textos"> Fecha de Fabricaci&oacute;n: </label> <br/>
                                            <label> <?php echo $fecha_fabricacion_normal; ?> </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="textos"> Observaciones: </label> <br/>
                                            <textarea name="observaciones" id="observaciones" style="resize: none; height: 60px; width: 300px;"> <?php echo $observaciones_fabricacion; ?> </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="hidden" name="id_empleado_sistema" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" /> </td>
                                        <td> <input type="hidden" name="hora_real" value="<?php echo $hora; ?>" /> </td>
                                        <td> <input type="hidden" name="folio_molde" id="folio_molde" readonly="readonly" value="<?php echo $num_folio_molde; ?>" size="5" /> </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="hidden" name="fecha_actual" readonly="readonly" value="<?php echo $fecha_actual; ?>" size="10" /> </td>
                                        <td> <input type="hidden" name="nombre_paciente" id="nombre_paciente" value="<?php echo $nombre; ?>" /> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right;"> <input type="submit" name="btn_datos_elaboracion" id="btn_datos_elaboracion" class="fondo_boton" title="Guardar Cambios" value="Guardar Cambios" /> </td>
                                    </tr>
                                </table>
                            </fieldset>
                        <?php
                        }
                    ?>
                </form>
                <form name="form_datos_elaboracion_2" id="form_detalles_moldes" method="post" action="cambiar_estatus_moldes_modificacion.php">
                    <?php
                        if( $id_estatus_molde == 5 || $id_estatus_molde == 6 )
                        {
                        ?>
                            <fieldset>
                    	        <legend> Elaboraci&oacute;n </legend>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="textos"> Fabric&oacute;: </label> <br/>
                                            <select name="empleado" id="empleado">
                                        <?php
                                            // SE REALIZA QUERY QUE OBTIENE LOS EMPLEADOS REGISTRADOS EN EL SISTEMA
                                            $query_empleados_sistema = "SELECT id_empleado, CONCAT(nombre,' ',paterno,' ',materno) AS nombre_empleado
                                                                        FROM empleados";

                                            // SE EJECUTA EL QUERY Y SE OBTIENE EL RESULTADO
                                            $resultado_empleados = mysql_query($query_empleados_sistema) or die(mysql_error());

                                            // SE REALIZA UN CICLO QUE MUESTRA TODOS LOS RESULTADOS OBTENIDOS
                                            while( $row_empleado = mysql_fetch_array($resultado_empleados) )
                                            {
                                                $id_empleado = $row_empleado['id_empleado'];
                                                $nombre_empleado = $row_empleado['nombre_empleado'];

                                                // SE VALIDA PARA SELECCIONAR EL EMPLEADO QUE FABRICO EL MOLDE
                                                if( $id_empleado == $id_empleado_fabrico )
                                                {
                                                ?>
                                                    <option value="<?php echo $id_empleado; ?>" selected="selected"> <?php echo $nombre_empleado; ?> </option>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="<?php echo $id_empleado; ?>"> <?php echo $nombre_empleado; ?> </option>
                                                <?php
                                                }
                                            }
                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <label class="textos"> Fecha de Fabricaci&oacute;n: </label> <br/>
                                            <label> <?php echo $fecha_fabricacion_normal; ?> </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="textos"> Observaciones: </label> <br/>
                                            <textarea name="observaciones" id="observaciones" style="resize: none; height: 60px; width: 300px;"> <?php echo $observaciones_fabricacion; ?> </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="hidden" name="id_empleado_sistema" value="<?php echo $_SESSION["id_empleado_usuario"]; ?>" /> </td>
                                        <td> <input type="hidden" name="hora_real" value="<?php echo $hora; ?>" /> </td>
                                        <td> <input type="hidden" name="folio_molde" id="folio_molde" readonly="readonly" value="<?php echo $num_folio_molde; ?>" size="5" /> </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="hidden" name="fecha_actual" readonly="readonly" value="<?php echo $fecha_actual; ?>" size="10" /> </td>
                                        <td> <input type="hidden" name="nombre_paciente" id="nombre_paciente" value="<?php echo $nombre; ?>" /> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right;"> <input type="submit" name="btn_datos_elaboracion" id="btn_datos_elaboracion" class="fondo_boton" title="Guardar Cambios" value="Guardar Cambios" /> </td>
                                    </tr>
                                </table>
                            </fieldset>
                        <?php
                        }
                    ?>
                </form>
                <br />
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <a href="lista_modificacion_moldes.php">
                                <input name="volver" type="button" value="Volver" class="fondo_boton" />
                            </a>
                        </td>
                    </tr>
                </table>
                <!-- Para mostrar el movimiento del auxiliar del paciente -->
                    <a href="#" onClick="muestra_oculta('contenido_a_mostrar')" title="">
                        <label class="textos" style="font-size:13px;"> Mostrar Movimientos del Molde </label>
                    </a>
                    <div id="contenido_a_mostrar">
                        <table style="border-collapse:collapse;">
                            <tr>
                                <th width="60">N° nota</th>
                                <th width="80">Fecha</th>
                                <th width="100">Hora</th>
                                <th width="130">Movimiento</th>
                                <th width="100">Responsable</th>
                                <th width="100">Estatus</th>
                            </tr>
     	                <?php
                            /* Consulta el movimiento del molde de la tabla movimientos_moldes */
    		                $consulta_movimientos_molde = mysql_query("SELECT *
                                                                       FROM movimientos_moldes
    						                                           WHERE folio_num_molde=".$num_folio_molde) or die(mysql_error());

                            while( $row8 = mysql_fetch_array($consulta_movimientos_molde) )
                            {
                    			$num_folio = $row8["folio_num_molde"];
                    			$id_movimiento = $row8["id_estado_movimiento"];
                    			$fecha_movimiento = $row8["fecha"];
                    			$fecha_movimiento_separada = explode("-", $fecha_movimiento);
                    			$fecha_movimiento_normal = $fecha_movimiento_separada[2]."/".$fecha_movimiento_separada[1]."/".$fecha_movimiento_separada[0];
                    			$hora_movimiento = $row8["hora"];
                    			$estatus_molde = $row8["id_estatus_moldes"];
                    			$id_responsable = $row8["id_empleado"];

                                /* Consulta el estado del movimiento */
                    			$consulta_descripcion_estado = mysql_query("SELECT estado_movimiento
    															            FROM estados_movimientos
                															WHERE id_estado_movimiento = ".$id_movimiento) or die(mysql_error());

    			                $row9 = mysql_fetch_array($consulta_descripcion_estado);
    			                $estado_movimiento = $row9["estado_movimiento"];

                                /* Consulta el responsable del movimiento */
                    			$consulta_responsable = mysql_query("SELECT alias
                                                                     FROM empleados
    																 WHERE id_empleado = ".$id_responsable) or die(mysql_error());

    			                $row10 = mysql_fetch_array($consulta_responsable);
                    			$alias_responsable = $row10["alias"];

                                /* Consulta el estatus de la reparacion */
    			                $consulta_estatus_molde = mysql_query("SELECT estado_molde
    							                                       FROM estatus_moldes
    															       WHERE id_estatus_moldes = ".$estatus_molde) or die(mysql_error());

                                $row11 = mysql_fetch_array($consulta_estatus_molde);
                    			$descripcion_estatus_molde = $row11["estado_molde"];
    	                ?>
                           	<tr>
                                <td> <?php echo $num_folio; ?> </td>
                                <td> <?php echo $fecha_movimiento_normal; ?> </td>
                                <td> <?php echo $hora_movimiento; ?> </td>
                                <td> <?php echo $estado_movimiento; ?> </td>
                                <td> <?php echo $alias_responsable; ?> </td>
                                <td> <?php echo $descripcion_estatus_molde; ?> </td>
                            </tr>
    	                <?php
    		                }
    	                ?>
                        </table>
                    </div><!--Fin de contenido a mostrar-->
                    <br /><br />
                </div><!--Fin contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</html>