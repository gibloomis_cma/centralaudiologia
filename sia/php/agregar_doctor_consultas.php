<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agregar Doctores para Consultas</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="wrapp">
<div id="contenido_columna2">
    <div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria" style="width:306px;">
                Servicios Medicos
            </div>
        </div><!--Fin de fondo titulo-->
		<?php
            include("config.php");
            if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                $filtro = $_POST['filtro'];
                $consultar_consultas = mysql_query("SELECT id_empleado FROM comisiones_consultas") or die(mysql_error());
                while($row_consulta = mysql_fetch_array($consultar_consultas)){
                    $id_empleado = $row_consulta["id_empleado"];
                    $buscar_consultas = mysql_query("SELECT COUNT(id_empleado) FROM empleados WHERE (nombre LIKE '%".$filtro."%'
                                                                                    OR paterno LIKE '%".$filtro."%'
                                                                                    OR materno LIKE '%".$filtro."%')
                                                                                    AND id_empleado=".$id_empleado)
                                                                                    or die(mysql_error());
                    $row_busqueda = mysql_fetch_array($buscar_consultas);
                    $variable_contador +=  $row_busqueda["COUNT(id_empleado)"];
                    $res2 = "Tu busqueda '".$filtro."', encontro '".$variable_contador."' resultado(s)";
                }
            }else{
                $res2="";
            }
        ?>
        <div class="buscar2">
            <form name="busqueda" method="post" action="agregar_doctor_consultas.php">
                <label class="textos"><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
        </div><!-- Fin de la clase buscar -->
        <div class="area_contenido2">
        	<div class="contenido_proveedor">
        <form name="forma1" action="proceso_agregar_consultas.php" method="post">
        <center>
            <table>
                <tr>
                    <th width="250">Concepto</th>
                    <th width="230">Doctor</th>
                    <th width="60">Precio</th>
                    <th width="60">Comisión</th>
                    <th></th>
                </tr>
    			<?php
					// QUERY QUE OBTIENE LOS almacenes
					if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
						$filtro = $_POST['filtro'];
						$consulta_lista_doctores = mysql_query("SELECT comision, id_subcategoria, comisiones_consultas.id_empleado,
																		nombre, materno, paterno 
																FROM comisiones_consultas, empleados 
																WHERE (nombre LIKE '%".$filtro."%'OR paterno LIKE '%".$filtro."%'
																OR materno LIKE '%".$filtro."%') AND
																comisiones_consultas.id_empleado=empleados.id_empleado ORDER BY id_subcategoria") 
																or die(mysql_error());
					}else{
						$consulta_lista_doctores = mysql_query("SELECT id_registro, comision, id_subcategoria, id_empleado 
																FROM comisiones_consultas ORDER BY id_subcategoria") or die(mysql_error());																
					}
					$n=0;
					while($row = mysql_fetch_array($consulta_lista_doctores)){
						$n++;
						$id_doctor = $row["id_empleado"];
						$comision = $row["comision"];
						$id_consulta = $row["id_registro"];
						$id_subcategoria = $row["id_subcategoria"];
						/* Consulta la subcategoria */
						$consulta_subcategoria=mysql_query("SELECT subcategoria, precio FROM subcategorias_productos, consultas
															WHERE subcategorias_productos.id_subcategoria=".$id_subcategoria." 
															AND subcategorias_productos.id_subcategoria=consultas.id_subcategoria")
															or die(mysql_error());
						$row_consulta_subcategoria=mysql_fetch_array($consulta_subcategoria);
						$subcategoria_servicios=$row_consulta_subcategoria["subcategoria"];
						$precio=$row_consulta_subcategoria["precio"];
						/* Consulta el nombre del empleado */
						$consulta_nombre_doctor = mysql_query("SELECT nombre, paterno, materno, ocupacion
															FROM empleados WHERE id_empleado=".$id_doctor) 
															or die(mysql_error());
						$row3 = mysql_fetch_array($consulta_nombre_doctor);
						$nombre = $row3["nombre"];
						$paterno = $row3["paterno"];
						$materno = $row3["materno"];
						$ocupacion = $row3["ocupacion"];

				?> 
                <tr>
                    <td>
                      	<label class="textos"><?php echo $subcategoria_servicios; ?></label>
                    </td><td>
                        <label class="textos">
                            <?php echo $ocupacion." ".$nombre." ".$paterno." ".$materno; ?>
                        </label>
                    </td><td>
                        <label class="textos">
                            <?php echo "$".number_format($precio,2); ?>
                        </label>
                    </td><td>
                        <label class="textos">
                            <?php echo $comision." %"; ?>
                        </label>
                    </td>
                    <td>
                    	<a href="modificar_doctor_consultas.php?id_consulta=<?php echo $id_consulta; ?>"><img src="../img/modify.png" /></a>
                  	</td>
                </tr>
   				<?php
					}
					if($n==0){
				?>  
                <tr>
                    <td style="text-align:center;" colspan="4">
                        <label class="textos">"No hay Doctores registrados"</label>
                    </td>
                </tr> 
				<?php
               		}	
				?>   			
            	<tr>
                	<th colspan="5">
                    	Nuevo Doctor
                    </th>
                </tr><tr>
                	<td id="alright">
                		<label class="textos">Doctor: </label>
                    </td><td colspan="3">
                        <select name="id_doctor" id="id_doctor">
                            <option value="0">Seleccione</option>
                            <?php
                                $consulta_doctores = mysql_query("SELECT nombre, paterno, materno, ocupacion, id_empleado
                                                                        FROM empleados WHERE ocupacion 
                                                                        LIKE '%Aud.%' OR ocupacion LIKE '%Dr%'") or die(mysql_error());
                                while($row2 = mysql_fetch_array($consulta_doctores)){
                                    $nombre = $row2["nombre"];
                                    $paterno = $row2["paterno"];	
                                    $materno = $row2["materno"];
                                    $ocupacion = $row2["ocupacion"];
                                    $id_empleado = $row2["id_empleado"];
                            ?>			
                            <option value="<?php echo $id_empleado; ?>">
                                <?php echo $ocupacion." ".$nombre." ".$paterno." ".$materno; ?>
                            </option>	
                            <?php 
                                }
                            ?>
                        </select>                	
                		<input type="button" onclick="window.location.href='agregar_personal2.php'" value="Agregar Dr(a)." class="fondo_boton">
                	</td>
            	</tr><tr>
                	<td id="alright">
                    	<label class="textos">Servicio: </label>
                    </td><td colspan="3">
                        <select name="servicio" id="servicios">
                            <option value="0" selected="selected">Seleccione</option>
                        </select> 
                    </td>
                </tr><tr>
                	<td id="alright">
                    	<label class="textos">Comision: </label>
                    </td><td colspan="3">
                    	<input name="comision" type="text" />%
                    </td>
                </tr><tr>
                	<td id="alright" colspan="5">
                    	<input name="accion" type="submit" value="Guardar" class="fondo_boton" />
                    </td>
                </tr>
            </table>
            </center>
            </form>
            </div><!--Fin de contenido proveedor -->
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>