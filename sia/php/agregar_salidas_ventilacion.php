<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Agregar Salidas </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
</head>

<body>
	<div id="wrapp">
		<div id="contenido_columna2">
					<div class="contenido_pagina">
						<div class="fondo_titulo1">
							<div class="categoria">
								Salidas
							</div><!-- FIN DIV CATEGORIA -->
						</div><!-- FIN DIV FONDO TITULO 1 -->
							<div class="area_contenido1">
								<br/>
								<?php
									include("config.php");
									$id_catalogo = $_GET['id_catalogo'];
									$id_cat_nivel1 = $_GET['id_cat_nivel1'];
									$query_estilo_consultado = mysql_query("SELECT estilo,foto
																			FROM estilos,catalogo
																			WHERE estilos.id_estilo = catalogo.id_estilo
																			AND id_catalogo = '$id_catalogo'") or die (mysql_error());
									
									$row_estilo = mysql_fetch_array($query_estilo_consultado);
									$estilo = $row_estilo['estilo'];
									$imagen = $row_estilo['foto'];
									
									$query_material = mysql_query("SELECT material
															   FROM cat_nivel1,materiales
															   WHERE cat_nivel1.id_material = materiales.id_material
															   AND id_catalogo = '$id_catalogo'
															   AND id_cat_nivel1 = '$id_cat_nivel1'") or die (mysql_error());
									$row_material = mysql_fetch_array($query_material);
									$material = $row_material['material'];
									
									$query_ventilaciones_consultadas = mysql_query("SELECT id_cat_nivel2,nombre_ventilacion
																					FROM cat_nivel2,ventilaciones
																					WHERE cat_nivel2.id_ventilacion = ventilaciones.id_ventilacion
																					AND cat_nivel2.id_cat_nivel1 = '$id_cat_nivel1'") or die (mysql_error());
								?>
								<div class="titulos"> Estilo </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td> <label class="textos"> Nombre del Estilo: </label> </td>
											<td>  &nbsp;<label style="font-size:12px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;"> <?php echo $estilo; ?> </label> </td>
											<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
											<td> <img width="100" height="100" src="../moldes/estilos/<?php echo $imagen; ?>"/> </td>
										</tr>
									</table>
								</center>
								<br/>
								<div class="titulos"> Material </div><!-- FIN DIV TITULOS -->
								<br/>
								<center>
									<table>
										<tr>
											<td colspan="2"> 
                                            <label class="textos"> Nombre del Material: </label><?php echo $material; ?> </label> 
                                            </td>
										</tr>
									</table>
								</center>
								<br/>
									<center>
									<table>
                                    	<tr><th colspan="3">Ventilaciones</th></tr>
										<tr>
											<th width="120"> N° Ventilación </th>
											<th> Ventilación </th>
                                            <th width="180"></th>
										</tr>
										<?php
										while( $row_ventilacion = mysql_fetch_array($query_ventilaciones_consultadas) )
										{
											$id_ventilacion = $row_ventilacion['id_cat_nivel2'];
											$ventilacion = $row_ventilacion['nombre_ventilacion'];
										?>
										<tr>
											<td id="centrado"><a href="agregar_salida_ventilacion.php?id_catalogo=<?php echo $id_catalogo?>&id_cat_nivel1=<?php echo $id_cat_nivel1; ?>&id_ventilacion=<?php echo $id_ventilacion; ?>"> <?php echo $id_ventilacion; ?></a> </td>
											<td>  <?php echo utf8_encode(ucwords(strtolower($ventilacion))); ?> </td>
											<td><a  href="agregar_salida_ventilacion.php?id_catalogo=<?php echo $id_catalogo?>&id_cat_nivel1=<?php echo $id_cat_nivel1; ?>&id_ventilacion=<?php echo $id_ventilacion; ?>">
                                            	Agregar Salidas 
                                            </a></td>
                                        </tr>
										<?php
										}
										?>
										<tr>
											<td colspan="3"> <br/> </td>
										</tr>
										<tr>
											<td colspan="3"> <center> <a href="agregar_ventilaciones_material.php?id_catalogo=<?php echo $id_catalogo; ?>&id_material=<?php echo $id_cat_nivel1; ?>"> <input type="button" name="terminar_salida" id="terminar_salida" value="Terminar" title="Terminar" class="fondo_boton"/> </a> </center> </td>
										</tr>
									</table><!-- FIN DE TABLA -->
									</center>
									<br/>
							</div><!-- FIN DIV AREA CONTENIDO1 -->
					</div><!-- FIN DIV CONTENIDO PAGINA -->
				</div><!-- FIN DIV CONTENIDO COLUMNA2 -->
	</div><!-- FIN DIV WRAPP -->
</body>

</html>
