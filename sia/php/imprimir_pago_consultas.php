<?php
    // SE RECIBEN LAS VARIABLES DEL FORMULARIO
    $id_empleado = $_GET['id_empleado'];
    $fecha_final_mysql = $_GET['fecha_final'];
    $fecha_final_separada = explode("-", $fecha_final_mysql);
    $fecha_final = $fecha_final_separada[2]."/".$fecha_final_separada[1]."/".$fecha_final_separada[0];
    $fecha_inicio_mysql = $_GET['fecha_inicio'];
    $fecha_inicio_separada = explode("-", $fecha_inicio_mysql);
    $fecha_inicio = $fecha_inicio_separada[2]."/".$fecha_inicio_separada[1]."/".$fecha_inicio_separada[0];

    // SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
    include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Reporte de Pago de Consultas </title>
    <script type="text/javascript" language="javascript">
        function hora()
        {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            document.getElementById("hora").innerHTML +=  h + ':' + min + ':' + sec;
            window.print();
        }
    </script>    
</head>
<body onload="hora()">
    <?php
        // SE REALIZA QUERY QUE OBTIENE EL NOMBRE COMPLETO DEL DOCTOR SELECCIONADO
        $query_nombre_doctor = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre_completo_doctor,ocupacion
                                FROM empleados
                                WHERE id_empleado = '$id_empleado'";

        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
        $resultado_nombre_doctor = mysql_query($query_nombre_doctor) or die(mysql_error());
        $row_nombre_doctor = mysql_fetch_array($resultado_nombre_doctor);
        $nombre_completo_doctor = $row_nombre_doctor['nombre_completo_doctor'];
        $ocupacion = $row_nombre_doctor['ocupacion'];

        // SE DECLARA UN CONTADOR PARA SABER SI EXISTEN CONSULTAS PARA EL DOCTOR SELECCIONADO
        $contador = 0;

        // SE DECLARA UNA VARIABLE PARA IR SUMANDA EL TOTAL DE LAS CONSULTAS REALIZADAS
        $total_consultas = 0;

        // SE DECLARA UNA VARIABLE PARA SUMAR LA CANTIDAD EN DINERO A PAGARLE AL DOCTOR
        $total_pago = 0;

        // SE REALIZA QUERY QUE OBTIENE LOS DATOS DE LAS CONSULTAS REALIZADAS POR EL DOCTOR SELECCIONADO
        $query_lista_consultas = "SELECT id_registro,fecha,pago,cantidad,descripcion
                                  FROM consultas,pago_consultas
                                  WHERE consultas.id_subcategoria = pago_consultas.id_subcategoria
                                  AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                  AND estado = 'No pagado'
                                  AND id_empleado = '$id_empleado'
                                  ORDER BY fecha ASC";

        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO EN UNA VARIABLE
        $resultado_lista_consultas = mysql_query($query_lista_consultas) or die(mysql_error());
    ?>
        <center>
            <form name="form_pago_consulta" method="post" action="guardar_pago_consultas.php">
                <table>
                    <tr>
                        <td colspan="5" style="text-align:center; font-weight:bold; font-size:24px;"> <label> Pago de Consultas </label> </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align:center; font-size:18px;"> <label> Del: &nbsp; <?php echo $fecha_inicio; ?> &nbsp; Al: &nbsp; <?php echo $fecha_final; ?> </label> </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align:center; font-size:18px;"> <label> <?php echo $ocupacion." ".$nombre_completo_doctor; ?> </label> </td>
                    </tr>
                </table>
                <table border="1" style="width:1000px;">
                    <tr>
                        <th style="font-size:14px; font-weight:bold;"> N° de Registro </th>
                        <th style="font-size:14px; font-weight:bold;"> Fecha </th>
                        <th style="font-size:14px; font-weight:bold;"> Cantidad </th>
                        <th style="font-size:14px; font-weight:bold;"> Pago </th>
                        <th style="font-size:14px; font-weight:bold;"> Descripci&oacute;n </th>
                    </tr>
                <?php
                    // SE REALIZA UN CICLO PARA MOSTRAR LOS RESULTADOS OBTENIDOS
                    while ( $row_lista_consultas = mysql_fetch_array($resultado_lista_consultas) )
                    {
                        $id_registro = $row_lista_consultas['id_registro'];
                        $fecha = $row_lista_consultas['fecha'];
                        $fecha_separada = explode("-", $fecha);
                        $fecha_normal = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
                        $cantidad = $row_lista_consultas['cantidad'];
                        $pago = $row_lista_consultas['pago'];
                        $descripcion = $row_lista_consultas['descripcion'];
                        $total_consultas = $total_consultas + $cantidad;
                        $total_pago = $total_pago + $pago;
                        $contador++;
                ?>
                        <tr>
                            <td style="text-align:center;"> <?php echo $id_registro; ?> </td>
                            <td style="text-align:center;"> <?php echo $fecha_normal; ?> </td>
                            <td style="text-align:center;"> <?php echo $cantidad; ?> </td>
                            <td style="text-align:center;"> <?php echo "$ ".number_format($pago,2); ?> </td>
                            <td style="text-align:center;"> <?php echo $descripcion; ?> </td>
                        </tr>
                <?php
                    }
                    // SE VALIDA SI EL CONTADOR ES MAYOR O ES IGUAL A CERO
                    if ( $contador == 0 )
                    {
                    ?>
                        <tr>
                            <td colspan="5" style="text-align:center;"> <h3> No existen consultas registradas para el Dr(a) <?php echo $nombre_completo_doctor; ?> </h3> </td>
                        </tr>
                    <?php
                    }
                    else
                    {
                    ?>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Total de Consultas <br/> <?php echo $total_consultas; ?> </td>
                            <td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Total de Pago <br/> <?php echo "$ ".number_format($total_pago,2); ?> </td>
                            <td>  </td>
                        </tr>
                    </table>
                    <?php
                        // SE REALIZA QUERY QUE OBTIENE EN RESUMEN EL TOTAL DE CADA TIPO DE CONSULTA REALIZADA POR EL DOCTOR SELECCIONADO
                        $query_resumen_categoria = "SELECT SUM(cantidad) As total_consultas,descripcion,SUM(pago) AS pago_total
                                                    FROM consultas,pago_consultas
                                                    WHERE consultas.id_subcategoria = pago_consultas.id_subcategoria
                                                    AND fecha BETWEEN '$fecha_inicio_mysql' AND '$fecha_final_mysql'
                                                    AND estado = 'No pagado'
                                                    AND id_empleado = '$id_empleado'
                                                    GROUP BY descripcion
                                                    ORDER BY fecha ASC";

                        // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO OBTENIDO
                        $resultado_resumen_categoria = mysql_query($query_resumen_categoria) or die(mysql_error());
                    ?>
                        <br/><br/>
                        <table border="1" style="width:1000px;">
                            <tr>
                                <th colspan="3" style="text-align:center; font-size:18px;"> Resumen de Consultas </th>
                            </tr>
                            <tr>
                                <th style="font-size:14px; font-weight:bold;"> Total </th>
                                <th style="font-size:14px; font-weight:bold;"> Descripci&oacute;n </th>
                                <th style="font-size:14px; font-weight:bold;"> Total a Pagar </th>
                            </tr>
                        <?php
                            $total_a_pagar = 0;
                            
                            // SE REALIZA UN CICLO PARA MOSTRAR EL RESULTADO OBTENIDO DEL QUERY
                            while ( $row_resumen = mysql_fetch_array($resultado_resumen_categoria) )
                            {
                                $total_consultas = $row_resumen['total_consultas'];
                                $descripcion = $row_resumen['descripcion'];
                                $pago_total = $row_resumen['pago_total'];
                                $total_a_pagar = $total_a_pagar + $pago_total;
                        ?>
                                <tr>
                                    <td style="text-align:center;"> <?php echo $total_consultas; ?> </td>
                                    <td style="text-align:center;"> <?php echo $descripcion; ?> </td>
                                    <td style="text-align:center;"> <?php echo "$ ".number_format($pago_total,2); ?> </td>
                                </tr>
                        <?php
                            }
                        ?>
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td style="text-align:center; color:#ac1f1f; font-size:14px; font-weight:bold;"> Total a Pagar <br/> <?php echo "$ ".number_format($total_a_pagar,2); ?> </td>
                                </tr>
                                <tr>
                                    <td id="hora" colspan="3" style="text-align:center; font-size:12px; font-weight:bold;"> Fecha y hora de Impresi&oacute;n: <?php echo date('d/m/Y')."  "; ?> </td>
                                </tr>
                            </table>
                        <?php
                        }
                    ?>
                </form>
            </center>
</body>
</html>