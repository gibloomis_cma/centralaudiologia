<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
	<?php
			date_default_timezone_set('America/Monterrey');
			$script_tz = date_default_timezone_get();
			$fecha = date("d/m/Y");
	?>
<div id="wrapp">
			<div id="contenido_columna2">
				<div class="contenido_pagina">
					<div class="fondo_titulo1">
            			<div class="categoria">
            				Pacientes
            			</div>
        			</div><!--Fin de fondo titulo-->
 	<?php 
            	include("config.php");
            	if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                	$filtro = $_POST['filtro'];
					$consulta_clientes_con_relacion_aparatos= mysql_query("SELECT DISTINCT(id_cliente) 
																				FROM relacion_aparatos")or die(mysql_error());
					while($row_clientes_relacion = mysql_fetch_array($consulta_clientes_con_relacion_aparatos)){
						$id_cliente_relacion = $row_clientes_relacion["id_cliente"];
                		$res_busqueda = mysql_query("SELECT COUNT(*) 
                                                    	FROM ficha_identificacion
                                                    	WHERE (nombre LIKE '%".$filtro."%' 
														OR paterno LIKE '%".$filtro."%'
														OR materno LIKE '%".$filtro."%') AND id_cliente=".$id_cliente_relacion)
														 or die(mysql_error());														
						$row_busqueda = mysql_fetch_array($res_busqueda);
						$busqueda += $row_busqueda["COUNT(*)"];
					}   
                	$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
            	}else{
               		$res2="";
            	}
	?>
        			<div class="buscar2">
                    <form name="busqueda" method="post" action="lista_pacientes_relacion_aparatos.php">
                        <label class="textos"><?php echo $res2; ?></label>
                        <input name="filtro" type="text" size="15" maxlength="15" />
                        <input name="buscar" type="submit" value="Buscar" class="fondo_boton" style="height:25px;" />
                    </form>
       				</div><!-- Fin de la clase buscar -->        
        			<div class="area_contenido2">
    				<center>
                        <table>
                            <tr style="width:650px;">
                                <th colspan="3">Relación de Aparatos</th>
                            </tr>
                            <tr>
                                <th width="100">Nº Paciente</th>
                                <th width="300">Nombre Completo</th>
                                <th width="50"></th>
                            </tr>
   	<?php 
				// QUERY QUE OBTIENE LOS almacenes
                if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                    $filtro = $_POST['filtro'];
					$consulta_relacion_aparatos = mysql_query("SELECT  DISTINCT(ficha_identificacion.id_cliente), 
																				nombre, paterno, materno 
                                                            		FROM relacion_aparatos, ficha_identificacion
                                                            		WHERE (nombre LIKE '%".$filtro."%' 
                                                            		OR paterno LIKE '%".$filtro."%' 
                                                            		OR materno LIKE '%".$filtro."%') 
                                                            		AND ficha_identificacion.id_cliente=
																	relacion_aparatos.id_cliente")
                                                            		or die(mysql_error());
				}else{
                	$consulta_relacion_aparatos = mysql_query("SELECT DISTINCT (ficha_identificacion.id_cliente), nombre, paterno, materno
                                                            FROM ficha_identificacion, relacion_aparatos 
                                                            WHERE ficha_identificacion.id_cliente=relacion_aparatos.id_cliente")
															or die(mysql_error());
				}
                $n_cliente=0;
				while($row = mysql_fetch_array($consulta_relacion_aparatos)){
					$id_cliente = $row["id_cliente"];
					$nombre = $row["nombre"];
					$paterno = $row["paterno"];
					$materno = $row["materno"];				
					$n_cliente++;
	?>
                            <tr>
                                <td><?php echo $id_cliente; ?></td>
                                <td style="text-align:left;"><?php echo $nombre." ".$paterno." ".$materno; ?></td>
                                <td><a href="descripcion_aparato_cliente.php?id_paciente=<?php echo $id_cliente; ?>">Ver</a></td>
                            </tr>        
    <?php
            	}
                if($n_cliente==0){
	?>
                            <tr>
                                <td style="text-align:center;" colspan="4">
                                    <label class="textos">"No hay Pacientes registrados"</label>
                                </td>
                            </tr>         
	<?php		
				}
	?>
						</table>
					</center>
        			<br />
                    	<div class="titulos">Agregar Nueva Relacion de Aparato</div>
                        <div class="contenido_proveedor">
                        <br />
                  <form name="forma1" method="post" action="proceso_guardar_relacion_aparatos.php">
                  <?php $id_sucursal = 1; ?>
                  			<input name="sucursal" type="hidden" value="<?php echo $id_sucursal; ?>" id="sucursal"/>
                        	<label class="textos">Paciente: </label>
                        	<select name="id_cliente" id="cliente">
                        		<option value="0">Seleccione</option>
   	<?php
				$consulta_todos_los_pacientes = mysql_query("SELECT id_cliente, nombre, paterno, materno 
																FROM ficha_identificacion") or die(mysql_error());
				while($row_todos_los_pacientes = mysql_fetch_array($consulta_todos_los_pacientes)){
					$id_cliente = $row_todos_los_pacientes["id_cliente"];
					$paterno = $row_todos_los_pacientes["paterno"];
					$materno = $row_todos_los_pacientes["materno"];
					$nombre = $row_todos_los_pacientes["nombre"];
	?>
    							<option value="<?php echo $id_cliente; ?>"><?php echo $nombre." ".$paterno." ".$materno; ?></option>
    <?php
				}
	?>
                        	</select>
               				<label class="textos">Folio del estudio para la asignación del aparato: </label>
                      		<input name="ultimo_estudio" type="text" id="estudio" size="6" maxlength="6" readonly="readonly" />
                   		<br /><br />
							<label class="textos">Fecha de la Venta: </label><?php echo $fecha; ?>
                            <input name="fecha_venta" type="hidden" value="<?php echo $fecha; ?>"  />
   						<br /><br />
                       		<label class="textos">Tipo de aparato:</label>
							<select name="tipo" id="tipo_aparato">
                            	<option value="0">Seleccione</option>
   	<?php
				$consulta_tipo_aparatos = mysql_query("SELECT * FROM tipos_modelos_aparatos")or die(mysql_error());
				while($row_tipo_aparatos = mysql_fetch_array($consulta_tipo_aparatos)){
					$modelo_aparato = $row_tipo_aparatos["modelo_aparato"];
					$id_tipo_modelo = $row_tipo_aparatos["id_tipo_modelo"];
					
	?>
    							<option value="<?php echo $id_tipo_modelo; ?>"><?php echo $modelo_aparato; ?></option>
    <?php
				}
	?>
                            </select>
                        	<label class="textos">Modelo y num. serie: </label>
                            <select name="modelos" id="modelos">
                            	<option value="0">Seleccione</option>
               				</select>
                           
	<br /><br />
                            <label class="textos">Estatus: </label>
                            <select name="estatus_relacion_aparato" id="estatus_relacion">
                            	<option value="0">Seleccione</option>
                           	</select>
                       	<br /><br />
                    		<label class="textos">Elaboro estudio: </label>
                            <input name="elaboro_estudio" type="text" id="elaboro_estudio" size="50" maxlength="50" readonly="readonly" />
        				<br /><br />
                        	<label class="textos">Quien hizo la venta: </label>
                        	<select name="quien_hizo_la_venta">
    							<option value="0">Seleccione</option>
   	<?php
				$consulta_nombre_empleado=mysql_query("SELECT nombre, paterno, materno, id_empleado 
															FROM empleados")or die(mysql_error());
				while($row_nombre_empleado=mysql_fetch_array($consulta_nombre_empleado)){
				$nombre = $row_nombre_empleado["nombre"];
				$paterno = $row_nombre_empleado["paterno"];
				$materno = $row_nombre_empleado["materno"];
				$id_empleado = $row_nombre_empleado["id_empleado"];
	?>        
    							<option value="<?php echo $id_empleado; ?>">
									<?php echo $nombre." ".$paterno." ".$materno; ?>
                                </option>            
    <?php
				}
	?>
	                       	</select>
                      	<br /><br />
                        	<label class="textos">Observaciones:</label><br />
       						<textarea name="observaciones" cols="60" rows="2"></textarea>
                   		<p align="right">
                                <input name="accion" type="submit" value="Guardar Relacion" class="fondo_boton"/>
       					</p>
                          </form>
                        </div><!--Fin de contenido proveedor-->
        			</div><!-- Fin de area contenido -->
				</div><!--Fin de contenido pagina-->
			</div><!--Fin de contenido clolumna 2-->
</div><!--Fin de wrapp-->
</body>
</body>
</html>