<?php
	// SE VALIDA CUAL BOTON HA SIDO OPRIMIDO EN LAS REIMPRESIONES DE LOS TICKETS
	if ( isset($_POST['btn_reimprimir_vale']) && $_POST['btn_reimprimir_vale'] == "Reimprimir Vale" ) 
	{
		// SE RECIBEN LAS VARIABLES CORRESPONDIENTES PARA GENERAR EL TICKET
		$id_vale_bateria = $_POST['folio_vale_bateria'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_vale_bateria.php?folio_vale=<?php echo $id_vale_bateria; ?>", "Vale de Baterías", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
	elseif ( isset($_POST['btn_reimprimir_vale_refaccion']) && $_POST['btn_reimprimir_vale_refaccion'] == 'Reimprimir Vale' ) 
	{
		$id_vale_refaccion = $_POST['folio_vale_refaccion'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_vale_refaccion.php?folio_vale=<?php echo $id_vale_refaccion; ?>", "Vale de Refacciones", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
	elseif ( isset($_POST['btn_reimprimir_ticket']) && $_POST['btn_reimprimir_ticket'] == 'Reimprimir Ticket' ) 
	{
		$folio_num_venta = $_POST['num_folio'];
		$sucursal = $_POST['id_sucursal'];
		$vendedor = $_POST['id_empleado'];
		$fecha = $_POST['fecha_folio'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_ticket_venta.php?folio_num_venta=<?php echo $folio_num_venta; ?>&sucursal=<?php echo $sucursal ?>&vendedor=<?php echo $vendedor; ?>&fecha=<?php echo $fecha; ?>", "Ticket de Ventas", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
	elseif ( isset($_POST['btn_reimprimir_corte']) && $_POST['btn_reimprimir_corte'] == 'Reimprimir Ticket' ) 
	{
		$id_sucursal = $_POST['sucursal_empleado'];
		$empleado_corte = $_POST['id_empleado_corte'];
		$fecha_corte = $_POST['fecha_corte_mysql'];
		$folio_corte = $_POST['folio_corte_venta'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_corte_venta.php?folio_corte=<?php echo $folio_corte; ?>&sucursal=<?php echo $id_sucursal ?>&responsable=<?php echo $empleado_corte; ?>&fecha_corte=<?php echo $fecha_corte; ?>", "Ticket Corte de Ventas", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
	elseif ( isset($_POST['btn_reimprimir_reparacion']) && $_POST['btn_reimprimir_reparacion'] == "Reimprimir Reparacion" ) 
	{
		$folio_num_reparacion = $_POST['folio_nota_reparacion'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_nota_reparacion.php?folio_num_reparacion=<?php echo $folio_num_reparacion; ?>", "Nota de Reparaci&oacute;n", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=850, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
	elseif ( isset($_POST['btn_reimprimir_molde']) && $_POST['btn_reimprimir_molde'] == "Reimprimir Molde" ) 
	{
		$folio_num_molde = $_POST['folio_nota_molde'];
	?>
		<script type="text/javascript" language="javascript">
			window.open("reimpresion_nota_molde.php?folio_num_molde=<?php echo $folio_num_molde; ?>", "Nota de Molde", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=850, height=1000");
			window.location.href = 'sia.php';
		</script>
	<?php
	}
?>