<?php
	include("config.php");
	include("metodo_cambiar_fecha.php");	
	$accion = $_POST["accion"];
	$fecha = $_POST["fecha"];
	$hora = $_POST["hora"];
	$descripcion = $_POST["descripcion"];
	$cantidad = $_POST["cantidad"];
	$uso_refaccion = $_POST["id_uso"];
	$origen = $_POST["id_origen"];
	$responsable = $_POST["responsable"];
	$observaciones = $_POST["observaciones"];
	$vale_refaccion = $_POST["folio_vale"];
	/* Si accion es igual a agregar has lo siguiente */
	if($accion == "Agregar"){
		/* Si vale de refaccion es igual a nada, entonces has los siguiente */
		if($vale_refaccion == ""){
			$fecha_mysql = cambiaf_a_mysql($fecha);
			/* Inserta en la tabla vales de refacciones los datos de vale */
			mysql_query("INSERT INTO vales_refacciones (fecha, hora, uso, responsable, observaciones) 
												VALUES ('".$fecha_mysql."','".$hora."','".$uso_refaccion."',
														'".$responsable."', '".$observaciones."')") 
														or die(mysql_error());
			/* Regresa el ultimo id, que sera el numero del vale de refaccion */
			$vale_refaccion = mysql_insert_id();
			/* Consulta la tabla base productos */	
			$consulta_id_base_refaccion = mysql_query("SELECT id_base_producto, id_subcategoria 
															FROM base_productos 
															WHERE id_base_producto ='".$descripcion."'")
															or die(mysql_error());
			$row = mysql_fetch_array($consulta_id_base_refaccion);
			$id_base_producto = $row["id_base_producto"];
			$id_subcategoria = $row["id_subcategoria"];	
			/* Inserta en la tabla descripcion de vales de refacciones los siguientes datos */
			mysql_query("INSERT INTO descripcion_vale_refacciones(id_vale_refaccion, origen, codigo, cantidad)
								VALUES('".$vale_refaccion."','".$origen."','".$id_base_producto."','".$cantidad."')") 
								or die(mysql_error());
			/* Regresa el ultimo id, que sera el registro de la descripcion del vale de refacciones */
			$id_descripcion_vale_refaccion = mysql_insert_id();
			/* Consulta los datos de la tabla de inventarios, la informacion de las refacciones en existencia */	
			$consulta_datos_inventario = mysql_query("SELECT id_registro, num_serie_cantidad, id_almacen
														FROM inventarios
														WHERE id_subcategoria='".$id_subcategoria."' AND
														id_articulo='".$id_base_producto."' AND id_almacen='".$origen."' 
														ORDER BY id_registro")or die(mysql_error());
			while($row2 = mysql_fetch_array($consulta_datos_inventario)){
				$id_registro[] = $row2["id_registro"];
				$num_serie_cantidad[] = $row2["num_serie_cantidad"];
				$id_almacen_inventario[] = $row2["id_almacen"];
			}
			for($i = 0; $i < count($num_serie_cantidad); $i++){
				$id_registro[$i];
				$num_serie_cantidad[$i];
				$id_almacen_inventario[$i];
				$cantidad_original_num_serie = $num_serie_cantidad[$i];
				if($cantidad != 0){
					$cantidad_nueva = $cantidad - $num_serie_cantidad[$i];
					$nueva_cantidad = $cantidad;
					$cantidad = $cantidad_nueva;
					/* Sin cantidad es menor que cero, has lo siguiente */
					if($cantidad_nueva <= 0){
						$cantidad = 0;
						$num_serie_cantidad[$i]= -($cantidad_nueva);
						/* Inserta en la tabla vale de refaccion temporal, los siguientes datos */
						mysql_query("INSERT INTO vale_refaccion_temporal(id_registro_inventario, cantidad,
																 id_registro_descripcion_vale_refaccion)
															VALUES('".$id_registro[$i]."','".$nueva_cantidad."',
																'".$id_descripcion_vale_refaccion."')")
																or die(mysql_error());
						/* Actualiza la tabla inventarios */
						mysql_query("UPDATE inventarios SET num_serie_cantidad='".$num_serie_cantidad[$i]."'
														WHERE id_registro='".$id_registro[$i]."' 
														AND id_almacen=".$id_almacen_inventario[$i]) 
														or die(mysql_error());
					/* Si cantidad es diferente de cero, has lo siguiente */
					}else{
						/* Inserta en la tabla vale de refaccion temporal, los siguientes datos */
						mysql_query("INSERT INTO vale_refaccion_temporal(id_registro_inventario, cantidad,
																 id_registro_descripcion_vale_refaccion)
															VALUES('".$id_registro[$i]."',
																	'".$cantidad_original_num_serie."',
																	'".$id_descripcion_vale_refaccion."')")
																	or die(mysql_error());
						/* Actualiza la tabla inventarios */
						mysql_query("UPDATE inventarios SET num_serie_cantidad='0'
														WHERE id_registro='".$id_registro[$i]."' AND 
														id_almacen=".$id_almacen_inventario[$i]) 
														or die(mysql_error());		
					}
				}
			}	
							
			header('Location: vale_refacciones.php?folio_vale='.$vale_refaccion);
		/* Si vale de bateria es diferente de nada */
		}else{
			/* Consulta la tabla base productos */	
			$consulta_id_base_refaccion = mysql_query("SELECT id_base_producto, id_subcategoria
															FROM base_productos 
															WHERE id_base_producto ='".$descripcion."'")
															or die(mysql_error());
			$row = mysql_fetch_array($consulta_id_base_refaccion);
			$id_base_producto = $row["id_base_producto"];
			$id_subcategoria = $row["id_subcategoria"];
			/* Inserta en la tabla descripcion de vales de refacciones los siguientes datos */
			mysql_query("INSERT INTO descripcion_vale_refacciones(id_vale_refaccion, origen, codigo, cantidad)
															VALUES('".$vale_refaccion."','".$origen."','".$id_base_producto."',
																	'".$cantidad."')") 
															or die(mysql_error());
			/* Regresa el ultimo id, que sera el registro de la descripcion del vale de refacciones */
			$id_descripcion_vale_refaccion = mysql_insert_id();
			/* Consulta los datos de la tabla de inventarios, la informacion de las refacciones en existencia */												
			$consulta_datos_inventario = mysql_query("SELECT id_registro, num_serie_cantidad, id_almacen
														FROM inventarios
														WHERE id_subcategoria='".$id_subcategoria."' AND
														id_articulo='".$id_base_producto."' AND 
														id_almacen='".$origen."' ORDER BY id_registro") 
														or die(mysql_error());
			while($row2 = mysql_fetch_array($consulta_datos_inventario)){
				$id_registro[] = $row2["id_registro"];
				$num_serie_cantidad[] = $row2["num_serie_cantidad"];
				$id_almacen_inventario[] = $row2["id_almacen"];
			}
			for($i = 0; $i < count($num_serie_cantidad); $i++){
				$id_registro[$i];
				$num_serie_cantidad[$i];
				$id_almacen_inventario[$i];
				$cantidad_original_num_serie = $num_serie_cantidad[$i];
				if($cantidad != 0){
					$cantidad_nueva = $cantidad - $num_serie_cantidad[$i];
					$nueva_cantidad = $cantidad;
					$cantidad = $cantidad_nueva;
					/* Sin cantidad es menor que cero, has lo siguiente */
					if($cantidad_nueva <= 0){
						$cantidad = 0;
						$num_serie_cantidad[$i]= -($cantidad_nueva);
						/* Inserta en la tabla vale de refaccion temporal, los siguientes datos */
						mysql_query("INSERT INTO vale_refaccion_temporal(id_registro_inventario, cantidad,
																 id_registro_descripcion_vale_refaccion)
															VALUES('".$id_registro[$i]."','".$nueva_cantidad."',
																'".$id_descripcion_vale_refaccion."')")
																or die(mysql_error());
						/* Actualiza la tabla inventarios */
						mysql_query("UPDATE inventarios SET num_serie_cantidad='".$num_serie_cantidad[$i]."'
														WHERE id_registro='".$id_registro[$i]."' 
														AND id_almacen=".$id_almacen_inventario[$i]) 
														or die(mysql_error());
					/* Si cantidad es diferente de cero, has lo siguiente */
					}else{
						/* Inserta en la tabla vale de refaccion temporal, los siguientes datos */
						mysql_query("INSERT INTO vale_refaccion_temporal(id_registro_inventario, cantidad,
																 id_registro_descripcion_vale_refaccion)
															VALUES('".$id_registro[$i]."',
																	'".$cantidad_original_num_serie."',
																	'".$id_descripcion_vale_refaccion."')")
																	or die(mysql_error());
						/* Actualiza la tabla inventarios */
						mysql_query("UPDATE inventarios SET num_serie_cantidad='0'
														WHERE id_registro='".$id_registro[$i]."' AND 
														id_almacen=".$id_almacen_inventario[$i]) 
														or die(mysql_error());		
					}
				}
			}				
			header('Location: vale_refacciones.php?folio_vale='.$vale_refaccion);
		}	
	}
	if($accion == "Guardar"){
		$fecha_mysql = cambiaf_a_mysql($fecha);
		/* Si uso de refaccion es igual a alguno de estos estados, entonces se hace lo siguiente */
		if($uso_refaccion == 6 or $uso_refaccion == 7 or $uso_refaccion == 8 or $uso_refaccion == 9 or $uso_refaccion == 10){
			/* Consulta el uso del vale de refaccion de la tabla vales_refacciones */
			$consulta_vale_refacciones=mysql_query("SELECT uso FROM vales_refacciones 
																		WHERE id_registro=".$vale_refaccion)
																		or die(mysql_error());
			$row_vale_refaccion = mysql_fetch_array($consulta_vale_refacciones);
			$uso_de_refaccion = $row_vale_refaccion["uso"];

			/* Consulta la descripcion del uso del vale de refaccion */
			$consulta_usos_refaccion=mysql_query("SELECT uso FROM uso_refacciones 
																	WHERE id_uso_refaccion=".$uso_de_refaccion)
																	or die(mysql_error());
			$row_usos_refacciones=mysql_fetch_array($consulta_usos_refaccion);
			$descripcion_uso_refaccion = $row_usos_refacciones["uso"];
			if($uso_de_refaccion==6){
				/* Sacamos el identificador para consultar para que almacen van */
				$almacen_vales_refaccion=explode(" ",$descripcion_uso_refaccion);
				$almacen_vales_refaccion[1];
				/* Consultamos el almacen a donde van a parar las baterias */
				$consultamos_almacen=mysql_query("SELECT id_almacen FROM almacenes WHERE almacen 
																					LIKE '%".$almacen_vales_refaccion[1]."%'")
																					or die(mysql_error());
				$row_consultamos_almacen=mysql_fetch_array($consultamos_almacen);
				$id_almacen_para_donde_van_refacciones=$row_consultamos_almacen["id_almacen"];
				/* Variable del almacen donde quedaran las baterias */
				$id_almacen_para_donde_van_refacciones;
			}else{
				/* Sacamos el identificador para consultar para que almacen van */
				$almacen_vales_refaccion=explode(" ",$descripcion_uso_refaccion);
				$almacen_vales_refaccion[2];
				/* Consultamos el almacen a donde van a parar las baterias */
				$consultamos_almacen=mysql_query("SELECT id_almacen FROM almacenes WHERE almacen 
																					LIKE '%".$almacen_vales_refaccion[2]."%'")
																					or die(mysql_error());
				$row_consultamos_almacen=mysql_fetch_array($consultamos_almacen);
				$id_almacen_para_donde_van_refacciones=$row_consultamos_almacen["id_almacen"];
				/* Variable del almacen donde quedaran las baterias */
				$id_almacen_para_donde_van_refacciones;
			}			
			/* Consultamos todos los registros de las refacciones de la descripcion del vale*/
			$consulta_descripcion_refacciones=mysql_query("SELECT id_registro, codigo, cantidad  
															FROM descripcion_vale_refacciones 
															WHERE id_vale_refaccion=".$vale_refaccion)or die(mysql_error());
			while($row_descripcion_refacciones=mysql_fetch_array($consulta_descripcion_refacciones)){
				$id_registro_refacciones = $row_descripcion_refacciones["id_registro"];
				$codigo_refaccion = $row_descripcion_refacciones["codigo"];
				$cantidad_refaccion = $row_descripcion_refacciones["cantidad"];
				/* Consultar el id_subcategoria de la tabla base_productos */
				$consulta_base_productos=mysql_query("SELECT id_subcategoria FROM base_productos 
																			WHERE id_base_producto=".$codigo_refaccion)
																			or die(mysql_error());
				$row_subcategoria_refaccion=mysql_fetch_array($consulta_base_productos);
				$id_subcategoria_refaccion=	$row_subcategoria_refaccion["id_subcategoria"];
				/* Se guardan en el inventario segun las refacciones y el almacen escogidos */
				mysql_query("INSERT INTO inventarios(id_subcategoria, id_articulo, id_almacen, num_serie_cantidad, fecha_entrada,
													 hora)
											VALUES('".$id_subcategoria_refaccion."','".$codigo_refaccion."',
													'".$id_almacen_para_donde_van_refacciones."','".$cantidad_refaccion."',
													'".$fecha_mysql."','".$hora."')")or die(mysql_error());
				mysql_query("DELETE FROM vale_refaccion_temporal 
									WHERE id_registro_descripcion_vale_refaccion=".$id_registro_refacciones) or die(mysql_error());
			}
		}
?>
		<script  language='javascript' type='text/javascript'> 
			alert('El vale de refacción con el N° de folio <?php echo $vale_refaccion; ?> se guardo exitosamente !');
			window.open("impresion_vale_refacciones.php", "Vale de Refacciones", "directories=no, location=no, menubar=no, resizable=no, scrollbars=yes, statusbar=no, tittlebar=no, width=250, height=1000");
			window.location.href='sia.php';
		</script>
<?php
	}
	if($accion == "Cancelar"){
		$vale_refaccion;
		/* Cuenta cuantos registro del numero del vale hay en la tabla descripcion de vales de bateria */
		$consulta_todo_descripcion_vale_refaccion = mysql_query("SELECT COUNT(id_registro) AS contador 
																FROM descripcion_vale_refacciones 
																WHERE id_vale_refaccion=".$vale_refaccion) 
																or die(mysql_error());
		$row_todo_descripcion_vale_refaccion = mysql_fetch_array($consulta_todo_descripcion_vale_refaccion);
		$id_registro_descripcion_vale = $row_todo_descripcion_vale_refaccion["contador"];	
		/* Si el registro de descripcion es diferente de cero, quiere decir que aun hay registros y manda un mensaje */
		if($id_registro_descripcion_vale <> 0){
?>
		<script  language='javascript' type='text/javascript'> 
            alert('Elimina toda la carga del vale de refacción, para poder eliminar el vale');
            window.location.href='vale_refacciones.php?folio_vale=<?php echo $vale_refaccion; ?>'
       	</script>
<?php
		/* Si es igual a cero, elimina el vale de bateria y manda un mensaje */
		}else{
		mysql_query("DELETE FROM vales_refacciones
							WHERE id_registro=".$vale_refaccion)or die(mysql_error());
?>
		<script  language='javascript' type='text/javascript'> 
            alert('El vale de refacción se elimino satisfactoriamente');
            window.location.href='sia.php'
       	</script>
<?php
		}
	}
?>