<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Reporte de Ventas </title>
    <link rel="stylesheet" href="../css/style3.css" type="text/css">
	<link rel="stylesheet" href="../css/themes/base/jquery.ui.all.css">
	<script src="../js/jquery-1.7.1.js"></script>
	<script src="../js/ui/jquery.ui.core.js"></script>
	<script src="../js/ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../css/themes/base/demos.css">
	<script>
	$(function() {
		var dates = $( "#from, #to" ).datepicker({
			defaultDate: "",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
	</script>
	<script language="javascript">
        <!-- Se abre el comentario para ocultar el script de navegadores antiguos -->
        function muestraReloj()
        {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";

            // Pongo el formato 12 horas
            if ( horas >= 12 )
            {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;

            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;

            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<input id='hora' name='hora' size='10' value='" + horas + ":" + minutos + ":" + segundos + " " + mt + "' type='hidden'/>";

            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers)
            {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;

            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }
        // Fin del script
    </script>
</head>
<body onload="muestraReloj()">
<div id="wrapp">
	<div id="contenido_columna2">
    	<div class="contenido_pagina">
			<div class="fondo_titulo1">
				<div class="categoria" style="width:405px">
                	Reporte Ventas
           		</div>
        	</div><!--Fin de fondo titulo-->

        	<div class="area_contenido1">
            	<div class="contenido_proveedor">
            	<br />
               		<div class="titulos"> Reporte de Ventas </div>
            	<br />
                	<form name="form" action="reporte_ventas_realizadas.php" method="post">
                	<center>
                    <div class="demo">
                  		<label class="textos"> Opciones de busqueda: </label><br />
                        <label class="textos"> Sucursal Matriz </label><input name="sucursal1" type="checkbox" value="1"/>
                        <label class="textos"> Sucursal Ocolusen </label><input name="sucursal2" type="checkbox" value="1" />
                    <br /><br />
                    	<label for="from" class="textos">De: </label>
                    	<input type="text" id="from" name="from"/>
                    	<label for="to" class="textos">A: </label>
                    	<input type="text" id="to" name="to"/>
                	</div><!-- End demo -->
                    </center>
            	<p align="center">
                	<input type="submit" name="accion" value="Aceptar" class="fondo_boton"/>
            	</p>
                	</form>
            	</div><!--Fin de contenido proveedor-->
 	<?php
					include("config.php");
					//include("metodo_cambiar_fecha.php");
                    if(isset($_POST['accion'])){
                        $accion = $_POST["accion"];
                        $fechaInicio = $_POST["from"];
                        $fecha_inicio_separada = explode("/", $fechaInicio);
                        $fechaFinal = $_POST["to"];
                        $fecha_final_separada = explode("/", $fechaFinal);
                    }else{
                        $accion="";                    
                    }
					

					if($accion == "Aceptar"){
						
						if(isset($_POST["sucursal1"])){
							$sucursal1 = 1;
						}else{
							$sucursal1 = 0;
						}
						if(isset($_POST["sucursal2"])){    						
    						$sucursal2 = 2;    					                   
                        }else{
                            $sucursal2 = 0;
                        } 
                        //Condiciones por las fechas
						if(isset($fechaInicio) && $fechaInicio != "" and isset($fechaFinal) && $fechaFinal !=" "){
							$fecha_inicio_mysql = $fecha_inicio_separada[2]."-".$fecha_inicio_separada[1]."-".$fecha_inicio_separada[0];
							$fecha_final_mysql = $fecha_final_separada[2]."-".$fecha_final_separada[1]."-".$fecha_final_separada[0];
  	?>
    				<center>
                    <style>
						td{
							font-size:10px;
						}
						th{
							font-size:11px;
						}
					</style>
                    <table>
                        <tr>
                        	<th width="50">Sucursal</th>
                            <th width="20">Nº Folio</th>
                            <th width="170">Fecha</th>
                            <th width="60">Vendedor</th>
                            <th width="400">Descripcion</th>
                            <th width="60">Cantidad</th>
                            <th width="60">Descuento</th>
                            <th width="70">Costo Total con Descuento</th>
                        </tr>
   	<?php
		if($sucursal1 == 1 && $sucursal2 == 2){
			$consulta_folios_venta=mysql_query("SELECT * FROM ventas
														WHERE fecha BETWEEN '".$fecha_inicio_mysql."' AND
														'".$fecha_final_mysql."' AND (id_sucursal = 1 OR id_sucursal = 2) ORDER BY folio_num_venta DESC")
														or die(mysql_error());
		}elseif($sucursal1 == 1 && $sucursal2 == 0){
			$consulta_folios_venta=mysql_query("SELECT * FROM ventas
														WHERE fecha BETWEEN '".$fecha_inicio_mysql."' AND
														'".$fecha_final_mysql."' AND id_sucursal = 1 ORDER BY folio_num_venta DESC")
														or die(mysql_error());
		}elseif($sucursal2 == 2 && $sucursal1 == 0){
			$consulta_folios_venta=mysql_query("SELECT * FROM ventas
														WHERE fecha BETWEEN '".$fecha_inicio_mysql."' AND
														'".$fecha_final_mysql."' AND id_sucursal = 2 ORDER BY folio_num_venta DESC")
														or die(mysql_error());
		}elseif($sucursal1 == 0 && $sucursal2 == 0){
			$consulta_folios_venta=mysql_query("SELECT * FROM ventas
														WHERE fecha BETWEEN '".$fecha_inicio_mysql."' AND
														'".$fecha_final_mysql."' ORDER BY folio_num_venta DESC")
														or die(mysql_error());
		}
        $contador = 0;
		while($row_folios_venta=mysql_fetch_array($consulta_folios_venta))
        {
			$descuento = $row_folios_venta["descuento"];
			$vendedor = $row_folios_venta["vendedor"];
			$folio_num_venta = $row_folios_venta["folio_num_venta"];
			$id_sucursal = $row_folios_venta["id_sucursal"];
			$fecha = $row_folios_venta["fecha"];
			$fecha_separada = explode("-", $fecha);
			$fecha_venta = $fecha_separada[2]."/".$fecha_separada[1]."/".$fecha_separada[0];
			$hora_venta = $row_folios_venta['hora'];
			$consultar_nombre_vendedor=mysql_query("SELECT alias FROM empleados
																WHERE id_empleado=".$vendedor)or die(mysql_error());
			$row_nombre_vendedor=mysql_fetch_array($consultar_nombre_vendedor);
			$alias = $row_nombre_vendedor["alias"];
			/* Consulta la descripcion de la venta */
			$consulta_descripcion_venta=mysql_query("SELECT * FROM descripcion_venta
																WHERE folio_num_venta='".$folio_num_venta."'
																AND id_sucursal='".$id_sucursal."'")or die(mysql_error());
			while($row_descripcion_venta=mysql_fetch_array($consulta_descripcion_venta))
            {
                $contador++;
				$descripcion = $row_descripcion_venta["descripcion"];
				$cantidad = $row_descripcion_venta["cantidad"];
				$costo_unitario = $row_descripcion_venta["costo_unitario"];
				$costo_total = ($cantidad * $costo_unitario);
				$costo_descuento = ($costo_total * $descuento)/100;
				$costo_con_descuento = $costo_total - $costo_descuento;
	?>
    						<tr>
                            	<td>
							<?php
								if($id_sucursal == 1){
									echo "Matriz";
								}else{
									echo "Ocolusen";
								}
							?>
                                </td>
                                <td><?php echo $folio_num_venta; ?></td>
                                <td style="text-align:center;"><?php echo $fecha_venta."<br/><label style='font-size:10px;'>".$hora_venta."</label>"; ?></td>
                                <td><?php echo $alias; ?></td>
                                <td><?php echo $descripcion; ?></td>
                                <td><?php echo $cantidad; ?></td>
                                <td><?php echo $descuento; ?></td>
                                <td><?php echo "$".number_format($costo_con_descuento,2); ?></td>
                            </tr>
  	<?php
			}
		}
	?>
						<br />
                        </table>
                        <?php
                            if($contador>0){  
                                $id_sucu=0;                              
                                if($sucursal1 == 1){ 
                                    $id_sucu+= 1; 
                                }
                                if($sucursal2 == 2){ 
                                    $id_sucu+= 2; 
                                }                               
                            ?>
                                <div id="opciones" style="float:right; margin-right:70px;">
                                    <form name="form_moldes" style="float:right; margin-left:20px;" method="post" action="reporte_ventas_excel.php">
                                        <div id="spanreloj"></div>
                                        <input type="hidden" name="id_sucursal" id="id_sucursal" value="<?php echo $id_sucu; ?>" />
                                        <input type="hidden" name="fecha_inicio" id="fecha_inicio" value="<?php echo $fecha_inicio_mysql; ?>" />
                                        <input type="hidden" name="fecha_final" id="fecha_final" value="<?php echo $fecha_final_mysql; ?>" />
                                        <input type="submit" name="btn_excel" id="btn_excel" value="" title="Exportar a Excel" />
                                    </form> &nbsp;&nbsp;
                                    <a href="#" title="Imprimir Reporte" style="float:right; margin-left:15px;" onclick="window.open('imprimir_reporte_ventas.php?sucursal=<?php if( $sucursal1 == 1 ){ echo "1"; } elseif( $sucursal2 == 1 ){ echo "2"; } ?>&fecha_inicio=<?php echo $fecha_inicio_mysql; ?>&fecha_final=<?php echo $fecha_final_mysql; ?>','Reporte de Pago de Consultas','toolbars=no,scrollbars=yes,location=no,statusbars=no,menubars=no, width=1150, height=700');"> <img src="../img/print icon.png"/> </a>
                                </div> <!-- FIN DIV OPCIONES -->
                                <br/> <br/>
                            <?php
                            }
                        ?>
                        </center>
                    	<br /><br/>
    <?php
						}
					}
	?>
        	</div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>