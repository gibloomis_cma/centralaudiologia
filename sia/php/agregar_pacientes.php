<?php
	error_reporting(0);

	// SE INICIA SESION PARA EL USUARIO QUE ENTRO AL SISTEMA
	session_start();

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Pacientes</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
	function agregar() 
	{
		campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="">Seleccione</option><option value="Telefono">Telefono</option><option value="Celular">Celular</option><option value="Correo">Correo</option><option value="Fax">Fax</option></select>';
		$("#nuevo").append(campo2);
		campo = '<label class="textos"> Descripcion: </label><input type="text" name="descripcion[]" /><label class="textos"> Titular: </label><input name="titular[]" type="text" /><br/><br/>';
		$("#nuevo").append(campo);
	}

	$(function() {
     		//call the function onload, default to page 1
     	getdata( 1 );
	});
		
	function getdata( pageno ){                     
   		var targetURL = 'lista_pacientes_resultado.php?page=' + pageno; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_pacientes').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_pacientes').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow');
	}
	
	function getdata2( ){   
		var valor = $('#paginas option:selected').attr('value');
		var targetURL = 'lista_pacientes_resultado.php?page=' + valor; //page no was used internally by the pagination class, its value was supplied by our navigation buttons
   		$('#tabla_lista_pacientes').html('<p><img src="../img/ajax-loader.gif" /></p>');       
   		$('#tabla_lista_pacientes').load( targetURL,{
   			filtro: "<?php echo $_POST['filtro']; ?>"
   		} ).hide().fadeIn('slow');                  
   		
	}     
</script>
</head>
<body>
	<div id="contenido_columna2">
        <div class="contenido_pagina">
			<div class="fondo_titulo1">
                <div class="categoria">
                    Pacientes
                </div>
        	</div><!--Fin de fondo titulo-->
            
            <?php 
						date_default_timezone_set('America/Monterrey');
						$script_tz = date_default_timezone_get();
						$fecha = date("d/m/Y");
						$hora = date("h:i:s A");
            			if( isset($_REQUEST['filtro']) and $_REQUEST['filtro'] != "" )
            			{
                			$filtro = $_REQUEST['filtro'];

                			// SE REALIZA QUERY PARA OBTENER EL RESULTADO DE LA BUSQUEDA REALIZADA
                			$res_busqueda = mysql_query("SELECT COUNT(DISTINCT(id_cliente)) AS contador
                                                                FROM ficha_identificacion
                                                                WHERE nombre LIKE '%".$filtro."%' 
                                                                OR paterno LIKE '%".$filtro."%' 
                                                                OR materno LIKE '%".$filtro."%'")
                                                                or die(mysql_error());

                			// SE ALMACENA EN UNA VARIABLE EL RESULTADO DEL QUERY EN FORMA DE ARREGLO
							$row_busqueda = mysql_fetch_array($res_busqueda);
							$busqueda += $row_busqueda["contador"];

							// SE VALIDA SI LA BUSQUEDA OBTUVO RESULTADOS
							if( $busqueda == 0 )
							{
								$busqueda = 0;	
							}
                			$res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
            			}
            			else
            			{
               				$res2 = "";
            			}
					?> 
            
            <div class="buscar2">
                <form name="busqueda" method="post" action="agregar_pacientes.php">
                    <label><?php echo $res2; ?></label>
                    <input name="filtro" type="text" size="15" maxlength="15" />
                    <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
                </form>
            </div><!-- Fin de la clase buscar2 -->                
       		<div class="area_contenido2">
            	<center>
                <form action="procesa_agregar_paciente.php" method="post" name="form_agregar_pacientes" 
                id="form_agregar_pacientes" onsubmit="return validarPacientes()" >
                <table>
                    <tr>
                        <th colspan="3">Pacientes</th>
                    </tr>
                </table>
                <div id="tabla_lista_pacientes">
					<img src="../img/ajax-loader.gif"/>
				</div>
                    
                <table>
                    <tr>
                        <th colspan="4">Nuevo Paciente</th>
                    </tr><tr>  
                        <td id="alright">                      
                            <input name="contacto" type="hidden" value="<?php echo $_SESSION["contacto"]; ?>" />                        	
                            <label class="textos">Nombre(s): </label>
                        </td><td>
                            <input name="nombre" type="text" size="30" maxlength="25" title="Nombre" />
                        </td><td id="alright">
                            <label class="textos">Edad: </label>
                        </td><td>
                            <input name="edad" type="text" size="3" maxlength="3" title="Edad" />
                        </td>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Paterno: </label>
                        </td><td>
                            <input name="paterno" type="text" size="30" maxlength="20" title="Apellido Paterno" />
                        </td><td id="alright">
                            <label class="textos">Materno: </label>
                        </td><td>
                            <input name="materno" type="text" size="30" maxlength="20" title="Apellido Materno" />
                        </td>
                    </tr><tr>
                        <td id="alright">
                            <label class="textos">Ocupacion: </label>
                        </td><td>
                            <input name="ocupacion" type="text" size="30" title="Ocupación" />
                        </td><td id="alright">
                            <label class="textos">Fecha de Nacimiento: </label>
                        </td><td>
                            <input name="fecha_nacimiento" type="text" size="10" maxlength="10" 
                            onkeyup="Validar(this,'/',patron,true)" title="Fecha de Nacimiento" />
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Genero:</label>
                        </td><td>
                        	<select name="genero" title="Genero" style="width:200px;">
                                <option value="0">--- Seleccione ---</option>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino">Masculino</option>
                            </select>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">RFC: </label>
                        </td><td>
                        	<input name="rfc" type="text" size="30" title="RFC" />
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                        <td colspan="4" style="text-align:center">
                            <label class="textos">--Dirección--</label>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Calle: </label>
                        </td><td>
                        	<input name="calle" type="text" size="30" title="Calle" />
                        </td><td id="alright">
                        	<label class="textos">Numero: </label>
                        </td><td>
                        	<label class="textos">Ext.: </label>
                        	<input name="num_ext" type="text" size="3" maxlength="5" onchange="validarSiNumero(this.value)" />
                            <label class="textos">Int.: </label>
                            <input name="num_int" type="text" size="3" maxlength="6" />
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Colonia:</label>
                        </td><td>
                        	<input name="colonia" type="text" size="30" maxlength="30" title="Colonia"/>
                        </td><td id="alright">
                        	<label class="textos">Codigo Postal: </label>
                        </td><td>
                        	<input name="codigo_postal" type="text" size="5" maxlength="5" 
                            title="Código Postal" onchange="validarCP(this.value)"/>
                        </td>
                    </tr><tr>
                    	<td id="alright">
                        	<label class="textos">Estado: </label>
                        </td><td>
                        	<select id="id_estado2" name="id_estado" title="Estados" style="width:200px;">
								<option value="0" selected="selected"> --- Estado --- </option>
								<?php									
									$consulta_estados = mysql_query("SELECT id_estado,estado FROM estados");
									while( $row3 = mysql_fetch_array($consulta_estados))
									{ 
										$id_estado = $row3["id_estado"];
										$estado = $row3["estado"];	
								?>
										<option value="<?php echo $id_estado; ?>"> 
											<?php echo utf8_encode($estado); ?> 
                                    	</option>
								<?php
									}
								?>
       						</select>
                        </td><td id="alright">
                        	<label class="textos">Ciudad: </label>
                        </td><td>
                        	<select name="id_municipio2" id="id_municipio" title="Municipios" disabled="disabled" style="width:200px;">
                                <option value="0" selected="selected">--- Municipio ---</option>
                            </select>
                        </td>
                    </tr><tr>
                    	<td colspan="3" id="alright">
                        	<label class="textos">Otra:</label>
                        </td><td>
                        	<input name="otra" type="text"  size="30"/>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>                                        
                    	<td colspan="4" style="text-align:center">
                        	<label class="textos">--Forma(s) de Contacto--</label>
                        </td>
               		</tr><tr>
                    	<td colspan="4" style="text-align:center">
                            <label class="textos">Tipo: </label>
                            <select name="tipo_telefono[]">
                                <option value="">Seleccione</option>
                                <option value="Telefono">Telefono</option>
                                <option value="Celular">Celular</option>
                                <option value="Correo">Telefono</option>
                                <option value="Fax">Celular</option>
                            </select>
                            <label class="textos"> Descripción: </label>
                            <input name="descripcion[]" type="text" />
                            <label class="textos"> Titular: </label>
                            <input name="titular[]" type="text" />
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">
                        	<div id="nuevo"></div>
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">
                        	<input type="button" class="fondo_boton" onclick="agregar();" value="Agregar"/>                        	
                        </td>
                    </tr><tr>
                    	<td colspan="4" style="text-align:center">
                        	<label class="textos">Tipo de contacto: </label>
                            <select name="tipo_paciente" id="tipo_paciente" title="Tipo de Paciente" style="width:200px;">
                                <option value="0" selected="selected">--- Seleccione ---</option>
                                <option value="Directo">Directo</option>
                                <option value="Institucion">Institucion</option>
                            </select>
                         	<select name="inst" id="inst" title="Institución" disabled="disabled" style="width:200px;">
                                <option value="0" selected="selected">--- Seleccione ---</option>
                            </select>
                        </td>
                    </tr><tr>
                    	<td colspan="4">
                        	<hr />
                        </td>
                    </tr><tr>
                    	<td colspan="4" id="alright">
                        	<input name="cancelar" type="reset" value="Cancelar" class="fondo_boton" title="Cancelar"/>              						
              				<input name="guarda" type="submit" value="Guardar" class="fondo_boton" title="Guardar"/>
                        </td>
                    </tr>
                </table> 
                </form>    
                </center>
			</div>              
		</div><!--Fin de contenido pagina-->
	</div><!--Fin de contenido columna 2-->
</body>
</html>