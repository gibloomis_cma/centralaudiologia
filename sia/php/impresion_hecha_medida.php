<?php
	error_reporting(0);
	include("config.php");
	$consulta = "SELECT CONCAT(nombre,' ',paterno,' ',materno) AS nombre
				 FROM ficha_identificacion";
	$query = mysql_query($consulta);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title> Moldes </title>
<link type="text/css" rel="stylesheet" href="../css/style3.css"/>
<link type="text/css" rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css"/>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery-ui-1.8.18.custom.js"></script>
<script type="text/javascript" language="javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" language="javascript">
    $(function(){
        <?php
            while($row = mysql_fetch_array($query)){
                $nombre = $row['nombre'];
                $elementos[] = '"'.$nombre.'"';
            }
            $arreglo = implode(", ", $elementos);
        ?>	
        var availableTags = new Array(<?php echo $arreglo; ?>);
        $("#nombre_paciente").autocomplete({
            source: availableTags
        });
    });
</script>
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Moldes
                </div><!-- FIN DIV CLASS CATEGORIA -->
            </div><!-- FIN DIV CLASS FONDO TITULO1 -->
            <div class="area_contenido1">
           	<br/>
                <div class="titulos"> Toma de Molde o Impresión </div>
           	<br />
            	<div class="contenido_proveedor">
              	<form name="impresion_molde" id="impresion_molde" method="post" action="procesa_impresion_hecha_medida.php" onsubmit="return validaImpresionHechaMedida()">
			<?php
                $id_estudio = $_GET['id_estudio'];
                $id_paciente = $_GET['id_cliente'];
                if( $id_estudio == "" and $id_paciente == "" ){
            ?>
                    <table>
                        <tr>
                            <td width="300"> Nombre del Paciente: </td>
                            <td> <input type="text" name="nombre_paciente" id="nombre_paciente"/> </td>
                            <td> N° de Estudio: </td>
                            <td>
                                <select id="id_estudio" name="id_estudio" disabled="disabled">
                                    <option value="0"> --- Seleccione Estudio --- </option>
                                </select>
                            </td>
                        </tr>
                    </table><!-- FIN DE TABLA -->
              	<br />
                <input name="id_paciente" id="id_paciente_actual" type="hidden" />
			<?php		
                }
                else{
            ?>
            	<input type="hidden" name="id_estudio_directo" id="id_estudio_directo" value="<?php echo $id_estudio; ?>"/>
                <input type="hidden" name="id_paciente" id="id_paciente" value="<?php echo $id_paciente; ?>"/> 
            <?php
                }
            ?>
                    <table>
                        <tr>
                            <td> Es impresion: </td>
                            <td>
                                <select id="impresion" name="impresion">
                                    <option value="0"> --- Seleccione --- </option>
                                    <option value="si"> Sí </option>
                                    <option value="no"> No </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <br/> </td>
                        </tr>
                        <tr>
                            <td> Fabricar hecho a la medida: </td>
                            <td>
                                <select id="hecho_medida" name="hecho_medida">
                                    <option value="0"> --- Seleccione ---</option>
                                    <option value="si"> Sí </option>
                                    <option value="no"> No </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2"> <center> <input type="submit" name="accion" class="fondo_boton" value="Siguiente" title="Siguiente"/> </center> </td>
                        </tr>
                    </table><!-- FIN TABLA -->
                    </form>
                </div><!-- Fin de contenido proveedor -->
            </div><!-- FIN DIV CLASS AREA CONTENIDO1 -->
        </div><!-- FIN DIV CLASS CONTENIDO PAGINA -->
    </div><!-- FIN DEL DIV CONTENIDO COLUMNA2 -->
</div><!-- FIN DIV WRAPP -->
</body>
</html>