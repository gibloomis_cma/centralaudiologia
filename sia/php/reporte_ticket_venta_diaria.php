<?php 
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Lista de Cortes de Ventas Diarias </title>
<link rel="stylesheet" href="../css/style3.css" type="text/css">
</head>
<body>
<div id="wrapp">
    <div id="contenido_columna2">
        <div class="contenido_pagina">
            <div class="fondo_titulo1">
                <div class="categoria">
                    Reportes
                </div>   
            </div><!--Fin de fondo titulo-->
        <?php
                include("config.php");
                /* Consulta el departamento del empleado que entro en el sistema */
                $consulta_departamento_empleado=mysql_query("SELECT id_departamento FROM empleados 
                                                                                WHERE id_empleado=".$_SESSION["id_empleado_usuario"])
                                                                                or die(mysql_error());
                $row_departamento_empleado=mysql_fetch_array($consulta_departamento_empleado);
                $id_departamento_empleado=$row_departamento_empleado["id_departamento"];
                /* Consulta la sucursal del departamento */
                $consulta_sucursal=mysql_query("SELECT id_sucursal FROM areas_departamentos 
                                                                    WHERE id_departamento=".$id_departamento_empleado)
                                                                    or die(mysql_error());
                $row_sucursal=mysql_fetch_array($consulta_sucursal);
                $id_sucursal=$row_sucursal["id_sucursal"];
                if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                    $filtro = $_POST['filtro'];
                    $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                        FROM corte_ventas_diario
                                                        WHERE folio_corte LIKE '%".$filtro."%' AND id_sucursal=".$id_sucursal)
                                                         or die(mysql_error());														
                    $row_busqueda = mysql_fetch_array($res_busqueda);
                    $busqueda += $row_busqueda["COUNT(*)"];  
                    $res2 = "Tu busqueda '".$filtro."', encontro ".$busqueda." resultado(s)";
                }else{
                    $res2="";
                }		
        ?>
            <div class="buscar2">
            <form name="busqueda" method="post" action="reporte_ticket_venta_diaria.php">
                <label class="textos"><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input name="buscar" type="submit" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
            </div>
            <div class="area_contenido2">
                <div class="contenido_proveedor">
                <?php
					if($id_sucursal == 1){
						$nombre_sucursal="Matriz";	
					}else{
						$nombre_sucursal="Ocolusen";	
					}
				?>
                <table>
                    <tr>
                        <th colspan="4">Lista de Cortes de Ventas Diarias Sucursal <?php echo $nombre_sucursal; ?></th>
                    </tr>
                    <tr>
                        <th width="120">N° Folio</th>
                        <th width="150">Fecha de Corte</th>
                        <th width="150">Hora de Corte</th>
                        <th width="100"></th>
                   </tr>
        <?php
            if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                $filtro = $_POST['filtro'];
                $consulta_ticket_ventas_diarias=mysql_query("SELECT id_registro, folio_corte, fecha_corte, hora 
                                                            FROM corte_ventas_diario
                                                            WHERE folio_corte LIKE '%".$filtro."%' AND id_sucursal=".$id_sucursal)
                                                            or die(mysql_error());
            }else{
                $consulta_ticket_ventas_diarias=mysql_query("SELECT id_registro, folio_corte, fecha_corte, hora 
                                                            FROM corte_ventas_diario WHERE id_sucursal=".$id_sucursal)
                                                            or die(mysql_error());
            }
            $n_folios=0;
            while($row_ticket_ventas_diarias = mysql_fetch_array($consulta_ticket_ventas_diarias)){
                $id_registro = $row_ticket_ventas_diarias["id_registro"];
                $folio_corte = $row_ticket_ventas_diarias["folio_corte"];
                $fecha_corte = $row_ticket_ventas_diarias["fecha_corte"];
                $fecha_corte_separada = explode("-", $fecha_corte);
                $fecha_corte_normal = $fecha_corte_separada[2]."/".$fecha_corte_separada[1]."/".$fecha_corte_separada[0];
                $hora_corte = $row_ticket_ventas_diarias["hora"];
                $n_folios++;            
        ?>
                    <tr>
                        <td><?php echo $folio_corte; ?></td>
                        <td><?php echo $fecha_corte_normal; ?></td>
                        <td><?php echo $hora_corte; ?></td>
                        <td><a href="mostrar_descripcion_corte.php?id_registro_corte=<?php echo $id_registro; ?>">Ver</a></td>
                    </tr>
       <?php		
            }               
            if($n_folios==0){
        ?>
                    <tr>
                        <td style="text-align:center;" colspan="4">
                            <label class="textos">"No hay Tickets registrados"</label>
                        </td>
                    </tr>         
        <?php
            }
        ?>
                </table>
                </div><!--Fin de contenido proveedor-->
            </div><!--Fin de area contenido-->
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>