<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Proveedores</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script language="javascript" type="text/javascript" src="../js/funcion.js"></script>
</head>
<body>
<div id="contenido_columna2">
	<div class="contenido_pagina">
        <div class="fondo_titulo1">
            <div class="categoria">
                Proveedores
            </div>
        </div><!--Fin de fondo titulo-->
        <div class="area_contenido1">
    	<?php
			include("config.php");       
			$id_proveedor = $_GET["id_proveedor"];
			$consulta_datos_proveedor = mysql_query("SELECT * FROM proveedores 
												WHERE id_proveedor = ".$id_proveedor)
												or die(mysql_error());
			$row = mysql_fetch_array($consulta_datos_proveedor);
			$proveedor = $row["proveedor"];
			$razon_social = $row["razon_social"];
			$calle = $row["calle"];
			$num_exterior = $row["num_exterior"];
			$num_interior = $row["num_interior"];
			$codigo_postal = $row["codigo_postal"];
			$colonia = $row["colonia"];
			$id_estado = $row["id_estado"];
			$id_ciudad = $row["id_ciudad"];
			$id_pais = $row["id_pais"];
			$credito = $row["credito"];
			$plazos_pago = $row["plazos_pago"];
			$rfc = $row["rfc"];
    	?>
            <div class="contenido_proveedor"  id="kls">
                <br />                                           
                <form action="procesa_modificar_proveedor.php" method="post" name="form_modificar_proveedor" 
                onsubmit="return validarModificarProveedor()" class="textos2"> 
                <table>                    
                    <tr>
                        <th colspan="4">Datos Generales</th>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <label class="textos">Nombre: </label>
                        </td>
                        <td style="text-align:left" colspan="3">
                            <input name="id_proveedor" type="hidden" value="<?php echo $id_proveedor; ?>" />
                            <input name="proveedor" type="text" value="<?php echo $proveedor; ?>" size="60" />
                        </td>
                  	</tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Razon Social: </label>
                        </td>
                        <td style="text-align:left" colspan="3">
                            <input name="razon_social" type="text" size="90" 
                        value="<?php echo $razon_social; ?>" maxlength="90" />
                        </td>
                    </tr><tr>                   
                        <td style="text-align:right">
                            <label class="textos">Calle: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="calle" id="calle" type="text" size="30" 
                            value="<?php echo $calle; ?>" title="Calle">
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Número: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="num_ext" id="num_ext" type="text" size="3" maxlength="5" 
                            title="Exterior" value="<?php echo $num_exterior; ?>"
                            onchange="validarSiNumero(this.value)">
                            <label class="textos"> - </label>                           
                            <input name="num_int" id="num_int" type="text" size="3" 
                            title="Interior" value="<?php echo $num_interior; ?>">
                        </td>
                   </tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Colonia:</label>
                        </td>
                        <td style="text-align:left">
                            <input name="colonia" id="colonia" type="text" size="30" 
                            value="<?php echo $colonia; ?>" title="Colonia">
                        </td>
                        <td style="text-align:right">
                            <label class="textos">C.P.: </label>
                        </td>
                        <td style="text-align:left;">
                            <input name="codigo_postal" id="codigo_postal" type="text" size="4" maxlength="5" 
                            title="Código Postal" onchange="validarCP(this.value)" value="<?php echo $codigo_postal; ?>">
                            <label class="textos"> RFC: </label>
                            <input name="rfc" type="text" value="<?php echo strtoupper($rfc); ?>" size="14" />
                        </td>
                    </tr><tr>
                        	<td style="text-align:right"><label class="textos">País: </label>
                            </td>
                            <td><select name="pais" id="pais">
                            	<option value="0">Seleccione</option>
                           		<?php
                                // QUERY QUE OBTIENE LOS ESTADOS DE MEXICO
                                $consulta_pais = mysql_query("SELECT * FROM paises")or die(mysql_error());
                                while( $row3 = mysql_fetch_array($consulta_pais))
                                { 
                                    $id_pais_consultado = $row3["id_pais"];
                                    $pais = $row3["pais"];	
									if($id_pais<>"" && $id_pais == $id_pais_consultado){
                                ?>
                                    <option value="<?php echo $id_pais; ?>" selected="selected">
                                    	<?php echo $pais; ?>
                                    </option>
                                <?php
									}else{
									?>
                                     <option value="<?php echo $id_pais_consultado; ?>">
                                    	<?php echo $pais; ?>
                                    </option>
                                    <?php	
									}
                                }
                                ?>
                            </select></td>
                            <td id="alright">
                                <label class="textos">Otra: </label>
                            </td><td id="alleft">
                                <input name="pais_nuevo" type="text" maxlength="50" size="40"/>
                            </td>
                  	</tr><tr>                    
                            <td style="text-align:right">
                                <label class="textos">Estado: </label>
                            </td>
                            <td style="text-align:left">
                                <select id="id_estado2" name="id_estado" title="Estados">
                                    <option value="0" selected="selected"> --- Estado --- </option>
                                <?php
                                    $consulta_estados = mysql_query("SELECT * FROM estados WHERE id_pais=".$id_pais);
                                    while( $row3 = mysql_fetch_array($consulta_estados)){ 
                                        $id_estado_consultada = $row3["id_estado"];
                                        $estado = $row3["estado"];
                                        if($id_estado<>"" && $id_estado == $id_estado_consultada){
                                ?>
                                    <option value="<?php echo $id_estado; ?>" selected="selected">
                                       <?php echo utf8_encode($estado); ?> 
                                    </option>
                                <?php
                                        }else{
                                ?>       
                                    <option value="<?php echo $id_estado_consultada; ?>">
                                        <?php echo utf8_encode($estado);?>
                                    </option>
                                <?php 
                                        } 
                                    }
                                ?>
                                </select>
                            </td>
                        	<td id="alright">
                            	<label class="textos">Otra: </label>
                            </td><td id="alleft">
                            	<input name="estado_nuevo" type="text" maxlength="50" size="40"/>
                            </td>
             		</tr><tr>
                        <td style="text-align:right">
                            <label class="textos">Ciudad: </label>
                        </td>
                        <td style="text-align:left">
                            <select name="id_municipio" id="id_municipio" title="Municipio">
                                <option value="0"> --- Ciudad --- </option>
                            <?php
                                $consulta_ciudad = mysql_query("SELECT ciudad, id_ciudad 
                                                                FROM ciudades 
                                                                WHERE id_estado=".$id_estado);
                                while( $row4 = mysql_fetch_array($consulta_ciudad)){ 
                                    $id_ciudad_consultada = $row4["id_ciudad"];
                                    $ciudad =$row4["ciudad"];
                                    if($id_ciudad<>"" && $id_ciudad == $id_ciudad_consultada){
                            ?>
                                <option value="<?php echo $id_ciudad; ?>" selected="selected">
                                    <?php echo $ciudad; ?> 
                                </option>
                            <?php
                                    }else{
                            ?>       
                                <option value="<?php echo $id_ciudad_consultada; ?>">
                                    <?php echo $ciudad; ?>
                                </option>
                            <?php
                                    } 
                                }
                            ?>
                            </select>
                        </td>
                        <td id="alright">
                        	<label class="textos">Otra:</label>
                        </td><td id="alleft">
                        	<input name="municipio_nuevo" type="text" size="40">
                        </td>
                    </tr><tr>                  
                        <td>
                            <label class="textos">Crédito: </label>
                        </td>
                        <td style="text-align:left">
                            $<input name="credito" id="credito" type="text" title="Crédito" 
                            onchange="validarSiNumero(this.value)" size="6" value="<?php echo $credito; ?>">
                        </td>
                        <td style="text-align:right">
                            <label class="textos">Plazo de Pago: </label>
                        </td>
                        <td style="text-align:left">
                            <input name="plazo_pago" id="plazo_pago" type="text" 
                            value="<?php echo $plazos_pago; ?>" title="Plazo de Pago" size="3">
                            <label class="textos">días</label>
                        </td> 
                    </tr><tr>                               
                        <td colspan="4" style="text-align:right">                        
                            <input name="accion" type="submit" value="Guardar" class="fondo_boton" 
                            title="Modificar Datos Generales"/>
                        </td>
                    </tr>
                </table>  
                </form>
            </div><!--Fin de contenido proveedor-->    <br />   
	        <div class="contenido_proveedor">
                <table>
                	<style type="text/css">
						#klss table tr td{
							border:1px solid #939;
						}
                    </style>
                    <tr>
                        <th colspan="6">Contacto(s)</th>
                    </tr>
					<?php    
                        $consulta_contacto = mysql_query("SELECT * FROM contactos_proveedores 
                                                        WHERE id_proveedor=".$id_proveedor) 
                                                        or die(mysql_error());
						$contador=0;            
                        while($row2 = mysql_fetch_array($consulta_contacto)){
                            $id_contacto_proveedor = $row2["id_contacto_proveedor"];
                            $nombre = $row2["nombre"];
                            $paterno = $row2["paterno"];
                            $materno = $row2["materno"];
                            $contador += 1;
                    ?>
	                <form name="forma2" action="procesa_modificar_contactos_proveedor.php" method="post" >                   	
                    <tr>
                        <td colspan="2">
                            <input name="id_contacto" type="hidden" value="<?php echo $id_contacto_proveedor; ?>" />
                            <input name="id_proveedor" type="hidden" value="<?php echo $id_proveedor; ?>" />
                            <label class="textos">Nombre:</label>
                            <input name="nombre" type="text" value="<?php echo $nombre; ?>" />
                            <label class="textos">Paterno:</label>
                            <input name="paterno" type="text" value="<?php echo $paterno; ?>" />
                            <label class="textos">Materno:</label>
                            <input name="materno" type="text" value="<?php echo $materno; ?>" />
                        </td>
                    </tr><tr>
                        <td colspan="2" style="text-align:center">
                            <label class="textos">Formas de contacto</label>
                        </td>
                    </tr>                             
					<?php							
                        $consulta_forma_contacto = mysql_query("SELECT * FROM forma_contacto_proveedores
                                                        WHERE id_contacto_proveedor=".$id_contacto_proveedor)
                                                        or die(mysql_error());                
                        while($row5 = mysql_fetch_array($consulta_forma_contacto)){
                            $id_forma_contacto = $row5["id_forma_contacto"];
                            $tipo = $row5["tipo"];
                            $descripcion = $row5["descripcion"];	            
                    ?>
                    <tr>
                        <td style="text-align:center" colspan="2">	
                            <input name="id_forma_contacto[]" type="hidden" value="<?php echo $id_forma_contacto; ?>" />
                            <label class="textos">Tipo:</label> 
                            <select name="tipo_telefono[]"/>
                        <?php
                            $formas_contacto[0] = "Telefono";
                            $formas_contacto[1] = "Celular";
                            $formas_contacto[2] = "Fax";
                            $formas_contacto[3] = "Correo";						 
                            for($i=0; $i<4; $i++){
                                if($tipo == $formas_contacto[$i]){							
                        ?>                                                           
                                    <option value="<?php echo $tipo; ?>" selected="selected">
                                        <?php echo $tipo; ?>
                                    </option>
                        <?php 
                                }else{
                        ?>                            
                                <option value="<?php echo $formas_contacto[$i]; ?>">
                                    <?php echo $formas_contacto[$i]; ?>
                                </option>
                        <?php
                                }
                            }
                        ?>
                            </select>
                            <label class="textos">Descripción:</label>
                            <input name="descripcion[]" type="text" value="<?php echo $descripcion; ?>" style="width:200px;"/>                            
                            <a href="proceso_eliminar_forma_contacto_proveedor.php?id_forma_contacto=<?php echo $id_forma_contacto; ?>&id_proveedor=<?php echo $id_proveedor; ?>">
                            	<img src="../img/delete.png" title="Eliminar <?php echo $tipo; ?>" />
                            </a>                                                            
                        </td>
                    </tr>         
		            <?php
                		}
            		?>  
                    <tr>                       	  
                        <td id="alright">
                            <input type="button" value="Agregar" class="fondo_boton"
                            title="Agregar Nueva Forma de Contacto"
                            onclick="location='agregar_forma_contacto_proveedor2.php?id_proveedor=<?php echo $id_proveedor; ?>&id_contacto=<?php echo $id_contacto_proveedor; ?>'"/>                                                       
                            <input name="accion" type="submit" value="Guardar Cambios" class="fondo_boton" 
                            title="Guardar Cambios de <?php echo $nombre." ".$paterno; ?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="2"><hr /></td>
                    </tr>
				</form>
	            
		                 	
        
        	<?php
            	}
        	?>  
            		<tr>
                    	<td colspan="2" style="text-align:center"> 
                        <input type="button" value="Volver" class="fondo_boton" title="Volver"
		               onclick="location='detalles_proveedor.php?id_proveedor=<?php echo $id_proveedor; ?>'"/>
                    	<input type="button" value="Nuevo Contacto" class="fondo_boton" title="Agregar Nuevo Contacto"
		               onclick="location='agregar_contacto_proveedor2.php?id_proveedor=<?php echo $id_proveedor; ?>'"/>
                       	</td>
                	</tr> 
            	</table>
            </div><!--Fin contenido proveedor-->
        </div><!--Fin de area contenido-->
    </div><!--Fin de contenido pagina-->
</div><!--Fin de contenido columna 2-->
</body>
</html>