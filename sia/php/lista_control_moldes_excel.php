<?php
    // SE RECIBE LA HORA POR METODO POST
    $hora = $_POST['hora_reparaciones'];

    // SE IMPORTAN LAS LIBRERIAS NECESARIAS PARA CONVERTIR EL ARCHIVO A EXCEL
    header("Content-Type: application/vnd.ms-excel");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("content-disposition: attachment;filename=lista_control_moldes.xls");

	// SE IMPORTA EL ARCHIVO DE CONEXION A LA BASE DE DATOS
	include('config.php');

	// SE REALIZA EL QUERY QUE OBTIENE TODOS LOS REGISTROS DE LA TABLA DE REPARACIONES
	$query_moldes = "SELECT folio_num_molde, CONCAT(nombre,' ', paterno,' ', materno) AS nombre_cliente, fecha_entrada, id_estatus_moldes, costo, adaptacion, reposicion, pago_parcial
                     FROM moldes
                     WHERE id_estatus_moldes <> 7
                     ORDER BY fecha_entrada DESC";

    // SE EJECUTA EL QUERY Y SE ALMACENA EL RESULTADO
    $resultado_query_moldes = mysql_query($query_moldes) or die(mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>  </title>
	<!--<link type="text/css" rel="stylesheet" href="../css/style3.css" />-->
	<script type="text/javascript" language="javascript" src="../js/jquery-1.7.1.js"></script>
</head>
<body>
	<div id="wrapp">       
		<div id="contenido_columna2">
    		<div class="contenido_pagina">
				<div class="fondo_titulo1">
					<div class="categoria" style="width:600px;">
           			</div>
        		</div><!--Fin de fondo titulo-->
        		<div class="area_contenido1">
            		<div class="contenido_proveedor">
            		<br />
               			<!--<div class="titulos"> Lista de Control de Moldes </div>-->
            		<br />
                    <table>
                        <tr>
                            <td colspan="4" style="font-size:14px; text-align:center; background-color:#7f7f7f; color:#FFF; font-weight:bold; height:20px;"> Lista de Control de Moldes <?php echo date('d/m/Y')." ".$hora; ?> </td>
                        </tr>
                    </table>
            		<table border="1">
            			<tr>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> N° Molde </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Nombre Completo </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Costo </th>
            				<th style="text-align:center; font-size:14px; background-color:#7f7f7f; color:#FFF; height:30px;"> Estatus </th>
            			</tr>
            		<?php
            			// SE REALIZA UN CICLO PARA MOSTRAR TODOS LOS REGISTROS DE LAS REPARACIONES
            			while ( $row_molde = mysql_fetch_array($resultado_query_moldes) )
            			{
            				$folio_num_molde = $row_molde["folio_num_molde"];
                            $nombre_cliente = $row_molde['nombre_cliente'];
                            $estado_molde = $row_molde["id_estatus_moldes"];
                            $fecha_entrada = $row_molde["fecha_entrada"];
                            $costo_molde = $row_molde['costo'];
                            $pago_parcial = $row_molde['pago_parcial'];
                            $adaptacion = $row_molde['adaptacion'];
                            $reposicion = $row_molde['reposicion'];
            
                            $consulta_estatus_nota_molde = mysql_query("SELECT * FROM estatus_moldes
                                                                        WHERE id_estatus_moldes = ".$estado_molde) or die(mysql_error());   
                            
                            $row_estatus_nota_molde = mysql_fetch_array($consulta_estatus_nota_molde);
                            $estatus_molde = $row_estatus_nota_molde["estado_molde"];                                 
                    ?>
                            <tr>
                                <td style="font-size:12px; text-align:center;"> <?php echo  $folio_num_molde; ?> </td>
                                <td style="font-size:12px;"> <?php echo $nombre_cliente; ?> </td>
                                <td style="font-size:12px; text-align:center;"> 
                                    <?php 
                                        if($adaptacion == "si" && $reposicion == "")
                                        { 
                                            echo "$ 0.00"; 
                                        }
                                        elseif( $reposicion == "si" && $adaptacion == "" )
                                        { 
                                            echo "$ 0.00"; 
                                        }
                                        else
                                        { 
                                            echo "$ ".number_format($costo_molde - $pago_parcial,2); 
                                        } 
                                    ?> 
                                </td>
                                <td style="font-size:12px; text-align:center;"> <?php echo $estatus_molde; ?> </td>
                            </tr>
            		<?php
            			}
            		?>
            		</table>
            		<br/>
            	</div><!--Fin de contenido proveedor-->
        	</div><!--Fin de area contenido-->       
        </div><!--Fin de contenido pagina-->
    </div><!--Fin de contenido columna 2-->
</div><!--Fin de wrapp-->
</body>
</html>