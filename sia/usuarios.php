<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Personal</title>
<link rel="stylesheet" href="../css/style3.css" type="text/css"/>
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/Validacion.js"></script>
<script type="text/javascript">
function agregar() 
{
	campo2 = '<label class="textos">Tipo: </label><select name="tipo_telefono[]"><option value="">Seleccione</option><option value="Telefono">Telefono</option><option value="Celular">Celular</option></select>';
	$("#nuevo").append(campo2);
	campo = '<label class="textos"> Descripcion: </label></td><td><input type="text" name="descripcion[]" /><br/><br/>';
	$("#nuevo").append(campo);
}
</script>
</head>
<body>
<div id="contenido_columna2">
	<div class="contenido_pagina">
    	<div class="fondo_titulo1">
        	<div class="categoria">
            	Usuarios
            </div><!-- Fin DIV categoria -->
        </div><!--Fin de fondo titulo-->
        <div class="buscar2">
		<?php
            // SE IMPORTA EL ARCHIVO QUE CONTIENE LA CONEXION A LA BASE DE DATOS 
            include("config.php");	
			$res2="";							
            if(isset($_POST['filtro']) and $_POST['filtro'] != ""){
                $filtro = $_POST['filtro'];				
                $res_busqueda = mysql_query("SELECT COUNT(*) 
                                                    FROM usuarios
                                                    WHERE usuario LIKE '%".$filtro."%'")
													or die(mysql_error());	
                
				$row_busqueda = mysql_fetch_array($res_busqueda);
                $res2 = "Tu busqueda '".$filtro."', encontro ".$row_busqueda['COUNT(*)']." resultado(s)";
            }
        ?>              
            <form name="busqueda" method="post" action="usuarios.php">
                <label><?php echo $res2; ?></label>
                <input name="filtro" type="text" size="15" maxlength="15" />
                <input type="submit" name="buscar" value="Buscar" class="fondo_boton" style="height:25px;" />
            </form>
        </div><!-- Fin de la clase buscar2 -->                
        <div class="area_contenido2">
            <center>
            <form  action="procesa_usuarios.php" method="post"
            name="form_agregar_personal" >       
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Contraseña</th>
                    <th colspan="2">Departamento</th>
                </tr>
				<?php 
                    if(isset($_POST['buscar']) and $_POST['buscar'] != ""){
                        $consulta_personal = mysql_query("SELECT DISTINCT(id_usuario), 
                                                            user, pass, departamentos, nombre, paterno, materno
                                                            FROM empleados, usuarios, areas_departamentos
                                                            WHERE nombre LIKE '%".$filtro."%' 
                                                            OR paterno LIKE '%".$filtro."%' 
                                                            OR materno LIKE '%".$filtro."%' 
                                                            AND usuarios.id_empleado=empleados.id_empleado 
                                                            AND empleados.id_departamento=areas_departamentos.id_departamento")
                                                            or die(mysql_error());
                    }else{
                        $consulta_personal = mysql_query("SELECT id_usuario, user, pass, departamentos, nombre, paterno, materno
                                                            FROM empleados, usuarios, areas_departamentos 
                                                            WHERE usuarios.id_empleado=empleados.id_empleado 
                                                            AND empleados.id_departamento=areas_departamentos.id_departamento") or die(mysql_error());
                    }
                    $n_empleados=0;
                    while($row = mysql_fetch_array($consulta_personal)){
                        $id_usuario = $row["id_usuario"];
                        $user = ucwords($row["user"]);
                        $pass = ucwords($row["pass"]);
                        $departamento = ucwords($row["departamentos"]);
                        $nombre = ucwords($row["nombre"]);
                        $paterno = ucwords($row["paterno"]);
                        $materno = ucwords($row["materno"]);				
                        $n_empleados++;
                ?>                 
				<tr>
                    <td style="text-align:center">
                    	<label class="textos">
	                        <?php echo $nombre." ".$paterno." ".$materno; ?>
                        </label>
                    </td><td>
                    	<label class="textos">
                        	<?php echo $user; ?>
                        </label>
                    </td><td>
                    	<label class="textos"><?php echo $pass; ?></label>
                    </td><td>
                    	<label class="textos"><?php echo $departamento; ?></label>
                    </td> 
                    <td id="alright">
                    	 <a href="modificar_personal.php?id_empleado=<?php echo $id_usuario; ?>">
                         	<img src="../img/modify.png" title="<?php echo $nombre." ".$paterno." ".$materno; ?>" />
                         </a>
                    </td>                   
                </tr>
				<?php
                    }
                    if($n_empleados==0){
                ?>
        		<tr>
                    <td style="text-align:center" colspan="5">
                    	<label class="textos">No hay Usuarios registrados</label>
                    </td>                    
                </tr>
				<?php
                    }
                ?>
                </table>
                <br />                
                <table>
                	<tr>
                    	<th colspan="4">Nuevo Usuario</th>
                    </tr>
                	<tr>
                    	<td style="text-align:right">                    		
                    		<label class="textos">Usuario: </label>
                        </td><td style="text-align:left">
                   			<input name="user" type="text" size="25" maxlength="25" />
                        </td><td style="text-align:right">
                            <label class="textos">Contraseña: </label>
                        </td><td style="text-align:left">
                            <input name="pass" type="text" maxlength="16" size="25"/>
                        </td>
					</tr>
				</table>                        
                </form>
			</center>               
        </div>
    </div><!--Fin de cuerpo-->   
</div><!--Fin de wrapp-->
</body>
</html>